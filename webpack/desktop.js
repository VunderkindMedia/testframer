const path = require('path')

module.exports = function () {
    return {
        entry: './src/index.tsx',
        output: {
            filename: 'bundle.js?v=[contenthash:6]',
            path: path.resolve(__dirname, '../build')
        }
    }
}
