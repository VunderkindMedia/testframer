const path = require('path')

module.exports = function () {
    return {
        entry: './src/index-mobile.tsx',
        output: {
            filename: 'bundle.js?v=[contenthash:6]',
            path: path.resolve(__dirname, '../build-mobile')
        }
    }
}
