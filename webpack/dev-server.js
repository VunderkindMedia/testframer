module.exports = function () {
    return {
        devServer: {
            stats: 'errors-only',
            proxy: {
                '/api': 'http://localhost:3000'
            }
        }
    }
}
