const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

module.exports = function () {
    return {
        mode: 'development',
        module: {
            rules: [
                {
                    test: /\.(png|jpg|gif|svg)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                outputPath: './images/'
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff2)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                outputPath: './fonts/'
                            }
                        }
                    ]
                },
                {
                    test: /\.mp4$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                outputPath: './video/'
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new ForkTsCheckerWebpackPlugin({
                eslint: {
                    files: './src/**/*.{ts,tsx,js,jsx}'
                }
            })
        ]
    }
}
