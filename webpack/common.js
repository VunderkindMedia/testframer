const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const Dotenv = require('dotenv-webpack')
const workboxPlugin = require('workbox-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = function () {
    return {
        // optimization: {
        //     splitChunks: {
        //         chunks: 'all'
        //     }
        // },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: '/node_modules/'
                },
                {
                    test: /\.inline.svg$/,
                    use: ['@svgr/webpack']
                }
            ]
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.json']
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env.APP_VERSION': JSON.stringify(new Date().toGMTString())
            }),
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: './src/index.html'
            }),
            new CopyWebpackPlugin({
                patterns: [
                    {
                        from: './src/favicon',
                        to: './'
                    }
                ]
            }),
            new Dotenv({
                path: './.env'
            }),
            new workboxPlugin.InjectManifest({
                swSrc: './src/sw.js',
                swDest: 'sw.js',
                maximumFileSizeToCacheInBytes: 5242880
            })
            // new BundleAnalyzerPlugin()
        ]
    }
}
