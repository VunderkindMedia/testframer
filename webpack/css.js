module.exports = function (paths) {
    return {
        module: {
            rules: [
                {
                    test: /\.module\.css$/,
                    include: paths,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true,
                                modules: {
                                    exportLocalsConvention: 'camelCase',
                                    localIdentName: '[name]_[local]_[hash:base64:5]'
                                }
                            }
                        }
                    ]
                },
                {
                    test: /\.css$/,
                    include: paths,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ],
                    exclude: /\.module\.css$/
                }
            ]
        }
    }
}
