const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

module.exports = function () {
    return {
        mode: 'production',
        optimization: {
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        output: {
                            comments: false
                        }
                    }
                })
            ]
        },
        module: {
            rules: [
                {
                    test: /\.(png|jpg|gif|svg)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                outputPath: 'images/'
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff2)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                outputPath: 'fonts/'
                            }
                        }
                    ]
                },
                {
                    test: /\.mp4$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                outputPath: 'video/'
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [new CleanWebpackPlugin(), new ForkTsCheckerWebpackPlugin()]
    }
}
