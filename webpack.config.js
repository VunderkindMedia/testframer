const {merge} = require('webpack-merge')
const desktop = require('./webpack/desktop')
const mobile = require('./webpack/mobile')
const common = require('./webpack/common')
const devServer = require('./webpack/dev-server')
const css = require('./webpack/css')
const extractCSS = require('./webpack/css.extract')
const prod = require('./webpack/prod')
const dev = require('./webpack/dev')

module.exports = function (env) {
    const build = env && env.mobile ? mobile() : desktop()
    if (env && env.prod) {
        return merge([build, common(), prod(), extractCSS()])
    }
    return merge([build, common(), dev(), devServer(), css()])
}
