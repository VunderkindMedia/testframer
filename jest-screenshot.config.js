module.exports = {
    preset: 'jest-puppeteer',
    roots: [`./src/screenshot-tests/${process.env.MOBILE ? 'mobile' : 'desktop'}`],
    transform: {
        '^.+\\.tsx?$': 'ts-jest'
    }
}
