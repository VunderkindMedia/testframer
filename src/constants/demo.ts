import {ListItem} from '../model/list-item'
import {FactoryTypes} from '../model/framer/factory-types'

export enum DemoUsers {
    Perm = 'demo',
    Moscow = 'demo_msk'
}

interface IDemoAccess {
    login: string
    password: string
}

interface IDemoTrackingInfo {
    id: string
    factory: string
}

export const DEMO_TRACKING_INFO: {[key in DemoUsers]: IDemoTrackingInfo} = {
    demo: {id: 'perm', factory: 'Пермь'},
    // eslint-disable-next-line @typescript-eslint/naming-convention
    demo_msk: {id: 'msk', factory: 'Электросталь'}
}

export const DEMO_ACCESS: {[key in FactoryTypes]: IDemoAccess} = {
    '3': {login: DemoUsers.Perm, password: DemoUsers.Perm},
    '4': {login: DemoUsers.Moscow, password: DemoUsers.Moscow}
}

export const DEMO_FACTORIES = [
    new ListItem(String(FactoryTypes.Perm), 'Амега, г. Пермь', FactoryTypes.Perm),
    new ListItem(String(FactoryTypes.Moscow), 'Амега, г. Электросталь', FactoryTypes.Moscow)
]

export const LP_ACCOUNT: IDemoAccess = {login: 'landing ', password: 'Xxh8zLMQ'}
