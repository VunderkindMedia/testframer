import {ListItem} from '../model/list-item'

const GLASS_TYPES = [
    ['4', '4мм'],
    ['4 i', '4 i'],
    ['4 Ц', '4 Ц'],
    ['4 Т', '4 Т'],
    ['4зак', '4мм закаленное'],
    ['6', '6мм'],
    ['6 i', '6 i'],
    ['6 Ц', '6 Ц'],
    ['6 Т', '6 Т'],
    ['6зак', '6мм закаленное'],
    ['8мм Triplex', '8мм Triplex']
]
const FRAME_WIDTH = [
    ['10', '10мм'],
    ['12', '12мм'],
    ['14', '14мм'],
    ['16', '16мм']
]
const TINT_FILM_TYPES = [
    ['-', 'Не выбрано'],
    ['A1', 'Бесцветная'],
    ['A2', 'Бесцветная'],
    ['A3', 'Бесцветная'],
    ['S CL SR PS 11', 'Бесцветная'],
    ['S CL SR PS 7', 'Бесцветная'],
    ['mil 4', 'mil 4'],
    ['R 15 B SR CDF', 'Бронза с зеркальным эффектом'],
    ['R 20 SR CDF', 'Пленка R 20 SR CDF Silver'],
    ['STR 20 SI SR PS', 'Серебряный темный']
]

export const GLASS_TYPES_ITEMS = GLASS_TYPES.map(([key, value]) => new ListItem(key, value, key))
export const FRAME_WIDTH_ITEMS = FRAME_WIDTH.map(([key, value]) => new ListItem(key, value, key))
export const TINT_FILM_ITEMS = TINT_FILM_TYPES.map(([key, value]) => {
    return key !== '-' ? new ListItem(key, `${key} (${value})`, key) : new ListItem(key, value, key)
})

export const ENERGY_FILLING_IDS = [41, 98, 13, 15, 43]
