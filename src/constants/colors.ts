export enum Colors {
    Blue = '#0075FF',
    Red = '#DA4052',
    Orange = '#FFA620',
    Green = '#22B56E',
    Gray = '#C4C4C4',
    LightGray = '#505565',
    White = '#fff',
    Bg = '#F2F2F2',
    Black = '#515257',
    Normal = '#C3C3C3',
    Active = '#30C2FF',
    Selected = '#E3F7FD',
    LightGrayCard = '#9A9A9A',
    GreenCard = '#27AE60',
    BlueCard = '#30C2FF'
}
