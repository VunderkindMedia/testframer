export const NAV_ROOT = '/'
export const NAV_LOGIN = '/login'
export const NAV_PROFILE = '/profile'
export const NAV_ORDERS = '/orders'
export const NAV_ORDER = '/order'
export const NAV_BOOKING = '/booking'
export const NAV_CLIENTS = '/clients'
export const NAV_CLIENT = '/client'
export const NAV_PAYMENT = '/payment'
export const NAV_PAYMENT_RESULT = '/paymentresult'
export const NAV_KP = '/kp'
export const NAV_BILL = '/bill'
export const NAV_START_TUTORIAL = '/start_tutorial'
export const NAV_PARTNER_GOODS = '/partner_goods'
export const NAV_RESET_PASSWORD = '/reset-password'
export const NAV_FORGOT_PASSWORD = '/forgot-password'
export const NAV_REGISTRATION = '/register'
export const NAV_PARTNER_GOODS_REPORT = '/partner_goods_report'
export const NAV_EVENTS = '/events'
export const NAV_EVENTS_REPORT = '/events_report'
export const NAV_MOUNTINGS = '/mountings'
export const NAV_MOUNTING = '/mounting'
export const NAV_MOUNTING_REPORT = '/mounting_report'
export const NAV_BUILDER = '/builder'
export const NAV_ENERGY_PROGRAM = '/energy_program'
export const NAV_ENERGY_PRODUCTS = '/energy_products'

export const ORDER_TAB_ID = 'order'
