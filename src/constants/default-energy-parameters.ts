export const ENERGY_CATEGORY = 'energy'
export const DEFAULT_ENERGY_PROFILE_ID = 41
export const DEFAULT_ENERGY_FILLING = '4 Ц-10-4-10-4 i'
export const DEFAULT_ENERGY_DOOR_FILLING = '4-16-4 i'
export const DEFAULT_ENERGY_GLASS_COLOR = 'ClimaGuard Solar (бесцветное)'
