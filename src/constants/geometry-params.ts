export const BEAD_WIDTH = 10
export const DEFAULT_B = 28
export const POROG_B = 8
export const ALUMINIUM_B = 4

export const MIN_WIDTH = 100
export const MIN_HEIGHT = 100

export const MAX_WIDTH = 2800
export const MAX_HEIGHT = 2500

export const MAX_BALCON_DOOR_WIDTH = 2500
export const MAX_BALCON_DOOR_HEIGHT = 2800

export const MAX_ALUMINIUM_WIDTH = 5950
export const MAX_ALUMINIUM_HEIGHT = 2500

export const APPROXIMATE_BEAM_WIDTH = 70

export const MIN_CONNECTOR_HEIGHT = 100
