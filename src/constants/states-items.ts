import {ListItem} from '../model/list-item'
import {AllOrderStates, DraftOrderStates, OrderStates, ProducingOrderStates} from '../model/framer/order-states'

export const STATES_ITEMS: ListItem<OrderStates[]>[] = [
    new ListItem('Все', 'Все', AllOrderStates),
    new ListItem('Расчеты', 'Расчеты', DraftOrderStates),
    new ListItem('В производстве', 'В производстве', ProducingOrderStates),
    new ListItem('Завершены', 'Завершены', [OrderStates.Completed]),
    new ListItem('Отменены', 'Отменены', [OrderStates.Cancelled])
]
