import {IFurnitureJSON} from '../model/framer/furniture'

export const FURNITURE_MACO: IFurnitureJSON = {
    id: 27,
    name: 'Фурнитура Maco MM',
    displayName: 'Maco MM',
    isDefault: false,
    isEmpty: false,
    vars: '145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701'
}

export const FURNITURE_ROTO: IFurnitureJSON = {
    id: 35,
    name: 'Фурнитура Roto NT',
    displayName: 'Roto NT',
    isDefault: false,
    isEmpty: false,
    vars: '140,280;190,481;283,601;433,801;533,1001;583,1201;1020,1801'
}
