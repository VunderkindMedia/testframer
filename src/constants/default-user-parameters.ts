import {UserParameter} from '../model/framer/user-parameter/user-parameter'
import {UserParameterTypes} from '../model/framer/user-parameter/user-parameter-types'

export const DEFAULT_GLASS_COLOR = new UserParameter(UserParameterTypes.GlassColor, 'Нет', '', '')

export const INSIDE_FILLING_COLOR_ID = 11
export const OUTSIDE_FILLING_COLOR_ID = 10
export const INSIDE_GALVANIZED_COLOR_ID = 1490
export const OUTSIDE_GALVANIZED_COLOR_ID = 1488
