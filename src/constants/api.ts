import {AffiliateMarginTypes} from '../model/affiliate-margin-types'
import {ModelParts} from '../model/framer/model-parts'

export const SERVER = process.env.SERVER
export const DOCS_SERVER = 'https://strapi.framer.ru'

export const API_LOGIN = `${SERVER}/login`
export const API_REFRESH = `${SERVER}/refresh`

export const API_GET_PROFILES_AND_FURNITURE = (constructionType: number, productType: number): string =>
    `${SERVER}/v1/constrtypes/${constructionType}/prodtypes/${productType}/systems`

export const API_GET_PROFILE = (profileId: number): string => `${SERVER}/v1/profiles/${profileId}`

export const API_GET_FILLINGS = (profileId: number, limit = 500): string => `${SERVER}/v1/profiles/${profileId}/fillings?limit=${limit}`

export const API_CLIENTS = `${SERVER}/v1/clients`
export const API_CLIENT = (id: string): string => `${SERVER}/v1/clients/${id}`
export const API_CLIENT_ORDERS = (id: string): string => `${SERVER}/v1/clients/${id}/orders`

export const API_ORDERS = `${SERVER}/v1/orders`
export const API_ORDERS_SAVE = (id: number): string => `${API_ORDERS}/${id}/save`
export const API_ORDERS_DUPLICATE = (id: number): string => `${SERVER}/v1/orders/${id}/duplicate`
export const API_ORDERS_WITH_PARTNER_GOODS = `${SERVER}/v1/reports/goods`
export const API_GET_ORDER = (id: number): string => `${API_ORDERS}/${id}`
export const API_ORDERS_BOOKING_OPTIONS = (id: number): string => `${API_ORDERS}/${id}/bookingoptions`
export const API_ORDERS_BOOKING_DATES = (id: number): string => `${API_ORDERS}/${id}/bookingdates`
export const API_ORDERS_BOOK = (id: number): string => `${API_ORDERS}/${id}/book`
export const API_ORDERS_UNBOOK = (id: number): string => `${API_ORDERS}/${id}/unbook`
export const API_SHIPPING_INFO = (id: number): string => `${API_ORDERS}/${id}/shippinginfo`

export const API_CHECKOUT = `${SERVER}/v1/checkout`
export const API_RESERVE = `${API_CHECKOUT}/reserve`
export const API_DEPOSIT = `${API_CHECKOUT}/deposit`
export const API_BILL = (sum: number): string => `${API_CHECKOUT}/bill?sum=${sum}`
export const API_PAYMENTS = `${API_CHECKOUT}/operations`

export const API_ACCOUNT = `${SERVER}/v1/profile/account`
export const API_PROFILE_SETTINGS = `${SERVER}/v1/profile/settings`
export const API_DEALERS = `${SERVER}/dealers`
export const API_DEALERS_SAVE = `${SERVER}/v1/dealers`
export const API_INN = `${SERVER}/v1/dealers/innlookup`
export const API_SELECT_DEALER = (dealerId: number): string => `${SERVER}/v1/profile/selectdealer/${dealerId}`
export const API_OFFER_FIELDS = `${SERVER}/v1/profile/offerfields`
export const API_PROFILE_NEWS = `${SERVER}/v1/profile/news/id`
export const API_REGIONS = `${SERVER}/regions`
export const API_REGISTER = `${SERVER}/register`
export const API_FORGOT_PASSWORD = `${SERVER}/forgotpassword`
export const API_RESET_PASSWORD = `${SERVER}/resetpassword`

export const API_SERVICES = `${SERVER}/v1/profile/services`
export const API_MARGIN = `${SERVER}/v1/profile/margin`
export const API_GOODS_MARGIN = `${SERVER}/v1/profile/goodsmargin`

export const API_GET_LAMINATION = `${SERVER}/v1/colors/lamination`

export const API_SHIPPING_CALC = `${SERVER}/v1/shipping/calc`

export const API_GOOD_GROUPS = `${SERVER}/v1/goodgroups`
export const API_GOODS = `${SERVER}/v1/goods`

export const API_PARTNER_GOODS = `${SERVER}/v1/partnergoods`
export const API_PARTNER_GOOD_GROUPS = `${SERVER}/v1/partnergoodgroups`
export const API_PARTNER_GOODS_TEMPLATES = `${API_PARTNER_GOODS}/templates`

export const API_INSTALLATIONS = `${SERVER}/v1/installations/catalog`
export const API_CONSTRUCTION = (id: number): string => `${SERVER}/v1/installations/catalog/${id}/construction`

export const API_SERVICES_2 = `${SERVER}/v1/services`

export const API_AFFILIATES = `${SERVER}/v1/filials`
export const API_AFFILIATES_MARGIN = (id: number, type: AffiliateMarginTypes): string => `${SERVER}/v1/filials/${id}/${type}`
export const API_EMPLOYEES = `${SERVER}/v1/users`

export const API_CALENDAR = `${SERVER}/v1/calendar`

export const API_MOUNTINGS = `${SERVER}/v1/mountings`
export const API_MOUNTING = (id: number): string => `${SERVER}/v1/mountings/${id}`

export const API_DOCS_AUTH = `${DOCS_SERVER}/auth/local`
export const API_DOCS_NEWS = `${DOCS_SERVER}/what-s-news?_sort=published_at:DESC`

export const API_APP_VERSION = `${SERVER}/v1/version`

export const API_MODEL_PARAMS = (modelPart: ModelParts, productOrLeafGuid: string): string =>
    `${SERVER}/v1/modelparams/get?modelPart=${modelPart}&guid=${productOrLeafGuid}`
export const API_VISIBLE_MODEL_PARAMS = (cityId: number): string => `${SERVER}/v1/modelparams/getvisible?cityId=${cityId}`

export const API_NOTIFICATIONS = `${SERVER}/v1/notifications`
export const API_NOTIFICATIONS_READ_ALL = `${SERVER}/v1/notifications/readall`
export const API_NOTIFICATIONS_READ = (id: string): string => `${SERVER}/v1/notifications/${id}/read`
export const API_NOTIFICATIONS_UNREAD = (id: string): string => `${SERVER}/v1/notifications/${id}/unread`
