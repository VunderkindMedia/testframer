import {OpeningSystem} from '../model/opening-system'
import {OpenTypes} from '../model/framer/open-types'
import {OpenSides} from '../model/framer/open-sides'
import {ShtulpOpenTypes} from '../model/framer/shtulp-open-types'
import op_1_1_0 from './resources/opening-systems/op_1_1_0.svg'
import op_1_1_0_big from './resources/opening-systems/op_1_1_0_big.svg'
import op_1_0_0 from './resources/opening-systems/op_1_0_0.svg'
import op_1_0_0_big from './resources/opening-systems/op_1_0_0_big.svg'
import op_3_3_0 from './resources/opening-systems/op_3_3_0.svg'
import op_3_3_0_big from './resources/opening-systems/op_3_3_0_big.svg'
import op_2_1_0 from './resources/opening-systems/op_2_1_0.svg'
import op_2_1_0_big from './resources/opening-systems/op_2_1_0_big.svg'
import op_2_0_0 from './resources/opening-systems/op_2_0_0.svg'
import op_2_0_0_big from './resources/opening-systems/op_2_0_0_big.svg'
import op_1_1_1 from './resources/opening-systems/op_1_1_1.svg'
import op_1_1_1_big from './resources/opening-systems/op_1_1_1_big.svg'
import op_1_0_1 from './resources/opening-systems/op_1_0_1.svg'
import op_1_0_1_big from './resources/opening-systems/op_1_0_1_big.svg'
import op_0_4_0 from './resources/opening-systems/op_0_4_0.svg'
import op_0_4_0_big from './resources/opening-systems/op_0_4_0_big.svg'

export const RIGHT_OPENING_SYSTEM = new OpeningSystem(op_1_1_0, op_1_1_0_big, 'Поворотное правое', OpenTypes.Povorotnaya, OpenSides.ToRight)
export const LEFT_OPENING_SYSTEM = new OpeningSystem(op_1_0_0, op_1_0_0_big, 'Поворотное левое', OpenTypes.Povorotnaya, OpenSides.ToLeft)

export const DOWN_OPENING_SYSTEM = new OpeningSystem(op_3_3_0, op_3_3_0_big, 'Фрамужное', OpenTypes.Otkidnaya, OpenSides.ToBottom)

export const RIGHT_DOWN_OPENING_SYSTEM = new OpeningSystem(
    op_2_1_0,
    op_2_1_0_big,
    'Поворотно-откидное правое',
    OpenTypes.PovorotnoOtkidnaya,
    OpenSides.ToRight
)

export const LEFT_DOWN_OPENING_SYSTEM = new OpeningSystem(
    op_2_0_0,
    op_2_0_0_big,
    'Поворотно-откидное левое',
    OpenTypes.PovorotnoOtkidnaya,
    OpenSides.ToLeft
)

export const RIGHT_SHTULP_OPENING_SYSTEM = new OpeningSystem(
    op_1_1_1,
    op_1_1_1_big,
    'Поворотное правое (штульп)',
    OpenTypes.Povorotnaya,
    OpenSides.ToRight,
    ShtulpOpenTypes.ShtulpOnLeaf
)

export const LEFT_SHTULP_OPENING_SYSTEM = new OpeningSystem(
    op_1_0_1,
    op_1_0_1_big,
    'Поворотное левое (штульп)',
    OpenTypes.Povorotnaya,
    OpenSides.ToLeft,
    ShtulpOpenTypes.ShtulpOnLeaf
)

export const EMPTY_OPENING_SYSTEM = new OpeningSystem(op_0_4_0, op_0_4_0_big, 'Глухое', OpenTypes.Gluhoe, OpenSides.NoOpening)

export const OPENING_SYSTEMS: OpeningSystem[] = [
    RIGHT_OPENING_SYSTEM,
    LEFT_OPENING_SYSTEM,
    DOWN_OPENING_SYSTEM,
    RIGHT_DOWN_OPENING_SYSTEM,
    LEFT_DOWN_OPENING_SYSTEM,
    RIGHT_SHTULP_OPENING_SYSTEM,
    LEFT_SHTULP_OPENING_SYSTEM,
    EMPTY_OPENING_SYSTEM
]
