/* eslint-disable */

import {ConnectorTypes} from '../model/framer/connector-types'
import {IConnectorParametersJSON} from '../model/framer/connector-parameters'

export const TEST_PROFILE_TEMPLATE = {
    id: 18,
    name: 'Plaswin',
    thickness: 58,
    class: '',
    brand: 'Plaswin',
    camernost: 3,
    allowedThickness: [0, 4, 5, 6, 24, 32],
    parts: [
        {
            id: 343,
            marking: 'PW-0707 PW',
            modelPart: 1,
            a: 63,
            b: null,
            c: 43,
            d: null,
            e: null,
            f: null,
            g: null,
            comment: 'Рама 63 мм'
        },
        {
            id: 344,
            marking: 'PW-0317 PW',
            modelPart: 2,
            a: 76.5,
            b: 0,
            c: 56.5,
            d: null,
            e: 20,
            f: null,
            g: null,
            comment: 'Створка 77 мм'
        },
        {
            id: 345,
            marking: 'PW-0132 PW',
            modelPart: 3,
            a: 41,
            b: null,
            c: 21,
            d: null,
            e: null,
            f: null,
            g: null,
            comment: 'Импост 82 мм'
        },
        {
            id: 349,
            marking: 'PW 20104',
            modelPart: 4,
            a: 2.5,
            b: null,
            c: null,
            d: null,
            e: null,
            f: null,
            g: null,
            comment: 'Соединитель Н-образный'
        }
    ],
    isDefault: true,
    isMoskitka: false,
    isEmpty: false
}

export const TEST_EMPTY_PROFILE_TEMPLATE = {
    id: 2,
    name: 'Без профиля',
    thickness: 0,
    class: '',
    brand: '',
    camernost: 0,
    allowedThickness: [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
        36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56
    ],
    parts: [
        {
            id: 1,
            marking: 'Без рамы',
            modelPart: 1,
            a: 0,
            b: 0,
            c: 0,
            d: 0,
            e: 0,
            f: 0,
            g: 0,
            comment: ''
        },
        {
            id: 2,
            marking: 'Без створки',
            modelPart: 2,
            a: 0,
            b: 0,
            c: 0,
            d: 0,
            e: 0,
            f: 0,
            g: 0,
            comment: ''
        },
        {
            id: 3,
            marking: 'Без импоста',
            modelPart: 3,
            a: 0,
            b: 0,
            c: 0,
            d: 0,
            e: 0,
            f: 0,
            g: 0,
            comment: ''
        },
        {
            id: 145,
            marking: 'Без соединителя',
            modelPart: 4,
            a: null,
            b: null,
            c: null,
            d: null,
            e: null,
            f: null,
            g: null,
            comment: ''
        }
    ],
    isDefault: false,
    isMoskitka: false,
    isEmpty: true
}

export const TEST_MOSKITKA_PROFILE_TEMPLATE = {
    id: 3,
    name: 'Москитные сетки',
    thickness: 0,
    class: '',
    brand: '',
    camernost: 0,
    allowedThickness: [0],
    parts: [
        {
            id: 28,
            marking: 'SKS 6211',
            modelPart: 1,
            a: 25,
            b: null,
            c: 25,
            d: null,
            e: null,
            f: -60,
            g: null,
            comment: 'Профиль МС окна'
        },
        {
            id: 10,
            marking: 'Без створки',
            modelPart: 2,
            a: 0,
            b: 0,
            c: 0,
            d: 0,
            e: 0,
            f: 0,
            g: 0,
            comment: ''
        },
        {
            id: 4,
            marking: 'SKS 6212',
            modelPart: 3,
            a: 12.5,
            b: 0,
            c: 12.5,
            d: 0,
            e: null,
            f: null,
            g: null,
            comment: 'Поперечена стандартной МС'
        },
        {
            id: 771,
            marking: 'Фетр МС',
            modelPart: 4,
            a: 1,
            b: null,
            c: null,
            d: null,
            e: null,
            f: null,
            g: null,
            comment: ''
        }
    ],
    isDefault: false,
    isMoskitka: true,
    isEmpty: false
}

export const TEST_FURNITURE_TEMPLATE = {
    id: 33,
    name: 'Фурнитура Axor',
    displayName: 'Axor',
    isDefault: true,
    isEmpty: false
}

export const TEST_CONNECTORS_PARAMETERS_TEMPLATES: IConnectorParametersJSON[] = [
    {
        id: 351,
        marking: '144',
        modelPart: 4,
        a: 30,
        b: -1,
        c: -1,
        d: -1,
        e: -1,
        f: -1,
        g: -1,
        comment: 'Расширитель 30',
        connectorType: ConnectorTypes.Extender,
        isDefault: false
    },
    {
        id: 644,
        marking: '147',
        modelPart: 4,
        a: 120,
        b: -1,
        c: -1,
        d: -1,
        e: -1,
        f: -1,
        g: -1,
        comment: 'Расширитель 120 мм',
        connectorType: ConnectorTypes.Extender,
        isDefault: false
    },
    {
        id: 856,
        marking: '150',
        modelPart: 4,
        a: 5,
        b: -1,
        c: -1,
        d: -1,
        e: -1,
        f: -1,
        g: -1,
        comment: 'Соединитель с Н-образный',
        connectorType: ConnectorTypes.Connector,
        isDefault: false
    },
    {
        id: 353,
        marking: '152',
        modelPart: 4,
        a: 20,
        b: -1,
        c: -1,
        d: -1,
        e: -1,
        f: -1,
        g: -1,
        comment: 'Соединитель с Н-образный с армированием',
        connectorType: ConnectorTypes.Connector,
        isDefault: false
    },
    {
        id: 350,
        marking: '155',
        modelPart: 4,
        a: 149,
        b: -1,
        c: -1,
        d: -1,
        e: -1,
        f: -1,
        g: -1,
        comment: 'Соединитель угловой 90 градусов',
        connectorType: ConnectorTypes.Connector,
        isDefault: false
    },
    {
        id: 354,
        marking: '540+541',
        modelPart: 4,
        a: 182,
        b: -1,
        c: -1,
        d: -1,
        e: -1,
        f: -1,
        g: -1,
        comment: 'Соединитель с переменным углом',
        connectorType: ConnectorTypes.Connector,
        isDefault: false
    },
    {
        id: 417,
        marking: '545',
        modelPart: 4,
        a: 45,
        b: -1,
        c: -1,
        d: -1,
        e: -1,
        f: -1,
        g: -1,
        comment: 'Расширитель 45 мм',
        connectorType: ConnectorTypes.Extender,
        isDefault: false
    },
    {
        id: 352,
        marking: '546',
        modelPart: 4,
        a: 60,
        b: -1,
        c: -1,
        d: -1,
        e: -1,
        f: -1,
        g: -1,
        comment: 'Расширитель 60 мм',
        connectorType: ConnectorTypes.Extender,
        isDefault: false
    },
    {
        id: 349,
        marking: 'PW 20104',
        modelPart: 4,
        a: 2.5,
        b: -1,
        c: -1,
        d: -1,
        e: -1,
        f: -1,
        g: -1,
        comment: 'Соединитель Н-образный',
        connectorType: ConnectorTypes.Connector,
        isDefault: false
    }
]
