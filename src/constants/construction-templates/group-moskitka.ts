import {IConstructionJSON} from '../../model/framer/construction'
import {MoskitkaColors} from '../../model/framer/moskitka-colors'

export const PMS_OK: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 600,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1330
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1330
                        },
                        end: {
                            x: 600,
                            y: 1330
                        }
                    },
                    {
                        start: {
                            x: 600,
                            y: 1330
                        },
                        end: {
                            x: 600,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 600,
                                        y: 0
                                    },
                                    end: {
                                        x: 0,
                                        y: 0
                                    }
                                },
                                {
                                    start: {
                                        x: 0,
                                        y: 0
                                    },
                                    end: {
                                        x: 0,
                                        y: 1330
                                    }
                                },
                                {
                                    start: {
                                        x: 0,
                                        y: 1330
                                    },
                                    end: {
                                        x: 600,
                                        y: 1330
                                    }
                                },
                                {
                                    start: {
                                        x: 600,
                                        y: 1330
                                    },
                                    end: {
                                        x: 600,
                                        y: 0
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: 'фибергласс',
                                        fillingPoint: {
                                            x: 300,
                                            y: 665
                                        }
                                    }
                                }
                            ],
                            openType: 0,
                            openSide: 0,
                            fillingPoint: {
                                x: 300,
                                y: 665
                            },
                            moskitka: {
                                set: true,
                                color: MoskitkaColors.White,
                                catproof: false
                            }
                        }
                    }
                ],
                openSide: 0,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 8,
            productionTypeId: 1191
        }
    ]
}

export const PMS_BD: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 800,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2000
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 2000
                        },
                        end: {
                            x: 800,
                            y: 2000
                        }
                    },
                    {
                        start: {
                            x: 800,
                            y: 2000
                        },
                        end: {
                            x: 800,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 800,
                                        y: 0
                                    },
                                    end: {
                                        x: 0,
                                        y: 0
                                    }
                                },
                                {
                                    start: {
                                        x: 0,
                                        y: 0
                                    },
                                    end: {
                                        x: 0,
                                        y: 2000
                                    }
                                },
                                {
                                    start: {
                                        x: 0,
                                        y: 2000
                                    },
                                    end: {
                                        x: 800,
                                        y: 2000
                                    }
                                },
                                {
                                    start: {
                                        x: 800,
                                        y: 2000
                                    },
                                    end: {
                                        x: 800,
                                        y: 0
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 0,
                                        y: 1332
                                    },
                                    end: {
                                        x: 800,
                                        y: 1332
                                    }
                                },
                                {
                                    start: {
                                        x: 0,
                                        y: 666
                                    },
                                    end: {
                                        x: 800,
                                        y: 666
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: 'фибергласс',
                                        fillingPoint: {
                                            x: 400,
                                            y: 1666
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'фибергласс',
                                        fillingPoint: {
                                            x: 400,
                                            y: 999
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'фибергласс',
                                        fillingPoint: {
                                            x: 400,
                                            y: 333
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 0,
                            moskitka: {
                                set: true,
                                color: MoskitkaColors.White,
                                catproof: false
                            },
                            fillingPoint: {
                                x: 400,
                                y: 1000
                            }
                        }
                    }
                ],
                openSide: 0,
                openType: 0,
                fillingPoint: {
                    x: 400,
                    y: 1000
                }
            },
            constructionTypeId: 10,
            productionTypeId: 1191
        }
    ]
}

export const PMS_OKX2: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: 'aabf06fe-7a6c-83cf-7701-5fd529b2fd1e'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 650
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 650
                        },
                        end: {
                            x: 650,
                            y: 650
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 650
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 650,
                                        y: 0
                                    },
                                    end: {
                                        x: 0,
                                        y: 0
                                    }
                                },
                                {
                                    start: {
                                        x: 0,
                                        y: 0
                                    },
                                    end: {
                                        x: 0,
                                        y: 650
                                    }
                                },
                                {
                                    start: {
                                        x: 0,
                                        y: 650
                                    },
                                    end: {
                                        x: 650,
                                        y: 650
                                    }
                                },
                                {
                                    start: {
                                        x: 650,
                                        y: 650
                                    },
                                    end: {
                                        x: 650,
                                        y: 0
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: 'фибергласс',
                                        fillingPoint: {
                                            x: 325,
                                            y: 325
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 0,
                            moskitka: {
                                set: true,
                                color: MoskitkaColors.White,
                                catproof: false
                            },
                            fillingPoint: {
                                x: 325,
                                y: 325
                            }
                        }
                    }
                ],
                openSide: 0,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 325
                }
            },
            connectors: [
                {
                    start: {
                        x: 0,
                        y: 0
                    },
                    end: {
                        x: 650,
                        y: 0
                    },
                    profileVendorCode: 'Фетр МС',
                    connectedProductGuid: '5cc5eef7-75fd-1501-5c88-d0ab204275a9',
                    connectedBeamGuid: 'aabf06fe-7a6c-83cf-7701-5fd529b2fd1e'
                }
            ],
            productOffset: 0,
            constructionTypeId: 8,
            productionTypeId: 1191
        },
        {
            productGuid: '5cc5eef7-75fd-1501-5c88-d0ab204275a9',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 650
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 650
                        },
                        end: {
                            x: 650,
                            y: 650
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 650
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 650,
                                        y: 0
                                    },
                                    end: {
                                        x: 0,
                                        y: 0
                                    }
                                },
                                {
                                    start: {
                                        x: 0,
                                        y: 0
                                    },
                                    end: {
                                        x: 0,
                                        y: 650
                                    }
                                },
                                {
                                    start: {
                                        x: 0,
                                        y: 650
                                    },
                                    end: {
                                        x: 650,
                                        y: 650
                                    }
                                },
                                {
                                    start: {
                                        x: 650,
                                        y: 650
                                    },
                                    end: {
                                        x: 650,
                                        y: 0
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: 'фибергласс',
                                        fillingPoint: {
                                            x: 325,
                                            y: 325
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 0,
                            moskitka: {
                                set: true,
                                color: MoskitkaColors.White,
                                catproof: false
                            },
                            fillingPoint: {
                                x: 325,
                                y: 325
                            }
                        }
                    }
                ],
                openSide: 0,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 325
                }
            },
            constructionTypeId: 8,
            productionTypeId: 1191
        }
    ]
}
