import {IConstructionJSON} from '../../model/framer/construction'

export const B_DV_1: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 615,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 615,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 700
                                    },
                                    end: {
                                        x: 615,
                                        y: 700
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 325,
                                            y: 1432.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 325,
                                            y: 417.5
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 325,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 1050
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 1184
        }
    ]
}

export const B_DV_1_1: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 615,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 615,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 700
                                    },
                                    end: {
                                        x: 615,
                                        y: 700
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 325,
                                            y: 1382.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 325,
                                            y: 367.5
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 325,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 1050
                }
            },
            connectors: [],
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 1184
        }
    ]
}

export const B_DV_4: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 1500,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 2100
                        },
                        end: {
                            x: 1500,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 800
                        },
                        end: {
                            x: 750,
                            y: 800
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 375,
                                y: 1450
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 375,
                                y: 400
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1450,
                                        y: 50
                                    },
                                    end: {
                                        x: 765,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 765,
                                        y: 50
                                    },
                                    end: {
                                        x: 765,
                                        y: 2050
                                    }
                                },
                                {
                                    start: {
                                        x: 765,
                                        y: 2050
                                    },
                                    end: {
                                        x: 1450,
                                        y: 2050
                                    }
                                },
                                {
                                    start: {
                                        x: 1450,
                                        y: 2050
                                    },
                                    end: {
                                        x: 1450,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 765,
                                        y: 800
                                    },
                                    end: {
                                        x: 1450,
                                        y: 800
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1107.5,
                                            y: 1425
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1107.5,
                                            y: 425
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 1107.5,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 1050
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 1184
        }
    ]
}

export const B_DV_5: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 1500,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 2100
                        },
                        end: {
                            x: 1500,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 2100
                        }
                    },
                    {
                        start: {
                            x: 750,
                            y: 800
                        },
                        end: {
                            x: 1500,
                            y: 800
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 737,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 2065
                                    },
                                    end: {
                                        x: 737,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 737,
                                        y: 2065
                                    },
                                    end: {
                                        x: 737,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 800
                                    },
                                    end: {
                                        x: 737,
                                        y: 800
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 386,
                                            y: 1432.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 386,
                                            y: 417.5
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 386,
                                y: 1050
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1125,
                                y: 1450
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1125,
                                y: 400
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 1050
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 1184
        }
    ]
}

export const B_DV_6: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {
                            x: 1450,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 1450,
                            y: 2100
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 1450,
                            y: 2100
                        },
                        end: {
                            x: 1450,
                            y: 0
                        },
                        modelPart: 1
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 600,
                            y: 0
                        },
                        end: {
                            x: 600,
                            y: 2100
                        },
                        modelPart: 6,
                        shtulpType: 0
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            shtulpOpenType: 1,
                            frameBeams: [
                                {
                                    start: {
                                        x: 600,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 2065
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 2065
                                    },
                                    end: {
                                        x: 600,
                                        y: 2065
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 600,
                                        y: 2065
                                    },
                                    end: {
                                        x: 600,
                                        y: 35
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 735
                                    },
                                    end: {
                                        x: 600,
                                        y: 735
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 317.5,
                                            y: 1400
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 317.5,
                                            y: 385
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 317.5,
                                y: 1050
                            }
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 2,
                            frameBeams: [
                                {
                                    start: {
                                        x: 1415,
                                        y: 35
                                    },
                                    end: {
                                        x: 600,
                                        y: 35
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 600,
                                        y: 35
                                    },
                                    end: {
                                        x: 600,
                                        y: 2065
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 600,
                                        y: 2065
                                    },
                                    end: {
                                        x: 1415,
                                        y: 2065
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 1415,
                                        y: 2065
                                    },
                                    end: {
                                        x: 1415,
                                        y: 35
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 600,
                                        y: 735
                                    },
                                    end: {
                                        x: 1415,
                                        y: 735
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1007.5,
                                            y: 1400
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 1007.5,
                                            y: 385
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1007.5,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 725,
                    y: 1050
                }
            },
            connectors: [],
            productOffset: 0,
            constructionTypeId: 3,
            productionTypeId: 1184
        }
    ]
}

export const B_DV_7: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {
                            x: 1450,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 1450,
                            y: 2100
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 1450,
                            y: 2100
                        },
                        end: {
                            x: 1450,
                            y: 0
                        },
                        modelPart: 1
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 850,
                            y: 0
                        },
                        end: {
                            x: 850,
                            y: 2100
                        },
                        modelPart: 6,
                        shtulpType: 1
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            shtulpOpenType: 2,
                            frameBeams: [
                                {
                                    start: {
                                        x: 850,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 2065
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 2065
                                    },
                                    end: {
                                        x: 850,
                                        y: 2065
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 850,
                                        y: 2065
                                    },
                                    end: {
                                        x: 850,
                                        y: 35
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 735
                                    },
                                    end: {
                                        x: 850,
                                        y: 735
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 442.5,
                                            y: 1400
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 442.5,
                                            y: 385
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 442.5,
                                y: 1050
                            }
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 1,
                            frameBeams: [
                                {
                                    start: {
                                        x: 1415,
                                        y: 35
                                    },
                                    end: {
                                        x: 850,
                                        y: 35
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 850,
                                        y: 35
                                    },
                                    end: {
                                        x: 850,
                                        y: 2065
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 850,
                                        y: 2065
                                    },
                                    end: {
                                        x: 1415,
                                        y: 2065
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 1415,
                                        y: 2065
                                    },
                                    end: {
                                        x: 1415,
                                        y: 35
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 850,
                                        y: 735
                                    },
                                    end: {
                                        x: 1415,
                                        y: 735
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1132.5,
                                            y: 1400
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 1132.5,
                                            y: 385
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1132.5,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 725,
                    y: 1050
                }
            },
            connectors: [],
            productOffset: 0,
            constructionTypeId: 3,
            productionTypeId: 1184
        }
    ]
}
