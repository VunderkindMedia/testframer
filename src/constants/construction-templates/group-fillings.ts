import {IConstructionJSON} from '../../model/framer/construction'

export const OC_1: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-24-4',
                            fillingPoint: {
                                x: 450,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 700
                }
            },
            productOffset: 0,
            constructionTypeId: 9,
            productionTypeId: 106
        }
    ]
}

export const OC_2: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1000
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1000
                        },
                        end: {
                            x: 900,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-24-4',
                            fillingPoint: {
                                x: 450,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 700
                }
            },
            productOffset: 0,
            constructionTypeId: 9,
            productionTypeId: 106
        }
    ]
}

export const OC_3: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 1000
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1000
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-24-4',
                            fillingPoint: {
                                x: 450,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 700
                }
            },
            productOffset: 0,
            constructionTypeId: 9,
            productionTypeId: 106
        }
    ]
}

export const OC_4: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 980
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 980
                        },
                        end: {
                            x: 370,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 370,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-24-4',
                            fillingPoint: {
                                x: 450,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 700
                }
            },
            productOffset: 0,
            constructionTypeId: 9,
            productionTypeId: 106
        }
    ]
}

export const OC_5: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 530,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 530,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 980
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 980
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-24-4',
                            fillingPoint: {
                                x: 450,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 700
                }
            },
            productOffset: 0,
            constructionTypeId: 9,
            productionTypeId: 106
        }
    ]
}

export const OC_6: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 450,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 450,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-24-4',
                            fillingPoint: {
                                x: 450,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 700
                }
            },
            productOffset: 0,
            constructionTypeId: 9,
            productionTypeId: 106
        }
    ]
}
