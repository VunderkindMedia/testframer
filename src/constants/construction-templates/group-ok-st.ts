import {IConstructionJSON} from '../../model/framer/construction'

export const OK_ST_1: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1300,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 650,
                            y: 1400
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 325,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1250,
                                        y: 50
                                    },
                                    end: {
                                        x: 665,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 665,
                                        y: 50
                                    },
                                    end: {
                                        x: 665,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 665,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1250,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 1250,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1250,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 957.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 957.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 650,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 898
        }
    ]
}

export const OK_ST_2: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1300,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 650,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 637,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1365
                                    },
                                    end: {
                                        x: 637,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 637,
                                        y: 1365
                                    },
                                    end: {
                                        x: 637,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 336,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 336,
                                y: 700
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 975,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 650,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 898
        }
    ]
}

export const OK_ST_3: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1300,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 650,
                            y: 1400
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 635,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 1350
                                    },
                                    end: {
                                        x: 635,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 635,
                                        y: 1350
                                    },
                                    end: {
                                        x: 635,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 342.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 0,
                            fillingPoint: {
                                x: 342.5,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1250,
                                        y: 50
                                    },
                                    end: {
                                        x: 665,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 665,
                                        y: 50
                                    },
                                    end: {
                                        x: 665,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 665,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1250,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 1250,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1250,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 957.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 957.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 650,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 898
        }
    ]
}

export const OK_ST_5: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1300,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 650,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 900
                        },
                        end: {
                            x: 1300,
                            y: 900
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 637,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1365
                                    },
                                    end: {
                                        x: 637,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 637,
                                        y: 1365
                                    },
                                    end: {
                                        x: 637,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 336,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 336,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1265,
                                        y: 913
                                    },
                                    end: {
                                        x: 663,
                                        y: 913
                                    }
                                },
                                {
                                    start: {
                                        x: 663,
                                        y: 913
                                    },
                                    end: {
                                        x: 663,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 663,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1265,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1265,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1265,
                                        y: 913
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 964,
                                            y: 1139
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 964,
                                y: 1139
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 975,
                                y: 450
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 650,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 898,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OK_ST_6: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1300,
                            y: 1400
                        },
                        end: {
                            x: 1300,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 650,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 900
                        },
                        end: {
                            x: 0,
                            y: 900
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 637,
                                        y: 913
                                    },
                                    end: {
                                        x: 35,
                                        y: 913
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 913
                                    },
                                    end: {
                                        x: 35,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1365
                                    },
                                    end: {
                                        x: 637,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 637,
                                        y: 1365
                                    },
                                    end: {
                                        x: 637,
                                        y: 913
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 336,
                                            y: 1139
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 0,
                            fillingPoint: {
                                x: 336,
                                y: 1139
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 325,
                                y: 450
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1265,
                                        y: 35
                                    },
                                    end: {
                                        x: 663,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 663,
                                        y: 35
                                    },
                                    end: {
                                        x: 663,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 663,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1265,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1265,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1265,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 964,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 964,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 650,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 898,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OK_ST_7: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1700
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1700
                        },
                        end: {
                            x: 1300,
                            y: 1700
                        }
                    },
                    {
                        start: {
                            x: 1300,
                            y: 1700
                        },
                        end: {
                            x: 1300,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 0,
                            y: 1200
                        },
                        end: {
                            x: 1300,
                            y: 1200
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 650,
                            y: 1200
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 650,
                                y: 1450
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 325,
                                y: 600
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1250,
                                        y: 50
                                    },
                                    end: {
                                        x: 665,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 665,
                                        y: 50
                                    },
                                    end: {
                                        x: 665,
                                        y: 1185
                                    }
                                },
                                {
                                    start: {
                                        x: 665,
                                        y: 1185
                                    },
                                    end: {
                                        x: 1250,
                                        y: 1185
                                    }
                                },
                                {
                                    start: {
                                        x: 1250,
                                        y: 1185
                                    },
                                    end: {
                                        x: 1250,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 957.5,
                                            y: 617.5
                                        }
                                    }
                                }
                            ],
                            openType: 2,
                            openSide: 1,
                            fillingPoint: {
                                x: 957.5,
                                y: 617.5
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 650,
                    y: 850
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 898
        }
    ]
}

export const OK_ST_8: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1700
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1700
                        },
                        end: {
                            x: 1300,
                            y: 1700
                        }
                    },
                    {
                        start: {
                            x: 1300,
                            y: 1700
                        },
                        end: {
                            x: 1300,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 0,
                            y: 1200
                        },
                        end: {
                            x: 1300,
                            y: 1200
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 1200
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 650,
                                y: 1450
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 637,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1187
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1187
                                    },
                                    end: {
                                        x: 637,
                                        y: 1187
                                    }
                                },
                                {
                                    start: {
                                        x: 637,
                                        y: 1187
                                    },
                                    end: {
                                        x: 637,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 336,
                                            y: 611
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 336,
                                y: 611
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 975,
                                y: 600
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 650,
                    y: 850
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 898
        }
    ]
}

export const OK_ST_9: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1700
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1700
                        },
                        end: {
                            x: 1300,
                            y: 1700
                        }
                    },
                    {
                        start: {
                            x: 1300,
                            y: 1700
                        },
                        end: {
                            x: 1300,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 0,
                            y: 1200
                        },
                        end: {
                            x: 1300,
                            y: 1200
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 1200
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 650,
                                y: 1450
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 637,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1187
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1187
                                    },
                                    end: {
                                        x: 637,
                                        y: 1187
                                    }
                                },
                                {
                                    start: {
                                        x: 637,
                                        y: 1187
                                    },
                                    end: {
                                        x: 637,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 336,
                                            y: 611
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 336,
                                y: 611
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1265,
                                        y: 35
                                    },
                                    end: {
                                        x: 663,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 663,
                                        y: 35
                                    },
                                    end: {
                                        x: 663,
                                        y: 1187
                                    }
                                },
                                {
                                    start: {
                                        x: 663,
                                        y: 1187
                                    },
                                    end: {
                                        x: 1265,
                                        y: 1187
                                    }
                                },
                                {
                                    start: {
                                        x: 1265,
                                        y: 1187
                                    },
                                    end: {
                                        x: 1265,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 964,
                                            y: 611
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 964,
                                y: 611
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 650,
                    y: 850
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 898
        }
    ]
}

export const OK_ST_9_SHTULP: IConstructionJSON = {
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1700
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 1700
                        },
                        end: {
                            x: 1300,
                            y: 1700
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 1300,
                            y: 1700
                        },
                        end: {
                            x: 1300,
                            y: 0
                        },
                        modelPart: 1
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 0,
                            y: 1200
                        },
                        end: {
                            x: 1300,
                            y: 1200
                        },
                        modelPart: 3,
                        shtulpType: 3
                    },
                    {
                        start: {
                            x: 650,
                            y: 1200
                        },
                        end: {
                            x: 650,
                            y: 0
                        },
                        modelPart: 6,
                        shtulpType: 0
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 650,
                                y: 1450
                            }
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 1,
                            frameBeams: [
                                {
                                    start: {
                                        x: 650,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1187
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1187
                                    },
                                    end: {
                                        x: 650,
                                        y: 1187
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 650,
                                        y: 1187
                                    },
                                    end: {
                                        x: 650,
                                        y: 35
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 342.5,
                                            y: 611
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 342.5,
                                y: 611
                            }
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 2,
                            frameBeams: [
                                {
                                    start: {
                                        x: 1265,
                                        y: 35
                                    },
                                    end: {
                                        x: 650,
                                        y: 35
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 650,
                                        y: 35
                                    },
                                    end: {
                                        x: 650,
                                        y: 1187
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 650,
                                        y: 1187
                                    },
                                    end: {
                                        x: 1265,
                                        y: 1187
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 1265,
                                        y: 1187
                                    },
                                    end: {
                                        x: 1265,
                                        y: 35
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 957.5,
                                            y: 611
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 957.5,
                                y: 611
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 650,
                    y: 850
                }
            },
            connectors: [],
            productOffset: 0,
            constructionTypeId: 2,
            productionTypeId: 898
        }
    ]
}
