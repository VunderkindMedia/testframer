import {IConstructionJSON} from '../../model/framer/construction'

export const LODZH_PVH_01: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 375,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1465,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1465,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1114,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1114,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '3',
                    connectedProductGuid: '1'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 737,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 737,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 386,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 386,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1125,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_02: IConstructionJSON = {
    products: [
        {
            productGuid: '84b22e1c-bb7e-73bd-33de-37498fe5b63e',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: 'db3f4f14-a3d3-2f4c-0c71-bef8e5dc6fc1'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 375,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1465,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1465,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1114,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1114,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    connectedProductGuid: 'b55efd10-180a-a47c-b651-08ee9aea3fd2',
                    connectedBeamGuid: 'db3f4f14-a3d3-2f4c-0c71-bef8e5dc6fc1'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'b55efd10-180a-a47c-b651-08ee9aea3fd2',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: '3cd9bfda-257d-0c0e-8b59-9a67acad3359'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 737,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 737,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 386,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 386,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1125,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    profileVendorCode: '155',
                    connectedProductGuid: 'ab341e52-1897-4f7b-67c0-65b23006329f',
                    connectedBeamGuid: '3cd9bfda-257d-0c0e-8b59-9a67acad3359'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'ab341e52-1897-4f7b-67c0-65b23006329f',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 450,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 750
                }
            },
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_03: IConstructionJSON = {
    products: [
        {
            productGuid: 'f3dbcb22-81ad-7550-80f5-4d68722ccda8',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 0
                        },
                        beamGuid: '166bd91a-2f20-9ad1-b3e1-8cbee3027215'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 450,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 900,
                        y: 0
                    },
                    end: {
                        x: 900,
                        y: 1500
                    },
                    profileVendorCode: '155',
                    connectedProductGuid: '99fcfe83-796d-9461-79b8-ab4193633a0e',
                    connectedBeamGuid: '166bd91a-2f20-9ad1-b3e1-8cbee3027215'
                }
            ],
            productOffset: 0,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '99fcfe83-796d-9461-79b8-ab4193633a0e',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: '151786a2-49f6-8ebc-16b1-f2cc42fe7fa2'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 375,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1465,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1465,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1114,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1114,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    connectedProductGuid: 'b64543e3-dc7e-6c2d-571c-2c912b65de68',
                    connectedBeamGuid: '151786a2-49f6-8ebc-16b1-f2cc42fe7fa2'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'b64543e3-dc7e-6c2d-571c-2c912b65de68',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 737,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 737,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 386,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 386,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1125,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_04: IConstructionJSON = {
    products: [
        {
            productGuid: '04404d45-f83f-34c7-c305-f147cecde05c',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 0
                        },
                        beamGuid: '166bd91a-2f20-9ad1-b3e1-8cbee3027215'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 450,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 900,
                        y: 0
                    },
                    end: {
                        x: 900,
                        y: 1500
                    },
                    profileVendorCode: '155',
                    connectedProductGuid: '2e14c10a-38c0-e5f1-f36f-30aeeb358d3f',
                    connectedBeamGuid: '166bd91a-2f20-9ad1-b3e1-8cbee3027215'
                }
            ],
            productOffset: 0,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '2e14c10a-38c0-e5f1-f36f-30aeeb358d3f',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: '151786a2-49f6-8ebc-16b1-f2cc42fe7fa2'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 375,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1465,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1465,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1114,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1114,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    connectedProductGuid: '420f46b2-20ca-9447-cec8-2038540e7adc',
                    connectedBeamGuid: '151786a2-49f6-8ebc-16b1-f2cc42fe7fa2'
                }
            ],
            productOffset: 0,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '420f46b2-20ca-9447-cec8-2038540e7adc',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: '25f01be4-3b21-de2a-a279-aafa4e20d011'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 737,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 737,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 386,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 386,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1125,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    profileVendorCode: '155',
                    connectedProductGuid: 'db680e2b-b7c6-5d2e-b178-8ac45854398a',
                    connectedBeamGuid: '25f01be4-3b21-de2a-a279-aafa4e20d011'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'db680e2b-b7c6-5d2e-b178-8ac45854398a',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 450,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 750
                }
            },
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_05: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1700,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1700,
                            y: 1500
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 1700,
                            y: 1500
                        },
                        end: {
                            x: 1700,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 850,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 850,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1700,
                        y: 0
                    },
                    end: {
                        x: 1700,
                        y: 1500
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '3',
                    connectedProductGuid: '1'
                },
                {
                    start: {
                        x: 0,
                        y: 0
                    },
                    end: {
                        x: 0,
                        y: 1500
                    },
                    beamGuid: '6',
                    connectedBeamGuid: '1',
                    connectedProductGuid: '2'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2150,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 2150,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 2150,
                            y: 1500
                        },
                        end: {
                            x: 2150,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 850,
                            y: 0
                        },
                        end: {
                            x: 850,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 837,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 837,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 436,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 436,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1500,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1075,
                    y: 750
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2150,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 2150,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 2150,
                            y: 1500
                        },
                        end: {
                            x: 2150,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 1300,
                            y: 0
                        },
                        end: {
                            x: 1300,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 650,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 2115,
                                        y: 35
                                    },
                                    end: {
                                        x: 1313,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 1313,
                                        y: 35
                                    },
                                    end: {
                                        x: 1313,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1313,
                                        y: 1465
                                    },
                                    end: {
                                        x: 2115,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 2115,
                                        y: 1465
                                    },
                                    end: {
                                        x: 2115,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1714,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1714,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1075,
                    y: 750
                }
            },
            productGuid: '2',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_06: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 2000,
                            y: 1500
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1500
                        },
                        end: {
                            x: 2000,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 1000,
                            y: 0
                        },
                        end: {
                            x: 1000,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 500,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1500,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1000,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 2000,
                        y: 0
                    },
                    end: {
                        x: 2000,
                        y: 1500
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '3',
                    connectedProductGuid: '1'
                },
                {
                    start: {
                        x: 0,
                        y: 0
                    },
                    end: {
                        x: 0,
                        y: 1500
                    },
                    beamGuid: '6',
                    connectedBeamGuid: '1',
                    connectedProductGuid: '2'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 2000,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1500
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 800,
                            y: 0
                        },
                        end: {
                            x: 800,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 787,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 787,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 787,
                                        y: 1465
                                    },
                                    end: {
                                        x: 787,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 411,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 411,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1400,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1000,
                    y: 750
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 2000,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1500
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 1200,
                            y: 0
                        },
                        end: {
                            x: 1200,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 600,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1965,
                                        y: 35
                                    },
                                    end: {
                                        x: 1213,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 1213,
                                        y: 35
                                    },
                                    end: {
                                        x: 1213,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1213,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1965,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1965,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1965,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1589,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1589,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1000,
                    y: 750
                }
            },
            productGuid: '2',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_07: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2550,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 2550,
                            y: 1500
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 2550,
                            y: 1500
                        },
                        end: {
                            x: 2550,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 850,
                            y: 0
                        },
                        end: {
                            x: 850,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1700,
                            y: 0
                        },
                        end: {
                            x: 1700,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 425,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1687,
                                        y: 35
                                    },
                                    end: {
                                        x: 863,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 863,
                                        y: 35
                                    },
                                    end: {
                                        x: 863,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 863,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1687,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1687,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1687,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1275,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1275,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 2125,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1275,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 2550,
                        y: 0
                    },
                    end: {
                        x: 2550,
                        y: 1500
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '3',
                    connectedProductGuid: '2a08cfa5-cb7d-cd82-b8ea-bce7281a8953'
                },
                {
                    start: {
                        x: 0,
                        y: 0
                    },
                    end: {
                        x: 0,
                        y: 1500
                    },
                    beamGuid: '6',
                    connectedBeamGuid: '1',
                    connectedProductGuid: '2c8bde0c-5b67-222c-58cb-d18274d1ca62'
                }
            ],
            productGuid: '479a1634-5d9d-e49b-95a8-f12ac4f7e7c2',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1720,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1720,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1720,
                            y: 1500
                        },
                        end: {
                            x: 1720,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 850,
                            y: 0
                        },
                        end: {
                            x: 850,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 837,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 837,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 436,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 436,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1285,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 860,
                    y: 750
                }
            },
            productGuid: '2a08cfa5-cb7d-cd82-b8ea-bce7281a8953',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1720,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1720,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1720,
                            y: 1500
                        },
                        end: {
                            x: 1720,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 870,
                            y: 0
                        },
                        end: {
                            x: 870,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 435,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1685,
                                        y: 35
                                    },
                                    end: {
                                        x: 883,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 883,
                                        y: 35
                                    },
                                    end: {
                                        x: 883,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 883,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1685,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1685,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1685,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1284,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1284,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 860,
                    y: 750
                }
            },
            productGuid: '2c8bde0c-5b67-222c-58cb-d18274d1ca62',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_09: IConstructionJSON = {
    products: [
        {
            productGuid: '61a68056-28b7-7213-1b96-cc80967f60f1',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: '8316acdf-34c5-7221-b0e4-cc6f6d8cfbcc'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 375,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1465,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1465,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1114,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1114,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    connectedProductGuid: 'ecfc2f1d-9b89-5872-d921-09cb770e5e90',
                    connectedBeamGuid: '8316acdf-34c5-7221-b0e4-cc6f6d8cfbcc'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'ecfc2f1d-9b89-5872-d921-09cb770e5e90',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: 'e92fa6bc-6b11-2305-c40c-8f3c9a871245'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 375,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1465,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1465,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1114,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1114,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    profileVendorCode: '540+541',
                    connectedProductGuid: '43b1fad4-b21a-4981-e72e-20881cfe1386',
                    connectedBeamGuid: 'e92fa6bc-6b11-2305-c40c-8f3c9a871245'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '43b1fad4-b21a-4981-e72e-20881cfe1386',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 450,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 450,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 0
                        },
                        beamGuid: '918549eb-d53b-0f30-2009-2099fbb04e72'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 225,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 225,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 450,
                        y: 0
                    },
                    end: {
                        x: 450,
                        y: 1500
                    },
                    profileVendorCode: '540+541',
                    connectedProductGuid: 'fc586290-1464-94b9-f928-71667f38ea2b',
                    connectedBeamGuid: '918549eb-d53b-0f30-2009-2099fbb04e72'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'fc586290-1464-94b9-f928-71667f38ea2b',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 450,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 450,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 0
                        },
                        beamGuid: '76bb3305-8407-28ed-ee0a-888e6af0671a'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 415,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 415,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 415,
                                        y: 1465
                                    },
                                    end: {
                                        x: 415,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 225,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 225,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 225,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 450,
                        y: 0
                    },
                    end: {
                        x: 450,
                        y: 1500
                    },
                    profileVendorCode: '540+541',
                    connectedProductGuid: '0e600e5d-0081-2fd5-ced5-207f0c638556',
                    connectedBeamGuid: '76bb3305-8407-28ed-ee0a-888e6af0671a'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '0e600e5d-0081-2fd5-ced5-207f0c638556',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 450,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 450,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 225,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 225,
                    y: 750
                }
            },
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_10: IConstructionJSON = {
    products: [
        {
            productGuid: '514aa363-27f8-e1d7-d724-d8279c414876',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 450,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 450,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 0
                        },
                        beamGuid: '8655d463-f24d-4819-0f5e-381e0ea4b8ea'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 225,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 225,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 450,
                        y: 0
                    },
                    end: {
                        x: 450,
                        y: 1500
                    },
                    profileVendorCode: '540+541',
                    connectedProductGuid: '4f47c281-f2ce-dd4b-1eee-683dc9510181',
                    connectedBeamGuid: '8655d463-f24d-4819-0f5e-381e0ea4b8ea'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '4f47c281-f2ce-dd4b-1eee-683dc9510181',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 450,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 450,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 0
                        },
                        beamGuid: '5eb85b49-ab6b-c5c0-864a-7ccb2f934868'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 415,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 415,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 415,
                                        y: 1465
                                    },
                                    end: {
                                        x: 415,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 225,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 225,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 225,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 450,
                        y: 0
                    },
                    end: {
                        x: 450,
                        y: 1500
                    },
                    profileVendorCode: '540+541',
                    connectedProductGuid: '4f0bf7bd-11f7-fb64-c2b0-27830ff8ccdc',
                    connectedBeamGuid: '5eb85b49-ab6b-c5c0-864a-7ccb2f934868'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '4f0bf7bd-11f7-fb64-c2b0-27830ff8ccdc',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 450,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 450,
                            y: 1500
                        },
                        end: {
                            x: 450,
                            y: 0
                        },
                        beamGuid: '511f6f73-a022-5c07-91c4-f0f727eadcc6'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 225,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 225,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 450,
                        y: 0
                    },
                    end: {
                        x: 450,
                        y: 1500
                    },
                    profileVendorCode: '540+541',
                    connectedProductGuid: 'caec07ca-36e1-710a-46f5-3d7a3b73cf6f',
                    connectedBeamGuid: '511f6f73-a022-5c07-91c4-f0f727eadcc6'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'caec07ca-36e1-710a-46f5-3d7a3b73cf6f',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: 'a54c1003-3145-9952-d5b2-b89f886ce59d'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 737,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 737,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 386,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 386,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1125,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    connectedProductGuid: '826ca776-8d1b-d498-72ff-352215553774',
                    connectedBeamGuid: 'a54c1003-3145-9952-d5b2-b89f886ce59d'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '826ca776-8d1b-d498-72ff-352215553774',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 737,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 737,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 386,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 386,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1125,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_14: IConstructionJSON = {
    products: [
        {
            productGuid: 'd6368061-63df-e9a9-d95e-f9e2a9dd8c15',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 0
                        },
                        beamGuid: '272df374-d57a-7a09-c168-8773c688047f'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 450,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 900,
                        y: 0
                    },
                    end: {
                        x: 900,
                        y: 1500
                    },
                    profileVendorCode: '155',
                    connectedProductGuid: '7dcc3be4-5673-e389-b158-4a092d8116ed',
                    connectedBeamGuid: '272df374-d57a-7a09-c168-8773c688047f'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '7dcc3be4-5673-e389-b158-4a092d8116ed',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1700,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1700,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1700,
                            y: 1500
                        },
                        end: {
                            x: 1700,
                            y: 0
                        },
                        beamGuid: '6897b649-79ed-24e4-cf56-ef5790c7110f'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 850,
                            y: 0
                        },
                        end: {
                            x: 850,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 837,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 837,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 436,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 436,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1665,
                                        y: 35
                                    },
                                    end: {
                                        x: 863,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 863,
                                        y: 35
                                    },
                                    end: {
                                        x: 863,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 863,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1665,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1665,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1665,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1264,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1264,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 850,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1700,
                        y: 0
                    },
                    end: {
                        x: 1700,
                        y: 1500
                    },
                    connectedProductGuid: '0cf277a5-5fe4-e4f8-76c6-8ee2053a1dfa',
                    connectedBeamGuid: '6897b649-79ed-24e4-cf56-ef5790c7110f'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '0cf277a5-5fe4-e4f8-76c6-8ee2053a1dfa',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 870,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 870,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 870,
                            y: 1500
                        },
                        end: {
                            x: 870,
                            y: 0
                        },
                        beamGuid: '75f50982-abe0-fa46-4d27-6b32ce38bde6'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 435,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 435,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 870,
                        y: 0
                    },
                    end: {
                        x: 870,
                        y: 1500
                    },
                    connectedProductGuid: '631cc53b-903a-7ed9-cb4f-6e8f4a4cbfc2',
                    connectedBeamGuid: '75f50982-abe0-fa46-4d27-6b32ce38bde6'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '631cc53b-903a-7ed9-cb4f-6e8f4a4cbfc2',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1700,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1700,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1700,
                            y: 1500
                        },
                        end: {
                            x: 1700,
                            y: 0
                        },
                        beamGuid: '9862b016-92a3-aab8-57a4-8a56757e2466'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 850,
                            y: 0
                        },
                        end: {
                            x: 850,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 837,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 837,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 436,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 436,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1275,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 850,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1700,
                        y: 0
                    },
                    end: {
                        x: 1700,
                        y: 1500
                    },
                    connectedProductGuid: '18ec76f4-2a39-ab3d-8af8-3857470895d0',
                    connectedBeamGuid: '9862b016-92a3-aab8-57a4-8a56757e2466'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '18ec76f4-2a39-ab3d-8af8-3857470895d0',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1700,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1700,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1700,
                            y: 1500
                        },
                        end: {
                            x: 1700,
                            y: 0
                        },
                        beamGuid: 'e1bce930-0a97-c12b-6e17-57712b83b92f'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 850,
                            y: 0
                        },
                        end: {
                            x: 850,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 837,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 837,
                                        y: 1465
                                    },
                                    end: {
                                        x: 837,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 436,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 436,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1665,
                                        y: 35
                                    },
                                    end: {
                                        x: 863,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 863,
                                        y: 35
                                    },
                                    end: {
                                        x: 863,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 863,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1665,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1665,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1665,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1264,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1264,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 850,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1700,
                        y: 0
                    },
                    end: {
                        x: 1700,
                        y: 1500
                    },
                    profileVendorCode: '155',
                    connectedProductGuid: '0d7a72ef-b5a0-c8ea-ec0f-d99c194325be',
                    connectedBeamGuid: 'e1bce930-0a97-c12b-6e17-57712b83b92f'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '0d7a72ef-b5a0-c8ea-ec0f-d99c194325be',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1500
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 450,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 750
                }
            },
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}

export const LODZH_PVH_15: IConstructionJSON = {
    products: [
        {
            productGuid: '80fe073a-c95e-e864-373b-27cf706f0aac',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: '8e0f5579-1af8-b7c9-6073-117c866742e6'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 375,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1465,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 35
                                    },
                                    end: {
                                        x: 763,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 763,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1465,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1465,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1114,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1114,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 1500
                    },
                    connectedProductGuid: 'fe231c56-999c-acab-9f5e-9083e67dcc36',
                    connectedBeamGuid: '8e0f5579-1af8-b7c9-6073-117c866742e6'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'fe231c56-999c-acab-9f5e-9083e67dcc36',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1500,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1500
                        },
                        end: {
                            x: 1500,
                            y: 0
                        },
                        beamGuid: 'bcc6cd46-4621-bd56-e3d1-ca6b13394ee0'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 750,
                            y: 0
                        },
                        end: {
                            x: 750,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 737,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 737,
                                        y: 1465
                                    },
                                    end: {
                                        x: 737,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 386,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 386,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1125,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 750,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1500,
                        y: 0
                    },
                    end: {
                        x: 1500,
                        y: 800
                    },
                    connectedProductGuid: 'fc3cf971-aa0f-0b6c-1d97-872b56b25113',
                    connectedBeamGuid: 'bcc6cd46-4621-bd56-e3d1-ca6b13394ee0'
                }
            ],
            productOffset: 0,
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'fc3cf971-aa0f-0b6c-1d97-872b56b25113',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 245,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 800
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 800
                        },
                        end: {
                            x: 245,
                            y: 800
                        }
                    },
                    {
                        start: {
                            x: 245,
                            y: 800
                        },
                        end: {
                            x: 245,
                            y: 0
                        },
                        beamGuid: '27dcc95f-e088-af3d-0fe6-0bc276d6220f'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 122.5,
                                y: 400
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 122.5,
                    y: 400
                }
            },
            connectors: [
                {
                    start: {
                        x: 245,
                        y: 0
                    },
                    end: {
                        x: 245,
                        y: 800
                    },
                    connectedProductGuid: '6191b5e4-5e28-141c-41e2-f4d696662b90',
                    connectedBeamGuid: '27dcc95f-e088-af3d-0fe6-0bc276d6220f'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: '6191b5e4-5e28-141c-41e2-f4d696662b90',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1493,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1493,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1493,
                            y: 1500
                        },
                        end: {
                            x: 1493,
                            y: 0
                        },
                        beamGuid: '7ae6c1e1-35c8-64fe-d6a8-d13390d62f1f'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 746,
                            y: 0
                        },
                        end: {
                            x: 746,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 373,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1458,
                                        y: 35
                                    },
                                    end: {
                                        x: 759,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 759,
                                        y: 35
                                    },
                                    end: {
                                        x: 759,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 759,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1458,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1458,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1458,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1108.5,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1108.5,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 746.5,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1493,
                        y: 0
                    },
                    end: {
                        x: 1493,
                        y: 1500
                    },
                    connectedProductGuid: 'a9a6ab91-4d1b-5799-6a6a-892900b5eaaf',
                    connectedBeamGuid: '7ae6c1e1-35c8-64fe-d6a8-d13390d62f1f'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'a9a6ab91-4d1b-5799-6a6a-892900b5eaaf',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1498,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1498,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1498,
                            y: 1500
                        },
                        end: {
                            x: 1498,
                            y: 0
                        },
                        beamGuid: '02b4e6a4-779e-50c6-b141-37186b27ec4d'
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 749,
                            y: 0
                        },
                        end: {
                            x: 749,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 736,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1465
                                    },
                                    end: {
                                        x: 736,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 736,
                                        y: 1465
                                    },
                                    end: {
                                        x: 736,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 385.5,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 385.5,
                                y: 750
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1123.5,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 749,
                    y: 750
                }
            },
            connectors: [
                {
                    start: {
                        x: 1498,
                        y: 0
                    },
                    end: {
                        x: 1498,
                        y: 1500
                    },
                    profileVendorCode: '155',
                    connectedProductGuid: 'fbc02006-f41a-ad9d-8107-091234c27f48',
                    connectedBeamGuid: '02b4e6a4-779e-50c6-b141-37186b27ec4d'
                }
            ],
            constructionTypeId: 2,
            productionTypeId: 1192
        },
        {
            productGuid: 'fbc02006-f41a-ad9d-8107-091234c27f48',
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1200,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1500
                        },
                        end: {
                            x: 1200,
                            y: 1500
                        }
                    },
                    {
                        start: {
                            x: 1200,
                            y: 1500
                        },
                        end: {
                            x: 1200,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 600,
                            y: 0
                        },
                        end: {
                            x: 600,
                            y: 1500
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 300,
                                y: 750
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1165,
                                        y: 35
                                    },
                                    end: {
                                        x: 613,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 613,
                                        y: 35
                                    },
                                    end: {
                                        x: 613,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 613,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1165,
                                        y: 1465
                                    }
                                },
                                {
                                    start: {
                                        x: 1165,
                                        y: 1465
                                    },
                                    end: {
                                        x: 1165,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 889,
                                            y: 750
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 889,
                                y: 750
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 600,
                    y: 750
                }
            },
            constructionTypeId: 2,
            productionTypeId: 1192
        }
    ]
}
