import {IConstructionJSON} from '../../model/framer/construction'

export const AL_LODZH_01: IConstructionJSON = {
    products: [
        {
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {x: 1600, y: 0},
                        end: {x: 0, y: 0},
                        modelPart: 1,
                        profileVendorCode: 'C640/02'
                    },
                    {
                        start: {x: 0, y: 0},
                        end: {x: 0, y: 1600},
                        modelPart: 1,
                        profileVendorCode: 'C640/03'
                    },
                    {
                        start: {x: 0, y: 1600},
                        end: {x: 1600, y: 1600},
                        modelPart: 1,
                        profileVendorCode: 'C640/01'
                    },
                    {
                        start: {x: 1600, y: 1600},
                        end: {x: 1600, y: 0},
                        modelPart: 1,
                        profileVendorCode: 'C640/03'
                    }
                ],
                impostBeams: [
                    {
                        start: {x: 800, y: 0},
                        end: {x: 800, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'Сцепка'
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 825, y: 28},
                                    end: {x: 24, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 24, y: 28},
                                    end: {x: 24, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                },
                                {
                                    start: {x: 24, y: 1573},
                                    end: {x: 825, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 825, y: 1573},
                                    end: {x: 825, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingPoint: {x: 424.5, y: 800.5},
                                        fillingVendorCode: '4мм Стекло'
                                    }
                                }
                            ],
                            fillingPoint: {x: 424.5, y: 800.5},
                            openSide: 1,
                            openType: 5
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 1576, y: 28},
                                    end: {x: 775, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 775, y: 28},
                                    end: {x: 775, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                },
                                {
                                    start: {x: 775, y: 1573},
                                    end: {x: 1576, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 1576, y: 1573},
                                    end: {x: 1576, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingPoint: {x: 1175.5, y: 800.5},
                                        fillingVendorCode: '4мм Стекло'
                                    }
                                }
                            ],
                            fillingPoint: {x: 1175.5, y: 800.5},
                            openSide: 0,
                            openType: 5
                        }
                    }
                ],
                fillingPoint: {x: 800, y: 800},
                openSide: 4,
                openType: 0
            },
            insideColorId: 234,
            outsideColorId: 234,
            constructionTypeId: 13,
            productionTypeId: 247,
            templateID: 1873
        }
    ]
}

export const AL_LODZH_02: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {x: 2400, y: 0},
                        end: {x: 0, y: 0},
                        modelPart: 1,
                        profileVendorCode: 'C640/02'
                    },
                    {
                        start: {x: 0, y: 0},
                        end: {x: 0, y: 1600},
                        modelPart: 1,
                        profileVendorCode: 'C640/03'
                    },
                    {
                        start: {x: 0, y: 1600},
                        end: {x: 2400, y: 1600},
                        modelPart: 1,
                        profileVendorCode: 'C640/01'
                    },
                    {
                        start: {x: 2400, y: 1600},
                        end: {x: 2400, y: 0},
                        modelPart: 1,
                        profileVendorCode: 'C640/03'
                    }
                ],
                impostBeams: [
                    {
                        start: {x: 795, y: 0},
                        end: {x: 795, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'Сцепка'
                    },
                    {
                        start: {x: 1570, y: 0},
                        end: {x: 1570, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'C640/30 Стык'
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 820, y: 28},
                                    end: {x: 24, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 24, y: 28},
                                    end: {x: 24, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                },
                                {
                                    start: {x: 24, y: 1573},
                                    end: {x: 820, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 820, y: 1573},
                                    end: {x: 820, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 422, y: 800.5}
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 5,
                            fillingPoint: {x: 422, y: 800.5}
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 1570, y: 28},
                                    end: {x: 770, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 770, y: 28},
                                    end: {x: 770, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                },
                                {
                                    start: {x: 770, y: 1573},
                                    end: {x: 1570, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 1570, y: 1573},
                                    end: {x: 1570, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 1170, y: 800.5}
                                    }
                                }
                            ],
                            openType: 5,
                            openSide: 0,
                            fillingPoint: {x: 1170, y: 800.5}
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 2376, y: 28},
                                    end: {x: 1571, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 1571, y: 28},
                                    end: {x: 1571, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                },
                                {
                                    start: {x: 1571, y: 1573},
                                    end: {x: 2376, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 2376, y: 1573},
                                    end: {x: 2376, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 1973.5, y: 800.5}
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 5,
                            fillingPoint: {x: 1973.5, y: 800.5}
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {x: 1200, y: 800}
            },
            insideColorId: 234,
            outsideColorId: 234,
            constructionTypeId: 13,
            productionTypeId: 247,
            templateID: 1874
        }
    ]
}

export const AL_LODZH_03: IConstructionJSON = {
    products: [
        {
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {x: 3000, y: 0},
                        end: {x: 0, y: 0},
                        modelPart: 1,
                        profileVendorCode: 'C640/02'
                    },
                    {
                        start: {x: 0, y: 0},
                        end: {x: 0, y: 1600},
                        modelPart: 1,
                        profileVendorCode: 'C640/03'
                    },
                    {
                        start: {x: 0, y: 1600},
                        end: {x: 3000, y: 1600},
                        modelPart: 1,
                        profileVendorCode: 'C640/01'
                    },
                    {
                        start: {x: 3000, y: 1600},
                        end: {x: 3000, y: 0},
                        modelPart: 1,
                        profileVendorCode: 'C640/03'
                    }
                ],
                impostBeams: [
                    {
                        start: {x: 762, y: 0},
                        end: {x: 762, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'Сцепка'
                    },
                    {
                        start: {x: 1500, y: 0},
                        end: {x: 1500, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'C640/30 Стык'
                    },
                    {
                        start: {x: 2238, y: 0},
                        end: {x: 2238, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'Сцепка'
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 787, y: 28},
                                    end: {x: 24, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 24, y: 28},
                                    end: {x: 24, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                },
                                {
                                    start: {x: 24, y: 1573},
                                    end: {x: 787, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 787, y: 1573},
                                    end: {x: 787, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 405.5, y: 800.5}
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 5,
                            fillingPoint: {x: 405.5, y: 800.5}
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 1499, y: 28},
                                    end: {x: 737, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 737, y: 28},
                                    end: {x: 737, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                },
                                {
                                    start: {x: 737, y: 1573},
                                    end: {x: 1499, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 1499, y: 1573},
                                    end: {x: 1499, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 1118, y: 800.5}
                                    }
                                }
                            ],
                            openType: 5,
                            openSide: 0,
                            fillingPoint: {x: 1118, y: 800.5}
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 2263, y: 28},
                                    end: {x: 1501, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 1501, y: 28},
                                    end: {x: 1501, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                },
                                {
                                    start: {x: 1501, y: 1573},
                                    end: {x: 2263, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 2263, y: 1573},
                                    end: {x: 2263, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 1882, y: 800.5}
                                    }
                                }
                            ],
                            openType: 5,
                            openSide: 1,
                            fillingPoint: {x: 1882, y: 800.5}
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 2976, y: 28},
                                    end: {x: 2213, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 2213, y: 28},
                                    end: {x: 2213, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                },
                                {
                                    start: {x: 2213, y: 1573},
                                    end: {x: 2976, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 2976, y: 1573},
                                    end: {x: 2976, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 2594.5, y: 800.5}
                                    }
                                }
                            ],
                            openType: 5,
                            openSide: 0,
                            fillingPoint: {x: 2594.5, y: 800.5}
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {x: 1500, y: 800}
            },
            insideColorId: 234,
            outsideColorId: 234,
            constructionTypeId: 13,
            productionTypeId: 247,
            templateID: 1875
        }
    ]
}

export const AL_LODZH_04: IConstructionJSON = {
    products: [
        {
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {x: 4000, y: 0},
                        end: {x: 0, y: 0},
                        modelPart: 1,
                        profileVendorCode: 'C640/02'
                    },
                    {
                        start: {x: 0, y: 0},
                        end: {x: 0, y: 1600},
                        modelPart: 1,
                        profileVendorCode: 'C640/03'
                    },
                    {
                        start: {x: 0, y: 1600},
                        end: {x: 4000, y: 1600},
                        modelPart: 1,
                        profileVendorCode: 'C640/01'
                    },
                    {
                        start: {x: 4000, y: 1600},
                        end: {x: 4000, y: 0},
                        modelPart: 1,
                        profileVendorCode: 'C640/03'
                    }
                ],
                impostBeams: [
                    {
                        start: {x: 808, y: 0},
                        end: {x: 808, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'Сцепка'
                    },
                    {
                        start: {x: 1592, y: 0},
                        end: {x: 1592, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'C640/30 Стык'
                    },
                    {
                        start: {x: 2408, y: 0},
                        end: {x: 2408, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'C640/30 Стык'
                    },
                    {
                        start: {x: 3192, y: 0},
                        end: {x: 3192, y: 1600},
                        modelPart: 3,
                        shtulpType: 3,
                        profileVendorCode: 'Сцепка'
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 833, y: 28},
                                    end: {x: 24, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 24, y: 28},
                                    end: {x: 24, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                },
                                {
                                    start: {x: 24, y: 1573},
                                    end: {x: 833, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 833, y: 1573},
                                    end: {x: 833, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 428.5, y: 800.5}
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 5,
                            fillingPoint: {x: 428.5, y: 800.5}
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 1592, y: 28},
                                    end: {x: 783, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 783, y: 28},
                                    end: {x: 783, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                },
                                {
                                    start: {x: 783, y: 1573},
                                    end: {x: 1592, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 1592, y: 1573},
                                    end: {x: 1592, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 1187.5, y: 800.5}
                                    }
                                }
                            ],
                            openType: 5,
                            openSide: 0,
                            fillingPoint: {x: 1187.5, y: 800.5}
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 2407, y: 28},
                                    end: {x: 1593, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 1593, y: 28},
                                    end: {x: 1593, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                },
                                {
                                    start: {x: 1593, y: 1573},
                                    end: {x: 2407, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 2407, y: 1573},
                                    end: {x: 2407, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 2000, y: 800.5}
                                    }
                                }
                            ],
                            openType: 5,
                            openSide: 1,
                            fillingPoint: {x: 2000, y: 800.5}
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 3217, y: 28},
                                    end: {x: 2408, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 2408, y: 28},
                                    end: {x: 2408, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                },
                                {
                                    start: {x: 2408, y: 1573},
                                    end: {x: 3217, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 3217, y: 1573},
                                    end: {x: 3217, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 2812.5, y: 800.5}
                                    }
                                }
                            ],
                            openType: 5,
                            openSide: 1,
                            fillingPoint: {x: 2812.5, y: 800.5}
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {x: 3976, y: 28},
                                    end: {x: 3167, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 3167, y: 28},
                                    end: {x: 3167, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/11'
                                },
                                {
                                    start: {x: 3167, y: 1573},
                                    end: {x: 3976, y: 1573},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/12'
                                },
                                {
                                    start: {x: 3976, y: 1573},
                                    end: {x: 3976, y: 28},
                                    modelPart: 2,
                                    profileVendorCode: 'C640/10'
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4мм Стекло',
                                        fillingPoint: {x: 3571.5, y: 800.5}
                                    }
                                }
                            ],
                            openType: 5,
                            openSide: 0,
                            fillingPoint: {x: 3571.5, y: 800.5}
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {x: 2000, y: 800}
            },
            insideColorId: 234,
            outsideColorId: 234,
            constructionTypeId: 13,
            productionTypeId: 247,
            templateID: 1876
        }
    ]
}

export const AL_LODZH_05: IConstructionJSON = {
    products: [
        {
            frame: {
                fillingPoint: {
                    x: 450,
                    y: 800
                },
                frameBeams: [
                    {
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 900,
                            y: 0
                        },
                        userParameters: []
                    },
                    {
                        end: {
                            x: 0,
                            y: 1600
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 0,
                            y: 0
                        },
                        userParameters: []
                    },
                    {
                        end: {
                            x: 900,
                            y: 1600
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 0,
                            y: 1600
                        },
                        userParameters: []
                    },
                    {
                        end: {
                            x: 900,
                            y: 0
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 900,
                            y: 1600
                        },
                        userParameters: []
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingPoint: {
                                x: 450,
                                y: 800
                            },
                            fillingVendorCode: '4мм Стекло'
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                shtulpOpenType: 0,
                userParameters: []
            },
            insideColorId: 234,
            outsideColorId: 234,
            constructionTypeId: 13,
            productionTypeId: 247
        }
    ]
}

export const AL_LODZH_06: IConstructionJSON = {
    products: [
        {
            frame: {
                fillingPoint: {
                    x: 450,
                    y: 800
                },
                frameBeams: [
                    {
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 900,
                            y: 0
                        },
                        userParameters: []
                    },
                    {
                        end: {
                            x: 0,
                            y: 1600
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 0,
                            y: 0
                        },
                        userParameters: []
                    },
                    {
                        end: {
                            x: 900,
                            y: 1600
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 0,
                            y: 1600
                        },
                        userParameters: []
                    },
                    {
                        end: {
                            x: 900,
                            y: 0
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 900,
                            y: 1600
                        },
                        userParameters: []
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            fillingPoint: {
                                x: 450,
                                y: 800
                            },
                            frameBeams: [
                                {
                                    end: {
                                        x: 25,
                                        y: 25
                                    },
                                    modelPart: 2,
                                    profileVendorCode: 'P400/02',
                                    start: {
                                        x: 875,
                                        y: 25
                                    },
                                    userParameters: []
                                },
                                {
                                    end: {
                                        x: 25,
                                        y: 1575
                                    },
                                    modelPart: 2,
                                    profileVendorCode: 'P400/02',
                                    start: {
                                        x: 25,
                                        y: 25
                                    },
                                    userParameters: []
                                },
                                {
                                    end: {
                                        x: 875,
                                        y: 1575
                                    },
                                    modelPart: 2,
                                    profileVendorCode: 'P400/02',
                                    start: {
                                        x: 25,
                                        y: 1575
                                    },
                                    userParameters: []
                                },
                                {
                                    end: {
                                        x: 875,
                                        y: 25
                                    },
                                    modelPart: 2,
                                    profileVendorCode: 'P400/02',
                                    start: {
                                        x: 875,
                                        y: 1575
                                    },
                                    userParameters: []
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingPoint: {
                                            x: 450,
                                            y: 800
                                        },
                                        fillingVendorCode: '4мм Стекло'
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            shtulpOpenType: 0,
                            userParameters: []
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                shtulpOpenType: 0
            },
            insideColorId: 234,
            outsideColorId: 234,
            constructionTypeId: 13,
            productionTypeId: 247,
            profileSystemName: 'Provedal C640, P400'
        }
    ]
}

export const AL_LODZH_07: IConstructionJSON = {
    products: [
        {
            frame: {
                fillingPoint: {
                    x: 450,
                    y: 800
                },
                frameBeams: [
                    {
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 900,
                            y: 0
                        },
                        userParameters: []
                    },
                    {
                        end: {
                            x: 0,
                            y: 1600
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 0,
                            y: 0
                        },
                        userParameters: []
                    },
                    {
                        end: {
                            x: 900,
                            y: 1600
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 0,
                            y: 1600
                        },
                        userParameters: []
                    },
                    {
                        end: {
                            x: 900,
                            y: 0
                        },
                        modelPart: 1,
                        profileVendorCode: 'C640/35',
                        start: {
                            x: 900,
                            y: 1600
                        },
                        userParameters: []
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            fillingPoint: {
                                x: 450,
                                y: 800
                            },
                            frameBeams: [
                                {
                                    end: {
                                        x: 25,
                                        y: 25
                                    },
                                    modelPart: 2,
                                    profileVendorCode: 'P400/02',
                                    start: {
                                        x: 875,
                                        y: 25
                                    },
                                    userParameters: []
                                },
                                {
                                    end: {
                                        x: 25,
                                        y: 1575
                                    },
                                    modelPart: 2,
                                    profileVendorCode: 'P400/02',
                                    start: {
                                        x: 25,
                                        y: 25
                                    },
                                    userParameters: []
                                },
                                {
                                    end: {
                                        x: 875,
                                        y: 1575
                                    },
                                    modelPart: 2,
                                    profileVendorCode: 'P400/02',
                                    start: {
                                        x: 25,
                                        y: 1575
                                    },
                                    userParameters: []
                                },
                                {
                                    end: {
                                        x: 875,
                                        y: 25
                                    },
                                    modelPart: 2,
                                    profileVendorCode: 'P400/02',
                                    start: {
                                        x: 875,
                                        y: 1575
                                    },
                                    userParameters: []
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingPoint: {
                                            x: 450,
                                            y: 800
                                        },
                                        fillingVendorCode: '4мм Стекло'
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            shtulpOpenType: 0,
                            userParameters: []
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                shtulpOpenType: 0
            },
            insideColorId: 234,
            outsideColorId: 234,
            constructionTypeId: 13,
            productionTypeId: 247,
            profileSystemName: 'Provedal C640, P400'
        }
    ]
}
