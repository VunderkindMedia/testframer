import {IConstructionJSON} from '../../model/framer/construction'

export const OK_STER_1: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 667,
                            y: 1400
                        },
                        end: {
                            x: 667,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 1400
                        },
                        end: {
                            x: 1333,
                            y: 0
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 333.5,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1318,
                                        y: 50
                                    },
                                    end: {
                                        x: 682,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 682,
                                        y: 50
                                    },
                                    end: {
                                        x: 682,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 682,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1318,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 1318,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1318,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1000,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 1000,
                                y: 700
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1666.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1000,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1177
        }
    ]
}

export const OK_STER_2: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 667,
                            y: 0
                        },
                        end: {
                            x: 667,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 0
                        },
                        end: {
                            x: 1333,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 654,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1365
                                    },
                                    end: {
                                        x: 654,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 654,
                                        y: 1365
                                    },
                                    end: {
                                        x: 654,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 344.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 0,
                            fillingPoint: {
                                x: 344.5,
                                y: 700
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1000,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1965,
                                        y: 35
                                    },
                                    end: {
                                        x: 1346,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 35
                                    },
                                    end: {
                                        x: 1346,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1965,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1965,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1965,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1655.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1655.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1000,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1177
        }
    ]
}

export const OK_STER_3: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 667,
                            y: 0
                        },
                        end: {
                            x: 667,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 0
                        },
                        end: {
                            x: 1333,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 654,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1365
                                    },
                                    end: {
                                        x: 654,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 654,
                                        y: 1365
                                    },
                                    end: {
                                        x: 654,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 344.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 344.5,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1320,
                                        y: 35
                                    },
                                    end: {
                                        x: 680,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 680,
                                        y: 35
                                    },
                                    end: {
                                        x: 680,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 680,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1320,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1320,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1320,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1000,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 0,
                            fillingPoint: {
                                x: 1000,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1965,
                                        y: 35
                                    },
                                    end: {
                                        x: 1346,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 35
                                    },
                                    end: {
                                        x: 1346,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1965,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1965,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1965,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1655.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 1655.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1000,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1177
        }
    ]
}

export const OK_STER_4: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 667,
                            y: 0
                        },
                        end: {
                            x: 667,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 0
                        },
                        end: {
                            x: 1333,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 900
                        },
                        end: {
                            x: 2000,
                            y: 900
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 654,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1365
                                    },
                                    end: {
                                        x: 654,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 654,
                                        y: 1365
                                    },
                                    end: {
                                        x: 654,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 344.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 344.5,
                                y: 700
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1000,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1965,
                                        y: 913
                                    },
                                    end: {
                                        x: 1346,
                                        y: 913
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 913
                                    },
                                    end: {
                                        x: 1346,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1965,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1965,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1965,
                                        y: 913
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1655.5,
                                            y: 1139
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 1655.5,
                                y: 1139
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1965,
                                        y: 35
                                    },
                                    end: {
                                        x: 1346,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 35
                                    },
                                    end: {
                                        x: 1346,
                                        y: 887
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 887
                                    },
                                    end: {
                                        x: 1965,
                                        y: 887
                                    }
                                },
                                {
                                    start: {
                                        x: 1965,
                                        y: 887
                                    },
                                    end: {
                                        x: 1965,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1655.5,
                                            y: 461
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1655.5,
                                y: 461
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1000,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1177,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OK_STER_5: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 667,
                            y: 0
                        },
                        end: {
                            x: 667,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 0
                        },
                        end: {
                            x: 1333,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 667,
                            y: 900
                        },
                        end: {
                            x: 0,
                            y: 900
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 654,
                                        y: 913
                                    },
                                    end: {
                                        x: 35,
                                        y: 913
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 913
                                    },
                                    end: {
                                        x: 35,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1365
                                    },
                                    end: {
                                        x: 654,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 654,
                                        y: 1365
                                    },
                                    end: {
                                        x: 654,
                                        y: 913
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 344.5,
                                            y: 1139
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 344.5,
                                y: 1139
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 654,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 887
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 887
                                    },
                                    end: {
                                        x: 654,
                                        y: 887
                                    }
                                },
                                {
                                    start: {
                                        x: 654,
                                        y: 887
                                    },
                                    end: {
                                        x: 654,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 344.5,
                                            y: 461
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 0,
                            fillingPoint: {
                                x: 344.5,
                                y: 461
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1000,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1965,
                                        y: 35
                                    },
                                    end: {
                                        x: 1346,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 35
                                    },
                                    end: {
                                        x: 1346,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1346,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1965,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1965,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1965,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1655.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1655.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1000,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1177,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}
