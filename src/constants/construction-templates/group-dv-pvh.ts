import {IConstructionJSON} from '../../model/framer/construction'
import {PointsOfView} from '../../model/points-of-view'
import {HandlePositionTypes} from '../../model/framer/handle-position-types'

export const DV_PVH_1: IConstructionJSON = {
    defaultPointOfView: PointsOfView.Outside,
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        modelPart: 5,
                        start: {
                            x: 950,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2000
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 2000
                        },
                        end: {
                            x: 950,
                            y: 2000
                        }
                    },
                    {
                        start: {
                            x: 950,
                            y: 2000
                        },
                        end: {
                            x: 950,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 915,
                                        y: 9.5625
                                    },
                                    end: {
                                        x: 35,
                                        y: 9.5625
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 9.5625
                                    },
                                    end: {
                                        x: 35,
                                        y: 1965
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1965
                                    },
                                    end: {
                                        x: 915,
                                        y: 1965
                                    }
                                },
                                {
                                    start: {
                                        x: 915,
                                        y: 1965
                                    },
                                    end: {
                                        x: 915,
                                        y: 9.5625
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 800
                                    },
                                    end: {
                                        x: 915,
                                        y: 800
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 475,
                                            y: 1387.2813
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 475,
                                            y: 409.5625
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            handlePositionType: HandlePositionTypes.Fixed,
                            fillingPoint: {
                                x: 475,
                                y: 987.2813
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 475,
                    y: 1000
                }
            },
            connectors: [],
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 7,
            productionTypeId: 1197
        }
    ]
}

export const DV_PVH_1_0: IConstructionJSON = {
    defaultPointOfView: PointsOfView.Outside,
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {
                            x: 950,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 5
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2500
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 2500
                        },
                        end: {
                            x: 950,
                            y: 2500
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 950,
                            y: 2500
                        },
                        end: {
                            x: 950,
                            y: 0
                        },
                        modelPart: 1
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 0,
                            y: 2000
                        },
                        end: {
                            x: 950,
                            y: 2000
                        },
                        modelPart: 3,
                        shtulpType: 3
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-16-4',
                            fillingPoint: {
                                x: 475,
                                y: 2250
                            }
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 0,
                            frameBeams: [
                                {
                                    start: {
                                        x: 906,
                                        y: 12
                                    },
                                    end: {
                                        x: 44,
                                        y: 12
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 44,
                                        y: 12
                                    },
                                    end: {
                                        x: 44,
                                        y: 1987
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 44,
                                        y: 1987
                                    },
                                    end: {
                                        x: 906,
                                        y: 1987
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 906,
                                        y: 1987
                                    },
                                    end: {
                                        x: 906,
                                        y: 12
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 44,
                                        y: 800
                                    },
                                    end: {
                                        x: 906,
                                        y: 800
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 475,
                                            y: 1393.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 475,
                                            y: 406
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 475,
                                y: 999.5
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 475,
                    y: 1250
                }
            },
            constructionTypeId: 7,
            productionTypeId: 1197
        }
    ]
}

export const DV_PVH_2: IConstructionJSON = {
    defaultPointOfView: PointsOfView.Outside,
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {
                            x: 1350,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 5
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2000
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 2000
                        },
                        end: {
                            x: 1350,
                            y: 2000
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 1350,
                            y: 2000
                        },
                        end: {
                            x: 1350,
                            y: 0
                        },
                        modelPart: 1
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 420,
                            y: 0
                        },
                        end: {
                            x: 420,
                            y: 2000
                        },
                        modelPart: 6,
                        shtulpType: 0
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            shtulpOpenType: 1,
                            frameBeams: [
                                {
                                    start: {
                                        x: 416,
                                        y: 12
                                    },
                                    end: {
                                        x: 44,
                                        y: 12
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 44,
                                        y: 12
                                    },
                                    end: {
                                        x: 44,
                                        y: 1956
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 44,
                                        y: 1956
                                    },
                                    end: {
                                        x: 416,
                                        y: 1956
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 416,
                                        y: 1956
                                    },
                                    end: {
                                        x: 416,
                                        y: 12
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 44,
                                        y: 812
                                    },
                                    end: {
                                        x: 416,
                                        y: 812
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 230,
                                            y: 1384
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 230,
                                            y: 412
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 230,
                                y: 984
                            }
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 2,
                            frameBeams: [
                                {
                                    start: {
                                        x: 1306,
                                        y: 12
                                    },
                                    end: {
                                        x: 424,
                                        y: 12
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 424,
                                        y: 12
                                    },
                                    end: {
                                        x: 424,
                                        y: 1956
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 424,
                                        y: 1956
                                    },
                                    end: {
                                        x: 1306,
                                        y: 1956
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 1306,
                                        y: 1956
                                    },
                                    end: {
                                        x: 1306,
                                        y: 12
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 424,
                                        y: 812
                                    },
                                    end: {
                                        x: 1306,
                                        y: 812
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 865,
                                            y: 1384
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 865,
                                            y: 412
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 865,
                                y: 984
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 675,
                    y: 1000
                }
            },
            connectors: [],
            productOffset: 0,
            constructionTypeId: 7,
            productionTypeId: 1197
        }
    ]
}

export const DV_PVH_3: IConstructionJSON = {
    defaultPointOfView: PointsOfView.Outside,
    products: [
        {
            insideColorId: 2,
            outsideColorId: 2,
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {
                            x: 1350,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 5
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2000
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 2000
                        },
                        end: {
                            x: 1350,
                            y: 2000
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 1350,
                            y: 2000
                        },
                        end: {
                            x: 1350,
                            y: 0
                        },
                        modelPart: 1
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 950,
                            y: 0
                        },
                        end: {
                            x: 950,
                            y: 2000
                        },
                        modelPart: 6,
                        shtulpType: 1
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            shtulpOpenType: 2,
                            frameBeams: [
                                {
                                    start: {
                                        x: 946,
                                        y: 12
                                    },
                                    end: {
                                        x: 44,
                                        y: 12
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 44,
                                        y: 12
                                    },
                                    end: {
                                        x: 44,
                                        y: 1956
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 44,
                                        y: 1956
                                    },
                                    end: {
                                        x: 946,
                                        y: 1956
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 946,
                                        y: 1956
                                    },
                                    end: {
                                        x: 946,
                                        y: 12
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 44,
                                        y: 812
                                    },
                                    end: {
                                        x: 946,
                                        y: 812
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 495,
                                            y: 1384
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 495,
                                            y: 412
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 495,
                                y: 984
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    },
                    {
                        leaf: {
                            shtulpOpenType: 1,
                            frameBeams: [
                                {
                                    start: {
                                        x: 1306,
                                        y: 12
                                    },
                                    end: {
                                        x: 954,
                                        y: 12
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 954,
                                        y: 12
                                    },
                                    end: {
                                        x: 954,
                                        y: 1956
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 954,
                                        y: 1956
                                    },
                                    end: {
                                        x: 1306,
                                        y: 1956
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 1306,
                                        y: 1956
                                    },
                                    end: {
                                        x: 1306,
                                        y: 12
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 954,
                                        y: 812
                                    },
                                    end: {
                                        x: 1306,
                                        y: 812
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 1130,
                                            y: 1384
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 1130,
                                            y: 412
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1130,
                                y: 984
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 675,
                    y: 1000
                }
            },
            connectors: [],
            productOffset: 0,
            constructionTypeId: 7,
            productionTypeId: 1197
        }
    ]
}

export const DV_PVH_4: IConstructionJSON = {
    defaultPointOfView: PointsOfView.Outside,
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1350,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 5
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2500
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 0,
                            y: 2500
                        },
                        end: {
                            x: 1350,
                            y: 2500
                        },
                        modelPart: 1
                    },
                    {
                        start: {
                            x: 1350,
                            y: 2500
                        },
                        end: {
                            x: 1350,
                            y: 0
                        },
                        modelPart: 1
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 0,
                            y: 2000
                        },
                        end: {
                            x: 1350,
                            y: 2000
                        }
                    },
                    {
                        start: {
                            x: 420,
                            y: 2000
                        },
                        end: {
                            x: 420,
                            y: 0
                        },
                        modelPart: 6,
                        shtulpType: 0
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-16-4',
                            fillingPoint: {
                                x: 675,
                                y: 2250
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 407,
                                        y: 12
                                    },
                                    end: {
                                        x: 35,
                                        y: 12
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 12
                                    },
                                    end: {
                                        x: 35,
                                        y: 1987
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1987
                                    },
                                    end: {
                                        x: 407,
                                        y: 1987
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 407,
                                        y: 1987
                                    },
                                    end: {
                                        x: 407,
                                        y: 12
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 800
                                    },
                                    end: {
                                        x: 407,
                                        y: 800
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 221,
                                            y: 1388.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 221,
                                            y: 401
                                        }
                                    }
                                }
                            ],
                            shtulpOpenType: 1,
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 221,
                                y: 999.5
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1315,
                                        y: 12
                                    },
                                    end: {
                                        x: 433,
                                        y: 12
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 433,
                                        y: 12
                                    },
                                    end: {
                                        x: 433,
                                        y: 1987
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 433,
                                        y: 1987
                                    },
                                    end: {
                                        x: 1315,
                                        y: 1987
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 1315,
                                        y: 1987
                                    },
                                    end: {
                                        x: 1315,
                                        y: 12
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 433,
                                        y: 800
                                    },
                                    end: {
                                        x: 1315,
                                        y: 800
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 874,
                                            y: 1388.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 874,
                                            y: 401
                                        }
                                    }
                                }
                            ],
                            shtulpOpenType: 2,
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 874,
                                y: 999.5
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 675,
                    y: 1250
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 7,
            productionTypeId: 1197
        }
    ]
}

export const DV_PVH_5: IConstructionJSON = {
    defaultPointOfView: PointsOfView.Outside,
    products: [
        {
            frame: {
                shtulpOpenType: 0,
                frameBeams: [
                    {
                        start: {
                            x: 1350,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        modelPart: 5
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2500
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 2500
                        },
                        end: {
                            x: 1350,
                            y: 2500
                        }
                    },
                    {
                        start: {
                            x: 1350,
                            y: 2500
                        },
                        end: {
                            x: 1350,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 0,
                            y: 2000
                        },
                        end: {
                            x: 1350,
                            y: 2000
                        },
                        modelPart: 3
                    },
                    {
                        start: {
                            x: 950,
                            y: 2000
                        },
                        end: {
                            x: 950,
                            y: 0
                        },
                        modelPart: 6,
                        shtulpType: 1
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-16-4',
                            fillingPoint: {
                                x: 675,
                                y: 2250
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 937,
                                        y: 12
                                    },
                                    end: {
                                        x: 35,
                                        y: 12
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 12
                                    },
                                    end: {
                                        x: 35,
                                        y: 1987
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1987
                                    },
                                    end: {
                                        x: 937,
                                        y: 1987
                                    },
                                    modelPart: 2
                                },
                                {
                                    start: {
                                        x: 937,
                                        y: 1987
                                    },
                                    end: {
                                        x: 937,
                                        y: 12
                                    },
                                    modelPart: 2
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 800
                                    },
                                    end: {
                                        x: 937,
                                        y: 800
                                    },
                                    modelPart: 3,
                                    shtulpType: 3
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 486,
                                            y: 1388.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 486,
                                            y: 401
                                        }
                                    }
                                }
                            ],
                            shtulpOpenType: 2,
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 486,
                                y: 999.5
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1315,
                                        y: 12
                                    },
                                    end: {
                                        x: 963,
                                        y: 12
                                    }
                                },
                                {
                                    start: {
                                        x: 963,
                                        y: 12
                                    },
                                    end: {
                                        x: 963,
                                        y: 1987
                                    }
                                },
                                {
                                    start: {
                                        x: 963,
                                        y: 1987
                                    },
                                    end: {
                                        x: 1315,
                                        y: 1987
                                    }
                                },
                                {
                                    start: {
                                        x: 1315,
                                        y: 1987
                                    },
                                    end: {
                                        x: 1315,
                                        y: 12
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 963,
                                        y: 800
                                    },
                                    end: {
                                        x: 1315,
                                        y: 800
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-16-4',
                                        fillingPoint: {
                                            x: 1139,
                                            y: 1388.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 24 мм',
                                        fillingPoint: {
                                            x: 1139,
                                            y: 401
                                        }
                                    }
                                }
                            ],
                            shtulpOpenType: 1,
                            openSide: 1,
                            openType: 1,
                            fillingPoint: {
                                x: 1139,
                                y: 999.5
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 675,
                    y: 1250
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 7,
            productionTypeId: 1197
        }
    ]
}
