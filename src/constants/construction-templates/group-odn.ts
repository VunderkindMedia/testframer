import {IConstructionJSON} from '../../model/framer/construction'

export const ODN_1: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 450,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1176
        }
    ]
}

export const ODN_2: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 900,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 850,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 1350
                                    },
                                    end: {
                                        x: 850,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 850,
                                        y: 1350
                                    },
                                    end: {
                                        x: 850,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 450,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 2,
                            openSide: 1,
                            fillingPoint: {
                                x: 450,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 450,
                    y: 700
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1176
        }
    ]
}
