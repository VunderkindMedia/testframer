import {IConstructionJSON} from '../../model/framer/construction'

export const B_B_1: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 2100
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 650,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 600,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 2050
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 2050
                                    },
                                    end: {
                                        x: 600,
                                        y: 2050
                                    }
                                },
                                {
                                    start: {
                                        x: 600,
                                        y: 2050
                                    },
                                    end: {
                                        x: 600,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 722
                                    },
                                    end: {
                                        x: 615,
                                        y: 722
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 325,
                                            y: 1386
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 325,
                                            y: 386
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 325,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 1050
                }
            },
            connectors: [
                {
                    start: {
                        x: 650,
                        y: 2100
                    },
                    end: {
                        x: 650,
                        y: 700
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '3',
                    connectedProductGuid: '1'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 922
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1323,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1323,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1323,
                            y: 1400
                        },
                        end: {
                            x: 1323,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 661.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 661.5,
                    y: 700
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 922
        }
    ]
}

export const B_B_2: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 2100
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 650,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 600,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 2050
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 2050
                                    },
                                    end: {
                                        x: 600,
                                        y: 2050
                                    }
                                },
                                {
                                    start: {
                                        x: 600,
                                        y: 2050
                                    },
                                    end: {
                                        x: 600,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 50,
                                        y: 722
                                    },
                                    end: {
                                        x: 600,
                                        y: 722
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 325,
                                            y: 1386
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 325,
                                            y: 386
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 325,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 1050
                }
            },
            connectors: [
                {
                    start: {
                        x: 0,
                        y: 2100
                    },
                    end: {
                        x: 0,
                        y: 700
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '1',
                    connectedProductGuid: '1'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 922
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1323,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1323,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1323,
                            y: 1400
                        },
                        end: {
                            x: 1323,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 661.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 661.5,
                    y: 700
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 922
        }
    ]
}

export const B_B_3: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 2100
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 650,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 615,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 615,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 722
                                    },
                                    end: {
                                        x: 615,
                                        y: 722
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 325,
                                            y: 1393.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 325,
                                            y: 378.5
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 325,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 1050
                }
            },
            connectors: [
                {
                    start: {
                        x: 650,
                        y: 700
                    },
                    end: {
                        x: 650,
                        y: 2100
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '3',
                    connectedProductGuid: '1'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 922
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1350,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1350,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1350,
                            y: 1400
                        },
                        end: {
                            x: 1350,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 675,
                            y: 0
                        },
                        end: {
                            x: 675,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 337.5,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1315,
                                        y: 35
                                    },
                                    end: {
                                        x: 688,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 688,
                                        y: 35
                                    },
                                    end: {
                                        x: 688,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 688,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1315,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1315,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1315,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1001.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 2,
                            openSide: 1,
                            fillingPoint: {
                                x: 1001.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 675,
                    y: 700
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 922
        }
    ]
}

export const B_B_4: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 2100
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 650,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 615,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 615,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 722
                                    },
                                    end: {
                                        x: 615,
                                        y: 722
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 325,
                                            y: 1393.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 325,
                                            y: 378.5
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 325,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 1050
                }
            },
            connectors: [
                {
                    start: {
                        x: 0,
                        y: 700
                    },
                    end: {
                        x: 0,
                        y: 2100
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '1',
                    connectedProductGuid: '1'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 922
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1323,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1323,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1323,
                            y: 1400
                        },
                        end: {
                            x: 1323,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 662,
                            y: 0
                        },
                        end: {
                            x: 662,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 649,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1365
                                    },
                                    end: {
                                        x: 649,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 649,
                                        y: 1365
                                    },
                                    end: {
                                        x: 649,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 342,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 2,
                            openSide: 0,
                            fillingPoint: {
                                x: 342,
                                y: 700
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 992.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 661.5,
                    y: 700
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 922
        }
    ]
}

export const B_B_5: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 2100
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 650,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 615,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 615,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 722
                                    },
                                    end: {
                                        x: 615,
                                        y: 722
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 325,
                                            y: 1393.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 325,
                                            y: 378.5
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 325,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 1050
                }
            },
            connectors: [
                {
                    start: {
                        x: 650,
                        y: 700
                    },
                    end: {
                        x: 650,
                        y: 2100
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '3',
                    connectedProductGuid: '1'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 922
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1350,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1350,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1350,
                            y: 1400
                        },
                        end: {
                            x: 1350,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 675,
                            y: 0
                        },
                        end: {
                            x: 675,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 662,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1365
                                    },
                                    end: {
                                        x: 662,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 662,
                                        y: 1365
                                    },
                                    end: {
                                        x: 662,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 348.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 348.5,
                                y: 700
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1012.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 675,
                    y: 700
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 922
        }
    ]
}
export const B_B_6: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 2100
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 650,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 615,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 2065
                                    }
                                },
                                {
                                    start: {
                                        x: 615,
                                        y: 2065
                                    },
                                    end: {
                                        x: 615,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 35,
                                        y: 722
                                    },
                                    end: {
                                        x: 615,
                                        y: 722
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 325,
                                            y: 1393.5
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 325,
                                            y: 378.5
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 325,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 1050
                }
            },
            connectors: [
                {
                    start: {
                        x: 0,
                        y: 700
                    },
                    end: {
                        x: 0,
                        y: 2100
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '1',
                    connectedProductGuid: '1'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 922
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1323,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1323,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1323,
                            y: 1400
                        },
                        end: {
                            x: 1323,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 662,
                            y: 0
                        },
                        end: {
                            x: 662,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 331,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1288,
                                        y: 35
                                    },
                                    end: {
                                        x: 675,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 675,
                                        y: 35
                                    },
                                    end: {
                                        x: 675,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 675,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1288,
                                        y: 1365
                                    }
                                },
                                {
                                    start: {
                                        x: 1288,
                                        y: 1365
                                    },
                                    end: {
                                        x: 1288,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 981.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 2,
                            fillingPoint: {
                                x: 981.5,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 661.5,
                    y: 700
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 922
        }
    ]
}

export const B_B_7: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        },
                        beamGuid: '0'
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 2100
                        },
                        beamGuid: '1'
                    },
                    {
                        start: {
                            x: 0,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 2100
                        },
                        beamGuid: '2'
                    },
                    {
                        start: {
                            x: 650,
                            y: 2100
                        },
                        end: {
                            x: 650,
                            y: 0
                        },
                        beamGuid: '3'
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 600,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 2050
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 2050
                                    },
                                    end: {
                                        x: 600,
                                        y: 2050
                                    }
                                },
                                {
                                    start: {
                                        x: 600,
                                        y: 2050
                                    },
                                    end: {
                                        x: 600,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [
                                {
                                    start: {
                                        x: 50,
                                        y: 722
                                    },
                                    end: {
                                        x: 600,
                                        y: 722
                                    }
                                }
                            ],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 325,
                                            y: 1386
                                        }
                                    }
                                },
                                {
                                    solid: {
                                        fillingVendorCode: 'Сэндвич 32 мм',
                                        fillingPoint: {
                                            x: 325,
                                            y: 386
                                        }
                                    }
                                }
                            ],
                            openSide: 1,
                            openType: 2,
                            fillingPoint: {
                                x: 325,
                                y: 1050
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 1050
                }
            },
            connectors: [
                {
                    start: {
                        x: 650,
                        y: 2100
                    },
                    end: {
                        x: 650,
                        y: 700
                    },
                    beamGuid: '5',
                    connectedBeamGuid: '3',
                    connectedProductGuid: '1'
                },
                {
                    start: {
                        x: 0,
                        y: 2100
                    },
                    end: {
                        x: 0,
                        y: 700
                    },
                    beamGuid: '6',
                    connectedBeamGuid: '1',
                    connectedProductGuid: '2'
                }
            ],
            productGuid: '0',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 3,
            productionTypeId: 922
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 650,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 1400
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 325,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 700
                }
            },
            productGuid: '1',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 922
        },
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 650,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 650,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 650,
                            y: 1400
                        },
                        end: {
                            x: 650,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 325,
                                y: 700
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 325,
                    y: 700
                }
            },
            productGuid: '2',
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 922
        }
    ]
}
