import {IConstructionJSON} from '../../model/framer/construction'
import {HandlePositionTypes} from '../../model/framer/handle-position-types'

export const OB_MN_1: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 700,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1000
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1000
                        },
                        end: {
                            x: 700,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 700,
                            y: 1400
                        },
                        end: {
                            x: 700,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 665,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 979.6887
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 979.6887
                                    },
                                    end: {
                                        x: 665,
                                        y: 1339.6887
                                    }
                                },
                                {
                                    start: {
                                        x: 665,
                                        y: 1339.6887
                                    },
                                    end: {
                                        x: 665,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 350,
                                            y: 597.3444
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 350,
                                y: 597.3444
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 350,
                    y: 600
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_2: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 700,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 700,
                            y: 1000
                        }
                    },
                    {
                        start: {
                            x: 700,
                            y: 1000
                        },
                        end: {
                            x: 700,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 665,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1339.6887
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1339.6887
                                    },
                                    end: {
                                        x: 665,
                                        y: 979.6887
                                    }
                                },
                                {
                                    start: {
                                        x: 665,
                                        y: 979.6887
                                    },
                                    end: {
                                        x: 665,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 350,
                                            y: 597.3443
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 0,
                            fillingPoint: {
                                x: 350,
                                y: 597.3443
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 350,
                    y: 600
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_3: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1400,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 900
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 900
                        },
                        end: {
                            x: 1400,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1400,
                            y: 1400
                        },
                        end: {
                            x: 1400,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 700,
                            y: 0
                        },
                        end: {
                            x: 700,
                            y: 1150
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 350,
                                y: 512.5
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1365,
                                        y: 35
                                    },
                                    end: {
                                        x: 713,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 713,
                                        y: 35
                                    },
                                    end: {
                                        x: 713,
                                        y: 1117.4777
                                    }
                                },
                                {
                                    start: {
                                        x: 713,
                                        y: 1117.4777
                                    },
                                    end: {
                                        x: 1365,
                                        y: 1350.3348
                                    }
                                },
                                {
                                    start: {
                                        x: 1365,
                                        y: 1350.3348
                                    },
                                    end: {
                                        x: 1365,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1039,
                                            y: 634.4531
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 1039,
                                y: 634.4531
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 700,
                    y: 575
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_4: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1400,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1400,
                            y: 900
                        }
                    },
                    {
                        start: {
                            x: 1400,
                            y: 900
                        },
                        end: {
                            x: 1400,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 700,
                            y: 0
                        },
                        end: {
                            x: 700,
                            y: 1150
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 687,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 35
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 35
                                    },
                                    end: {
                                        x: 35,
                                        y: 1350.3348
                                    }
                                },
                                {
                                    start: {
                                        x: 35,
                                        y: 1350.3348
                                    },
                                    end: {
                                        x: 687,
                                        y: 1117.4777
                                    }
                                },
                                {
                                    start: {
                                        x: 687,
                                        y: 1117.4777
                                    },
                                    end: {
                                        x: 687,
                                        y: 35
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 361,
                                            y: 634.4531
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 0,
                            fillingPoint: {
                                x: 361,
                                y: 634.4531
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1050,
                                y: 512.5
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 700,
                    y: 575
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_9: IConstructionJSON = {
    products: [
        {
            frame: {
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 50,
                    y: 50
                },
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 500,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 500,
                            y: 1400
                        },
                        end: {
                            x: 1500,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1500,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 667,
                            y: 0
                        },
                        end: {
                            x: 667,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 0
                        },
                        end: {
                            x: 1333,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 100,
                                y: 200
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 100,
                                y: 200
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 100,
                                y: 200
                            }
                        }
                    }
                ]
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_10: IConstructionJSON = {
    products: [
        {
            frame: {
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 50,
                    y: 50
                },
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 980
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 980
                        },
                        end: {
                            x: 370,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 370,
                            y: 1400
                        },
                        end: {
                            x: 1630,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1630,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 980
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 980
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 667,
                            y: 0
                        },
                        end: {
                            x: 667,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 0
                        },
                        end: {
                            x: 1333,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 100,
                                y: 200
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1318,
                                        y: 50
                                    },
                                    end: {
                                        x: 682,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 682,
                                        y: 50
                                    },
                                    end: {
                                        x: 682,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 682,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1318,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 1318,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1318,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1000,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 1000,
                                y: 700
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 100,
                                y: 200
                            }
                        }
                    }
                ]
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_11: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 980
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 980
                        },
                        end: {
                            x: 370,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 370,
                            y: 1400
                        },
                        end: {
                            x: 1630,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1630,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 980
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 980
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 667,
                            y: 0
                        },
                        end: {
                            x: 667,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 0
                        },
                        end: {
                            x: 1333,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 870
                        },
                        end: {
                            x: 667,
                            y: 870
                        }
                    },
                    {
                        start: {
                            x: 667,
                            y: 870
                        },
                        end: {
                            x: 1333,
                            y: 870
                        }
                    },
                    {
                        start: {
                            x: 1333,
                            y: 870
                        },
                        end: {
                            x: 2000,
                            y: 870
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 340.8,
                                y: 1104
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 652,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 855
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 855
                                    },
                                    end: {
                                        x: 652,
                                        y: 855
                                    }
                                },
                                {
                                    start: {
                                        x: 652,
                                        y: 855
                                    },
                                    end: {
                                        x: 652,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 351,
                                            y: 452.5
                                        }
                                    }
                                }
                            ],
                            openSide: 0,
                            openType: 1,
                            fillingPoint: {
                                x: 351,
                                y: 452.5
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1000,
                                y: 1135
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1000,
                                y: 435
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1659.2,
                                y: 1104
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1950,
                                        y: 50
                                    },
                                    end: {
                                        x: 1348,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 1348,
                                        y: 50
                                    },
                                    end: {
                                        x: 1348,
                                        y: 855
                                    }
                                },
                                {
                                    start: {
                                        x: 1348,
                                        y: 855
                                    },
                                    end: {
                                        x: 1950,
                                        y: 855
                                    }
                                },
                                {
                                    start: {
                                        x: 1950,
                                        y: 855
                                    },
                                    end: {
                                        x: 1950,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1649,
                                            y: 452.5
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 1649,
                                y: 452.5
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1000,
                    y: 793.3333
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_12: IConstructionJSON = {
    products: [
        {
            frame: {
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 50,
                    y: 50
                },
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1000
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1000
                        },
                        end: {
                            x: 1000,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1000,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 1000
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1000
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 0,
                            y: 850
                        },
                        end: {
                            x: 2000,
                            y: 850
                        }
                    },
                    {
                        start: {
                            x: 641,
                            y: 0
                        },
                        end: {
                            x: 641,
                            y: 850
                        }
                    },
                    {
                        start: {
                            x: 1359,
                            y: 0
                        },
                        end: {
                            x: 1359,
                            y: 850
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 100,
                                y: 200
                            }
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 100,
                                y: 200
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1344,
                                        y: 50
                                    },
                                    end: {
                                        x: 656,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 656,
                                        y: 50
                                    },
                                    end: {
                                        x: 656,
                                        y: 835
                                    }
                                },
                                {
                                    start: {
                                        x: 656,
                                        y: 835
                                    },
                                    end: {
                                        x: 1344,
                                        y: 835
                                    }
                                },
                                {
                                    start: {
                                        x: 1344,
                                        y: 835
                                    },
                                    end: {
                                        x: 1344,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1000,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 1000,
                                y: 700
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 100,
                                y: 200
                            }
                        }
                    }
                ]
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_14: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1350,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 900,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 900,
                            y: 1400
                        },
                        end: {
                            x: 1350,
                            y: 900
                        }
                    },
                    {
                        start: {
                            x: 1350,
                            y: 900
                        },
                        end: {
                            x: 1350,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 724,
                            y: 0
                        },
                        end: {
                            x: 724,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 709,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 50
                                    },
                                    end: {
                                        x: 50,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 50,
                                        y: 1350
                                    },
                                    end: {
                                        x: 709,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 709,
                                        y: 1350
                                    },
                                    end: {
                                        x: 709,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 379.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 0,
                            fillingPoint: {
                                x: 379.5,
                                y: 700
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1049.6,
                                y: 756
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 760,
                    y: 756
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_15: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1350,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 958
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 958
                        },
                        end: {
                            x: 368,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 368,
                            y: 1400
                        },
                        end: {
                            x: 1350,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1350,
                            y: 1400
                        },
                        end: {
                            x: 1350,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 626,
                            y: 0
                        },
                        end: {
                            x: 626,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 324,
                                y: 751.6
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1300,
                                        y: 50
                                    },
                                    end: {
                                        x: 641,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 641,
                                        y: 50
                                    },
                                    end: {
                                        x: 641,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 641,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1300,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 1300,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1300,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 970.5,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 1,
                            openSide: 1,
                            fillingPoint: {
                                x: 970.5,
                                y: 700
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 613.6,
                    y: 751.6
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_16: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 2000,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1400
                        },
                        end: {
                            x: 1630,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1630,
                            y: 1400
                        },
                        end: {
                            x: 2000,
                            y: 1032
                        }
                    },
                    {
                        start: {
                            x: 2000,
                            y: 1032
                        },
                        end: {
                            x: 2000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [
                    {
                        start: {
                            x: 641,
                            y: 0
                        },
                        end: {
                            x: 641,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1359,
                            y: 0
                        },
                        end: {
                            x: 1359,
                            y: 1400
                        }
                    }
                ],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 320.5,
                                y: 700
                            }
                        }
                    },
                    {
                        leaf: {
                            frameBeams: [
                                {
                                    start: {
                                        x: 1344,
                                        y: 50
                                    },
                                    end: {
                                        x: 656,
                                        y: 50
                                    }
                                },
                                {
                                    start: {
                                        x: 656,
                                        y: 50
                                    },
                                    end: {
                                        x: 656,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 656,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1344,
                                        y: 1350
                                    }
                                },
                                {
                                    start: {
                                        x: 1344,
                                        y: 1350
                                    },
                                    end: {
                                        x: 1344,
                                        y: 50
                                    }
                                }
                            ],
                            impostBeams: [],
                            innerFillings: [
                                {
                                    solid: {
                                        fillingVendorCode: '4-10-4-10-4',
                                        fillingPoint: {
                                            x: 1000,
                                            y: 700
                                        }
                                    }
                                }
                            ],
                            openType: 2,
                            openSide: 0,
                            fillingPoint: {
                                x: 1000,
                                y: 700
                            },
                            handlePositionType: HandlePositionTypes.Fixed
                        }
                    },
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 1669.6,
                                y: 766.4
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 1126,
                    y: 766.4
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}

export const OB_MN_17: IConstructionJSON = {
    products: [
        {
            frame: {
                frameBeams: [
                    {
                        start: {
                            x: 1000,
                            y: 0
                        },
                        end: {
                            x: 400,
                            y: 0
                        }
                    },
                    {
                        start: {
                            x: 400,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 400
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 400
                        },
                        end: {
                            x: 0,
                            y: 1000
                        }
                    },
                    {
                        start: {
                            x: 0,
                            y: 1000
                        },
                        end: {
                            x: 400,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 400,
                            y: 1400
                        },
                        end: {
                            x: 1000,
                            y: 1400
                        }
                    },
                    {
                        start: {
                            x: 1000,
                            y: 1400
                        },
                        end: {
                            x: 1400,
                            y: 1000
                        }
                    },
                    {
                        start: {
                            x: 1400,
                            y: 1000
                        },
                        end: {
                            x: 1400,
                            y: 400
                        }
                    },
                    {
                        start: {
                            x: 1400,
                            y: 400
                        },
                        end: {
                            x: 1000,
                            y: 0
                        }
                    }
                ],
                impostBeams: [],
                innerFillings: [
                    {
                        solid: {
                            fillingVendorCode: '4-10-4-10-4',
                            fillingPoint: {
                                x: 613.6,
                                y: 751.6
                            }
                        }
                    }
                ],
                openSide: 4,
                openType: 0,
                fillingPoint: {
                    x: 613.6,
                    y: 751.6
                }
            },
            insideColorId: 2,
            outsideColorId: 2,
            constructionTypeId: 2,
            productionTypeId: 1194,
            hardwareSystemName: 'Фурнитура Maco MM'
        }
    ]
}
