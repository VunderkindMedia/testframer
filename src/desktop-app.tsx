import * as React from 'react'
import './desktop-app.css'
import {Route} from 'react-router'
import {
    NAV_BILL,
    NAV_BOOKING,
    NAV_PROFILE,
    NAV_CLIENTS,
    NAV_CLIENT,
    NAV_KP,
    NAV_ORDERS,
    NAV_PAYMENT,
    NAV_PAYMENT_RESULT,
    NAV_ROOT,
    NAV_START_TUTORIAL,
    NAV_ORDER,
    NAV_PARTNER_GOODS,
    NAV_RESET_PASSWORD,
    NAV_FORGOT_PASSWORD,
    NAV_REGISTRATION,
    NAV_LOGIN,
    NAV_PARTNER_GOODS_REPORT,
    NAV_EVENTS,
    NAV_EVENTS_REPORT,
    NAV_MOUNTINGS,
    NAV_MOUNTING,
    NAV_MOUNTING_REPORT,
    NAV_BUILDER
} from './constants/navigation'
import {Header} from './components/desktop/header/header'
import {LoginPage} from './components/common/authorization-pages/login-page/login-page'
import {RegistrationPage} from './components/common/authorization-pages/registration-page/registration-page'
import {ForgotPasswordPage} from './components/common/authorization-pages/forgot-password-page/forgot-password-page'
import {ResetPasswordPage} from './components/common/authorization-pages/reset-password-page/reset-password-page'
import {PaymentPage} from './components/common/payment-page/payment-page'
import {Statusbar} from './components/common/statusbar/statusbar'
import {ProgressBar} from './components/common/progress-bar/progress-bar'
import {PaymentResultPage} from './components/common/payment-result-page/payment-result-page'
import {KpPage} from './components/desktop/kp-page/kp-page'
import {BillPage} from './components/desktop/bill-page/bill-page'
import {StartTutorialPage} from './components/common/start-tutorial-page/start-tutorial-page'
import {useSelector} from 'react-redux'
import {IAppState} from './redux/root-reducer'
import {ProfilePage} from './components/desktop/profile-page/profile-page'
import {BuilderPageErrorBoundaryConnected} from './components/common/builder-page-error-boundary/builder-page-error-boundary'
import {Menu} from './components/desktop/menu/menu'
import {OrderPage} from './components/desktop/order-page/order-page'
import {PartnerGoodsPage} from './components/desktop/partner-goods-page/partner-goods-page'
import {OrdersPage} from './components/desktop/orders-page/orders-page'
import {BookingPage} from './components/common/booking-page/booking-page'
import {PartnerGoodsReportPage} from './components/desktop/partner-goods-report-page/partner-goods-report-page'
import {useEffect} from 'react'
import {useHideNavigation} from './hooks/navigation'
import {EventsPage} from './components/desktop/events-page/events-page'
import {EventsPageReport} from './components/desktop/events-page-report/events-page-report'
import {MountingsPage} from './components/desktop/mountings-page/mountings-page'
import {MountingPage} from './components/desktop/mounting-page/mounting-page'
import {MountingReport} from './components/desktop/mounting-report/mounting-report'
import {useUpdateScreenSizeDependencies, useInitCarrotQuest, useAppContext, useShowOnboarding} from './hooks/app'
import {ClientsPage} from './components/desktop/clients-page/clients-page'
import {ClientPage} from './components/desktop/client-page/client-page'
import {Builder} from './components/desktop/builder/builder'
import {usePermissions, useIsDemoUser} from './hooks/permissions'
import {Onboarding} from './components/common/onboarding/onboarding'
import {EnergyPage} from './components/common/energy-page/energy-page'

export function DesktopApp(): JSX.Element {
    const isVisible = useSelector((state: IAppState) => state.statusbar.isVisible)
    const isHideNavigation = useHideNavigation()
    useUpdateScreenSizeDependencies()
    useInitCarrotQuest()
    const {isTablet} = useAppContext()
    const {isNonQualified} = usePermissions()
    const isDemoUser = useIsDemoUser()
    const showOnboarding = useShowOnboarding()

    useEffect(() => {
        document.body.classList.add(isTablet ? 'tablet' : 'desktop')

        return () => {
            document.body.classList.remove(isTablet ? 'tablet' : 'desktop')
        }
    }, [isTablet])

    useEffect(() => {
        const height = isNonQualified || isDemoUser ? 80 : 50
        document.documentElement.style.setProperty('--header-height', `${height}px`)
    }, [isNonQualified, isDemoUser])

    return (
        <div className={`app ${isVisible && 'statusbar-visible'} ${isHideNavigation && 'hide-navigation'}`}>
            <Statusbar />
            {showOnboarding && <Onboarding />}
            <div className="app__content">
                <Route exact path={NAV_ROOT} render={() => <LoginPage />} />
                <Route exact path={NAV_LOGIN} render={() => <LoginPage />} />
                <Route exact path={NAV_REGISTRATION} render={() => <RegistrationPage />} />
                <Route exact path={NAV_FORGOT_PASSWORD} render={() => <ForgotPasswordPage />} />
                <Route exact path={NAV_RESET_PASSWORD} render={() => <ResetPasswordPage />} />
                <Route path={NAV_PROFILE} render={() => <ProfilePage />} />
                <Route path={NAV_ORDERS} render={() => <OrdersPage />} />
                <Route
                    path={NAV_ORDER}
                    render={() => (
                        <BuilderPageErrorBoundaryConnected>
                            <OrderPage />
                        </BuilderPageErrorBoundaryConnected>
                    )}
                />
                <Route path={NAV_BOOKING} render={() => <BookingPage />} />
                <Route path={NAV_CLIENTS} render={() => <ClientsPage />} />
                <Route path={NAV_CLIENT} render={() => <ClientPage />} />
                <Route path={NAV_PAYMENT} render={() => <PaymentPage />} />
                <Route path={NAV_PAYMENT_RESULT} render={() => <PaymentResultPage />} />
                <Route path={NAV_KP} render={() => <KpPage />} />
                <Route path={NAV_BILL} render={() => <BillPage />} />
                <Route path={NAV_START_TUTORIAL} render={() => <StartTutorialPage />} />
                <Route path={NAV_PARTNER_GOODS} render={() => <PartnerGoodsPage />} />
                <Route path={NAV_PARTNER_GOODS_REPORT} render={() => <PartnerGoodsReportPage />} />
                <Route path={NAV_EVENTS} render={() => <EventsPage />} />
                <Route path={NAV_EVENTS_REPORT} render={() => <EventsPageReport />} />
                <Route path={NAV_MOUNTINGS} render={() => <MountingsPage />} />
                <Route path={NAV_MOUNTING} render={() => <MountingPage />} />
                <Route path={NAV_MOUNTING_REPORT} render={() => <MountingReport />} />
                <Route path={NAV_BUILDER} render={() => <Builder />} />
                <Route path={'/energy_*'} render={() => <EnergyPage />} />
                {!isHideNavigation && <Header />}
                <Menu />
                <ProgressBar />
            </div>
        </div>
    )
}
