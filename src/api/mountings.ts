import {request, RequestMethods} from './request'
import {IMountingsResponseJSON, MountingsResponse} from '../model/framer/mountings-response'
import {Mounting, IMountingJSON} from '../model/framer/mounting'
import {API_MOUNTINGS, API_MOUNTING} from '../constants/api'

export const mountings = {
    get: async (filters: string): Promise<MountingsResponse> => {
        const response = await request.fetch(`${API_MOUNTINGS}${filters ?? ''}`)
        const json = (await response.json()) as IMountingsResponseJSON

        return MountingsResponse.parse(json)
    },

    getById: async (id: number): Promise<Mounting> => {
        const response = await request.fetch(API_MOUNTING(id))
        const json = (await response.json()) as IMountingJSON

        return Mounting.parse(json)
    },

    add: async (date: string, executorsIds: number[], orderId: number, clientId?: number): Promise<Mounting> => {
        const response = await request.fetch(API_MOUNTINGS, {
            method: RequestMethods.POST,
            body: JSON.stringify({
                datetime: date,
                executorsIds,
                orderId,
                clientId
            })
        })
        const json = (await response.json()) as IMountingJSON

        return Mounting.parse(json)
    },

    update: async (id: number, mounting: Mounting): Promise<Mounting> => {
        const response = await request.fetch(API_MOUNTING(id), {method: RequestMethods.POST, body: JSON.stringify(mounting)})
        const json = (await response.json()) as IMountingJSON

        return Mounting.parse(json)
    },

    delete: async (id: number): Promise<string> => {
        const response = await request.fetch(API_MOUNTING(id), {method: RequestMethods.DELETE})
        const text = await response.text()

        return response.ok ? '' : text
    }
}
