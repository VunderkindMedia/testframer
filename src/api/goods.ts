import {request, RequestMethods} from './request'
import {API_GOODS, API_ORDERS, API_GOOD_GROUPS} from '../constants/api'
import {Good, IGoodJSON} from '../model/framer/good'
import {IOrderJSON, Order} from '../model/framer/order'
import {GoodGroup, IGoodGroupJSON} from '../model/framer/good-group'
import {stringify} from '../helpers/json'

export const goods = {
    get: async (): Promise<Good[]> => {
        const response = await request.fetch(API_GOODS)
        const json = (await response.json()) as IGoodJSON[]

        return json.map(obj => Good.parse(obj))
    },

    add: async (orderId: number, goodGroupId: number): Promise<Order> => {
        const response = await request.fetch(`${API_ORDERS}/${orderId}/addgood`, {
            method: RequestMethods.POST,
            body: stringify({goodGroupId})
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    add2: async (orderId: number, good: Good): Promise<Order> => {
        const response = await request.fetch(`${API_ORDERS}/${orderId}/addgood`, {
            method: RequestMethods.POST,
            body: stringify({
                goodId: good.goodId,
                goodGroupId: good.goodGroupId,
                constructionId: good.constructionId
            })
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    getGroups: async (): Promise<GoodGroup[]> => {
        const response = await request.fetch(API_GOOD_GROUPS)
        const json = (await response.json()) as IGoodGroupJSON[]

        return json.map(obj => GoodGroup.parse(obj))
    }
}
