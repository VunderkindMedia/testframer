import {clients} from './clients'
import {orders} from './orders'
import {design} from './design'
import {booking} from './booking'
import {services} from './services'
import {checkout} from './checkout'
import {goods} from './goods'
import {partnerGoods} from './partner-goods'
import {installations} from './installations'
import {account} from './account'
import {affiliates} from './affiliates'
import {employees} from './employees'
import {dealers} from './dealers'
import {calendar} from './calendar'
import {cms} from './cms'
import {mountings} from './mountings'
import {settings} from './settings'
import {notifications} from './notifications'

export const api = {
    clients,
    orders,
    design,
    booking,
    services,
    checkout,
    goods,
    partnerGoods,
    installations,
    account,
    affiliates,
    employees,
    dealers,
    calendar,
    cms,
    mountings,
    settings,
    notifications
}
