import {ICalendarResponseJson} from '../model/framer/calendar/i-calendar-response-json'
import {stringify} from '../helpers/json'
import {API_CALENDAR} from '../constants/api'
import {ICalendarCustomEventJson, ICalendarEventJson} from '../model/framer/calendar/event'
import {request, RequestMethods} from './request'
import {ICalendarRequestEvent} from '../model/framer/calendar/i-calendar-request-event'

export const calendar = {
    getEvents: async (filters: string): Promise<ICalendarResponseJson> => {
        const response = await request.fetch(`${API_CALENDAR}${filters ?? ''}`)
        return (await response.json()) as ICalendarResponseJson
    },

    getEventsByOrderId: async (orderId: number, from: Date, to: Date): Promise<ICalendarResponseJson> => {
        const response = await request.fetch(`${API_CALENDAR}/${orderId}?from=${from.getTime()}&to=${to.getTime()}`)
        return (await response.json()) as ICalendarResponseJson
    },

    createEvent: async (event: ICalendarRequestEvent): Promise<ICalendarCustomEventJson | ICalendarEventJson> => {
        const response = await request.fetch(API_CALENDAR, {
            method: RequestMethods.POST,
            body: stringify(event)
        })

        return (await response.json()) as ICalendarCustomEventJson | ICalendarEventJson
    }
}
