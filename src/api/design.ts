import {request, RequestMethods} from './request'
import {
    API_GET_FILLINGS,
    API_GET_LAMINATION,
    API_GET_PROFILE,
    API_GET_PROFILES_AND_FURNITURE,
    API_MODEL_PARAMS,
    API_VISIBLE_MODEL_PARAMS
} from '../constants/api'
import {Furniture, IFurnitureJSON} from '../model/framer/furniture'
import {IProfileJSON, Profile} from '../model/framer/profile'
import {Filling, IFillingJSON} from '../model/framer/filling'
import {ILaminationJSON, Lamination} from '../model/framer/lamination'
import {ConnectorParameters, IConnectorParametersJSON} from '../model/framer/connector-parameters'
import {ModelParts} from '../model/framer/model-parts'
import {Construction} from '../model/framer/construction'
import {IUserParameterConfigJSON, UserParameterConfig} from '../model/framer/user-parameter/user-parameter-config'
import {
    IUserParameterConstructionConfigJSON,
    UserParameterConstructionConfig
} from '../model/framer/user-parameter/user-parameter-construction-config'

export const design = {
    // prettier-ignore
    getProfilesAndFurniture: async (constructionType: number, productType: number): Promise<{profiles: Profile[], furniture: Furniture[]}> => {
        const response = await request.fetch(API_GET_PROFILES_AND_FURNITURE(constructionType, productType))
        const json = await response.json() as {profiles: IProfileJSON[], hardwares: IFurnitureJSON[]}

        return {
            profiles: json.profiles.map(p => Profile.parse(p)),
            furniture: json.hardwares.map(f => Furniture.parse(f))
        }
    },

    getFillings: async (profileId: number): Promise<Filling[]> => {
        const response = await request.fetch(API_GET_FILLINGS(profileId))
        const json = (await response.json()) as {results: IFillingJSON[]}

        // TODO: remove filtering duplicates
        // return json.results.map(obj => Filling.parse(obj))
        const res = json.results.map(obj => Filling.parse(obj))
        const r2: Filling[] = []
        res.map(r => {
            if (r2.findIndex(i => i.id === r.id) === -1) {
                r2.push(r)
            }
        })
        return r2
    },

    getLamination: async (): Promise<Lamination[]> => {
        const response = await request.fetch(API_GET_LAMINATION)
        const json = (await response.json()) as ILaminationJSON[]

        return json.map(obj => Lamination.parse(obj))
    },

    getConnectorsParameters: async (profileId: number): Promise<ConnectorParameters[]> => {
        const response = await request.fetch(`${API_GET_PROFILE(profileId)}/connectors`)
        const json = (await response.json()) as {results: IConnectorParametersJSON[]}

        return json.results.map(res => ConnectorParameters.parse(res))
    },

    getModelParams: async (construction: Construction, modelPart: ModelParts, productOrLeafGuid: string): Promise<UserParameterConfig[]> => {
        const response = await request.fetch(`${API_MODEL_PARAMS(modelPart, productOrLeafGuid)}`, {
            method: RequestMethods.POST,
            body: JSON.stringify(construction)
        })
        const groups = (await response.json()) as IUserParameterConfigJSON[]
        const params = groups.map(group => UserParameterConfig.parse(group))
        return params.filter(p => p.id)
    },

    getVisibleModelParams: async (construction: Construction, cityId: number): Promise<UserParameterConstructionConfig[]> => {
        const response = await request.fetch(`${API_VISIBLE_MODEL_PARAMS(cityId)}`, {
            method: RequestMethods.POST,
            body: JSON.stringify(construction)
        })
        const config = (await response.json()) as IUserParameterConstructionConfigJSON[]
        return config.map(c => UserParameterConstructionConfig.parse(c))
    }
}
