import {request, RequestMethods} from './request'
import {API_NOTIFICATIONS, API_NOTIFICATIONS_READ, API_NOTIFICATIONS_UNREAD, API_NOTIFICATIONS_READ_ALL} from '../constants/api'
import {TNotificationList, NotificationList} from '../model/framer/notifications/notification-list'
import {Notification, INotificationJSON} from '../model/framer/notifications/notification'

export const notifications = {
    get: async (): Promise<NotificationList> => {
        const response = await request.fetch(`${API_NOTIFICATIONS}?limit=${Number.MAX_SAFE_INTEGER}`)
        const json = (await response.json()) as TNotificationList

        return NotificationList.parse(json)
    },

    markAllAsRead: async (): Promise<NotificationList> => {
        const response = await request.fetch(API_NOTIFICATIONS_READ_ALL, {method: RequestMethods.POST})
        const json = (await response.json()) as TNotificationList

        return NotificationList.parse(json)
    },

    markAsRead: async (id: string): Promise<Notification> => {
        const response = await request.fetch(API_NOTIFICATIONS_READ(id), {method: RequestMethods.POST})
        const json = (await response.json()) as INotificationJSON

        return Notification.parse(json)
    },

    markAsUnread: async (id: string): Promise<Notification> => {
        const response = await request.fetch(API_NOTIFICATIONS_UNREAD(id), {method: RequestMethods.POST})
        const json = (await response.json()) as INotificationJSON

        return Notification.parse(json)
    }
}
