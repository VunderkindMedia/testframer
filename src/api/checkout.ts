import {request, RequestMethods} from './request'
import {API_CHECKOUT, API_DEPOSIT, API_RESERVE, API_SHIPPING_CALC, API_SHIPPING_INFO, API_PAYMENTS} from '../constants/api'
import {IOrderJSON, Order} from '../model/framer/order'
import {ShippingTypes} from '../model/framer/shipping-types'
import {Address} from '../model/address'
import {ContactInfo} from '../model/framer/contact-info'
import {IShippingResponse} from '../model/framer/i-shipping-response'
import {IPaymentResultJSON, PaymentResult} from '../model/framer/payment-result'
import {Coordinates} from '../model/coordinates'
import {IPaymentStateJSON, PaymentState} from '../model/framer/payment-state'
import {TPaymentHistoryJSON, PaymentHistory} from '../model/framer/payment-history'
import {stringify} from '../helpers/json'

export const checkout = {
    reserve: async (ids: number[], reserveBonusScore: number, isFramerPointsShouldBeUsed: boolean): Promise<Order> => {
        const response = await request.fetch(API_RESERVE, {
            method: RequestMethods.POST,
            body: stringify({ids, reserveBonusScore, isFramerPointsShouldBeUsed})
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    saveShippingInfo: async (
        orderId: number,
        warehouseId: number,
        shippingType: ShippingTypes = ShippingTypes.Pickup,
        address: Address | null = null,
        contactPerson: ContactInfo | null = null,
        shippingComment: string | null = null
    ): Promise<void> => {
        await request.fetch(API_SHIPPING_INFO(orderId), {
            method: RequestMethods.POST,
            body: stringify({
                warehouseId,
                shippingType,
                address,
                dadataAddress: address?.dadataAddress ?? null,
                contactPerson,
                shippingComment
            })
        })
    },

    calcShipping: async (
        orderId: number,
        destination: Coordinates,
        address: Address,
        shippingFloor: number,
        oversizedProducts: number
    ): Promise<IShippingResponse> => {
        const response = await request.fetch(API_SHIPPING_CALC, {
            method: RequestMethods.POST,
            body: stringify({
                orderId,
                destination,
                address,
                shippingFloor,
                oversizedProducts
            })
        })
        return (await response.json()) as IShippingResponse
    },

    checkout: async (reservationId: number): Promise<PaymentState> => {
        const response = await request.fetch(`${API_CHECKOUT}/${reservationId}`)
        const json = (await response.json()) as IPaymentStateJSON

        return PaymentState.parse(json)
    },

    deposit: async (amount: number): Promise<PaymentResult> => {
        const response = await request.fetch(API_DEPOSIT, {
            method: RequestMethods.POST,
            body: stringify({
                amount
            })
        })
        const json = (await response.json()) as IPaymentResultJSON

        return PaymentResult.parse(json)
    },

    payments: async (filter: string): Promise<PaymentHistory> => {
        const response = await request.fetch(`${API_PAYMENTS}${filter}&op=1,-1`)
        const json = (await response.json()) as TPaymentHistoryJSON

        return PaymentHistory.parse(json)
    },

    paymentsCsv: async (filter: string): Promise<string> => {
        const response = await request.fetch(`${API_PAYMENTS}${filter}&op=1,-1&export=csv`)

        return await response.text()
    }
}
