import {request, RequestMethods} from './request'
import {IInstallationJSON, Installation} from '../model/framer/installation'
import {ConstructionCost, IConstructionCostJSON} from '../model/framer/construction-cost'
import {API_INSTALLATIONS, API_CONSTRUCTION} from '../constants/api'
import {stringify} from '../helpers/json'

export const installations = {
    get: async (): Promise<Installation[]> => {
        const response = await request.fetch(API_INSTALLATIONS)
        const json = (await response.json()) as IInstallationJSON[]

        return json.map(item => Installation.parse(item))
    },

    update: async (construction: ConstructionCost): Promise<ConstructionCost> => {
        const {id} = construction
        const response = await request.fetch(API_CONSTRUCTION(id), {
            method: RequestMethods.POST,
            body: stringify(construction)
        })
        const json = (await response.json()) as IConstructionCostJSON

        return ConstructionCost.parse(json)
    }
}
