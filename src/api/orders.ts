import {request, RequestMethods} from './request'
import {IOrdersResponseJSON, OrdersResponse} from '../model/framer/orders-response'
import {IOrderJSON, Order} from '../model/framer/order'
import {Discount} from '../model/framer/discount'
import {API_ORDERS, API_ORDERS_SAVE, API_GET_ORDER, API_ORDERS_WITH_PARTNER_GOODS, API_ORDERS_DUPLICATE, SERVER} from '../constants/api'
import {stringify} from '../helpers/json'

export const orders = {
    getByFilter: async (ordersFilter: string): Promise<OrdersResponse> => {
        const response = await request.fetch(`${API_ORDERS}${ordersFilter}`)
        const json = (await response.json()) as IOrdersResponseJSON

        return OrdersResponse.parse(json)
    },

    getById: async (orderId: number | string): Promise<Order> => {
        const url = isNaN(+orderId) ? `${SERVER}/orders/${orderId}` : API_GET_ORDER(orderId as number)
        const response = await request.fetch(url)
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    getWithPartnerGoods: async (ordersFilter: string): Promise<OrdersResponse> => {
        const response = await request.fetch(`${API_ORDERS_WITH_PARTNER_GOODS}${ordersFilter}`)
        const json = (await response.json()) as IOrdersResponseJSON

        return OrdersResponse.parse(json)
    },

    create: async (draftOrder: Order): Promise<Order> => {
        const requestObj = {
            constructions: draftOrder.preparedForServer.constructions
        }
        const response = await request.fetch(API_ORDERS, {
            method: RequestMethods.POST,
            body: stringify(requestObj)
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    recalculate: async (draftOrder: Order, isFramerPointsShouldBeUsed: boolean): Promise<Order> => {
        const preparedOrder = draftOrder.preparedForServer
        const requestObj = {
            orderInfo: {
                orderId: preparedOrder.id
            },
            constructions: preparedOrder.constructions,
            goods: preparedOrder.goods,
            services: preparedOrder.services,
            userActions: preparedOrder.userActions,
            isFramerPointsShouldBeUsed
        }
        const response = await request.fetch(`${API_ORDERS}/${preparedOrder.id}`, {
            method: RequestMethods.POST,
            body: stringify(requestObj)
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    delete: async (orderId: number | string): Promise<boolean> => {
        await request.fetch(`${API_ORDERS}/${orderId}`, {
            method: RequestMethods.DELETE
        })

        return true
    },

    setClient: async (orderId: number, clientId: number): Promise<Order> => {
        const response = await request.fetch(`${API_ORDERS}/${orderId}/client`, {
            method: RequestMethods.POST,
            body: stringify({clientId})
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    updateDiscount: async (orderId: number, discount: Discount): Promise<Order> => {
        const response = await request.fetch(`${API_ORDERS}/${orderId}/discount`, {
            method: RequestMethods.POST,
            body: stringify(discount)
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    updateMountingDate: async (orderId: number, mountingDate: string): Promise<Order> => {
        const response = await request.fetch(`${API_ORDERS}/${orderId}/mounting`, {
            method: RequestMethods.POST,
            body: stringify({mountingDate})
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    duplicateOrder: async (orderId: number): Promise<Order> => {
        const response = await request.fetch(API_ORDERS_DUPLICATE(orderId), {
            method: RequestMethods.POST
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    },

    save: async (draftOrder: Order): Promise<Order> => {
        const {id, constructions, goods, services, userActions} = draftOrder.preparedForServer
        const requestObj = {
            orderInfo: {
                orderId: id
            },
            constructions,
            goods,
            services,
            userActions
        }
        const response = await request.fetch(API_ORDERS_SAVE(id), {
            method: RequestMethods.POST,
            body: stringify(requestObj)
        })
        const json = (await response.json()) as IOrderJSON

        return Order.parse(json)
    }
}
