import {request, RequestMethods} from './request'
import {API_PARTNER_GOOD_GROUPS, API_PARTNER_GOODS, API_PARTNER_GOODS_TEMPLATES} from '../constants/api'
import {Good, IGoodJSON} from '../model/framer/good'
import {GoodGroup, IGoodGroupJSON} from '../model/framer/good-group'
import {stringify} from '../helpers/json'

export const partnerGoods = {
    get: async (): Promise<Good[]> => {
        const response = await request.fetch(API_PARTNER_GOODS)
        const json = (await response.json()) as IGoodJSON[]

        return json.map(obj => Good.parse(obj))
    },

    getGroups: async (): Promise<GoodGroup[]> => {
        const response = await request.fetch(API_PARTNER_GOOD_GROUPS)
        const json = (await response.json()) as IGoodGroupJSON[]

        return json.map(obj => GoodGroup.parse(obj))
    },

    getTemplates: async (): Promise<Good[]> => {
        const response = await request.fetch(API_PARTNER_GOODS_TEMPLATES)
        const json = (await response.json()) as IGoodJSON[]

        return json.map(obj => Good.parse(obj))
    },

    save: async (good: Good): Promise<Good> => {
        const response = await request.fetch(API_PARTNER_GOODS, {
            method: RequestMethods.POST,
            body: stringify({...good})
        })
        const json = (await response.json()) as IGoodJSON

        return Good.parse(json)
    },

    remove: async (id: number): Promise<Response> => {
        return await request.fetch(`${API_PARTNER_GOODS}/${id}`, {
            method: RequestMethods.DELETE
        })
    }
}
