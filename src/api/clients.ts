import {ClientsResponse, IClientsResponseJSON} from '../model/framer/clients-response'
import {request, RequestMethods} from './request'
import {Client, IClientJSON} from '../model/framer/client'
import {Order} from '../model/framer/order'
import {OrdersResponse, IOrdersResponseJSON} from '../model/framer/orders-response'
import {API_CLIENTS, API_CLIENT, API_CLIENT_ORDERS} from '../constants/api'
import {stringify} from '../helpers/json'

export const clients = {
    get: async (clientsFilter: string): Promise<ClientsResponse> => {
        const response = await request.fetch(`${API_CLIENTS}${clientsFilter}`)
        const json = (await response.json()) as IClientsResponseJSON

        return ClientsResponse.parse(json)
    },

    getById: async (id: string): Promise<Client> => {
        const response = await request.fetch(API_CLIENT(id))
        const json = (await response.json()) as IClientJSON

        return Client.parse(json)
    },

    create: async (client: Client): Promise<Client> => {
        const response = await request.fetch(API_CLIENTS, {
            method: RequestMethods.POST,
            body: stringify(client)
        })
        const json = (await response.json()) as IClientJSON

        return Client.parse(json)
    },

    update: async (client: Client): Promise<Client> => {
        const response = await request.fetch(`${API_CLIENTS}/${client.id}`, {
            method: RequestMethods.POST,
            body: stringify(client)
        })
        const json = (await response.json()) as IClientJSON

        return Client.parse(json)
    },

    getClientOrders: async (id: string): Promise<Order[]> => {
        const response = await request.fetch(API_CLIENT_ORDERS(id))
        const json = (await response.json()) as IOrdersResponseJSON
        const ordersResponse = OrdersResponse.parse(json)

        return ordersResponse.orders
    },

    deleteClient: async (id: string): Promise<boolean> => {
        const response = await request.fetch(API_CLIENT(id), {method: RequestMethods.DELETE})
        return response.status === 200
    }
}
