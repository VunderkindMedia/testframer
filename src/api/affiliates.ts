import {request, RequestMethods} from './request'
import {API_AFFILIATES, API_AFFILIATES_MARGIN} from '../constants/api'
import {Affiliate, IAffiliateJSON} from '../model/framer/affiliate'
import {Margin} from '../model/framer/margin'
import {AffiliateMarginTypes} from '../model/affiliate-margin-types'

export const affiliates = {
    get: async (): Promise<Affiliate[]> => {
        const response = await request.fetch(API_AFFILIATES)
        const json = (await response.json()) as {results: IAffiliateJSON[]}

        return json.results.map(item => Affiliate.parse(item))
    },

    create: async (name: string): Promise<Affiliate> => {
        const response = await request.fetch(API_AFFILIATES, {
            method: RequestMethods.POST,
            body: JSON.stringify({name})
        })
        const json = (await response.json()) as IAffiliateJSON

        return Affiliate.parse(json)
    },

    update: async (id: number, name: string, isExternal: boolean): Promise<Affiliate> => {
        const response = await request.fetch(`${API_AFFILIATES}/${id}`, {
            method: RequestMethods.POST,
            body: JSON.stringify({name, isExternal})
        })
        const json = (await response.json()) as IAffiliateJSON

        return Affiliate.parse(json)
    },

    delete: async (id: number): Promise<boolean> => {
        const response = await request.fetch(`${API_AFFILIATES}/${id}`, {
            method: RequestMethods.DELETE
        })
        return response.ok
    },

    updateMargin: async (id: number, margin: Margin, type: AffiliateMarginTypes): Promise<Affiliate> => {
        const response = await request.fetch(API_AFFILIATES_MARGIN(id, type), {method: RequestMethods.POST, body: JSON.stringify(margin)})
        const json = (await response.json()) as IAffiliateJSON
        return Affiliate.parse(json)
    }
}
