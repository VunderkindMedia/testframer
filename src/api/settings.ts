import {request} from './request'
import {API_APP_VERSION} from '../constants/api'
import {IAppVersionJSON, AppVersion} from '../model/app-version'

export const settings = {
    getAppVersion: async (): Promise<AppVersion> => {
        const response = await request.fetch(API_APP_VERSION)
        const json = (await response.json()) as IAppVersionJSON

        return AppVersion.parse(json)
    }
}
