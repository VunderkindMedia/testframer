import {SessionManager} from '../session-manager'
import {Store} from 'redux'
import {IAppState} from '../redux/root-reducer'

let instance: Request | null = null

export enum RequestMethods {
    GET = 'GET',
    POST = 'POST',
    DELETE = 'DELETE'
}

export interface IRequestOptions {
    method?: RequestMethods
    body?: string
    headers?: {[key: string]: string}
}

class Request {
    private _store!: Store<IAppState>
    private _defaultOptions: IRequestOptions = {
        method: RequestMethods.GET
    }

    constructor() {
        if (!instance) {
            instance = this
        }

        return instance
    }

    init(store: Store<IAppState>): void {
        this._store = store
    }

    fetch = async (url: string, options?: IRequestOptions, needAuth = true): Promise<Response> => {
        options = {
            ...this._defaultOptions,
            ...options
        }

        const token = new URLSearchParams(this._store.getState().router.location.search).get('t')
        if (needAuth) {
            options.headers = options.headers || {}
            options.headers['Authorization'] = `Bearer ${this._store.getState().account.token ?? token}`
        }

        const response = await fetch(url, options)

        const shouldRepeat = await SessionManager.checkSession(response)
        if (shouldRepeat) {
            if (needAuth) {
                options.headers = options.headers || {}
                options.headers['Authorization'] = `Bearer ${this._store.getState().account.token ?? token}`
            }
            return await fetch(url, options)
        }

        return response
    }
}

export const request = new Request()
