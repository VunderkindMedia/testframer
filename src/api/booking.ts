import {request, RequestMethods} from './request'
import {BookingOptions, IBookingOptionsJSON} from '../model/framer/booking-options'
import {ShippingTypes} from '../model/framer/shipping-types'
import {Address} from '../model/address'
import {Order, IOrderJSON} from '../model/framer/order'
import {ContactInfo} from '../model/framer/contact-info'
import {API_ORDERS_BOOK, API_ORDERS_BOOKING_OPTIONS, API_ORDERS_BOOKING_DATES, API_ORDERS_UNBOOK} from '../constants/api'
import {stringify} from '../helpers/json'
import {ISBookingDates} from '../model/framer/i-booking-dates'

export const booking = {
    getOptions: async (orderId: number): Promise<BookingOptions> => {
        const response = await request.fetch(API_ORDERS_BOOKING_OPTIONS(orderId))
        const json = (await response.json()) as IBookingOptionsJSON

        return BookingOptions.parse(json)
    },

    getDates: async (orderId: number): Promise<ISBookingDates> => {
        const response = await request.fetch(API_ORDERS_BOOKING_DATES(orderId))

        return (await response.json()) as ISBookingDates
    },

    book: async (
        orderId: number,
        date: string,
        warehouseId: number,
        shippingType: ShippingTypes = ShippingTypes.Pickup,
        address: Address | null = null,
        contactPerson: ContactInfo | null = null
    ): Promise<boolean> => {
        const response = await request.fetch(API_ORDERS_BOOK(orderId), {
            method: RequestMethods.POST,
            body: stringify({
                date,
                warehouseId,
                shippingType,
                address,
                dadataAddress: address?.dadataAddress ?? null,
                contactPerson
            })
        })

        return response.status === 200
    },

    unbook: async (orderId: number): Promise<Order | null> => {
        const response = await request.fetch(API_ORDERS_UNBOOK(orderId), {
            method: RequestMethods.POST
        })

        if (response.ok) {
            const json = (await response.json()) as IOrderJSON
            return Order.parse(json)
        }
        return null
    }
}
