import {API_DOCS_NEWS} from '../constants/api'
import {INewsJSON, News} from '../model/framer/news'
import {CMSManager} from '../cms-manager'

export const cms = {
    getNews: async (): Promise<News[]> => {
        const response = await CMSManager.fetch(API_DOCS_NEWS)
        const json = (await response.json()) as INewsJSON[]

        return json.map(item => News.parse(item))
    }
}
