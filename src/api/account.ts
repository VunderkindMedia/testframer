import {request, RequestMethods} from './request'
import {
    API_ACCOUNT,
    API_GOODS_MARGIN,
    API_MARGIN,
    API_OFFER_FIELDS,
    API_PROFILE_SETTINGS,
    API_SELECT_DEALER,
    API_REGIONS,
    API_REGISTER,
    API_FORGOT_PASSWORD,
    API_RESET_PASSWORD,
    API_LOGIN,
    API_REFRESH,
    API_PROFILE_NEWS
} from '../constants/api'
import {IPartnerJSON, Partner} from '../model/framer/partner'
import {Margin} from '../model/framer/margin'
import {Dealer, IDealerJSON} from '../model/framer/dealer'
import {Account, IAccountJSON} from '../model/framer/account'
import {IRegionJSON, Region} from '../model/framer/region'
import {User} from '../model/framer/user'
import {ITokenJSON, IToken} from '../model/token'
import {stringify} from '../helpers/json'

export const account = {
    get: async (): Promise<Account> => {
        const response = await request.fetch(API_ACCOUNT)
        const json = (await response.json()) as IAccountJSON

        return Account.parse(json)
    },

    getPartner: async (): Promise<Partner> => {
        const response = await request.fetch(API_PROFILE_SETTINGS)
        const json = (await response.json()) as IPartnerJSON

        return Partner.parse(json)
    },

    updateMargin: async (margin: Margin): Promise<Dealer> => {
        const response = await request.fetch(API_MARGIN, {
            method: RequestMethods.POST,
            body: stringify(margin)
        })
        const json = (await response.json()) as IDealerJSON

        return Dealer.parse(json)
    },

    setOfferFields: async (line1: string, line2: string): Promise<Partner> => {
        const response = await request.fetch(API_OFFER_FIELDS, {
            method: RequestMethods.POST,
            body: stringify({
                line1,
                line2
            })
        })
        const json = (await response.json()) as IPartnerJSON

        return Partner.parse(json)
    },

    setCurrentNewsId: async (lastReadNewsId: number): Promise<number> => {
        const response = await request.fetch(API_PROFILE_NEWS, {
            method: RequestMethods.POST,
            body: stringify({lastReadNewsId})
        })
        const json = await response.json()

        return JSON.parse(json)
    },

    updateGoodsMargin: async (margin: Margin): Promise<Dealer> => {
        const response = await request.fetch(API_GOODS_MARGIN, {
            method: RequestMethods.POST,
            body: stringify(margin)
        })
        const json = (await response.json()) as IDealerJSON

        return Dealer.parse(json)
    },

    setCurrentDealer: async (dealerId: number): Promise<Dealer> => {
        const response = await request.fetch(API_SELECT_DEALER(dealerId), {
            method: RequestMethods.POST
        })
        const json = (await response.json()) as IDealerJSON

        return Dealer.parse(json)
    },

    getRegions: async (): Promise<Region[]> => {
        const response = await request.fetch(API_REGIONS, {}, false)
        const json = (await response.json()) as {results: IRegionJSON[]}

        return json.results.map(region => Region.parse(region))
    },

    register: async (user: User, utm?: string): Promise<string> => {
        const response = await request.fetch(
            `${API_REGISTER}${utm ? `?${utm}` : ''}`,
            {
                method: RequestMethods.POST,
                body: stringify(user)
            },
            false
        )

        const text = await response.text()

        return response.ok ? '' : text
    },

    forgotPassword: async (email: string): Promise<string> => {
        const response = await request.fetch(
            API_FORGOT_PASSWORD,
            {
                method: RequestMethods.POST,
                body: stringify({email})
            },
            false
        )

        switch (response.status) {
            case 200:
                return ''
            case 208:
                return 'Запрос на восстановление пароля уже был отправлен'
            case 404:
                return 'Пользователь не найден'
            default: {
                const json = (await response.json()) as {error: string}
                return json.error
            }
        }
    },

    resetPassword: async (resetCode: string, password: string, confirmPassword: string): Promise<string> => {
        const response = await request.fetch(
            API_RESET_PASSWORD,
            {
                method: RequestMethods.POST,
                body: stringify({resetCode, password, confirmPassword})
            },
            false
        )

        switch (response.status) {
            case 200:
                return ''
            case 404:
                return 'Пользователь не найден'
            default: {
                const json = (await response.json()) as {error: string}
                return json.error
            }
        }
    },

    login: async (username: string, password: string): Promise<IToken> => {
        const response = await fetch(API_LOGIN, {
            method: RequestMethods.POST,
            body: JSON.stringify({username, password})
        })
        const json = (await response.json()) as ITokenJSON
        return IToken.parse(json)
    },

    refreshToken: async (refreshToken: string | null): Promise<IToken | null> => {
        const response = await fetch(API_REFRESH, {
            method: RequestMethods.POST,
            // eslint-disable-next-line @typescript-eslint/naming-convention
            body: JSON.stringify({refresh_token: refreshToken})
        })
        if (response.status === 401) {
            return null
        }
        const json = (await response.json()) as ITokenJSON
        return IToken.parse(json)
    }
}
