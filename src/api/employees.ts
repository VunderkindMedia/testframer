import {request, RequestMethods} from './request'
import {API_EMPLOYEES} from '../constants/api'
import {Employee, IEmployeeJSON} from '../model/framer/employee'
import {stringify} from '../helpers/json'

export const employees = {
    get: async (): Promise<Employee[]> => {
        const response = await request.fetch(API_EMPLOYEES)
        const json = (await response.json()) as {results: IEmployeeJSON[]}

        return json.results.map(item => Employee.parse(item))
    },

    update: async (employee: Employee): Promise<Employee> => {
        const {id} = employee
        const url = id ? `${API_EMPLOYEES}/${id}` : API_EMPLOYEES
        const response = await request.fetch(url, {
            method: RequestMethods.POST,
            body: stringify(employee)
        })

        if (!response.ok) {
            throw await response.text()
        }

        const json = (await response.json()) as IEmployeeJSON

        return Employee.parse(json)
    },

    delete: async (id: number): Promise<boolean> => {
        const response = await request.fetch(`${API_EMPLOYEES}/${id}`, {
            method: RequestMethods.DELETE
        })
        return response.ok
    }
}
