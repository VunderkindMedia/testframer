import {request, RequestMethods} from './request'
import {API_SERVICES, API_SERVICES_2} from '../constants/api'
import {IServiceJSON, Service} from '../model/framer/service'
import {stringify} from '../helpers/json'

export const services = {
    add: async (service: Service): Promise<Service> => {
        const response = await request.fetch(API_SERVICES, {
            method: RequestMethods.POST,
            body: stringify(service)
        })
        const json = (await response.json()) as IServiceJSON

        return Service.parse(json)
    },

    remove: async (serviceId: number): Promise<Response> => {
        return await request.fetch(`${API_SERVICES}/${serviceId}`, {
            method: RequestMethods.DELETE
        })
    },

    update: async (service: Service): Promise<Service> => {
        const response = await request.fetch(`${API_SERVICES}/${service.id}`, {
            method: RequestMethods.POST,
            body: stringify(service)
        })
        const json = (await response.json()) as IServiceJSON

        return Service.parse(json)
    },

    getByOrderId: async (orderId: number): Promise<Service[]> => {
        const response = await request.fetch(`${API_SERVICES_2}/orders/${orderId}`)
        const json = (await response.json()) as IServiceJSON[]

        return json.map(s => Service.parse(s))
    },

    getByOrderIdAndConstructionId: async (orderId: number, constructionId: string): Promise<Service[]> => {
        const response = await request.fetch(`${API_SERVICES_2}/orders/${orderId}/constructions/${constructionId}`)
        const json = (await response.json()) as IServiceJSON[]

        return json.map(s => Service.parse(s))
    }
}
