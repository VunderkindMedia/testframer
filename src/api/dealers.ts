import {request, RequestMethods} from './request'
import {API_DEALERS, API_INN, API_DEALERS_SAVE} from '../constants/api'
import {Dealer, IDealerJSON} from '../model/framer/dealer'
import {DealerInfo, IDealerInfoJSON} from '../model/framer/dealer-info'
import {stringify} from '../helpers/json'

export const dealers = {
    getById: async (dealerId: number): Promise<Dealer> => {
        const response = await request.fetch(`${API_DEALERS}/${dealerId}`, {}, false)
        const json = (await response.json()) as IDealerJSON

        return Dealer.parse(json)
    },

    checkInn: async (inn: string): Promise<DealerInfo> => {
        const response = await request.fetch(`${API_INN}/${inn}`)
        const json = (await response.json()) as IDealerInfoJSON

        return DealerInfo.parse(json)
    },

    save: async (info: DealerInfo): Promise<Dealer | string> => {
        const response = await request.fetch(API_DEALERS_SAVE, {
            method: RequestMethods.POST,
            body: stringify(info)
        })

        if (response.status === 200) {
            const json = (await response.json()) as IDealerJSON
            return Dealer.parse(json)
        }
        if (response.status === 409) {
            return `Контрагент с ИНН ${info.inn} уже зарегистрирован, пожалуйста, обратитесь в службу поддержки для добавления контрагента.`
        }
        return 'Контрагент не был сохранен'
    }
}
