const loadScreenshots = async () => {
    const response = await fetch('/api/screenshots')
    return await response.json()
}

renderReport = async () => {
    const images = await loadScreenshots()
    const template = document.querySelector('.test-template')
    const main = document.querySelector('main')
    const fragment = document.createDocumentFragment()

    for (img in images) {
        const test = template.cloneNode(true)
        test.content.querySelector('.test__title').innerText = img
        test.content.querySelector('.test__screenshot--original').src = images[img].target
        if (images[img].new) {
            test.content.querySelector('.test__screenshot--new').src = images[img].new
        }
        if (images[img].diff) {
            test.content.querySelector('.test__screenshot--diff').src = images[img].diff
        }
        fragment.appendChild(test.content)
    }
    main.appendChild(fragment)
}

renderReport()
