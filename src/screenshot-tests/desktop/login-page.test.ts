import {Browser, Page} from 'puppeteer'
import {afterAllAction, beforeAllAction, DEFAULT_TIMEOUT} from '../../e2e-tests/helpers'
import {takeScreenShotAndCompare} from '../helpers'

jest.setTimeout(DEFAULT_TIMEOUT)

let browser: Browser
let page: Page

beforeAll(async () => {
    const res = await beforeAllAction()
    browser = res.browser
    page = res.page
})

describe('login page', () => {
    test(
        'login',
        async () => {
            await page.waitForSelector('[data-test="login-form"]')

            const html = await page.$eval('title', e => e.innerHTML)
            expect(html).toBe('Framer')
            await takeScreenShotAndCompare(page, 'login')
        },
        DEFAULT_TIMEOUT
    )
    test(
        'registration',
        async () => {
            await page.waitForSelector('[data-test="registration-link"]')
            await page.$eval('[data-test="registration-link"]', e => (e as HTMLElement).click())

            await page.waitForSelector('[data-test="registration-button"]')
            await page.waitForSelector('[data-test="password-input"]')
            await page.type('[data-test="password-input"]', '5Wr$Vrpid')
            await takeScreenShotAndCompare(page, 'registration')

            await page.waitForSelector('[data-test="password-input-visibility-button"]')
            await page.$eval('[data-test="password-input-visibility-button"]', e => (e as HTMLElement).click())
            await takeScreenShotAndCompare(page, 'registration-show-password')
        },
        DEFAULT_TIMEOUT
    )
})

afterAll(async () => await afterAllAction(browser))
