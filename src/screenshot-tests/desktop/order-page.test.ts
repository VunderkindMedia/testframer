import {Browser, Page} from 'puppeteer'
import {afterAllAction, beforeAllAction, waitForResponse, DEFAULT_TIMEOUT} from '../../e2e-tests/helpers'
import {takeScreenShotAndCompare} from '../helpers'
import {ORDER_TAB_ID} from '../../constants/navigation'
import {testLoginAction} from '../../e2e-tests/desktop/helpers'

jest.setTimeout(DEFAULT_TIMEOUT)

let browser: Browser
let page: Page
let orderNumber: string

beforeAll(async () => {
    const res = await beforeAllAction()
    browser = res.browser
    page = res.page
})

describe('orders', () => {
    it(
        'orders table',
        async () => {
            await testLoginAction(browser, page)
            await page.waitForSelector('[data-test="menu-nav-to-orders-page"]')
            await page.$eval('[data-test="menu-nav-to-orders-page"]', e => (e as HTMLElement).click())

            await page.waitForSelector('[data-test="orders-page-table"]')
            await takeScreenShotAndCompare(page, 'orders-table')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'orders filters',
        async () => {
            await page.waitForSelector('[data-test="orders-date-range-start"]')
            await page.$eval('[data-test="orders-date-range-start"]', e => (e as HTMLElement).click())
            await takeScreenShotAndCompare(page, 'orders-filter-date-start')

            await page.waitForSelector('[data-test="orders-date-range-end"]')
            await page.$eval('[data-test="orders-date-range-end"]', e => (e as HTMLElement).click())
            await takeScreenShotAndCompare(page, 'orders-filter-date-end')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'create order page',
        async () => {
            await page.waitForSelector('[data-test="new-order-button"]')
            await page.$eval('[data-test="new-order-button"]', element => (element as HTMLElement).click())

            await page.waitForSelector('[data-test="group-1"], [data-test="group-1__template-1"]')
            const dataTest = await page.$eval('[data-test="group-1"], [data-test="group-1__template-1"]', e => e.getAttribute('data-test'))
            if (dataTest === 'group-1') {
                await page.$eval('[data-test="group-1"]', element => (element as HTMLElement).click())
            }
            await takeScreenShotAndCompare(page, 'order-template')

            await page.waitForSelector('[data-test="group-1__template-1"]')
            await page.$eval('[data-test="group-1__template-1"]', element => (element as HTMLElement).click())

            await page.waitForSelector('[data-test^="construction"]')
            await page.waitForSelector('[data-test="svg-construction-drawer-container"]')
            await takeScreenShotAndCompare(page, 'order-construction')

            const attr = await page.$eval(`[data-test^="${ORDER_TAB_ID}"]`, e => e.getAttribute('data-test'))
            orderNumber = attr?.split(' ')[1] ?? ''
        },
        DEFAULT_TIMEOUT
    )
    it(
        'order tab',
        async () => {
            await page.waitForSelector(`[data-test^="${ORDER_TAB_ID}"]`)
            await page.$eval(`[data-test^="${ORDER_TAB_ID}"]`, e => (e as HTMLElement).click())

            await page.waitForSelector('[data-test="order-mounting-date"]')
            await page.$eval('[data-test="order-mounting-date"]', e => (e as HTMLElement).click())
            await takeScreenShotAndCompare(page, 'order-mounting-date')

            await page.waitForSelector('[data-test="order-discount-input"]')
            await page.focus('[data-test="order-discount-input"]')
            await takeScreenShotAndCompare(page, 'order-discount')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'recalculate order',
        async () => {
            await page.waitForSelector('[data-test="order-page-recalculate-button"]')
            await page.$eval('[data-test="order-page-recalculate-button"]', e => (e as HTMLElement).click())

            await waitForResponse(page, 'orders')

            await page.waitForSelector('[data-test="order-page-to-production-button"]')
            await page.$eval('[data-test="order-page-to-production-button"]', e => (e as HTMLElement).click())
        },
        DEFAULT_TIMEOUT
    )
    it(
        'order shipping',
        async () => {
            await page.waitForSelector('[data-test="booking-page-transfer-to-production-button"]')
            await takeScreenShotAndCompare(page, 'order-production-pickup')

            await page.waitForSelector('[data-test="segmented-control-item-1"]')
            await page.$eval('[data-test="segmented-control-item-1"]', e => (e as HTMLElement).click())
            await takeScreenShotAndCompare(page, 'order-production-delivery')

            await page.$eval('[data-test="segmented-control-item-0"]', e => (e as HTMLElement).click())
            await page.$eval('[data-test="booking-page-transfer-to-production-button"]', e => (e as HTMLElement).click())

            await page.waitForSelector('[data-test="booking-page-transfer-to-production-modal-button"]')
            await takeScreenShotAndCompare(page, 'order-confirm-production')

            await page.$eval('[data-test="booking-page-transfer-to-production-modal-button"]', e => (e as HTMLElement).click())
            await takeScreenShotAndCompare(page, 'order-production-process')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'order booking',
        async () => {
            await page.waitForSelector('[data-test="booking-page-date-stage"]')
            await takeScreenShotAndCompare(page, 'order-select-date')

            await page.waitForSelector('[data-test="date-picker-available-day"]')
            await page.$eval('[data-test="date-picker-available-day"]', e => (e as HTMLElement).click())

            await page.waitForSelector('[data-test="booking-page-date-stage-modal-book-button"]')
            await takeScreenShotAndCompare(page, 'order-select-date-confirm')
            await page.$eval('[data-test="booking-page-date-stage-modal-book-button"]', e => (e as HTMLElement).click())

            await page.waitForSelector(`.booked[data-test="order-table-row-${orderNumber}"]`)
            await takeScreenShotAndCompare(page, 'order-booking-message')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'order view mode',
        async () => {
            await page.$eval(`[data-test="order-table-row-${orderNumber}"] a`, e => (e as HTMLElement).click())
            await page.waitForSelector('[data-test="close-order-view-mode-modal"]')
            await takeScreenShotAndCompare(page, 'order-view-mode')
        },
        DEFAULT_TIMEOUT
    )
})

afterAll(async () => await afterAllAction(browser))
