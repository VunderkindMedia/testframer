import {Browser, Page} from 'puppeteer'
import {afterAllAction, beforeAllAction, DEFAULT_TIMEOUT} from '../../e2e-tests/helpers'
import {takeScreenShotAndCompare} from '../helpers'
import {data} from '../../e2e-tests/desktop/profile-page-data'
import {testLoginAction} from '../../e2e-tests/desktop/helpers'

jest.setTimeout(DEFAULT_TIMEOUT)

let browser: Browser
let page: Page

beforeAll(async () => {
    const res = await beforeAllAction()
    browser = res.browser
    page = res.page
})

const navigateToTab = async (page: Page, tabId: string) => {
    await page.waitForSelector(`[data-test="${tabId}"]`)
    await page.$eval(`[data-test="${tabId}"]`, e => (e as HTMLElement).click())
    await page.waitForSelector(`[data-test="${tabId}-content"]`)
}

describe('profile page', () => {
    it(
        'margins',
        async () => {
            await testLoginAction(browser, page)
            await page.waitForSelector('[data-test="profile-page"]')
            await page.$eval('[data-test="profile-page"]', e => (e as HTMLElement).click())
            await page.waitForSelector('[data-test="margins"]')
            await takeScreenShotAndCompare(page, 'margins')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'print-settings',
        async () => {
            await navigateToTab(page, 'print-settings')
            await takeScreenShotAndCompare(page, 'print-settings')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'additional-services',
        async () => {
            await navigateToTab(page, 'additional-services')
            await page.waitForSelector('[data-test="add-additional-service"]')

            await takeScreenShotAndCompare(page, 'additional-services')

            await page.$eval('[data-test="add-additional-service"]', e => (e as HTMLElement).click())
            await takeScreenShotAndCompare(page, 'additional-services-add-new')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'partner-goods',
        async () => {
            await navigateToTab(page, 'partner-goods')

            await page.waitForSelector('[data-test="add-goods"]')
            await page.$eval('[data-test="add-goods"]', e => (e as HTMLElement).click())

            await takeScreenShotAndCompare(page, 'partner-goods')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'affiliates',
        async () => {
            await navigateToTab(page, 'affiliate')
            await takeScreenShotAndCompare(page, 'affiliate')

            await page.waitForSelector('[data-test="profile-page-add-affiliate-button"]')
            await page.$eval('[data-test="profile-page-add-affiliate-button"]', e => (e as HTMLElement).click())
            await page.waitForSelector('[data-test="profile-page-add-employee-button"]')
            await page.$eval('[data-test="profile-page-add-employee-button"]', e => (e as HTMLElement).click())
            await takeScreenShotAndCompare(page, 'affiliate-add-new')
        },
        DEFAULT_TIMEOUT
    )
    it(
        'dealers',
        async () => {
            await navigateToTab(page, 'dealers')
            await page.waitForSelector('[data-test="add-dealer"]')

            await page.focus('[data-test="add-dealer"]')
            await takeScreenShotAndCompare(page, 'dealers')

            await page.$eval('[data-test="add-dealer"]', e => (e as HTMLElement).click())
            await takeScreenShotAndCompare(page, 'dealers-modal')

            await page.waitForSelector('[data-test="modal-dealer"]')
            await page.waitForSelector('[data-test="delaer-inn"]')
            await page.type('[data-test="delaer-inn"]', data.dealer.inn)

            await page.waitForSelector('[data-test="save-dealer"]:not(:disabled)')
            await takeScreenShotAndCompare(page, 'dealers-add-new')
        },
        DEFAULT_TIMEOUT
    )
})

afterAll(async () => await afterAllAction(browser))
