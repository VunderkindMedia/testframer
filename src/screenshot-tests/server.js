const express = require('express')
const server = express()
const fs = require('fs')
const path = require('path')
const parseArgs = require('minimist')

const port = 3000

const dir = `${__dirname}/${parseArgs(process.argv).mobile ? 'mobile' : 'desktop'}`

server.use(express.static(`${__dirname}/public`))
server.use('/static', express.static(`${dir}/screenshots`))
server.use('/static', express.static(`${dir}/screenshots-target`))

server.get('/', (req, res) => {
    res.sendFile(`${__dirname}/report.html`)
})

server.get('/api/screenshots', (req, res) => {
    getImages(`${dir}/screenshots-target`, (err, files) => {
        const images = {}
        files.forEach(img => {
            const title = img.slice(0, -4)
            const titleKey = title.replace(/-/g, ' ')
            images[titleKey] = {
                target: `static/${img}`
            }

            if (isFileExists(`${dir}/screenshots/${title}-new.png`)) {
                images[titleKey].new = `static/${title}-new.png`
            }

            if (isFileExists(`${dir}/screenshots/${title}-diff.png`)) {
                images[titleKey].diff = `static/${title}-diff.png`
            }
        })

        res.json(images)
    })
})

server.listen(port)
console.log(`Server is running on localhost:${port}`)

const getImages = (imageDir, cb) => {
    const fileType = '.png'
    const files = []

    fs.readdir(imageDir, (err, list) => {
        for (let i = 0; i < list.length; i++) {
            if (path.extname(list[i]) === fileType) {
                files.push(list[i])
            }
        }
        cb(err, files)
    })
}

const isFileExists = path => {
    try {
        return fs.existsSync(path)
    } catch (err) {
        console.log(err)
    }
}
