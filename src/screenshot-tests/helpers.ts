import {Page} from 'puppeteer'
import pixelmatch from 'pixelmatch'
import * as fs from 'fs'
import * as pngjs from 'pngjs'

const device = process.env.MOBILE ? 'mobile' : 'desktop'
const targetDir = `${__dirname}/${device}/screenshots-target`
const dir = `${__dirname}/${device}/screenshots`

export const takeScreenShotAndCompare = async (page: Page, title: string): Promise<void> => {
    if (process.env.TARGET) {
        await page.screenshot({path: `${targetDir}/${title}.png`})
        return
    }
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir)
    }
    await page.screenshot({path: `${dir}/${title}-new.png`})
    try {
        const PNG = pngjs.PNG
        const imgTarget = PNG.sync.read(fs.readFileSync(`${targetDir}/${title}.png`))
        const img = PNG.sync.read(fs.readFileSync(`${dir}/${title}-new.png`))
        const {width, height} = imgTarget
        const imgDiff = new PNG({width, height})

        pixelmatch(imgTarget.data, img.data, imgDiff.data, width, height, {threshold: 0.1})
        fs.writeFileSync(`${dir}/${title}-diff.png`, PNG.sync.write(imgDiff))
    } catch (err) {
        console.log(err)
    }
}
