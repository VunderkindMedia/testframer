import * as React from 'react'
import {applyMiddleware, createStore} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import {Provider} from 'react-redux'
import {createHashHistory} from 'history'
import {ConnectedRouter} from 'connected-react-router'
import {routerMiddleware} from 'connected-react-router'
import {createRootReducer} from './redux/root-reducer'
import {HistoryManager} from './history-manager'
import {SessionManager} from './session-manager'
import {CarrotQuestManager} from './carrot-quest-manager'
import {isPhone, isTablet} from './helpers/mobile'
import {CMSManager} from './cms-manager'
import {CentrifugeManager} from './centrifuge-manager'
import {ReactNode} from 'react'
import {request} from './api/request'

export interface IAppContext {
    isMobile: boolean
    isTablet: boolean
}

export const AppContext = React.createContext<IAppContext>({isMobile: isPhone(), isTablet: isTablet()})

const history = createHashHistory()
const store = createStore(createRootReducer(history), composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history))))

request.init(store)
HistoryManager.init(store)
CentrifugeManager.init(store)
SessionManager.init(store)
CarrotQuestManager.init(store)
CMSManager.init(store)

console.log(`Running app version from ${process.env.APP_VERSION}`)

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker
            .register('/sw.js')
            .then(registration => {
                console.log('SW registered: ', registration)
            })
            .catch(registrationError => {
                console.log('SW registration failed: ', registrationError)
            })
    })
}

export function App(props: {children?: ReactNode}): JSX.Element {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <AppContext.Provider value={{isMobile: isPhone(), isTablet: isTablet()}}>{props.children}</AppContext.Provider>
            </ConnectedRouter>
        </Provider>
    )
}
