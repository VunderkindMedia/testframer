import Centrifuge from 'centrifuge'
import {Dispatch, Store, bindActionCreators} from 'redux'
import {
    setAccountBalance,
    setNotification,
    setAccountCreditLimit,
    setBonusScore,
    setNextDiscount,
    setFramerPoints,
    setShowBalance,
    setAccountCreditLimitCents
} from './redux/account/actions'
import {checkAppVersion} from './redux/settings/actions'
import {IAppState} from './redux/root-reducer'
import {CentrifugeNotification, NotificationType} from './model/centrifuge-notification'
import {IAccountNotification} from './model/framer/notifications/account-notification'
import {AppVersion} from './model/app-version'
import {Notification} from './model/framer/notifications/notification'
import {BonusScore} from './model/framer/bonus-score'
import {loadAccount, loadNotifications} from './redux/account/actions'

let instance: CentrifugeManagerClass | null

export const CENTRIFUGE_SERVER = process.env.CENTRIFUGE

class CentrifugeManagerClass {
    private _centrifuge!: Centrifuge
    private _store!: Store<IAppState>
    private _accountTimerId?: ReturnType<typeof setInterval>
    private _versionTimerId?: ReturnType<typeof setInterval>
    private _notificationTimerId?: ReturnType<typeof setInterval>

    constructor() {
        if (!instance) {
            instance = this
        }

        return instance
    }

    init(store: Store<IAppState>): void {
        this._store = store
    }

    connect(): void {
        const token = this.state.account.token

        if (!token) {
            return
        }

        this._centrifuge = new Centrifuge(`${CENTRIFUGE_SERVER}/connection/websocket`)

        this._centrifuge.setToken(token)

        this._centrifuge.on('connect', () => {
            this._fallbackLoadingStop()
        })

        this._centrifuge.on('disconnect', () => {
            this._fallbackLoadingStart()
        })

        this._centrifuge.on('publish', ctx => {
            if (!ctx) {
                return
            }

            const notification = CentrifugeNotification.parse(ctx)
            if (!notification) {
                return
            }

            switch (notification.type) {
                case NotificationType.Account: {
                    const accountNotification = notification.value as IAccountNotification
                    const {
                        balance,
                        creditLimitInCents,
                        framerCreditLimitInCents,
                        potentialScore,
                        reservedScore,
                        availableScore,
                        earnedScore,
                        nextDiscount,
                        nextDiscountAmount,
                        framerPoints,
                        showBalance
                    } = accountNotification
                    this.dispatch(setAccountBalance(balance))
                    this.dispatch(setFramerPoints(framerPoints))
                    this.dispatch(setShowBalance(showBalance))
                    this.dispatch(setAccountCreditLimit(creditLimitInCents))
                    this.dispatch(setAccountCreditLimitCents(framerCreditLimitInCents))
                    this.dispatch(setBonusScore(new BonusScore(potentialScore, reservedScore, availableScore, earnedScore)))
                    this.dispatch(setNextDiscount(nextDiscount, nextDiscountAmount))
                    return
                }
                case NotificationType.Version: {
                    const checkAppVersionFn = bindActionCreators(checkAppVersion, this.dispatch)
                    checkAppVersionFn(notification.value as AppVersion)
                    return
                }
                case NotificationType.Notification: {
                    this.dispatch(setNotification(notification.value as Notification))
                }
            }
        })

        this._fallbackLoadingStart()
        this._centrifuge.connect()
    }

    disconnect(): void {
        this._centrifuge && this._centrifuge.disconnect()
        this._fallbackLoadingStop()
    }

    private _fallbackLoadingStart(): void {
        this._fallbackLoadingStop()

        if (!this.state.account.token) {
            return
        }

        const loadAccountFn = bindActionCreators(loadAccount, this.dispatch)
        loadAccountFn()
        this._accountTimerId = setInterval(loadAccountFn, 5 * 1000)

        const checkAppVersionFn = bindActionCreators(checkAppVersion, this.dispatch)
        checkAppVersionFn()
        this._versionTimerId = setInterval(() => checkAppVersionFn, 10 * 60 * 1000)

        const loadNotificationsFn = bindActionCreators(loadNotifications, this.dispatch)
        loadNotificationsFn()
        this._notificationTimerId = setInterval(loadNotificationsFn, 5 * 60 * 1000)
    }

    private _fallbackLoadingStop(): void {
        this._accountTimerId && clearInterval(this._accountTimerId)
        this._versionTimerId && clearInterval(this._versionTimerId)
        this._notificationTimerId && clearInterval(this._notificationTimerId)
    }

    private get state(): IAppState {
        return this._store.getState()
    }

    private get dispatch(): Dispatch {
        return this._store.dispatch
    }
}

export const CentrifugeManager = new CentrifugeManagerClass()
