import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {App} from './app'
import {DesktopApp} from './desktop-app'

ReactDOM.render(
    <App>
        <DesktopApp />
    </App>,
    document.getElementById('root') as HTMLElement
)
