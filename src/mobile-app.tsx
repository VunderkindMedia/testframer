import * as React from 'react'
import './desktop-app.css'
import './mobile-app.css'
import {Route} from 'react-router'
import {
    NAV_BOOKING,
    NAV_PROFILE,
    NAV_CLIENTS,
    NAV_CLIENT,
    NAV_ORDERS,
    NAV_PAYMENT,
    NAV_ROOT,
    NAV_ORDER,
    NAV_RESET_PASSWORD,
    NAV_FORGOT_PASSWORD,
    NAV_REGISTRATION,
    NAV_PARTNER_GOODS,
    NAV_START_TUTORIAL,
    NAV_MOUNTINGS,
    NAV_MOUNTING,
    NAV_EVENTS,
    NAV_LOGIN
} from './constants/navigation'
import {LoginPage} from './components/common/authorization-pages/login-page/login-page'
import {RegistrationPage} from './components/common/authorization-pages/registration-page/registration-page'
import {ForgotPasswordPage} from './components/common/authorization-pages/forgot-password-page/forgot-password-page'
import {ResetPasswordPage} from './components/common/authorization-pages/reset-password-page/reset-password-page'
import {PaymentPage} from './components/common/payment-page/payment-page'
import {IAppState} from './redux/root-reducer'
import {ProgressBar} from './components/common/progress-bar/progress-bar'
import {useSelector} from 'react-redux'
import {useEffect} from 'react'
import {Statusbar} from './components/common/statusbar/statusbar'
import {ProfilePage} from './components/mobile/profile-page/profile-page'
import {BuilderPageErrorBoundaryConnected} from './components/common/builder-page-error-boundary/builder-page-error-boundary'
import {OrdersPage} from './components/mobile/orders-page/orders-page'
import {BookingPage} from './components/common/booking-page/booking-page'
import {useUpdateScreenSizeDependencies, useInitCarrotQuest, useShowOnboarding} from './hooks/app'
import {Header} from './components/mobile/header/header'
import {useHideNavigation} from './hooks/navigation'
import {useResetScrollOnPathnameChange} from './hooks/ui'
import {OrderPage} from './components/mobile/order-page/order-page'
import {ClientsPage} from './components/mobile/clients-page/clients-page'
import {PartnerGoodsPage} from './components/mobile/partner-goods-page/partner-goods-page'
import {StartTutorialPage} from './components/common/start-tutorial-page/start-tutorial-page'
import {MountingPage} from './components/mobile/mounting-page/mounting-page'
import {MountingsPage} from './components/mobile/mountings-page/mountings-page'
import {ClientPage} from './components/mobile/client-page/client-page'
import {EventsPage} from './components/mobile/events-page/events-page'
import {usePermissions, useIsDemoUser} from './hooks/permissions'
import {Onboarding} from './components/common/onboarding/onboarding'
import {EnergyPage} from './components/common/energy-page/energy-page'

export function MobileApp(): JSX.Element {
    const windowSize = useSelector((state: IAppState) => state.browser.windowSize)
    const isVisible = useSelector((state: IAppState) => state.statusbar.isVisible)
    useUpdateScreenSizeDependencies()
    useInitCarrotQuest()
    useResetScrollOnPathnameChange()
    const isHideNavigation = useHideNavigation()
    const {isNonQualified} = usePermissions()
    const isDemoUser = useIsDemoUser()
    const showOnboarding = useShowOnboarding()

    useEffect(() => {
        const height = isNonQualified || isDemoUser ? 74 : 44
        document.documentElement.style.setProperty('--header-height', `${height}px`)
    }, [isNonQualified, isDemoUser])

    useEffect(() => {
        document.body.classList.add('mobile')

        return () => {
            document.body.classList.remove('mobile')
        }
    }, [])

    return (
        <div
            style={{minHeight: `${windowSize.height}px`}}
            className={`mobile-app ${isVisible && 'statusbar-visible'} ${isHideNavigation && 'hide-navigation'}`}>
            <Statusbar />
            {showOnboarding && <Onboarding />}
            {!isHideNavigation && <Header />}
            <div className="mobile-app__content">
                <Route exact path={NAV_ROOT} render={() => <LoginPage />} />
                <Route exact path={NAV_LOGIN} render={() => <LoginPage />} />
                <Route exact path={NAV_REGISTRATION} render={() => <RegistrationPage />} />
                <Route exact path={NAV_FORGOT_PASSWORD} render={() => <ForgotPasswordPage />} />
                <Route exact path={NAV_RESET_PASSWORD} render={() => <ResetPasswordPage />} />
                <Route path={NAV_PROFILE} render={() => <ProfilePage />} />
                <Route
                    path={NAV_ORDER}
                    render={() => (
                        <BuilderPageErrorBoundaryConnected>
                            <OrderPage />
                        </BuilderPageErrorBoundaryConnected>
                    )}
                />
                <Route path={NAV_ORDERS} render={() => <OrdersPage />} />
                <Route path={NAV_BOOKING} render={() => <BookingPage />} />
                <Route path={NAV_CLIENTS} render={() => <ClientsPage />} />
                <Route path={NAV_CLIENT} render={() => <ClientPage />} />
                <Route path={NAV_PAYMENT} render={() => <PaymentPage />} />
                <Route path={NAV_PARTNER_GOODS} render={() => <PartnerGoodsPage />} />
                <Route path={NAV_START_TUTORIAL} render={() => <StartTutorialPage />} />
                <Route path={NAV_MOUNTINGS} render={() => <MountingsPage />} />
                <Route path={NAV_MOUNTING} render={() => <MountingPage />} />
                <Route path={NAV_EVENTS} render={() => <EventsPage />} />
                <Route path={'/energy_*'} render={() => <EnergyPage />} />
            </div>
            {/*TODO: еще страницы мобилки*/}
            {/*<Route path={NAV_PAYMENT_RESULT} render={() => <PaymentResultPage/>} />*/}
            <ProgressBar />
        </div>
    )
}
