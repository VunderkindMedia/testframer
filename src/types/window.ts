import {ICarrotQuest} from './carrot-quest'

declare global {
    // eslint-disable-next-line @typescript-eslint/naming-convention,@typescript-eslint/no-unused-vars
    interface Window {
        carrotquest?: ICarrotQuest
        undo: () => void
        redo: () => void
        ym: (siteId: number, action: string, target: string) => void
        dataLayer: {
            push: (data?: unknown) => void
        }
    }
}
