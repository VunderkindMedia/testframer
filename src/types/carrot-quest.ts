export interface IICarrotQuestCallbackData {
    id: string
    body: string
}

export interface IICarrotQuestIdentifyData {
    [key: string]: string | boolean | number
}

export type TICarrotQuestCallbackTypes =
    | 'conversation_started'
    | 'messenger_opened'
    | 'messenger_closed'
    | 'user_replied'
    | 'conversation_opened'
    | 'event_tracked'

export type TCarrotQuestConfig = {
    settings: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        messenger_position: 'left_bottom' | 'right_bottom'
    }
}

interface ICarrotQuestData {
    user: {[key: string]: string}
}

export interface ICarrotQuest {
    onReady: (callback: () => void) => void
    addCallback: (topic: TICarrotQuestCallbackTypes, callback: (data: IICarrotQuestCallbackData) => void) => void
    removeChat: () => void
    auth: (userId: number, hash: string) => void
    identify: (data: IICarrotQuestIdentifyData | IICarrotQuestIdentifyData[]) => void
    open: () => void
    connect: (apiKey: string | undefined, config?: TCarrotQuestConfig) => void
    track: (eventName: string, data?: unknown) => void
    connected: boolean
    realtimeServices?: {[key: string]: () => void}
    data: ICarrotQuestData
}
