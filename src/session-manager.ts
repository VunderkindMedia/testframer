import {bindActionCreators, Dispatch, Store} from 'redux'
import {
    NAV_BILL,
    NAV_FORGOT_PASSWORD,
    NAV_KP,
    NAV_PARTNER_GOODS_REPORT,
    NAV_REGISTRATION,
    NAV_RESET_PASSWORD,
    NAV_ROOT,
    NAV_MOUNTING_REPORT,
    NAV_BUILDER
} from './constants/navigation'
import {loadLamination} from './redux/lamination/actions'
import {loadAccount, loadPartner, setAccountRefreshToken, setAccountToken, setUserPermissions} from './redux/account/actions'
import {checkAppVersion} from './redux/settings/actions'
import {loadFactoryGoodGroups, loadFactoryGoods, loadPartnerGoodGroups, loadPartnerGoods} from './redux/goods/actions'
import {push} from 'connected-react-router'
import {resetSession} from './redux/global-actions'
import {getDefaultOrdersLink} from './helpers/orders'
import {IAppState} from './redux/root-reducer'
import {clearCache} from './helpers/cache'
import {setUtm} from './helpers/account'
import {CentrifugeManager} from './centrifuge-manager'
import {api} from './api'
import {loadNews} from './redux/cms/actions'

let instance: SessionManagerClass | null

enum TokenStatus {
    Valid,
    Refreshing,
    Invalid
}

class SessionManagerClass {
    private _tokenStatus = TokenStatus.Valid
    private _store!: Store<IAppState>

    constructor() {
        if (!instance) {
            instance = this
        }

        return instance
    }

    init(store: Store<IAppState>): void {
        this._store = store

        setUtm(this.state.router.location.search)

        if ([NAV_REGISTRATION, NAV_FORGOT_PASSWORD, NAV_RESET_PASSWORD].includes(this.state.router.location.pathname)) {
            return
        }

        this.initSession()

        if (this.state.account.token && this.state.router.location.pathname === NAV_ROOT) {
            setTimeout(() => this.dispatch(push(getDefaultOrdersLink(this.state.orders.ordersOnPage))), 0)
        }

        const loadAccountFn = bindActionCreators(loadAccount, this.dispatch)
        loadAccountFn()

        const checkAppVersionFn = bindActionCreators(checkAppVersion, this.dispatch)
        checkAppVersionFn()
    }

    private _waitUntilTokenRefreshing(): Promise<void> {
        return new Promise(resolve => {
            if (this._tokenStatus !== TokenStatus.Refreshing) {
                resolve()
            }
            const timerId = setInterval(() => {
                if (this._tokenStatus !== TokenStatus.Refreshing) {
                    clearInterval(timerId)
                    resolve()
                }
            }, 100)
        })
    }

    async checkSession(response: Response): Promise<boolean> {
        if (
            this.state.router.location.pathname === NAV_KP ||
            this.state.router.location.pathname === NAV_BILL ||
            this.state.router.location.pathname === NAV_PARTNER_GOODS_REPORT ||
            this.state.router.location.pathname === NAV_MOUNTING_REPORT ||
            this.state.router.location.pathname === NAV_BUILDER
        ) {
            return false
        }

        if (response.status === 401) {
            try {
                if (this._tokenStatus === TokenStatus.Valid) {
                    this._tokenStatus = TokenStatus.Refreshing

                    const token = await api.account.refreshToken(this._store.getState().account.refreshToken)

                    if (!token) {
                        await this.resetSession()
                        this._tokenStatus = TokenStatus.Invalid
                        return false
                    }

                    this.dispatch(setUserPermissions(token.permissions))
                    this.dispatch(setAccountToken(token.accessToken))
                    this.dispatch(setAccountRefreshToken(token.refreshToken))
                    this._tokenStatus = TokenStatus.Valid
                } else {
                    await this._waitUntilTokenRefreshing()
                }

                return this._tokenStatus === TokenStatus.Valid
            } catch (e) {
                console.error(e)
            }
        }

        return false
    }

    initSession(): void {
        this._tokenStatus = TokenStatus.Valid
        bindActionCreators(loadLamination, this.dispatch)()
        bindActionCreators(loadNews, this.dispatch)()
        bindActionCreators(loadPartner, this.dispatch)()
        bindActionCreators(loadFactoryGoodGroups, this.dispatch)()
        bindActionCreators(loadFactoryGoods, this.dispatch)()
        bindActionCreators(loadPartnerGoodGroups, this.dispatch)()
        bindActionCreators(loadPartnerGoods, this.dispatch)()
        CentrifugeManager.connect()
    }

    async resetSession(path = NAV_ROOT): Promise<void> {
        this._tokenStatus = TokenStatus.Invalid
        this.dispatch(resetSession())
        if (this.state.router.location.pathname !== path) {
            this.dispatch(push(path))
        }

        await clearCache()
        CentrifugeManager.disconnect()
    }

    private get state(): IAppState {
        return this._store.getState()
    }

    private get dispatch(): Dispatch {
        return this._store.dispatch
    }
}

export const SessionManager = new SessionManagerClass()
