import {NAV_ORDER, NAV_ORDERS, NAV_PARTNER_GOODS, ORDER_TAB_ID} from '../constants/navigation'
import {AllOrderStates, OrderStates} from '../model/framer/order-states'
import {Order} from '../model/framer/order'
import {getDateYYYYMMDDFormat, getNextMonthDate, getPreviousMonthDate, parseDateFromString} from './date'
import {ORDER_FILTERS, GOODS_FILTERS, ORDER_COUNT} from '../constants/local-storage'
import {Construction} from '../model/framer/construction'
import {api} from '../api'
import {UserParameterConfig} from '../model/framer/user-parameter/user-parameter-config'
import {SolidFilling} from '../model/framer/solid-filling'
import {FrameFilling} from '../model/framer/frame-filling'
import {Connector} from '../model/framer/connector'
import {isDemoAccount} from '../helpers/account'
import {UserParameter} from '../model/framer/user-parameter/user-parameter'
import {DEFAULT_GLASS_COLOR} from '../constants/default-user-parameters'
import {Product} from '../model/framer/product'

export interface IOrdersFilterParams {
    states: OrderStates[]
    dateFrom: Date
    dateTo: Date
    orderNumber: string
    offset: number
    limit: number
    filials?: number[]
    managers?: number[]
    id?: string
    framePoints?: string
}

export const getOrderPageUrl = (orderId: string | number, tabs: string[], tabId: string = ORDER_TAB_ID): string => {
    if (!tabs.includes(tabId)) {
        tabs.push(tabId)
    }
    return `${NAV_ORDER}?orderId=${orderId}&tabs=${tabs.join(',')}&activeTab=${tabId}`
}

export const getOrderItemUrl = (orderId: string | number, activeTab: string | number): string => {
    return `${NAV_ORDER}?orderId=${orderId}&tabs=${activeTab}&activeTab=${activeTab}`
}

export const getOrdersFilter = (params: IOrdersFilterParams): string => {
    const {states, dateFrom, dateTo, id, orderNumber, offset, limit, filials, managers, framePoints} = params
    const st = states.join(',')
    const from = getDateYYYYMMDDFormat(dateFrom)
    const to = getDateYYYYMMDDFormat(dateTo)
    const fl = filials ? filials.join(',') : ''
    const ow = managers ? managers.join(',') : ''
    const calcType = framePoints ? framePoints : ''
    return `?st=${st}&from=${from}&to=${to}&num=${orderNumber}&fl=${fl}&ow=${ow}&calcType=${calcType}&offset=${offset}&limit=${limit}${
        id ? `&id=${id}` : ''
    }`
}

export const getOrdersFilterParams = (filter: string): IOrdersFilterParams => {
    const params = new URLSearchParams(filter)

    const from = params.get('from')
    const to = params.get('to')
    const fl = params.get('fl')
    const ow = params.get('ow')

    return {
        states: (params.get('st') ?? '').split(',').map(state => state as OrderStates),
        dateFrom: from ? parseDateFromString(from) : getPreviousMonthDate(new Date()),
        dateTo: to ? parseDateFromString(to) : new Date(),
        orderNumber: params.get('num') ?? '',
        offset: +(params.get('offset') ?? 0),
        limit: +(params.get('limit') ?? 2),
        id: params.get('id') ?? '',
        filials: fl ? fl.split(',').map(fl => Number(fl)) : [],
        managers: ow ? ow.split(',').map(ow => Number(ow)) : [],
        framePoints: params.get('calcType') ?? ''
    }
}

export const getResetOrdersFilterParams = (limit: number): IOrdersFilterParams => {
    return {
        states: AllOrderStates,
        dateFrom: getPreviousMonthDate(new Date()),
        dateTo: new Date(),
        orderNumber: '',
        offset: 0,
        limit,
        filials: [],
        managers: [],
        framePoints: ''
    }
}

export const getResetOrdersFilter = (limit: number): string => {
    localStorage.removeItem(ORDER_FILTERS)
    return getOrdersFilter(getResetOrdersFilterParams(limit))
}

export const getDefaultOrdersFilterParams = (limit: number): IOrdersFilterParams => {
    const defaultFilters = getResetOrdersFilterParams(limit)

    const orderFilters = localStorage.getItem(ORDER_FILTERS)

    if (orderFilters) {
        const parsedFilters = JSON.parse(orderFilters) as IOrdersFilterParams
        return {
            states: parsedFilters.states || defaultFilters.states,
            dateFrom: parsedFilters.dateFrom ? new Date(parsedFilters.dateFrom) : defaultFilters.dateFrom,
            dateTo: defaultFilters.dateTo,
            orderNumber: parsedFilters.orderNumber || defaultFilters.orderNumber,
            offset: defaultFilters.offset,
            limit: defaultFilters.limit,
            filials: parsedFilters.filials || defaultFilters.filials,
            managers: parsedFilters.managers || defaultFilters.managers
        }
    }

    return defaultFilters
}

export const getDefaultOrdersFilter = (limit: number): string => {
    return getOrdersFilter(getDefaultOrdersFilterParams(limit))
}

export const getResetGoodsFilterParams = (limit: number): IOrdersFilterParams => {
    return {
        states: AllOrderStates,
        dateFrom: new Date(),
        dateTo: getNextMonthDate(new Date()),
        orderNumber: '',
        offset: 0,
        limit
    }
}

export const getGoodsFilterParams = (filter: string): IOrdersFilterParams => {
    const params = new URLSearchParams(filter)

    const from = params.get('from')
    const to = params.get('to')

    return {
        states: (params.get('st') ?? '').split(',').map(state => state as OrderStates),
        dateFrom: from ? parseDateFromString(from) : new Date(),
        dateTo: to ? parseDateFromString(to) : getNextMonthDate(new Date()),
        orderNumber: params.get('num') ?? '',
        offset: +(params.get('offset') ?? 0),
        limit: +(params.get('limit') ?? 2),
        id: params.get('id') ?? ''
    }
}

export const getDefaultGoodsFilterParams = (limit: number): IOrdersFilterParams => {
    const defaultFilters = getResetGoodsFilterParams(limit)

    const goodsFilters = localStorage.getItem(GOODS_FILTERS)

    if (goodsFilters) {
        const parsedFilters = JSON.parse(goodsFilters) as IOrdersFilterParams
        return {
            ...defaultFilters,
            states: parsedFilters.states || defaultFilters.states,
            dateFrom: parsedFilters.dateFrom ? new Date(parsedFilters.dateFrom) : defaultFilters.dateFrom,
            orderNumber: parsedFilters.orderNumber || defaultFilters.orderNumber
        }
    }

    return defaultFilters
}

export const getDefaultGoodsFilter = (limit: number): string => {
    return getOrdersFilter(getDefaultGoodsFilterParams(limit))
}

export const getResetGoodsFilter = (limit: number): string => {
    localStorage.removeItem(GOODS_FILTERS)
    return getOrdersFilter(getResetGoodsFilterParams(limit))
}

export const getDefaultOrdersLink = (limit: number): string => `${NAV_ORDERS}${getDefaultOrdersFilter(limit)}`

export const getResetOrdersLink = (limit: number): string => `${NAV_ORDERS}${getResetOrdersFilter(limit)}`

export const getDefaultPartnerGoodsLink = (limit: number): string => `${NAV_PARTNER_GOODS}${getDefaultGoodsFilter(limit)}`

export const getResetPartnerGoodsLink = (limit: number): string => `${NAV_PARTNER_GOODS}${getResetGoodsFilter(limit)}`

export const getOrderLink = (order: Order): string => {
    return `${NAV_ORDER}?orderId=${order.id}${order.constructions.length ? `&tabs=${order.constructions.map(c => c.id).join(',')}` : ''}`
}
export const getConstructionTypedIndex = (construction: Construction, constructions: Construction[]): number => {
    const filter = (testConstruction: Construction): boolean => {
        if (construction.isMoskitka) {
            return testConstruction.isMoskitka
        } else if (construction.isEmpty) {
            return testConstruction.isEmpty
        }
        return true
    }

    return constructions.filter(filter).findIndex(c => c.id === construction.id)
}

export const getConstructionPosition = (construction: Construction, constructions: Construction[]): number => {
    if (construction.position) {
        return construction.position
    }
    return getConstructionTypedIndex(construction, constructions) + 1
}

export const loadUserParameters = async (
    construction: Construction,
    cityId?: number | undefined
): Promise<{[guid: string]: UserParameterConfig[]}> => {
    if (!cityId) {
        return {}
    }
    const availableUserParameterConfigs = await api.design.getVisibleModelParams(construction, cityId)
    const configMap: {[guid: string]: UserParameterConfig[]} = {}
    availableUserParameterConfigs.map(productConfig =>
        productConfig.partUserParameters.map(({partGuid, userParameters}) => (configMap[partGuid] = userParameters))
    )
    return configMap
}

export const getConstructionWithVisibleUserParams = (
    construction: Construction,
    configMap: {[guid: string]: UserParameterConfig[]} | undefined,
    handleParams: (filling: FrameFilling | SolidFilling | Product | Connector, params: UserParameterConfig[] | undefined) => void
): Construction => {
    if (!configMap) {
        return construction
    }
    construction.products.map(p => {
        const solidFillings: SolidFilling[] = []
        const leafFillings: FrameFilling[] = []

        handleParams(p, configMap[p.productGuid])

        p.connectors.forEach(connector => {
            handleParams(connector, configMap[connector.beamGuid])
        })

        p.frame.innerFillings.forEach(innerFilling => {
            innerFilling.solid && solidFillings.push(innerFilling.solid)
            innerFilling.leaf && leafFillings.push(innerFilling.leaf)
        })

        leafFillings.map(f => {
            handleParams(f, configMap[f.frameFillingGuid])

            f.innerFillings.map(i => {
                i.solid && handleParams(i.solid, configMap[i.solid.solidFillingGuid])
            })
        })

        solidFillings.map(f => {
            handleParams(f, configMap[f.solidFillingGuid])
        })
    })
    return construction
}

export const getConstructionWithDefaultParameters = async (construction: Construction, cityId?: number): Promise<Construction> => {
    const configMap = await loadUserParameters(construction, cityId)
    return getConstructionWithVisibleUserParams(construction, configMap, (filling, params) => {
        if (filling instanceof SolidFilling) {
            filling.resetColorParams()
        }

        const userParameters = [...filling.userParameters]

        params &&
            params.map(p => {
                const {defaultValueId, values, id, isColor, name} = p
                const value = defaultValueId && values && values.find(v => v.id === defaultValueId)
                const param = userParameters.find(param => p.id === param.id)
                if (!param && value) {
                    userParameters.push({id, value: value.value, title: name, hex: isColor ? value.hex : ''})
                }
            })

        filling.userParameters = userParameters
    })
}

export const getUpdatedUserParams = (constructionParams: UserParameter[], availableParams?: UserParameterConfig[]): UserParameter[] | null => {
    const params: UserParameter[] = []
    let isValidParams = true

    constructionParams.map(param => {
        if (param.id === DEFAULT_GLASS_COLOR.id) {
            params.push(param)
            return
        }

        const paramConfig = availableParams?.find(p => p.id === param.id)

        if (!paramConfig) {
            isValidParams = false
            return
        }

        const isAvailable = paramConfig.values?.some(config => config.value === param.value)
        if (isAvailable) {
            const option = paramConfig?.values?.find(v => v.value === param.value)
            if (paramConfig.isColor && option?.hex !== param.hex) {
                isValidParams = false
                param.hex = option?.hex
                param.title = paramConfig.name
            } else if (!paramConfig.isColor && param.title !== paramConfig.name) {
                isValidParams = false
                param.title = paramConfig.name
            }
            params.push(param)
        } else {
            isValidParams = false
            params.push({id: param.id, value: '', title: paramConfig.name, hex: ''})
        }
    })
    availableParams?.map(paramConfig => {
        const index = constructionParams.findIndex(cp => cp.id === paramConfig.id)
        if (index === -1) {
            const option = paramConfig?.values?.find(v => v.id === paramConfig?.defaultValueId)
            if (option) {
                isValidParams = false
                params.push({id: paramConfig.id, value: option.value, title: paramConfig.name, hex: paramConfig.isColor ? option.hex : ''})
            }
        }
    })
    return isValidParams ? null : params
}

export const actualizeUserParameters = async (construction: Construction, cityId?: number): Promise<boolean> => {
    let needUpdate = false
    const config = await loadUserParameters(construction, cityId)
    getConstructionWithVisibleUserParams(construction, config, (filling, params) => {
        const updatedParams = getUpdatedUserParams(filling.userParameters, params)

        if (!updatedParams) {
            return
        }
        filling.userParameters = updatedParams

        needUpdate = true
    })
    return needUpdate
}

export const controlOrderCount = (login: string | null): void => {
    if (!login || !isDemoAccount(login)) {
        return
    }
    const orderCount = sessionStorage.getItem(ORDER_COUNT)
    let count: {[key: string]: number}
    if (orderCount) {
        count = JSON.parse(orderCount) as {[key: string]: number}
        count[login] = count[login] ? count[login] + 1 : 1
    } else {
        count = {[login]: 1}
    }
    sessionStorage.setItem(ORDER_COUNT, JSON.stringify(count))
}

export const isNewOrderAvailable = (login: string | null): boolean => {
    if (!login || !isDemoAccount(login)) {
        return true
    }
    const orderCount = sessionStorage.getItem(ORDER_COUNT)
    if (!orderCount) {
        return true
    }
    const count: {[key: string]: number} = JSON.parse(orderCount) as {[key: string]: number}
    return count[login] ? count[login] < 3 : true
}
