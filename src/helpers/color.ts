import {Colors} from '../constants/colors'

export const isWhiteColor = (color: string): boolean => {
    const normalColorName = color.toLowerCase().replace(' ', '')
    return (
        normalColorName === '#fff' ||
        normalColorName === '#ffffff' ||
        normalColorName === 'rgb(255,255,255)' ||
        normalColorName === 'rgba(255,255,255,1)'
    )
}

export const getColor = (defaultColor: string, hovered: boolean, selected: boolean): string => {
    if (selected) {
        return Colors.Active
    }
    if (hovered) {
        return Colors.Selected
    }
    return defaultColor
}

export const getFillingTitleColor = (color: string | undefined, hovered: boolean, selected: boolean): string => {
    if (color && !hovered && !selected && !isWhiteColor(color)) {
        return Colors.White
    }
    return Colors.LightGray
}
