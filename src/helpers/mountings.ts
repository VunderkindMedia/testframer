import {OrderStates, AllOrderStates} from '../model/framer/order-states'
import {getDateYYYYMMDDFormat, parseDateFromString, getPreviousMonthDate, getNextMonthDate} from './date'
import {MOUNTINGS_FILTERS} from '../constants/local-storage'
import {NAV_MOUNTINGS, NAV_MOUNTING} from '../constants/navigation'

export interface IMountingsFilterParams {
    states: OrderStates[]
    offset: number
    limit: number
    from: Date
    to: Date
    number: string
    owners: number[]
    clients: number[]
    dateFrom: Date
    dateTo: Date
    // shippingFrom?: Date
    // shippingTo?: Date
    filials: number[]
}

export const getMountingsFilterParams = (filter: string): IMountingsFilterParams => {
    const params = new URLSearchParams(filter)

    const from = params.get('from')
    const to = params.get('to')
    const dateFrom = params.get('date_from')
    const dateTo = params.get('date_to')
    const limit = params.get('limit')
    const filials = params.get('fl')
    const clients = params.get('cl')
    const owners = params.get('ow')

    return {
        states: (params.get('st') ?? '').split(',').map(state => state as OrderStates),
        from: from ? parseDateFromString(from) : getPreviousMonthDate(new Date()),
        to: to ? parseDateFromString(to) : new Date(),
        number: params.get('num') ?? '',
        owners: owners ? owners.split(',').map(o => +o) : [],
        clients: clients ? clients.split(',').map(c => +c) : [],
        dateFrom: dateFrom ? parseDateFromString(dateFrom) : new Date(),
        dateTo: dateTo ? parseDateFromString(dateTo) : getNextMonthDate(new Date()),
        filials: filials ? filials.split(',').map(f => +f) : [],
        offset: +(params.get('offset') ?? 0),
        limit: +(limit && +limit ? limit : 2)
    }
}

export const getMountingsFilter = (params: IMountingsFilterParams): string => {
    const {states, number, offset, limit, owners, filials, clients} = params
    const from = getDateYYYYMMDDFormat(params.from)
    const to = getDateYYYYMMDDFormat(params.to)
    const dateFrom = getDateYYYYMMDDFormat(params.dateFrom)
    const dateTo = getDateYYYYMMDDFormat(params.dateTo)
    const st = states.join(',')
    const fl = filials.join(',')
    const cl = clients.join(',')
    const ow = owners.join(',')

    // eslint-disable-next-line max-len
    return `?st=${st}&from=${from}&to=${to}&num=${number}&ow=${ow}&cl=${cl}&date_from=${dateFrom}&date_to=${dateTo}&fl=${fl}&offset=${offset}&limit=${limit}`
}

const getResetMountingsFilterParams = (limit: number): IMountingsFilterParams => {
    return {
        states: AllOrderStates,
        offset: 0,
        limit,
        from: getPreviousMonthDate(new Date()),
        to: new Date(),
        number: '',
        owners: [],
        clients: [],
        dateFrom: new Date(),
        dateTo: getNextMonthDate(new Date()),
        filials: []
    }
}

export const getDefaultMountingsFilterParams = (limit: number): IMountingsFilterParams => {
    const defaultFilters = getResetMountingsFilterParams(limit)
    const savedFilters = localStorage.getItem(MOUNTINGS_FILTERS)

    if (savedFilters) {
        const parsedFilters = JSON.parse(savedFilters) as IMountingsFilterParams

        return {
            states: parsedFilters.states || defaultFilters.states,
            offset: defaultFilters.offset,
            limit: defaultFilters.limit,
            from: parsedFilters.from ? new Date(parsedFilters.from) : defaultFilters.from,
            to: defaultFilters.to,
            number: parsedFilters.number || defaultFilters.number,
            owners: parsedFilters.owners || defaultFilters.owners,
            clients: parsedFilters.clients || defaultFilters.clients,
            dateFrom: parsedFilters.dateFrom ? new Date(parsedFilters.dateFrom) : defaultFilters.dateFrom,
            dateTo: defaultFilters.dateTo,
            filials: parsedFilters.filials || defaultFilters.filials
        }
    }

    return defaultFilters
}

export const getDefaultMountingsFilter = (limit: number): string => {
    return getMountingsFilter(getDefaultMountingsFilterParams(limit))
}

export const getDefaultMountingsLink = (limit: number): string => `${NAV_MOUNTINGS}${getDefaultMountingsFilter(limit)}`

export const getMountingLink = (mountingId: number): string => `${NAV_MOUNTING}?mountingId=${mountingId}`
