import {clamp} from './number'

export const getTableItemsOnPage = (tableHeight: number, tableHeaderHeight: number, tableRowHeight: number): number => {
    const availableHeight = tableHeight - tableHeaderHeight

    return clamp(Math.ceil(availableHeight / tableRowHeight) - 1, 2, 999)
}
