export function clamp(value: number, min: number, max: number): number {
    if (value < min) {
        return min
    }
    if (value > max) {
        return max
    }
    return value
}

export const round = (num: number, fractionDigits = 1): number => {
    return parseFloat(num.toFixed(fractionDigits))
}

export const approximatelyEqual = (number1: number, number2: number, epsilon = 0.5): boolean => {
    const diff = Math.abs(number1 - number2)
    return diff <= epsilon
}

export const radiansToDegrees = (radians: number): number => {
    return (radians * 180) / Math.PI
}

export const degreesToRadians = (degrees: number): number => {
    return (degrees * Math.PI) / 180
}

export const det = (a: number, b: number, c: number, d: number): number => a * d - b * c

export const generateNumberArray = (count: number, offset = 0): number[] => {
    return Array.from(new Array(count), (x, i) => i + offset)
}

export const getRandomInt = (min: number, max: number): number => Math.floor(Math.random() * (max - min)) + min
