import {getPreviousMonthDate, getDateYYYYMMDDFormat, parseDateFromString} from './date'
import {DemoUsers} from '../constants/demo'
import {NAV_PAYMENT} from '../constants/navigation'
import {DEMO_ACCOUNT, UTM_REGISTRATION} from '../constants/local-storage'
import {Partner} from '../model/framer/partner'
import {IUserPermissionsJSON, UserPermissions} from '../model/framer/user-permissions'
import {Order} from '../model/framer/order'
import {getCurrency} from '../helpers/currency'

interface IPaymentsFilterParams {
    dateFrom: Date
    dateTo: Date
    offset: number
    limit: number
}

export const parseJWT = <T>(token: string): T | null => {
    const tokenArr = token.split('.')
    if (!tokenArr[1]) {
        return null
    }
    const base64Url = tokenArr[1]
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    const jsonPayload = decodeURIComponent(
        atob(base64)
            .split('')
            .map(c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2))
            .join('')
    )

    return JSON.parse(jsonPayload) as T
}

export const getPaymentsFilterParams = (filter: string): IPaymentsFilterParams => {
    const params = new URLSearchParams(filter)

    const from = params.get('from')
    const to = params.get('to')

    return {
        dateFrom: from ? parseDateFromString(from) : getPreviousMonthDate(new Date()),
        dateTo: to ? parseDateFromString(to) : new Date(),
        offset: +(params.get('offset') ?? 0),
        limit: +(params.get('limit') ?? 2)
    }
}

export const getPaymentsFilter = (params: IPaymentsFilterParams): string => {
    const {dateFrom, dateTo, offset, limit} = params
    const from = getDateYYYYMMDDFormat(dateFrom)
    const to = getDateYYYYMMDDFormat(dateTo)

    return `?from=${from}&to=${to}&offset=${offset}&limit=${limit}`
}

const getDefaultPaymentsFilterParams = (limit: number): IPaymentsFilterParams => {
    return {
        dateFrom: getPreviousMonthDate(new Date()),
        dateTo: new Date(),
        offset: 0,
        limit
    }
}

export const getDefaultPaymentsLink = (limit: number): string => {
    return `${NAV_PAYMENT}${getPaymentsFilter(getDefaultPaymentsFilterParams(limit))}`
}

export const isDemoAccount = (login: string | null): boolean => {
    return login === DemoUsers.Perm || login === DemoUsers.Moscow
}

export const isRegisteredFromDemo = (): boolean => {
    return !!localStorage.getItem(DEMO_ACCOUNT)
}

// eslint-disable-next-line @typescript-eslint/member-delimiter-style
export const getDiscountForPartner = (partner: Partner, nextDiscount: {nextDiscount: number; nextDiscountAmount: number}): string => {
    if (nextDiscount.nextDiscount || nextDiscount.nextDiscountAmount) {
        return `До получения скидки в ${nextDiscount.nextDiscount}% осталось купить на ${nextDiscount.nextDiscountAmount.toLocaleString(
            'ru-RU'
        )} рублей.`
    }
    const {showDiscountInProfile, discount} = partner
    if (!discount || showDiscountInProfile) {
        return `Ваша скидка от завода - ${discount}%`
    }
    return 'Ваша персональная скидка установлена'
}

export const getAmegaCostTooltip = (order: Order, isSubdealerOrder: boolean): string => {
    return isSubdealerOrder
        ? `Стоимость заказа ${getCurrency(order.totalAmegaCostWithDealerDiscount)} руб., ` +
              (order.calculatedWithFramerPoints
                  ? `при оплате баллами Фрамер ${getCurrency(order.totalAmegaCost)} руб.`
                  : 'стоимость при оплате баллами Фрамер будет доступна после пересчета в режиме баллов Фрамер')
        : `Стоимость заказа ${getCurrency(order.totalAmegaCostWithDealerDiscountToDisplay)} руб., ` +
              (order.calculatedWithFramerPoints
                  ? `при оплате баллами Фрамер ${getCurrency(order.totalAmegaCostToDisplay)} руб.`
                  : 'стоимость при оплате баллами Фрамер будет доступна после пересчета в режиме баллов Фрамер')
}

export const getPermissions = (token: string): UserPermissions | null => {
    if (!token) {
        return null
    }
    try {
        const parsedToken = parseJWT<IUserPermissionsJSON>(token)
        return parsedToken ? UserPermissions.parse(parsedToken) : null
    } catch (e) {
        console.error(e)
        return null
    }
}

export const setUtm = (search: string): void => {
    // @typescript-eslint/naming-convention
    const currentUtm = localStorage.getItem(UTM_REGISTRATION)

    if (currentUtm) {
        return
    }

    const utmList: {[key: string]: string} = {}
    const utmParams = ['utm_source', 'utm_campaign', 'utm_medium']
    const searchParams = new URLSearchParams(search)
    utmParams.forEach(utm => {
        const utmValue = searchParams.get(utm)
        utmValue && (utmList[utm] = utmValue)
    })
    if (Object.keys(utmList).length) {
        localStorage.setItem(UTM_REGISTRATION, JSON.stringify(utmList))
    }
}
