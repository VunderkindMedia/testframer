import {IProductJSON} from '../model/framer/product'

export const requiredFurnitureMaco = (productObj: IProductJSON): boolean => {
    return productObj.hardwareSystemName === 'Фурнитура Maco MM'
}
