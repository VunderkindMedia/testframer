export const safeWhile = (condition: () => boolean, body: () => boolean | void): void => {
    let stepCounter = 0
    while (condition()) {
        const needBreak = body()
        if (needBreak === true) {
            break
        }
        stepCounter++
        if (stepCounter > 999) {
            throw new Error('while step overflow')
        }
    }
}
