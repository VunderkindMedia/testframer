import {IConstructionJSON} from '../model/framer/construction'
import {IInnerFillingJSON} from '../model/framer/inner-filling'
import {
    DEFAULT_ENERGY_FILLING,
    ENERGY_CATEGORY,
    DEFAULT_ENERGY_GLASS_COLOR,
    DEFAULT_ENERGY_DOOR_FILLING
} from '../constants/default-energy-parameters'
import {ISolidFillingJSON} from '../model/framer/solid-filling'
import {UserParameterTypes} from '../model/framer/user-parameter/user-parameter-types'
import {ProductTypes} from '../model/framer/product-types'

const DEFAULT_FILLING = '4-10-4-10-4'
const DEFAULT_DOOR_FILLING = '4-16-4'

export const isEnergyConstruction = (category: string): boolean => {
    return category === ENERGY_CATEGORY
}

const getSolidFillings = (innerFillings: IInnerFillingJSON[]): ISolidFillingJSON[] => {
    const solidFillings: ISolidFillingJSON[] = []
    innerFillings.forEach(innerFilling => {
        if (innerFilling.solid) {
            solidFillings.push(innerFilling.solid)
        }
        if (innerFilling.leaf) {
            solidFillings.push(...getSolidFillings(innerFilling.leaf.innerFillings))
        }
    })
    return solidFillings
}

export const getTemplateWithEnergyParameters = (data: IConstructionJSON): IConstructionJSON => {
    const dataClone = JSON.parse(JSON.stringify(data)) as IConstructionJSON
    dataClone.haveEnergyProducts = true
    dataClone.energyProductType = 'Basic I'
    dataClone.products.forEach(p => {
        const fillings = getSolidFillings(p.frame.innerFillings)
        fillings.forEach(f => {
            if (f.fillingVendorCode === DEFAULT_DOOR_FILLING && p.productionTypeId === ProductTypes.EntranceDoor) {
                f.fillingVendorCode = DEFAULT_ENERGY_DOOR_FILLING
            } else if (f.fillingVendorCode === DEFAULT_FILLING) {
                f.fillingVendorCode = DEFAULT_ENERGY_FILLING
                if (f.userParameters) {
                    const param = f.userParameters?.find(p => p.id === UserParameterTypes.GlassColor)
                    if (param) {
                        param.value = DEFAULT_ENERGY_GLASS_COLOR
                    } else {
                        f.userParameters?.push({id: UserParameterTypes.GlassColor, value: DEFAULT_ENERGY_GLASS_COLOR})
                    }
                } else {
                    f.userParameters = [{id: UserParameterTypes.GlassColor, value: DEFAULT_ENERGY_GLASS_COLOR}]
                }
            }
        })
    })

    return dataClone
}
