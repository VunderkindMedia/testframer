import {Product} from '../model/framer/product'
import {Lamination} from '../model/framer/lamination'

export const getProductLamination = (product: Product | null, lamination: Lamination[]): Lamination[] => {
    const outsideColorId = product?.outsideColorId
    const insideColorId = product?.insideColorId

    const whiteLamination = Lamination.getDefault()
    const outsideLamination = lamination.find(l => l.id === outsideColorId)
    const insideLamination = lamination.find(l => l.id === insideColorId)

    return [outsideLamination ?? whiteLamination, insideLamination ?? whiteLamination]
}
