import * as React from 'react'

export const getPreparedText = (text: string): JSX.Element[] => {
    return text.split('\n').map((text, key) => {
        return (
            <React.Fragment key={key}>
                {text}
                <br />
            </React.Fragment>
        )
    })
}

export const getTextOrMdash = (text: string | undefined): JSX.Element | string => {
    return text && text.length > 0 ? text : <>&mdash;</>
}

export function getSuggestionItem(value: string, query: string, queryClass = 'query'): string {
    const startIndex = value.toLowerCase().indexOf(query.toLowerCase())

    if (startIndex === -1) {
        return value
    }

    const substring = value.substr(startIndex, query.length)

    return value.replace(substring, `<span class="${queryClass}">${substring}</span>`)
}
