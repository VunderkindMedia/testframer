import {Line} from '../model/geometry/line'
import {MarkedLine} from '../model/marked-line'
import {Point} from '../model/geometry/point'
import {DimensionLine} from '../model/dimension-line'
import {safeWhile} from './safe-while'

export function getLeftmostLine<T extends Line>(lines: T[]): T {
    const localLines = lines
        .slice(0)
        .filter(line => line.OY.length !== 0)
        .sort((l1, l2) => l1.rect.center.x - l2.rect.center.x)

    return localLines[0]
}

export function getRightmostLine<T extends Line>(lines: T[]): T {
    const localLines = lines
        .slice(0)
        .filter(line => line.OY.length !== 0)
        .sort((l1, l2) => l2.rect.center.x - l1.rect.center.x)

    return localLines[0]
}

export function getTopmostLine<T extends Line>(lines: T[]): T {
    const localLines = lines
        .slice(0)
        .filter(line => line.OX.length !== 0)
        .sort((l1, l2) => l2.rect.center.y - l1.rect.center.y)

    return localLines[0]
}

export function getBottommostLine<T extends Line>(lines: T[]): T {
    const localLines = lines
        .slice(0)
        .filter(line => line.OX.length !== 0)
        .sort((l1, l2) => l1.rect.center.y - l2.rect.center.y)

    return localLines[0]
}

export const getSortedLines = (lines: MarkedLine[], compareFn: (l1: MarkedLine, l2: MarkedLine) => number): MarkedLine[] => {
    const localLines = lines.slice(0).filter(line => line.length !== 0)

    const getUniqueLines = (lines: MarkedLine[]): MarkedLine[] => {
        const filteredLines: MarkedLine[] = []

        lines.forEach(line => {
            const fLine = filteredLines.find(l => l.equals(line))
            if (fLine) {
                fLine.markers = Array.from(new Set(fLine.markers.concat(line.markers)))
            } else {
                filteredLines.push(line)
            }
        })

        return filteredLines
    }

    const sortedLines = getUniqueLines(localLines).sort((line1, line2) => {
        const dif = compareFn(line1, line2)
        if (dif === 0) {
            return line2.length - line1.length
        }
        return dif
    })

    const newLines: MarkedLine[] = [sortedLines[0]]

    safeWhile(
        () => newLines.length !== sortedLines.length,
        () => {
            const lastLine = newLines[newLines.length - 1]
            const freeLines = sortedLines.filter(line => !newLines.includes(line))
            const connectedLine = freeLines.find(line => lastLine.end.equals(line.start))

            if (connectedLine) {
                newLines.push(connectedLine)
            } else {
                freeLines[0] && newLines.push(freeLines[0])
            }
        }
    )

    return newLines
}

const getCoordinatePairsFromCoordinateArray = (array: number[]): number[][] => {
    const coordinatePairs: number[][] = []

    const sortedArray = Array.from(new Set(array)).sort((c1, c2) => c1 - c2)

    sortedArray.forEach((coordinate, index) => {
        const prevCoordinate = sortedArray[index - 1]
        if (!isNaN(prevCoordinate)) {
            coordinatePairs.push([prevCoordinate, coordinate])
        }
    })

    return coordinatePairs
}

export const getHorizontalLinesFromXArray = (xArray: number[]): Line[] => {
    return getCoordinatePairsFromCoordinateArray(xArray).map(pair => new Line(new Point(pair[0], 0), new Point(pair[1], 0)))
}

export const getVerticalLinesFromYArray = (yArray: number[]): Line[] => {
    return getCoordinatePairsFromCoordinateArray(yArray).map(pair => new Line(new Point(0, pair[0]), new Point(0, pair[1])))
}

export const getDimensionLinesOffsets = (dimensionLines: DimensionLine[], lineOffset: number, bracketWidth: number): number[] => {
    const verticalDimensionLines = dimensionLines.filter(line => line.isVertical)
    const horizontalDimensionLines = dimensionLines.filter(line => line.isHorizontal)

    const verticalLinesWidth = new Set(verticalDimensionLines.map(l => l.start.x)).size * lineOffset + bracketWidth / 2
    const horizontalLinesHeight = new Set(horizontalDimensionLines.map(l => l.start.y)).size * lineOffset + bracketWidth / 2

    return [verticalLinesWidth, horizontalLinesHeight]
}
