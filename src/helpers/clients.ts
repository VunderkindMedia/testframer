export interface IClientsFilterParams {
    offset: number
    limit: number
}

export const getClientsFilter = (offset: number, limit: number): string => {
    return `?offset=${offset}&limit=${limit}`
}

export const getClientsFilterParams = (filter: string): IClientsFilterParams => {
    const params = new URLSearchParams(filter)

    return {
        offset: +(params.get('offset') ?? 0),
        limit: +(params.get('limit') ?? 2)
    }
}
