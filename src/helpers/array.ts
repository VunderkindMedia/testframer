export function arraysHaveSameItems<T>(array1: T[], array2: T[], predicate: (item1: T, item2: T) => boolean): boolean {
    if (array1.length !== array2.length) {
        return false
    }

    if (array1.length === 0 && array2.length === 0) {
        return false
    }

    let isEquals = true
    array1.forEach(array1item => {
        if (!array2.find(array2item => predicate(array2item, array1item))) {
            isEquals = false
        }
    })

    return isEquals
}

export const getUniqItems = <T>(arr: T[]): T[] => {
    return arr.filter((value, index) => arr.indexOf(value) === index)
}
