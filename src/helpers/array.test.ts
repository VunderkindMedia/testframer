import {arraysHaveSameItems} from './array'

test('Тест на то, что массивы имеют одни и теже элементы', () => {
    const array1 = [1, 2, 3, 4, 5, 6, 7]
    const array2 = array1.slice(0).reverse()
    const array3 = array1.slice(0).reverse().concat(1)

    const result1 = arraysHaveSameItems(array1, array2, (item1, item2) => item1 === item2)
    const result2 = arraysHaveSameItems(array1, array3, (item1, item2) => item1 === item2)

    expect(result1).toBe(true)
    expect(result2).toBe(false)
})
