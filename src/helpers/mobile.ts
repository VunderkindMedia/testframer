/* eslint-disable */

export const isMobileBrowser = (): boolean => {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)
}

export const isIOS = (): boolean => {
    return /iPhone|iPad|iPod/i.test(navigator.userAgent)
}

export const isTablet = (): boolean => {
    return isMobileBrowser() && matchMedia('screen and (min-width: 1024px)').matches
}

export const isPhone = (): boolean => {
    return isMobileBrowser() && !isTablet()
}
