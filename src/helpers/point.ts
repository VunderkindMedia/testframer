import {Point} from '../model/geometry/point'
import {Line} from '../model/geometry/line'
import {approximatelyEqual} from './number'
import {GUID} from './guid'

export const getCenter = (points: Point[]): Point => {
    let x = 0
    let y = 0

    points.forEach(point => {
        x += point.x
        y += point.y
    })

    return new Point(x / points.length, y / points.length)
}

// хрень для расчета точек для отирсовки импоста
export const getOrderedPoints = (points: Point[]): Point[] => {
    const point0 = points[0]
    const orderedPoints: Point[] = [point0]

    const point1 = point0.findNearestPoint(points.filter(p => !orderedPoints.includes(p)))
    orderedPoints.push(point1)

    points.forEach(point => {
        if (!point0.equals(point) && orderedPoints.includes(point)) {
            return
        }
        const prevLastOrderedPoint = orderedPoints[orderedPoints.length - 2]
        const lastOrderedPoint = orderedPoints[orderedPoints.length - 1]

        const lastLine = new Line(prevLastOrderedPoint, lastOrderedPoint)

        const availablePoints = points
            .filter(p => !orderedPoints.includes(p))
            .sort((p1, p2) => {
                return (
                    lastLine.findIntersectionAngle(new Line(lastOrderedPoint, p2)) -
                    lastLine.findIntersectionAngle(new Line(lastOrderedPoint, p1))
                )
            })

        orderedPoints.push(availablePoints[0])
    })

    return orderedPoints
}

export const getUniquePoints = (points: Point[]): Point[] => {
    const filteredPoints: Point[] = []

    points.forEach(point => {
        let isContains = false
        filteredPoints.forEach(fPoint => {
            if (approximatelyEqual(point.x, fPoint.x) && approximatelyEqual(point.y, fPoint.y)) {
                isContains = true
            }
        })
        if (!isContains) {
            filteredPoints.push(point)
        }
    })

    return filteredPoints
}

export const getSvgPoints = (points: Point[]): string => {
    return points.map(point => `${point.x}, ${point.y}`).join(' ')
}

export const getIdFromPoints = (prefix: string, points: Point[]): string => {
    if (points.length > 0) {
        return `${prefix}-${points.map(point => `${point.x.toFixed(0)}-${point.y.toFixed(0)}`).join('--')}`
    } else {
        return `${prefix}-${GUID()}`
    }
}
