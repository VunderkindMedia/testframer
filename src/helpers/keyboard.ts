export const isUndo = (e: KeyboardEvent): boolean => {
    return e.code === 'KeyZ' && (e.ctrlKey || e.metaKey) && !e.shiftKey
}

export const isRedo = (e: KeyboardEvent): boolean => {
    return (e.code === 'KeyZ' && (e.ctrlKey || e.metaKey) && e.shiftKey) || (e.code === 'KeyY' && e.ctrlKey && !e.shiftKey)
}
