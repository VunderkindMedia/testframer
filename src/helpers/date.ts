export const getDateDDMMYYYYFormat = (date: Date | null): string => {
    return date
        ? new Intl.DateTimeFormat('ru', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric'
          }).format(date)
        : '—'
}

export const getDateDDMMMMYYYYFormat = (date: Date | null): string => {
    return date
        ? `${new Intl.DateTimeFormat('ru', {
              month: 'long',
              day: 'numeric'
          }).format(date)} ${date.getFullYear()}`
        : '-'
}

export const getWeekdayName = (date: Date): string => {
    return new Intl.DateTimeFormat('ru', {
        weekday: 'long'
    }).format(date)
}

export const getDateDDMonthFormat = (date: Date): string => {
    return new Intl.DateTimeFormat('ru', {
        month: 'long',
        day: 'numeric'
    }).format(date)
}

export const getDateDDMonthYYYYFormat = (date: Date): string => {
    return new Intl.DateTimeFormat('ru', {}).format(date)
}

export const getDateYYYYMMDDFormat = (date: Date): string => {
    return `${date.getFullYear()}-${(date.getMonth() + 1).toString(10).padStart(2, '0')}-${date.getDate().toString(10).padStart(2, '0')}`
}

export const parseDateFromString = (date: string): Date => {
    const args = date.split('-')
    const [year, month, day] = args
    return new Date(+year, +month - 1, +day)
}

export const getDateMMYYYYFormat = (date: Date): string => {
    return `${date.toLocaleString('ru', {month: 'long'})} ${date.getFullYear()}`
}

export const getTimeHHMMFormat = (date: Date): string => {
    return new Intl.DateTimeFormat('ru', {
        hour: '2-digit',
        minute: '2-digit'
    }).format(date)
}

export const getFullDateFormat = (date: Date): string => {
    const day = new Intl.DateTimeFormat('ru', {
        month: 'short',
        day: 'numeric'
    }).format(date)

    const time = new Intl.DateTimeFormat('ru', {
        hour: '2-digit',
        minute: '2-digit'
    }).format(date)

    return `${day} в ${time}`
}

export const getDayStartDate = (date: Date): Date => {
    const newDate = new Date(date)
    newDate.setHours(0, 0, 1, 0)
    return newDate
}

export const getDayEndDate = (date: Date): Date => {
    const newDate = new Date(date)
    newDate.setHours(23, 59, 59)
    return newDate
}

export const getIsoWeekday = (date: Date): number => {
    const day = date.getDay()
    if (day === 0) {
        return 7
    }
    return day
}

export const getNextMonthDate = (date: Date): Date => {
    const dateClone = new Date(date)
    dateClone.setMonth(dateClone.getMonth() + 1)
    return dateClone
}

export const getPreviousMonthDate = (date: Date): Date => {
    const dateClone = new Date(date)
    dateClone.setMonth(dateClone.getMonth() - 1)
    return dateClone
}

export const getPreviousDayDate = (date: Date): Date => {
    const dateClone = new Date(date)
    dateClone.setDate(dateClone.getDate() - 1)
    return dateClone
}

export const getMonthStartDate = (date: Date): Date => {
    return new Date(date.getFullYear(), date.getMonth(), 1)
}

export const getMonthEndDate = (date: Date): Date => {
    return getDayEndDate(new Date(date.getFullYear(), date.getMonth() + 1, 0))
}

export const getDateWithDaysOffset = (date: Date, offset: number): Date => {
    const newDate = new Date(date)
    newDate.setDate(date.getDate() + offset)
    return newDate
}

export const getWeekStartDate = (date: Date): Date => getDayStartDate(getDateWithDaysOffset(date, -getIsoWeekday(date) + 1))

export const getWeekEndDate = (date: Date): Date => getDayEndDate(getDateWithDaysOffset(date, 7 - getIsoWeekday(date)))

export const getAllDatesFromRange = (from: Date, to: Date): Date[] => {
    const dates: Date[] = []

    const currentDate = new Date(from)

    while (currentDate < to) {
        dates.push(new Date(currentDate))
        currentDate.setDate(currentDate.getDate() + 1)
    }

    return dates
}

export const isMonthDate = (date: Date, month: Date = new Date()): boolean => {
    return date.getMonth() === month.getMonth()
}

export const getShortMonthList = (): string[] => {
    return ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек']
}
