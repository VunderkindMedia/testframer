interface IClassNamesInterface {
    [key: string]: boolean
}

export function getClassNames(classStr: string, classObj: IClassNamesInterface): string {
    const keys = Object.keys(classObj)
    const res = keys.filter(key => classObj[key])
    return [classStr, ...res].join(' ')
}
