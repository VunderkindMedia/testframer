import * as React from 'react'
import {GUID} from './guid'

interface IPrintPageView {
    view: JSX.Element
    height: number
}

export class PrintPageBuilder {
    static pxToPt(px: number): number {
        return px * 0.75
    }

    static ptToPx(pt: number): number {
        return pt / 0.75
    }

    private readonly _onePageHeight: number
    private readonly _footerBuilder?: (pageIndex: number, pagesCount: number) => JSX.Element
    private _views: IPrintPageView[] = []

    constructor(onePageHeight: number, footerBuilder?: (pageIndex: number, pagesCount: number) => JSX.Element) {
        this._onePageHeight = onePageHeight
        this._footerBuilder = footerBuilder
    }

    addView(view: JSX.Element, height: number): void {
        this._views.push({view, height})
    }

    get currentPageAvailableHeight(): number {
        let availableHeight = this._onePageHeight
        const heights = this._views.map(v => v.height)

        heights.forEach(height => {
            if (height <= availableHeight) {
                availableHeight -= height
            } else {
                availableHeight = this._onePageHeight
                availableHeight -= height
            }
        })

        return availableHeight
    }

    build(): JSX.Element {
        let pagesCount = 1
        let availableHeight = this._onePageHeight
        const heights = this._views.map(v => v.height)

        heights.forEach(height => {
            if (height <= availableHeight) {
                availableHeight -= height
            } else {
                availableHeight = this._onePageHeight
                availableHeight -= height
                pagesCount++
            }
        })

        const pages: JSX.Element[] = []
        const currentPageContents: JSX.Element[] = []

        let currentPage = 0
        availableHeight = this._onePageHeight

        const addContentToPage = (currentPageContents: JSX.Element[]): void => {
            pages.push(
                <React.Fragment key={GUID()}>
                    {currentPageContents.map(content => (
                        <React.Fragment key={GUID()}>{content}</React.Fragment>
                    ))}
                    {this._footerBuilder && this._footerBuilder(currentPage, pagesCount)}
                </React.Fragment>
            )
            currentPage++
        }

        const addContent = (content: JSX.Element, height: number): void => {
            if (availableHeight >= height) {
                currentPageContents.push(content)
                availableHeight -= height
            } else {
                addContentToPage(currentPageContents)

                availableHeight = this._onePageHeight
                currentPageContents.length = 0
                currentPageContents.push(content)
                availableHeight -= height
            }
        }

        this._views.forEach(view => addContent(view.view, view.height))

        addContentToPage(currentPageContents)

        return (
            <>
                {pages.map(page => (
                    <div key={GUID()} className="a4">
                        {page}
                    </div>
                ))}
            </>
        )
    }
}
