export const getTabs = (search: string): string[] => {
    const pageParams = new URLSearchParams(search)
    const tabsStr = pageParams.get('tabs') ?? ''
    return tabsStr.split(',').filter(t => t.length !== 0)
}
