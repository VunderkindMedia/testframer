import {ConfigContexts} from '../model/config-contexts'
import * as React from 'react'
import {CSSProperties, useCallback} from 'react'
import {ProfileSelector} from '../components/common/profile-selector/profile-selector'
import {FurnitureSelector} from '../components/common/furniture-selector/furniture-selector'
import {LaminationSelector} from '../components/common/lamination-selector/lamination-selector'
import {ConnectorSelector} from '../components/common/connector-selector/connector-selector'
import {IContext} from '../model/i-context'
import {Connector} from '../model/framer/connector'
import {ConnectorTypes} from '../model/framer/connector-types'
import {FillingSelector} from '../components/common/filling-selector/filling-selector'
import {HandleSelector} from '../components/common/handle-selector/handle-selector'
import {MarkingSelector} from '../components/common/marking-selector/marking-selector'
import {MoskitkaConfigurator} from '../components/common/moskitka-configurator/moskitka-configurator'
import {GlassColorSelector} from '../components/common/glass-color-selector/glass-color-selector'
import {ColorSelector} from '../components/common/color-selector/color-selector'
import {FillingEditor} from '../components/common/filling-editor/filling-editor'
import {DimensionLine} from '../model/dimension-line'
import {useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {useUpdateCurrentOrder} from '../hooks/orders'
import {Product} from '../model/framer/product'
import {ModelParts} from '../model/framer/model-parts'
import {Beam} from '../model/framer/beam'
import {FrameFilling} from '../model/framer/frame-filling'
import {SolidFilling} from '../model/framer/solid-filling'
import {Impost} from '../model/framer/impost'
import {UserParameterConfig} from '../model/framer/user-parameter/user-parameter-config'
import {
    INSIDE_FILLING_COLOR_ID,
    OUTSIDE_FILLING_COLOR_ID,
    INSIDE_GALVANIZED_COLOR_ID,
    OUTSIDE_GALVANIZED_COLOR_ID
} from '../constants/default-user-parameters'
import {UserParameter} from '../model/framer/user-parameter/user-parameter'
import {FactoryTypes} from '../model/framer/factory-types'

export interface IConfiguratorViewWithParams {
    title: string
    closeTitle?: string
    view: JSX.Element
    subView?: IConfiguratorViewWithParams[]
    style?: CSSProperties
    shouldResetContext?: boolean
    shouldResetBuilderMode?: boolean
    rootContext?: ConfigContexts
    context?: ConfigContexts
}

export const getConfiguratorViewWithParamsByConfigContext = (
    product: Product | null,
    context: IContext | null,
    configContext: ConfigContexts | null,
    isMobile: boolean
): IConfiguratorViewWithParams | null => {
    if (configContext === ConfigContexts.Profile) {
        return {
            title: 'Профиль',
            closeTitle: 'Назад к общим параметрам',
            view: <ProfileSelector />,
            style: {width: '400px'}
        }
    } else if (configContext === ConfigContexts.Furniture) {
        return {
            title: 'Фурнитура',
            closeTitle: 'Назад к общим параметрам',
            view: <FurnitureSelector />
        }
    } else if (configContext === ConfigContexts.LaminationInside || configContext === ConfigContexts.LaminationOutside) {
        return {
            title: `Ламинация ${configContext === ConfigContexts.LaminationInside ? 'внутри' : 'снаружи'}`,
            closeTitle: 'Назад к общим параметрам',
            view: <LaminationSelector />,
            style: {width: '676px'}
        }
    } else if (configContext === ConfigContexts.Connector || context instanceof Connector) {
        const end = context instanceof Connector && context.connectorType === ConnectorTypes.Extender ? 'расширителя' : 'соединителя'
        const config: IConfiguratorViewWithParams = {
            title: `Выбор ${end}`,
            closeTitle: `Назад к параметрам ${end}`,
            view: <ConnectorSelector />,
            style: {width: '350px'}
        }
        if (context instanceof Connector && configContext !== ConfigContexts.Connector && !isRemovableConnector(product, context)) {
            return {...config, closeTitle: 'Назад к общим параметрам', shouldResetContext: true, shouldResetBuilderMode: true}
        }
        if (!context) {
            config.closeTitle = 'Назад к общим параметрам'
            config.shouldResetBuilderMode = true
        }
        if (configContext === ConfigContexts.Connector) {
            return config
        }
    } else if (
        configContext === ConfigContexts.Filling ||
        configContext === ConfigContexts.FillingEditor ||
        configContext === ConfigContexts.GlassColorEditor
    ) {
        return {
            title: 'Заполнение',
            closeTitle: 'Назад к параметрам заполнения',
            view: <FillingSelector />,
            context: ConfigContexts.Filling,
            subView: [
                {
                    view: <FillingEditor />,
                    title: 'Стеклопакет',
                    style: {right: '568px', width: '260px'},
                    rootContext: isMobile ? ConfigContexts.Filling : undefined,
                    context: ConfigContexts.FillingEditor
                },
                {
                    view: <GlassColorSelector />,
                    title: 'Цвет стекла',
                    style: {right: '568px', width: '260px'},
                    rootContext: ConfigContexts.FillingEditor,
                    context: ConfigContexts.GlassColorEditor
                }
            ],
            style: {width: '558px'}
        }
    } else if (configContext === ConfigContexts.HandleSelector) {
        return {
            title: 'Тип ручки',
            closeTitle: 'Назад к параметрам ручки',
            view: <HandleSelector />,
            style: {width: '430px'}
        }
    } else if (configContext === ConfigContexts.MarkingSelector) {
        let contextText = ''
        if (context instanceof Connector) {
            contextText = ' к параметрам коннектора'
        } else if (context instanceof Impost) {
            contextText = ' к параметрам импоста'
        } else if (context instanceof Beam) {
            contextText = ' к параметрам балки'
        } else if (context instanceof FrameFilling) {
            contextText = ' к параметрам створки'
        }
        return {
            title: 'Артикул',
            closeTitle: `Назад${contextText}`,
            view: <MarkingSelector />,
            style: {width: '430px'}
        }
    } else if (configContext === ConfigContexts.MoskitkaConfigurator) {
        return {
            title: 'Москитная сетка',
            closeTitle: 'Назад к параметрам москитки',
            view: <MoskitkaConfigurator />,
            style: {width: '415px'}
        }
    } else if (configContext === ConfigContexts.GlassColorSelector) {
        return {
            title: 'Цвет стекла',
            closeTitle: 'Назад к параметрам заполнения',
            view: <GlassColorSelector />
        }
    } else if (configContext === ConfigContexts.ColorSelectorOutside || configContext == ConfigContexts.ColorSelectorInside) {
        return {
            title: `Цвет заполнения ${configContext === ConfigContexts.ColorSelectorOutside ? 'снаружи' : 'внутри'}`,
            closeTitle: 'Назад к параметрам заполнения',
            view: <ColorSelector />,
            style: {width: '676px'}
        }
    }
    return null
}

export const useChangeDimensionLineValueHandler = (): ((dLine: DimensionLine, value: number, context: IContext | null | undefined) => void) => {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const partner = useSelector((state: IAppState) => state.account.partner)
    const maxWidthMsk = partner?.factoryId === FactoryTypes.Moscow ? 3000 : undefined

    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        (dLine: DimensionLine, value: number, context: IContext | null | undefined) => {
            if (!currentOrder || !currentConstruction || !currentProduct) {
                return
            }

            const order = currentOrder.clone
            const construction = order.findConstructionById(currentConstruction.id)
            const product = construction?.findProductByProductGuid(currentProduct?.productGuid ?? '') ?? null

            if (!construction || !product) {
                return
            }

            construction.changeSizesAndOffsets(dLine, value, context, maxWidthMsk)
            const lineInfo = `(${dLine.start.x}, ${dLine.start.y}; ${dLine.end.x}, ${dLine.end.y})`
            order.addAction(`Изменили значение размерной линии ${lineInfo} с ${dLine.length} на ${value}`, construction, product)
            updateCurrentOrder(order)
        },
        [currentOrder, currentConstruction, currentProduct, updateCurrentOrder]
    )
}

export const isRemovableConnector = (product: Product | null, connector: Connector | null): boolean => {
    const connectedBeam = product?.connectors.find(c => c.connectedBeamGuid === connector?.nodeId)
    return !connectedBeam && !connector?.connectedProductGuid
}

export const getModePartByContext = (context: IContext | null): ModelParts | null => {
    let modelPart: ModelParts | null = null

    if (context instanceof Beam) {
        modelPart = context.modelPart
    } else if (context instanceof FrameFilling) {
        modelPart = context.frameBeams[0].modelPart
    } else if (context instanceof SolidFilling) {
        modelPart = ModelParts.Zapolneniya
    }

    return modelPart
}

export const getGuidByContext = (context: IContext | null): string | null => {
    if (context instanceof FrameFilling) {
        return context.frameFillingGuid
    }
    if (context instanceof SolidFilling) {
        return context.solidFillingGuid
    }
    if (context instanceof Connector) {
        return context.beamGuid
    }
    if (context instanceof Product) {
        return context.productGuid
    }
    return null
}

export const calcUserParameterConfigsHash = (modelPart: ModelParts, productOrLeafGuid: string): string => `${modelPart}_${productOrLeafGuid}`

export const getColorSelectorConfigContext = (config: UserParameterConfig): ConfigContexts | null => {
    if (config.id === INSIDE_GALVANIZED_COLOR_ID || config.id === INSIDE_FILLING_COLOR_ID) {
        return ConfigContexts.ColorSelectorInside
    }
    if (config.id === OUTSIDE_FILLING_COLOR_ID || config.id === OUTSIDE_GALVANIZED_COLOR_ID) {
        return ConfigContexts.ColorSelectorOutside
    }
    return null
}

export const getUserParameterConfigsHash = (currentContext: IContext | null): string | null => {
    if (!currentContext) {
        return null
    }
    const modelPart = getModePartByContext(currentContext)
    const guid = getGuidByContext(currentContext)
    return modelPart && guid ? calcUserParameterConfigsHash(modelPart, guid) : null
}

export const getInnerColorUserParameter = (solidFilling: SolidFilling): UserParameter | undefined => {
    return solidFilling.userParameters.find(p => p.id === INSIDE_FILLING_COLOR_ID || p.id === INSIDE_GALVANIZED_COLOR_ID)
}
