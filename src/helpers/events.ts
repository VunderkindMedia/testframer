import {EVENTS_FILTERS} from '../constants/local-storage'
import {getDateYYYYMMDDFormat, getMonthEndDate, getMonthStartDate} from './date'
import {NAV_EVENTS} from '../constants/navigation'
import {CalendarEventTypes} from '../model/framer/calendar/calendar-event-types'

export interface IEventsFilterParams {
    from: Date
    to: Date
    owners: number[]
    clients: number[]
    filials: number[]
    states: CalendarEventTypes[]
}

export const getEventsFilterParams = (filter: string): IEventsFilterParams => {
    const params = new URLSearchParams(filter)

    const from = params.get('from')
    const to = params.get('to')
    const filials = params.get('fl')
    const clients = params.get('cl')
    const owners = params.get('ow')
    const states = params.get('st')

    return {
        from: from ? new Date(from) : getMonthStartDate(new Date()),
        to: to ? new Date(to) : getMonthEndDate(new Date()),
        owners: owners ? owners.split(',').map(o => +o) : [],
        clients: clients ? clients.split(',').map(c => +c) : [],
        filials: filials ? filials.split(',').map(f => +f) : [],
        states: states ? states.split(',').map(s => s as CalendarEventTypes) : []
    }
}

export const getEventsFilter = (params: IEventsFilterParams): string => {
    const {owners, filials, clients, states} = params
    const from = getDateYYYYMMDDFormat(params.from)
    const to = getDateYYYYMMDDFormat(params.to)
    const fl = filials.join(',')
    const cl = clients.join(',')
    const ow = owners.join(',')
    const st = states.join(',')

    return `?from=${from}&to=${to}&ow=${ow}&cl=${cl}&fl=${fl}&st=${st}`
}

const getResetEventsFilterParams = (): IEventsFilterParams => {
    return {
        from: getMonthStartDate(new Date()),
        to: getMonthEndDate(new Date()),
        owners: [],
        clients: [],
        filials: [],
        states: []
    }
}

export const getDefaultEventsFilterParams = (): IEventsFilterParams => {
    const defaultFilters = getResetEventsFilterParams()
    const savedFilters = localStorage.getItem(EVENTS_FILTERS)

    if (savedFilters) {
        const parsedFilters = JSON.parse(savedFilters) as IEventsFilterParams
        const from = parsedFilters.from ? new Date(parsedFilters.from) : defaultFilters.from

        return {
            from,
            to: parsedFilters.to ? new Date(parsedFilters.to) : new Date(getMonthEndDate(from)),
            owners: parsedFilters.owners || defaultFilters.owners,
            clients: parsedFilters.clients || defaultFilters.clients,
            filials: parsedFilters.filials || defaultFilters.filials,
            states: parsedFilters.states || defaultFilters.states
        }
    }

    return defaultFilters
}

export const getDefaultEventsFilter = (): string => {
    return getEventsFilter(getDefaultEventsFilterParams())
}

export const getDefaultEventsLink = (): string => `${NAV_EVENTS}${getDefaultEventsFilter()}`
