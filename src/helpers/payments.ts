import {getPreviousMonthDate, getDateYYYYMMDDFormat} from './date'
import {NAV_PAYMENT} from '../constants/navigation'

interface IPaymentsFilterParams {
    dateFrom: Date
    dateTo: Date
    offset: number
    limit: number
}

export const getPaymentsFilterParams = (filter: string): IPaymentsFilterParams => {
    const params = new URLSearchParams(filter)

    const from = params.get('from')
    const to = params.get('to')

    return {
        dateFrom: from ? new Date(from) : getPreviousMonthDate(new Date()),
        dateTo: to ? new Date(to) : new Date(),
        offset: +(params.get('offset') ?? 0),
        limit: +(params.get('limit') ?? 2)
    }
}

export const getPaymentsFilter = (params: IPaymentsFilterParams): string => {
    const {dateFrom, dateTo, offset, limit} = params
    const from = getDateYYYYMMDDFormat(dateFrom)
    const to = getDateYYYYMMDDFormat(dateTo)

    return `?from=${from}&to=${to}&offset=${offset}&limit=${limit}`
}

const getDefaultPaymentsFilterParams = (limit: number): IPaymentsFilterParams => {
    return {
        dateFrom: getPreviousMonthDate(new Date()),
        dateTo: new Date(),
        offset: 0,
        limit
    }
}

export const getDefaultPaymentsLink = (limit: number): string => {
    return `${NAV_PAYMENT}${getPaymentsFilter(getDefaultPaymentsFilterParams(limit))}`
}
