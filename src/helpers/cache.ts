export const clearCache = async (): Promise<void> => {
    if (!caches) {
        return
    }
    const allCaches = await caches.keys()
    const cacheDeletionPromises = allCaches.map(cache => caches.delete(cache))

    await Promise.all([...cacheDeletionPromises])
}
