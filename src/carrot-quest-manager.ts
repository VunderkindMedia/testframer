import {Dispatch, Store} from 'redux'
import {IAppState} from './redux/root-reducer'
import {setChatIsOpened, showChatButton} from './redux/chat/actions'
import {IICarrotQuestIdentifyData, TCarrotQuestConfig} from './types/carrot-quest'
import {isPhone} from './helpers/mobile'
import {BuildTypes} from './model/build-types'

let instance: CarrotQuestManagerClass | null

class CarrotQuestManagerClass {
    private _lastUserId?: number
    private _lastUserHash?: string
    private _store!: Store<IAppState>

    constructor() {
        if (!instance) {
            instance = this
        }

        return instance
    }

    init(store: Store<IAppState>): void {
        this._store = store
    }

    auth(userId: number, hash: string): void {
        if (!CarrotQuestManagerClass.shouldUseCarrot) {
            return
        }

        if (!window.carrotquest?.connected) {
            const script = document.createElement('script')
            script.setAttribute('type', 'text/javascript')
            script.innerHTML =
                '!function(){function t(t,e){return function(){window.carrotquestasync.push(t,arguments)}}if("undefined"==typeof carrotquest){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="//cdn.carrotquest.app/api.min.js",document.getElementsByTagName("head")[0].appendChild(e),window.carrotquest={},window.carrotquestasync=[],carrotquest.settings={};for(var n=["connect","track","identify","auth","oth","onReady","addCallback","removeCallback","trackMessageInteraction"],a=0;a<n.length;a++)carrotquest[n[a]]=t(n[a])}}();'
            document.body.appendChild(script)

            const config: TCarrotQuestConfig = {
                settings: {
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    messenger_position: isPhone() ? 'right_bottom' : 'left_bottom'
                }
            }
            window.carrotquest?.connect(process.env.CARROT_QUEST_API_KEY, config)

            window.carrotquest?.onReady(() => {
                this.dispatch(showChatButton())

                window.carrotquest?.removeChat()
                window.carrotquest?.addCallback('messenger_opened', () => this.dispatch(setChatIsOpened(true)))
                window.carrotquest?.addCallback('messenger_closed', () => this.dispatch(setChatIsOpened(false)))
                window.carrotquest?.addCallback('conversation_opened', () => this.defaultIdentify())
            })
        }

        if (userId !== this._lastUserId || hash !== this._lastUserHash) {
            this._lastUserId = userId
            this._lastUserHash = hash
            window.carrotquest?.auth(userId, hash)
        }
    }

    defaultIdentify(): void {
        const partner = this.state.account.partner
        const search = this.state.router.location.search

        const pageParams = new URLSearchParams(search)

        if (!partner) {
            return
        }

        const currentDealer = partner.dealers.find(d => d.id === partner.selectedDealerId)
        const name = partner.name

        this.identify({
            $name: name,
            $email: partner.contactInfo?.email ?? '',
            $phone: partner.contactInfo?.phone ?? '',
            region: partner.region,
            username: name,
            factoryId: partner.factoryId,
            partnerId: partner.id,
            partnerName: partner.name,
            dealerId: currentDealer?.id ?? '',
            dealerName: currentDealer?.name ?? '',
            marginConstr: partner.margin.toString() ?? '',
            marginGoods: partner.goodsMargin.toString() ?? '',
            dealerExternalId: currentDealer?.externalId ?? '',
            dealerDiscount: partner.discount ?? '',
            openedOrderId: pageParams.get('orderId') ?? ''
        })
    }

    identify(data: IICarrotQuestIdentifyData | IICarrotQuestIdentifyData[]): void {
        if (!CarrotQuestManagerClass.shouldUseCarrot) {
            return
        }
        // !ATTENTION: вызывать строго толлько после auth
        window.carrotquest?.identify(data)
    }

    track(eventName: string, data?: unknown): void {
        if (!CarrotQuestManagerClass.shouldUseCarrot) {
            return
        }
        window.carrotquest?.track(eventName, data)
    }

    getUserId(): string | null {
        return window.carrotquest?.data.user.id || null
    }

    private get state(): IAppState {
        return this._store.getState()
    }

    private get dispatch(): Dispatch {
        return this._store.dispatch
    }

    private static get shouldUseCarrot(): boolean {
        return process.env.BUILD_TYPE === BuildTypes.Prod
    }
}

export const CarrotQuestManager = new CarrotQuestManagerClass()
