/* eslint-disable @typescript-eslint/naming-convention */
import {Dispatch, Store} from 'redux'
import {IRequestOptions, RequestMethods} from './api/request'
import {API_DOCS_AUTH} from './constants/api'
import {setToken} from './redux/cms/actions'
import {IJWT} from './model/token'
import {parseJWT} from './helpers/account'
import {IAppState} from './redux/root-reducer'

let CMSManagerInstance: CMSManagerClass | null = null

class CMSManagerClass {
    private _store!: Store<IAppState>
    private _token: string | null = null
    private _expire: number | null = null

    constructor() {
        if (!CMSManagerInstance) {
            CMSManagerInstance = this
        }

        return CMSManagerInstance
    }

    private get dispatch(): Dispatch {
        return this._store.dispatch
    }

    init(store: Store<IAppState>): void {
        this._store = store
    }

    setToken(token: string | null, expire: number | null): void {
        this._token = token
        this._expire = expire
    }

    checkJWT = async (): Promise<string> => {
        if (this._token && this._expire && this._expire * 1000 >= new Date().getTime()) {
            return this._token
        }
        const token = await this.getJWT()
        const parsed = parseJWT<IJWT>(token)
        if (parsed) {
            this.dispatch(setToken(token, parsed.exp))
            this._token = token
            this._expire = parsed.exp
        }
        return token
    }

    getJWT = async (): Promise<string> => {
        const payload = {
            identifier: 'dev',
            password: 'yvUCciRbj7tF2Yb'
        }
        const response = await this.fetch(
            API_DOCS_AUTH,
            {
                method: RequestMethods.POST,
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            },
            false
        )
        const json = (await response.json()) as {jwt: string}
        return json.jwt
    }

    fetch = async (url: string, options?: IRequestOptions, needAuth = true): Promise<Response> => {
        if (needAuth) {
            const jwt = await this.checkJWT()
            options = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                ...options
            }
        }
        return await fetch(url, options)
    }
}

export const CMSManager = new CMSManagerClass()
