let instance: YandexMetricsManagerClass | null

class YandexMetricsManagerClass {
    constructor() {
        if (!instance) {
            instance = this
        }

        return instance
    }

    reachGoal(target: string): void {
        window.ym(57333352, 'reachGoal', target)
    }

    registerPurchase(id: number, name: string, price: number): void {
        window.dataLayer.push({
            currencyCode: 'RUB',
            ecommerce: {
                purchase: {
                    actionField: {
                        id: id
                    },
                    products: [
                        {
                            id: id,
                            name: name,
                            price: price
                        }
                    ]
                }
            }
        })
    }
}

export const YandexMetricsManager = new YandexMetricsManagerClass()
