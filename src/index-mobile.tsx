import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {App} from './app'
import {MobileApp} from './mobile-app'

ReactDOM.render(
    <App>
        <MobileApp />
    </App>,
    document.getElementById('root') as HTMLElement
)
