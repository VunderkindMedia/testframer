import {Reducer} from 'redux'
import {BookingOptions} from '../../model/framer/booking-options'
import {SET_BOOKING_DATES, SET_BOOKING_OPTIONS, SET_SHIPPING_RESPONSE, TBookingActionTypes} from './action-types'
import {IShippingResponse} from '../../model/framer/i-shipping-response'
import {RESET_SESSION} from '../global-action-types'
import {ISBookingDates} from '../../model/framer/i-booking-dates'

export interface IBookingState {
    bookingOptions: BookingOptions | null
    bookingDates: ISBookingDates | null
    shippingResponse: IShippingResponse | null
}

const initialState: IBookingState = {
    bookingOptions: null,
    bookingDates: null,
    shippingResponse: null
}

export const booking: Reducer<IBookingState> = (state = initialState, action: TBookingActionTypes) => {
    switch (action.type) {
        case SET_BOOKING_OPTIONS:
            return {...state, bookingOptions: action.payload}
        case SET_BOOKING_DATES:
            return {...state, bookingDates: action.payload}
        case SET_SHIPPING_RESPONSE:
            return {...state, shippingResponse: action.payload}
        case RESET_SESSION:
            return initialState
        default:
            return state
    }
}
