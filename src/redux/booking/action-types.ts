import {BookingOptions} from '../../model/framer/booking-options'
import {ISBookingDates} from '../../model/framer/i-booking-dates'
import {IShippingResponse} from '../../model/framer/i-shipping-response'
import {IResetSessionAction} from '../global-action-types'

export const SET_BOOKING_OPTIONS = 'SET_BOOKING_OPTIONS'
export const SET_BOOKING_DATES = 'SET_BOOKING_DATES'
export const SET_SHIPPING_RESPONSE = 'SET_SHIPPING_RESPONSE'

export interface ISetBookingOptionsAction {
    type: typeof SET_BOOKING_OPTIONS
    payload: BookingOptions | null
}

export interface ISetBookingDatesAction {
    type: typeof SET_BOOKING_DATES
    payload: ISBookingDates | null
}

export interface ISetShippingResponseAction {
    type: typeof SET_SHIPPING_RESPONSE
    payload: IShippingResponse | null
}

export type TBookingActionTypes = IResetSessionAction | ISetBookingOptionsAction | ISetBookingDatesAction | ISetShippingResponseAction
