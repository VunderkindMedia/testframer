import {Dispatch} from 'redux'
import {runLongNetworkOperation} from '../request/actions'
import {BookingOptions} from '../../model/framer/booking-options'
import {
    ISetBookingDatesAction,
    ISetBookingOptionsAction,
    ISetShippingResponseAction,
    SET_BOOKING_DATES,
    SET_BOOKING_OPTIONS,
    SET_SHIPPING_RESPONSE
} from './action-types'
import {ShippingTypes} from '../../model/framer/shipping-types'
import {Address} from '../../model/address'
import {IAppState} from '../root-reducer'
import {showStatusbar} from '../statusbar/actions'
import {push} from 'connected-react-router'
import {IShippingResponse} from '../../model/framer/i-shipping-response'
import {Coordinates} from '../../model/coordinates'
import {ContactInfo} from '../../model/framer/contact-info'
import {Order} from '../../model/framer/order'
import {getDefaultOrdersLink} from '../../helpers/orders'
import {api} from '../../api'
import {ISBookingDates} from '../../model/framer/i-booking-dates'

export const setBookingOptions = (bookingOptions: BookingOptions | null): ISetBookingOptionsAction => ({
    type: SET_BOOKING_OPTIONS,
    payload: bookingOptions
})

export const setBookingDates = (bookingDates: ISBookingDates | null): ISetBookingDatesAction => ({
    type: SET_BOOKING_DATES,
    payload: bookingDates
})

export const setShippingResponse = (shippingResponse: IShippingResponse | null): ISetShippingResponseAction => ({
    type: SET_SHIPPING_RESPONSE,
    payload: shippingResponse
})

export const loadBookingOptions =
    (orderId: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const bookingOptions = await api.booking.getOptions(orderId)
            dispatch(setBookingOptions(bookingOptions))
        })(dispatch)
    }

export const loadBookingDates =
    (orderId: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const bookingDates = await api.booking.getDates(orderId)
            dispatch(setBookingDates(bookingDates))
        })(dispatch)
    }

export const book =
    (
        order: Order,
        date: string,
        warehouseId: number,
        shippingType: ShippingTypes,
        address: Address | null,
        contactPerson: ContactInfo | null
    ) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(
            async () => {
                const isSuccess = await api.booking.book(order.id, date, warehouseId, shippingType, address, contactPerson)

                if (isSuccess) {
                    dispatch(showStatusbar('Ваш заказ забронирован'))
                    dispatch(push(`${getDefaultOrdersLink(getState().orders.ordersOnPage)}&id=${order.id}`))
                } else {
                    dispatch(showStatusbar('Не удалось забронировать заказ', true))
                }
            },
            () => {
                dispatch(showStatusbar('Не удалось забронировать заказ', true))
            }
        )(dispatch)
    }

export const calcShipping =
    (orderId: number, destination: Coordinates, address: Address, shippingFloor: number, oversizedProducts: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const shippingResponse = await api.checkout.calcShipping(orderId, destination, address, shippingFloor, oversizedProducts)
            dispatch(setShippingResponse(shippingResponse))
        })(dispatch)
    }
