import {Size} from '../../model/size'
import {Reducer} from 'redux'
import {SET_BROWSER_WINDOW_SIZE, TBrowserActionTypes} from './action-types'
import {RESET_SESSION} from '../global-action-types'

export interface IBrowserState {
    windowSize: Size
}

const initialState: IBrowserState = {
    windowSize: new Size(window.innerWidth, window.innerHeight)
}

export const browser: Reducer<IBrowserState> = (state = initialState, action: TBrowserActionTypes) => {
    switch (action.type) {
        case SET_BROWSER_WINDOW_SIZE:
            return {...state, windowSize: action.payload}
        case RESET_SESSION:
            return initialState
        default:
            return state
    }
}
