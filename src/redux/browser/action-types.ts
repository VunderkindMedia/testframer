import {IResetSessionAction} from '../global-action-types'
import {Size} from '../../model/size'

export const SET_BROWSER_WINDOW_SIZE = 'SET_BROWSER_WINDOW_SIZE'

export interface ISetBrowserWindowSizeAction {
    type: typeof SET_BROWSER_WINDOW_SIZE
    payload: Size
}

export type TBrowserActionTypes = IResetSessionAction | ISetBrowserWindowSizeAction
