import {Size} from '../../model/size'
import {ISetBrowserWindowSizeAction, SET_BROWSER_WINDOW_SIZE} from './action-types'

export const setBrowserWindowSize = (size: Size): ISetBrowserWindowSizeAction => ({
    type: SET_BROWSER_WINDOW_SIZE,
    payload: size
})
