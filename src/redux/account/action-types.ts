import {IResetSessionAction} from '../global-action-types'
import {Partner} from '../../model/framer/partner'
import {Dealer} from '../../model/framer/dealer'
import {UserPermissions} from '../../model/framer/user-permissions'
import {Region} from '../../model/framer/region'
import {Affiliate} from '../../model/framer/affiliate'
import {Employee} from '../../model/framer/employee'
import {DealerInfo} from '../../model/framer/dealer-info'
import {PaymentState} from '../../model/framer/payment-state'
import {BonusScore} from '../../model/framer/bonus-score'
import {Notification} from '../../model/framer/notifications/notification'

export const SET_ACCOUNT_TOKEN = 'SET_ACCOUNT_TOKEN'
export const SET_ACCOUNT_REFRESH_TOKEN = 'SET_ACCOUNT_REFRESH_TOKEN'
export const SET_ACCOUNT_BALANCE = 'SET_ACCOUNT_BALANCE'
export const SET_FRAMER_POINTS = 'SET_FRAMER_POINTS'
export const SET_SHOW_BALANCE = 'SET_SHOW_BALANCE'
export const SET_ACCOUNT_CREDIT_LIMIT = 'SET_ACCOUNT_CREDIT_LIMIT'
export const SET_ACCOUNT_CREDIT_LIMIT_CENTS = 'SET_ACCOUNT_CREDIT_LIMIT_CENTS'
export const SET_PARTNER = 'SET_PARTNER'
export const SET_CURRENT_DEALER = 'SET_CURRENT_DEALER'
export const SET_DEALER_INFO = 'SET_DEALER_INFO'
export const UPDATE_DEALER = 'UPDATE_DEALER'
export const UPDATE_DEALER_INFO = 'UPDATE_DEALER_INFO'
export const SET_USER_PERMISSIONS = 'SET_USER_PERMISSIONS'
export const SET_REGIONS = 'SET_REGIONS'
export const SET_AFFILIATES = 'SET_AFFILIATES'
export const UPDATE_AFFILIATE = 'UPDATE_AFFILIATE'
export const DELETE_AFFILIATE = 'DELETE_AFFILIATE'
export const SET_EMPLOYEES = 'SET_EMPLOYEES'
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE'
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE'
export const SET_PAYMENT_STATE = 'SET_PAYMENT_STATE'
export const SET_ACCOUNT_NAME = 'SET_ACCOUNT_NAME'
export const SET_BONUS_SCORE = 'SET_BONUS_SCORE'
export const SET_NOTIFICATIONS = 'SET_NOTIFICATIONS'
export const SET_NOTIFICATION = 'SET_NOTIFICATION'
export const SET_NEXT_DISCOUNT = 'SET_NEXT_DISCOUNT'
export const SET_IS_CASH_MODE = 'SET_IS_CASH_MODE'
export const SET_LAST_READ_NEWS = 'SET_LAST_READ_NEWS'

export interface ISetAccountTokenAction {
    type: typeof SET_ACCOUNT_TOKEN
    payload: string
}

export interface ISetAccountRefreshTokenAction {
    type: typeof SET_ACCOUNT_REFRESH_TOKEN
    payload: string
}

export interface ISetAccountBalanceAction {
    type: typeof SET_ACCOUNT_BALANCE
    payload: number
}

export interface ISetFramerPointsAction {
    type: typeof SET_FRAMER_POINTS
    payload: number
}

export interface ISetShowBalanceAction {
    type: typeof SET_SHOW_BALANCE
    payload: boolean
}

export interface ISetAccountCreditLimitAction {
    type: typeof SET_ACCOUNT_CREDIT_LIMIT
    payload: number
}

export interface ISetAccountCreditCentsLimitAction {
    type: typeof SET_ACCOUNT_CREDIT_LIMIT_CENTS
    payload: number
}

export interface ISetPartnerAction {
    type: typeof SET_PARTNER
    payload: Partner
}

export interface ISetCurrentDealerAction {
    type: typeof SET_CURRENT_DEALER
    payload: Dealer
}

export interface ISetDealerInfoAction {
    type: typeof SET_DEALER_INFO
    payload: DealerInfo | null
}

export interface IUpdateDealerAction {
    type: typeof UPDATE_DEALER
    payload: {
        id: number
        dealer: Dealer
    }
}

export interface IUpdateDealerInfoAction {
    type: typeof UPDATE_DEALER_INFO
    payload: {
        id: number
        info: DealerInfo
    }
}

export interface ISetUserPermissionsAction {
    type: typeof SET_USER_PERMISSIONS
    payload: UserPermissions | null
}

export interface ISetRegionsAction {
    type: typeof SET_REGIONS
    payload: Region[]
}

export interface ISetAffiliatesAction {
    type: typeof SET_AFFILIATES
    payload: Affiliate[]
}

export interface IUpdateAffiliateAction {
    type: typeof UPDATE_AFFILIATE
    payload: Affiliate
}

export interface IDeleteAffiliateAction {
    type: typeof DELETE_AFFILIATE
    payload: number
}

export interface ISetEmployeesAction {
    type: typeof SET_EMPLOYEES
    payload: Employee[]
}

export interface IUpdateEmployeeAction {
    type: typeof UPDATE_EMPLOYEE
    payload: Employee
}

export interface IDeleteEmployeeAction {
    type: typeof DELETE_EMPLOYEE
    payload: number
}

export interface ISetPaymentStateAction {
    type: typeof SET_PAYMENT_STATE
    payload: PaymentState | null
}

export interface ISetAccountNameAction {
    type: typeof SET_ACCOUNT_NAME
    payload: string
}

export interface ISetNotificationsAction {
    type: typeof SET_NOTIFICATIONS
    payload: Notification[]
}

export interface ISetNotificationAction {
    type: typeof SET_NOTIFICATION
    payload: Notification
}

export interface ISetBonusScoreAction {
    type: typeof SET_BONUS_SCORE
    payload: BonusScore
}

export interface ISetNextDiscountAction {
    type: typeof SET_NEXT_DISCOUNT
    // eslint-disable-next-line @typescript-eslint/member-delimiter-style
    payload: {nextDiscount: number; nextDiscountAmount: number}
}

export interface ISetIsCashModeAction {
    type: typeof SET_IS_CASH_MODE
    payload: boolean
}

export interface ISetLastReadNewsAction {
    type: typeof SET_LAST_READ_NEWS
    payload: number
}

export type TAccountActionTypes =
    | IResetSessionAction
    | ISetAccountTokenAction
    | ISetAccountRefreshTokenAction
    | ISetAccountBalanceAction
    | ISetAccountCreditLimitAction
    | ISetAccountCreditCentsLimitAction
    | ISetPartnerAction
    | ISetCurrentDealerAction
    | ISetDealerInfoAction
    | IUpdateDealerAction
    | IUpdateDealerInfoAction
    | ISetUserPermissionsAction
    | ISetRegionsAction
    | ISetAffiliatesAction
    | IUpdateAffiliateAction
    | IDeleteAffiliateAction
    | ISetEmployeesAction
    | IUpdateEmployeeAction
    | IDeleteEmployeeAction
    | ISetPaymentStateAction
    | ISetAccountNameAction
    | ISetBonusScoreAction
    | ISetNotificationsAction
    | ISetNotificationAction
    | ISetNextDiscountAction
    | ISetIsCashModeAction
    | ISetFramerPointsAction
    | ISetShowBalanceAction
    | ISetLastReadNewsAction
