import {API_BILL} from '../../constants/api'
import {
    SET_ACCOUNT_BALANCE,
    SET_ACCOUNT_CREDIT_LIMIT,
    SET_ACCOUNT_TOKEN,
    SET_CURRENT_DEALER,
    SET_PARTNER,
    SET_REGIONS,
    SET_USER_PERMISSIONS,
    SET_AFFILIATES,
    UPDATE_AFFILIATE,
    DELETE_AFFILIATE,
    SET_EMPLOYEES,
    UPDATE_EMPLOYEE,
    DELETE_EMPLOYEE,
    SET_DEALER_INFO,
    UPDATE_DEALER,
    UPDATE_DEALER_INFO,
    SET_ACCOUNT_NAME,
    SET_BONUS_SCORE,
    SET_NEXT_DISCOUNT,
    SET_IS_CASH_MODE,
    SET_FRAMER_POINTS,
    ISetAccountTokenAction,
    ISetAccountBalanceAction,
    ISetAccountCreditLimitAction,
    ISetPartnerAction,
    ISetCurrentDealerAction,
    ISetUserPermissionsAction,
    ISetRegionsAction,
    ISetAffiliatesAction,
    IUpdateAffiliateAction,
    IDeleteAffiliateAction,
    ISetEmployeesAction,
    IUpdateEmployeeAction,
    IDeleteEmployeeAction,
    IUpdateDealerInfoAction,
    IUpdateDealerAction,
    ISetDealerInfoAction,
    SET_PAYMENT_STATE,
    ISetPaymentStateAction,
    ISetAccountNameAction,
    SET_ACCOUNT_REFRESH_TOKEN,
    ISetAccountRefreshTokenAction,
    ISetBonusScoreAction,
    SET_NOTIFICATIONS,
    ISetNotificationAction,
    SET_NOTIFICATION,
    ISetNotificationsAction,
    ISetNextDiscountAction,
    ISetIsCashModeAction,
    ISetFramerPointsAction,
    ISetShowBalanceAction,
    SET_SHOW_BALANCE,
    SET_ACCOUNT_CREDIT_LIMIT_CENTS,
    ISetAccountCreditCentsLimitAction,
    ISetLastReadNewsAction,
    SET_LAST_READ_NEWS
} from './action-types'
import {showStatusbar, hideStatusbar} from '../statusbar/actions'
import {push} from 'connected-react-router'
import {runLongNetworkOperation} from '../request/actions'
import {Service} from '../../model/framer/service'
import {Margin} from '../../model/framer/margin'
import {Dispatch} from 'redux'
import {IAppState} from '../root-reducer'
import {UserPermissions} from '../../model/framer/user-permissions'
import {TUTORIAL_SHOWED, DEMO_ACCOUNT, ORDER_COUNT, UTM_REGISTRATION} from '../../constants/local-storage'
import {NAV_START_TUTORIAL, NAV_ROOT} from '../../constants/navigation'
import {Partner} from '../../model/framer/partner'
import {Dealer} from '../../model/framer/dealer'
import {DealerInfo} from '../../model/framer/dealer-info'
import {Region} from '../../model/framer/region'
import {User} from '../../model/framer/user'
import {Affiliate} from '../../model/framer/affiliate'
import {Employee} from '../../model/framer/employee'
import {BonusScore} from '../../model/framer/bonus-score'
import {getDefaultOrdersLink} from '../../helpers/orders'
import {api} from '../../api'
import {SessionManager} from '../../session-manager'
import {PaymentState} from '../../model/framer/payment-state'
import {AnalyticsManager} from '../../analytics-manager'
import {CarrotQuestManager} from '../../carrot-quest-manager'
import {AffiliateMarginTypes} from '../../model/affiliate-margin-types'
import {Notification} from '../../model/framer/notifications/notification'
import {DEMO_TRACKING_INFO, DemoUsers} from '../../constants/demo'
import {resetSession} from '../global-actions'
import {setOnboardingStep} from '../builder/actions'

export const setAccountToken = (token: string): ISetAccountTokenAction => ({
    type: SET_ACCOUNT_TOKEN,
    payload: token
})

export const setAccountRefreshToken = (token: string): ISetAccountRefreshTokenAction => ({
    type: SET_ACCOUNT_REFRESH_TOKEN,
    payload: token
})

export const setAccountName = (login: string): ISetAccountNameAction => ({
    type: SET_ACCOUNT_NAME,
    payload: login
})

export const setAccountBalance = (balance: number): ISetAccountBalanceAction => ({
    type: SET_ACCOUNT_BALANCE,
    payload: balance
})

export const setFramerPoints = (points: number): ISetFramerPointsAction => ({
    type: SET_FRAMER_POINTS,
    payload: points
})

export const setLastReadNews = (lastReadNewsId: number): ISetLastReadNewsAction => ({
    type: SET_LAST_READ_NEWS,
    payload: lastReadNewsId
})

export const setShowBalance = (payload: boolean): ISetShowBalanceAction => ({
    type: SET_SHOW_BALANCE,
    payload
})

export const setAccountCreditLimit = (limit: number): ISetAccountCreditLimitAction => ({
    type: SET_ACCOUNT_CREDIT_LIMIT,
    payload: limit
})

export const setAccountCreditLimitCents = (limit: number): ISetAccountCreditCentsLimitAction => ({
    type: SET_ACCOUNT_CREDIT_LIMIT_CENTS,
    payload: limit
})

export const setPartner = (partner: Partner): ISetPartnerAction => ({
    type: SET_PARTNER,
    payload: partner
})

export const setCurrentDealer = (dealer: Dealer): ISetCurrentDealerAction => ({
    type: SET_CURRENT_DEALER,
    payload: dealer
})

export const setUserPermissions = (permissions: UserPermissions | null): ISetUserPermissionsAction => ({
    type: SET_USER_PERMISSIONS,
    payload: permissions
})

export const setBonusScore = (bonusScore: BonusScore): ISetBonusScoreAction => ({
    type: SET_BONUS_SCORE,
    payload: bonusScore
})

export const setNextDiscount = (nextDiscount: number, nextDiscountAmount: number): ISetNextDiscountAction => ({
    type: SET_NEXT_DISCOUNT,
    payload: {nextDiscount, nextDiscountAmount}
})

export const setIsCashMode = (payload: boolean): ISetIsCashModeAction => ({
    type: SET_IS_CASH_MODE,
    payload
})

const setRegions = (regions: Region[]): ISetRegionsAction => ({
    type: SET_REGIONS,
    payload: regions
})

const setAffiliates = (affiliates: Affiliate[]): ISetAffiliatesAction => ({
    type: SET_AFFILIATES,
    payload: affiliates
})

const updateAffiliateAction = (affiliate: Affiliate): IUpdateAffiliateAction => ({
    type: UPDATE_AFFILIATE,
    payload: affiliate
})

const deleteAffiliateAction = (id: number): IDeleteAffiliateAction => ({
    type: DELETE_AFFILIATE,
    payload: id
})

const setEmployees = (employees: Employee[]): ISetEmployeesAction => ({
    type: SET_EMPLOYEES,
    payload: employees
})

const updateEmployeeAction = (employee: Employee): IUpdateEmployeeAction => ({
    type: UPDATE_EMPLOYEE,
    payload: employee
})

const deleteEmployeeAction = (id: number): IDeleteEmployeeAction => ({
    type: DELETE_EMPLOYEE,
    payload: id
})

const updateDealerInfo = (id: number, info: DealerInfo): IUpdateDealerInfoAction => ({
    type: UPDATE_DEALER_INFO,
    payload: {id, info}
})

const updateDealer = (dealer: Dealer): IUpdateDealerAction => ({
    type: UPDATE_DEALER,
    payload: {id: dealer.id, dealer}
})

export const setDealerInfo = (info: DealerInfo | null): ISetDealerInfoAction => ({
    type: SET_DEALER_INFO,
    payload: info
})

export const setPaymentState = (state: PaymentState | null): ISetPaymentStateAction => ({
    type: SET_PAYMENT_STATE,
    payload: state
})

const setNotifications = (notifications: Notification[]): ISetNotificationsAction => ({
    type: SET_NOTIFICATIONS,
    payload: notifications
})

export const setNotification = (notification: Notification): ISetNotificationAction => ({
    type: SET_NOTIFICATION,
    payload: notification
})

export const login =
    (username: string, password: string, isDemo = false) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const token = await api.account.login(username, password)
            if (token.accessToken) {
                const tutorialShowed = localStorage.getItem(TUTORIAL_SHOWED)

                dispatch(setUserPermissions(token.permissions))
                dispatch(setAccountToken(token.accessToken))
                dispatch(setAccountRefreshToken(token.refreshToken))
                dispatch(setAccountName(username))

                const isRestrictedMode = getState().settings.isRestrictedMode
                if (!isRestrictedMode) {
                    const redirectLink = !tutorialShowed ? NAV_START_TUTORIAL : getDefaultOrdersLink(getState().orders.ordersOnPage)
                    dispatch(push(redirectLink))
                }

                SessionManager.initSession()

                AnalyticsManager.trackAuthorized()

                if (isDemo) {
                    dispatch(setOnboardingStep(0))
                    const {id, factory} = DEMO_TRACKING_INFO[username as DemoUsers]
                    AnalyticsManager.trackDemoLoginClick(factory, id)
                }
            } else {
                dispatch(showStatusbar(token.error, true))
                throw new Error(token.error)
            }
        })(dispatch)
    }

export const loadAccount =
    () =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        try {
            const account = await api.account.get()

            const {balance, creditLimitInCents, framerCreditLimitInCents, bonusScore, nextDiscount, framerPoints, showBalance} =
                getState().account
            const {availableScore, potentialScore} = bonusScore

            if (balance !== account.balance) {
                dispatch(setAccountBalance(account.balance))
            }
            if (framerPoints !== account.framerPoints) {
                dispatch(setFramerPoints(account.framerPoints))
            }
            if (showBalance !== account.showBalance) {
                dispatch(setShowBalance(account.showBalance))
            }
            if (creditLimitInCents !== account.creditLimitInCents) {
                dispatch(setAccountCreditLimit(account.creditLimitInCents))
            }
            if (framerCreditLimitInCents !== account.framerCreditLimitInCents) {
                dispatch(setAccountCreditLimitCents(account.framerCreditLimitInCents))
            }
            if (availableScore !== account.availableScore || potentialScore !== account.potentialScore) {
                dispatch(
                    setBonusScore(new BonusScore(account.potentialScore, account.reservedScore, account.availableScore, account.earnedScore))
                )
            }

            if (nextDiscount.nextDiscount !== account.nextDiscount || nextDiscount.nextDiscountAmount !== account.nextDiscountAmount) {
                dispatch(setNextDiscount(account.nextDiscount, account.nextDiscountAmount))
            }
        } catch (e) {
            console.error(e)
        }
    }

export const loadPartner =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const partner = await api.account.getPartner()
            dispatch(setPartner(partner))
        })(dispatch)
    }

export const setCurrentDealerRemote =
    (dealerId: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.account.setCurrentDealer(dealerId)
            await loadPartner()(dispatch)
            const account = await api.account.get()
            dispatch(setAccountBalance(account.balance))
            dispatch(setNextDiscount(account.nextDiscount, account.nextDiscountAmount))
            dispatch(setFramerPoints(account.framerPoints))
            dispatch(setAccountCreditLimit(account.creditLimitInCents))
            dispatch(setAccountCreditLimitCents(account.framerCreditLimitInCents))
        })(dispatch)
    }

export const loadDealer =
    (dealerId: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const dealer = await api.dealers.getById(dealerId)
            dispatch(setCurrentDealer(dealer))
        })(dispatch)
    }

export const addService =
    (service: Service) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.services.add(service)
            await loadPartner()(dispatch)
        })(dispatch)
    }

export const removeService =
    (serviceId: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.services.remove(serviceId)
            await loadPartner()(dispatch)
        })(dispatch)
    }

export const updateService =
    (service: Service) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.services.update(service)
            await loadPartner()(dispatch)
        })(dispatch)
    }

export const updateMargin =
    (margin: Margin) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.account.updateMargin(margin)
            await loadPartner()(dispatch)
        })(dispatch)
    }

export const updateGoodsMargin =
    (margin: Margin) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.account.updateGoodsMargin(margin)
            await loadPartner()(dispatch)
        })(dispatch)
    }

export const deposit =
    (amount: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const result = await api.checkout.deposit(amount)
            window.location.replace(result.sberFormURL)
        })(dispatch)
    }

export const checkout =
    (reservationId: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const paymentState = await api.checkout.checkout(reservationId)
            dispatch(setPaymentState(paymentState))
        })(dispatch)
    }

export const setOfferFields =
    (offerAddonLine1: string, offerAddonLine2: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const partner = await api.account.setOfferFields(offerAddonLine1, offerAddonLine2)
            dispatch(setPartner(partner))
        })(dispatch)
    }

export const lastReadNews =
    (id: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const lastReadNewsId = await api.account.setCurrentNewsId(id)
            dispatch(setLastReadNews(lastReadNewsId))
        })(dispatch)
    }

export const loadRegions = async (dispatch: Dispatch): Promise<void> => {
    await runLongNetworkOperation(async () => {
        const regions = await api.account.getRegions()
        dispatch(setRegions(regions))
    })(dispatch)
}

export const register =
    (user: User, isDemoUser: boolean) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        dispatch(hideStatusbar())

        await runLongNetworkOperation(async () => {
            const utmStr = localStorage.getItem(UTM_REGISTRATION)
            let utm = ''
            if (utmStr) {
                const utmObj = JSON.parse(utmStr) as {[key: string]: string}
                const utmArr = Object.keys(utmObj).map(u => `${u}=${utmObj[u]}`)
                utm = utmArr.join('&')
            }

            const error = await api.account.register(user, utm)

            if (error) {
                dispatch(showStatusbar(error, true))
            } else {
                dispatch(showStatusbar('Вы успешно зарегистрировались'))

                if (isDemoUser) {
                    const {permissions, token, refreshToken} = getState().account
                    dispatch(resetSession())
                    token && dispatch(setAccountToken(token))
                    refreshToken && dispatch(setAccountRefreshToken(refreshToken))
                    dispatch(setUserPermissions(permissions))

                    localStorage.setItem(DEMO_ACCOUNT, String(new Date().valueOf()))
                    sessionStorage.removeItem(ORDER_COUNT)

                    const currentUserId = CarrotQuestManager.getUserId()

                    const timerId = setInterval(() => {
                        if (currentUserId !== CarrotQuestManager.getUserId()) {
                            clearInterval(timerId)
                            AnalyticsManager.trackRegistered({$email: user.email})
                            AnalyticsManager.trackRegisterFromDemoSendForm()
                        }
                    }, 1000)
                } else {
                    AnalyticsManager.trackRegistered({$email: user.email})
                }
                await login(user.email, user.password)(dispatch, getState)
            }
        })(dispatch)
    }

export const forgotPassword =
    (email: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        dispatch(hideStatusbar())

        await runLongNetworkOperation(async () => {
            const error = await api.account.forgotPassword(email)
            if (error) {
                dispatch(showStatusbar(error, true))
            } else {
                dispatch(showStatusbar('Письмо для сброса пароля отправлено'))
            }
        })(dispatch)
    }

export const resetPassword =
    (resetCode: string, password: string, confirmPassword: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        dispatch(hideStatusbar())

        await runLongNetworkOperation(async () => {
            const error = await api.account.resetPassword(resetCode, password, confirmPassword)
            if (error) {
                dispatch(showStatusbar(error, true))
            } else {
                dispatch(showStatusbar('Пароль обновлен'))
                setTimeout(() => dispatch(push(NAV_ROOT)), 3000)
            }
        })(dispatch)
    }

export const loadAffiliates =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const affiliates = await api.affiliates.get()
            dispatch(setAffiliates(affiliates))
        })(dispatch)
    }

export const createAffiliate =
    (name: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const result = await api.affiliates.create(name)
            dispatch(updateAffiliateAction(result))
        })(dispatch)
    }

export const updateAffiliate =
    (id: number, name: string, isExternal: boolean) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const result = await api.affiliates.update(id, name, isExternal)
            dispatch(updateAffiliateAction(result))
        })(dispatch)
    }

export const updateAffiliateMargin =
    (id: number, margin: Margin, type: AffiliateMarginTypes) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const result = await api.affiliates.updateMargin(id, margin, type)
            dispatch(updateAffiliateAction(result))
        })(dispatch)
    }

export const deleteAffiliate =
    (id: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const isSuccess = await api.affiliates.delete(id)
            if (isSuccess) {
                dispatch(deleteAffiliateAction(id))
            }
        })(dispatch)
    }

export const loadEmployees =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const result = await api.employees.get()
            dispatch(setEmployees(result))
        })(dispatch)
    }

export const updateEmployee =
    (employee: Employee) =>
    async (dispatch: Dispatch): Promise<void> => {
        dispatch(hideStatusbar())

        await runLongNetworkOperation(
            async () => {
                const result = await api.employees.update(employee)
                dispatch(updateEmployeeAction(result))
            },
            e => dispatch(showStatusbar(e.message, true))
        )(dispatch)
    }

export const deleteEmployee =
    (id: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const isSuccess = await api.employees.delete(id)
            if (isSuccess) {
                dispatch(deleteEmployeeAction(id))
            }
        })(dispatch)
    }

export const checkInn =
    (id: number, inn: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        dispatch(hideStatusbar())

        await runLongNetworkOperation(async () => {
            const result = await api.dealers.checkInn(inn)

            if (!result.dealerType || !result.inn) {
                dispatch(showStatusbar('ИНН не найден', true))
            } else {
                dispatch(id ? updateDealerInfo(id, result) : setDealerInfo(result))
            }
        })(dispatch)
    }

export const saveDealer =
    (dealer: DealerInfo) =>
    async (dispatch: Dispatch): Promise<void> => {
        dispatch(hideStatusbar())

        const requestKey = 'saveDealer'

        await runLongNetworkOperation(
            async () => {
                const result = await api.dealers.save(dealer)
                if (result instanceof Dealer) {
                    dispatch(updateDealer(result))
                    if (result.id) {
                        dispatch(showStatusbar('Запрос на подтверждение отправлен, ожидайте письмо с подтверждением на электронную почту'))
                    } else {
                        dispatch(showStatusbar('Запрос на добавление отправлен, ожидайте письмо с подтверждением на электронную почту'))
                    }
                    localStorage.removeItem(DEMO_ACCOUNT)
                } else {
                    dispatch(showStatusbar(result, true))
                }
            },
            () => dispatch(showStatusbar('Контрагент не был сохранен', true))
        )(dispatch, requestKey)
    }

export const loadBill =
    (sum: number) =>
    (dispatch: Dispatch, getState: () => IAppState): void => {
        const link = document.createElement('a')
        link.setAttribute('href', `${API_BILL(sum)}&t=${getState().account.token}`)
        link.click()
    }

export const loadNotifications =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        try {
            const notificationList = await api.notifications.get()
            dispatch(setNotifications(notificationList.notifications))
        } catch (e) {
            console.error(e)
        }
    }

export const markAllNotificationsAsRead =
    () =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const notificationList = await api.notifications.markAllAsRead()
            const currentNotifications = [...getState().account.notifications]
            notificationList.notifications.map(n => {
                const index = currentNotifications.findIndex(cn => n.id === cn.id)
                if (index !== -1) {
                    currentNotifications[index] = n
                }
            })
            dispatch(setNotifications(currentNotifications))
        })(dispatch)
    }

export const markNotificationAsRead =
    (id: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const notification = await api.notifications.markAsRead(id)
            dispatch(setNotification(notification))
        })(dispatch)
    }

export const markNotificationAsUnread =
    (id: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const notification = await api.notifications.markAsUnread(id)
            dispatch(setNotification(notification))
        })(dispatch)
    }
