import {Reducer} from 'redux'
import {
    SET_ACCOUNT_BALANCE,
    SET_ACCOUNT_CREDIT_LIMIT,
    SET_ACCOUNT_TOKEN,
    SET_CURRENT_DEALER,
    SET_PARTNER,
    SET_REGIONS,
    SET_USER_PERMISSIONS,
    SET_AFFILIATES,
    UPDATE_AFFILIATE,
    DELETE_AFFILIATE,
    SET_EMPLOYEES,
    UPDATE_EMPLOYEE,
    DELETE_EMPLOYEE,
    SET_DEALER_INFO,
    UPDATE_DEALER,
    UPDATE_DEALER_INFO,
    TAccountActionTypes,
    SET_PAYMENT_STATE,
    SET_ACCOUNT_NAME,
    SET_ACCOUNT_REFRESH_TOKEN,
    SET_BONUS_SCORE,
    SET_NOTIFICATIONS,
    SET_NOTIFICATION,
    SET_NEXT_DISCOUNT,
    SET_IS_CASH_MODE,
    SET_FRAMER_POINTS,
    SET_SHOW_BALANCE,
    SET_ACCOUNT_CREDIT_LIMIT_CENTS,
    SET_LAST_READ_NEWS
} from './action-types'
import {ACCOUNT_TOKEN, ACCOUNT_NAME, ACCOUNT_REFRESH_TOKEN} from '../../constants/local-storage'
import {Partner} from '../../model/framer/partner'
import {Dealer} from '../../model/framer/dealer'
import {UserPermissions} from '../../model/framer/user-permissions'
import {Region} from '../../model/framer/region'
import {Affiliate} from '../../model/framer/affiliate'
import {Employee} from '../../model/framer/employee'
import {RESET_SESSION} from '../global-action-types'
import {PaymentState} from '../../model/framer/payment-state'
import {Notification} from '../../model/framer/notifications/notification'
import {getPermissions} from '../../helpers/account'
import {BonusScore} from '../../model/framer/bonus-score'

export interface IAccountState {
    token: string | null
    refreshToken: string | null
    login: string | null
    balance: number
    framerPoints: number
    creditLimitInCents: number
    framerCreditLimitInCents: number
    partner: Partner | null
    currentDealer: Dealer | null
    permissions: UserPermissions | null
    regions: Region[]
    affiliates: Affiliate[] | null
    employees: Employee[] | null
    paymentState: PaymentState | null
    bonusScore: BonusScore
    notifications: Notification[]
    // eslint-disable-next-line @typescript-eslint/member-delimiter-style
    nextDiscount: {nextDiscount: number; nextDiscountAmount: number}
    isCashMode: boolean
    showBalance: boolean
}

const token = localStorage.getItem(ACCOUNT_TOKEN)
const refreshToken = localStorage.getItem(ACCOUNT_REFRESH_TOKEN)
const login = localStorage.getItem(ACCOUNT_NAME)

const initialState: IAccountState = {
    token,
    refreshToken,
    login,
    balance: 0,
    framerPoints: 0,
    creditLimitInCents: 0,
    framerCreditLimitInCents: 0,
    partner: null,
    currentDealer: null,
    permissions: token ? getPermissions(token) : null,
    regions: [],
    affiliates: null,
    employees: null,
    paymentState: null,
    bonusScore: {availableScore: 0, potentialScore: 0, reservedScore: 0, earnedScore: 0},
    notifications: [],
    nextDiscount: {nextDiscount: 0, nextDiscountAmount: 0},
    isCashMode: false,
    showBalance: false
}

export const account: Reducer<IAccountState, TAccountActionTypes> = (state = initialState, action) => {
    switch (action.type) {
        case SET_ACCOUNT_TOKEN: {
            localStorage.setItem(ACCOUNT_TOKEN, action.payload)
            return {...state, token: action.payload}
        }
        case SET_ACCOUNT_REFRESH_TOKEN: {
            localStorage.setItem(ACCOUNT_REFRESH_TOKEN, action.payload)
            return {...state, refreshToken: action.payload}
        }
        case SET_ACCOUNT_NAME: {
            localStorage.setItem(ACCOUNT_NAME, action.payload)
            return {...state, login: action.payload}
        }
        case SET_ACCOUNT_BALANCE:
            return {...state, balance: action.payload}
        case SET_FRAMER_POINTS:
            return {...state, framerPoints: action.payload}
        case SET_ACCOUNT_CREDIT_LIMIT:
            return {...state, creditLimitInCents: action.payload}
        case SET_ACCOUNT_CREDIT_LIMIT_CENTS:
            return {...state, framerCreditLimitInCents: action.payload}
        case SET_PARTNER: {
            const partner: Partner = action.payload
            const currentDealer = partner.dealers.find(dealer => dealer.id === partner.selectedDealerId) ?? null
            return {...state, partner, currentDealer}
        }
        case SET_LAST_READ_NEWS: {
            const {partner} = state
            if (!partner) {
                return state
            }

            const partnerClone = partner.clone
            partnerClone.lastReadNewsId = action.payload

            return {...state, partner: partnerClone}
        }
        case SET_CURRENT_DEALER:
            return {...state, currentDealer: action.payload}
        case SET_DEALER_INFO: {
            const {partner} = state
            if (!partner) {
                return state
            }

            const partnerClone = partner.clone
            partnerClone.newDealerInfo = action.payload

            return {...state, partner: partnerClone}
        }
        case UPDATE_DEALER_INFO: {
            const {partner} = state
            if (!partner) {
                return state
            }

            const dealers = [...partner.dealers]
            const dealerIndex = dealers.findIndex(d => d.id === action.payload.id)
            if (dealerIndex !== -1) {
                dealers[dealerIndex].info = action.payload.info
            }

            const partnerClone = partner.clone
            partnerClone.dealers = dealers

            return {...state, partner: partnerClone}
        }
        case UPDATE_DEALER: {
            const {partner} = state
            if (!partner) {
                return state
            }

            const dealers = [...partner.dealers]
            const dealerIndex = dealers.findIndex(d => d.id === action.payload.id)
            if (dealerIndex !== -1) {
                dealers[dealerIndex] = Dealer.parse({...dealers[dealerIndex], ...action.payload.dealer})
            } else {
                dealers.push(action.payload.dealer)
            }

            const partnerClone = partner.clone
            partnerClone.dealers = dealers

            return {...state, partner: partnerClone}
        }
        case RESET_SESSION: {
            localStorage.removeItem(ACCOUNT_TOKEN)
            localStorage.removeItem(ACCOUNT_REFRESH_TOKEN)
            return {...initialState, token: null, refreshToken: null}
        }
        case SET_USER_PERMISSIONS:
            return {...state, permissions: action.payload}
        case SET_REGIONS:
            return {...state, regions: action.payload}
        case SET_AFFILIATES:
            return {...state, affiliates: action.payload}
        case UPDATE_AFFILIATE: {
            const {affiliates} = state

            if (!affiliates) {
                return state
            }

            const updatedAffiliateIndex = affiliates.findIndex(a => a.id === action.payload.id)

            if (updatedAffiliateIndex !== -1) {
                const updatedAffiliates = [...affiliates]
                updatedAffiliates[updatedAffiliateIndex] = action.payload

                return {...state, affiliates: updatedAffiliates}
            }

            return {...state, affiliates: [...affiliates, action.payload]}
        }
        case DELETE_AFFILIATE: {
            if (!state.affiliates) {
                return state
            }

            const affiliates = [...state.affiliates]
            const deletedIndex = affiliates.findIndex(a => a.id === action.payload)

            if (deletedIndex !== -1) {
                affiliates.splice(deletedIndex, 1)
            }

            return {...state, affiliates}
        }
        case SET_EMPLOYEES:
            return {...state, employees: action.payload}
        case UPDATE_EMPLOYEE: {
            const {employees} = state
            if (!employees) {
                return state
            }

            const updatedEmployeeIndex = employees.findIndex(a => a.id === action.payload.id)

            if (updatedEmployeeIndex !== -1) {
                const updatedEmployees = [...employees]
                updatedEmployees[updatedEmployeeIndex] = action.payload

                return {
                    ...state,
                    employees: updatedEmployees
                }
            }

            return {
                ...state,
                employees: [...employees, action.payload]
            }
        }
        case DELETE_EMPLOYEE: {
            if (!state.employees) {
                return state
            }

            const employees = [...state.employees]
            const deletedIndex = employees.findIndex(a => a.id === action.payload)

            if (deletedIndex !== -1) {
                employees.splice(deletedIndex, 1)
            }

            return {...state, employees}
        }
        case SET_PAYMENT_STATE:
            return {...state, paymentState: action.payload}
        case SET_BONUS_SCORE:
            return {...state, bonusScore: action.payload}
        case SET_NOTIFICATIONS:
            return {...state, notifications: action.payload}
        case SET_NOTIFICATION: {
            const {notifications} = state
            const index = notifications.findIndex(n => n.id === action.payload.id)

            if (index !== -1) {
                return {
                    ...state,
                    notifications: [...notifications.slice(0, index), action.payload, ...notifications.slice(index + 1)]
                }
            }

            return {
                ...state,
                notifications: [action.payload, ...notifications]
            }
        }
        case SET_NEXT_DISCOUNT: {
            const {nextDiscount, nextDiscountAmount} = action.payload
            return {
                ...state,
                nextDiscount: {nextDiscount, nextDiscountAmount}
            }
        }
        case SET_IS_CASH_MODE: {
            return {
                ...state,
                isCashMode: action.payload
            }
        }
        case SET_SHOW_BALANCE: {
            return {
                ...state,
                showBalance: action.payload
            }
        }
        default:
            return state
    }
}
