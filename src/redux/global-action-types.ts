export const RESET_SESSION = 'RESET_SESSION'

export interface IResetSessionAction {
    type: typeof RESET_SESSION
}
