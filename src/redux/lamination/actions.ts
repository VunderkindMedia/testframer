import {Lamination} from '../../model/framer/lamination'
import {ISetLaminationAction, SET_LAMINATION} from './action-types'
import {Dispatch} from 'redux'
import {runLongNetworkOperation} from '../request/actions'
import {api} from '../../api'

export const setLamination = (lamination: Lamination[]): ISetLaminationAction => ({
    type: SET_LAMINATION,
    payload: lamination
})

export const loadLamination =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const lamination = await api.design.getLamination()
            dispatch(setLamination(lamination))
        })(dispatch)
    }
