import {Lamination} from '../../model/framer/lamination'
import {IResetSessionAction} from '../global-action-types'

export const SET_LAMINATION = 'SET_LAMINATION'

export interface ISetLaminationAction {
    type: typeof SET_LAMINATION
    payload: Lamination[]
}

export type TLaminationActionTypes = IResetSessionAction | ISetLaminationAction
