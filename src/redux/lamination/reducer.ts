import {Reducer} from 'redux'
import {SET_LAMINATION, TLaminationActionTypes} from './action-types'
import {Lamination} from '../../model/framer/lamination'
import {RESET_SESSION} from '../global-action-types'

export interface ILaminationState {
    all: Lamination[]
}

const initialState: ILaminationState = {
    all: []
}

export const lamination: Reducer<ILaminationState> = (state = initialState, action: TLaminationActionTypes) => {
    switch (action.type) {
        case SET_LAMINATION:
            return {...state, all: action.payload}
        case RESET_SESSION:
            return initialState
        default:
            return state
    }
}
