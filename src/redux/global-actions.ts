import {IResetSessionAction, RESET_SESSION} from './global-action-types'

export const resetSession = (): IResetSessionAction => ({
    type: RESET_SESSION
})
