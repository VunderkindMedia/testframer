import {Order} from '../../model/framer/order'
import {Construction} from '../../model/framer/construction'
import {Product} from '../../model/framer/product'
import {Good} from '../../model/framer/good'
import {IResetSessionAction} from '../global-action-types'

export const SET_ORDERS = 'SET_ORDERS'
export const SET_ORDERS_TOTAL_COUNT = 'SET_ORDERS_TOTAL_COUNT'
export const SET_ORDERS_ON_PAGE = 'SET_ORDERS_ON_PAGE'
export const SET_CURRENT_ORDER = 'SET_CURRENT_ORDER'
export const UPDATE_CURRENT_ORDER = 'UPDATE_CURRENT_ORDER'
export const SET_CURRENT_CONSTRUCTION = 'SET_CURRENT_CONSTRUCTION'
export const SET_CURRENT_PRODUCT = 'SET_CURRENT_PRODUCT'
export const SET_GENERIC_PRODUCT = 'SET_GENERIC_PRODUCT'
export const SET_ORDERS_PARTNER_GOODS = 'SET_ORDERS_PARTNER_GOODS'
export const SELECT_GOODS = 'SELECT_GOODS'
export const DESELECT_GOODS = 'DESELECT_GOODS'

export interface ISetOrdersAction {
    type: typeof SET_ORDERS
    payload: Order[]
}

export interface ISetOrdersTotalCountAction {
    type: typeof SET_ORDERS_TOTAL_COUNT
    payload: number
}

export interface ISetOrdersOnPageAction {
    type: typeof SET_ORDERS_ON_PAGE
    payload: number
}

export interface ISetCurrentOrderAction {
    type: typeof SET_CURRENT_ORDER
    payload: Order | null
}

export interface IUpdateCurrentOrderAction {
    type: typeof UPDATE_CURRENT_ORDER
    payload: Order
}

export interface ISetCurrentConstructionAction {
    type: typeof SET_CURRENT_CONSTRUCTION
    payload: Construction | null
}

export interface ISetCurrentProductAction {
    type: typeof SET_CURRENT_PRODUCT
    payload: Product | null
}

export interface ISetGenericProductAction {
    type: typeof SET_GENERIC_PRODUCT
    payload: Product | null
}

export interface ISetOrdersPartnerGoodsAction {
    type: typeof SET_ORDERS_PARTNER_GOODS
    payload: Good[]
}

export interface ISelectGoodsAction {
    type: typeof SELECT_GOODS
    payload: Good[]
}

export interface IDeselectGoodsAction {
    type: typeof DESELECT_GOODS
    payload: Good[]
}

export type TOrderActionTypes =
    | IResetSessionAction
    | ISetOrdersAction
    | ISetOrdersTotalCountAction
    | ISetOrdersOnPageAction
    | ISetCurrentOrderAction
    | IUpdateCurrentOrderAction
    | ISetCurrentConstructionAction
    | ISetCurrentProductAction
    | ISetGenericProductAction
    | ISetOrdersPartnerGoodsAction
    | ISelectGoodsAction
    | IDeselectGoodsAction
