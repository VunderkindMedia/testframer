import {
    DESELECT_GOODS,
    SELECT_GOODS,
    SET_CURRENT_CONSTRUCTION,
    SET_CURRENT_ORDER,
    SET_CURRENT_PRODUCT,
    SET_GENERIC_PRODUCT,
    SET_ORDERS,
    SET_ORDERS_ON_PAGE,
    SET_ORDERS_PARTNER_GOODS,
    SET_ORDERS_TOTAL_COUNT,
    UPDATE_CURRENT_ORDER,
    IDeselectGoodsAction,
    ISelectGoodsAction,
    ISetCurrentConstructionAction,
    ISetCurrentOrderAction,
    ISetCurrentProductAction,
    ISetGenericProductAction,
    ISetOrdersAction,
    ISetOrdersOnPageAction,
    ISetOrdersPartnerGoodsAction,
    ISetOrdersTotalCountAction,
    IUpdateCurrentOrderAction
} from './action-types'
import {Construction} from '../../model/framer/construction'
import {runLongNetworkOperation, showSpecialLoader} from '../request/actions'
import {Product} from '../../model/framer/product'
import {goBack, push, replace} from 'connected-react-router'
import {NAV_ORDER, NAV_ORDERS} from '../../constants/navigation'
import {LAST_ENERGY_CONSTRUCTION_ID} from '../../constants/local-storage'
import {showStatusbar} from '../statusbar/actions'
import {Discount} from '../../model/framer/discount'
import {Dispatch} from 'redux'
import {Client} from '../../model/framer/client'
import {setClients} from '../clients/actions'
import {OrderStates} from '../../model/framer/order-states'
import {Order} from '../../model/framer/order'
import {IAppState} from '../root-reducer'
import {ShippingTypes} from '../../model/framer/shipping-types'
import {Address} from '../../model/address'
import {ContactInfo} from '../../model/framer/contact-info'
import {API_ORDERS, API_ORDERS_WITH_PARTNER_GOODS} from '../../constants/api'
import {ConstructionTypes} from '../../model/framer/construction-types'
import {ProductTypes} from '../../model/framer/product-types'
import {Good} from '../../model/framer/good'
import {
    getOrderItemUrl,
    getOrdersFilter,
    getOrdersFilterParams,
    controlOrderCount,
    loadUserParameters,
    getConstructionWithVisibleUserParams,
    getUpdatedUserParams
} from '../../helpers/orders'
import {GoodSources} from '../../model/framer/good-sources'
import {resetTemplateSelector} from '../template-selector/actions'
import {api} from '../../api'
import {AnalyticsManager} from '../../analytics-manager'
import {setConfigContext, setCurrentContext, setUserParameterConstructionConfigs} from '../builder/actions'

export const setOrders = (orders: Order[]): ISetOrdersAction => ({
    type: SET_ORDERS,
    payload: orders
})

export const setOrdersTotalCount = (count: number): ISetOrdersTotalCountAction => ({
    type: SET_ORDERS_TOTAL_COUNT,
    payload: count
})

export const setOrdersOnPage = (count: number): ISetOrdersOnPageAction => ({
    type: SET_ORDERS_ON_PAGE,
    payload: count
})

export const setCurrentOrder = (order: Order | null): ISetCurrentOrderAction => ({
    type: SET_CURRENT_ORDER,
    payload: order
})

export const updateCurrentOrder = (order: Order): IUpdateCurrentOrderAction => ({
    type: UPDATE_CURRENT_ORDER,
    payload: order
})

export const setCurrentConstruction = (construction: Construction | null): ISetCurrentConstructionAction => ({
    type: SET_CURRENT_CONSTRUCTION,
    payload: construction
})

export const setCurrentProduct = (product: Product): ISetCurrentProductAction => ({
    type: SET_CURRENT_PRODUCT,
    payload: product
})

export const setGenericProduct = (product: Product | null): ISetGenericProductAction => ({
    type: SET_GENERIC_PRODUCT,
    payload: product
})

export const setOrdersPartnerGoods = (goods: Good[]): ISetOrdersPartnerGoodsAction => ({
    type: SET_ORDERS_PARTNER_GOODS,
    payload: goods
})

export const selectGoods = (goods: Good[]): ISelectGoodsAction => ({
    type: SELECT_GOODS,
    payload: goods
})

export const deselectGoods = (goods: Good[]): IDeselectGoodsAction => ({
    type: DESELECT_GOODS,
    payload: goods
})

const saveOrderIfChanged = async (order: Order): Promise<void> => {
    if (order.changed) {
        await api.orders.save(order)
    }
}

let lastSyncOrdersFilter = ''
export const loadOrders =
    (ordersFilter = '') =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            lastSyncOrdersFilter = ordersFilter
            const response = await api.orders.getByFilter(ordersFilter)

            if (ordersFilter !== lastSyncOrdersFilter) {
                return
            }

            const currentOrder = getState().orders.currentOrder
            if (response.orders.length > 0) {
                const order = response.orders.find(o => o.id === currentOrder?.id)
                dispatch(setCurrentOrder(order ?? response.orders[0]))
            } else {
                dispatch(setCurrentOrder(null))
            }

            dispatch(setOrdersTotalCount(response.totalCount))
            dispatch(setOrders(response.orders))

            const filterParams = getOrdersFilterParams(ordersFilter)
            if (filterParams.id && filterParams.id.length > 0) {
                const filter = getOrdersFilter({
                    ...filterParams,
                    offset: response.offset,
                    limit: response.limit
                })

                const id = +filterParams.id
                const order = response.orders.find(o => o.id === id)
                if (order) {
                    dispatch(setCurrentOrder(order))
                    dispatch(replace(`${NAV_ORDERS}${filter}`))
                }
            }
        })(dispatch)
    }

export const loadPartnerGoods =
    (filter = '') =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const response = await api.orders.getWithPartnerGoods(filter)

            const {orders} = response
            const allPartnerGoods: Good[] = []

            orders.forEach(order => {
                const partnerGoods = order.goods.filter(g => g.goodSource === GoodSources.Partner)
                order.goods = []
                partnerGoods.forEach(good => (good.order = order))
                allPartnerGoods.push(...partnerGoods)
            })

            dispatch(setOrdersPartnerGoods(allPartnerGoods))
        })(dispatch)
    }

// TODO: возмможно потом стоит пересмотеть задание конструкции
let lastSyncConstructionId = ''
export const setCurrentConstructionAsync =
    (construction: Construction) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        lastSyncConstructionId = construction.id

        await runLongNetworkOperation(async () => {
            const promises: Promise<void>[] = []
            for (const product of construction.products) {
                if (lastSyncConstructionId === construction.id) {
                    promises.push(
                        new Promise(resolve => {
                            Promise.all([
                                api.design
                                    .getProfilesAndFurniture(
                                        product.constructionTypeId ?? ConstructionTypes.Okno,
                                        product.productionTypeId ?? ProductTypes.Okno
                                    )
                                    .then(({profiles, furniture}) => {
                                        product.availableProfiles = profiles
                                        product.availableFurniture = furniture
                                    }),
                                api.design.getFillings(product.profile.id).then(availableFillings => {
                                    product.availableFillings = availableFillings
                                })
                            ])
                                .then(() => resolve())
                                .catch(err => console.error(err))
                        })
                    )
                }
            }
            const orderClone = getState().orders.currentOrder?.clone
            const constructionClone = orderClone?.findConstructionById(construction.id)
            if (!orderClone || !constructionClone) {
                return
            }
            try {
                await Promise.all(promises)
                const [connectorsParameters, configMap] = await Promise.all([
                    api.design.getConnectorsParameters(construction.products[0].profile.id),
                    loadUserParameters(construction, orderClone.city?.id)
                ])

                construction.connectorsParameters = connectorsParameters
                construction.update()

                getConstructionWithVisibleUserParams(construction, configMap, (filling, params) => {
                    const updatedParams = getUpdatedUserParams(filling.userParameters, params)
                    if (!updatedParams) {
                        return
                    }
                    filling.userParameters = updatedParams
                })

                const energyConstructionId = localStorage.getItem(LAST_ENERGY_CONSTRUCTION_ID)
                if (energyConstructionId === construction.id) {
                    construction.energyProductType = 'Basic I'
                    construction.haveEnergyProducts = true
                }

                dispatch(setCurrentConstruction(construction))
                dispatch(setUserParameterConstructionConfigs(configMap))
            } catch (err) {
                console.error(err)
            }
        })(dispatch)
    }

export const loadOrder =
    (orderId: number | string) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const orders: Order[] = getState().orders.all
            let order = orders.find(order => order.id === orderId)
            if (!order) {
                order = await api.orders.getById(orderId)
            }
            order.needRecalc = true

            dispatch(setCurrentOrder(order))
        })(dispatch)
    }

export const createOrder =
    (draftOrder: Order, redirectToBuilder = false) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(
            async () => {
                dispatch(setCurrentOrder(draftOrder))
                dispatch(setCurrentConstruction(draftOrder.constructions[0]))
                const isRestrictedMode = getState().settings.isRestrictedMode
                !isRestrictedMode &&
                    dispatch(push(`${NAV_ORDER}?tabs=${draftOrder.constructions[0].id}&activeTab=${draftOrder.constructions[0].id}`))

                const order = await api.orders.create(draftOrder)
                const fullOrder = await api.orders.getById(order.id)
                fullOrder.addAction('Создали заказ')
                fullOrder.needRecalc = true

                dispatch(setCurrentOrder(fullOrder))
                dispatch(resetTemplateSelector())

                if (isRestrictedMode) {
                    return
                }

                if (redirectToBuilder) {
                    dispatch(push(getOrderItemUrl(fullOrder.id, fullOrder.constructions[0].id)))
                }

                AnalyticsManager.trackEvent({
                    name: 'Создали новый заказ',
                    target: 'create-new-order',
                    data: {
                        id: order.id,
                        orderNumber: order.fullOrderNumber
                    }
                })

                controlOrderCount(getState().account.login)
            },
            () => {
                dispatch(goBack())
                dispatch(showStatusbar('Не получилось создать заказ, пожалуйста, попробуйте снова через несколько минут.', true))
            }
        )(dispatch)
    }

export const recalculateOrder =
    (draftOrder: Order, cb?: () => void) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const {isCashMode} = getState().account
            const order = await api.orders.recalculate(draftOrder, isCashMode)
            const fullOrder = await api.orders.getById(order.id)

            dispatch(resetTemplateSelector())
            localStorage.removeItem(LAST_ENERGY_CONSTRUCTION_ID)

            if (order.calcResp) {
                if (order.calcResp.exceptionMessage) {
                    dispatch(showStatusbar(order.calcResp.exceptionMessage, true))
                } else if (order.calcResp.timeoutError) {
                    dispatch(showStatusbar(order.calcResp.timeoutError, true))
                    fullOrder.changed = true
                }
            }

            dispatch(setCurrentOrder(fullOrder))

            const {currentContext} = getState().builder
            if (currentContext) {
                const {node} = fullOrder.findConstructionAndProductAndNodeByNodeId(currentContext.nodeId)
                dispatch(setCurrentContext(node))
                !node && dispatch(setConfigContext(null))
            }
            cb && cb()
        })(dispatch)
    }

export const deleteOrder =
    (orderId: number) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.orders.delete(orderId)

            const filter = getState().router.location.search
            const ordersResponse = await api.orders.getByFilter(filter)

            dispatch(setCurrentOrder(ordersResponse.orders[0]))
            dispatch(setOrders(ordersResponse.orders))
        })(dispatch)
    }

export const setClientToOrder =
    (order: Order, client: Client) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            let clientId = client.id
            if (clientId === -1) {
                const newClient = await api.clients.create(client)
                const clients = getState().clients.all.splice(0)
                clients.unshift(newClient)
                dispatch(setClients(clients))
                clientId = newClient.id
            }

            const updatedOrder = await api.orders.setClient(order.id, clientId)
            const fullOrder = await api.orders.getById(updatedOrder.id)

            dispatch(setCurrentOrder(fullOrder))
        })(dispatch)
    }

export const updateOrderDiscount =
    (order: Order, discount: Discount) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const updatedOrder = await api.orders.updateDiscount(order.id, discount)
            updatedOrder.addAction(`Добавили скидку ${discount.toString()} в заказе ${updatedOrder.id}`)
            dispatch(updateCurrentOrder(updatedOrder))
        })(dispatch)
    }

export const updateMountingDate =
    (order: Order, mountingDate: Date) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const updatedOrder = await api.orders.updateMountingDate(order.id, mountingDate.toISOString())
            dispatch(updateCurrentOrder(updatedOrder))
        })(dispatch)
    }

export const loadOffer =
    (order: Order) =>
    (dispatch: Dispatch, getState: () => IAppState): void => {
        const link = document.createElement('a')
        link.setAttribute('href', `${API_ORDERS}/${order.id}/offer?t=${getState().account.token}`)
        link.click()
    }

export const loadPassport =
    (order: Order) =>
    (dispatch: Dispatch, getState: () => IAppState): void => {
        const link = document.createElement('a')
        link.setAttribute('href', `${API_ORDERS}/${order.id}/passport?t=${getState().account.token}`)
        link.click()
    }

export const loadOrderXml =
    (order: Order) =>
    (dispatch: Dispatch, getState: () => IAppState): void => {
        const link = document.createElement('a')
        link.setAttribute('href', `${API_ORDERS}/${order.id}/xml?t=${getState().account.token}`)
        link.click()
    }

export const loadPartnerGoodsReport =
    (filter: string) =>
    (dispatch: Dispatch, getState: () => IAppState): void => {
        const link = document.createElement('a')
        link.setAttribute('href', `${API_ORDERS_WITH_PARTNER_GOODS}/print${filter}&t=${getState().account.token}`)
        link.click()
    }

export const transferToProduction =
    (
        order: Order,
        warehouseId: number,
        shippingType: ShippingTypes,
        address: Address | null = null,
        contactPerson: ContactInfo | null = null,
        shippingComment: string | null = null,
        reserveBonusScore: number,
        isFramerPointsShouldBeUsed: boolean
    ) =>
    async (dispatch: Dispatch): Promise<void> => {
        dispatch(showSpecialLoader(true))

        await runLongNetworkOperation(
            async () => {
                if (order.state === OrderStates.Draft) {
                    await api.checkout.saveShippingInfo(order.id, warehouseId, shippingType, address, contactPerson, shippingComment)

                    const reservedOrder = await api.checkout.reserve([order.id], reserveBonusScore, isFramerPointsShouldBeUsed)
                    if (!reservedOrder.windrawOrderNumber || reservedOrder.windrawOrderNumber.length === 0) {
                        throw new Error('Не получили номер заказа на производстве')
                    }
                }
                const updatedOrder = await api.orders.getById(order.id)

                dispatch(setCurrentOrder(updatedOrder))

                AnalyticsManager.registerPurchase(updatedOrder)
            },
            () =>
                dispatch(
                    showStatusbar('Не получилось передать заказ на производство, пожалуйста, попробуйте снова через несколько минут', true)
                )
        )(dispatch)

        dispatch(showSpecialLoader(false))
    }

export const saveOrder =
    (order: Order) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.orders.save(order)
            const fullOrder = await api.orders.getById(order.id)

            dispatch(setCurrentOrder(fullOrder))
        })(dispatch)
    }

export const addGood2 =
    (order: Order, good: Good) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            if (order) {
                await saveOrderIfChanged(order)
            }

            const updatedOrder = await api.goods.add2(order.id, good)
            updatedOrder.addAction(`Добавили ${good.toString()}`)

            dispatch(setCurrentOrder(updatedOrder))
            dispatch(resetTemplateSelector())
        })(dispatch)
    }

export const duplicateOrder =
    (orderId: number) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const {orders} = getState()
            const duplicatedOrder = await api.orders.duplicateOrder(orderId)

            dispatch(setOrders([duplicatedOrder, ...orders.all]))
            controlOrderCount(getState().account.login)
        })(dispatch)
    }

export const cancelOrder =
    (orderId: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const updatedOrder = await api.booking.unbook(orderId)
            if (updatedOrder) {
                dispatch(setCurrentOrder(updatedOrder))
                dispatch(showStatusbar('Запрошена отмена заказа'))
            } else {
                dispatch(showStatusbar('Не удалось запросить отмену заказа', true))
            }
        })(dispatch)
    }
