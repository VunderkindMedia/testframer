import {Reducer} from 'redux'
import {Order} from '../../model/framer/order'
import {
    SET_CURRENT_CONSTRUCTION,
    SET_CURRENT_PRODUCT,
    SET_GENERIC_PRODUCT,
    SET_ORDERS,
    SET_CURRENT_ORDER,
    SET_ORDERS_TOTAL_COUNT,
    SET_ORDERS_ON_PAGE,
    UPDATE_CURRENT_ORDER,
    SELECT_GOODS,
    DESELECT_GOODS,
    SET_ORDERS_PARTNER_GOODS,
    TOrderActionTypes
} from './action-types'
import {Construction} from '../../model/framer/construction'
import {Product} from '../../model/framer/product'
import {Good} from '../../model/framer/good'
import {RESET_SESSION} from '../global-action-types'

export interface IOrdersState {
    all: Order[]
    ordersOnPage: number
    totalCount: number
    currentOrder: Order | null
    currentConstruction: Construction | null
    currentProduct: Product | null
    genericProduct: Product | null
    partnerGoods: Good[]
    selectedPartnerGoods: Good[]
}

const initialState: IOrdersState = {
    all: [],
    ordersOnPage: -1,
    totalCount: 0,
    currentOrder: null,
    currentConstruction: null,
    currentProduct: null,
    genericProduct: null,
    partnerGoods: [],
    selectedPartnerGoods: []
}

export const orders: Reducer<IOrdersState> = (state = initialState, action: TOrderActionTypes) => {
    switch (action.type) {
        case SET_ORDERS:
            return {...state, all: action.payload}
        case SET_CURRENT_CONSTRUCTION: {
            const currentProduct = action.payload?.products.find(p => p.productGuid === state.currentProduct?.productGuid)
            return {
                ...state,
                currentConstruction: action.payload,
                currentProduct: currentProduct ?? action.payload?.products[0] ?? null
            }
        }
        case SET_CURRENT_PRODUCT:
            return {...state, currentProduct: action.payload}
        case SET_CURRENT_ORDER: {
            if (!action.payload) {
                return {...state, currentOrder: null, currentConstruction: null, currentProduct: null}
            }

            const orders = [...state.all]
            const orderIndex = orders.findIndex(o => o.id === action.payload?.id)

            if (orderIndex !== -1) {
                orders[orderIndex] = action.payload
            }

            return {...state, all: orders, currentOrder: action.payload}
        }
        case SET_GENERIC_PRODUCT:
            return {...state, genericProduct: action.payload}
        case UPDATE_CURRENT_ORDER: {
            const {currentOrder, all} = state

            if (!currentOrder) {
                return state
            }

            const updatedOrder: Order = action.payload
            updatedOrder.changed = true

            const orders = [...all]
            const orderIndex = orders.findIndex(o => o.id === updatedOrder.id)
            if (orderIndex !== -1) {
                orders[orderIndex] = updatedOrder
            }

            if (updatedOrder.constructions.length === 0) {
                return {
                    ...state,
                    currentOrder: updatedOrder,
                    currentConstruction: null,
                    currentProduct: null,
                    all: orders
                }
            }

            let {currentConstruction, currentProduct} = state
            if (!currentConstruction) {
                currentConstruction = updatedOrder.constructions[0]
                currentProduct = currentConstruction.products[0]
            }

            if (!currentConstruction || !currentProduct) {
                return state
            }

            const updatedConstruction = updatedOrder.findConstructionById(currentConstruction.id)
            const updatedProduct = updatedConstruction
                ? updatedConstruction.findProductByProductGuid(currentProduct.productGuid) ?? updatedConstruction.products[0]
                : null

            return {
                ...state,
                currentOrder: updatedOrder,
                currentConstruction: updatedConstruction,
                currentProduct: updatedProduct,
                all: orders
            }
        }
        case SET_ORDERS_PARTNER_GOODS:
            return {...state, partnerGoods: action.payload}
        case SELECT_GOODS: {
            const goodsSet = new Set(state.selectedPartnerGoods)
            const selectedGoods: Good[] = action.payload
            selectedGoods.forEach(good => goodsSet.add(good))
            return {...state, selectedPartnerGoods: [...goodsSet]}
        }
        case DESELECT_GOODS: {
            const goodsSet = new Set(state.selectedPartnerGoods)
            const deselectedGoods: Good[] = action.payload
            deselectedGoods.forEach(good => goodsSet.delete(good))
            return {...state, selectedPartnerGoods: [...goodsSet]}
        }
        case SET_ORDERS_TOTAL_COUNT:
            return {...state, totalCount: action.payload}
        case SET_ORDERS_ON_PAGE:
            return {...state, ordersOnPage: action.payload}
        case RESET_SESSION:
            return {...initialState, ordersOnPage: state.ordersOnPage}
        default:
            return state
    }
}
