import {combineReducers, Reducer} from 'redux'
import {connectRouter, RouterState} from 'connected-react-router'
import {builder, IBuilderState} from './builder/reducer'
import {orders, IOrdersState} from './orders/reducer'
import {statusbar, IStatusbarState} from './statusbar/reducer'
import {account, IAccountState} from './account/reducer'
import {clients, IClientsState} from './clients/reducer'
import {request, IRequestReducerState} from './request/reducer'
import {browser, IBrowserState} from './browser/reducer'
import {settings, ISettingsState} from './settings/reducer'
import {History} from 'history'
import {lamination, ILaminationState} from './lamination/reducer'
import {booking, IBookingState} from './booking/reducer'
import {goods, IGoodsState} from './goods/reducer'
import {ITemplateSelectorState, templateSelector} from './template-selector/reducer'
import {IChatState, chat} from './chat/reducer'
import {IServicesState, services} from './services/reducer'
import {calendar, ICalendarState} from './calendar/reducer'
import {cms, ICmsState} from './cms/reducer'
import {IMountingsState, mountings} from './mountings/reducer'
import {IPaymentsState, payments} from './payments/reducer'

export interface IAppState {
    router: RouterState
    builder: IBuilderState
    orders: IOrdersState
    statusbar: IStatusbarState
    lamination: ILaminationState
    account: IAccountState
    clients: IClientsState
    request: IRequestReducerState
    browser: IBrowserState
    booking: IBookingState
    goods: IGoodsState
    templateSelector: ITemplateSelectorState
    chat: IChatState
    services: IServicesState
    calendar: ICalendarState
    cms: ICmsState
    mountings: IMountingsState
    settings: ISettingsState
    payments: IPaymentsState
}

export const createRootReducer = (history: History): Reducer<IAppState> =>
    combineReducers<IAppState>({
        router: connectRouter(history),
        builder,
        orders,
        statusbar,
        lamination,
        account,
        clients,
        request,
        browser,
        booking,
        goods,
        templateSelector,
        chat,
        services,
        calendar,
        cms,
        mountings,
        settings,
        payments
    })
