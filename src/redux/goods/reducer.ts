import {Reducer} from 'redux'
import {GoodGroup} from '../../model/framer/good-group'
import {
    SET_FACTORY_GOOD_GROUPS,
    SET_FACTORY_GOODS,
    SET_PARTNER_GOOD_GROUPS,
    SET_PARTNER_GOODS,
    SET_PARTNER_GOODS_TEMPLATES,
    ADD_PARTNER_GOOD,
    UPDATE_PARTNER_GOOD,
    REMOVE_PARTNER_GOOD,
    TGoodsActionTypes
} from './action-types'
import {Good} from '../../model/framer/good'
import {RESET_SESSION} from '../global-action-types'

export interface IGoodsState {
    factoryGoodGroups: GoodGroup[]
    factoryGoods: Good[]
    partnerGoodGroups: GoodGroup[]
    partnerGoods: Good[]
    partnerGoodsTemplates: Good[]
}

const initialState: IGoodsState = {
    factoryGoodGroups: [],
    factoryGoods: [],
    partnerGoodGroups: [],
    partnerGoods: [],
    partnerGoodsTemplates: []
}

export const goods: Reducer<IGoodsState> = (state = initialState, action: TGoodsActionTypes) => {
    switch (action.type) {
        case SET_FACTORY_GOOD_GROUPS: {
            return {...state, factoryGoodGroups: action.payload}
        }
        case SET_FACTORY_GOODS: {
            return {...state, factoryGoods: action.payload}
        }
        case SET_PARTNER_GOOD_GROUPS: {
            return {...state, partnerGroups: action.payload}
        }
        case SET_PARTNER_GOODS: {
            return {...state, partnerGoods: action.payload}
        }
        case SET_PARTNER_GOODS_TEMPLATES: {
            return {...state, partnerGoodsTemplates: action.payload}
        }
        case ADD_PARTNER_GOOD: {
            return {...state, partnerGoods: [...state.partnerGoods, action.payload]}
        }
        case UPDATE_PARTNER_GOOD: {
            const updatedIndex = state.partnerGoods.findIndex(good => good.id === action.payload.id)

            if (updatedIndex !== -1) {
                const partnerGoods = [...state.partnerGoods]

                partnerGoods[updatedIndex] = action.payload
                return {...state, partnerGoods}
            }
            return {...state}
        }
        case REMOVE_PARTNER_GOOD: {
            const updatedIndex = state.partnerGoods.findIndex(good => good.id === action.payload)

            if (updatedIndex !== -1) {
                const partnerGoods = [...state.partnerGoods]
                partnerGoods.splice(updatedIndex, 1)

                return {
                    ...state,
                    partnerGoods
                }
            }
            return {...state}
        }
        case RESET_SESSION:
            return initialState
        default:
            return state
    }
}
