import {GoodGroup} from '../../model/framer/good-group'
import {
    SET_FACTORY_GOOD_GROUPS,
    SET_FACTORY_GOODS,
    SET_PARTNER_GOOD_GROUPS,
    SET_PARTNER_GOODS,
    SET_PARTNER_GOODS_TEMPLATES,
    ADD_PARTNER_GOOD,
    UPDATE_PARTNER_GOOD,
    REMOVE_PARTNER_GOOD,
    ISetFactoryGoodGroupsAction,
    ISetFactoryGoodsAction,
    ISetPartnerGoodGroupsAction,
    ISetPartnerGoodsAction,
    ISetPartnerGoodsTemplatesAction,
    IAddPartnerGoodAction,
    IUpdatePartnerGoodAction,
    IRemovePartnerGoodAction
} from './action-types'
import {Dispatch} from 'redux'
import {runLongNetworkOperation} from '../request/actions'
import {Good} from '../../model/framer/good'
import {api} from '../../api'

export const setFactoryGoodGroups = (groups: GoodGroup[]): ISetFactoryGoodGroupsAction => ({
    type: SET_FACTORY_GOOD_GROUPS,
    payload: groups
})

export const setFactoryGoods = (goods: Good[]): ISetFactoryGoodsAction => ({
    type: SET_FACTORY_GOODS,
    payload: goods
})

export const setPartnerGoodGroups = (groups: GoodGroup[]): ISetPartnerGoodGroupsAction => ({
    type: SET_PARTNER_GOOD_GROUPS,
    payload: groups
})

export const setPartnerGoods = (goods: Good[]): ISetPartnerGoodsAction => ({
    type: SET_PARTNER_GOODS,
    payload: goods
})

const setPartnerGoodsTemplates = (goods: Good[]): ISetPartnerGoodsTemplatesAction => ({
    type: SET_PARTNER_GOODS_TEMPLATES,
    payload: goods
})

const addPartnerGood = (good: Good): IAddPartnerGoodAction => ({
    type: ADD_PARTNER_GOOD,
    payload: good
})

const updatePartnerGood = (good: Good): IUpdatePartnerGoodAction => ({
    type: UPDATE_PARTNER_GOOD,
    payload: good
})

const removePartnerGood = (id: number): IRemovePartnerGoodAction => ({
    type: REMOVE_PARTNER_GOOD,
    payload: id
})

export const loadFactoryGoodGroups =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const goodGroups = await api.goods.getGroups()
            dispatch(setFactoryGoodGroups(goodGroups))
        })(dispatch)
    }

export const loadFactoryGoods =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const goods = await api.goods.get()
            dispatch(setFactoryGoods(goods))
        })(dispatch)
    }

export const loadPartnerGoodGroups =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const goodGroups = await api.partnerGoods.getGroups()
            dispatch(setPartnerGoodGroups(goodGroups))
        })(dispatch)
    }

export const loadPartnerGoods =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const goods = await api.partnerGoods.get()
            dispatch(setPartnerGoods(goods))
        })(dispatch)
    }

export const loadPartnerGoodsTemplates =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const goods = await api.partnerGoods.getTemplates()
            dispatch(setPartnerGoodsTemplates(goods))
        })(dispatch)
    }

export const savePartnerGood =
    (good: Good) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const savedGood = await api.partnerGoods.save(good)
            dispatch(good.id ? updatePartnerGood(savedGood) : addPartnerGood(savedGood))
        })(dispatch)
    }

export const deletePartnerGood =
    (id: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.partnerGoods.remove(id)
            dispatch(removePartnerGood(id))
        })(dispatch)
    }
