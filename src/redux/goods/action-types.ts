import {GoodGroup} from '../../model/framer/good-group'
import {Good} from '../../model/framer/good'
import {IResetSessionAction} from '../global-action-types'

export const SET_FACTORY_GOOD_GROUPS = 'SET_FACTORY_GOOD_GROUPS'
export const SET_FACTORY_GOODS = 'SET_FACTORY_GOODS'
export const SET_PARTNER_GOOD_GROUPS = 'SET_PARTNER_GOOD_GROUPS'
export const SET_PARTNER_GOODS = 'SET_PARTNER_GOODS'
export const SET_PARTNER_GOODS_TEMPLATES = 'SET_PARTNER_GOODS_TEMPLATES'
export const ADD_PARTNER_GOOD = 'ADD_PARTNER_GOOD'
export const UPDATE_PARTNER_GOOD = 'UPDATE__PARTNER_GOOD'
export const REMOVE_PARTNER_GOOD = 'REMOVE_PARTNER_GOOD'

export interface ISetFactoryGoodGroupsAction {
    type: typeof SET_FACTORY_GOOD_GROUPS
    payload: GoodGroup[]
}

export interface ISetFactoryGoodsAction {
    type: typeof SET_FACTORY_GOODS
    payload: Good[]
}

export interface ISetPartnerGoodGroupsAction {
    type: typeof SET_PARTNER_GOOD_GROUPS
    payload: GoodGroup[]
}

export interface ISetPartnerGoodsAction {
    type: typeof SET_PARTNER_GOODS
    payload: Good[]
}

export interface ISetPartnerGoodsTemplatesAction {
    type: typeof SET_PARTNER_GOODS_TEMPLATES
    payload: Good[]
}

export interface IAddPartnerGoodAction {
    type: typeof ADD_PARTNER_GOOD
    payload: Good
}

export interface IUpdatePartnerGoodAction {
    type: typeof UPDATE_PARTNER_GOOD
    payload: Good
}

export interface IRemovePartnerGoodAction {
    type: typeof REMOVE_PARTNER_GOOD
    payload: number
}

export type TGoodsActionTypes =
    | IResetSessionAction
    | ISetFactoryGoodGroupsAction
    | ISetFactoryGoodsAction
    | ISetPartnerGoodGroupsAction
    | ISetPartnerGoodsAction
    | ISetPartnerGoodsTemplatesAction
    | IAddPartnerGoodAction
    | IUpdatePartnerGoodAction
    | IRemovePartnerGoodAction
