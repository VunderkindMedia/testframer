import {IResetSessionAction} from '../global-action-types'
import {CalendarEvent} from '../../model/framer/calendar/calendar-event'

export const SET_CALENDAR_EVENTS = 'SET_CALENDAR_EVENTS'

export interface ISetCalendarEventsAction {
    type: typeof SET_CALENDAR_EVENTS
    payload: CalendarEvent[]
}

export type TCalendarActionTypes = IResetSessionAction | ISetCalendarEventsAction
