import {Reducer} from 'redux'
import {SET_CALENDAR_EVENTS, TCalendarActionTypes} from './action-types'
import {RESET_SESSION} from '../global-action-types'
import {CalendarEvent} from '../../model/framer/calendar/calendar-event'
import {CalendarEventTypes} from '../../model/framer/calendar/calendar-event-types'

export interface ICalendarState {
    events: CalendarEvent[]
}

export const defaultEventTypes = [
    CalendarEventTypes.Measuring,
    CalendarEventTypes.Mounting,
    CalendarEventTypes.OtherEvent,
    CalendarEventTypes.Shipping
]

const initialState: ICalendarState = {
    events: []
}

export const calendar: Reducer<ICalendarState> = (state = initialState, action: TCalendarActionTypes) => {
    switch (action.type) {
        case SET_CALENDAR_EVENTS:
            return {...state, events: action.payload}
        case RESET_SESSION:
            return initialState
        default:
            return state
    }
}
