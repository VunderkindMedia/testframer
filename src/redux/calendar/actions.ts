import {ISetCalendarEventsAction, SET_CALENDAR_EVENTS} from './action-types'
import {Dispatch} from 'redux'
import {runLongNetworkOperation} from '../request/actions'
import {api} from '../../api'
import {CalendarEvent} from '../../model/framer/calendar/calendar-event'
import {ICalendarRequestEvent} from '../../model/framer/calendar/i-calendar-request-event'

export const setCalendarEvents = (events: CalendarEvent[]): ISetCalendarEventsAction => ({
    type: SET_CALENDAR_EVENTS,
    payload: events
})

export const loadCalendarEvents =
    (filters: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const response = await api.calendar.getEvents(filters)
            dispatch(setCalendarEvents(CalendarEvent.parseEvents(response)))
        })(dispatch)
    }

export const loadCalendarEventsByOrderId =
    (orderId: number, from: Date, to: Date) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const response = await api.calendar.getEventsByOrderId(orderId, from, to)
            dispatch(setCalendarEvents(CalendarEvent.parseEvents(response)))
        })(dispatch)
    }

export const createEvent =
    (event: ICalendarRequestEvent) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            await api.calendar.createEvent(event)
        })(dispatch)
    }
