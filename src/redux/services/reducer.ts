import {Reducer} from 'redux'
import {
    SET_CONSTRUCTION_AVAILABLE_SERVICES,
    SET_INSTALLATIONS,
    SET_ORDER_AVAILABLE_SERVICES,
    TServicesActionTypes,
    UPDATE_INSTALLATION
} from './action-types'
import {Installation} from '../../model/framer/installation'
import {Service} from '../../model/framer/service'
import {RESET_SESSION} from '../global-action-types'

export interface IServicesState {
    installations: Installation[]
    orderAvailableServices: Service[]
    constructionAvailableServices: {[key: string]: Service[]}
}

const initialState: IServicesState = {
    installations: [],
    orderAvailableServices: [],
    constructionAvailableServices: {}
}

export const services: Reducer<IServicesState> = (state = initialState, action: TServicesActionTypes): IServicesState => {
    switch (action.type) {
        case SET_INSTALLATIONS: {
            return {...state, installations: action.payload}
        }
        case UPDATE_INSTALLATION: {
            const installation = state.installations[0]
            const {constructionsCosts} = installation
            const index = constructionsCosts.findIndex(item => item.id === action.payload.id)
            return {
                ...state,
                installations: [
                    {
                        ...installation,
                        constructionsCosts: [...constructionsCosts.slice(0, index), action.payload, ...constructionsCosts.slice(index + 1)]
                    }
                ]
            }
        }
        case SET_ORDER_AVAILABLE_SERVICES: {
            return {...state, orderAvailableServices: action.payload, constructionAvailableServices: {}}
        }
        case SET_CONSTRUCTION_AVAILABLE_SERVICES: {
            return {
                ...state,
                constructionAvailableServices: {
                    ...state.constructionAvailableServices,
                    [action.payload.constructionId]: action.payload.services
                }
            }
        }
        case RESET_SESSION:
            return initialState
        default:
            return state
    }
}
