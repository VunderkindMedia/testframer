import {
    ISetConstructionAvailableServicesAction,
    ISetInstallationsAction,
    ISetOrderAvailableServicesAction,
    IUpdateInstallationAction,
    SET_CONSTRUCTION_AVAILABLE_SERVICES,
    SET_INSTALLATIONS,
    SET_ORDER_AVAILABLE_SERVICES,
    UPDATE_INSTALLATION
} from './action-types'
import {Dispatch} from 'redux'
import {runLongNetworkOperation} from '../request/actions'
import {api} from '../../api'
import {Installation} from '../../model/framer/installation'
import {ConstructionCost} from '../../model/framer/construction-cost'
import {Service} from '../../model/framer/service'

const setInstallations = (installations: Installation[]): ISetInstallationsAction => ({
    type: SET_INSTALLATIONS,
    payload: installations
})

const updateInstallationAction = (constructionCost: ConstructionCost): IUpdateInstallationAction => ({
    type: UPDATE_INSTALLATION,
    payload: constructionCost
})

const setOrderAvailableServices = (services: Service[]): ISetOrderAvailableServicesAction => ({
    type: SET_ORDER_AVAILABLE_SERVICES,
    payload: services
})

const setConstructionAvailableServices = (constructionId: string, services: Service[]): ISetConstructionAvailableServicesAction => ({
    type: SET_CONSTRUCTION_AVAILABLE_SERVICES,
    payload: {constructionId, services}
})

export const loadInstallations =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const installations = await api.installations.get()
            dispatch(setInstallations(installations))
        })(dispatch)
    }

export const updateInstallation =
    (construction: ConstructionCost) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const updatedConstruction = await api.installations.update(construction)
            dispatch(updateInstallationAction(updatedConstruction))
        })(dispatch)
    }

export const loadOrderAvailableServices =
    (orderId: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const services = await api.services.getByOrderId(orderId)
            dispatch(setOrderAvailableServices(services))
        })(dispatch)
    }

export const loadConstructionAvailableServices =
    (orderId: number, constructionId: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const services = await api.services.getByOrderIdAndConstructionId(orderId, constructionId)
            dispatch(setConstructionAvailableServices(constructionId, services))
        })(dispatch)
    }
