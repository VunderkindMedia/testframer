import {Installation} from '../../model/framer/installation'
import {ConstructionCost} from '../../model/framer/construction-cost'
import {Service} from '../../model/framer/service'
import {IResetSessionAction} from '../global-action-types'

export const SET_INSTALLATIONS = 'SET_INSTALLATIONS'
export const UPDATE_INSTALLATION = 'UPDATE_INSTALLATION'
export const SET_ORDER_AVAILABLE_SERVICES = 'SET_ORDER_AVAILABLE_SERVICES'
export const SET_CONSTRUCTION_AVAILABLE_SERVICES = 'SET_CONSTRUCTION_AVAILABLE_SERVICES'

export interface ISetInstallationsAction {
    type: typeof SET_INSTALLATIONS
    payload: Installation[]
}

export interface IUpdateInstallationAction {
    type: typeof UPDATE_INSTALLATION
    payload: ConstructionCost
}

export interface ISetOrderAvailableServicesAction {
    type: typeof SET_ORDER_AVAILABLE_SERVICES
    payload: Service[]
}

export interface ISetConstructionAvailableServicesAction {
    type: typeof SET_CONSTRUCTION_AVAILABLE_SERVICES
    payload: {
        constructionId: string
        services: Service[]
    }
}

export type TServicesActionTypes =
    | IResetSessionAction
    | ISetInstallationsAction
    | IUpdateInstallationAction
    | ISetOrderAvailableServicesAction
    | ISetConstructionAvailableServicesAction
