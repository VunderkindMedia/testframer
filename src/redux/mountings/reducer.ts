import {Reducer} from 'redux'
import {
    SET_MOUNTINGS,
    SET_MOUNTINGS_TOTAL_COUNT,
    SET_CURRENT_MOUNTING,
    UPDATE_CURRENT_MOUNTING,
    DELETE_MOUNTING,
    SET_MOUNTINGS_ON_PAGE,
    TMountingsActionTypes
} from './action-types'
import {RESET_SESSION} from '../global-action-types'
import {Mounting} from '../../model/framer/mounting'

export interface IMountingsState {
    all: Mounting[]
    totalCount: number
    current: Mounting | null
    mountingsOnPage: number
}

const initialState: IMountingsState = {
    all: [],
    totalCount: 0,
    current: null,
    mountingsOnPage: 0
}

export const mountings: Reducer<IMountingsState> = (state = initialState, action: TMountingsActionTypes) => {
    switch (action.type) {
        case SET_MOUNTINGS:
            return {...state, all: action.payload}
        case SET_MOUNTINGS_TOTAL_COUNT:
            return {...state, totalCount: action.payload}
        case SET_CURRENT_MOUNTING:
            return {...state, current: action.payload}
        case SET_MOUNTINGS_ON_PAGE:
            return {...state, mountingsOnPage: action.payload}
        case UPDATE_CURRENT_MOUNTING: {
            const mountings = [...state.all]
            const currentIndex = mountings.findIndex(m => m.id === action.payload.id)

            if (currentIndex !== -1) {
                mountings[currentIndex] = action.payload
            }

            return {...state, all: mountings, current: action.payload}
        }
        case DELETE_MOUNTING: {
            const {all, current} = state
            const currentIndex = all.findIndex(m => m.id === action.payload)

            if (currentIndex !== -1) {
                const mountings = [...all.slice(0, currentIndex), ...all.slice(currentIndex + 1)]
                const currentMounting = current?.id === action.payload ? null : current
                return {...state, all: mountings, current: currentMounting}
            }

            return state
        }
        case RESET_SESSION:
            return {...initialState, mountingsOnPage: state.mountingsOnPage}
        default:
            return state
    }
}
