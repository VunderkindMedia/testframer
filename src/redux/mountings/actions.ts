import {Mounting} from '../../model/framer/mounting'
import {
    ISetMountingsAction,
    ISetMountingsTotalCountAction,
    ISetCurrentMountingAction,
    ISetMountingsOnPageAction,
    IAddMountingAction,
    IUpdateCurrentMountingAction,
    IDeleteMountingAction,
    SET_MOUNTINGS,
    SET_MOUNTINGS_TOTAL_COUNT,
    SET_CURRENT_MOUNTING,
    SET_MOUNTINGS_ON_PAGE,
    ADD_MOUNTING,
    UPDATE_CURRENT_MOUNTING,
    DELETE_MOUNTING
} from './action-types'
import {Dispatch} from 'redux'
import {runLongNetworkOperation} from '../request/actions'
import {api} from '../../api'
import {showStatusbar} from '../statusbar/actions'
import {API_MOUNTING} from '../../constants/api'
import {IAppState} from '../root-reducer'
import {Order} from '../../model/framer/order'
import {updateCurrentOrder} from '../orders/actions'
import {Client} from '../../model/framer/client'

const setMountings = (mountings: Mounting[]): ISetMountingsAction => ({
    type: SET_MOUNTINGS,
    payload: mountings
})

const setMountingsTotalCount = (count: number): ISetMountingsTotalCountAction => ({
    type: SET_MOUNTINGS_TOTAL_COUNT,
    payload: count
})

export const setCurrentMounting = (mounting: Mounting): ISetCurrentMountingAction => ({
    type: SET_CURRENT_MOUNTING,
    payload: mounting
})

const addMountingAction = (mounting: Mounting): IAddMountingAction => ({
    type: ADD_MOUNTING,
    payload: mounting
})

const updateCurrentMounting = (mounting: Mounting): IUpdateCurrentMountingAction => ({
    type: UPDATE_CURRENT_MOUNTING,
    payload: mounting
})

const deleteMountingAction = (id: number): IDeleteMountingAction => ({
    type: DELETE_MOUNTING,
    payload: id
})

export const setMountingsOnPage = (count: number): ISetMountingsOnPageAction => ({
    type: SET_MOUNTINGS_ON_PAGE,
    payload: count
})

export const loadMountings =
    (filters: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const response = await api.mountings.get(filters)
            const {mountings, totalCount} = response
            dispatch(setMountings(mountings))
            dispatch(setMountingsTotalCount(totalCount))
            dispatch(setCurrentMounting(mountings[0] ?? null))
        })(dispatch)
    }

export const loadMounting =
    (id: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const mounting = await api.mountings.getById(id)
            dispatch(setCurrentMounting(mounting))
        })(dispatch)
    }

export const updateMounting =
    (id: number, mounting: Mounting) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const response = await api.mountings.update(id, mounting)
            dispatch(updateCurrentMounting(response))
        })(dispatch)
    }

export const deleteMounting =
    (id: number) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const error = await api.mountings.delete(id)

            if (error) {
                dispatch(showStatusbar(error, true))
            } else {
                const filter = getState().router.location.search
                const response = await api.mountings.get(filter)
                const {mountings, totalCount} = response

                dispatch(deleteMountingAction(id))

                dispatch(setMountings(mountings))
                dispatch(setMountingsTotalCount(totalCount))
                dispatch(setCurrentMounting(mountings[0] ?? null))

                dispatch(showStatusbar('Заявка на монтаж удалена'))
            }
        })(dispatch)
    }

export const addMounting =
    (date: string, executorsIds: number[], order: Order) =>
    async (dispatch: Dispatch): Promise<void> => {
        return runLongNetworkOperation(async () => {
            const {id, client} = order
            const response = await api.mountings.add(date, executorsIds, id, client?.id)

            const orderClone = order.clone
            orderClone.mountingDate = response.datetime
            dispatch(updateCurrentOrder(orderClone))
            dispatch(addMountingAction(response))
        })(dispatch)
    }

export const downloadMountingsReport =
    (mounting: Mounting) =>
    (dispatch: Dispatch, getState: () => IAppState): void => {
        const link = document.createElement('a')
        link.setAttribute('href', `${API_MOUNTING(mounting.id)}/offer?t=${getState().account.token}`)
        link.click()
    }

export const updateClientAddress =
    (mounting: Mounting, client: Client) =>
    (dispatch: Dispatch): Promise<void> => {
        return runLongNetworkOperation(async () => {
            const updatedClient = await api.clients.update(client)

            const updatedMounting = mounting.clone
            updatedMounting.client = updatedClient

            dispatch(updateCurrentMounting(updatedMounting))
        })(dispatch)
    }
