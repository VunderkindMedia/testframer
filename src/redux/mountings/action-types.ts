import {IResetSessionAction} from '../global-action-types'
import {Mounting} from '../../model/framer/mounting'

export const SET_MOUNTINGS = 'SET_MOUNTINGS'
export const SET_MOUNTINGS_TOTAL_COUNT = 'SET_MOUNTINGS_TOTAL_COUNT'
export const SET_CURRENT_MOUNTING = 'SET_CURRENT_MOUNTING'
export const ADD_MOUNTING = 'ADD_MOUNTING'
export const UPDATE_CURRENT_MOUNTING = 'UPDATE_CURRENT_MOUNTING'
export const DELETE_MOUNTING = 'DELETE_MOUNTING'
export const SET_MOUNTINGS_ON_PAGE = 'SET_MOUNTINGS_ON_PAGE'

export interface ISetMountingsAction {
    type: typeof SET_MOUNTINGS
    payload: Mounting[]
}

export interface ISetCurrentMountingAction {
    type: typeof SET_CURRENT_MOUNTING
    payload: Mounting
}

export interface IUpdateCurrentMountingAction {
    type: typeof UPDATE_CURRENT_MOUNTING
    payload: Mounting
}

export interface IDeleteMountingAction {
    type: typeof DELETE_MOUNTING
    payload: number
}

export interface ISetMountingsOnPageAction {
    type: typeof SET_MOUNTINGS_ON_PAGE
    payload: number
}

export interface IAddMountingAction {
    type: typeof ADD_MOUNTING
    payload: Mounting
}

export interface ISetMountingsTotalCountAction {
    type: typeof SET_MOUNTINGS_TOTAL_COUNT
    payload: number
}

export type TMountingsActionTypes =
    | IResetSessionAction
    | ISetMountingsAction
    | ISetCurrentMountingAction
    | IUpdateCurrentMountingAction
    | IDeleteMountingAction
    | ISetMountingsOnPageAction
    | IAddMountingAction
    | ISetMountingsTotalCountAction
