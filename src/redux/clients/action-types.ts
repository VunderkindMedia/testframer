import {Client} from '../../model/framer/client'
import {Order} from '../../model/framer/order'
import {IResetSessionAction} from '../global-action-types'

export const SET_CLIENTS = 'SET_CLIENTS'
export const SET_CURRENT_CLIENT = 'SET_CURRENT_CLIENT'
export const SET_CLIENTS_ON_PAGE = 'SET_CLIENTS_ON_PAGE'
export const SET_CLIENTS_TOTAL_COUNT = 'SET_CLIENTS_TOTAL_COUNT'
export const UPDATE_CLIENT = 'UPDATE_CLIENT'
export const ADD_CLIENT = 'ADD_CLIENT'
export const SET_CLIENT_SEARCH_QUERY = 'SET_CLIENT_SEARCH_QUERY'
export const SET_CLIENT_ORDERS = 'SET_CLIENT_ORDERS'
export const DELETE_CLIENT = 'DELETE_CLIENT'

export interface ISetClientsAction {
    type: typeof SET_CLIENTS
    payload: Client[]
}

export interface ISetCurrentClientAction {
    type: typeof SET_CURRENT_CLIENT
    payload: Client | null
}

export interface ISetClientsOnPageAction {
    type: typeof SET_CLIENTS_ON_PAGE
    payload: number
}

export interface ISetClientsTotalCountAction {
    type: typeof SET_CLIENTS_TOTAL_COUNT
    payload: number
}

export interface IUpdateClientAction {
    type: typeof UPDATE_CLIENT
    payload: Client
}

export interface IAddClientAction {
    type: typeof ADD_CLIENT
    payload: Client
}

export interface ISetClientSearchQueryAction {
    type: typeof SET_CLIENT_SEARCH_QUERY
    payload: string
}

export interface ISetClientOrdersAction {
    type: typeof SET_CLIENT_ORDERS
    payload: Order[]
}

export interface IDeleteClientAction {
    type: typeof DELETE_CLIENT
    payload: number
}

export type TClientsAction =
    | IResetSessionAction
    | ISetClientsAction
    | ISetCurrentClientAction
    | ISetClientsOnPageAction
    | ISetClientsTotalCountAction
    | IUpdateClientAction
    | IAddClientAction
    | ISetClientSearchQueryAction
    | ISetClientOrdersAction
    | IDeleteClientAction
