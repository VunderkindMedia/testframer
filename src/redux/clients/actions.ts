import {Client} from '../../model/framer/client'
import {Order} from '../../model/framer/order'
import {
    SET_CLIENTS,
    SET_CLIENTS_ON_PAGE,
    SET_CLIENTS_TOTAL_COUNT,
    SET_CURRENT_CLIENT,
    UPDATE_CLIENT,
    SET_CLIENT_SEARCH_QUERY,
    ADD_CLIENT,
    SET_CLIENT_ORDERS,
    DELETE_CLIENT,
    ISetClientsAction,
    ISetClientsOnPageAction,
    ISetClientsTotalCountAction,
    ISetCurrentClientAction,
    IUpdateClientAction,
    IAddClientAction,
    ISetClientSearchQueryAction,
    ISetClientOrdersAction,
    IDeleteClientAction
} from './action-types'
import {runLongNetworkOperation} from '../request/actions'
import {api} from '../../api'
import {Dispatch} from 'redux'
import {IAppState} from '../root-reducer'
import {showStatusbar} from '../statusbar/actions'
import {NAV_CLIENTS} from '../../constants/navigation'
import {push} from 'connected-react-router'

export const setClients = (clients: Client[]): ISetClientsAction => ({
    type: SET_CLIENTS,
    payload: clients
})

export const setClientsOnPage = (count: number): ISetClientsOnPageAction => ({
    type: SET_CLIENTS_ON_PAGE,
    payload: count
})

export const setClientTotalCount = (count: number): ISetClientsTotalCountAction => ({
    type: SET_CLIENTS_TOTAL_COUNT,
    payload: count
})

export const setCurrentClient = (client: Client | null): ISetCurrentClientAction => ({
    type: SET_CURRENT_CLIENT,
    payload: client
})

const updateClient = (client: Client): IUpdateClientAction => ({
    type: UPDATE_CLIENT,
    payload: client
})

const addClient = (client: Client): IAddClientAction => ({
    type: ADD_CLIENT,
    payload: client
})

export const setClientSearchQuery = (query: string): ISetClientSearchQueryAction => ({
    type: SET_CLIENT_SEARCH_QUERY,
    payload: query
})

const setCurrentClientOrders = (orders: Order[]): ISetClientOrdersAction => ({
    type: SET_CLIENT_ORDERS,
    payload: orders
})

const deleteClientAction = (id: number): IDeleteClientAction => ({
    type: DELETE_CLIENT,
    payload: id
})

export const loadClients =
    (clientsFilter = '') =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const response = await api.clients.get(clientsFilter)

            const currentClient = getState().clients.currentClient
            if (response.clients.length > 0) {
                dispatch(setCurrentClient(response.clients.find(c => c.id === currentClient?.id) ?? response.clients[0]))
            }

            dispatch(setClientTotalCount(response.totalCount))
            dispatch(setClients(response.clients))
        })(dispatch)
    }

export const createClient =
    (client: Client) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const newClient = await api.clients.create(client)
            dispatch(addClient(newClient))
        })(dispatch)
    }

export const updateClientAsync =
    (client: Client) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const updatedClient = await api.clients.update(client)
            dispatch(updateClient(updatedClient))
        })(dispatch)
    }

export const loadClient =
    (id: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const client = await api.clients.getById(id)
            dispatch(setCurrentClient(client))
        })(dispatch)
    }

export const loadClientOrders =
    (id: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const response = await api.clients.getClientOrders(id)
            dispatch(setCurrentClientOrders(response))
        })(dispatch)
    }

export const deleteClient =
    (id: number) =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(
            async () => {
                const isSuccess = await api.clients.deleteClient(String(id))

                if (isSuccess) {
                    dispatch(showStatusbar('Клиент удален'))
                    dispatch(deleteClientAction(id))
                    dispatch(push(NAV_CLIENTS))
                } else {
                    dispatch(showStatusbar('Не удалось удалить клиента', true))
                }
            },
            () => {
                dispatch(showStatusbar('Не удалось удалить клиента', true))
            }
        )(dispatch)
    }
