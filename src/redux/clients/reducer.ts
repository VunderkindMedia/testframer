import {Reducer} from 'redux'
import {Client} from '../../model/framer/client'
import {Order} from '../../model/framer/order'
import {
    SET_CLIENTS,
    SET_CLIENTS_ON_PAGE,
    SET_CLIENTS_TOTAL_COUNT,
    SET_CURRENT_CLIENT,
    UPDATE_CLIENT,
    ADD_CLIENT,
    SET_CLIENT_SEARCH_QUERY,
    SET_CLIENT_ORDERS,
    DELETE_CLIENT,
    TClientsAction
} from './action-types'
import {RESET_SESSION} from '../global-action-types'

export interface IClientsState {
    all: Client[]
    currentClient: Client | null
    currentClientOrders: Order[] | null
    clientsOnPage: number
    totalCount: number
    searchQuery: string
    filtered: Client[]
}

const initialState: IClientsState = {
    all: [],
    currentClient: null,
    currentClientOrders: null,
    clientsOnPage: -1,
    totalCount: -1,
    searchQuery: '',
    filtered: []
}

export const clients: Reducer<IClientsState> = (state = initialState, action: TClientsAction) => {
    switch (action.type) {
        case SET_CLIENTS:
            return {...state, all: action.payload}
        case SET_CURRENT_CLIENT:
            return {...state, currentClient: action.payload}
        case SET_CLIENTS_ON_PAGE:
            return {...state, clientsOnPage: action.payload}
        case SET_CLIENTS_TOTAL_COUNT:
            return {...state, totalCount: action.payload}
        case SET_CLIENT_ORDERS:
            return {...state, currentClientOrders: action.payload}
        case UPDATE_CLIENT: {
            const {all} = state
            const index = all.findIndex(client => client.id === action.payload.id)
            const allClients = [...all.slice(0, index), action.payload, ...all.slice(index + 1)]
            if (index !== -1) {
                return {
                    ...state,
                    all: allClients,
                    filtered: !state.searchQuery
                        ? []
                        : [...allClients.filter(c => c.name.toLowerCase().startsWith(state.searchQuery.toLowerCase()))]
                }
            }
            return state
        }
        case ADD_CLIENT: {
            return {
                ...state,
                all: [action.payload, ...state.all],
                totalCount: state.totalCount + 1,
                currentClient: action.payload,
                searchQuery: '',
                filtered: []
            }
        }
        case SET_CLIENT_SEARCH_QUERY: {
            const clients = !action.payload ? [] : [...state.all.filter(c => c.name.toLowerCase().startsWith(action.payload.toLowerCase()))]

            return {
                ...state,
                searchQuery: action.payload,
                filtered: clients,
                currentClient: action.payload ? clients[0] : state.all[0]
            }
        }
        case DELETE_CLIENT: {
            const clients = [...state.all]
            const deletedIndex = clients.findIndex(c => c.id === action.payload)

            if (deletedIndex !== -1) {
                clients.splice(deletedIndex, 1)
            }

            const filtered = !state.searchQuery
                ? []
                : [...clients.filter(c => c.name.toLowerCase().startsWith(state.searchQuery.toLowerCase()))]
            return {
                ...state,
                all: clients,
                totalCount: state.totalCount - 1,
                currentClient: !state.searchQuery ? clients[0] : filtered[0],
                currentClientOrders: null,
                filtered
            }
        }
        case RESET_SESSION:
            return {...initialState, clientsOnPage: state.clientsOnPage}
        default:
            return state
    }
}
