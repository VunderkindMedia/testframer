import {Reducer} from 'redux'
import {RequestStates} from '../../model/request-states'
import {SET_REQUEST_STATE, SHOW_SPECIAL_LOADER, CLEAR_REQUEST_STATE, TRequestActionTypes} from './action-types'

export interface IRequestReducerState {
    showSpecial: boolean
    count: number
    keys: {[key: string]: RequestStates}
}

const initialState: IRequestReducerState = {
    count: 0,
    showSpecial: false,
    keys: {}
}

export const request: Reducer<IRequestReducerState> = (state = initialState, action: TRequestActionTypes) => {
    switch (action.type) {
        case SET_REQUEST_STATE: {
            let {count} = state
            const keys = {...state.keys}
            const {requestState, key} = action.payload

            if (requestState === RequestStates.Processing) {
                count++
            } else {
                count--
            }
            if (key) {
                keys[key] = requestState
            }
            return {...state, count, keys}
        }
        case SHOW_SPECIAL_LOADER:
            return {...state, showSpecial: action.payload}
        case CLEAR_REQUEST_STATE: {
            const keys = {...state.keys}
            if (action.payload) {
                delete keys[action.payload]
                return {...state, keys}
            }
            return {...state, keys: {}}
        }
        default:
            return state
    }
}
