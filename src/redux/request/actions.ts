import {RequestStates} from '../../model/request-states'
import {
    SET_REQUEST_STATE,
    SHOW_SPECIAL_LOADER,
    CLEAR_REQUEST_STATE,
    ISetRequestStateAction,
    IClearRequestStateAction,
    IShowSpecialLoaderAction
} from './action-types'
import {Dispatch} from 'redux'

const setRequestState = (requestState: RequestStates, key?: string): ISetRequestStateAction => ({
    type: SET_REQUEST_STATE,
    payload: {requestState, key}
})

export const clearRequestState = (key?: string): IClearRequestStateAction => ({
    type: CLEAR_REQUEST_STATE,
    payload: key
})

export const showSpecialLoader = (showSpecial: boolean): IShowSpecialLoaderAction => ({
    type: SHOW_SPECIAL_LOADER,
    payload: showSpecial
})

export const runLongNetworkOperation =
    (operation: () => Promise<void>, onError?: (e: Error) => void) =>
    async (dispatch: Dispatch, requestKey?: string): Promise<void> => {
        dispatch(setRequestState(RequestStates.Processing, requestKey))
        try {
            await operation()
            dispatch(setRequestState(RequestStates.Succeeded, requestKey))
        } catch (e) {
            onError && onError(e)
            dispatch(setRequestState(RequestStates.Failed, requestKey))
        }
    }
