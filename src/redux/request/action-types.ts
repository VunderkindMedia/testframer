import {RequestStates} from '../../model/request-states'

export const SET_REQUEST_STATE = 'SET_REQUEST_STATE'
export const CLEAR_REQUEST_STATE = 'CLEAR_REQUEST_STATE'
export const SHOW_SPECIAL_LOADER = 'SHOW_SPECIAL_LOADER'

export interface ISetRequestStateAction {
    type: typeof SET_REQUEST_STATE
    payload: {
        requestState: RequestStates
        key?: string
    }
}

export interface IClearRequestStateAction {
    type: typeof CLEAR_REQUEST_STATE
    payload?: string
}

export interface IShowSpecialLoaderAction {
    type: typeof SHOW_SPECIAL_LOADER
    payload: boolean
}

export type TRequestActionTypes = ISetRequestStateAction | IClearRequestStateAction | IShowSpecialLoaderAction
