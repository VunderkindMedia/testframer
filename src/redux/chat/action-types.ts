import {IResetSessionAction} from '../global-action-types'

export const SHOW_CHAT_BUTTON = 'SHOW_CHAT'
export const SET_CHAT_IS_OPENED = 'SET_CHAT_IS_OPENED'

export interface IShowChatButtonAction {
    type: typeof SHOW_CHAT_BUTTON
}

export interface ISetChatIsOpenedAction {
    type: typeof SET_CHAT_IS_OPENED
    payload: boolean
}

export type TChatActionTypes = IResetSessionAction | IShowChatButtonAction | ISetChatIsOpenedAction
