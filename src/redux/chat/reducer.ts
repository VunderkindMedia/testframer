import {Reducer} from 'redux'
import {SET_CHAT_IS_OPENED, SHOW_CHAT_BUTTON, TChatActionTypes} from './action-types'

export interface IChatState {
    isVisibleButton: boolean
    isOpened: boolean
}

const initialState: IChatState = {
    isVisibleButton: false,
    isOpened: false
}

export const chat: Reducer<IChatState> = (state = initialState, action: TChatActionTypes) => {
    switch (action.type) {
        case SHOW_CHAT_BUTTON:
            return {...state, isVisibleButton: true}
        case SET_CHAT_IS_OPENED:
            return {...state, isOpened: action.payload}
        default:
            return state
    }
}
