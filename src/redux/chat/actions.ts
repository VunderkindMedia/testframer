import {ISetChatIsOpenedAction, IShowChatButtonAction, SET_CHAT_IS_OPENED, SHOW_CHAT_BUTTON} from './action-types'

export const showChatButton = (): IShowChatButtonAction => ({
    type: SHOW_CHAT_BUTTON
})

export const setChatIsOpened = (isOpened: boolean): ISetChatIsOpenedAction => ({
    type: SET_CHAT_IS_OPENED,
    payload: isOpened
})
