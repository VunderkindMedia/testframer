import {Template} from '../../model/template'
import {
    SET_CURRENT_TEMPLATE,
    SET_TEMPLATE_FILTER,
    SET_CONNECTING_SIDE,
    SET_HOST_PRODUCT,
    RESET_TEMPLATE_SELECTOR,
    ISetConnectingSideAction,
    ISetCurrentTemplateAction,
    ISetHostProductAction,
    ISetTemplateFilterAction,
    IResetTemplateSelectorAction
} from './action-types'
import {Construction} from '../../model/framer/construction'
import {Sides} from '../../model/sides'
import {Product} from '../../model/framer/product'

export const setCurrentTemplate = (template: Template | null): ISetCurrentTemplateAction => ({
    type: SET_CURRENT_TEMPLATE,
    payload: template
})

export const setTemplateFilter = (filter: (construction: Construction) => boolean): ISetTemplateFilterAction => ({
    type: SET_TEMPLATE_FILTER,
    payload: filter
})

export const setConnectingSide = (side: Sides | null): ISetConnectingSideAction => ({
    type: SET_CONNECTING_SIDE,
    payload: side
})

export const setHostProduct = (product: Product | null): ISetHostProductAction => ({
    type: SET_HOST_PRODUCT,
    payload: product
})

export const resetTemplateSelector = (): IResetTemplateSelectorAction => ({
    type: RESET_TEMPLATE_SELECTOR
})
