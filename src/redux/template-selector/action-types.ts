import {Template} from '../../model/template'
import {Construction} from '../../model/framer/construction'
import {Sides} from '../../model/sides'
import {Product} from '../../model/framer/product'
import {IResetSessionAction} from '../global-action-types'

export const SET_CURRENT_TEMPLATE = 'SET_CURRENT_TEMPLATE'
export const SET_TEMPLATE_FILTER = 'SET_TEMPLATE_FILTER'
export const SET_CONNECTING_SIDE = 'SET_CONNECTING_SIDE'
export const SET_HOST_PRODUCT = 'SET_HOST_PRODUCT'
export const RESET_TEMPLATE_SELECTOR = 'RESET_TEMPLATE_SELECTOR'

export interface ISetCurrentTemplateAction {
    type: typeof SET_CURRENT_TEMPLATE
    payload: Template | null
}

export interface ISetTemplateFilterAction {
    type: typeof SET_TEMPLATE_FILTER
    payload: (construction: Construction) => boolean
}

export interface ISetConnectingSideAction {
    type: typeof SET_CONNECTING_SIDE
    payload: Sides | null
}

export interface ISetHostProductAction {
    type: typeof SET_HOST_PRODUCT
    payload: Product | null
}

export interface IResetTemplateSelectorAction {
    type: typeof RESET_TEMPLATE_SELECTOR
}

export type TTemplateSelectorActionTypes =
    | IResetSessionAction
    | ISetCurrentTemplateAction
    | ISetTemplateFilterAction
    | ISetConnectingSideAction
    | ISetHostProductAction
    | IResetTemplateSelectorAction
