import {Reducer} from 'redux'
import {
    SET_CONNECTING_SIDE,
    SET_CURRENT_TEMPLATE,
    SET_HOST_PRODUCT,
    SET_TEMPLATE_FILTER,
    RESET_TEMPLATE_SELECTOR,
    TTemplateSelectorActionTypes
} from './action-types'
import {Template} from '../../model/template'
import {Construction} from '../../model/framer/construction'
import {Sides} from '../../model/sides'
import {Product} from '../../model/framer/product'
import {RESET_SESSION} from '../global-action-types'

export interface ITemplateSelectorState {
    currentTemplate: Template | null
    templateFilter: (construction: Construction) => boolean
    connectingSide: Sides | null
    hostProduct: Product | null
}

const initialState: ITemplateSelectorState = {
    currentTemplate: null,
    templateFilter: () => true,
    connectingSide: null,
    hostProduct: null
}

export const templateSelector: Reducer<ITemplateSelectorState> = (state = initialState, action: TTemplateSelectorActionTypes) => {
    switch (action.type) {
        case RESET_TEMPLATE_SELECTOR:
            return {...state, templateFilter: () => true, connectingSide: null, hostProduct: null}
        case SET_CURRENT_TEMPLATE:
            return {...state, currentTemplate: action.payload}
        case SET_TEMPLATE_FILTER:
            return {...state, templateFilter: action.payload}
        case SET_CONNECTING_SIDE:
            return {...state, connectingSide: action.payload}
        case SET_HOST_PRODUCT:
            return {...state, hostProduct: action.payload}
        case RESET_SESSION:
            return initialState
        default:
            return state
    }
}
