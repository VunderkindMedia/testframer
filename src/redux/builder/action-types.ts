import {IContext} from '../../model/i-context'
import {DimensionLine} from '../../model/dimension-line'
import {ConfigContexts} from '../../model/config-contexts'
import {IResetSessionAction} from '../global-action-types'
import {BuilderModes} from '../../model/builder-modes'
import {UserParameterConfig} from '../../model/framer/user-parameter/user-parameter-config'

export const SET_CURRENT_CONTEXT = 'SET_CURRENT_CONTEXT'
export const SET_HIGHLIGHTED_CONTEXT = 'SET_HIGHLIGHTED_CONTEXT'
export const SET_CURRENT_DIMENSION_LINE = 'SET_CURRENT_DIMENSION_LINE'
export const SET_HIGHLIGHTED_DIMENSION_LINE = 'SET_HIGHLIGHTED_DIMENSION_LINE'
export const SET_CONFIG_CONTEXT = 'SET_CONFIG_CONTEXT'
export const SET_BUILDER_MODE = 'SET_BUILDER_MODE'
export const SET_USER_PARAMETER_CONSTRUCTION_CONFIGS = 'SET_USER_PARAMETER_CONSTRUCTION_CONFIGS'
export const SET_USER_COLOR_PARAMETERS = 'SET_USER_COLOR_PARAMETERS'
export const SET_ONBOARDING_STEP = 'SET_ONBOARDING_STEP'

export interface ISetCurrentContextAction {
    type: typeof SET_CURRENT_CONTEXT
    payload: IContext | null
}

export interface ISetHighlightedContextAction {
    type: typeof SET_HIGHLIGHTED_CONTEXT
    payload: IContext | null
}

export interface ISetCurrentDimensionLineAction {
    type: typeof SET_CURRENT_DIMENSION_LINE
    payload: DimensionLine | null
}

export interface ISetHighlightedDimensionLineAction {
    type: typeof SET_HIGHLIGHTED_DIMENSION_LINE
    payload: DimensionLine | null
}

export interface ISetConfigContextAction {
    type: typeof SET_CONFIG_CONTEXT
    payload: ConfigContexts | null
}

export interface ISetBuilderMode {
    type: typeof SET_BUILDER_MODE
    payload: BuilderModes | null
}

export interface ISetUserParameterConstructionConfigs {
    type: typeof SET_USER_PARAMETER_CONSTRUCTION_CONFIGS
    payload: {[hash: string]: UserParameterConfig[]}
}

export interface ISetUserColorParameters {
    type: typeof SET_USER_COLOR_PARAMETERS
    payload: UserParameterConfig | null
}

export interface ISetOnboardingStep {
    type: typeof SET_ONBOARDING_STEP
    payload: number | null
}

export type TBuilderActionTypes =
    | IResetSessionAction
    | ISetCurrentContextAction
    | ISetHighlightedContextAction
    | ISetCurrentDimensionLineAction
    | ISetHighlightedDimensionLineAction
    | ISetConfigContextAction
    | ISetBuilderMode
    | ISetUserParameterConstructionConfigs
    | ISetUserColorParameters
    | ISetOnboardingStep
