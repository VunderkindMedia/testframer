import {
    ISetBuilderMode,
    ISetConfigContextAction,
    ISetCurrentContextAction,
    ISetCurrentDimensionLineAction,
    ISetHighlightedContextAction,
    ISetHighlightedDimensionLineAction,
    ISetUserParameterConstructionConfigs,
    ISetUserColorParameters,
    ISetOnboardingStep,
    SET_BUILDER_MODE,
    SET_CONFIG_CONTEXT,
    SET_CURRENT_CONTEXT,
    SET_CURRENT_DIMENSION_LINE,
    SET_HIGHLIGHTED_CONTEXT,
    SET_HIGHLIGHTED_DIMENSION_LINE,
    SET_USER_PARAMETER_CONSTRUCTION_CONFIGS,
    SET_USER_COLOR_PARAMETERS,
    SET_ONBOARDING_STEP
} from './action-types'
import {IContext} from '../../model/i-context'
import {ConfigContexts} from '../../model/config-contexts'
import {DimensionLine} from '../../model/dimension-line'
import {Order} from '../../model/framer/order'
import {Product} from '../../model/framer/product'
import {Dispatch} from 'redux'
import {runLongNetworkOperation} from '../request/actions'
import {Profile} from '../../model/framer/profile'
import {Furniture} from '../../model/framer/furniture'
import {api} from '../../api'
import {ConstructionTypes} from '../../model/framer/construction-types'
import {ProductTypes} from '../../model/framer/product-types'
import {setCurrentConstruction, setGenericProduct, updateCurrentOrder} from '../orders/actions'
import {IAppState} from '../root-reducer'
import {BuilderModes} from '../../model/builder-modes'
import {UserParameterConfig} from '../../model/framer/user-parameter/user-parameter-config'

export const setCurrentContext = (context: IContext | null): ISetCurrentContextAction => ({
    type: SET_CURRENT_CONTEXT,
    payload: context
})

export const setHighlightedContext = (context: IContext | null): ISetHighlightedContextAction => ({
    type: SET_HIGHLIGHTED_CONTEXT,
    payload: context
})

export const setCurrentDimensionLine = (dimensionLine: DimensionLine | null): ISetCurrentDimensionLineAction => ({
    type: SET_CURRENT_DIMENSION_LINE,
    payload: dimensionLine
})

export const setHighlightedDimensionLine = (dimensionLine: DimensionLine | null): ISetHighlightedDimensionLineAction => ({
    type: SET_HIGHLIGHTED_DIMENSION_LINE,
    payload: dimensionLine
})

export const setConfigContext = (context: ConfigContexts | null): ISetConfigContextAction => ({
    type: SET_CONFIG_CONTEXT,
    payload: context
})

export const setBuilderMode = (mode: BuilderModes | null): ISetBuilderMode => ({
    type: SET_BUILDER_MODE,
    payload: mode
})

export const setUserParameterConstructionConfigs = (configs: {
    [hash: string]: UserParameterConfig[]
}): ISetUserParameterConstructionConfigs => ({
    type: SET_USER_PARAMETER_CONSTRUCTION_CONFIGS,
    payload: configs
})

export const setUserColorParameters = (params: UserParameterConfig | null): ISetUserColorParameters => ({
    type: SET_USER_COLOR_PARAMETERS,
    payload: params
})

export const setOnboardingStep = (step: number | null): ISetOnboardingStep => ({
    type: SET_ONBOARDING_STEP,
    payload: step
})

export const loadConstructionParams =
    (order: Order) =>
    (dispatch: Dispatch): void => {
        const constructionProducts: Product[] = []
        order.constructions.map(c => constructionProducts.push(...c.products))
        const productAvailableProfiles: Profile[][] = []
        const productAvailableFurniture: Furniture[][] = []
        const productProfiles: number[] = []
        const productFurniture: number[] = []
        const productFilling: string[] = []

        void runLongNetworkOperation(async () => {
            dispatch(setCurrentConstruction(order.constructions[0]))
            for (const product of constructionProducts) {
                const {profiles, furniture} = await api.design.getProfilesAndFurniture(
                    product.constructionTypeId ?? ConstructionTypes.Okno,
                    product.productionTypeId ?? ProductTypes.Okno
                )

                productFurniture.push(product.furniture.id)
                productProfiles.push(product.profile.id)
                productAvailableProfiles.push(profiles)
                productAvailableFurniture.push(furniture)

                productFilling.push(...product.frame.allSolidFillings.map(f => f.fillingVendorCode))
            }

            const baseProduct = order.constructions[0].products[0].clone
            const product = new Product()

            if (productFurniture.every(f => f === productFurniture[0])) {
                product.furniture = baseProduct.furniture
            }

            if (productProfiles.every(f => f === productProfiles[0])) {
                product.profile = baseProduct.profile

                if (new Set(productFilling).size === 1) {
                    product.availableFillings = await api.design.getFillings(baseProduct.profile.id)
                    product.frame = baseProduct.frame
                }
            }

            product.availableProfiles = productAvailableProfiles.reduce((commonProfiles, currentProductProfiles) => {
                return commonProfiles.filter(p => {
                    return currentProductProfiles.findIndex(p2 => p2.id === p.id) !== -1
                })
            })
            product.availableFurniture = productAvailableFurniture.reduce((commonFurniture, currentProductFurniture) => {
                return commonFurniture.filter(f => {
                    return currentProductFurniture.findIndex(f2 => f2.id === f.id) !== -1
                })
            })

            dispatch(setGenericProduct(product))
        })(dispatch)
    }

export const setProfileToAllConstructions =
    (profile: Profile) =>
    (dispatch: Dispatch, getState: () => IAppState): void => {
        const state = getState()
        const {currentOrder, genericProduct} = state.orders

        if (!currentOrder || !genericProduct) {
            return
        }

        const order = currentOrder.clone

        try {
            for (const construction of order.constructions) {
                for (const product of construction.products) {
                    product.setProfile(profile)
                }
            }

            order.changed = true
            const product = new Product()
            product.availableProfiles = genericProduct.availableProfiles
            product.profile = profile
            product.availableFurniture = genericProduct.availableFurniture
            product.furniture = genericProduct.furniture
            product.availableFillings = genericProduct.availableFillings
            product.frame = genericProduct.frame
            order.addAction(`Задали профиль ${profile.displayName ? profile.displayName : profile.name} для всех заполнений`)

            dispatch(updateCurrentOrder(order))
            dispatch(setGenericProduct(product))
        } catch (e) {
            console.error(e)
        }
    }

export const setFurnitureToAllConstructions =
    (furniture: Furniture) =>
    (dispatch: Dispatch, getState: () => IAppState): void => {
        const state = getState()
        const {currentOrder, genericProduct} = state.orders

        if (!currentOrder || !genericProduct) {
            return
        }

        const order = currentOrder.clone

        try {
            for (const construction of order.constructions) {
                for (const product of construction.products) {
                    product.setFurniture(furniture)
                }
            }

            order.changed = true
            const product = new Product()
            product.availableProfiles = genericProduct.availableProfiles
            product.profile = genericProduct.profile
            product.availableFurniture = genericProduct.availableFurniture
            product.furniture = furniture
            product.availableFillings = genericProduct.availableFillings
            product.frame = genericProduct.frame
            order.addAction(
                // eslint-disable-next-line max-len
                `Задали фурнитуру ${furniture.displayName} для всех заполнений`
            )

            dispatch(updateCurrentOrder(order))
            dispatch(setGenericProduct(product))
        } catch (e) {
            console.error(e)
        }
    }
