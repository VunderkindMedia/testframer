import {Reducer} from 'redux'
import {
    SET_BUILDER_MODE,
    SET_CONFIG_CONTEXT,
    SET_CURRENT_CONTEXT,
    SET_CURRENT_DIMENSION_LINE,
    SET_HIGHLIGHTED_CONTEXT,
    SET_HIGHLIGHTED_DIMENSION_LINE,
    SET_USER_PARAMETER_CONSTRUCTION_CONFIGS,
    SET_USER_COLOR_PARAMETERS,
    SET_ONBOARDING_STEP,
    TBuilderActionTypes
} from './action-types'
import {IContext} from '../../model/i-context'
import {ConfigContexts} from '../../model/config-contexts'
import {DimensionLine} from '../../model/dimension-line'
import {RESET_SESSION} from '../global-action-types'
import {BuilderModes} from '../../model/builder-modes'
import {UserParameterConfig} from '../../model/framer/user-parameter/user-parameter-config'
import {BUILDER_ONBOARDING, BUILDER_ONBOARDING_SHOWED, TUTORIAL_SHOWED} from '../../constants/local-storage'

export interface IBuilderState {
    currentContext: IContext | null
    highlightedContext: IContext | null
    currentDimensionLine: DimensionLine | null
    highlightedDimensionLine: DimensionLine | null
    configContext: ConfigContexts | null
    mode: BuilderModes | null
    userParameterConfigsMap: {[hash: string]: UserParameterConfig[]}
    userColorParameters: UserParameterConfig | null
    onboarding?: number
}

const getOnboardingStep = (): number | undefined => {
    const tutorialShowed = localStorage.getItem(TUTORIAL_SHOWED)
    const builderOnboardingShowed = localStorage.getItem(BUILDER_ONBOARDING_SHOWED)
    const builderOnboarding = localStorage.getItem(BUILDER_ONBOARDING)
    return !builderOnboardingShowed && tutorialShowed ? (builderOnboarding ? +builderOnboarding : 0) : undefined
}

const initialState: IBuilderState = {
    currentContext: null,
    highlightedContext: null,
    currentDimensionLine: null,
    highlightedDimensionLine: null,
    configContext: null,
    mode: null,
    userParameterConfigsMap: {},
    userColorParameters: null,
    onboarding: getOnboardingStep()
}

export const builder: Reducer<IBuilderState> = (state = initialState, action: TBuilderActionTypes) => {
    switch (action.type) {
        case SET_CURRENT_CONTEXT:
            return {...state, currentContext: action.payload}
        case SET_HIGHLIGHTED_CONTEXT:
            return {...state, highlightedContext: action.payload}
        case SET_CURRENT_DIMENSION_LINE:
            return {...state, currentDimensionLine: action.payload}
        case SET_HIGHLIGHTED_DIMENSION_LINE:
            return {...state, highlightedDimensionLine: action.payload}
        case SET_CONFIG_CONTEXT:
            return {...state, configContext: action.payload}
        case SET_BUILDER_MODE:
            return {...state, mode: action.payload}
        case SET_USER_PARAMETER_CONSTRUCTION_CONFIGS: {
            return {
                ...state,
                userParameterConfigsMap: action.payload ?? {}
            }
        }
        case SET_USER_COLOR_PARAMETERS: {
            return {
                ...state,
                userColorParameters: action.payload
            }
        }
        case SET_ONBOARDING_STEP: {
            if (action.payload !== null) {
                localStorage.removeItem(BUILDER_ONBOARDING_SHOWED)
                localStorage.setItem(BUILDER_ONBOARDING, action.payload.toString())
                return {...state, onboarding: action.payload}
            }
            localStorage.removeItem(BUILDER_ONBOARDING)
            localStorage.setItem(BUILDER_ONBOARDING_SHOWED, '1')
            return {...state, onboarding: undefined}
        }
        case RESET_SESSION:
            return {...initialState, onboarding: getOnboardingStep()}
        default:
            return state
    }
}
