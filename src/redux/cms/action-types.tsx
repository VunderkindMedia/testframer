import {News} from '../../model/framer/news'
import {IResetSessionAction} from '../global-action-types'
import {IToken} from './reducer'

export const SET_NEWS = 'SET_NEWS'
export const SET_TOKEN = 'SET_TOKEN'

export interface ISetNewsAction {
    type: typeof SET_NEWS
    payload: News[]
}

export interface ISetToken {
    type: typeof SET_TOKEN
    payload: IToken
}

export type TCmsActionTypes = IResetSessionAction | ISetNewsAction | ISetToken
