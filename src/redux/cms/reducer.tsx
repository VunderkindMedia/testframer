import {Reducer} from 'redux'
import {News} from '../../model/framer/news'
import {TCmsActionTypes, SET_NEWS, SET_TOKEN} from './action-types'
import {CMS_TOKEN} from '../../constants/local-storage'
import {parseJWT} from '../../helpers/account'
import {IJWT} from '../../model/token'
import {CMSManager} from '../../cms-manager'

export interface IToken {
    jwt: string
    expired: number
}

export interface ICmsState {
    news: News[]
    token: IToken | null
}

const token = localStorage.getItem(CMS_TOKEN)
const parsedToken = token ? parseJWT<IJWT>(token) : null
CMSManager.setToken(token, parsedToken ? parsedToken.exp : null)

const initialState: ICmsState = {
    news: [],
    token: token && parsedToken ? {jwt: token, expired: parsedToken.exp} : null
}

export const cms: Reducer<ICmsState> = (state = initialState, action: TCmsActionTypes) => {
    switch (action.type) {
        case SET_TOKEN: {
            localStorage.setItem(CMS_TOKEN, action.payload.jwt)
            return {...state, token: action.payload}
        }
        case SET_NEWS: {
            return {...state, news: action.payload}
        }
        default:
            return state
    }
}
