import {SET_TOKEN, SET_NEWS, ISetNewsAction, ISetToken} from './action-types'
import {News} from '../../model/framer/news'
import {Dispatch} from 'redux'
import {runLongNetworkOperation} from '../request/actions'
import {api} from '../../api'

const setNews = (news: News[]): ISetNewsAction => {
    return {
        type: SET_NEWS,
        payload: news
    }
}

export const setToken = (jwt: string, expired: number): ISetToken => {
    return {
        type: SET_TOKEN,
        payload: {jwt, expired}
    }
}

export const loadNews =
    () =>
    async (dispatch: Dispatch): Promise<void> => {
        await runLongNetworkOperation(async () => {
            const news = await api.cms.getNews()
            dispatch(setNews(news))
        })(dispatch)
    }
