import {Reducer} from 'redux'
import {HIDE_STATUSBAR, SHOW_STATUSBAR, TStatusbarActionTypes} from './action-types'

export interface IStatusbarState {
    isVisible: boolean
    isError: boolean
    message: string
}

const initialState: IStatusbarState = {
    isVisible: false,
    isError: true,
    message: 'Ваш заказ оплачивается'
}

export const statusbar: Reducer<IStatusbarState> = (state = initialState, action: TStatusbarActionTypes) => {
    switch (action.type) {
        case SHOW_STATUSBAR:
            return {
                ...state,
                isVisible: true,
                isError: action.payload.isError,
                message: action.payload.message
            }
        case HIDE_STATUSBAR:
            return {
                ...state,
                isVisible: false
            }
        default:
            return state
    }
}
