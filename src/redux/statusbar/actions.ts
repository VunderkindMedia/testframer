import {HIDE_STATUSBAR, IHideStatusbarAction, IShowStatusbarAction, SHOW_STATUSBAR} from './action-types'

export const showStatusbar = (message: string, isError = false): IShowStatusbarAction => ({
    type: SHOW_STATUSBAR,
    payload: {
        message,
        isError
    }
})

export const hideStatusbar = (): IHideStatusbarAction => ({
    type: HIDE_STATUSBAR
})
