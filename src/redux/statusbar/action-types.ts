import {IResetSessionAction} from '../global-action-types'

export const SHOW_STATUSBAR = 'SHOW_STATUSBAR'
export const HIDE_STATUSBAR = 'HIDE_STATUSBAR'

export interface IShowStatusbarAction {
    type: typeof SHOW_STATUSBAR
    payload: {
        message: string
        isError: boolean
    }
}

export interface IHideStatusbarAction {
    type: typeof HIDE_STATUSBAR
}

export type TStatusbarActionTypes = IResetSessionAction | IShowStatusbarAction | IHideStatusbarAction
