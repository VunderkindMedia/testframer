import {Dispatch} from 'redux'
import {api} from '../../api'
import {PaymentState} from '../../model/framer/payment-state'
import {
    SET_PAYMENTS,
    SET_PAYMENTS_ON_PAGE,
    SET_PAYMENTS_TOTAL_COUNT,
    ISetPayments,
    ISetPaymentsTotalCount,
    ISetPaymentsOnPage
} from './action-types'

const setPayments = (payments: PaymentState[]): ISetPayments => ({
    type: SET_PAYMENTS,
    payload: payments
})

export const setPaymentsOnPage = (count: number): ISetPaymentsOnPage => ({
    type: SET_PAYMENTS_ON_PAGE,
    payload: count
})

const setPaymentsTotalCount = (count: number): ISetPaymentsTotalCount => ({
    type: SET_PAYMENTS_TOTAL_COUNT,
    payload: count
})

export const loadPaymentHistory =
    (filter = '') =>
    async (dispatch: Dispatch): Promise<void> => {
        const result = await api.checkout.payments(filter)

        dispatch(setPayments(result.payments))
        dispatch(setPaymentsTotalCount(result.totalCount))
    }

export const loadPaymentsCsv =
    (filter = '') =>
    async (): Promise<void> => {
        const result = await api.checkout.paymentsCsv(filter)
        const link = document.createElement('a')
        link.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(result))
        link.setAttribute('download', 'payments.csv')
        link.click()
    }
