import {Reducer} from 'redux'
import {PaymentState} from '../../model/framer/payment-state'
import {TAccountPaymentsTypes, SET_PAYMENTS, SET_PAYMENTS_ON_PAGE, SET_PAYMENTS_TOTAL_COUNT} from './action-types'
import {RESET_SESSION} from '../global-action-types'

export interface IPaymentsState {
    payments: PaymentState[]
    paymentsOnPage: number
    paymentsTotalCount: number
}

const initialState: IPaymentsState = {
    payments: [],
    paymentsOnPage: -1,
    paymentsTotalCount: 0
}

export const payments: Reducer<IPaymentsState, TAccountPaymentsTypes> = (state = initialState, action) => {
    switch (action.type) {
        case SET_PAYMENTS:
            return {...state, payments: action.payload}
        case SET_PAYMENTS_TOTAL_COUNT:
            return {...state, paymentsTotalCount: action.payload}
        case SET_PAYMENTS_ON_PAGE:
            return {...state, paymentsOnPage: action.payload}
        case RESET_SESSION: {
            return {...initialState, paymentsOnPage: state.paymentsOnPage}
        }
        default:
            return state
    }
}
