import {IResetSessionAction} from '../global-action-types'
import {PaymentState} from '../../model/framer/payment-state'

export const SET_PAYMENTS = 'SET_PAYMENTS'
export const SET_PAYMENTS_TOTAL_COUNT = 'SET_PAYMENTS_TOTAL_COUNT'
export const SET_PAYMENTS_ON_PAGE = 'SET_PAYMENTS_ON_PAGE'

export interface ISetPayments {
    type: typeof SET_PAYMENTS
    payload: PaymentState[]
}

export interface ISetPaymentsOnPage {
    type: typeof SET_PAYMENTS_ON_PAGE
    payload: number
}

export interface ISetPaymentsTotalCount {
    type: typeof SET_PAYMENTS_TOTAL_COUNT
    payload: number
}

export type TAccountPaymentsTypes = IResetSessionAction | ISetPayments | ISetPaymentsOnPage | ISetPaymentsTotalCount
