import {IResetSessionAction} from '../global-action-types'

export const SET_APP_VERSION = 'SET_APP_VERSION'
export const SET_LATEST_APP_VERSION = 'SET_LATEST_APP_VERSION'
export const SET_RESTRICTED_MODE = 'SET_RESTRICTED_MODE'

export interface ISetAppVersionAction {
    type: typeof SET_APP_VERSION
    payload: string
}

export interface ISetLatestAppVersionAction {
    type: typeof SET_LATEST_APP_VERSION
    payload?: string
}

export interface ISetRestrictedMode {
    type: typeof SET_RESTRICTED_MODE
    payload: boolean
}

export type TRequestActionTypes = IResetSessionAction | ISetAppVersionAction | ISetLatestAppVersionAction | ISetRestrictedMode
