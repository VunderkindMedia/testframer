import {Reducer} from 'redux'
import {TRequestActionTypes, SET_APP_VERSION, SET_LATEST_APP_VERSION, SET_RESTRICTED_MODE} from './action-types'
import {APP_VERSION} from '../../constants/local-storage'

export interface ISettingsState {
    appVersion: string
    latestAppVersion?: string
    isRestrictedMode: boolean
}

const appVersion = localStorage.getItem(APP_VERSION)

const initialState: ISettingsState = {
    appVersion: appVersion ?? '',
    isRestrictedMode: false
}

export const settings: Reducer<ISettingsState> = (state = initialState, action: TRequestActionTypes) => {
    switch (action.type) {
        case SET_APP_VERSION: {
            localStorage.setItem(APP_VERSION, action.payload)
            return {...state, appVersion: action.payload}
        }
        case SET_LATEST_APP_VERSION: {
            const updatedState = {...state}

            if (!action.payload) {
                delete updatedState.latestAppVersion
            } else {
                updatedState.latestAppVersion = action.payload
            }

            return updatedState
        }
        case SET_RESTRICTED_MODE: {
            return {...state, isRestrictedMode: action.payload}
        }
        default: {
            return state
        }
    }
}
