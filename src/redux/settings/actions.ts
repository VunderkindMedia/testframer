import {Dispatch} from 'redux'
import {
    SET_APP_VERSION,
    SET_LATEST_APP_VERSION,
    SET_RESTRICTED_MODE,
    ISetAppVersionAction,
    ISetLatestAppVersionAction,
    ISetRestrictedMode
} from './action-types'
import {IAppState} from '../root-reducer'
import {api} from '../../api'
import {clearCache} from '../../helpers/cache'
import {NAV_ORDERS} from '../../constants/navigation'
import {AppVersion} from '../../model/app-version'

const setAppVersion = (version: string): ISetAppVersionAction => ({
    type: SET_APP_VERSION,
    payload: version
})

export const setLatestAppVersion = (version?: string): ISetLatestAppVersionAction => ({
    type: SET_LATEST_APP_VERSION,
    payload: version
})

export const setRestrictedMode = (flag: boolean): ISetRestrictedMode => ({
    type: SET_RESTRICTED_MODE,
    payload: flag
})

export const checkAppVersion =
    (appVersion?: AppVersion) =>
    async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
        try {
            const version = appVersion ?? (await api.settings.getAppVersion())

            const state = getState()
            const {settings, router} = state
            const currentVersion = settings.appVersion

            if (version.frontendVersion !== currentVersion) {
                if (router.location.pathname === NAV_ORDERS) {
                    await updateAppVersion(version.frontendVersion)(dispatch)
                } else {
                    dispatch(setLatestAppVersion(version.frontendVersion))
                }
            }
        } catch (e) {
            console.error(e)
        }
    }

export const updateAppVersion =
    (version: string) =>
    async (dispatch: Dispatch): Promise<void> => {
        dispatch(setAppVersion(version))
        dispatch(setLatestAppVersion())
        await clearCache()
        document.location.reload()
    }
