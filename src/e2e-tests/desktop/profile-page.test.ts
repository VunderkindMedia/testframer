import {Browser, Page} from 'puppeteer'
import {afterAllAction, beforeAllAction, DEFAULT_TIMEOUT, waitForResponse} from '../helpers'
import {data} from './profile-page-data'
import {testLoginAction} from './helpers'

jest.setTimeout(DEFAULT_TIMEOUT)

let browser: Browser
let page: Page

beforeAll(async () => {
    const res = await beforeAllAction()
    browser = res.browser
    page = res.page
})

const navigateToTab = async (page: Page, tabId: string) => {
    await page.waitForSelector(`[data-test="${tabId}"]`)
    await page.$eval(`[data-test="${tabId}"]`, e => (e as HTMLElement).click())
    await page.waitForSelector(`[data-test="${tabId}-content"]`)
}

const updateOfferField = async (selector: string, value: string) => {
    await page.evaluate(s => {
        const input = document.querySelector(s) as HTMLInputElement
        input.value = ''
    }, `[data-test="${selector}"]`)

    const input = await page.waitForSelector(`[data-test="${selector}"]`)

    await input.type(value)
    await waitForResponse(page, '/offerfields')

    const newValue = await page.evaluate(el => el.value, input)
    expect(newValue).toBe(value)
}

describe('navigate to profile page', () => {
    test(
        'navigate to profile page',
        async () => {
            await testLoginAction(browser, page)
            await page.waitForSelector('[data-test="profile-page"]')
            await page.$eval('[data-test="profile-page"]', e => (e as HTMLElement).click())
            await page.waitForSelector('[data-test="margins"]')
        },
        DEFAULT_TIMEOUT
    )
})

describe('margins', () => {
    test(
        'update construction margin',
        async () => {
            const marginInput = await page.waitForSelector('[data-test="construction-margin"]')

            await page.evaluate(s => {
                const input = document.querySelector(s) as HTMLInputElement
                input.value = ''
            }, '[data-test="construction-margin"]')

            await marginInput.type(data.constructionMargin)
            await waitForResponse(page, '/margin')

            const newValue = await page.evaluate(el => el.value, marginInput)
            expect(newValue).toBe(data.constructionMargin)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'update good margin',
        async () => {
            const marginInput = await page.waitForSelector('[data-test="good-margin"]')

            await page.evaluate(s => {
                const input = document.querySelector(s) as HTMLInputElement
                input.value = ''
            }, '[data-test="good-margin"]')

            await marginInput.type(data.goodMargin)
            await waitForResponse(page, '/goodsmargin')

            const newValue = await page.evaluate(el => el.value, marginInput)
            expect(newValue).toBe(data.goodMargin)
        },
        DEFAULT_TIMEOUT
    )
})

describe('print settings', () => {
    test('navigate to print settings tab', async () => await navigateToTab(page, 'print-settings'), DEFAULT_TIMEOUT)
    test('update company name', async () => await updateOfferField('offerAddonLine1', data.companyName), DEFAULT_TIMEOUT)
    test('update contacts', async () => await updateOfferField('offerAddonLine2', data.contacts), DEFAULT_TIMEOUT)
})

describe('goods', () => {
    test('navigate to partner goods tab', async () => await navigateToTab(page, 'partner-goods'), DEFAULT_TIMEOUT)
    test(
        'add new good',
        async () => {
            const goodsCountStart = (await page.$$('[data-test="partner-goods-list"] li')).length

            await page.waitForSelector(`[data-test="add-goods-item-0-level-0-title"]`)

            await page.waitForSelector('[data-test="add-goods"]', {visible: true})
            await page.$eval('[data-test="add-goods"]', e => (e as HTMLElement).click())

            await page.waitForSelector(`[data-test="add-goods-item-0-level-0-title"]`, {visible: true})
            await page.$eval(`[data-test="add-goods-item-0-level-0-title"]`, e => (e as HTMLElement).click())

            await waitForResponse(page, '/partnergoods')
            await page.waitForTimeout(5000)

            await page.waitForSelector('[data-test="partner-goods-list"]')
            const goodsCountEnd = (await page.$$('[data-test="partner-goods-list"] li')).length
            expect(goodsCountEnd).toBeGreaterThan(goodsCountStart)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'update good',
        async () => {
            const sku = await page.waitForSelector('[data-test="partner-goods-list"] li:first-of-type input[data-test="partner-goods-sku"]')
            const skuValue = await page.evaluate(el => el.value, sku)
            const title = await page.waitForSelector('[data-test="partner-goods-list"] li:first-of-type input[data-test="partner-goods-title"]')
            const titleValue = await page.evaluate(el => el.value, title)
            const price = await page.waitForSelector('[data-test="partner-goods-list"] li:first-of-type input[data-test="partner-goods-price"]')
            const priceValue = await page.evaluate(el => el.value, price)

            await sku.type('1')
            await waitForResponse(page, '/partnergoods')
            const newSkuValue = await page.evaluate(el => el.value, sku)

            await title.type('1')
            await waitForResponse(page, '/partnergoods')
            const newTitleValue = await page.evaluate(el => el.value, title)

            await price.type('1')
            await waitForResponse(page, '/partnergoods')
            const newPriceValue = await page.evaluate(el => el.value, price)

            expect(newSkuValue).toBe(`${skuValue}1`)
            expect(newTitleValue).toBe(`${titleValue}1`)
            expect(newPriceValue).toBe(`${priceValue}1`)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'delete good',
        async () => {
            await page.waitForSelector('[data-test="partner-goods-list"] li:first-of-type button')
            const goodsCountStart = (await page.$$('[data-test="partner-goods-list"] li')).length
            await page.$eval('[data-test="partner-goods-list"] li:first-of-type button', e => (e as HTMLElement).click())

            await waitForResponse(page, '/partnergoods')
            await page.waitForTimeout(5000)

            const goodsCountEnd = (await page.$$('[data-test="partner-goods-list"] li')).length
            expect(goodsCountEnd).toBeLessThan(goodsCountStart)
        },
        DEFAULT_TIMEOUT
    )
})

describe('services', () => {
    test('navigate to services tab', async () => await navigateToTab(page, 'additional-services'), DEFAULT_TIMEOUT)
    test(
        'update installation',
        async () => {
            const input = await page.waitForSelector('[data-test="installation"] input[type="number"]')

            await page.evaluate(() => {
                const input = document.querySelector('[data-test="installation"] input[type="number"]') as HTMLInputElement
                input.value = ''
            })

            await input.type(data.installationCost)
            await waitForResponse(page, '/construction')

            const value = await page.evaluate(el => el.value, input)
            expect(value).toBe(data.installationCost)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'add additional service',
        async () => {
            const checkServiceTemplate = async () => {
                const title = await page.waitForSelector('[data-test="service-editor"]:last-of-type input[type="text"]')
                const titleValue = await page.evaluate(el => el.value, title)
                expect(titleValue).toBe('')

                const price = await page.waitForSelector('[data-test="service-editor"]:last-of-type input[type="number"]')
                const priceValue = await page.evaluate(el => el.value, price)
                expect(priceValue).toBe('')
            }

            await checkServiceTemplate()

            const title = await page.waitForSelector('[data-test="service-editor"]:last-of-type input[type="text"]')
            await title.type(data.additionalService.title)

            const price = await page.waitForSelector('[data-test="service-editor"]:last-of-type input[type="number"]')
            await price.type(data.additionalService.price)

            await waitForResponse(page, '/services')
            const newTitleValue = await page.evaluate(el => el.value, title)
            expect(newTitleValue).toBe(data.additionalService.title)
            const newPriceValue = await page.evaluate(el => el.value, price)
            expect(newPriceValue).toBe(data.additionalService.price)

            await checkServiceTemplate()
        },
        DEFAULT_TIMEOUT
    )
    test(
        'update additional service',
        async () => {
            const price = await page.waitForSelector('[data-test="service-editor"]:first-of-type input[type="number"]')
            await page.evaluate(() => {
                const input = document.querySelector('[data-test="service-editor"]:first-of-type input[type="number"]') as HTMLInputElement
                input.value = ''
            })
            await price.type(data.additionalService.price)

            await waitForResponse(page, '/services')
            const newPriceValue = await page.evaluate(el => el.value, price)
            expect(newPriceValue).toBe(data.additionalService.price)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'delete additional service',
        async () => {
            await page.waitForSelector('[data-test="service-editor"]:first-of-type')
            const id = await page.$eval('[data-test="service-editor"]:first-of-type', e => e.getAttribute('data-id'))

            await page.waitForSelector('[data-test="service-editor"]:first-of-type button')
            await page.$eval('[data-test="service-editor"]:first-of-type button', e => (e as HTMLElement).click())

            await waitForResponse(page, '/services')
            await page.waitForSelector(`[data-id="${id}"]`, {hidden: true})
        },
        DEFAULT_TIMEOUT
    )
})

describe('affiliates', () => {
    test('navigate to affiliates tab', async () => await navigateToTab(page, 'affiliate'), DEFAULT_TIMEOUT)
    test(
        'add affiliate',
        async () => {
            await page.waitForSelector('[data-test="affiliates-list"]')
            const affiliatesCountStart = (await page.$$('[data-test="affiliates-list"] li')).length

            await page.waitForSelector('[data-test="profile-page-add-affiliate-button"]')
            await page.$eval('[data-test="profile-page-add-affiliate-button"]', e => (e as HTMLElement).click())
            await page.waitForSelector('[data-test="affiliates-item-new"]')

            const input = await page.waitForSelector('[data-test="affiliates-item-new"] input')
            const inputValue = await page.evaluate(el => el.value, input)
            expect(inputValue).toBe('')

            await input.type(data.affiliate)
            await waitForResponse(page, '/filials')
            await page.waitForSelector(`[data-test="affiliates-item-new"]`, {hidden: true})

            const affiliatesCountEnd = (await page.$$('[data-test="affiliates-list"] li')).length
            expect(affiliatesCountEnd).toBeGreaterThan(affiliatesCountStart)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'update affiliate',
        async () => {
            const input = await page.waitForSelector('[data-test="affiliates-list"] li:last-of-type input')
            const inputValue = await page.evaluate(el => el.value, input)
            await input.type('1')

            await waitForResponse(page, '/filials')
            const newValue = await page.evaluate(el => el.value, input)
            expect(newValue).toBe(`${inputValue}1`)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'delete affiliate',
        async () => {
            const affiliatesCountStart = (await page.$$('[data-test="affiliates-list"] li')).length
            const selector = await page.$eval('[data-test="affiliates-list"] li:last-of-type', e => e.getAttribute('data-test'))
            await page.waitForSelector(`[data-test="${selector}"] button`)
            await page.$eval(`[data-test="${selector}"] button`, e => (e as HTMLElement).click())

            await waitForResponse(page, '/filials')
            await page.waitForSelector(`[data-test="${selector}"]`, {hidden: true})

            const affiliatesCountEnd = (await page.$$('[data-test="affiliates-list"] li')).length
            expect(affiliatesCountEnd).toBeLessThan(affiliatesCountStart)
        },
        DEFAULT_TIMEOUT
    )
})

describe('employees', () => {
    test(
        'add new employee',
        async () => {
            const employeeCountStart = (await page.$$('[data-test="employees-list"] li')).length

            await page.waitForSelector('[data-test="profile-page-add-employee-button"]')

            await page.$eval('[data-test="profile-page-add-employee-button"]', e => (e as HTMLElement).click())
            await page.waitForSelector('[data-test="employees-list"]')

            const inputName = await page.waitForSelector('[data-test="employees-item-new"] > input:first-of-type')
            await inputName.type(data.user.name)

            const inputEmail = await page.waitForSelector('[data-test="employees-item-new"] > input:nth-of-type(2)')
            await inputEmail.type(`${new Date().getTime()}${data.user.email}`)

            await page.waitForSelector('[data-test="employees-item-new"] [data-test="employees-role"]')
            await page.$eval('[data-test="employees-item-new"] [data-test="employees-role"]', e => (e as HTMLElement).click())
            await page.waitForSelector('[data-test="employees-role-body"]')
            await page.waitForSelector('[data-test="employees-item-new"] [data-test^="employees-role-item"] > p')
            await page.$eval(`[data-test="employees-item-new"] [data-test^="employees-role-item"] > p`, e => (e as HTMLElement).click())

            await page.waitForSelector('[data-test="employees-item-new"] [data-test="employees-affiliate"]')
            await page.$eval('[data-test="employees-item-new"] [data-test="employees-affiliate"]', e => (e as HTMLElement).click())
            await page.waitForSelector('[data-test="employees-affiliate-body"]')
            await page.waitForSelector('[data-test="employees-item-new"] [data-test^="employees-affiliate-item"] > p')
            await page.$eval('[data-test="employees-item-new"] [data-test^="employees-affiliate-item"] > p', e => (e as HTMLElement).click())

            await waitForResponse(page, '/users')
            await page.waitForTimeout(5000)
            await page.waitForSelector(`[data-test="affiliates-item-new"]`, {hidden: true})

            const employeeCountEnd = (await page.$$('[data-test="employees-list"] li')).length

            expect(employeeCountEnd).toBeGreaterThan(employeeCountStart)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'update employee',
        async () => {
            await page.waitForSelector('[data-test="employees-list"] li:first-of-type')

            const inputName = await page.waitForSelector('[data-test="employees-list"] li:first-of-type > input:first-of-type')
            const inputNameValue = await page.evaluate(el => el.value, inputName)
            const inputEmail = await page.waitForSelector('[data-test="employees-list"] li:first-of-type > input:nth-of-type(2)')
            const inputEmailValue = await page.evaluate(el => el.value, inputEmail)

            await page.waitForSelector('[data-test="employees-list"] li:first-of-type [data-test="employees-role"]')
            await page.$eval('[data-test="employees-list"] li:first-of-type [data-test="employees-role"]', e => (e as HTMLElement).click())
            await page.waitForSelector('[data-test="employees-role-body"]')
            await page.waitForSelector('[data-test="employees-list"] li:first-of-type [data-test^="employees-role-item"] > p')
            await page.$eval('[data-test="employees-list"] li:first-of-type [data-test^="employees-role-item"] > p', e =>
                (e as HTMLElement).click()
            )

            await inputName.type('1')
            await waitForResponse(page, '/users')
            await page.waitForTimeout(8000)

            await inputEmail.type('m')
            await waitForResponse(page, '/users')
            await page.waitForTimeout(8000)

            const newInputNameValue = await page.evaluate(el => el.value, inputName)
            const newInputEmailValue = await page.evaluate(el => el.value, inputEmail)

            expect(newInputNameValue).toBe(`${inputNameValue}1`)
            expect(newInputEmailValue).toBe(`${inputEmailValue}1`)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'delete employee',
        async () => {
            await page.waitForSelector('[data-test="employees-list"] li:first-of-type button')
            const employeeCountStart = (await page.$$('[data-test="employees-list"] li')).length

            await page.$eval('[data-test="employees-list"] li:first-of-type button', e => (e as HTMLElement).click())
            await waitForResponse(page, '/users')
            await page.waitForTimeout(5000)

            const employeeCountEnd = (await page.$$('[data-test="employees-list"] li')).length
            expect(employeeCountEnd).toBeLessThan(employeeCountStart)
        },
        DEFAULT_TIMEOUT
    )
})

describe('dealers', () => {
    test('navigate to dealers tab', async () => await navigateToTab(page, 'dealers'), DEFAULT_TIMEOUT)
    test(
        'add new dealer',
        async () => {
            await page.waitForSelector('[data-test="dealer-list"] label')
            const dealerCountStart = (await page.$$('[data-test="dealer-list"] label')).length

            await page.waitForSelector('[data-test="add-dealer"]')
            await page.$eval('[data-test="add-dealer"]', e => (e as HTMLElement).click())

            await page.waitForSelector('[data-test="modal-dealer"]')
            await page.waitForSelector('[data-test="dealer-name"][disabled]')
            await page.waitForSelector('[data-test="dealer-contacts"][disabled]')
            await page.waitForSelector('[data-test="dealer-contact-person"][disabled]')
            await page.waitForSelector('[data-test="save-dealer"][disabled]')
            await page.waitForSelector('[data-test="delaer-inn"]')
            await page.type('[data-test="delaer-inn"]', data.dealer.inn)

            await waitForResponse(page, '/dealers/innlookup')
            await page.waitForTimeout(5000)

            const dealerType = await page.waitForSelector('[data-test="dealer-type"] > p')
            const dealerTypeValue = await page.evaluate(el => el.innerText, dealerType)
            expect(dealerTypeValue).toBe(data.dealer.type)

            await page.waitForSelector('[data-test="dealer-name"]:not(:disabled)')
            await page.type('[data-test="dealer-name"]', data.dealer.name)

            await page.waitForSelector('[data-test="dealer-contact-person"]:not(:disabled)')
            await page.type('[data-test="dealer-contact-person"]', data.dealer.contactPerson)

            await page.waitForSelector('[data-test="dealer-contacts"]:not(:disabled)')
            await page.type('[data-test="dealer-contacts"]', data.dealer.contacts)

            await page.waitForSelector('[data-test="save-dealer"]:not(:disabled)')
            await page.$eval('[data-test="save-dealer"', e => (e as HTMLElement).click())

            await waitForResponse(page, '/dealers')
            await page.waitForTimeout(5000)

            await page.waitForSelector('body:not(.with-modal)')
        },
        DEFAULT_TIMEOUT
    )
})

afterAll(async () => await afterAllAction(browser))
