import {Browser, Page} from 'puppeteer'
import {afterAllAction, beforeAllAction, DEFAULT_TIMEOUT} from '../helpers'
import {testLoginAction} from './helpers'

jest.setTimeout(DEFAULT_TIMEOUT)

let browser: Browser
let page: Page

beforeAll(async () => {
    const res = await beforeAllAction()
    browser = res.browser
    page = res.page
})

describe('login page', () => {
    test('login', async () => await testLoginAction(browser, page), DEFAULT_TIMEOUT)
})

afterAll(async () => await afterAllAction(browser))
