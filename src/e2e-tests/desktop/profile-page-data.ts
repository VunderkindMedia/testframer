export const data = {
    constructionMargin: '10',
    goodMargin: '15',
    companyName: 'ИП Константинополeский Константин Константинович',
    contacts: 'Ваш персональный менеджер: Зубарев Сергей тел.: +79226459592',
    installationCost: '500',
    additionalService: {title: 'Новая услуга', price: '1000'},
    affiliate: 'Филиал',
    user: {name: 'Иванов', email: 'ivan1@gmail.com'},
    dealer: {inn: '111111111111', type: 'Физическое лицо', name: 'Иванов Иван', contactPerson: 'Иванов Иван', contacts: '111111'}
}
