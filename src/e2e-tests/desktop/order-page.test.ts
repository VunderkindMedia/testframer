import {Browser, Page} from 'puppeteer'
import {afterAllAction, beforeAllAction, DEFAULT_TIMEOUT, waitForResponse} from '../helpers'
import {ORDER_TAB_ID} from '../../constants/navigation'
import {ShippingTypes} from '../../model/framer/shipping-types'
import {testLoginAction, testCreateOrderAction} from './helpers'

jest.setTimeout(DEFAULT_TIMEOUT)

let browser: Browser
let page: Page
let orderNumber: string

export const testRemoveOrderAction = async (browser: Browser, page: Page): Promise<void> => {
    await page.waitForSelector('[data-test="menu-nav-to-orders-page"]')
    await page.$eval('[data-test="menu-nav-to-orders-page"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="orders-number-filter-input"]')
    await page.evaluate(s => {
        const input = document.querySelector(s) as HTMLInputElement
        input.value = ''
    }, '[data-test="orders-number-filter-input"]')
    await page.$eval('[data-test="orders-number-filter-input"]', e => (e as HTMLElement).click())
    await page.type('[data-test="orders-number-filter-input"]', orderNumber)

    await page.waitForSelector(`.selected[data-test="order-table-row-${orderNumber}"]`)

    await page.waitForSelector('[data-test="remove-order-button"]')
    await page.$eval('[data-test="remove-order-button"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="submit-order-remove"]')
    await page.$eval('[data-test="submit-order-remove"]', e => (e as HTMLElement).click())

    await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"]`, {hidden: true})
}

export const testSetOrderDiscountAction = async (browser: Browser, page: Page): Promise<void> => {
    await page.waitForSelector(`[data-test^="${ORDER_TAB_ID}"]`)
    await page.$eval(`[data-test^="${ORDER_TAB_ID}"]`, element => (element as HTMLElement).click())

    const testDiscount = async (value: string) => {
        await page.$eval('[data-test="order-discount-input"]', e => (e as HTMLElement).click())

        const discountInput = await page.waitForSelector('[data-test="order-discount-input"]')
        await discountInput.type(value)

        await waitForResponse(page, 'discount')

        await page.reload()

        await page.waitForSelector('[data-test="order-discount-input"]')
        const discount = await page.$eval('[data-test="order-discount-input"]', e => (e as HTMLInputElement).value)

        expect(discount).toBe(value)
    }

    await testDiscount('500')

    await page.waitForSelector('[data-test="order-discount-radio-group"] > [data-test="radio-group-item"]:nth-child(2)')
    await page.$eval('[data-test="order-discount-radio-group"] > [data-test="radio-group-item"]:nth-child(2)', e => (e as HTMLElement).click())

    await testDiscount('10')
}

export const testAddGoodsAction = async (browser: Browser, page: Page): Promise<void> => {
    // factory goods
    await page.waitForSelector('[data-test="order-page-factory-goods-select"]')
    await page.$eval('[data-test="order-page-factory-goods-select"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="order-page-factory-goods-select-item-0-level-1-title"]', {visible: true})
    await page.$eval('[data-test="order-page-factory-goods-select-item-0-level-1-title"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="order-page-factory-goods-select-item-0-level-2-title"]', {visible: true})
    await page.$eval('[data-test="order-page-factory-goods-select-item-0-level-2-title"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="order-page-factory-goods-select-item-0-level-3-title"]', {visible: true})
    await page.$eval('[data-test="order-page-factory-goods-select-item-0-level-3-title"]', e => (e as HTMLElement).click())

    await waitForResponse(page, 'addgood')
    await page.waitForSelector('[data-test="order-page-factory-goods-good-card-0"]')

    // partner goods
    await page.waitForSelector('[data-test="order-page-partner-goods-select"]')
    await page.$eval('[data-test="order-page-partner-goods-select"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="order-page-partner-goods-select-item-0-level-0-title"]', {visible: true})
    await page.$eval('[data-test="order-page-partner-goods-select-item-0-level-0-title"]', e => (e as HTMLElement).click())

    await waitForResponse(page, 'addgood')
    await page.waitForSelector('[data-test="order-page-partner-goods-good-card-0"]')
}

export const testAddServicesAction = async (browser: Browser, page: Page): Promise<void> => {
    await page.waitForSelector('[data-test="order-page-partner-services-select"]')
    await page.$eval('[data-test="order-page-partner-services-select"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="order-page-partner-services-select-item-0-level-0-title"]', {visible: true})
    await page.$eval('[data-test="order-page-partner-services-select-item-0-level-0-title"]', e => (e as HTMLElement).click())

    await waitForResponse(page, 'orders')
    await page.waitForSelector('[data-test="order-page-partner-services-service-card-0"]')
}

export const testRecalculateOrder = async (browser: Browser, page: Page): Promise<void> => {
    await page.waitForSelector('[data-test="order-page-recalculate-button"]')
    await page.$eval('[data-test="order-page-recalculate-button"]', e => (e as HTMLElement).click())

    await waitForResponse(page, 'orders')

    await page.waitForSelector('[data-test="order-page-to-production-button"]')

    await page.waitForSelector('[data-test="order-page-order-total-cost"]')
    const totalCost = await page.$eval('[data-test="order-page-order-total-cost"]', e =>
        parseFloat(e.innerHTML.replace('&nbsp;', '').replace(' ', ''))
    )

    expect(totalCost).toBeGreaterThan(0)
}

export const testTransferToProductionOrder = async (
    browser: Browser,
    page: Page,
    shippingType: ShippingTypes = ShippingTypes.Pickup
): Promise<void> => {
    await page.waitForSelector('[data-test="order-page-to-production-button"]')
    await page.$eval('[data-test="order-page-to-production-button"]', e => (e as HTMLElement).click())

    if (shippingType === ShippingTypes.Delivery) {
        await page.waitForSelector('[data-test="segmented-control-item-1"]')
        await page.$eval('[data-test="segmented-control-item-1"]', e => (e as HTMLElement).click())

        await page.waitForSelector('[data-test="booking-page-address-stage-name-input"]')
        await page.$eval('[data-test="booking-page-address-stage-name-input"]', e => (e as HTMLElement).click())
        await page.type('[data-test="booking-page-address-stage-name-input"]', 'Тестовое имя')

        await page.waitForSelector('[data-test="booking-page-address-stage-phone-input"]')
        await page.$eval('[data-test="booking-page-address-stage-phone-input"]', e => (e as HTMLElement).click())
        await page.type('[data-test="booking-page-address-stage-phone-input"]', '+79991270666')

        await page.waitForSelector('[data-test="booking-page-address-stage-address-input"]')
        await page.$eval('[data-test="booking-page-address-stage-address-input"]', e => (e as HTMLElement).click())
        await page.type('[data-test="booking-page-address-stage-address-input"]', 'Ленина 10')

        await page.waitForSelector('[data-test="autocomplete-input-suggestion-item-0"]')
        await page.$eval('[data-test="autocomplete-input-suggestion-item-0"]', e => (e as HTMLElement).click())

        await waitForResponse(page, 'shipping/calc')
    }

    await page.waitForSelector('[data-test="booking-page-transfer-to-production-button"]:not(:disabled)')
    await page.$eval('[data-test="booking-page-transfer-to-production-button"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="booking-page-transfer-to-production-modal-button"]')
    await page.$eval('[data-test="booking-page-transfer-to-production-modal-button"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="booking-page-date-stage"]')

    await page.waitForSelector('[data-test="date-picker-available-day"]')
    await page.$eval('[data-test="date-picker-available-day"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="booking-page-date-stage-modal-book-button"]', {visible: true})
    await page.$eval('[data-test="booking-page-date-stage-modal-book-button"]', e => (e as HTMLElement).click())

    await page.waitForSelector(`.booked[data-test="order-table-row-${orderNumber}"]`)
}

const testOrderViewMode = async (browser: Browser, page: Page): Promise<void> => {
    await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"] a`)
    await page.$eval(`[data-test="order-table-row-${orderNumber}"] a`, e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="close-order-view-mode-modal"]')
    await page.waitForSelector('[data-test="order-page-recalculate-button"][disabled]')
}

beforeAll(async () => {
    const res = await beforeAllAction()
    browser = res.browser
    page = res.page
})

describe('order page', () => {
    test(
        'create order',
        async () => {
            await testLoginAction(browser, page)
            orderNumber = await testCreateOrderAction(browser, page)
        },
        DEFAULT_TIMEOUT
    )
    test('set order discount', async () => await testSetOrderDiscountAction(browser, page), DEFAULT_TIMEOUT)
    test('recalculate order', async () => await testRecalculateOrder(browser, page), DEFAULT_TIMEOUT)
    test('add goods', async () => await testAddGoodsAction(browser, page), DEFAULT_TIMEOUT)
    test('add services', async () => await testAddServicesAction(browser, page), DEFAULT_TIMEOUT)
    test('transfer to production order (warehouse)', async () => await testTransferToProductionOrder(browser, page), DEFAULT_TIMEOUT)
    test(
        'transfer to production order (delivery)',
        async () => {
            orderNumber = await testCreateOrderAction(browser, page)
            await testRecalculateOrder(browser, page)
            await testTransferToProductionOrder(browser, page, ShippingTypes.Delivery)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'check view mode for order in production',
        async () => {
            await testOrderViewMode(browser, page)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'remove order',
        async () => {
            orderNumber = await testCreateOrderAction(browser, page)
            await testRemoveOrderAction(browser, page)
        },
        DEFAULT_TIMEOUT
    )
})

afterAll(async () => await afterAllAction(browser))
