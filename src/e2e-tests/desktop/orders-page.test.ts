import {Browser, Page} from 'puppeteer'
import {afterAllAction, beforeAllAction, DEFAULT_TIMEOUT, waitForResponse} from '../helpers'
import {ShippingTypes} from '../../model/framer/shipping-types'
import {getPreviousDayDate} from '../../helpers/date'
import {testLoginAction, testCreateOrderAction} from './helpers'

jest.setTimeout(DEFAULT_TIMEOUT)

let browser: Browser
let page: Page
let orderNumber: string

beforeAll(async () => {
    const res = await beforeAllAction()
    browser = res.browser
    page = res.page
})

const filterOrderByNumber = async (): Promise<void> => {
    await page.waitForSelector('[data-test="orders-number-filter-input"]')
    await page.evaluate(s => {
        const input = document.querySelector(s) as HTMLInputElement
        input.value = ''
    }, '[data-test="orders-number-filter-input"]')
    await page.type('[data-test="orders-number-filter-input"]', orderNumber)

    await waitForResponse(page, 'orders')
    await page.waitForTimeout(3000)

    await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"]`)
    let orderCountEnd = (await page.$$('[data-test^="order-table-row"]')).length
    expect(orderCountEnd).toBe(1)
}

const filterOrderByDate = async (): Promise<void> => {
    const prevDate = getPreviousDayDate(new Date())
    await setEndDate(prevDate)

    await waitForResponse(page, 'orders')
    await page.waitForTimeout(3000)

    await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"]`, {hidden: true})
    const orderCountEnd = (await page.$$('[data-test^="order-table-row"]')).length
    expect(orderCountEnd).toBe(0)
}

const reopenOrdersPage = async (): Promise<void> => {
    await page.waitForSelector('[data-test="profile-page"]')
    await page.$eval('[data-test="profile-page"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="menu-nav-to-orders-page"]')
    await page.$eval('[data-test="menu-nav-to-orders-page"]', e => (e as HTMLElement).click())
}

const setStatusFilter = async (index: number): Promise<void> => {
    await page.waitForSelector('[data-test="order-status-select"]')
    await page.$eval('[data-test="order-status-select"]', e => (e as HTMLButtonElement).click())
    await page.waitForSelector('[data-test="order-status-select-body"]')
    await page.waitForSelector(`[data-test="order-status-select-body"] [data-test*="order-status-select-item"]:nth-of-type(${index}) > p`)
    await page.$eval(`[data-test="order-status-select-body"] [data-test*="order-status-select-item"]:nth-of-type(${index}) > p`, e =>
        (e as HTMLButtonElement).click()
    )
}

const setEndDate = async (date: Date): Promise<void> => {
    const dateInput = await page.waitForSelector('[data-test="orders-date-range-end"]')
    const currentDate = await page.evaluate(el => el.value, dateInput)
    const currentMonth = currentDate.split('.')[1] - 1
    await page.$eval('[data-test="orders-date-range-end"]', element => (element as HTMLElement).click())

    const monthDiff = currentMonth - date.getMonth()
    if (monthDiff) {
        const direction = monthDiff > 0 ? 'prev' : 'next'

        await page.waitForSelector(
            `[data-test="orders-date-range-end"] ~ [data-test="date-picker"] [data-test="date-picker-${direction}-button"]`
        )
        await page.$eval(
            `[data-test="orders-date-range-end"] ~ [data-test="date-picker"] [data-test="date-picker-${direction}-button"]`,
            element => (element as HTMLElement).click()
        )
    }

    const day = date.getDate()
    await page.waitForSelector(`[data-test="orders-date-range-end"] ~ [data-test="date-picker"] [data-test="date-picker-${day}"]`)
    await page.$eval(`[data-test="orders-date-range-end"] ~ [data-test="date-picker"] [data-test="date-picker-${day}"]`, element =>
        (element as HTMLElement).click()
    )
}

export const testRecalculateOrder = async (browser: Browser, page: Page): Promise<void> => {
    await page.waitForSelector('[data-test="order-page-recalculate-button"]')
    await page.$eval('[data-test="order-page-recalculate-button"]', e => (e as HTMLElement).click())

    await waitForResponse(page, 'orders')

    await page.waitForSelector('[data-test="order-page-to-production-button"]')

    await page.waitForSelector('[data-test="order-page-order-total-cost"]')
    const totalCost = await page.$eval('[data-test="order-page-order-total-cost"]', e =>
        parseFloat(e.innerHTML.replace('&nbsp;', '').replace(' ', ''))
    )

    expect(totalCost).toBeGreaterThan(0)
}

export const testTransferToProductionOrder = async (
    browser: Browser,
    page: Page,
    shippingType: ShippingTypes = ShippingTypes.Pickup
): Promise<void> => {
    await page.waitForSelector('[data-test="order-page-to-production-button"]')
    await page.$eval('[data-test="order-page-to-production-button"]', e => (e as HTMLElement).click())

    if (shippingType === ShippingTypes.Delivery) {
        await page.waitForSelector('[data-test="segmented-control-item-1"]')
        await page.$eval('[data-test="segmented-control-item-1"]', e => (e as HTMLElement).click())

        await page.waitForSelector('[data-test="booking-page-address-stage-name-input"]')
        await page.$eval('[data-test="booking-page-address-stage-name-input"]', e => (e as HTMLElement).click())
        await page.type('[data-test="booking-page-address-stage-name-input"]', 'Тестовое имя')

        await page.waitForSelector('[data-test="booking-page-address-stage-phone-input"]')
        await page.$eval('[data-test="booking-page-address-stage-phone-input"]', e => (e as HTMLElement).click())
        await page.type('[data-test="booking-page-address-stage-phone-input"]', '+79991270666')

        await page.waitForSelector('[data-test="booking-page-address-stage-address-input"]')
        await page.$eval('[data-test="booking-page-address-stage-address-input"]', e => (e as HTMLElement).click())
        await page.type('[data-test="booking-page-address-stage-address-input"]', 'Ленина 10')

        await page.waitForSelector('[data-test="autocomplete-input-suggestion-item-0"]')
        await page.$eval('[data-test="autocomplete-input-suggestion-item-0"]', e => (e as HTMLElement).click())

        await waitForResponse(page, 'shipping/calc')
    }

    await page.waitForSelector('[data-test="booking-page-transfer-to-production-button"]:not(:disabled)')
    await page.$eval('[data-test="booking-page-transfer-to-production-button"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="booking-page-transfer-to-production-modal-button"]')
    await page.$eval('[data-test="booking-page-transfer-to-production-modal-button"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="booking-page-date-stage"]')

    await page.waitForSelector('[data-test="date-picker-available-day"]')
    await page.$eval('[data-test="date-picker-available-day"]', e => (e as HTMLElement).click())

    await page.waitForSelector('[data-test="booking-page-date-stage-modal-book-button"]', {visible: true})
    await page.$eval('[data-test="booking-page-date-stage-modal-book-button"]', e => (e as HTMLElement).click())

    await page.waitForSelector(`.booked[data-test="order-table-row-${orderNumber}"]`)
}

describe('orders page', () => {
    let orderCountStart: number | undefined

    test(
        'create new order',
        async () => {
            await testLoginAction(browser, page)
            orderNumber = await testCreateOrderAction(browser, page)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'open orders list',
        async () => {
            await page.waitForSelector('[data-test="menu-nav-to-orders-page"]')
            await page.$eval('[data-test="menu-nav-to-orders-page"]', e => (e as HTMLElement).click())

            await page.waitForSelector('[data-test="orders-page-table"]')
        },
        DEFAULT_TIMEOUT
    )
    test(
        'set order number filter',
        async () => {
            orderCountStart = (await page.$$('[data-test^="order-table-row"]')).length

            await filterOrderByNumber()
        },
        DEFAULT_TIMEOUT
    )
    test(
        'check saved order number filter',
        async () => {
            await reopenOrdersPage()

            const input = await page.waitForSelector('[data-test="orders-number-filter-input"]')
            const inputValue = await page.evaluate(el => el.value, input)
            expect(inputValue).toBe(orderNumber)

            await page.waitForSelector('[data-test="orders-page-table"]')
            await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"]`)
            let orderCountEnd = (await page.$$('[data-test^="order-table-row"]')).length
            expect(orderCountEnd).toBe(1)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'clear order number filter',
        async () => {
            await page.waitForSelector('[data-test="orders-number-filter-input"] ~ button')
            await page.$eval('[data-test="orders-number-filter-input"] ~ button', element => (element as HTMLButtonElement).click())

            await waitForResponse(page, 'orders')
            await page.waitForTimeout(3000)

            let orderCountEnd = (await page.$$('[data-test^="order-table-row"]')).length
            expect(orderCountEnd).toBe(orderCountStart)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'set status filter',
        async () => {
            await filterOrderByNumber()
            await setStatusFilter(3)

            await waitForResponse(page, 'orders')
            await page.waitForTimeout(3000)

            await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"]`, {hidden: true})
            const orderCountEnd = (await page.$$('[data-test^="order-table-row"]')).length
            expect(orderCountEnd).toBe(0)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'check saved order status filter',
        async () => {
            await reopenOrdersPage()

            const statusInput = await page.waitForSelector('[data-test="order-status-select"] > p')
            const statusText = await page.evaluate(el => el.innerText, statusInput)
            expect(statusText).toBe('В производстве')

            await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"]`, {hidden: true})
            const orderCountEnd = (await page.$$('[data-test^="order-table-row"]')).length
            expect(orderCountEnd).toBe(0)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'clear status filter',
        async () => {
            await setStatusFilter(1)

            await waitForResponse(page, 'orders')
            await page.waitForTimeout(3000)

            await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"]`)
            const orderCountEnd = (await page.$$('[data-test^="order-table-row"]')).length
            expect(orderCountEnd).toBe(1)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'set date filter',
        async () => {
            await filterOrderByDate()
        },
        DEFAULT_TIMEOUT
    )
    test(
        'check saved order date filter',
        async () => {
            await reopenOrdersPage()

            const dateInput = await page.waitForSelector('[data-test="orders-date-range-end"]')
            const dateValue = await page.evaluate(el => el.value, dateInput)
            const date = new Date()
            const fmtDate = `${date.getDate().toString(10).padStart(2, '0')}.${(date.getMonth() + 1)
                .toString(10)
                .padStart(2, '0')}.${date.getFullYear()}`
            expect(dateValue).toBe(fmtDate)

            await page.waitForSelector('[data-test="orders-page-table"]')
            await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"]`)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'reset date filter',
        async () => {
            await filterOrderByDate()
            await setEndDate(new Date())

            await waitForResponse(page, 'orders')
            await page.waitForTimeout(3000)

            await page.waitForSelector('[data-test="orders-page-table"]')
            await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"]`)
        },
        DEFAULT_TIMEOUT
    )
    test(
        'clear filters when order changes status',
        async () => {
            await setStatusFilter(2)

            await waitForResponse(page, 'orders')
            await page.waitForTimeout(3000)

            await page.waitForSelector(`[data-test="order-table-row-${orderNumber}"] a`)
            const orderCountEnd = (await page.$$('[data-test^="order-table-row"]')).length
            expect(orderCountEnd).toBe(1)

            await page.$eval(`[data-test="order-table-row-${orderNumber}"] a`, e => (e as HTMLButtonElement).click())
            await page.waitForSelector(`[data-test="order ${orderNumber}"]`)
            await testRecalculateOrder(browser, page)
            await testTransferToProductionOrder(browser, page, ShippingTypes.Delivery)

            await page.waitForSelector('[data-test="orders-page-table"]')

            const input = await page.waitForSelector('[data-test="orders-number-filter-input"]')
            const inputValue = await page.evaluate(el => el.value, input)
            expect(inputValue).toBe('')

            const statusInput = await page.waitForSelector('[data-test="order-status-select"] > p')
            const statusText = await page.evaluate(el => el.innerText, statusInput)
            expect(statusText).toBe('Все')
        },
        DEFAULT_TIMEOUT
    )
})

afterAll(async () => await afterAllAction(browser))
