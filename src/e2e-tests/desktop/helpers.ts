import {Browser, Page} from 'puppeteer'
import {ORDER_TAB_ID} from '../../constants/navigation'

export const testLoginAction = async (browser: Browser, page: Page): Promise<void> => {
    await page.evaluate(() => {
        localStorage.setItem('CONSTRUCTION_CONFIGURATOR_HINT_SHOWED', 'true')
    })

    await page.waitForSelector('[data-test="login-form"]')

    const html = await page.$eval('title', e => e.innerHTML)
    expect(html).toBe('Framer')

    await page.$eval('[data-test="email-input"]', e => (e as HTMLElement).click())
    await page.type('[data-test="email-input"]', 'v00089812')
    await page.$eval('[data-test="password-input"]', e => (e as HTMLElement).click())
    await page.type('[data-test="password-input"]', '5Wr$Vrpid')
    await page.$eval('[data-test="submit-button"]', e => (e as HTMLElement).click())
    await page.waitForSelector('[data-test="login-form"]', {hidden: true})
}

export const testCreateOrderAction = async (browser: Browser, page: Page): Promise<string> => {
    await page.waitForSelector('[data-test="new-order-button"]')
    await page.$eval('[data-test="new-order-button"]', element => (element as HTMLElement).click())

    await page.waitForSelector('[data-test="group-1"], [data-test="group-1__template-1"]')
    const dataTest = await page.$eval('[data-test="group-1"], [data-test="group-1__template-1"]', e => e.getAttribute('data-test'))
    if (dataTest === 'group-1') {
        await page.$eval('[data-test="group-1"]', element => (element as HTMLElement).click())
    }

    await page.waitForSelector('[data-test="group-1__template-1"]')
    await page.$eval('[data-test="group-1__template-1"]', element => (element as HTMLElement).click())

    await page.waitForSelector('[data-test^="construction"]')

    const attr = await page.$eval(`[data-test^="${ORDER_TAB_ID}"]`, e => e.getAttribute('data-test'))
    const orderNumber = attr?.split(' ')[1] ?? ''

    expect(orderNumber.length).toBeGreaterThan(0)
    return orderNumber
}
