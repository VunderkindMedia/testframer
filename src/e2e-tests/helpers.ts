import puppeteer, {Browser, LaunchOptions, Page, Response} from 'puppeteer'

export const DEFAULT_TIMEOUT = 2 * 60 * 1000

const viewportDesktop = {
    width: 1920,
    height: 1080
}

const viewportMobile = {
    width: 320,
    height: 568
}

export const isDebugging = (): LaunchOptions => {
    const debuggingMode = {
        headless: false,
        slowMo: 10
        // devtools: true
    }
    return process.env.NODE_ENV === 'debug' ? debuggingMode : {slowMo: 10}
}

interface ITestObject {
    browser: Browser
    page: Page
}

export const beforeAllAction = async (): Promise<ITestObject> => {
    const browser = await puppeteer.launch(isDebugging())
    const page = await browser.newPage()
    await page.emulate({
        viewport: process.env.MOBILE ? viewportMobile : viewportDesktop,
        userAgent: ''
    })

    page.setDefaultNavigationTimeout(DEFAULT_TIMEOUT)
    await page.goto('http://localhost:3000/')

    return {browser, page}
}

export const afterAllAction = async (browser: Browser): Promise<void> => {
    if (isDebugging()) {
        await browser.close()
    }
}

export const waitForResponse = (page: Page, url: string): Promise<Response> => {
    return new Promise(resolve => {
        page.on('response', function callback(response) {
            const responseUrl = response.url()
            if (responseUrl === url || responseUrl.includes(url)) {
                resolve(response)
                page.removeListener('response', callback)
            }
        })
    })
}
