importScripts('https://storage.googleapis.com/workbox-cdn/releases/5.0.0/workbox-sw.js')

workbox.core.skipWaiting()
workbox.core.clientsClaim()

const CACHE_TIMES_SEC = 60 * 60

const ONE_DAY_CACHE_ITEMS = [
    {
        name: 'profiles',
        regex: /.*v1\/profiles.*/
    },
    {
        name: 'constrtypes',
        regex: /.*v1\/constrtypes.*/
    },
    {
        name: 'lamination-colors',
        regex: /.*v1\/colors\/lamination.*/
    },
    {
        name: 'goodgroups',
        regex: /.*v1\/goodgroups.*/
    },
    {
        name: 'goods',
        regex: /.*v1\/goods.*/
    }
]

ONE_DAY_CACHE_ITEMS.forEach(cacheItem => {
    workbox.routing.registerRoute(
        cacheItem.regex,
        new workbox.strategies.CacheFirst({
            cacheName: `${cacheItem.name}-cache`,
            plugins: [
                new workbox.cacheableResponse.CacheableResponsePlugin({statuses: [0, 200]}),
                new workbox.expiration.ExpirationPlugin({
                    maxAgeSeconds: CACHE_TIMES_SEC
                })
            ]
        })
    )
})
workbox.routing.registerRoute(
    /.*\/#.*/,
    new workbox.strategies.CacheFirst({
        cacheName: `hash-pages`,
        plugins: [
            new workbox.cacheableResponse.CacheableResponsePlugin({statuses: [0, 200]}),
            new workbox.expiration.ExpirationPlugin({
                maxAgeSeconds: 24 * 60 * 60
            })
        ]
    })
)

workbox.routing.registerRoute(
    '/',
    new workbox.strategies.CacheFirst({
        cacheName: `root`,
        plugins: [
            new workbox.cacheableResponse.CacheableResponsePlugin({statuses: [0, 200]}),
            new workbox.expiration.ExpirationPlugin({
                maxAgeSeconds: 24 * 60 * 60
            })
        ]
    })
)

workbox.precaching.precacheAndRoute(self.__WB_MANIFEST)

self.__WB_DISABLE_DEV_LOGS = true
