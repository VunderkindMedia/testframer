import {Line} from './geometry/line'
import {Point} from './geometry/point'
import {MarkedLine} from './marked-line'

export class DimensionLine extends MarkedLine {
    nodeId = ''
    origin: Line
    bracketWidth = 0
    scale = 1
    sign = 1
    autofocus = true

    constructor(line: MarkedLine) {
        super(line.start, line.end, line.width)

        if (line.isVertical) {
            this.nodeId = `dLine-v-${line.start.y.toFixed(0)}-${line.end.y.toFixed(0)}-${line.length.toFixed(0)}`
        } else {
            this.nodeId = `dLine-h-${line.start.x.toFixed(0)}-${line.end.x.toFixed(0)}-${line.length.toFixed(0)}`
        }

        this.markers = line.markers
        this.origin = line
    }

    get points(): Point[] {
        return [this.start, this.end]
    }

    get bracketPoints1(): Point[] {
        return this._getBracketPoints(this.start)
    }

    get bracketPoints2(): Point[] {
        return this._getBracketPoints(this.end)
    }

    get bracketLine1(): Line {
        return this._getBracketLine(this.start)
    }

    get bracketLine2(): Line {
        return this._getBracketLine(this.end)
    }

    equals(dimensionLine: DimensionLine): boolean {
        if (this.isHorizontal && dimensionLine.isHorizontal) {
            return this.leftBottomPoint.x === dimensionLine.leftBottomPoint.x && this.rightTopPoint.x === dimensionLine.rightTopPoint.x
        } else if (this.isVertical && dimensionLine.isVertical) {
            return this.leftBottomPoint.y === dimensionLine.leftBottomPoint.y && this.rightTopPoint.y === dimensionLine.rightTopPoint.y
        }

        return false
    }

    private _getBracketPoints(refPoint: Point): Point[] {
        const {bracketWidth, scale} = this

        return [
            refPoint.withOffset(this.isHorizontal ? 0 : bracketWidth / 2 / scale, this.isHorizontal ? bracketWidth / 2 / scale : 0),
            refPoint.withOffset(this.isHorizontal ? 0 : -bracketWidth / 2 / scale, this.isHorizontal ? -bracketWidth / 2 / scale : 0)
        ]
    }

    private _getBracketLine(refPoint: Point): Line {
        const {bracketWidth, scale} = this

        return new Line(
            refPoint.withOffset(this.isHorizontal ? 0 : bracketWidth / 2 / scale, this.isHorizontal ? bracketWidth / 2 / scale : 0),
            refPoint.withOffset(this.isHorizontal ? 0 : -bracketWidth / 2 / scale, this.isHorizontal ? -bracketWidth / 2 / scale : 0)
        )
    }
}
