export class CircularLinkedListItem<T> {
    readonly data: T
    readonly prev: T
    readonly next: T

    constructor(item: T, prev: T, next: T) {
        this.data = item
        this.prev = prev
        this.next = next
    }
}

export class CircularLinkedList<T> {
    readonly items: Array<CircularLinkedListItem<T>>

    constructor(items: T[]) {
        if (items.length < 3) {
            throw new Error(`Required 3 items, but got ${items.length}`)
        }
        this.items = items.slice(0).map((item, index) => {
            const prev = index === 0 ? items[items.length - 1] : items[index - 1]
            const next = index === items.length - 1 ? items[0] : items[index + 1]
            return new CircularLinkedListItem<T>(item, prev, next)
        })
    }
}
