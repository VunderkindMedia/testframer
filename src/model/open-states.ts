export enum OpenStates {
    Closed = 'closed',
    Opened = 'opened'
}
