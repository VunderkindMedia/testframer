import {UserParameter} from './framer/user-parameter/user-parameter'

export interface IContext {
    nodeId: string
    parentNodeId?: string | null
    userParameters?: UserParameter[]
}
