import {Size} from './size'
import {IConstructionJSON} from './framer/construction'
import {FactoryTypes} from './framer/factory-types'
import {BuildTypes} from './build-types'

export class Template {
    id = ''
    title = ''
    groupTitle = ''
    img = ''
    groupImg = ''
    size = new Size(0, 0)
    paddingBottom = 0
    textMarginTop = 0
    data?: IConstructionJSON
    templates: Template[] = []
    factoryTypeFilter?: FactoryTypes
    buildTypeFilter?: BuildTypes
    isHidden = false
    private _depth = 1

    static get Builder(): Builder {
        return new Builder()
    }

    get allTemplates(): Template[] {
        const templates: Template[] = [...this.templates]

        this.templates.forEach(template => {
            templates.push(...template.allTemplates)
        })

        return templates
    }

    get depth(): number {
        return this._depth
    }

    set depth(depth: number) {
        this._depth = depth

        this.templates.forEach(template => {
            template.depth = this.depth + 1
        })
    }

    contains(templates: Template[]): boolean {
        const all = this.allTemplates

        let isContains = true
        templates.forEach(template => {
            if (!all.includes(template)) {
                isContains = false
            }
        })

        return isContains
    }
}

class Builder {
    private _template = new Template()

    withId(id: string): Builder {
        this._template.id = id
        return this
    }

    withTitle(title: string): Builder {
        this._template.title = title
        return this
    }

    withGroupTitle(groupTitle: string): Builder {
        this._template.groupTitle = groupTitle
        return this
    }

    withImg(img: string): Builder {
        this._template.img = img
        return this
    }

    withGroupImg(groupImg: string): Builder {
        this._template.groupImg = groupImg
        return this
    }

    withSize(size: Size): Builder {
        this._template.size = size
        return this
    }

    withPaddingBottom(paddingBottom: number): Builder {
        this._template.paddingBottom = paddingBottom
        return this
    }

    withTextMarginTop(textMarginTop: number): Builder {
        this._template.textMarginTop = textMarginTop
        return this
    }

    withData(data: IConstructionJSON): Builder {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        this._template.data = data
        return this
    }

    withDataModify(modifyAction: (data: IConstructionJSON) => IConstructionJSON): Builder {
        if (this._template.data) {
            this._template.data = modifyAction(this._template.data)
        }
        return this
    }

    withTemplates(templates: Template[]): Builder {
        this._template.templates = templates
        return this
    }

    withFactoryTypeFilter(factoryTypeFilter: FactoryTypes): Builder {
        this._template.factoryTypeFilter = factoryTypeFilter
        return this
    }

    withBuildTypeFilter(buildTypeFilter: BuildTypes): Builder {
        this._template.buildTypeFilter = buildTypeFilter
        return this
    }

    build(): Template {
        // eslint-disable-next-line no-self-assign
        this._template.depth = this._template.depth
        return this._template
    }
}
