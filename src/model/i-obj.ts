export interface IObj {
    [key: string]: string | number | null | IObj
}
