import {OpenSides} from './framer/open-sides'
import {OpenTypes} from './framer/open-types'
import {ShtulpOpenTypes} from './framer/shtulp-open-types'

export class OpeningSystem {
    img: string
    mImg: string
    name: string
    openType: OpenTypes
    openSide: OpenSides
    shtulpOpenType: ShtulpOpenTypes

    constructor(img: string, mImg: string, name: string, openType: OpenTypes, openSide: OpenSides, shtulpOpenType = ShtulpOpenTypes.NoShtulp) {
        this.img = img
        this.mImg = mImg
        this.name = name
        this.openType = openType
        this.openSide = openSide
        this.shtulpOpenType = shtulpOpenType
    }

    clone(): OpeningSystem {
        return new OpeningSystem(this.img, this.mImg, this.name, this.openType, this.openSide, this.shtulpOpenType)
    }

    get code(): string {
        return `${this.openType}_${this.openSide}_${this.shtulpOpenType}`
    }

    equals(openingSystem: OpeningSystem): boolean {
        return this.code === openingSystem.code
    }
}
