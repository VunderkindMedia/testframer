/* eslint-disable @typescript-eslint/naming-convention */
import {UserPermissions} from './framer/user-permissions'
import {getPermissions} from '../helpers/account'

export interface IJWT {
    exp: number
    iat: number
}

export interface ITokenJSON {
    access_token: string
    refresh_token: string
    error: string
}

export class IToken {
    static parse(tokenJSON: ITokenJSON): IToken {
        const {access_token, refresh_token, error} = tokenJSON
        return new IToken(access_token, refresh_token, error)
    }

    constructor(accessToken: string, refreshToken: string, error: string) {
        this.accessToken = accessToken
        this.refreshToken = refreshToken
        this.error = error
    }

    get permissions(): UserPermissions | null {
        return getPermissions(this.accessToken)
    }

    accessToken: string
    refreshToken: string
    error: string
}
