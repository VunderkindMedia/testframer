export enum Sides {
    Left = 'left',
    Right = 'right',
    Top = 'top',
    Bottom = 'bottom'
}
