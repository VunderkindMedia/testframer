export enum ImpostTypes {
    Horizontal = 'Горизонтальный',
    Vertical = 'Вертикальный',
    Nope = 'Нет'
}
