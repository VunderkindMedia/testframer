export enum BuildTypes {
    Prod = 'prod',
    Dev = 'dev',
    Demo = 'demo'
}
