import {IContext} from './i-context'
import {HandlePositionTypes} from './framer/handle-position-types'
import {FrameFilling} from './framer/frame-filling'
import {OpenSides} from './framer/open-sides'
import {Polygon} from './geometry/polygon'
import {Point} from './geometry/point'
import {HandleTypes} from './framer/handle-types'
import {HandleColors} from './framer/handle-colors'
import {ModelParts} from './framer/model-parts'
import {Beam} from './framer/beam'
import {degreesToRadians} from '../helpers/number'

export class Handle implements IContext {
    nodeId: string
    parentNodeId: string | null
    handlePositionType: HandlePositionTypes
    type: HandleTypes
    color: HandleColors
    refPoint: Point
    angle: number
    private readonly _hostBeam: Beam | null = null

    constructor(frameFilling: FrameFilling) {
        this.nodeId = `handle-${frameFilling.nodeId}`
        this.parentNodeId = frameFilling.nodeId

        this.handlePositionType = frameFilling.handlePositionType
        this.type = frameFilling.handleType
        this.color = frameFilling.handleColor

        switch (frameFilling.openSide) {
            case OpenSides.ToBottom: {
                this._hostBeam = frameFilling.topmostBeam
                break
            }
            case OpenSides.ToLeft: {
                this._hostBeam = frameFilling.rightmostBeam
                frameFilling.isAluminium
                break
            }
            case OpenSides.ToRight: {
                this._hostBeam = frameFilling.leftmostBeam
                break
            }
        }

        if (!this._hostBeam) {
            throw new Error('HostBeam not defined')
        }

        const beamPolygon = Polygon.buildFromPoints(this._hostBeam.outerDrawingPoints)
        const x = beamPolygon.center.x
        const y = beamPolygon.center.y

        this.angle = (this._hostBeam.angle === 0 ? 180 : this._hostBeam.angle) - 90

        let offsetX = 0
        let offsetY = 0

        if (frameFilling.handlePositionType === HandlePositionTypes.Fixed) {
            const lengthWithoutFold = this._hostBeam.length - frameFilling.profile.getGeometryParameters(ModelParts.Stvorka).e * 2
            const handleParams = frameFilling.furniture.handleParams.reverse()

            let pair = handleParams.find(p => lengthWithoutFold >= p[1])
            if (!pair) {
                pair =
                    handleParams
                        .slice(0)
                        .reverse()
                        .find(p => p[1] > lengthWithoutFold) ?? handleParams[0]
            }

            const handleOffset = pair[0]
            const offset = handleOffset - this._hostBeam.length / 2

            offsetX = offset * Math.sin(degreesToRadians(this.angle))
            offsetY = offset * Math.cos(degreesToRadians(this.angle))
        }

        this.refPoint = new Point(x - offsetX, y + offsetY)
    }

    get hostBeam(): Beam | null {
        return this._hostBeam
    }
}
