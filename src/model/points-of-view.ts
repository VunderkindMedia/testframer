export enum PointsOfView {
    Outside = 'outside',
    Inside = 'inside'
}
