import {Line} from './geometry/line'

export class LinesChain<T extends Line> {
    lines: T[]

    constructor(lines: T[] = []) {
        this.lines = lines
    }

    includes(linesChain: LinesChain<T>): boolean {
        return linesChain.lines.every(line => Boolean(this.lines.find(l => l.equals(line))))
    }

    equals(linesChain: LinesChain<T>): boolean {
        return this.lines.length === linesChain.lines.length && linesChain.lines.every(line => Boolean(this.lines.find(l => l.equals(line))))
    }

    removeLines(lines: T[]): void {
        const newLines: T[] = []

        this.lines.forEach(line => {
            if (!lines.find(l => l.equals(line))) {
                newLines.push(line)
            }
        })

        this.lines = newLines
    }

    get length(): number {
        return this.lines.reduce((a, value) => a + value.length, 0)
    }
}
