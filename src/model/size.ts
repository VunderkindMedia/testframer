export class Size {
    width: number
    height: number

    constructor(width: number, height: number) {
        this.width = width
        this.height = height
    }

    equals(size: Size, epsilon = 1): boolean {
        return Math.abs(this.width - size.width) <= epsilon && Math.abs(this.height - size.height) <= epsilon
    }

    scaled(value: number): Size {
        return new Size(this.width * value, this.height * value)
    }
}
