import {FrameFilling} from './framer/frame-filling'
import {Impost} from './framer/impost'
import {SolidFilling} from './framer/solid-filling'

export class ShtulpGroup {
    readonly leftFilling: FrameFilling | SolidFilling
    readonly rightFilling: FrameFilling | SolidFilling
    readonly impost: Impost

    constructor(leftFilling: FrameFilling | SolidFilling, rightFilling: FrameFilling | SolidFilling, impost: Impost) {
        this.leftFilling = leftFilling
        this.rightFilling = rightFilling
        this.impost = impost
    }

    hasNode(context: FrameFilling | SolidFilling | Impost): boolean {
        return (
            this.leftFilling.nodeId === context.nodeId || this.rightFilling.nodeId === context.nodeId || this.impost.nodeId === context.nodeId
        )
    }

    equals(group: ShtulpGroup): boolean {
        return (
            this.leftFilling.nodeId === group.leftFilling.nodeId &&
            this.rightFilling.nodeId === group.rightFilling.nodeId &&
            this.impost.nodeId === group.impost.nodeId
        )
    }
}
