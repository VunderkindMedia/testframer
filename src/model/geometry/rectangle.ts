import {Point} from './point'
import {Size} from '../size'
import {getIdFromPoints} from '../../helpers/point'

export class Rectangle {
    static buildFromPoints(points: Point[]): Rectangle {
        const xArray = points.map(point => point.x)
        const yArray = points.map(point => point.y)

        const minX = Math.min(...xArray)
        const maxX = Math.max(...xArray)

        const minY = Math.min(...yArray)
        const maxY = Math.max(...yArray)

        return new Rectangle(new Point(minX, minY), new Size(maxX - minX, maxY - minY))
    }
    refPoint: Point
    size: Size

    private readonly _id: string

    constructor(refPoint: Point = Point.zero, size: Size = new Size(0, 0)) {
        this.refPoint = refPoint
        this.size = size

        this._id = getIdFromPoints('rectangle', this.drawingPoints)
    }

    get id(): string {
        return this._id
    }

    get center(): Point {
        return new Point(this.refPoint.x + this.size.width / 2, this.refPoint.y + this.size.height / 2)
    }

    get rightTopPoint(): Point {
        return this.refPoint.withOffset(this.size.width, this.size.height)
    }

    get leftBottomPoint(): Point {
        return this.refPoint
    }

    get rightBottomPoint(): Point {
        return this.refPoint.withOffset(this.size.width, 0)
    }

    get leftTopPoint(): Point {
        return this.refPoint.withOffset(0, this.size.height)
    }

    get drawingPoints(): Point[] {
        const {refPoint, size} = this

        return [
            new Point(refPoint.x, refPoint.y),
            new Point(refPoint.x + size.width, refPoint.y),
            new Point(refPoint.x + size.width, refPoint.y + size.height),
            new Point(refPoint.x, refPoint.y + size.height)
        ]
    }

    hasPointInside(point: Point): boolean {
        return (
            this.refPoint.x <= point.x &&
            point.x <= this.refPoint.x + this.size.width &&
            this.refPoint.y <= point.y &&
            point.y <= this.refPoint.y + this.size.height
        )
    }
}
