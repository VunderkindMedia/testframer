import {Line} from './line'
import {Point} from './point'
import {approximatelyEqual} from '../../helpers/number'

export class Segment extends Line {
    hasPoint(point: Point): boolean {
        try {
            const {start, end} = this

            const hasPoint = (start: number, end: number, coord: number): boolean => {
                return (
                    ((approximatelyEqual(start, coord) || start <= coord) && (approximatelyEqual(coord, end) || coord <= end)) ||
                    ((approximatelyEqual(end, coord) || end <= coord) && (approximatelyEqual(coord, start) || coord <= start))
                )
            }

            if (this.isVertical && approximatelyEqual(point.x, start.x)) {
                return hasPoint(start.y, end.y, point.y)
            }

            if (hasPoint(start.x, end.x, point.x)) {
                return approximatelyEqual((point.x - start.x) * (end.y - start.y) - (point.y - start.y) * (end.x - start.x), 0)
            }

            return false
        } catch (e) {
            return false
        }
    }

    hasCommonStartOrEnd(segment: Segment): boolean {
        return this.hasPoint(segment.start) || this.hasPoint(segment.end) || segment.hasPoint(this.start) || segment.hasPoint(this.end)
    }

    findCommonPoint(segment: Segment): Point | null {
        const intersectionPoint = this.findIntersectionPoint(segment)

        if (intersectionPoint && this.hasPoint(intersectionPoint) && segment.hasPoint(intersectionPoint)) {
            return intersectionPoint
        }

        return null
    }

    findCommonSegment(segment: Segment): Segment | null {
        if (this.hasPoint(segment.start) && this.hasPoint(segment.end)) {
            return new Segment(segment.start, segment.end)
        }

        if (segment.hasPoint(this.start) && segment.hasPoint(this.end)) {
            return new Segment(this.start, this.end)
        }

        if (this.hasPoint(segment.start)) {
            const point1 = segment.start
            const point2 = segment.hasPoint(this.start) ? this.start : this.end
            return new Segment(point1, point2)
        }

        if (this.hasPoint(segment.end)) {
            const point1 = segment.hasPoint(this.start) ? this.start : this.end
            const point2 = segment.end
            return new Segment(point1, point2)
        }

        return null
    }

    withOffset(offsetX: number, offsetY: number): Segment {
        const line = super.withOffset(offsetX, offsetY)
        const segment = new Segment(line.start, line.end)
        segment.width = this.width
        return segment
    }
}
