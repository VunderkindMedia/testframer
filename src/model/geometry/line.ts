import {Point} from './point'
import {Rectangle} from './rectangle'
import {getIdFromPoints} from '../../helpers/point'
import {radiansToDegrees} from '../../helpers/number'

export class Line {
    static get abscissa(): Line {
        return new Line(Point.zero, new Point(1, 0))
    }

    static ordinate(): Line {
        return new Line(Point.zero, new Point(0, 1))
    }

    start: Point
    end: Point
    private _width = 0
    private _id: string | null = null

    constructor(start: Point, end: Point, width = 0) {
        this.start = start
        this.end = end
        this.width = width
    }

    get angle(): number {
        if (this.isHorizontal) {
            return 0
        }

        const bottomostPoint = this.start.y < this.end.y ? this.start : this.end

        return new Line(bottomostPoint, bottomostPoint.withOffset(1, 0)).findIntersectionAngle(this)
    }

    get isVertical(): boolean {
        return this.start.x === this.end.x
    }

    get isHorizontal(): boolean {
        return this.start.y === this.end.y
    }

    get rect(): Rectangle {
        return Rectangle.buildFromPoints([this.start, this.end])
    }

    get length(): number {
        return this.start.distanceTo(this.end)
    }

    get rightTopPoint(): Point {
        return this.rect.rightTopPoint
    }

    get leftBottomPoint(): Point {
        return this.rect.leftBottomPoint
    }

    get perpendicular(): Line {
        const {start, end} = this

        const A = start.y - end.y
        const B = end.x - start.x

        const endPoint = new Point(A + start.x, B + start.y)

        return new Line(start, endPoint)
    }

    get OX(): Line {
        const {refPoint, size} = this.rect
        return new Line(new Point(refPoint.x, 0), new Point(refPoint.x + size.width, 0))
    }

    get OY(): Line {
        const {refPoint, size} = this.rect
        return new Line(new Point(0, refPoint.y), new Point(0, refPoint.y + size.height))
    }

    get width(): number {
        return this._width
    }

    set width(width: number) {
        this._width = width >= 0 ? width : 0
    }

    set id(id: string) {
        this._id = id
    }

    get id(): string {
        if (!this._id) {
            this._id = getIdFromPoints('line', [this.start, this.end])
        }

        return this._id
    }

    findIntersectionPoint(line: Line): Point | null {
        try {
            const x1 = this.start.x
            const y1 = this.start.y
            const x2 = this.end.x
            const y2 = this.end.y

            const x3 = line.start.x
            const y3 = line.start.y
            const x4 = line.end.x
            const y4 = line.end.y

            const a = x1 * y2 - y1 * x2
            const b = x3 * y4 - y3 * x4

            const x = (a * (x3 - x4) - (x1 - x2) * b) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
            const y = (a * (y3 - y4) - (y1 - y2) * b) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))

            if (isNaN(x) || !isFinite(x) || isNaN(y) || !isFinite(y)) {
                return null
            }

            return new Point(x, y)
        } catch (e) {
            // console.error(e)
            return null
        }
    }

    findIntersectionPoints(lines: Line[]): Point[] {
        const points: Point[] = []

        lines.forEach(line => {
            const point = line.findIntersectionPoint(this)
            point && points.push(point)
        })

        return points
    }

    findIntersectionAngle(line: Line): number {
        const {start, end} = this

        let line1Start = start
        let line1End = end
        let line2Start = line.start
        let line2End = line.end

        const intersectionPoint = this.findIntersectionPoint(line)

        if (!intersectionPoint) {
            return 0
        }

        if (intersectionPoint.equals(end)) {
            line1Start = end
            line1End = start
        }
        if (intersectionPoint.equals(line.end)) {
            line2Start = line.end
            line2End = line.start
        }

        const x1 = line1End.x - line1Start.x
        const y1 = line1End.y - line1Start.y

        const x2 = line2End.x - line2Start.x
        const y2 = line2End.y - line2Start.y

        const cosFi = (x1 * x2 + y1 * y2) / (Math.sqrt(x1 * x1 + y1 * y1) * Math.sqrt(x2 * x2 + y2 * y2))

        return radiansToDegrees(Math.acos(cosFi))
    }

    getParallels(d: number): Line[] {
        // a*x' + b*y' + c +- d*sqrt(a*a + b*b) = 0

        const intersectionAngle = this.findIntersectionAngle(new Line(Point.zero, new Point(100, 0)))

        const {start, end} = this

        const A = start.y - end.y
        const B = end.x - start.x
        const C = start.x * end.y - end.x * start.y

        if (intersectionAngle <= 45) {
            const yStart1 = (-d * Math.sqrt(A * A + B * B) - A * start.x - C) / B
            const yEnd1 = (-d * Math.sqrt(A * A + B * B) - A * end.x - C) / B

            const yStart2 = (d * Math.sqrt(A * A + B * B) - A * start.x - C) / B
            const yEnd2 = (d * Math.sqrt(A * A + B * B) - A * end.x - C) / B

            // в первую очередь отдаем внутренние паралели
            return [
                new Line(new Point(start.x, yStart1), new Point(end.x, yEnd1)),
                new Line(new Point(start.x, yStart2), new Point(end.x, yEnd2))
            ]
        } else {
            const xStart1 = (-d * Math.sqrt(A * A + B * B) - B * start.y - C) / A
            const xEnd1 = (-d * Math.sqrt(A * A + B * B) - B * end.y - C) / A

            const xStart2 = (d * Math.sqrt(A * A + B * B) - B * start.y - C) / A
            const xEnd2 = (d * Math.sqrt(A * A + B * B) - B * end.y - C) / A

            // в первую очередь отдаем внутренние паралели
            return [
                new Line(new Point(xStart1, start.y), new Point(xEnd1, end.y)),
                new Line(new Point(xStart2, start.y), new Point(xEnd2, end.y))
            ]
        }
    }

    withOffset(offsetX: number, offsetY: number): Line {
        return new Line(new Point(this.start.x + offsetX, this.start.y + offsetY), new Point(this.end.x + offsetX, this.end.y + offsetY))
    }

    equals(line: Line): boolean {
        const {start, end} = this
        return (start.equals(line.start) && end.equals(line.end)) || (start.equals(line.end) && end.equals(line.start))
    }

    normalizePointsOrder(): void {
        if (this.isVertical) {
            if (this.start.y > this.end.y) {
                // eslint-disable-next-line @typescript-eslint/no-extra-semi
                ;[this.start, this.end] = [this.end, this.start]
            }
        } else if (this.isHorizontal) {
            if (this.start.x > this.end.x) {
                // eslint-disable-next-line @typescript-eslint/no-extra-semi
                ;[this.start, this.end] = [this.end, this.start]
            }
        }
    }
}
