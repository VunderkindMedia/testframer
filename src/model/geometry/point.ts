export interface IPointJSON {
    x: number
    y: number
}

export class Point {
    static get zero(): Point {
        return new Point(0, 0)
    }

    static parse(pointJSON: IPointJSON): Point {
        return new Point(pointJSON.x, pointJSON.y)
    }

    x: number
    y: number

    constructor(x: number, y: number) {
        this.x = +x.toFixed(4)
        this.y = +y.toFixed(4)
    }

    equals(point: Point): boolean {
        return this.x === point.x && this.y === point.y
    }

    distanceTo(point: Point): number {
        return Math.sqrt(Math.pow(this.x - point.x, 2) + Math.pow(this.y - point.y, 2))
    }

    get clone(): Point {
        return new Point(this.x, this.y)
    }

    withOffset(offsetX: number, offsetY: number): Point {
        return new Point(this.x + offsetX, this.y + offsetY)
    }

    findNearestPoint(points: Point[]): Point {
        if (points.length === 0) {
            throw new Error('Нужно передать хотяб одну точку')
        }

        let foundedPoint: Point = points[0]
        let r = Infinity

        points.forEach(p => {
            const d = this.distanceTo(p)
            if (d < r) {
                r = d
                foundedPoint = p
            }
        })

        return foundedPoint
    }
}
