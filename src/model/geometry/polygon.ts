import {Point} from './point'
import {Line} from './line'
import {CircularLinkedList} from '../circular-linked-list'
import {Rectangle} from './rectangle'
import {Segment} from './segment'
import {APPROXIMATE_BEAM_WIDTH, BEAD_WIDTH} from '../../constants/geometry-params'
import {getCenter, getIdFromPoints, getUniquePoints} from '../../helpers/point'
import {getBottommostLine, getLeftmostLine, getRightmostLine, getTopmostLine} from '../../helpers/line'
import {det} from '../../helpers/number'
import {safeWhile} from '../../helpers/safe-while'

export class Polygon {
    static buildFromPoints(points: Point[]): Polygon {
        const pointsCircularLinkedList = new CircularLinkedList(points)
        const segments = pointsCircularLinkedList.items.map(item => new Segment(item.data, item.next))

        return new Polygon(segments)
    }

    private _id: string | null = null
    private _segments: Segment[] = []
    childrenPolygons: Polygon[] = []

    constructor(segments: Segment[] = []) {
        if (segments.length === 0) {
            return
        }

        const bottomSegment = getBottommostLine(segments)
        const orderedSegments: Segment[] = [bottomSegment]

        if (bottomSegment) {
            safeWhile(
                () => orderedSegments.length !== segments.length,
                () => {
                    const lastSegment = orderedSegments[orderedSegments.length - 1]
                    const nextSegment = segments.find(segment => segment.start.equals(lastSegment.end))
                    nextSegment && orderedSegments.push(nextSegment)
                }
            )
        }

        this.segments = orderedSegments.filter(s => Boolean(s)).map(s => new Segment(s.start, s.end, s.width))
    }

    get clone(): Polygon {
        const polygon = new Polygon(this.segments.map(s => new Segment(s.start, s.end, s.width)))

        polygon.childrenPolygons = this.childrenPolygons.map(p => p.clone)

        return polygon
    }

    get id(): string {
        if (!this._id) {
            this._id = getIdFromPoints('polygon', this.points)
        }

        return this._id
    }

    get segments(): Segment[] {
        return this._segments
    }

    set segments(segments: Segment[]) {
        this._segments = segments
    }

    get points(): Point[] {
        return this.segments.map(segment => segment.start)
    }

    get center(): Point {
        return getCenter(this.points)
    }

    get rect(): Rectangle {
        return Rectangle.buildFromPoints(this.points)
    }

    get innerSegments(): Segment[] {
        const segments: Segment[] = []

        // TODO: пока не уверен как именно фильтровать короткие участки
        new CircularLinkedList(this.segments.filter(segments => segments.length > APPROXIMATE_BEAM_WIDTH)).items.forEach(item => {
            const currentSegment = item.data
            const prevSegment = item.prev
            const nextSegment = item.next

            const currentBeamParallel = currentSegment.getParallels(currentSegment.width)[0]
            const prevBeamParallel = prevSegment.getParallels(prevSegment.width)[0]
            const nextBeamParallel = nextSegment.getParallels(nextSegment.width)[0]

            const point1 = currentBeamParallel.findIntersectionPoint(prevBeamParallel)
            const point2 = currentBeamParallel.findIntersectionPoint(nextBeamParallel)

            if (!point1 || !point2) {
                return
            }
            segments.push(new Segment(point1, point2))
        })

        const needRemoveLinesIndexes: number[] = []

        new CircularLinkedList(segments).items.forEach((item, index) => {
            const segment = item.data

            if (segment.length < BEAD_WIDTH) {
                needRemoveLinesIndexes.push(index)

                const prevSegment = item.prev
                const nextSegment = item.next

                const intersectionPoint = prevSegment.findIntersectionPoint(nextSegment)
                if (intersectionPoint) {
                    prevSegment.end = intersectionPoint
                    nextSegment.start = intersectionPoint
                }
            }
        })

        return segments.filter((segment, index) => !needRemoveLinesIndexes.includes(index))
    }

    get innerPolygon(): Polygon {
        return new Polygon(this.innerSegments)
    }

    get allChildrenPolygons(): Polygon[] {
        const polygons: Polygon[] = []

        this.childrenPolygons.forEach(polygon => {
            const allChildrenPolygons = polygon.allChildrenPolygons
            if (allChildrenPolygons.length === 0) {
                polygons.push(polygon)
            } else {
                polygons.push(...polygon.allChildrenPolygons)
                polygons.push(polygon)
            }
        })

        return polygons
    }

    get isRect(): boolean {
        return (
            this.segments.length === 4 &&
            this.segments[0].findIntersectionAngle(this.segments[1]) === 90 &&
            this.segments[2].findIntersectionAngle(this.segments[3]) === 90
        )
    }

    get square(): number {
        let detSum = 0

        new CircularLinkedList(this.points).items.forEach(item => {
            const point = item.data
            const nextPoint = item.next

            detSum += det(point.x, point.y, nextPoint.x, nextPoint.y)
        })

        return Math.abs(detSum) * 0.5
    }

    equals(polygon: Polygon): boolean {
        if (this.segments.length !== polygon.segments.length) {
            return false
        }

        return this.segments.every(segment => polygon.segments.some(s => s.equals(segment)))
    }

    getSubPolygons(dividingSegment: Segment): Polygon[] {
        const commonPoints: Point[] = []
        this.segments.forEach(segment => {
            const point = segment.findCommonPoint(dividingSegment)
            point && commonPoints.push(point)
        })

        const intersectionPoints = getUniquePoints(commonPoints)

        const firstIntersectionPoint = intersectionPoints[0]
        const secondIntersectionPoint = intersectionPoints[1]

        const firstPointIntersectionSegmentsIndexes = this.segments
            .filter(segment => segment.hasPoint(firstIntersectionPoint))
            .map(segment => this.segments.indexOf(segment))
        const secondPointIntersectionSegmentsIndexes = this.segments
            .filter(segment => segment.hasPoint(secondIntersectionPoint))
            .map(segment => this.segments.indexOf(segment))

        const firstIntersectionSegmentIndex = firstPointIntersectionSegmentsIndexes[0]
        const secondIntersectionSegmentIndex = secondPointIntersectionSegmentsIndexes[0]

        if (intersectionPoints.length < 2) {
            return []
        }

        const polygonSegments1: Segment[] = [
            new Segment(
                firstIntersectionPoint,
                this.segments[firstIntersectionSegmentIndex].end,
                this.segments[firstIntersectionSegmentIndex].width
            )
        ]
            .concat(this.segments.slice(firstIntersectionSegmentIndex + 1, secondIntersectionSegmentIndex))
            .concat(
                new Segment(
                    this.segments[secondIntersectionSegmentIndex].start,
                    secondIntersectionPoint,
                    this.segments[secondIntersectionSegmentIndex].width
                )
            )
            .concat(new Segment(secondIntersectionPoint, firstIntersectionPoint, dividingSegment.width / 2))
            .filter(line => !line.start.equals(line.end))

        const polygonSegments2: Segment[] = this.segments
            .slice(0, firstIntersectionSegmentIndex)
            .concat(
                new Segment(
                    this.segments[firstIntersectionSegmentIndex].start,
                    firstIntersectionPoint,
                    this.segments[firstIntersectionSegmentIndex].width
                )
            )
            .concat(new Segment(firstIntersectionPoint, secondIntersectionPoint, dividingSegment.width / 2))
            .concat(
                new Segment(
                    secondIntersectionPoint,
                    this.segments[secondIntersectionSegmentIndex].end,
                    this.segments[secondIntersectionSegmentIndex].width
                )
            )
            .concat(this.segments.slice(secondIntersectionSegmentIndex + 1))
            .filter(line => !line.start.equals(line.end))

        const subPolygons: Polygon[] = []
        if (polygonSegments1.length !== 0) {
            subPolygons.push(new Polygon(polygonSegments1))
        }
        if (polygonSegments2.length !== 0) {
            subPolygons.push(new Polygon(polygonSegments2))
        }

        // TODO: пока это вроде актуальная сортировка
        // (перевее должен идти полигон который выше или левее разделяющей линии)
        subPolygons.sort((p1, p2) => {
            if (getRightmostLine(p1.segments).equals(dividingSegment) || getBottommostLine(p1.segments).equals(dividingSegment)) {
                return -1
            } else if (getLeftmostLine(p2.segments).equals(dividingSegment) || getTopmostLine(p2.segments).equals(dividingSegment)) {
                return 1
            }

            return 0
        })

        return subPolygons
    }

    // prettier-ignore
    getSubPolygonsAndOrderedSegments<T extends Segment>(segments: T[]): {segments: T[], polygons: Polygon[]} {
        if (segments.length === 0) {
            return {
                segments: [],
                polygons: []
            }
        }

        let currentPolygon = this as Polygon

        const orderedSegments: T[] = []
        const orderedSubPolygons: Polygon[] = []

        const currentPolygonIntersectSegments = segments.filter(segment => this.hasSegmentInside(segment))

        safeWhile(
            () => orderedSegments.length !== currentPolygonIntersectSegments.length,
            () => {
                const freeSegmentsWithSubPolygons = currentPolygonIntersectSegments
                    .filter(s => Boolean(!orderedSegments.find(ss => ss.equals(s))))
                    .map(segment => ({
                        segment,
                        subPolygons: currentPolygon.getSubPolygons(segment)
                    }))
                    .filter(s => s.subPolygons.length === 2)

                const segment = freeSegmentsWithSubPolygons.find(freeSegmentsWithSubPolygon => {
                    const otherSegmentsWithSubPolygon = freeSegmentsWithSubPolygons.filter(
                        s => !s.segment.equals(freeSegmentsWithSubPolygon.segment)
                    )

                    const divider = otherSegmentsWithSubPolygon.find(
                        s => freeSegmentsWithSubPolygon.subPolygons[0].getSubPolygons(s.segment).length === 2
                    )

                    return !divider
                })

                if (!segment) {
                    return true
                }

                orderedSegments.push(segment.segment)
                orderedSubPolygons.push(segment.subPolygons[0])
                currentPolygon = segment.subPolygons[1]

                return false
            }
        )

        currentPolygon && orderedSubPolygons.push(currentPolygon)

        return {
            segments: orderedSegments,
            polygons: orderedSubPolygons
        }
    }

    hasSegmentInside(segment: Segment): boolean {
        const points: Point[] = this.segments
            .filter(polygonSegment => segment.hasCommonStartOrEnd(polygonSegment))
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            .map(polygonSegment => polygonSegment.findIntersectionPoint(segment)!)

        return getUniquePoints(points).length === 2
    }

    hasPointOnSide(point: Point): boolean {
        return !!this.segments.find(segment => segment.hasPoint(point))
    }

    segmentIsSide(segment: Segment): boolean {
        return this.segments.some(s => s.equals(segment))
    }

    getIntersectionPoints(dividingLine: Line): Point[] {
        const points: Point[] = []
        this.segments.forEach(segment => {
            const intersectionPoint = dividingLine.findIntersectionPoint(segment)
            if (intersectionPoint && segment.hasPoint(intersectionPoint)) {
                points.push(intersectionPoint)
            }
        })

        return points
    }

    hasIntersectionWith(polygon: Polygon): boolean {
        return this.segments.some(segment => polygon.segments.find(s => s.findCommonPoint(segment)))
    }

    withOffset(xOffset: number, yOffset: number): Polygon {
        return new Polygon(this.segments.map(s => s.withOffset(xOffset, yOffset)))
    }
}
