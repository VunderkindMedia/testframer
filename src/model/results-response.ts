export interface IResultsResponse<T> {
    results?: T[]
    offset: number
    limit: number
    // eslint-disable-next-line @typescript-eslint/naming-convention
    total_count: number
}
