import {Product} from './framer/product'
import {Rectangle} from './geometry/rectangle'

export class HostProductInfo {
    product: Product
    visualRect: Rectangle

    constructor(product: Product, visualRect: Rectangle) {
        this.product = product
        this.visualRect = visualRect
    }
}
