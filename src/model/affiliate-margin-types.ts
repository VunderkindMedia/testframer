export enum AffiliateMarginTypes {
    Construction = 'margin',
    Goods = 'goodsmargin',
    Lamination = 'laminationmargin',
    Glass = 'glassmargin',
    Tint = 'tintmargin',
    EntranceDoor = 'entrancedoormargin',
    NonStandard = 'nonstandardmargin',
    ConstructionDiscount = 'subdealerconstrdiscount',
    GoodsDiscount = 'subdealergoodsdiscount'
}
