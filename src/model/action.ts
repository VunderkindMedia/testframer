export class Action {
    readonly name: string
    readonly func: (() => void) | (() => Promise<void>)
    readonly dataTest?: string

    constructor(name: string, func: (() => void) | (() => Promise<void>), dataTest?: string) {
        this.name = name
        this.func = func
        this.dataTest = dataTest
    }
}
