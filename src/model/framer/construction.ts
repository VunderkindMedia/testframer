/* eslint-disable @typescript-eslint/naming-convention */
import {IProductJSON, Product} from './product'
import {IContext} from '../i-context'
import {Size} from '../size'
import {Point} from '../geometry/point'
import {Rectangle} from '../geometry/rectangle'
import {DimensionLine} from '../dimension-line'
import {Line} from '../geometry/line'
import {Beam} from './beam'
import {Sides} from '../sides'
import {Connector} from './connector'
import {CalcError, ICalcErrorJSON} from './calc-error'
import {
    MAX_ALUMINIUM_HEIGHT,
    MAX_ALUMINIUM_WIDTH,
    MAX_HEIGHT,
    MAX_WIDTH,
    MIN_CONNECTOR_HEIGHT,
    MIN_HEIGHT,
    MIN_WIDTH
} from '../../constants/geometry-params'
import {LinesChain} from '../lines-chain'
import {CoordinateSystems} from '../coordinate-systems'
import {ConnectorParameters, IConnectorParametersJSON} from './connector-parameters'
import {Moskitka} from './moskitka'
import {FrameFilling} from './frame-filling'
import {MarkedLine} from '../marked-line'
import {ConnectorTypes} from './connector-types'
import {SolidFilling} from './solid-filling'
import {ModelParts} from './model-parts'
import {PointsOfView} from '../points-of-view'
import {getHorizontalLinesFromXArray, getSortedLines, getVerticalLinesFromYArray} from '../../helpers/line'
import {clamp, round} from '../../helpers/number'
import {getClone} from '../../helpers/json'
import {GUID} from '../../helpers/guid'
import {MoskitkaColors} from './moskitka-colors'
import {FactoryTypes} from './factory-types'

export interface IConstructionJSON {
    id?: string
    totalCost?: number
    amegaCost?: number
    amegaCostToDisplay?: number
    weight?: number
    square?: number
    quantity?: number
    position?: number
    products: IProductJSON[]
    estShippingDate?: string
    errors?: ICalcErrorJSON[]
    connectorsParameters?: IConnectorParametersJSON[]
    coordinateSystem?: CoordinateSystems
    defaultPointOfView?: PointsOfView
    haveEnergyProducts?: boolean
    energyProductType?: string
}

export class Construction implements IContext {
    static parse(constructionJSON: IConstructionJSON): Construction {
        const construction = new Construction()
        Object.assign(construction, constructionJSON, {
            weight: constructionJSON.weight ? Math.round(constructionJSON.weight * 100) / 100 : 0,
            square: constructionJSON.square ? Math.round(constructionJSON.square * 100) / 100 : 0,
            products: constructionJSON.products.map(productObj => Product.parse(productObj)),
            errors: (constructionJSON.errors || []).map(obj => CalcError.parse(obj)),
            connectorsParameters: (constructionJSON.connectorsParameters || []).map(obj => ConnectorParameters.parse(obj))
        })
        construction.update()
        return construction
    }

    id = GUID()
    totalCost = 0 // цена для клиента
    amegaCost = 0 // цена для дилера
    amegaCostToDisplay = 0 //цена для сабдилера
    weight = 0
    square = 0
    quantity = 1
    position = 0
    products: Product[] = []
    estShippingDate = ''
    errors: CalcError[] = []
    connectorsParameters: ConnectorParameters[] = []
    coordinateSystem: CoordinateSystems = CoordinateSystems.Relative
    defaultPointOfView = PointsOfView.Inside
    haveEnergyProducts = false
    energyProductType = ''

    updateProductsGuids(): void {
        this.products.forEach(product => {
            const connectors = this.allConnectors.filter(connector => connector.connectedProductGuid === product.productGuid)

            const newGuid = GUID()

            connectors.forEach(connector => {
                connector.connectedProductGuid = newGuid
            })

            product.productGuid = newGuid
        })
    }

    setDefaultHandles(factoryId?: FactoryTypes): void {
        this.allFrameFillings.map(f => f.setDefaultHandle(factoryId))
    }

    get clone(): Construction {
        return Construction.parse(getClone(this))
    }

    findNodeById(nodeId: string): IContext | null {
        const product = this.findProductByNodeId(nodeId)

        if (!product) {
            return null
        }

        return product.findNodeById(nodeId)
    }

    findProductAndNodeByNodeId<T extends IContext>(
        nodeId: string
    ): {
        product: Product | null
        node: IContext | null
    } {
        for (const product of this.products) {
            const node = product.findNodeById(nodeId) as T

            if (node) {
                return {
                    product,
                    node
                }
            }
        }

        return {
            product: null,
            node: null
        }
    }

    findProductByProductGuid(productGuid: string): Product | null {
        return this.products.find(p => p.productGuid === productGuid) ?? null
    }

    findProductByNodeId(nodeId: string): Product | null {
        for (const product of this.products) {
            const node = product.findNodeById(nodeId)
            if (node) {
                return product
            }
        }

        return null
    }

    get rect(): Rectangle {
        const points: Point[] = []

        this.products.forEach(product => {
            const {refPoint, size} = product

            points.push(refPoint)
            points.push(refPoint.withOffset(size.width, size.height))

            product.connectors.forEach(connector => {
                points.push(...connector.innerDrawingPoints)
            })
        })

        return Rectangle.buildFromPoints(points)
    }

    get nodeId(): string {
        return this.id
    }

    toCoordinateSystem(coordinateSystem: CoordinateSystems): void {
        if (this.coordinateSystem === coordinateSystem) {
            return
        }

        this.coordinateSystem = coordinateSystem

        this.products.forEach(product => this._productToCoordinateSystem(product, coordinateSystem))
    }

    get leftmostProduct(): Product {
        const products = this.products.slice(0).sort((p1, p2) => p1.refPoint.x - p2.refPoint.x)

        return products[0]
    }

    get rightmostProduct(): Product {
        const products = this.products.slice(0).sort((p1, p2) => p2.refPoint.x - p1.refPoint.x)

        return products[0]
    }

    get bottommostProduct(): Product {
        const products = this.products.slice(0).sort((p1, p2) => p1.refPoint.y - p2.refPoint.y)

        return products[0]
    }

    get topmostProduct(): Product {
        const products = this.products.slice(0).sort((p1, p2) => p2.refPoint.y - p1.refPoint.y)

        return products[0]
    }

    get leftmostBeam(): Beam {
        return this.leftmostProduct.leftmostBeam
    }

    get rightmostBeam(): Beam {
        return this.rightmostProduct.rightmostBeam
    }

    get topmostBeam(): Beam {
        return this.topmostProduct.topmostBeam
    }

    get bottommostBeam(): Beam {
        return this.bottommostProduct.bottommostBeam
    }

    get allConnectors(): Connector[] {
        const connectors: Connector[] = []

        this.products.forEach(product => {
            connectors.push(...product.connectors)
        })

        return connectors
    }

    get allRootFrameBeams(): Beam[] {
        const beams: Beam[] = []

        this.products.forEach(product => {
            beams.push(...product.frame.frameBeams)
        })

        return beams
    }

    get size(): Size {
        return this.rect.size
    }

    get height(): number {
        return this.rect.size.height
    }

    get width(): number {
        return this.rect.size.width
    }

    get x(): number {
        return this.rect.refPoint.x
    }

    get y(): number {
        return this.rect.refPoint.y
    }

    get allFrameFillings(): FrameFilling[] {
        const frameFillings: FrameFilling[] = []

        this.products.forEach(product => {
            frameFillings.push(...product.frame.allFrameFillings)
        })

        return frameFillings
    }

    get allSolidFillings(): SolidFilling[] {
        const solidFillings: SolidFilling[] = []

        this.products.forEach(product => {
            solidFillings.push(...product.frame.allSolidFillings)
        })

        return solidFillings
    }

    get isMoskitka(): boolean {
        return this.products[0].isMoskitka
    }

    get isBrownMoskitka(): boolean {
        return (this.isMoskitka && this?.moskitka?.color === MoskitkaColors.Brown) ?? false
    }

    get moskitka(): Moskitka | null {
        return this.products[0].frame.innerFillings[0].leaf?.moskitka ?? null
    }

    get isEmpty(): boolean {
        return this.products[0].isEmpty
    }

    get isAluminium(): boolean {
        return this.products[0].isAluminium
    }

    get allPoints(): Point[] {
        const points: Point[] = []

        this.products.forEach(product => {
            points.push(...product.allPoints)
        })

        return points
    }

    addConnector(hostProduct: Product, connectingSide: Sides, params?: ConnectorParameters): void {
        const addConnectorToProduct = (product: Product, connectingSide: Sides): Beam | null => {
            let hostBeam!: Beam

            if (connectingSide === Sides.Left) {
                hostBeam = product.leftmostConnectedBeam
            } else if (connectingSide === Sides.Right) {
                hostBeam = product.rightmostConnectedBeam
            } else if (connectingSide === Sides.Top) {
                hostBeam = product.topmostConnectedBeam
            } else {
                hostBeam = product.bottommostConnectedBeam
            }

            const connectorDefaultParams =
                params ?? this.connectorsParameters.find(p => p.isHType()) ?? product.profile.getGeometryParameters(ModelParts.Connector)

            let start: Point | undefined
            let end: Point | undefined

            const hostBeamRect = Rectangle.buildFromPoints(hostBeam.innerDrawingPoints)

            if (hostBeam.isVertical) {
                const x = connectingSide === Sides.Right ? hostBeamRect.rightTopPoint.x : hostBeamRect.leftBottomPoint.x

                start = new Point(x, hostBeam.start.y)
                end = new Point(x, hostBeam.end.y)
            } else if (hostBeam.isHorizontal) {
                const y = connectingSide === Sides.Top ? hostBeamRect.rightTopPoint.y : hostBeamRect.leftBottomPoint.y

                start = new Point(hostBeam.start.x, y)
                end = new Point(hostBeam.end.x, y)
            }

            if (!start || !end) {
                throw new Error('Не определены концы конектора!')
            }

            const connector = Connector.Builder.withStart(start)
                .withEnd(end)
                .withProfileVendorCode(connectorDefaultParams.marking)
                .withWidth(connectorDefaultParams.a)
                .withHostBeamGuid(hostBeam.nodeId)
                .withConnectorType(
                    connectorDefaultParams instanceof ConnectorParameters ? connectorDefaultParams.connectorType : ConnectorTypes.Connector
                )
                .build()

            if (product.frame.containerPolygon.isRect && connector) {
                connector.normalizePointsOrder()

                if (connector.isVertical) {
                    const {topmostConnectedBeam, bottommostConnectedBeam} = product

                    connector.start.y = Rectangle.buildFromPoints(bottommostConnectedBeam.innerDrawingPoints).rightBottomPoint.y
                    connector.end.y = Rectangle.buildFromPoints(topmostConnectedBeam.innerDrawingPoints).rightTopPoint.y
                } else if (connector.isHorizontal) {
                    const verticalConnectors = product.connectors.filter(c => c.isVertical)

                    verticalConnectors.forEach(verticalConnector => {
                        verticalConnector.normalizePointsOrder()

                        if (verticalConnector.start.y === connector.start.y) {
                            verticalConnector.start.y -= connector.width

                            const connectedProduct = this.products.find(p => p.productGuid === verticalConnector.connectedProductGuid)

                            if (connectedProduct) {
                                connectedProduct.productOffset += connector.width
                            }
                        }
                        if (verticalConnector.end.y === connector.start.y) {
                            verticalConnector.end.y += connector.width
                        }
                    })
                }
            }

            product.addConnector(connector)
            product.update()

            return hostBeam
        }

        const hostBeam = addConnectorToProduct(hostProduct, connectingSide)

        if (hostBeam) {
            this.products.forEach(product => {
                if (product.nodeId === hostProduct.nodeId) return
                if (product.haveConnectorOnSide(connectingSide)) return

                const {topmostBeam, bottommostBeam, leftmostBeam, rightmostBeam} = product
                const productSidesBeam = {
                    top: topmostBeam,
                    bottom: bottommostBeam,
                    left: leftmostBeam,
                    right: rightmostBeam
                }
                const productHostBeam = productSidesBeam[connectingSide]
                if (connectingSide === Sides.Bottom || connectingSide === Sides.Top) {
                    if (hostBeam.start.y === productHostBeam.start.y) {
                        addConnectorToProduct(product, connectingSide)
                    }
                } else {
                    if (hostBeam.start.x === productHostBeam.start.x) {
                        addConnectorToProduct(product, connectingSide)
                    }
                }
            })
        }

        this.update()
    }

    removeConnector(connector: Connector): void {
        const hostProduct = this.findProductByNodeId(connector.nodeId)

        if (!hostProduct) {
            return
        }

        if (connector.isHorizontal && connector.connectorType === ConnectorTypes.Extender && hostProduct.frame.containerPolygon.isRect) {
            const isTopExtender = connector.nodeId === hostProduct.topmostConnectedBeam.nodeId

            const verticalConnectors = hostProduct.connectors.filter(c => {
                if (!c.isVertical) {
                    return false
                }

                c.normalizePointsOrder()

                return isTopExtender ? c.end.y === connector.start.y + connector.width : c.start.y === connector.start.y - connector.width
            })

            verticalConnectors.forEach(verticalConnector => {
                if (isTopExtender) {
                    verticalConnector.end.y -= connector.width
                } else {
                    verticalConnector.start.y += connector.width

                    const connectedProduct = this.products.find(p => p.productGuid === verticalConnector.connectedProductGuid)

                    if (connectedProduct) {
                        connectedProduct.productOffset -= connector.width
                    }
                }
            })
        }

        hostProduct.removeConnector(connector)

        this._updateProductOffsets()
    }

    addConnectorToChildFromParrent(hostProduct: Product, connectedProduct: Product, connectingSide: Sides): Product {
        let start: Point = new Point(0, 0)
        let end: Point = new Point(0, 0)
        const refPoint = connectedProduct.rect.refPoint
        const bottomMostLength = connectedProduct.bottommostBeam.length
        const topMostLength = connectedProduct.topmostBeam.length
        const rightMostLength = connectedProduct.rightmostBeam.length
        const leftMostLength = connectedProduct.leftmostBeam.length
        let parrentConnector: Connector | undefined

        for (const side in Sides) {
            parrentConnector = hostProduct.haveConnectorOnSide(side.toLowerCase())
            if (parrentConnector) {
                if (side === 'Bottom') {
                    if (connectingSide === Sides.Left || connectingSide === Sides.Right) {
                        start = new Point(refPoint.x, refPoint.y)
                        end = new Point(refPoint.x + bottomMostLength, refPoint.y)
                        connectedProduct.addConnector(
                            Connector.Builder.withStart(start)
                                .withEnd(end)
                                .withWidth(parrentConnector.width)
                                .withProfileVendorCode(parrentConnector.profileVendorCode)
                                .withHostBeamGuid(connectedProduct.bottommostBeam.nodeId)
                                .build()
                        )
                    }
                } else if (side === 'Left') {
                    if (connectingSide === Sides.Bottom || connectingSide === Sides.Top) {
                        start = new Point(refPoint.x, refPoint.y)
                        end = new Point(refPoint.x, refPoint.y + leftMostLength)
                        connectedProduct.addConnector(
                            Connector.Builder.withStart(start)
                                .withEnd(end)
                                .withWidth(parrentConnector.width)
                                .withProfileVendorCode(parrentConnector.profileVendorCode)
                                .withHostBeamGuid(connectedProduct.leftmostBeam.nodeId)
                                .build()
                        )
                    }
                } else if (side === 'Top') {
                    if (connectingSide === Sides.Left || connectingSide === Sides.Right) {
                        start = new Point(refPoint.x, refPoint.y + leftMostLength)
                        end = new Point(refPoint.x + topMostLength, refPoint.y + leftMostLength)
                        connectedProduct.addConnector(
                            Connector.Builder.withStart(start)
                                .withEnd(end)
                                .withWidth(parrentConnector.width)
                                .withProfileVendorCode(parrentConnector.profileVendorCode)
                                .withHostBeamGuid(connectedProduct.topmostBeam.nodeId)
                                .build()
                        )
                    }
                } else if (side === 'Right') {
                    if (connectingSide === Sides.Bottom || connectingSide === Sides.Top) {
                        start = new Point(refPoint.x + bottomMostLength, refPoint.y)
                        end = new Point(refPoint.x + bottomMostLength, refPoint.y + rightMostLength)
                        connectedProduct.addConnector(
                            Connector.Builder.withStart(start)
                                .withEnd(end)
                                .withWidth(parrentConnector.width)
                                .withProfileVendorCode(parrentConnector.profileVendorCode)
                                .withHostBeamGuid(connectedProduct.rightmostBeam.nodeId)
                                .build()
                        )
                    }
                }
            }
        }
        return connectedProduct
    }

    connectProduct(hostProduct: Product, connectedProduct: Product, connectingSide: Sides): void {
        let hostProductBeam!: Beam
        let connectedBeam!: Beam

        if (connectingSide === Sides.Left) {
            hostProductBeam = hostProduct.leftmostBeam
            connectedBeam = connectedProduct.rightmostBeam
        } else if (connectingSide === Sides.Right) {
            hostProductBeam = hostProduct.rightmostBeam
            connectedBeam = connectedProduct.leftmostBeam
        } else if (connectingSide === Sides.Top) {
            hostProductBeam = hostProduct.topmostBeam
            connectedBeam = connectedProduct.bottommostBeam
        } else if (connectingSide === Sides.Bottom) {
            hostProductBeam = hostProduct.bottommostBeam
            connectedBeam = connectedProduct.topmostBeam
        }

        connectedProduct = this.addConnectorToChildFromParrent(hostProduct, connectedProduct, connectingSide)
        this.products.push(connectedProduct)

        const door = this.products.find(product => product.isDoor)
        if (door) {
            this.products.forEach(product => {
                product.productionTypeId = door.productionTypeId
            })
        }

        connectedProduct.productGuid = GUID()
        connectedProduct.setProfile(hostProduct.profile)
        connectedProduct.setFurniture(hostProduct.furniture)

        const connectorLength = Math.min(hostProductBeam.length, connectedBeam.length)

        let connectorEnd: Point | undefined

        const hostProductBeamRect = Rectangle.buildFromPoints(hostProductBeam.innerDrawingPoints)

        if (hostProductBeam.isVertical) {
            const isConnectedToRight = hostProductBeam === hostProduct.rightmostBeam

            const x = isConnectedToRight ? hostProductBeamRect.rightTopPoint.x : hostProductBeamRect.leftBottomPoint.x

            connectorEnd = new Point(x, hostProductBeam.rect.refPoint.y + hostProductBeam.length)
        } else if (hostProductBeam.isHorizontal) {
            const isConnectedToTop = hostProductBeam === hostProduct.topmostBeam

            const y = isConnectedToTop ? hostProductBeamRect.rightTopPoint.y : hostProductBeamRect.leftBottomPoint.y

            connectorEnd = new Point(hostProductBeam.rect.refPoint.x + connectorLength, y)
        }

        if (!connectorEnd) {
            return
        }
        let defaultConnector = Connector.Builder.withStart(
            hostProductBeam.isVertical ? connectorEnd.withOffset(0, -connectorLength) : connectorEnd.withOffset(-connectorLength, 0)
        )
            .withEnd(connectorEnd)
            .withProfileVendorCode(hostProduct.profile.getGeometryParameters(ModelParts.Connector).marking)
            .withWidth(hostProduct.profile.getGeometryParameters(ModelParts.Connector).a)
            .withHostBeamGuid(hostProductBeam.nodeId)
            .withConnectedProductGuid(connectedProduct.productGuid)
            .build()

        const connectorIndex = hostProduct.connectors.findIndex(c => c.connectedBeamGuid === hostProductBeam.beamGuid)

        if (connectorIndex === -1) {
            hostProduct.addConnector(defaultConnector)
        } else {
            defaultConnector =
                hostProduct.connectors.find(connector => {
                    return connector.connectedBeamGuid === hostProductBeam.beamGuid
                }) || defaultConnector
            defaultConnector.connectedProductGuid = connectedProduct.productGuid
            hostProduct.replaceConnector(defaultConnector, connectorIndex)
        }

        hostProduct.update()

        connectedProduct.productOffset = hostProductBeam.isVertical ? defaultConnector.rect.size.height - connectedBeam.rect.size.height : 0

        if (hostProductBeam.isVertical) {
            const offsetX = connectingSide === Sides.Right ? defaultConnector.width : -connectedProduct.width - defaultConnector.width
            const beamOffsetY = connectedProduct.rect.refPoint.y - connectedBeam.leftBottomPoint.y
            connectedProduct.refPoint = defaultConnector.start.withOffset(offsetX, connectedProduct.productOffset + beamOffsetY)
        } else if (hostProductBeam.isHorizontal) {
            const offsetY = connectingSide === Sides.Top ? defaultConnector.width : -connectedProduct.height - defaultConnector.width

            const beamOffsetX = connectedProduct.rect.refPoint.x - connectedBeam.leftBottomPoint.x

            connectedProduct.refPoint = defaultConnector.start.withOffset(connectedProduct.productOffset + beamOffsetX, offsetY)
        }

        this._productToCoordinateSystem(connectedProduct, CoordinateSystems.Absolute)
    }

    removeProduct(product: Product): void {
        const hostProduct = this.products.find(p => !!p.connectors.find(c => c.connectedProductGuid === product.productGuid))

        if (!hostProduct) {
            console.warn('hostProduct no found')
            return
        }

        const removedConnector = hostProduct.connectors.find(c => c.connectedProductGuid === product.productGuid)

        if (!removedConnector) {
            console.warn('removedConnector no found')
            return
        }

        const removedProductIndex = this.products.indexOf(product)

        removedConnector.connectedProductGuid = undefined
        hostProduct.removeConnector(removedConnector)
        this.products.splice(removedProductIndex, 1)
    }

    getDimensionLines(): DimensionLine[] {
        const getMarkedLines = (construction: Construction, isHorizontal: boolean): MarkedLine[][] => {
            const getCoordinate = (point: Point): number => {
                return isHorizontal ? point.x : point.y
            }

            const getLinesFromArray = isHorizontal ? getHorizontalLinesFromXArray : getVerticalLinesFromYArray

            const getProductLines = (product: Product, isHorizontal: boolean): MarkedLine[] => {
                const lines: Line[] = []

                const coordinateSet = new Set<number>()
                product.frame.frameBeams.forEach(beam => {
                    const {start, end} = beam

                    coordinateSet.add(getCoordinate(start))
                    coordinateSet.add(getCoordinate(end))
                })
                const beamsCoordinateArray = Array.from(coordinateSet)

                const minCoordinate = Math.min(...beamsCoordinateArray)
                const maxCoordinate = Math.max(...beamsCoordinateArray)

                lines.push(
                    isHorizontal
                        ? new MarkedLine(new Point(minCoordinate, 0), new Point(maxCoordinate, 0))
                        : new MarkedLine(new Point(0, minCoordinate), new Point(0, maxCoordinate))
                )

                lines.push(...getLinesFromArray(beamsCoordinateArray))

                const imposts = [...product.frame.impostBeams]

                if (product.isMoskitka && product.frame.innerFillings[0] && product.frame.innerFillings[0].leaf) {
                    imposts.push(...product.frame.innerFillings[0].leaf.impostBeams)
                }

                const impostsWithProjection = imposts.filter(impost => {
                    const lineProjection = isHorizontal ? impost.OY : impost.OX
                    return !lineProjection.start.equals(lineProjection.end)
                })

                product.frame.containerPolygon.segments.forEach(segment => {
                    const segmentProjection = isHorizontal ? segment.OX : segment.OY

                    if (!segmentProjection.start.equals(segmentProjection.end)) {
                        const coordinateArray = [getCoordinate(segment.start), getCoordinate(segment.end)]
                        impostsWithProjection.forEach(impost => {
                            const intersectionPoint = segment.findIntersectionPoint(impost)
                            if (segment.hasCommonStartOrEnd(impost) && intersectionPoint) {
                                coordinateArray.push(getCoordinate(intersectionPoint))
                            }
                        })
                        lines.push(...getLinesFromArray(coordinateArray))
                    }
                })

                impostsWithProjection.forEach(impost => {
                    impost.containerPolygon.segments.forEach(segment => {
                        if (segment.hasCommonStartOrEnd(impost)) {
                            const xArray = [getCoordinate(segment.start), getCoordinate(segment.end)]
                            const intersectionPoint = segment.findIntersectionPoint(impost)
                            intersectionPoint && xArray.push(getCoordinate(intersectionPoint))
                            lines.push(...getLinesFromArray(xArray))
                        }
                    })
                })

                return lines.map(l => {
                    const mLine = new MarkedLine(l.start, l.end, l.width)
                    mLine.markers.push(product.nodeId)
                    return mLine
                })
            }

            const linesToDraw: MarkedLine[][] = []

            const lines: MarkedLine[] = [
                new MarkedLine(
                    isHorizontal ? new Point(construction.x, 0) : new Point(0, construction.y),
                    isHorizontal ? new Point(construction.x + construction.width, 0) : new Point(0, construction.y + construction.height)
                )
            ]

            construction.products.forEach(product => {
                lines.push(...getProductLines(product, isHorizontal))

                const points = product.allPoints
                product.connectors.forEach(connector => {
                    points.push(...connector.innerDrawingPoints)
                })
                const rect = Rectangle.buildFromPoints(points)
                lines.push(...getLinesFromArray(rect.drawingPoints.map(point => getCoordinate(point))).map(l => MarkedLine.from(l)))
            })

            const uniqueLines = getSortedLines(lines, (l1, l2) => getCoordinate(l1.start) - getCoordinate(l2.start))
            const usedLines: MarkedLine[] = []

            const findNextLines = (line: MarkedLine, lines: MarkedLine[]): MarkedLine[] => {
                const searchCoordinate = getCoordinate(line.end)
                return lines.filter(l => searchCoordinate - getCoordinate(l.start) <= 0)
            }

            const findLinesChain = (line: MarkedLine, chain: LinesChain<MarkedLine>): LinesChain<MarkedLine> => {
                const nextLines = findNextLines(
                    line,
                    uniqueLines.filter(l => !usedLines.includes(l))
                )

                if (nextLines.length === 0) {
                    return new LinesChain(chain.lines.concat(line))
                }

                const linesChainsArray = nextLines
                    .map(l => findLinesChain(l, new LinesChain(chain.lines.concat(line).slice(0))))
                    .sort((ch1, ch2) => {
                        let res = ch2.length - ch1.length
                        if (res === 0) {
                            res = ch1.lines.length - ch2.lines.length
                        }
                        return res
                    })

                return linesChainsArray[0]
            }

            uniqueLines.forEach(line => {
                if (!usedLines.includes(line)) {
                    const chain = findLinesChain(line, new LinesChain())
                    usedLines.push(...chain.lines)
                    linesToDraw.push(chain.lines)
                }
            })

            return linesToDraw.filter(lines => lines.length !== 0)
        }

        const verticalMarkedLines = getMarkedLines(this, false)
        const horizontalMarkedLines = getMarkedLines(this, true)

        const getDimensionLines = (linesGroups: MarkedLine[][]): DimensionLine[] => {
            const lineOffsetLocal = 1
            let currentOffset = -lineOffsetLocal * linesGroups.length

            const dimensionLines: DimensionLine[] = []

            linesGroups.forEach(lines => {
                lines.forEach(line => {
                    const lineWithOffset = MarkedLine.from(
                        line.withOffset(line.isHorizontal ? 0 : currentOffset, line.isHorizontal ? currentOffset : 0)
                    )
                    lineWithOffset.markers = line.markers

                    dimensionLines.push(new DimensionLine(lineWithOffset))
                })

                currentOffset += lineOffsetLocal
            })

            return dimensionLines
        }

        return getDimensionLines(horizontalMarkedLines).concat(getDimensionLines(verticalMarkedLines))
    }

    changeSizesAndOffsets(
        dimensionLine: DimensionLine,
        value: number,
        context: IContext | null | undefined = null,
        maxWidthMsk?: number
    ): void {
        const roundedValue = round(value)
        const isVerticalDimensionLine = dimensionLine.isVertical

        const connectedProductsOffsetsMap = new Map()

        this.products.forEach(product => {
            product.connectors.forEach(connector => {
                const connectedProduct = this.products.find(p => p.productGuid === connector.connectedProductGuid)
                if (connectedProduct) {
                    connectedProductsOffsetsMap.set(connectedProduct, this.getProductsRelativeOffset(product, connectedProduct))
                }
            })
        })

        this.products
            .filter(product => {
                if (context) {
                    return Boolean(product.findNodeById(context.nodeId))
                }

                return true
            })
            .forEach(product => {
                const {width, height, refPoint, isAluminium} = product

                const maxHeight = isAluminium ? MAX_ALUMINIUM_HEIGHT : width <= MAX_HEIGHT ? maxWidthMsk ?? MAX_WIDTH : MAX_HEIGHT
                const maxWidth = isAluminium ? MAX_ALUMINIUM_WIDTH : height <= MAX_HEIGHT ? maxWidthMsk ?? MAX_WIDTH : MAX_HEIGHT

                const productSizeProjection = isVerticalDimensionLine
                    ? new Line(Point.zero, new Point(0, height)).withOffset(0, refPoint.y)
                    : new Line(Point.zero, new Point(width, 0)).withOffset(refPoint.x, 0)
                const dimensionLineProjection = isVerticalDimensionLine ? dimensionLine.origin.OY : dimensionLine.origin.OX
                const dimensionName = isVerticalDimensionLine ? 'height' : 'width'
                const minDimension = isVerticalDimensionLine ? MIN_HEIGHT : MIN_WIDTH
                const maxDimension = isVerticalDimensionLine ? maxHeight : maxWidth
                const coordinateName = isVerticalDimensionLine ? 'y' : 'x'

                // флаг указывающий что размерная линия будем менять оступ справа или сверху
                const isEndOffset = dimensionLineProjection.end[coordinateName] === productSizeProjection.end[coordinateName]
                const oldCoordinate = isEndOffset ? dimensionLineProjection.start[coordinateName] : dimensionLineProjection.end[coordinateName]
                const newCoordinate = isEndOffset
                    ? dimensionLineProjection.start[coordinateName] + dimensionLineProjection.length - roundedValue
                    : dimensionLineProjection.end[coordinateName] - dimensionLineProjection.length + roundedValue

                if (dimensionLineProjection.leftBottomPoint[coordinateName] < productSizeProjection.leftBottomPoint[coordinateName]) {
                    return
                }

                // означает что размерная линия совпадает одним из габаритов (ширина или высота)
                if (dimensionLineProjection.equals(productSizeProjection)) {
                    const connectorPoints: Point[] = []
                    product.connectors.forEach(connector => {
                        connectorPoints.push(connector.start, connector.end)
                    })

                    const translateValue = roundedValue - product[dimensionName]
                    connectorPoints.forEach(point => {
                        if (point[coordinateName] >= dimensionLineProjection.end[coordinateName]) {
                            point[coordinateName] = point[coordinateName] + translateValue
                        }
                    })

                    console.log('Меняем размер изделия')

                    this._productToCoordinateSystem(product, CoordinateSystems.Relative)
                    product[dimensionName] = clamp(roundedValue, minDimension, maxDimension)
                    this._productToCoordinateSystem(product, CoordinateSystems.Absolute)

                    return
                }

                product.frame.allImposts.forEach(impost => {
                    if (impost.start[coordinateName] === oldCoordinate) {
                        impost.start[coordinateName] = newCoordinate
                    }
                    if (impost.end[coordinateName] === oldCoordinate) {
                        impost.end[coordinateName] = newCoordinate
                    }
                    product.updateNode(impost)
                })

                product.allPoints.forEach(point => {
                    if (point[coordinateName] === oldCoordinate) {
                        point[coordinateName] = newCoordinate
                    }
                })

                product.update()
            })

        connectedProductsOffsetsMap.forEach((offset: number, product: Product) => {
            this.setProductOffset(product, offset)
        })

        this.update()
    }

    setProductOffset(connectedProduct: Product, offset: number, needUpdate = true): void {
        const connector = this.allConnectors.find(c => c.connectedProductGuid === connectedProduct.productGuid)

        if (!connector) {
            return
        }

        const hostProduct = this.products.find(p => p.connectors.includes(connector))

        if (!hostProduct) {
            return
        }

        connector.normalizePointsOrder()

        const getProductMainDirectionSideBeam = (product: Product): Beam => {
            if (connector.isVertical) {
                return product.rightmostBeam
            }
            return product.topmostBeam
        }

        const getProductAltDirectionSideBeam = (product: Product): Beam => {
            if (connector.isVertical) {
                return product.leftmostBeam
            }
            return product.bottommostBeam
        }

        const getCoordinate = (point: Point): number => {
            if (connector.isVertical) {
                return point.y
            }
            return point.x
        }

        const isConnectedToMainDirection = getProductMainDirectionSideBeam(hostProduct).nodeId === connector.connectedBeamGuid

        const hostConnectedBeam = isConnectedToMainDirection
            ? getProductMainDirectionSideBeam(hostProduct)
            : getProductAltDirectionSideBeam(hostProduct)
        const connectedBeam = isConnectedToMainDirection
            ? getProductAltDirectionSideBeam(connectedProduct)
            : getProductMainDirectionSideBeam(connectedProduct)

        const hostConnectedLine = hostProduct.getSideLine(hostConnectedBeam)
        const connectedBeamLine = connectedProduct.getSideLine(connectedBeam)

        const connectorMinCoordinate = getCoordinate(hostConnectedLine.leftBottomPoint) + MIN_CONNECTOR_HEIGHT
        const connectorMaxCoordinate = getCoordinate(hostConnectedLine.rightTopPoint) - MIN_CONNECTOR_HEIGHT

        const connectedMinBeamEndCoordinate = getCoordinate(hostConnectedLine.leftBottomPoint) + MIN_CONNECTOR_HEIGHT
        const connectedMaxBeamEndCoordinate = getCoordinate(hostConnectedLine.rightTopPoint) + connectedBeamLine.length - MIN_CONNECTOR_HEIGHT

        const connectedBeamEndCoordinate = clamp(
            connector.isVertical
                ? hostConnectedLine.rightTopPoint.y + offset
                : hostConnectedLine.leftBottomPoint.x - offset + connectedBeamLine.length,
            connectedMinBeamEndCoordinate,
            connectedMaxBeamEndCoordinate
        )
        const connectedBeamStartCoordinate = connectedBeamEndCoordinate - connectedBeamLine.length

        connector.start[connector.isVertical ? 'y' : 'x'] = clamp(
            connectedBeamStartCoordinate,
            getCoordinate(hostConnectedLine.leftBottomPoint),
            connectorMaxCoordinate
        )
        connector.end[connector.isVertical ? 'y' : 'x'] = clamp(
            connectedBeamEndCoordinate,
            connectorMinCoordinate,
            getCoordinate(hostConnectedLine.rightTopPoint)
        )

        connectedProduct.productOffset = connectedBeamStartCoordinate - getCoordinate(connector.leftBottomPoint)

        if (connector.isVertical) {
            if (isConnectedToMainDirection) {
                if (connectedBeamLine.start.x === connectedProduct.bottommostBeam.leftBottomPoint.x) {
                    connectedProduct.productOffset +=
                        connectedProduct.frame.bottommostBeam.leftBottomPoint.y - connectedProduct.leftmostBottommostPoint.y
                }
            } else {
                if (connectedBeamLine.end.x === connectedProduct.bottommostBeam.rightTopPoint.x) {
                    connectedProduct.productOffset +=
                        connectedProduct.frame.bottommostBeam.rightTopPoint.y - connectedProduct.rightmostBottommostPoint.y
                }
            }
        }

        needUpdate && this.update()
    }

    update(): void {
        this.products.forEach(product => {
            const connectorDefaultParams = this.connectorsParameters.find(p => p.isHType())

            product.connectors.forEach(connector => {
                const connectorParams = this.connectorsParameters.find(p => p.marking === connector.profileVendorCode) || connectorDefaultParams

                if (connectorParams) {
                    connector.profileVendorCode = connectorParams.marking
                    connector.width = connectorParams.a
                    connector.connectorType = connectorParams.connectorType
                }
            })
        })

        this._defineProductsAbsoluteRefPoints()
    }

    getProductsRelativeOffset(hostProduct: Product, connectedProduct: Product): number {
        const connector = hostProduct.connectors.find(c => c.connectedProductGuid === connectedProduct.productGuid)

        if (!connector) {
            return 0
        }

        if (connector.isVertical) {
            const isConnectedToRight = hostProduct.rightmostBeam.nodeId === connector.connectedBeamGuid

            const hostConnectedSide = isConnectedToRight ? hostProduct.rightmostSide : hostProduct.leftmostSide
            const connectedBeamSide = isConnectedToRight ? connectedProduct.leftmostSide : connectedProduct.rightmostSide

            return connectedBeamSide.rightTopPoint.y - hostConnectedSide.rightTopPoint.y
        } else if (connector.isHorizontal) {
            const isConnectedToTop = hostProduct.topmostBeam.nodeId === connector.connectedBeamGuid

            const hostConnectedBeam = isConnectedToTop ? hostProduct.topmostBeam : hostProduct.bottommostBeam
            const connectedBeam = isConnectedToTop ? connectedProduct.bottommostBeam : connectedProduct.topmostBeam

            return -(connectedBeam.leftBottomPoint.x - hostConnectedBeam.leftBottomPoint.x)
        }

        return 0
    }

    alignDoorImpost(): void {
        if (!this.needAlignDoorImpost) {
            return
        }

        const door = this.products.find(product => product.isDoor)
        if (!door) {
            return
        }

        const horizontalImpost = door.frame.innerFillings[0]?.leaf?.impostBeams[0]
        const referenceBeamY = this.doorImpostReferenceY

        if (!horizontalImpost || !referenceBeamY) {
            return
        }

        horizontalImpost.start.y = referenceBeamY
        horizontalImpost.end.y = referenceBeamY
    }

    private get doorImpostReferenceY(): number | null {
        if (this.products.length < 2) {
            return null
        }

        const door = this.products.find(product => product.isDoor)
        if (!door) {
            return null
        }

        if (door.frame.innerFillings.length !== 1) {
            return null
        }

        const doorInnerFilling = door.frame.innerFillings[0]
        if (!doorInnerFilling.leaf) {
            return null
        }

        if (doorInnerFilling.leaf.impostBeams.length !== 1 || !doorInnerFilling.leaf.impostBeams[0].isHorizontal) {
            return null
        }

        const referenceProducts = this.products.filter(product =>
            product.connectors.some(connector => connector.isVertical && connector.connectedProductGuid === door.productGuid)
        )

        door.connectors
            .filter(connector => connector.isVertical)
            .forEach(connector => {
                const product = this.products.find(p => p.productGuid === connector.connectedProductGuid)
                product && referenceProducts.push(product)
            })

        const availableReferenceProducts = referenceProducts.filter(product => product.frame.bottommostBeam.isHorizontal)

        if (availableReferenceProducts.length === 0) {
            return null
        }

        const getReferenceBeam = (product: Product): Beam => {
            let referenceBeam = product.frame.bottommostBeam

            const availableBeams: Beam[] = []

            product.frame.innerFillings.forEach(innerFilling => {
                if (innerFilling.leaf) {
                    const bottommostBeam = innerFilling.leaf.bottommostBeam
                    bottommostBeam.isHorizontal && availableBeams.push(bottommostBeam)
                }
            })

            if (product.refPoint.x < door.refPoint.x) {
                // дверь справа от от окна

                const beam = availableBeams.find(
                    beam =>
                        beam.end.y - referenceBeam.end.y < referenceBeam.width &&
                        referenceBeam.rightTopPoint.x - beam.rightTopPoint.x < referenceBeam.width
                )

                if (beam) {
                    referenceBeam = beam
                }
            } else {
                // дверь слева от от окна

                const beam = availableBeams.find(
                    beam =>
                        beam.end.y - referenceBeam.end.y < referenceBeam.width &&
                        beam.leftBottomPoint.x - referenceBeam.leftBottomPoint.x < referenceBeam.width
                )

                if (beam) {
                    referenceBeam = beam
                }
            }

            return referenceBeam
        }

        const referenceBeams = availableReferenceProducts.map(product => getReferenceBeam(product)).sort((b1, b2) => b1.end.y - b2.end.y)

        if (referenceBeams.length >= 1 && door.frame.innerFillings[0].leaf) {
            const horizontalImpost = door.frame.innerFillings[0].leaf.impostBeams[0]
            const newY = referenceBeams[0].end.y + referenceBeams[0].width - horizontalImpost.width / 2

            const {bottommostBeam} = door.frame.innerFillings[0].leaf

            const bottommostBeamRect = Rectangle.buildFromPoints(bottommostBeam.innerDrawingPoints)
            if (bottommostBeamRect.rightTopPoint.y >= newY - (3 * horizontalImpost.width) / 2) {
                return null
            }

            return newY
        }

        return null
    }

    get needAlignDoorImpost(): boolean {
        const door = this.products.find(product => product.isDoor)
        if (!door) {
            return false
        }

        const horizontalImpost = door.frame.innerFillings[0]?.leaf?.impostBeams[0]
        if (!horizontalImpost) {
            return false
        }

        const {doorImpostReferenceY} = this
        return !!(doorImpostReferenceY && horizontalImpost.start.y !== doorImpostReferenceY)
    }

    alignFillingsByLightAperture(): void {
        this.products.forEach(p => p.alignFillingsByLightAperture())
        this.update()
    }

    get needAlignFillingsByLightAperture(): boolean {
        return this.products.some(p => p.needAlignFillingsByLightAperture)
    }

    setMoskitka(frameFillingId: string, moskitka: Moskitka): void {
        this.allFrameFillings.forEach(frame => {
            if (!frame.isRoot && frame.moskitka) {
                frame.moskitka.color = moskitka.color
                frame.moskitka.catproof = moskitka.catproof

                if (moskitka.set) {
                    frame.moskitka = moskitka
                } else if (frame.nodeId === frameFillingId) {
                    frame.moskitka = moskitka
                }
            }
        })
    }

    updateConnector(connectorNodeId: string, connectorParameter: ConnectorParameters, connectorsParameters: ConnectorParameters[]): void {
        const connector = this.findNodeById(connectorNodeId) as Connector
        if (!connector) {
            return
        }

        const widthDif = connectorParameter.a - connector.width

        connector.profileVendorCode = connectorParameter.marking
        connector.width = connectorParameter.a

        this.connectorsParameters = connectorsParameters

        let condition: (point: Point) => boolean
        let offset: number

        const connectorRectangle = Rectangle.buildFromPoints(connector.innerDrawingPoints)
        const coordinateName = connector.isVertical ? 'x' : 'y'

        if (connector.start[coordinateName] === connectorRectangle.refPoint[coordinateName]) {
            condition = point => point[coordinateName] > connector.start[coordinateName]
            offset = widthDif
        } else {
            condition = point => point[coordinateName] < connector.start[coordinateName]
            offset = -widthDif
        }

        this.translateAllPoints(condition, connector.isVertical ? offset : 0, connector.isHorizontal ? offset : 0)
    }

    translateAllPoints(condition: (p: Point) => boolean, xOffset: number, yOffset: number): void {
        this.products.forEach(product => {
            if (condition(product.refPoint)) {
                product.refPoint = product.refPoint.withOffset(xOffset, yOffset)
            }
        })
        this.allPoints.forEach(point => {
            if (condition(point)) {
                point.x += xOffset
                point.y += yOffset
            }
        })

        this.update()
    }

    private _defineProductsAbsoluteRefPoints(): void {
        this.toCoordinateSystem(CoordinateSystems.Relative)

        this.allRootFrameBeams.forEach(beam => (beam.isConnectionPart = false))
        this.allConnectors.forEach(connector => (connector.isConnectionPart = false))

        this.products.forEach(hostProduct => {
            hostProduct.connectors.forEach(connector => {
                const hostBeam = hostProduct.findHostBeam(connector)
                if (!hostBeam) {
                    return
                }

                const connectedProduct = this.products.find(product => product.productGuid === connector.connectedProductGuid)

                if (connectedProduct) {
                    if (connector.isVertical) {
                        const isConnectedToRight = hostBeam === hostProduct.rightmostBeam

                        const connectedBeam = isConnectedToRight ? connectedProduct.leftmostBeam : connectedProduct.rightmostBeam

                        hostBeam.isConnectionPart = true
                        connectedBeam.isConnectionPart = true

                        const beamOffsetY = connectedProduct.rect.refPoint.y - connectedBeam.leftBottomPoint.y

                        const x = isConnectedToRight
                            ? hostProduct.refPoint.x + connector.start.x + connector.width
                            : hostProduct.refPoint.x - connectedProduct.width + connector.start.x - connector.width
                        const y = connector.leftBottomPoint.y + hostProduct.refPoint.y + connectedProduct.productOffset + beamOffsetY

                        connectedProduct.refPoint = new Point(x, y)
                    } else if (connector.isHorizontal) {
                        const isConnectedToTop = hostBeam === hostProduct.topmostBeam

                        const connectedBeam = isConnectedToTop ? connectedProduct.bottommostBeam : connectedProduct.topmostBeam

                        hostBeam.isConnectionPart = true
                        connectedBeam.isConnectionPart = true

                        const beamOffsetX = connectedProduct.rect.refPoint.x - connectedBeam.leftBottomPoint.x

                        const x = connector.leftBottomPoint.x + hostProduct.refPoint.x + connectedProduct.productOffset + beamOffsetX
                        const y = isConnectedToTop
                            ? hostProduct.refPoint.y + connector.end.y + connector.width
                            : hostProduct.refPoint.y - connectedProduct.height + connector.start.y - connector.width

                        connectedProduct.refPoint = new Point(x, y)
                    }
                }
            })
        })

        this.toCoordinateSystem(CoordinateSystems.Absolute)
    }

    private _updateProductOffsets(): void {
        this.update()

        this.products.forEach(hostProduct => {
            hostProduct.connectors.forEach(connector => {
                const connectedProduct = this.products.find(product => product.productGuid === connector.connectedProductGuid)

                if (!connectedProduct) {
                    return
                }

                const offset = this.getProductsRelativeOffset(hostProduct, connectedProduct)

                this.setProductOffset(connectedProduct, offset)
            })
        })

        this.update()
    }

    private _productToCoordinateSystem(product: Product, coordinateSystem: CoordinateSystems): void {
        const {refPoint} = product
        const {leftBottomPoint} = product.frame.rect
        product.allPoints.forEach(point => {
            point.x += coordinateSystem === CoordinateSystems.Absolute ? refPoint.x : -leftBottomPoint.x
            point.y += coordinateSystem === CoordinateSystems.Absolute ? refPoint.y : -leftBottomPoint.y
        })
        product.update()
    }

    get energyProductTypeName(): string {
        return this.energyProductType ? `Energy ${this.energyProductType}` : ''
    }
}
