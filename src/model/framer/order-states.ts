import {Colors} from '../../constants/colors'

export enum OrderStates {
    Draft = 'draft',
    CalcErrors = 'calc_errors',
    NeedsRecalc = 'needs_recalc',
    AwaitingPayment = 'awaiting_payment',
    Paid = 'paid',
    Booking = 'booking',
    Booked = 'booked',
    Producing = 'producing',
    ProductionError = 'production_error',
    ReadyForPickup = 'ready_for_pickup',
    ProducedShippingToClient = 'produced__shipping_to_client',
    ShippingToClientAddress = 'shipping_to_client_address',
    ProducedShippingToWarehouse = 'produced__shipping_to_warehouse',
    ShippingToWarehouse = 'shipping_to_warehouse',
    AtWarehouseAwaitingShipping = 'at_warehouse_awaiting_shipping',
    AtWarehouseAwaitingPickup = 'at_warehouse_awaiting_pickup',
    Completed = 'completed',
    Cancelled = 'cancelled',
    CancelRequested = 'cancel_requested'
}

export const AllOrderStates = [
    OrderStates.Draft,
    OrderStates.CalcErrors,
    OrderStates.NeedsRecalc,
    OrderStates.AwaitingPayment,
    OrderStates.Paid,
    OrderStates.Booking,
    OrderStates.Booked,
    OrderStates.Producing,
    OrderStates.ProductionError,
    OrderStates.ReadyForPickup,
    OrderStates.ProducedShippingToClient,
    OrderStates.ShippingToClientAddress,
    OrderStates.ProducedShippingToWarehouse,
    OrderStates.ShippingToWarehouse,
    OrderStates.AtWarehouseAwaitingShipping,
    OrderStates.AtWarehouseAwaitingPickup,
    OrderStates.Completed,
    OrderStates.Cancelled,
    OrderStates.CancelRequested
]

export const DraftOrderStates = [OrderStates.Draft, OrderStates.CalcErrors, OrderStates.NeedsRecalc, OrderStates.Booking]

export const ProducingOrderStates = [
    OrderStates.Paid,
    OrderStates.Booked,
    OrderStates.Producing,
    OrderStates.ProductionError,
    OrderStates.ProducedShippingToClient,
    OrderStates.ShippingToClientAddress,
    OrderStates.ProducedShippingToWarehouse,
    OrderStates.ShippingToWarehouse,
    OrderStates.AtWarehouseAwaitingShipping,
    OrderStates.AtWarehouseAwaitingPickup,
    OrderStates.CancelRequested
]

export const getOrderStateName = (orderState: OrderStates): string => {
    switch (orderState) {
        case OrderStates.CalcErrors:
            return 'Ошибки построения'
        case OrderStates.NeedsRecalc:
            return 'Необходим пересчет'
        case OrderStates.AwaitingPayment:
            return 'Ожидает оплаты'
        case OrderStates.Paid:
            return 'Оплачен'
        case OrderStates.Booking:
            return 'Передаем в прозводство'
        case OrderStates.Booked:
            return 'В производстве'
        case OrderStates.Producing:
            return 'В производстве'
        case OrderStates.ProductionError:
            return 'Ошибка производства'
        case OrderStates.ReadyForPickup:
            return 'Готов к отгрузке'
        case OrderStates.ProducedShippingToClient:
            return 'Готов, ожидается доставка'
        case OrderStates.ShippingToClientAddress:
            return 'Доставка по адресу'
        case OrderStates.ProducedShippingToWarehouse:
            return 'Готов, ожидается доставка на склад'
        case OrderStates.ShippingToWarehouse:
            return 'Доставка на склад'
        case OrderStates.AtWarehouseAwaitingShipping:
            return 'На складе, ожидается доставка'
        case OrderStates.AtWarehouseAwaitingPickup:
            return 'На складе, ожидается самовывоз'
        case OrderStates.Completed:
            return 'Завершен'
        case OrderStates.Cancelled:
            return 'Отменен'
        case OrderStates.CancelRequested:
            return 'Запрос отмены'
        default:
            return 'Расчет'
    }
}

export const getOrderStateColor = (orderState: OrderStates): Colors => {
    switch (orderState) {
        case OrderStates.CalcErrors:
            return Colors.Red
        case OrderStates.NeedsRecalc:
            return Colors.LightGray
        case OrderStates.AwaitingPayment:
            return Colors.Orange
        case OrderStates.Paid:
        case OrderStates.Booking:
        case OrderStates.Booked:
        case OrderStates.Producing:
        case OrderStates.ProducedShippingToClient:
        case OrderStates.ShippingToClientAddress:
        case OrderStates.ProducedShippingToWarehouse:
        case OrderStates.ShippingToWarehouse:
        case OrderStates.AtWarehouseAwaitingShipping:
        case OrderStates.AtWarehouseAwaitingPickup:
            return Colors.Blue
        case OrderStates.ProductionError:
            return Colors.Red
        case OrderStates.ReadyForPickup:
            return Colors.Blue
        case OrderStates.Completed:
            return Colors.Green
        case OrderStates.Cancelled:
        case OrderStates.CancelRequested:
            return Colors.Red

        default:
            return Colors.LightGray
    }
}

export const getOrderCardColor = (orderState: OrderStates): Colors => {
    switch (orderState) {
        case OrderStates.CalcErrors:
        case OrderStates.ProductionError:
        case OrderStates.Cancelled:
        case OrderStates.CancelRequested:
            return Colors.Red
        case OrderStates.NeedsRecalc:
            return Colors.LightGrayCard
        case OrderStates.Booked:
        case OrderStates.Producing:
        case OrderStates.ProducedShippingToClient:
        case OrderStates.ShippingToClientAddress:
        case OrderStates.ProducedShippingToWarehouse:
        case OrderStates.ShippingToWarehouse:
        case OrderStates.AtWarehouseAwaitingShipping:
        case OrderStates.AtWarehouseAwaitingPickup:
        case OrderStates.ReadyForPickup:
        case OrderStates.Completed:
            return Colors.GreenCard
        case OrderStates.Paid:
            return Colors.BlueCard
        case OrderStates.Booking:
            return Colors.BlueCard
        default:
            return Colors.LightGrayCard
    }
}
