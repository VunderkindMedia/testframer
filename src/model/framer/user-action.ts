export interface IUserActionJSON {
    t: number
    a: string
}

export class UserAction {
    t: number
    a: string

    constructor(timestamp: number, action: string) {
        this.t = timestamp
        this.a = action
    }
}
