import {MoskitkaColors} from './moskitka-colors'
import {getClone} from '../../helpers/json'

export interface IMoskitkaJSON {
    set: boolean
    color: MoskitkaColors
    catproof: boolean
}

export class Moskitka {
    static parse(moskitkaJSON: IMoskitkaJSON): Moskitka {
        const moskitka = new Moskitka()
        Object.assign(moskitka, moskitkaJSON)
        return moskitka
    }

    set = false
    color = MoskitkaColors.White
    catproof = false

    get clone(): Moskitka {
        return Moskitka.parse(getClone(this))
    }
}
