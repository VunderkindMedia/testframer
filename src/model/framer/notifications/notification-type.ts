/* eslint-disable @typescript-eslint/naming-convention */
export interface INotificationTypeJSON {
    id: number
    Name: string
    Description: string
    Template: string
}

export class NotificationType {
    static parse(notificationTypeJSON: INotificationTypeJSON): NotificationType {
        const {id, Name, Description, Template} = notificationTypeJSON
        const notificationType = new NotificationType(id, Name, Description, Template)

        return notificationType
    }

    constructor(id: number, name: string, description: string, template: string) {
        this.id = id
        this.name = name
        this.description = description
        this.template = template
    }

    id: number
    name: string
    description: string
    template: string
}
