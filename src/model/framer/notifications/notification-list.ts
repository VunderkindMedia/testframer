import {IResultsResponse} from '../../results-response'
import {INotificationJSON, Notification} from './notification'

export type TNotificationList = IResultsResponse<INotificationJSON>

export class NotificationList {
    static parse(notificationListJSON: TNotificationList): NotificationList {
        const notificationList = new NotificationList()

        if (notificationListJSON.results) {
            notificationList.notifications = notificationListJSON.results.map(n => Notification.parse(n))
        }

        notificationList.offset = notificationListJSON.offset || 0
        notificationList.limit = notificationListJSON.limit
        notificationList.totalCount = notificationListJSON.total_count || 0

        return notificationList
    }

    notifications: Notification[] = []
    offset = 0
    limit = 2
    totalCount = 0
}
