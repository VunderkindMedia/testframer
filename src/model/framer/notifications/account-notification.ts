export interface IAccountNotification {
    accountNumber: string
    balance: number
    creditLimitInCents: number
    framerCreditLimitInCents: number
    potentialScore: number
    reservedScore: number
    availableScore: number
    earnedScore: number
    nextDiscount: number
    nextDiscountAmount: number
    framerPoints: number
    showBalance: boolean
}

export class AccountNotification {
    static parse(value: IAccountNotification): AccountNotification {
        const notification = new AccountNotification()
        Object.assign(notification, value)
        return notification
    }

    accountNumber = ''
    balance = 0
    creditLimitInCents = 0
    framerCreditLimitInCents = 0
    potentialScore = 0
    reservedScore = 0
    availableScore = 0
    earnedScore = 0
    nextDiscount = 0
    nextDiscountAmount = 0
    framerPoints = 0
    showBalance = false
}
