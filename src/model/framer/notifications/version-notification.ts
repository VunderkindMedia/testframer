import {AppVersion} from '../../app-version'

export class VersionNotification {
    static parse(value: AppVersion): VersionNotification {
        const notification = new VersionNotification()
        Object.assign(notification, value)
        return notification
    }

    frontendVersion = ''
}
