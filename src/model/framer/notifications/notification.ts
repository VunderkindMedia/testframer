import {INotificationTypeJSON, NotificationType} from './notification-type'
import {IOrderJSON, Order} from '../order'
import {getOrderStateName} from '../order-states'

export interface INotificationJSON {
    id: number
    createdAt: string
    readAt: string
    notificationType: INotificationTypeJSON
    additionalData: IOrderJSON
}

export class Notification {
    static parse(notificationJSON: INotificationJSON): Notification {
        const notification = new Notification()
        Object.assign(notification, notificationJSON)

        if (notificationJSON.notificationType) {
            notification.notificationType = NotificationType.parse(notificationJSON.notificationType)
        }

        if (notificationJSON.additionalData) {
            notification.additionalData = Order.parse(notificationJSON.additionalData)
        }

        return notification
    }

    id = 0
    createdAt = ''
    readAt: string | null = null
    notificationType: NotificationType | null = null
    additionalData: Order | null = null

    get description(): string {
        if (!this.notificationType?.template) {
            return ''
        }

        return this.notificationType.template.replace(/{{\.(\w+)}}/g, (substring: string, ...args: string[]): string => {
            const key = args[0] as keyof Order

            const value = this.additionalData
                ? key === 'state'
                    ? `<span>${getOrderStateName(this.additionalData[key])}</span>`
                    : this.additionalData[key]
                : ''
            return value ? String(value) : ''
        })
    }
}
