export interface IGeometryParametersJSON {
    id: number
    marking: string
    modelPart: number
    a: number
    b: number
    c: number
    d: number
    e: number
    f: number
    g: number
    comment: string
}

export class GeometryParameters {
    static parse(geometryParametersJSON: IGeometryParametersJSON): GeometryParameters {
        return new GeometryParameters(
            geometryParametersJSON.id,
            geometryParametersJSON.marking,
            geometryParametersJSON.modelPart,
            geometryParametersJSON.a,
            geometryParametersJSON.b,
            geometryParametersJSON.c,
            geometryParametersJSON.d,
            geometryParametersJSON.e,
            geometryParametersJSON.f,
            geometryParametersJSON.g,
            geometryParametersJSON.comment
        )
    }

    readonly id: number
    readonly marking: string
    readonly modelPart: number
    readonly a: number
    readonly b: number
    readonly c: number
    readonly d: number
    readonly e: number
    readonly f: number
    readonly g: number
    readonly comment: string

    constructor(
        id: number,
        marking: string,
        modelPart: number,
        a: number,
        b: number,
        c: number,
        d: number,
        e: number,
        f: number,
        g: number,
        comment: string
    ) {
        this.id = id
        this.marking = marking
        this.modelPart = modelPart
        this.a = a
        this.b = b
        this.c = c
        this.d = d
        this.e = e
        this.f = f
        this.g = g
        this.comment = comment
    }
}
