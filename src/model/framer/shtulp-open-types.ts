export enum ShtulpOpenTypes {
    NoShtulp = 0,
    ShtulpOnLeaf = 1,
    NoShtulpOnLeaf = 2
}
