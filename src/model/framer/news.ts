/* eslint-disable @typescript-eslint/naming-convention */
import {DOCS_SERVER} from '../../constants/api'

export interface INewsJSON {
    id: number
    title: string
    content: string
    published_at: string
    images: NewsImage[]
}

export class News {
    static parse(newsJSON: INewsJSON): News {
        const news = new News()
        const {id, title, content, published_at, images} = newsJSON
        news.id = id
        news.title = title
        news.content = content
        news.publishedAt = published_at
        news.images = images.map(i => NewsImage.parse(i))

        return news
    }

    id!: number
    title!: string
    content!: string
    publishedAt!: string
    images!: NewsImage[]
}

interface INewsImageJSON {
    id: number
    alternativeText: string
    url: string
}

class NewsImage {
    static parse(newsImageJSON: INewsImageJSON): NewsImage {
        const newsImage = new NewsImage()
        const {id, alternativeText, url} = newsImageJSON
        newsImage.id = id
        newsImage.alternativeText = alternativeText
        newsImage.url = `${DOCS_SERVER}${url}`

        return newsImage
    }

    id!: number
    alternativeText!: string
    url!: string
}
