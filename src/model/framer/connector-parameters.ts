import {GeometryParameters, IGeometryParametersJSON} from './geometry-parameters'
import {ConnectorTypes} from './connector-types'

export interface IConnectorParametersJSON extends IGeometryParametersJSON {
    isDefault: boolean
    connectorType: ConnectorTypes
}

export class ConnectorParameters extends GeometryParameters {
    static parse(connectorParametersJSON: IConnectorParametersJSON): ConnectorParameters {
        return new ConnectorParameters(
            connectorParametersJSON.id,
            connectorParametersJSON.marking,
            connectorParametersJSON.modelPart,
            connectorParametersJSON.a,
            connectorParametersJSON.b,
            connectorParametersJSON.c,
            connectorParametersJSON.d,
            connectorParametersJSON.e,
            connectorParametersJSON.f,
            connectorParametersJSON.g,
            connectorParametersJSON.comment,
            connectorParametersJSON.isDefault ?? false,
            connectorParametersJSON.connectorType ?? ConnectorTypes.Connector
        )
    }

    isDefault: boolean
    connectorType: ConnectorTypes

    constructor(
        id: number,
        marking: string,
        modelPart: number,
        a: number,
        b: number,
        c: number,
        d: number,
        e: number,
        f: number,
        g: number,
        comment: string,
        isDefault: boolean,
        connectorType: ConnectorTypes
    ) {
        super(id, marking, modelPart, a, b, c, d, e, f, g, comment)
        this.isDefault = isDefault
        this.connectorType = connectorType
    }

    isHType(): boolean {
        return (this.comment.includes('Н-образный') || this.comment.includes('H-образный')) && this.connectorType === ConnectorTypes.Connector
    }
}
