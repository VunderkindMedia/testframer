import {RUB_SYMBOL} from '../../constants/strings'
import {DiscountTypes} from './discount-types'
import {getCurrency} from '../../helpers/currency'

export interface IDiscountJSON {
    percent: number
    fixed: number
}

export class Discount {
    static parse(discountJSON: IDiscountJSON): Discount {
        const discount = new Discount()
        Object.assign(discount, discountJSON)
        return discount
    }

    percent = 0
    fixed = 0

    get discountType(): DiscountTypes {
        if (this.percent !== 0) {
            return DiscountTypes.Percentage
        }
        return DiscountTypes.Fixed
    }

    toString(): string {
        if (this.percent !== 0) {
            return `${this.percent}%`
        }
        return `${getCurrency(this.fixed)} ${RUB_SYMBOL}`
    }
}
