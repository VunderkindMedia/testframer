import {Coordinates, ICoordinatesJSON} from '../coordinates'

export interface IWarehouseJSON {
    id: number
    name: string
    coordinates: ICoordinatesJSON
}

export class Warehouse {
    static parse(warehouseJSON: IWarehouseJSON): Warehouse {
        return new Warehouse(warehouseJSON.id, warehouseJSON.name, Coordinates.parse(warehouseJSON.coordinates))
    }

    id: number
    name: string
    coordinates: Coordinates

    constructor(id: number, name: string, coordinates: Coordinates) {
        this.id = id
        this.name = name
        this.coordinates = coordinates
    }
}
