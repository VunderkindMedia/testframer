import {GoodUnit, IGoodUnitJSON} from './good-unit'
import {getClone} from '../../helpers/json'

export interface IConstructionCostJSON {
    id: number
    installationId: number
    unit: IGoodUnitJSON
    pricePerUnitForOffer: number
    numPos: number
}

export class ConstructionCost {
    static parse(constructionCostJSON: IConstructionCostJSON): ConstructionCost {
        const {id, installationId, unit, pricePerUnitForOffer, numPos} = constructionCostJSON
        return new ConstructionCost(id, installationId, GoodUnit.parse(unit), pricePerUnitForOffer, numPos)
    }

    id: number
    installationId: number
    unit: GoodUnit
    pricePerUnitForOffer: number
    numPos: number

    constructor(id: number, installationId: number, unit: GoodUnit, pricePerUnitForOffer: number, numPos: number) {
        this.id = id
        this.installationId = installationId
        this.unit = unit
        this.pricePerUnitForOffer = pricePerUnitForOffer
        this.numPos = numPos
    }

    get clone(): ConstructionCost {
        return ConstructionCost.parse(getClone(this))
    }
}
