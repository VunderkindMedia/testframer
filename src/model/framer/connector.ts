import {Beam, IBeamJSON} from './beam'
import {Point} from '../geometry/point'
import {ConnectorTypes} from './connector-types'
import {ModelParts} from './model-parts'
import {IContext} from '../i-context'

export interface IConnectorJSON extends IBeamJSON {
    connectedBeamGuid: string
    connectedProductGuid?: string
    // eslint-disable-next-line @typescript-eslint/naming-convention
    _connectorType?: ConnectorTypes
}

export class Connector extends Beam implements IContext {
    static parse(connectorJSON: IConnectorJSON, parentNodeId: string): Connector {
        const connector = new Connector()
        Object.assign(connector, connectorJSON)
        connector._parentNodeId = parentNodeId
        connector.start = Point.parse(connectorJSON.start)
        connector.end = Point.parse(connectorJSON.end)
        connector.normalizePointsOrder()
        return connector
    }

    static get Builder(): Builder {
        return new Builder()
    }

    connectedBeamGuid!: string // укзаывает на родительскую балку
    connectedProductGuid?: string
    modelPart = ModelParts.Connector
    private _connectorType = ConnectorTypes.Connector

    constructor() {
        super()
    }

    set connectorType(connectorType: ConnectorTypes) {
        this._connectorType = connectorType
    }

    get connectorType(): ConnectorTypes {
        return this._connectorType
    }
}

class Builder {
    private _connector = new Connector()

    withStart(start: Point): Builder {
        this._connector.start = start
        return this
    }

    withEnd(end: Point): Builder {
        this._connector.end = end
        return this
    }

    withProfileVendorCode(profileVendorCode: string): Builder {
        this._connector.profileVendorCode = profileVendorCode
        return this
    }

    withWidth(width: number): Builder {
        this._connector.width = width
        return this
    }

    withHostBeamGuid(hostBeamGuid: string): Builder {
        this._connector.connectedBeamGuid = hostBeamGuid
        return this
    }

    withConnectedProductGuid(connectedProductGuid?: string): Builder {
        this._connector.connectedProductGuid = connectedProductGuid
        return this
    }

    withConnectorType(connectorType: ConnectorTypes): Builder {
        this._connector.connectorType = connectorType
        return this
    }

    build(): Connector {
        return this._connector
    }
}
