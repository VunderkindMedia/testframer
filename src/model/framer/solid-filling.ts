import {IContext} from '../i-context'
import {IPointJSON, Point} from '../geometry/point'
import {FrameFilling} from './frame-filling'
import {Polygon} from '../geometry/polygon'
import {Rectangle} from '../geometry/rectangle'
import {Filling, IFillingJSON} from './filling'
import {CircularLinkedList} from '../circular-linked-list'
import {BEAD_WIDTH} from '../../constants/geometry-params'
import {IUserParameterJSON, UserParameter} from './user-parameter/user-parameter'
import {DEFAULT_GLASS_COLOR} from '../../constants/default-user-parameters'
import {UserParameterTypes} from './user-parameter/user-parameter-types'
import {IFillingSizeJSON, FillingSize} from './filling-size'
import {getClone} from '../../helpers/json'
import {GUID} from '../../helpers/guid'
import {ISolidFillingPart, SolidFillingPartType} from './filling-formula'
import {TINT_FILM_ITEMS} from '../../constants/filling-settings'

export interface ISolidFillingJSON {
    solidFillingGuid?: string
    fillingVendorCode: string
    fillingPoint: IPointJSON
    fillingSize?: IFillingSizeJSON
    filling?: IFillingJSON
    userParameters?: IUserParameterJSON[]
    hardcodedUserParameters?: IUserParameterJSON[]
}

export class SolidFilling implements IContext {
    static parse(solidFillingJSON: ISolidFillingJSON, parentNodeId: string): SolidFilling {
        const solidFilling = new SolidFilling()
        Object.assign(solidFilling, solidFillingJSON)
        solidFilling._parentNodeId = parentNodeId
        solidFilling.fillingPoint = Point.parse(solidFillingJSON.fillingPoint)
        solidFillingJSON.filling && (solidFilling.filling = Filling.parse(solidFillingJSON.filling))
        solidFillingJSON.userParameters && (solidFilling.userParameters = solidFillingJSON.userParameters.map(u => UserParameter.parse(u)))
        solidFillingJSON.hardcodedUserParameters &&
            (solidFilling.hardcodedUserParameters = solidFillingJSON.hardcodedUserParameters.map(u => UserParameter.parse(u)))
        solidFillingJSON.fillingSize && (solidFilling.fillingSize = FillingSize.parse(solidFillingJSON.fillingSize))
        return solidFilling
    }

    clone(): SolidFilling {
        return SolidFilling.parse(getClone(this), this.parentNodeId)
    }

    static buildFromFrameFilling(frameFilling: FrameFilling): SolidFilling {
        const {nodeId, parentNodeId, fillingPoint} = frameFilling

        if (!parentNodeId) {
            throw new Error('Отсутсвует parentNodeId')
        }

        const fillingVendorCode = frameFilling.innerFillings[0]?.solid ? frameFilling.innerFillings[0].solid.fillingVendorCode : null

        if (!fillingVendorCode) {
            throw new Error('Нет возможности задать fillingVendorCode')
        }

        const solidFilling = new SolidFilling()

        solidFilling.nodeId = nodeId
        solidFilling.parentNodeId = parentNodeId
        solidFilling.fillingVendorCode = fillingVendorCode
        solidFilling.fillingPoint = fillingPoint

        return solidFilling
    }

    solidFillingGuid = GUID()
    fillingVendorCode!: string
    fillingPoint!: Point
    filling?: Filling
    fillingSize?: FillingSize
    userParameters: UserParameter[] = [DEFAULT_GLASS_COLOR]
    hardcodedUserParameters: UserParameter[] = []
    private _parentNodeId!: string
    private _containerPolygon: Polygon = new Polygon() // полигон который определяет заполенние

    set containerPolygon(polygon: Polygon) {
        this._containerPolygon = polygon
        this.fillingPoint = polygon.center
    }

    get containerPolygon(): Polygon {
        return this._containerPolygon
    }

    get rect(): Rectangle {
        return this.containerPolygon.rect
    }

    get nodeId(): string {
        return this.solidFillingGuid
    }

    set nodeId(nodeId: string) {
        this.solidFillingGuid = nodeId
    }

    get parentNodeId(): string {
        return this._parentNodeId
    }

    set parentNodeId(parentNodeId: string) {
        this._parentNodeId = parentNodeId
    }

    get beads(): Polygon[] {
        const beads: Polygon[] = []

        new CircularLinkedList(this.containerPolygon.innerPolygon.segments).items.forEach(item => {
            const currentLine = item.data
            const prevLine = item.prev
            const nextLine = item.next

            const currentBeamParallel = currentLine.getParallels(BEAD_WIDTH)[0]
            const prevBeamParallel = prevLine.getParallels(BEAD_WIDTH)[0]
            const nextBeamParallel = nextLine.getParallels(BEAD_WIDTH)[0]

            const point1 = currentBeamParallel.findIntersectionPoint(nextBeamParallel)
            const point2 = currentBeamParallel.findIntersectionPoint(prevBeamParallel)

            if (!point1 || !point2) {
                return
            }

            beads.push(Polygon.buildFromPoints([currentLine.start, currentLine.end, point1, point2]))
        })

        return beads
    }

    get glassColor(): string {
        const colorParam = this.userParameters.find(p => p.id === UserParameterTypes.GlassColor)
        return colorParam?.value ?? 'Нет'
    }

    set glassColor(color: string) {
        let colorParam = this.userParameters.find(p => p.id === UserParameterTypes.GlassColor)
        if (colorParam) {
            colorParam.value = color
        } else {
            colorParam = new UserParameter(UserParameterTypes.GlassColor, color, '', '')
            this.userParameters.push(colorParam)
        }
    }

    resetColorParams(): void {
        const colorParam = this.userParameters.find(p => p.id === UserParameterTypes.GlassColor)
        this.userParameters = colorParam ? [colorParam] : []
    }

    get formula(): ISolidFillingPart[] {
        return this.fillingVendorCode.split('-').map((value, id) => {
            const tintFilmRegEx = /^(.+)\[(.+)\]/gm
            const someRegTintFilm = tintFilmRegEx.exec(value)
            const formulaObj: ISolidFillingPart = {
                type: id % 2 ? SolidFillingPartType.Frame : SolidFillingPartType.Glass,
                value: someRegTintFilm ? someRegTintFilm[1] : value,
                id,
                tintFilm: someRegTintFilm ? someRegTintFilm[2] : TINT_FILM_ITEMS[0].id
            }
            if (formulaObj.type === SolidFillingPartType.Frame) {
                const re = /(\d+)(ar)*_*(pvc)*/i
                const res = re.exec(formulaObj.value)
                if (res) {
                    formulaObj.value = res[1]
                    formulaObj.hasArgon = !!res[2]
                    formulaObj.hasPvc = !!res[3]
                }
            }
            return formulaObj
        })
    }
}
