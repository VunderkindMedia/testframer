export enum DiscountTypes {
    Fixed = 'fixed',
    Percentage = 'percentage'
}
