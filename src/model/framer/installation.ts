import {ConstructionCost, IConstructionCostJSON} from './construction-cost'

export interface IInstallationJSON {
    id: number
    partnerId: number
    name: string
    constructionsCosts: IConstructionCostJSON[]
}

export class Installation {
    static parse(installationJSON: IInstallationJSON): Installation {
        const {id, partnerId, name, constructionsCosts} = installationJSON
        const costList = constructionsCosts.map(item => ConstructionCost.parse(item))
        return new Installation(id, partnerId, name, costList)
    }

    id: number
    partnerId: number
    name: string
    constructionsCosts: ConstructionCost[]

    constructor(id: number, partnerId: number, name: string, constructionsCosts: ConstructionCost[]) {
        this.id = id
        this.partnerId = partnerId
        this.name = name
        this.constructionsCosts = constructionsCosts
    }
}
