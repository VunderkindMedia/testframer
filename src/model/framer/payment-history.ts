import {IResultsResponse} from '../results-response'
import {PaymentState, IPaymentStateJSON} from './payment-state'

export type TPaymentHistoryJSON = IResultsResponse<IPaymentStateJSON>

export class PaymentHistory {
    static parse(paymentHistoryJSON: TPaymentHistoryJSON): PaymentHistory {
        const response = new PaymentHistory()

        paymentHistoryJSON.results &&
            paymentHistoryJSON.results.forEach(obj => {
                try {
                    const payment = PaymentState.parse(obj)
                    response.payments.push(payment)
                } catch (e) {
                    console.error(e)
                    console.warn(`Payment ${obj.id} skipped`)
                }
            })

        response.offset = paymentHistoryJSON.offset || 0
        response.limit = paymentHistoryJSON.limit
        response.totalCount = paymentHistoryJSON.total_count || 0

        return response
    }

    payments: PaymentState[] = []
    offset = 0
    limit = 2
    totalCount = 0
}
