interface IFillingSizeParams {
    x: number
    y: number
}

export class FillingSizeParams {
    static parse(fillingSizeParams: IFillingSizeParams): FillingSizeParams {
        const params = new FillingSizeParams()
        Object.assign(params, fillingSizeParams)
        return params
    }

    x!: number
    y!: number
}

export interface IFillingSizeJSON {
    size: IFillingSizeParams
    bevels: IFillingSizeParams[]
}

export class FillingSize {
    static parse(fillingSizeJSON: IFillingSizeJSON): FillingSize {
        const bevels = fillingSizeJSON.bevels ? fillingSizeJSON.bevels.map(b => FillingSizeParams.parse(b)) : []
        return new FillingSize(FillingSizeParams.parse(fillingSizeJSON.size), bevels)
    }

    constructor(size: IFillingSizeParams, bevels: IFillingSizeParams[]) {
        this.size = size
        this.bevels = bevels
    }

    size: IFillingSizeParams
    bevels: IFillingSizeParams[]
}
