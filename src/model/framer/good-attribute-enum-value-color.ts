export interface IGoodAttributeEnumValueColorJSON {
    name: string
    value: number
    hexColor: string
}

export class GoodAttributeEnumValueColor implements IGoodAttributeEnumValueColorJSON {
    static parse(goodAttributeEnumValueColorJSON: IGoodAttributeEnumValueColorJSON): GoodAttributeEnumValueColor {
        return new GoodAttributeEnumValueColor(
            goodAttributeEnumValueColorJSON.name,
            goodAttributeEnumValueColorJSON.value,
            goodAttributeEnumValueColorJSON.hexColor
        )
    }

    name: string
    value: number
    hexColor: string

    constructor(name: string, value: number, hexColor: string) {
        this.name = name
        this.value = value
        this.hexColor = hexColor
    }

    equals(enumValueColor: GoodAttributeEnumValueColor): boolean {
        return this.name === enumValueColor.name && this.value === enumValueColor.value && this.hexColor === enumValueColor.hexColor
    }
}
