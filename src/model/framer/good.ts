import {GoodAttribute, IGoodAttributeJSON} from './good-attribute'
import {GoodUnit, IGoodUnitJSON} from './good-unit'
import {GoodSources} from './good-sources'
import {Order} from './order'
import {getClone} from '../../helpers/json'

export interface IGoodJSON {
    id: number
    orderId: number
    constructionId: string | null
    goodId: number
    unitId: number
    goodGroupId: number
    colorGroupPriceId: number
    sku: string
    name: string
    unit: IGoodUnitJSON
    pricePerUnit: number
    pricePerUnitToDisplay: number
    attributes: IGoodAttributeJSON[]
    costPerItem: number
    quantity: number
    cost: number
    amegaCost: number
    amegaCostToDisplay: number
    goodSource: GoodSources
    // eslint-disable-next-line @typescript-eslint/naming-convention
    _changed?: boolean
}

export class Good {
    static parse(goodJSON: IGoodJSON): Good {
        const good = new Good(
            goodJSON.id,
            goodJSON.orderId,
            goodJSON.constructionId,
            goodJSON.goodId,
            goodJSON.unitId,
            goodJSON.goodGroupId,
            goodJSON.colorGroupPriceId,
            goodJSON.sku,
            goodJSON.name,
            GoodUnit.parse(goodJSON.unit),
            goodJSON.pricePerUnit,
            goodJSON.pricePerUnitToDisplay,
            (goodJSON.attributes ?? []).map(a => GoodAttribute.parse(a)),
            goodJSON.costPerItem,
            goodJSON.quantity,
            goodJSON.cost,
            goodJSON.amegaCost,
            goodJSON.amegaCostToDisplay,
            goodJSON.goodSource
        )
        good.changed = goodJSON._changed ?? false
        return good
    }

    id: number
    orderId: number
    constructionId: string | null
    goodId: number
    unitId: number
    goodGroupId: number
    colorGroupPriceId: number
    sku: string
    name: string
    unit: GoodUnit
    pricePerUnit: number
    pricePerUnitToDisplay: number
    attributes: GoodAttribute[]
    costPerItem: number
    quantity: number
    cost: number
    amegaCost: number
    amegaCostToDisplay: number
    goodSource: GoodSources
    private _changed = false
    private _order: Order | null = null

    constructor(
        id: number,
        orderId: number,
        constructionId: string | null,
        goodId: number,
        unitId: number,
        goodGroupId: number,
        colorGroupPriceId: number,
        sku: string,
        name: string,
        unit: GoodUnit,
        pricePerUnit: number,
        pricePerUnitToDisplay: number,
        attributes: GoodAttribute[],
        costPerItem: number,
        quantity: number,
        cost: number,
        amegaCost: number,
        amegaCostToDisplay: number,
        goodSource: number
    ) {
        this.id = id
        this.orderId = orderId
        this.constructionId = constructionId
        this.goodId = goodId
        this.unitId = unitId
        this.goodGroupId = goodGroupId
        this.colorGroupPriceId = colorGroupPriceId
        this.sku = sku
        this.name = name
        this.unit = unit
        this.pricePerUnit = pricePerUnit
        this.pricePerUnitToDisplay = pricePerUnitToDisplay
        this.attributes = attributes
        this.costPerItem = costPerItem
        this.quantity = quantity
        this.cost = cost
        this.amegaCost = amegaCost
        this.amegaCostToDisplay = amegaCostToDisplay
        this.goodSource = goodSource
    }

    get clone(): Good {
        return Good.parse(getClone(this))
    }

    get order(): Order | null {
        return this._order
    }

    set order(order: Order | null) {
        this._order = order
    }

    set changed(changed: boolean) {
        this._changed = changed
    }

    get changed(): boolean {
        return this._changed
    }

    toString(): string {
        return `Доп. материал ${this.name} (id = ${this.id}; goodId = ${this.goodId}; constructionId = ${
            this.constructionId ?? ''
        }; quantity = ${this.quantity}; ${this.attributes.map(a => `${a.caption} = ${a.visibleValue}`).join('; ')})`
    }
}
