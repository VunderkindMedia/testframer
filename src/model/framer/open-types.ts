export enum OpenTypes {
    Gluhoe, // Глухое
    Povorotnaya, // Поворотная
    PovorotnoOtkidnaya, // Поворотно-откидная
    Otkidnaya, // Откидная
    Podvesnaya, // Подвесная
    Razdvizhnaya, // Раздвижная
    Mayatnikovaya, // Маятниковая
    SrednePodvesnaya, // Средне-подвесная
    PodemnoSdvizhnaya, // Подъемно-сдвижная
    Skladnaya, // Складная
    PodemnoPovorotnaya // Подъемно-поворотная
}
