import {IOrderInfoJSON, OrderInfo} from './order-info'
import {CalcError, ICalcErrorJSON} from './calc-error'
import {IProductResultJSON, ProductResult} from './product-result'

export interface ICalcResultJSON {
    orderInfo: IOrderInfoJSON
    calcErrors: ICalcErrorJSON[]
    productResults: IProductResultJSON[]
    exceptionMessage?: string
    // eslint-disable-next-line @typescript-eslint/naming-convention
    BookingDates: string
    timeoutError: string
}

export class CalcResult {
    static parse(calcResultJSON: ICalcResultJSON): CalcResult {
        const calcResult = new CalcResult()
        Object.assign(calcResult, calcResultJSON)

        calcResultJSON.orderInfo && (calcResult.orderInfo = OrderInfo.parse(calcResultJSON.orderInfo))
        calcResultJSON.calcErrors && (calcResult.calcErrors = calcResultJSON.calcErrors.map(calcErrorObj => CalcError.parse(calcErrorObj)))
        calcResultJSON.productResults &&
            (calcResult.productResults = calcResultJSON.productResults.map(productResultObj => ProductResult.parse(productResultObj)))

        return calcResult
    }

    orderInfo!: OrderInfo
    calcErrors: CalcError[] = []
    exceptionMessage?: string
    productResults: ProductResult[] = []
    // eslint-disable-next-line @typescript-eslint/naming-convention
    BookingDates!: string
    timeoutError!: string
}
