import {Mounting, IMountingJSON} from './mounting'

export interface IMountingsResponseJSON {
    offset: number
    limit: number
    // eslint-disable-next-line @typescript-eslint/naming-convention
    total_count: number
    results?: IMountingJSON[]
}

export class MountingsResponse {
    static parse(mountingsResponseJSON: IMountingsResponseJSON): MountingsResponse {
        const response = new MountingsResponse()

        if (!mountingsResponseJSON.results) {
            return response
        }

        mountingsResponseJSON.results.forEach(mountingObj => {
            try {
                const mounting = Mounting.parse(mountingObj)
                response.mountings.push(mounting)
            } catch (e) {
                console.error(e)
                console.warn(`Mounting ${mountingObj.id} skipped`)
            }
        })

        response.offset = mountingsResponseJSON.offset
        response.limit = mountingsResponseJSON.limit
        response.totalCount = mountingsResponseJSON.total_count

        return response
    }

    offset = 0
    limit = 0
    totalCount = 0
    mountings: Mounting[] = []
}
