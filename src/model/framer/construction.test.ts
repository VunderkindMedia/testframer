import {
    TEST_CONNECTORS_PARAMETERS_TEMPLATES,
    TEST_EMPTY_PROFILE_TEMPLATE,
    TEST_FURNITURE_TEMPLATE,
    TEST_MOSKITKA_PROFILE_TEMPLATE,
    TEST_PROFILE_TEMPLATE
} from '../../constants/default-templates'
import {ODN_1, ODN_2} from '../../constants/construction-templates/group-odn'
import {OK_ST_1, OK_ST_2, OK_ST_3, OK_ST_5, OK_ST_6, OK_ST_7, OK_ST_8, OK_ST_9} from '../../constants/construction-templates/group-ok-st'
import {OK_STER_1, OK_STER_2, OK_STER_3, OK_STER_4, OK_STER_5} from '../../constants/construction-templates/group-ok-ster'
import {B_DV_1, B_DV_1_1, B_DV_4, B_DV_5} from '../../constants/construction-templates/group-b-dv'
import {
    OB_MN_1,
    OB_MN_10,
    OB_MN_11,
    OB_MN_12,
    OB_MN_14,
    OB_MN_15,
    OB_MN_16,
    OB_MN_17,
    OB_MN_2,
    OB_MN_3,
    OB_MN_4,
    OB_MN_9
} from '../../constants/construction-templates/group-ob-mn'
import {
    LODZH_PVH_01,
    LODZH_PVH_02,
    LODZH_PVH_03,
    LODZH_PVH_04,
    LODZH_PVH_05,
    LODZH_PVH_06,
    LODZH_PVH_07,
    LODZH_PVH_09,
    LODZH_PVH_10,
    LODZH_PVH_14,
    LODZH_PVH_15
} from '../../constants/construction-templates/group-lodzh-pvh'
import {B_B_1, B_B_2, B_B_3, B_B_4, B_B_5, B_B_6, B_B_7} from '../../constants/construction-templates/group-b-b'
import {PMS_BD, PMS_OK, PMS_OKX2} from '../../constants/construction-templates/group-moskitka'
import {Construction, IConstructionJSON} from './construction'
import {CoordinateSystems} from '../coordinate-systems'
import {Beam} from './beam'
import {ConnectorParameters, IConnectorParametersJSON} from './connector-parameters'
import {ConnectorTypes} from './connector-types'
import {Sides} from '../sides'
import {Size} from '../size'
import {Point} from '../geometry/point'
import {Polygon} from '../geometry/polygon'
import {OC_4} from '../../constants/construction-templates/group-fillings'
import {ProductTypes} from './product-types'

const parseConstruction = (template: any): Construction => {
    template.products.forEach((productObj: any) => {
        if (productObj.productionTypeId === ProductTypes.Steklopaket) {
            productObj.profile = TEST_EMPTY_PROFILE_TEMPLATE
        } else if (productObj.productionTypeId === ProductTypes.Moskitka) {
            productObj.profile = TEST_MOSKITKA_PROFILE_TEMPLATE
        } else {
            productObj.profile = TEST_PROFILE_TEMPLATE
        }

        productObj.furniture = TEST_FURNITURE_TEMPLATE
    })
    return Construction.parse(template)
}

test('Тест на то что после конвертации в относительные координаты у нас не появляется координат меньше 0', () => {
    const constructionsTemplates = [
        ODN_1,
        ODN_2,
        OK_ST_1,
        OK_ST_2,
        OK_ST_3,
        OK_ST_5,
        OK_ST_6,
        OK_ST_7,
        OK_ST_8,
        OK_ST_9,
        OK_STER_1,
        OK_STER_2,
        OK_STER_3,
        OK_STER_4,
        OK_STER_5,
        B_DV_1,
        B_DV_1_1,
        B_DV_4,
        B_DV_5,
        OB_MN_1,
        OB_MN_10,
        OB_MN_11,
        OB_MN_12,
        OB_MN_14,
        OB_MN_15,
        OB_MN_16,
        OB_MN_17,
        OB_MN_2,
        OB_MN_3,
        OB_MN_4,
        OB_MN_9,
        LODZH_PVH_01,
        LODZH_PVH_02,
        LODZH_PVH_03,
        LODZH_PVH_04,
        LODZH_PVH_05,
        LODZH_PVH_06,
        LODZH_PVH_07,
        LODZH_PVH_09,
        LODZH_PVH_10,
        LODZH_PVH_14,
        LODZH_PVH_15,
        B_B_1,
        B_B_2,
        B_B_3,
        B_B_4,
        B_B_5,
        B_B_6,
        B_B_7,
        PMS_BD,
        PMS_OK,
        PMS_OKX2
    ]

    constructionsTemplates.forEach(template => {
        template.products.forEach((productObj: any) => {
            productObj.profile = TEST_PROFILE_TEMPLATE
            productObj.furniture = TEST_FURNITURE_TEMPLATE
        })

        const construction = Construction.parse(template as IConstructionJSON)
        construction.toCoordinateSystem(CoordinateSystems.Relative)
        construction.allPoints.forEach(point => {
            expect(point.x).toBeGreaterThanOrEqual(0)
            expect(point.y).toBeGreaterThanOrEqual(0)
        })
    })
})

test('Тест на выравнивание импоста в балконном блоке', () => {
    const BB1 = parseConstruction(B_B_1)
    const BB2 = parseConstruction(B_B_2)
    const BB3 = parseConstruction(B_B_3)
    const BB4 = parseConstruction(B_B_4)
    const BB5 = parseConstruction(B_B_5)
    const BB6 = parseConstruction(B_B_6)
    const BB7 = parseConstruction(B_B_7)

    const test = (construction: Construction, bottommostBeam: Beam | undefined): void => {
        expect(bottommostBeam).not.toBeUndefined()
        expect(construction.products.length).toBeGreaterThanOrEqual(2)
        construction.alignDoorImpost()

        const door = construction.products.find(product => product.isDoor)
        expect(door).not.toBeUndefined()

        const window = construction.products.find(product => !product.isDoor)
        expect(window).not.toBeUndefined()

        if (!door || !window) {
            return
        }

        const doorImpost = door.frame.innerFillings[0].leaf?.impostBeams[0]
        expect(doorImpost).not.toBeUndefined()

        if (!doorImpost || !bottommostBeam) {
            return
        }

        expect(doorImpost.start.y + doorImpost.width / 2).toBe(bottommostBeam.start.y + bottommostBeam.width)
    }

    test(BB1, BB1.rightmostProduct.bottommostBeam)
    test(BB2, BB2.leftmostProduct.bottommostBeam)
    test(BB3, BB3.rightmostProduct.bottommostBeam)
    test(BB4, BB4.leftmostProduct.bottommostBeam)
    test(BB5, BB5.rightmostProduct.frame.innerFillings.find(i => Boolean(i.leaf))?.leaf?.bottommostBeam)
    test(BB6, BB6.leftmostProduct.frame.innerFillings.find(i => Boolean(i.leaf))?.leaf?.bottommostBeam)
    test(BB7, BB7.rightmostProduct.bottommostBeam)
})

test('Тест на добавление расширителей свреху/снизу', () => {
    const connectorsParameters = TEST_CONNECTORS_PARAMETERS_TEMPLATES.map(template =>
        ConnectorParameters.parse(template as IConnectorParametersJSON)
    )

    const extender = connectorsParameters.find(cp => cp.connectorType === ConnectorTypes.Extender)
    expect(extender).not.toBeUndefined()

    if (!extender) {
        return
    }

    const testExtenders = (construction: Construction, connectingSide: Sides.Top | Sides.Bottom, expectedTotalExtendersCount: number): void => {
        expect(construction.products.length).toBeGreaterThanOrEqual(2)
        construction.connectorsParameters = connectorsParameters

        const firstProduct = construction.products[0]

        construction.addConnector(firstProduct, connectingSide, extender)

        const installedExtender = firstProduct.connectors.find(c => {
            if (!c.isHorizontal) {
                return false
            }
            return c.start.y === (connectingSide === Sides.Top ? firstProduct.topmostBeam.start.y : firstProduct.bottommostBeam.start.y)
        })
        expect(installedExtender).not.toBeUndefined()

        if (!installedExtender) {
            return
        }

        const extenders = construction.allConnectors.filter(c => c.isHorizontal && c.start.y === installedExtender.start.y)
        expect(extenders.length).toBe(expectedTotalExtendersCount)

        const verticalConnectors = construction.allConnectors.filter(c => c.isVertical)

        verticalConnectors.forEach(connector => {
            connector.normalizePointsOrder()
            if (connectingSide === Sides.Top) {
                expect(connector.end.y).toBe(installedExtender.start.y + installedExtender.width)
            } else {
                expect(connector.start.y).toBe(installedExtender.start.y - installedExtender.width)
            }
        })
    }

    testExtenders(parseConstruction(B_B_1), Sides.Top, 2)
    testExtenders(parseConstruction(B_B_2), Sides.Top, 2)
    testExtenders(parseConstruction(B_B_7), Sides.Top, 3)
    testExtenders(parseConstruction(LODZH_PVH_04), Sides.Top, 4)

    testExtenders(parseConstruction(LODZH_PVH_01), Sides.Bottom, 2)
    testExtenders(parseConstruction(LODZH_PVH_03), Sides.Bottom, 3)
    testExtenders(parseConstruction(LODZH_PVH_04), Sides.Bottom, 4)
})

test('Тест на сдвиги изделия по различным осям', () => {
    const testVertical = (
        construction: Construction,
        offset: number,
        expectedProductBottomY: number,
        expectedProductOffset: number,
        expectedConnectorBottomY: number,
        expectedConnectorLength: number
    ) => {
        expect(construction.products.length).toBeGreaterThanOrEqual(2)

        construction.setProductOffset(construction.products[1], offset)

        const secondProduct = construction.products[1]
        const connector = construction.products[0].connectors.find(c => c.connectedProductGuid === secondProduct.productGuid)!

        expect(secondProduct.frame.bottommostBeam.start.y).toBe(expectedProductBottomY)
        expect(secondProduct.productOffset).toBe(expectedProductOffset)

        expect(connector.start.y).toBe(expectedConnectorBottomY)
        expect(connector.length).toBe(expectedConnectorLength)
    }

    const BB1 = parseConstruction(B_B_1)
    testVertical(BB1, 100, 800, 0, 800, 1300)
    testVertical(BB1, -100, 600, 0, 600, 1400)
    testVertical(BB1, -700, 0, 0, 0, 1400)
    testVertical(BB1, -1000, -300, -300, 0, 1100)

    const connectorsParameters = TEST_CONNECTORS_PARAMETERS_TEMPLATES.map(template => ConnectorParameters.parse(template))

    const TEST_CONSTRUCTION_1 = parseConstruction(ODN_1)
    TEST_CONSTRUCTION_1.connectorsParameters = connectorsParameters
    TEST_CONSTRUCTION_1.connectProduct(TEST_CONSTRUCTION_1.products[0], TEST_CONSTRUCTION_1.products[0].clone, Sides.Right)

    const extender = connectorsParameters.find(cp => cp.connectorType === ConnectorTypes.Extender && cp.a === 120)
    expect(extender).not.toBeUndefined()

    if (!extender) {
        return
    }

    TEST_CONSTRUCTION_1.addConnector(TEST_CONSTRUCTION_1.products[0], Sides.Bottom, extender)
    testVertical(TEST_CONSTRUCTION_1, 0, 0, 120, -120, 1520)
    testVertical(TEST_CONSTRUCTION_1, 100, 100, 120, -20, 1420)

    TEST_CONSTRUCTION_1.addConnector(TEST_CONSTRUCTION_1.products[0], Sides.Top, extender)
    testVertical(TEST_CONSTRUCTION_1, 100, 220, 120, 100, 1420)

    const testHorizontal = (
        construction: Construction,
        offset: number,
        expectedProductLeftX: number,
        expectedProductOffset: number,
        expectedConnectorLeftX: number,
        expectedConnectorLength: number
    ) => {
        expect(construction.products.length).toBeGreaterThanOrEqual(2)

        construction.setProductOffset(construction.products[1], offset)

        const secondProduct = construction.products[1]
        const connector = construction.products[0].connectors.find(c => c.connectedProductGuid === secondProduct.productGuid)!

        expect(secondProduct.frame.leftmostBeam.start.x).toBe(expectedProductLeftX)
        expect(secondProduct.productOffset).toBe(expectedProductOffset)

        expect(connector.start.x).toBe(expectedConnectorLeftX)
        expect(connector.length).toBe(expectedConnectorLength)
    }

    const TEST_CONSTRUCTION_2 = parseConstruction(ODN_1)
    TEST_CONSTRUCTION_2.connectorsParameters = connectorsParameters
    TEST_CONSTRUCTION_2.connectProduct(TEST_CONSTRUCTION_2.products[0], TEST_CONSTRUCTION_2.products[0].clone, Sides.Bottom)

    testHorizontal(TEST_CONSTRUCTION_2, 100, -100, -100, 0, 800)
    testHorizontal(TEST_CONSTRUCTION_2, -100, 100, 0, 100, 800)
    testHorizontal(TEST_CONSTRUCTION_2, 0, 0, 0, 0, 900)
})

test('Тест на изменение размеров изделий', () => {
    const constructionResize = (construction: Construction, newSize: Size): Construction => {
        const constructionClone = construction.clone

        const dimensionLines = constructionClone.getDimensionLines()
        const widthDimensionLine = dimensionLines
            .slice(0)
            .filter(d => d.isHorizontal)
            .sort()[0]
        const heightDimensionLine = dimensionLines
            .slice(0)
            .filter(d => d.isVertical)
            .sort()[0]

        constructionClone.changeSizesAndOffsets(widthDimensionLine, newSize.width)
        constructionClone.changeSizesAndOffsets(heightDimensionLine, newSize.height)

        return constructionClone
    }

    const testResize = (construction: Construction) => {
        const ADDITIONAL_WIDTH = 100
        const ADDITIONAL_HEIGHT = 200

        const {width: startWidth, height: startHeight} = construction.size

        const newWidth = startWidth + ADDITIONAL_WIDTH
        const newHeight = startHeight + ADDITIONAL_HEIGHT

        const firstResizedConstruction = constructionResize(construction, new Size(newWidth, newHeight))

        expect(firstResizedConstruction.width).toBe(newWidth)
        expect(firstResizedConstruction.height).toBe(newHeight)

        const secondResizedConstruction = constructionResize(firstResizedConstruction, new Size(startWidth, startHeight))

        expect(secondResizedConstruction.width).toBe(startWidth)
        expect(secondResizedConstruction.height).toBe(startHeight)

        const originalDimensionLines = construction.getDimensionLines()
        const finalDimensionLines = secondResizedConstruction.getDimensionLines()

        originalDimensionLines.forEach((originalDimensionLine, idx) => {
            const finalDimensionLine = finalDimensionLines[idx]
            expect(originalDimensionLine.equals(finalDimensionLine)).toBe(true)
        })
    }

    testResize(parseConstruction(ODN_1))
    testResize(parseConstruction(ODN_2))

    testResize(parseConstruction(OK_ST_1))
    testResize(parseConstruction(OK_ST_2))
    testResize(parseConstruction(OK_ST_3))
    testResize(parseConstruction(OK_ST_5))
    testResize(parseConstruction(OK_ST_6))
    testResize(parseConstruction(OK_ST_7))
    testResize(parseConstruction(OK_ST_9))

    testResize(parseConstruction(OK_STER_1))
    testResize(parseConstruction(OK_STER_2))
    testResize(parseConstruction(OK_STER_3))
    testResize(parseConstruction(OK_STER_4))
    testResize(parseConstruction(OK_STER_5))

    testResize(parseConstruction(OB_MN_1))
    testResize(parseConstruction(OB_MN_10))
    testResize(parseConstruction(OB_MN_11))
    testResize(parseConstruction(OB_MN_12))
    testResize(parseConstruction(OB_MN_14))
    testResize(parseConstruction(OB_MN_15))
    testResize(parseConstruction(OB_MN_16))
    testResize(parseConstruction(OB_MN_17))
    testResize(parseConstruction(OB_MN_2))
    testResize(parseConstruction(OB_MN_3))
    testResize(parseConstruction(OB_MN_4))
    testResize(parseConstruction(OB_MN_9))
})

test('Тест на корректность точек отрисовки', () => {
    const testDrawingPoints = (construction: Construction, expectedDrawingPointsGroups: Point[][]) => {
        const drawingPointsGroups: Point[][] = []

        construction.products.forEach(product => {
            product.frame.allFrameFillings.forEach(frameFilling => {
                frameFilling.frameBeams.forEach(beam => drawingPointsGroups.push(beam.outerDrawingPoints))

                frameFilling.impostBeams.forEach(impost => drawingPointsGroups.push(impost.outerDrawingPoints))
            })

            drawingPointsGroups.push(...product.frame.allSolidFillings.map(s => s.containerPolygon.innerPolygon.points))
        })

        expect(drawingPointsGroups.length).toBe(expectedDrawingPointsGroups.length)
        expect(
            drawingPointsGroups.every(drawingPoints => {
                return expectedDrawingPointsGroups.some(expectedDrawingPoints =>
                    Polygon.buildFromPoints(drawingPoints).equals(Polygon.buildFromPoints(expectedDrawingPoints))
                )
            })
        ).toBe(true)
    }

    testDrawingPoints(parseConstruction(OK_ST_1), [
        [new Point(1300, 0), new Point(0, 0), new Point(63, 63), new Point(1237, 63)],
        [new Point(0, 0), new Point(0, 1400), new Point(63, 1337), new Point(63, 63)],
        [new Point(0, 1400), new Point(1300, 1400), new Point(1237, 1337), new Point(63, 1337)],
        [new Point(1300, 1400), new Point(1300, 0), new Point(1237, 63), new Point(1237, 1337)],

        [new Point(1265, 35), new Point(663, 35), new Point(739.5, 111.5), new Point(1188.5, 111.5)],
        [new Point(663, 35), new Point(663, 1365), new Point(739.5, 1288.5), new Point(739.5, 111.5)],
        [new Point(663, 1365), new Point(1265, 1365), new Point(1188.5, 1288.5), new Point(739.5, 1288.5)],
        [new Point(1265, 1365), new Point(1265, 35), new Point(1188.5, 111.5), new Point(1188.5, 1288.5)],

        [new Point(691, 0), new Point(609, 0), new Point(609, 1400), new Point(691, 1400)],

        [new Point(619, 53), new Point(53, 53), new Point(53, 1347), new Point(619, 1347)],

        [new Point(1198.5, 101.5), new Point(729.5, 101.5), new Point(729.5, 1298.5), new Point(1198.5, 1298.5)]
    ])

    testDrawingPoints(parseConstruction(OC_4), [
        [new Point(900, 0), new Point(0, 0), new Point(0, 0), new Point(900, 0)],
        [new Point(0, 0), new Point(0, 980), new Point(0, 980), new Point(0, 0)],
        [new Point(0, 980), new Point(370, 1400), new Point(370, 1400), new Point(0, 980)],
        [new Point(370, 1400), new Point(900, 1400), new Point(900, 1400), new Point(370, 1400)],
        [new Point(900, 1400), new Point(900, 0), new Point(900, 0), new Point(900, 1400)],

        [new Point(900, 0), new Point(0, 0), new Point(0, 980), new Point(370, 1400), new Point(900, 1400)]
    ])

    testDrawingPoints(parseConstruction(PMS_OK), [
        [new Point(600, 0), new Point(0, 0), new Point(25, 25), new Point(575, 25)],
        [new Point(0, 0), new Point(0, 1330), new Point(25, 1305), new Point(25, 25)],
        [new Point(0, 1330), new Point(600, 1330), new Point(575, 1305), new Point(25, 1305)],
        [new Point(600, 1330), new Point(600, 0), new Point(575, 25), new Point(575, 1305)],

        [new Point(575, 25), new Point(25, 25), new Point(25, 25), new Point(575, 25)],
        [new Point(25, 25), new Point(25, 1305), new Point(25, 1305), new Point(25, 25)],
        [new Point(25, 1305), new Point(575, 1305), new Point(575, 1305), new Point(25, 1305)],
        [new Point(575, 1305), new Point(575, 25), new Point(575, 25), new Point(575, 1305)],

        [new Point(575, 25), new Point(25, 25), new Point(25, 1305), new Point(575, 1305)]
    ])

    //TODO: тест на входные двери
})
