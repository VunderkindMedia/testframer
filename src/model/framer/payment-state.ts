export interface IPaymentStateJSON {
    id: number
    createdAt: string
    operationType: number
    paymentType: number
    operationState: number
    amount: number
    comment: string
    sberFormURL: string
    reservationType?: number
    orderId?: number
    reservedScore?: number
}

export class PaymentState {
    static parse(paymentStateJSON: IPaymentStateJSON): PaymentState {
        const paymentState = new PaymentState()
        Object.assign(paymentState, paymentStateJSON)
        return paymentState
    }

    id!: number
    createdAt!: string
    operationType!: number
    paymentType!: number
    operationState!: number
    amount!: number
    comment!: string
    sberFormURL!: string
    reservationType?: number
    orderId?: number
    reservedScore?: number
}
