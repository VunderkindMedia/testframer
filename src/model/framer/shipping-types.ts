export enum ShippingTypes {
    Delivery = 'delivery',
    Pickup = 'pickup'
}
