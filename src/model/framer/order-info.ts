export interface IOrderInfoJSON {
    orderName: string
    paymentStatus: number
    contragentId: string
    contragentName: string
    officeId: string
    webOrderName: string
}

export class OrderInfo {
    static parse(orderInfoJSON: IOrderInfoJSON): OrderInfo {
        const orderInfo = new OrderInfo()
        Object.assign(orderInfo, orderInfoJSON)
        return orderInfo
    }

    orderName!: string
    paymentStatus!: number
    contragentId!: string
    contragentName!: string
    officeId!: string
    webOrderName!: string
}
