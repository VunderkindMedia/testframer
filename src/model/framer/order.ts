import {Construction, IConstructionJSON} from './construction'
import {Client, IClientJSON} from './client'
import {IServiceJSON, Service} from './service'
import {OrderStates} from './order-states'
import {CalcResult, ICalcResultJSON} from './calc-result'
import {IMarginJSON, Margin} from './margin'
import {Discount, IDiscountJSON} from './discount'
import {IWarehouseJSON, Warehouse} from './warehouse'
import {Address, IAddressJSON} from '../address'
import {ShippingTypes} from './shipping-types'
import {CoordinateSystems} from '../coordinate-systems'
import {ContactInfo, IContactInfoJSON} from './contact-info'
import {IDadataAddress} from '../../kladr-manager'
import {Good, IGoodJSON} from './good'
import {IUserActionJSON, UserAction} from './user-action'
import {Product} from './product'
import {IContext} from '../i-context'
import {getClone} from '../../helpers/json'
import {Affiliate, IAffiliateJSON} from './affiliate'
import {Employee, IEmployeeJSON} from './employee'
import {Region, IRegionJSON} from './region'
import {DeliveryInfo, IDeliveryInfoJSON} from './deliveryInfo'

export interface IOrderJSON {
    id: number
    uniqueId: string
    orderNumber: string
    windrawOrderNumber: string
    windrawGoodsOrderNumber: string
    createdAt: string
    lastCalculatedAt: string
    state: OrderStates
    client: IClientJSON | null
    totalCost: number
    totalAmegaCost: number
    totalAmegaCostToDisplay: number
    totalAmegaCostWithDealerDiscount: number
    totalAmegaCostWithDealerDiscountToDisplay: number
    totalDiscount: number
    totalWeight: number
    totalSquare: number
    constructions: IConstructionJSON[]
    services: IServiceJSON[]
    calcResp: ICalcResultJSON
    margin: IMarginJSON
    goodsMargin: IMarginJSON
    discount: IDiscountJSON
    estShippingDate: string
    shippingType?: ShippingTypes
    shippingDate: string
    warehouse: IWarehouseJSON | null
    shippingAddress: IAddressJSON | null
    changed: boolean
    needRecalc: boolean
    readonly: boolean
    dadataAddress: IDadataAddress | null
    contactPerson: IContactInfoJSON | null
    goods: IGoodJSON[]
    userActions: IUserActionJSON[]
    offerAddonLine1?: string
    offerAddonLine2?: string
    mountingDate?: string
    filial: IAffiliateJSON | null
    owner: IEmployeeJSON | null
    haveEnergyProducts?: boolean
    isCancelAvailable?: boolean
    city?: IRegionJSON | null
    deliveryInfo?: IDeliveryInfoJSON[]
    calculatedWithFramerPoints: boolean
}

export class Order {
    static parse(orderJSON: IOrderJSON): Order {
        const order = new Order()
        Object.assign(order, orderJSON)

        orderJSON.client && (order.client = Client.parse(orderJSON.client))
        order.totalWeight = orderJSON.totalWeight ? Math.round(orderJSON.totalWeight * 100) / 100 : 0
        order.totalSquare = orderJSON.totalSquare ? Math.round(orderJSON.totalSquare * 100) / 100 : 0
        order.constructions = (orderJSON.constructions || []).map(constructionObj => Construction.parse(constructionObj))
        orderJSON.calcResp && (order.calcResp = CalcResult.parse(orderJSON.calcResp))
        order.services = (orderJSON.services || []).map(serviceObj => Service.parse(serviceObj))
        order.margin = Margin.parse(orderJSON.margin)
        order.goodsMargin = Margin.parse(orderJSON.goodsMargin)
        order.discount = Discount.parse(orderJSON.discount)
        order.shippingType = orderJSON.shippingType && orderJSON.shippingType.length !== 0 ? orderJSON.shippingType : ShippingTypes.Pickup
        orderJSON.warehouse && (order.warehouse = Warehouse.parse(orderJSON.warehouse))
        orderJSON.shippingAddress && (order.shippingAddress = Address.parse(orderJSON.shippingAddress))
        orderJSON.contactPerson && (order.contactPerson = ContactInfo.parse(orderJSON.contactPerson))
        orderJSON.goods && (order.goods = orderJSON.goods.map(g => Good.parse(g)))
        order.userActions = (orderJSON.userActions ?? []).map(ua => new UserAction(ua.t, ua.a))
        order.filial = orderJSON.filial ? Affiliate.parse(orderJSON.filial) : null
        order.owner = orderJSON.owner ? Employee.parse(orderJSON.owner) : null
        order.city = orderJSON.city ? Region.parse(orderJSON.city) : null
        order.deliveryInfo = (orderJSON.deliveryInfo || []).map(d => DeliveryInfo.parse(d))

        return order
    }

    static get Builder(): Builder {
        return new Builder()
    }

    id = -1
    uniqueId = ''
    orderNumber = ''
    windrawOrderNumber = ''
    windrawGoodsOrderNumber = ''
    createdAt = ''
    lastCalculatedAt = ''
    state = OrderStates.Draft
    client: Client | null = null
    totalCost = 0
    totalAmegaCost = 0
    totalAmegaCostToDisplay = 0
    totalAmegaCostWithDealerDiscount = 0
    totalAmegaCostWithDealerDiscountToDisplay = 0
    totalDiscount = 0
    totalWeight = 0
    totalSquare = 0
    constructions: Construction[] = []
    services: Service[] = []
    calcResp!: CalcResult
    margin = new Margin()
    goodsMargin = new Margin()
    discount = new Discount()
    estShippingDate = ''
    shippingType = ShippingTypes.Pickup
    shippingDate = ''
    warehouse: Warehouse | null = null
    shippingAddress: Address | null = null
    changed = false //TODO: возможно есть смысл просто хеш заказа считать
    needRecalc = false
    readonly = false
    dadataAddress: IDadataAddress | null = null
    contactPerson: ContactInfo | null = null
    goods: Good[] = []
    userActions: UserAction[] = []
    offerAddonLine1?: string
    offerAddonLine2?: string
    mountingDate?: string
    filial: Affiliate | null = null
    owner: Employee | null = null
    haveEnergyProducts = false
    isCancelAvailable = false
    city: Region | null = null
    deliveryInfo: DeliveryInfo[] = []
    calculatedWithFramerPoints = false

    addAction(
        message: string,
        construction: Construction | null | undefined = null,
        product: Product | null | undefined = null,
        node: IContext | null | undefined = null
    ): void {
        let action = ''
        if (construction) {
            action += `Construction ${construction.id}`
        }
        if (product) {
            action += `/ Product ${product.productGuid}`
        }
        if (node) {
            action += `/ Node ${node.nodeId}`
        }
        action += `${action.length > 0 ? ' :' : ''} ${message}`

        console.log(action)
        this.userActions.push(new UserAction(Date.now(), action))
        this.userActions = this.userActions.splice(-100)
    }

    findConstructionById(id: string): Construction | null {
        return this.constructions.find(c => c.id === id) ?? null
    }

    findProductByProductGuid(productGuid: string): Product | null {
        for (const construction of this.constructions) {
            const product = construction.products.find(p => p.productGuid === productGuid)

            if (product) {
                return product
            }
        }
        return null
    }

    findConstructionAndProductAndNodeByNodeId<T extends IContext>(
        nodeId: string
    ): {
        construction: Construction | null
        product: Product | null
        node: T | null
    } {
        for (const construction of this.constructions) {
            for (const product of construction.products) {
                const node = product.findNodeById(nodeId) as T

                if (node) {
                    return {
                        construction,
                        product,
                        node
                    }
                }
            }
        }

        return {
            construction: null,
            product: null,
            node: null
        }
    }

    get clone(): Order {
        return Order.parse(getClone<IOrderJSON>(this))
    }

    get producible(): boolean {
        const {state} = this

        return state === OrderStates.Draft || state === OrderStates.Paid
    }

    get fullOrderNumber(): string {
        if (this.windrawOrderNumber.length > 0) {
            return `${this.orderNumber} / ${this.windrawOrderNumber}`
        }
        return this.orderNumber
    }

    get preparedForServer(): Order {
        const order = this.clone
        order.constructions.forEach(construction => {
            construction.toCoordinateSystem(CoordinateSystems.Relative)
        })
        return order
    }

    get sortedConstructions(): Construction[] {
        const order = this.clone
        return order.constructions.sort((c1, c2) => {
            if (!c1.position) {
                return c2.position ? 1 : order.constructions.findIndex(c => c.id === c1.id) - order.constructions.findIndex(c => c.id === c2.id)
            }
            if (!c2.position) {
                return -1
            }
            return c1.position - c2.position
        })
    }

    get isCancelled(): boolean {
        return this.state === OrderStates.Cancelled
    }
}

class Builder {
    private _order = new Order()

    withConstructions(constructions: Construction[]): Builder {
        this._order.constructions = constructions
        return this
    }

    build(): Order {
        return this._order
    }
}
