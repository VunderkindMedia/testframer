import {GoodAttributeTypes} from './good-attribute-types'
import {GoodAttributeEnumValueColor, IGoodAttributeEnumValueColorJSON} from './good-attribute-enum-value-color'
import {getClone} from '../../helpers/json'

export interface IGoodAttributeJSON {
    name: string
    type: GoodAttributeTypes
    value: number
    caption: string
    editable: boolean
    maxValue?: number
    minValue?: number
    enumValues?: IGoodAttributeEnumValueColorJSON[]
    step?: number
}

export class GoodAttribute {
    static parse(goodAttributeJSON: IGoodAttributeJSON): GoodAttribute {
        const goodAttribute = new GoodAttribute(
            goodAttributeJSON.name,
            goodAttributeJSON.type,
            goodAttributeJSON.value,
            goodAttributeJSON.caption,
            goodAttributeJSON.editable
        )
        Object.assign(goodAttribute, goodAttributeJSON)

        goodAttributeJSON.enumValues &&
            (goodAttribute.enumValues = goodAttributeJSON.enumValues.map(ev => GoodAttributeEnumValueColor.parse(ev)))

        return goodAttribute
    }

    name: string
    type: GoodAttributeTypes
    value: number
    caption: string
    editable: boolean
    maxValue?: number
    minValue?: number
    step?: number
    enumValues?: GoodAttributeEnumValueColor[]

    constructor(name: string, type: GoodAttributeTypes, value: number, caption: string, editable: boolean) {
        this.name = name
        this.type = type
        this.value = value
        this.caption = caption
        this.editable = editable
    }

    get clone(): GoodAttribute {
        return GoodAttribute.parse(getClone(this))
    }

    get visibleValue(): number | string {
        if (this.type === GoodAttributeTypes.Color && this.enumValues) {
            return this.enumValues.find(v => v.value === this.value)?.name ?? ''
        }

        return this.value
    }

    get visibleDescription(): string {
        if (this.caption.includes(',')) {
            return this.caption.replace(',', ` ${this.visibleValue}`)
        }

        return `${this.caption} ${this.visibleValue}`
    }

    equals(attribute: GoodAttribute): boolean {
        const enumValues1 = this.enumValues ?? []
        const enumValues2 = attribute.enumValues ?? []

        return (
            this.name === attribute.name &&
            this.type === attribute.type &&
            this.caption === attribute.caption &&
            this.editable === attribute.editable &&
            this.maxValue === attribute.maxValue &&
            this.minValue === attribute.minValue &&
            enumValues1.length === enumValues2.length &&
            enumValues1.every(e1 => enumValues2.some(e2 => e2.equals(e1)))
        )
    }
}
