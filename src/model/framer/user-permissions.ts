/* eslint-disable @typescript-eslint/naming-convention */
import {IJWT} from '../token'

export interface IFilialsPermissions {
    Edit: boolean
}

export interface IOrdersPermissions {
    Edit: boolean
    PrintKP: boolean
    Produce: boolean
    Read: boolean
}

export interface IUsersPermissions {
    Edit: boolean
}

export enum Roles {
    NonqualifiedBossManager = 'NonqualifiedBossManager',
    BossManager = 'BossManager',
    ChiefManager = 'ChiefManager',
    CommonManager = 'CommonManager'
}

export interface IRoleJSON {
    Filials: IFilialsPermissions
    Orders: IOrdersPermissions
    Users: IUsersPermissions
}

export interface IUserPermissionsJSON extends IJWT {
    per: {
        [key in Roles]: {
            [key in string]: IRoleJSON
        }
    }
}

export class UserPermissions {
    static parse(userPermissionsJSON: IUserPermissionsJSON): UserPermissions {
        const userRole = Object.getOwnPropertyNames(userPermissionsJSON.per)[0] as Roles
        const permissions = Object.values(userPermissionsJSON.per[userRole])[0]
        return new UserPermissions(userRole, permissions.Filials, permissions.Orders, permissions.Users)
    }

    role: Roles
    filials: IFilialsPermissions
    orders: IOrdersPermissions
    users: IUsersPermissions

    constructor(role: Roles, filials: IFilialsPermissions, orders: IOrdersPermissions, users: IUsersPermissions) {
        this.role = role
        this.filials = filials
        this.orders = orders
        this.users = users
    }
}
