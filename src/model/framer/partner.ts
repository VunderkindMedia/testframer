import {ContactInfo, IContactInfoJSON} from './contact-info'
import {IServiceJSON, Service} from './service'
import {IMarginJSON, Margin} from './margin'
import {Dealer, IDealerJSON} from './dealer'
import {FactoryTypes} from './factory-types'
import {DealerInfo, IDealerInfoJSON} from './dealer-info'
import {Affiliate, IAffiliateJSON} from './affiliate'
import {getClone} from '../../helpers/json'

export interface IPartnerJSON {
    id: number
    externalId: string
    name: string
    contactInfo: IContactInfoJSON
    services: IServiceJSON[]
    margin: IMarginJSON
    goodsMargin: IMarginJSON
    discount: number
    lastReadNewsId: number
    dealers: IDealerJSON[]
    selectedDealerId: number
    factoryId: FactoryTypes
    offerAddonLine1: string
    offerAddonLine2: string
    newDealerInfo: IDealerInfoJSON | null
    userId: number
    userCQHash: string
    isSubdealershipAvailable: boolean
    isExportOrderToXmlAvailable: boolean
    isExternal: boolean
    isSubdealershipBasedOnDiscounts: boolean
    isColdAlAvailable?: boolean
    showDiscountInProfile: boolean
    isFramerPointsAvailable: boolean
    filial: IAffiliateJSON
}

export class Partner {
    static parse(partnerJSON: IPartnerJSON): Partner {
        const partner = new Partner()
        Object.assign(partner, partnerJSON)

        partner.contactInfo = ContactInfo.parse(partnerJSON.contactInfo)
        partner.services = (partnerJSON.services ?? []).map(serviceObj => Service.parse(serviceObj))
        partner.margin = Margin.parse(partnerJSON.margin)
        partner.goodsMargin = Margin.parse(partnerJSON.goodsMargin)
        partner.dealers = (partnerJSON.dealers ?? []).map(dealerObj => Dealer.parse(dealerObj))
        partner.filial = Affiliate.parse(partnerJSON.filial)

        return partner
    }

    id!: number
    externalId!: string
    name!: string
    contactInfo!: ContactInfo
    services: Service[] = []
    margin!: Margin
    goodsMargin!: Margin
    discount!: number
    dealers!: Dealer[]
    selectedDealerId!: number
    factoryId!: FactoryTypes
    offerAddonLine1 = ''
    offerAddonLine2 = ''
    region = ''
    userId!: number
    lastReadNewsId!: number
    userCQHash!: string
    newDealerInfo: DealerInfo | null = null
    isSubdealershipAvailable!: boolean
    isExportOrderToXmlAvailable = false
    isAdditionalMarginsAvailable = false
    isSubdealershipBasedOnDiscounts = false
    isExternal!: boolean
    showDiscountInProfile = false
    isFramerPointsAvailable = false
    isColdAlAvailable?: boolean
    filial!: Affiliate

    get clone(): Partner {
        return Partner.parse(getClone(this))
    }

    get isSubdealerAdditionalMarginsAvailable(): boolean {
        return this.isSubdealershipAvailable && this.isAdditionalMarginsAvailable
    }

    get isSubdealerDiscountsAvailable(): boolean {
        return this.isSubdealershipAvailable && this.isSubdealershipBasedOnDiscounts
    }
}
