import {Margin} from './margin'
import {IRegionJSON} from './region'

export interface IAffiliateJSON {
    id: number
    name: string
    isExternal: boolean
    margin: Margin
    goodsMargin: Margin
    laminationMargin?: Margin
    glassMargin?: Margin
    tintMargin?: Margin
    entranceDoorMargin?: Margin
    nonstandardMargin?: Margin
    subdealerConstrDiscount?: Margin
    subdealerGoodsDiscount?: Margin
    cityId: string
    city: IRegionJSON
}

export class Affiliate {
    static parse(affiliateJSON: IAffiliateJSON): Affiliate {
        const affiliate = new Affiliate()

        Object.assign(affiliate, affiliateJSON)
        affiliate.margin = Margin.parse(affiliateJSON.margin)
        affiliate.goodsMargin = Margin.parse(affiliateJSON.goodsMargin)
        affiliate.laminationMargin = affiliateJSON.laminationMargin ? Margin.parse(affiliateJSON.laminationMargin) : new Margin()
        affiliate.glassMargin = affiliateJSON.glassMargin ? Margin.parse(affiliateJSON.glassMargin) : new Margin()
        affiliate.tintMargin = affiliateJSON.tintMargin ? Margin.parse(affiliateJSON.tintMargin) : new Margin()
        affiliate.entranceDoorMargin = affiliateJSON.entranceDoorMargin ? Margin.parse(affiliateJSON.entranceDoorMargin) : new Margin()
        affiliate.nonstandardMargin = affiliateJSON.nonstandardMargin ? Margin.parse(affiliateJSON.nonstandardMargin) : new Margin()
        affiliate.subdealerConstrDiscount = affiliateJSON.subdealerConstrDiscount
            ? Margin.parse(affiliateJSON.subdealerConstrDiscount)
            : new Margin()
        affiliate.subdealerGoodsDiscount = affiliateJSON.subdealerGoodsDiscount
            ? Margin.parse(affiliateJSON.subdealerGoodsDiscount)
            : new Margin()

        return affiliate
    }

    id?: number
    name = ''
    isExternal = false
    isMain = false
    margin = new Margin()
    goodsMargin = new Margin()
    laminationMargin = new Margin()
    glassMargin = new Margin()
    tintMargin = new Margin()
    entranceDoorMargin = new Margin()
    nonstandardMargin = new Margin()
    subdealerConstrDiscount = new Margin()
    subdealerGoodsDiscount = new Margin()
    cityId!: number
    city: IRegionJSON | undefined
}
