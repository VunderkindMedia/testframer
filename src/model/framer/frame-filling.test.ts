import {ODN_1} from '../../constants/construction-templates/group-odn'
import {TEST_FURNITURE_TEMPLATE, TEST_PROFILE_TEMPLATE} from '../../constants/default-templates'
import {Construction} from './construction'
import {ImpostTypes} from '../impost-types'
import {Line} from '../geometry/line'
import {Point} from '../geometry/point'

const parseConstruction = (template: any): Construction => {
    template.products.forEach((productObj: any) => {
        productObj.profile = TEST_PROFILE_TEMPLATE
        productObj.furniture = TEST_FURNITURE_TEMPLATE
    })
    return Construction.parse(template)
}

const addImpost = (construction: Construction, impostType: ImpostTypes, addToEnd = true): void => {
    const product = construction.products[0]

    const {allSolidFillings} = product.frame

    const index = addToEnd ? allSolidFillings.length - 1 : 0
    const solidFilling = allSolidFillings[index]

    product.addImpost(solidFilling, impostType)
    product.update()
}

test('Тест на порядок импостов в раме', () => {
    const ODN1 = parseConstruction(ODN_1)
    const dimensionLines = ODN1.getDimensionLines()
    const widthDimensionLine = dimensionLines
        .slice(0)
        .filter(d => d.isHorizontal)
        .sort()[0]
    const heightDimensionLine = dimensionLines
        .slice(0)
        .filter(d => d.isVertical)
        .sort()[0]

    ODN1.changeSizesAndOffsets(widthDimensionLine, 2000)
    ODN1.changeSizesAndOffsets(heightDimensionLine, 2000)

    addImpost(ODN1, ImpostTypes.Vertical)
    addImpost(ODN1, ImpostTypes.Vertical, false)
    addImpost(ODN1, ImpostTypes.Horizontal)
    addImpost(ODN1, ImpostTypes.Horizontal)
    addImpost(ODN1, ImpostTypes.Vertical)
    addImpost(ODN1, ImpostTypes.Horizontal, false)
    addImpost(ODN1, ImpostTypes.Horizontal, false)
    addImpost(ODN1, ImpostTypes.Vertical, false)

    ODN1.products[0].frame.impostBeams.sort(() => Math.random() - 0.5)
    ODN1.update()

    const {impostBeams} = ODN1.products[0].frame

    expect(impostBeams[0].equals(new Line(new Point(500, 0), new Point(500, 2000)))).toBe(true)
    expect(impostBeams[1].equals(new Line(new Point(1000, 0), new Point(1000, 2000)))).toBe(true)
    expect(impostBeams[2].equals(new Line(new Point(500, 1500), new Point(0, 1500)))).toBe(true)
    expect(impostBeams[3].equals(new Line(new Point(500, 1000), new Point(0, 1000)))).toBe(true)
    expect(impostBeams[4].equals(new Line(new Point(1000, 1000), new Point(2000, 1000)))).toBe(true)
    expect(impostBeams[5].equals(new Line(new Point(1000, 500), new Point(2000, 500)))).toBe(true)
    expect(impostBeams[6].equals(new Line(new Point(250, 1500), new Point(250, 2000)))).toBe(true)
    expect(impostBeams[7].equals(new Line(new Point(1500, 500), new Point(1500, 0)))).toBe(true)
})
