import {Colors} from '../../constants/colors'
import {isWhiteColor} from '../../helpers/color'

export enum MoskitkaColors {
    White = 'white',
    Brown = 'brown',
    ConstructionColor = 'constructionColor'
}

export function getHexMoskitkaColor(moskitkaColor: MoskitkaColors, laminationColor: string): string {
    switch (moskitkaColor) {
        case MoskitkaColors.White:
            return Colors.Black
        case MoskitkaColors.Brown:
            return '#66321c'
        case MoskitkaColors.ConstructionColor:
            if (isWhiteColor(laminationColor)) {
                return Colors.Black
            }
            return laminationColor
    }
}

export function getMoskitkaColorName(moskitkaColor: MoskitkaColors): string {
    switch (moskitkaColor) {
        case MoskitkaColors.White:
            return 'Белый'
        case MoskitkaColors.Brown:
            return 'Коричневый\nв массе'
        case MoskitkaColors.ConstructionColor:
            return 'В цвет ламинации\nконструкции'
    }
}
