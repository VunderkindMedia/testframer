export interface IGoodGroupJSON {
    id: number
    parentGroupId: number
    name: string
}

export class GoodGroup {
    static parse(goodGroupJSON: IGoodGroupJSON): GoodGroup {
        return new GoodGroup(goodGroupJSON.id, goodGroupJSON.parentGroupId, goodGroupJSON.name)
    }

    id: number
    parentGroupId: number
    name: string

    constructor(id: number, parentGroupId: number, name: string) {
        this.id = id
        this.parentGroupId = parentGroupId
        this.name = name
    }
}
