export interface ILaminationJSON {
    id: string
    name: string
    hex: string
}

export class Lamination {
    static parse(laminationJSON: ILaminationJSON): Lamination {
        return new Lamination(+laminationJSON.id, laminationJSON.name, laminationJSON.hex)
    }

    static getDefault(): Lamination {
        return new Lamination(2, 'Белый', '#fff')
    }

    static getDefaultAluminium(): Lamination {
        return new Lamination(234, 'Белый', '#fff')
    }

    id: number
    name: string
    hex: string

    constructor(id: number, name: string, hex: string) {
        this.id = id
        this.name = name
        this.hex = hex
    }
}
