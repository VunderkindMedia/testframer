export interface IAccountJSON {
    accountNumber: string
    dealerId: number
    balance: number
    framerPoints: number
    creditLimitInCents: number
    framerCreditLimitInCents: number
    potentialScore: number
    reservedScore?: number
    availableScore: number
    earnedScore: number
    nextDiscount?: number
    nextDiscountAmount?: number
    showBalance: boolean
}

export class Account {
    static parse(accountJSON: IAccountJSON): Account {
        const account = new Account()
        Object.assign(account, accountJSON)
        return account
    }

    accountNumber!: string
    dealerId!: number
    balance!: number
    creditLimitInCents!: number
    framerCreditLimitInCents = 0
    potentialScore!: number
    reservedScore?: number
    availableScore!: number
    earnedScore!: number
    nextDiscount = 0
    nextDiscountAmount = 0
    framerPoints = 0
    showBalance = false
}
