import {IWarehouseJSON, Warehouse} from './warehouse'

export interface IBookingOptionsJSON {
    warehouses: IWarehouseJSON[]
    isDeliveryAvailable: boolean
    kladrRegionId: string
    kladrRegionName: string
    availableBonusScore: number
    overweightConstructionsCount: number
}

export class BookingOptions {
    static parse(bookingOptionsJSON: IBookingOptionsJSON): BookingOptions {
        const bookingOptions = new BookingOptions()
        Object.assign(bookingOptions, bookingOptionsJSON, {
            warehouses: bookingOptionsJSON.warehouses.map(warehouseObj => Warehouse.parse(warehouseObj))
        })
        return bookingOptions
    }

    warehouses: Warehouse[] = []
    isDeliveryAvailable = false
    kladrRegionId = ''
    kladrRegionName = ''
    availableBonusScore = 0
    overweightConstructionsCount = 0
}
