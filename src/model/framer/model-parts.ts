export enum ModelParts {
    NA = 0,
    Rama = 1,
    Stvorka = 2,
    Impost = 3,
    Connector = 4,
    Porog = 5,
    Shtulp = 6,
    Shtapik = 7,
    Falsh = 8,
    Zapolneniya = 9,
    Adapter = 10
}
