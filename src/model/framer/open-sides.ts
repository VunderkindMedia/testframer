export enum OpenSides {
    ToLeft, // Влево
    ToRight, // Вправо
    ToTop, // Вверх
    ToBottom, // Вниз
    NoOpening // Нет открывания
}
