import {GeometryParameters, IGeometryParametersJSON} from './geometry-parameters'
import {ModelParts} from './model-parts'
import {ENERGY_FILLING_IDS} from '../../constants/filling-settings'

export interface IProfileJSON {
    id: number
    name: string
    displayName?: string
    allowedThickness: number[]
    parts: IGeometryParametersJSON[]
    brand: string
    camernost: number
    thickness: number
    isDefault: boolean
    isEmpty: boolean
    isMoskitka: boolean
}

export class Profile {
    static parse(profileJSON: IProfileJSON): Profile {
        const profile = new Profile(profileJSON.id, profileJSON.name)
        Object.assign(profile, profileJSON)
        profileJSON.parts && (profile.parts = profileJSON.parts.map(partObj => GeometryParameters.parse(partObj)))
        return profile
    }

    id: number
    name: string
    displayName?: string
    allowedThickness: number[] = []
    parts: GeometryParameters[] = []
    class = ''
    brand = ''
    camernost = 0
    thickness = 0
    isDefault = false
    isEmpty = false
    isMoskitka = false
    isEnergy: null | boolean = null

    constructor(id: number, name: string) {
        this.id = id
        this.name = name
    }

    getGeometryParameters(modelPart: ModelParts): GeometryParameters {
        const geometryParameters = this.parts.find(p => p.modelPart === modelPart)
        if (!geometryParameters) {
            console.error(`Не найдены параметры для modelPart = ${modelPart}, вернули булшит чтоб не упало`)
            return new GeometryParameters(-1, '', modelPart, 0, 0, 0, 0, 0, 0, 0, '')
        }
        return geometryParameters
    }

    getFirstAvailableGeometryParameters(marking: string, modelPart: ModelParts): GeometryParameters {
        return this.parts.find(p => p.marking === marking && p.modelPart === modelPart) ?? this.getGeometryParameters(modelPart)
    }

    getAllGeometryParameters(modelPart: ModelParts): GeometryParameters[] {
        return this.parts.filter(p => p.modelPart === modelPart)
    }

    set isEnergyProfile(value: boolean) {
        this.isEnergy = value
    }

    get isEnergyProfile(): boolean {
        if (this.isEnergy !== null) {
            return this.isEnergy
        }

        const isEnergy = ENERGY_FILLING_IDS.includes(this.id)
        this.isEnergyProfile = isEnergy

        return isEnergy
    }
}
