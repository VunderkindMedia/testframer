/* eslint-disable @typescript-eslint/naming-convention */
import {FrameFilling, IFrameFillingJSON} from './frame-filling'
import {Size} from '../size'
import {IContext} from '../i-context'
import {Impost} from './impost'
import {SolidFilling} from './solid-filling'
import {ImpostTypes} from '../impost-types'
import {Connector, IConnectorJSON} from './connector'
import {IPointJSON, Point} from '../geometry/point'
import {Rectangle} from '../geometry/rectangle'
import {IProfileJSON, Profile} from './profile'
import {Furniture, IFurnitureJSON} from './furniture'
import {Beam} from './beam'
import {OpeningSystem} from '../opening-system'
import {ILaminationJSON, Lamination} from './lamination'
import {Line} from '../geometry/line'
import {ProductTypes} from './product-types'
import {Filling, IFillingJSON} from './filling'
import {OpenTypes} from './open-types'
import {ShtulpGroup} from '../shtulp-group'
import {ShtulpOpenTypes} from './shtulp-open-types'
import {ModelParts} from './model-parts'
import {getClone} from '../../helpers/json'
import {approximatelyEqual} from '../../helpers/number'
import {GUID} from '../../helpers/guid'
import {GeometryParameters} from './geometry-parameters'
import {ConstructionTypes} from './construction-types'
import {OpenSides} from './open-sides'
import {ShtulpTypes} from './shtulp-types'
import {
    LEFT_DOWN_OPENING_SYSTEM,
    LEFT_SHTULP_OPENING_SYSTEM,
    OPENING_SYSTEMS,
    RIGHT_DOWN_OPENING_SYSTEM,
    RIGHT_SHTULP_OPENING_SYSTEM
} from '../../constants/opening-systems'
import {IUserParameterJSON, UserParameter} from './user-parameter/user-parameter'

export interface IProductJSON {
    productGuid?: string
    name?: string
    insideColorId: number
    outsideColorId: number
    profileSystemName?: string
    hardwareSystemName?: string
    constructionTypeId: number
    productionTypeId: number
    templateID?: number
    frame: IFrameFillingJSON
    connectors?: IConnectorJSON[]
    profile?: IProfileJSON
    furniture?: IFurnitureJSON
    outsideLamination?: ILaminationJSON
    insideLamination?: ILaminationJSON
    productOffset?: number
    // eslint-disable-next-line @typescript-eslint/naming-convention
    _refPoint?: IPointJSON
    // eslint-disable-next-line @typescript-eslint/naming-convention
    _availableProfiles?: IProfileJSON[]
    // eslint-disable-next-line @typescript-eslint/naming-convention
    _availableFurniture?: IFurnitureJSON[]
    // eslint-disable-next-line @typescript-eslint/naming-convention
    _availableFillings?: IFillingJSON[]
    userParameters?: IUserParameterJSON[]
    hardcodedUserParameters?: IUserParameterJSON[]
}

export class Product implements IContext {
    static parse(productJSON: IProductJSON): Product {
        const product = new Product()
        Object.assign(product, productJSON)

        productJSON.profile && (product.profile = Profile.parse(productJSON.profile))
        productJSON.furniture && (product.furniture = Furniture.parse(productJSON.furniture))
        product.frame = FrameFilling.parse(productJSON.frame, null, product.profile, product.furniture)
        product.frame.isMoskitkaRoot = product.isMoskitka
        product.frame.isEmpty = product.isEmpty
        product.frame.isEntranceDoor = product.isEntranceDoor
        product.frame.isAluminium = product.isAluminium
        product.connectors = (productJSON.connectors ?? []).map(c => Connector.parse(c, product.productGuid))
        product.productOffset = productJSON.productOffset ?? 0
        productJSON._refPoint && (product.refPoint = Point.parse(productJSON._refPoint))

        product.availableProfiles = (productJSON._availableProfiles ?? []).map(p => Profile.parse(p))
        product.availableFurniture = (productJSON._availableFurniture ?? []).map(f => Furniture.parse(f))
        product.availableFillings = (productJSON._availableFillings ?? []).map(f => Filling.parse(f))
        productJSON.userParameters && (product.userParameters = productJSON.userParameters.map(u => UserParameter.parse(u)))
        productJSON.hardcodedUserParameters &&
            (product.hardcodedUserParameters = productJSON.hardcodedUserParameters.map(u => UserParameter.parse(u)))

        if (productJSON.outsideLamination && productJSON.insideLamination) {
            product.setOutsideLamination(Lamination.parse(productJSON.outsideLamination))
            product.setInsideLamination(Lamination.parse(productJSON.insideLamination))
        } else {
            const defaultLamination = product.isAluminium ? Lamination.getDefaultAluminium() : Lamination.getDefault()
            product.setOutsideLamination(defaultLamination)
            product.setInsideLamination(defaultLamination)
        }

        product.update()

        return product
    }

    productGuid = GUID()
    name?: string
    insideColorId!: number
    outsideColorId!: number
    profileSystemName?: string
    hardwareSystemName?: string
    constructionTypeId?: number
    productionTypeId?: number
    templateID?: number
    frame = new FrameFilling()
    connectors: Connector[] = []
    profile!: Profile
    furniture!: Furniture
    outsideLamination!: Lamination
    insideLamination!: Lamination
    productOffset = 0 // сдвиг относительно края конектора
    userParameters: UserParameter[] = []
    hardcodedUserParameters: UserParameter[] = []
    private _refPoint = Point.zero // абсолютное положение на координатной плоскости
    private _availableProfiles: Profile[] = []
    private _availableFurniture: Furniture[] = []
    private _availableFillings: Filling[] = []

    get rect(): Rectangle {
        const points: Point[] = []

        this.frame.frameBeams.forEach(beam => {
            points.push(beam.start, beam.end)
        })

        return Rectangle.buildFromPoints(points)
    }

    get width(): number {
        return this.frame.size.width
    }

    set width(value: number) {
        this.frame.size = new Size(value, this.size.height)
    }

    get height(): number {
        return this.frame.size.height
    }

    set height(value: number) {
        this.frame.size = new Size(this.size.width, value)
    }

    get size(): Size {
        return this.frame.size
    }

    get clone(): Product {
        return Product.parse(getClone<IProductJSON>(this))
    }

    // использовать только в пределах конструкции!!!
    get refPoint(): Point {
        return this._refPoint
    }

    set refPoint(point: Point) {
        this._refPoint = point
    }

    get nodeId(): string {
        return this.productGuid
    }

    get allPoints(): Point[] {
        const points = this.frame.allPoints

        this.connectors.forEach(connector => {
            points.push(connector.start)
            points.push(connector.end)
        })

        return points
    }

    get leftmostBottommostPoint(): Point {
        const {bottommostBeam} = this

        if (bottommostBeam instanceof Connector) {
            return bottommostBeam.leftBottomPoint.withOffset(0, -bottommostBeam.width)
        }

        return bottommostBeam.leftBottomPoint
    }

    get rightmostBottommostPoint(): Point {
        const {bottommostBeam} = this

        if (bottommostBeam instanceof Connector) {
            return bottommostBeam.rightTopPoint.withOffset(0, -bottommostBeam.width)
        }

        return bottommostBeam.rightTopPoint
    }

    get availableProfiles(): Profile[] {
        return this.getAvailableProfilesFilteredByModelParts(this.usedModelParts)
    }

    set availableProfiles(availableProfiles: Profile[]) {
        this._availableProfiles = availableProfiles
    }

    get availableFurniture(): Furniture[] {
        return this._availableFurniture
    }

    set availableFurniture(availableFurniture: Furniture[]) {
        this._availableFurniture = availableFurniture
    }

    get availableFillings(): Filling[] {
        return this._availableFillings
    }

    set availableFillings(availableFillings: Filling[]) {
        this._availableFillings = availableFillings
        this.updateSolidFillings(availableFillings)
    }

    setProfile(profile: Profile): void {
        this.profile = profile
        this.frame.setProfile(profile)
        this.update()
    }

    setFurniture(furniture: Furniture): void {
        this.furniture = furniture
        this.frame.setFurniture(furniture)
        this.update()
    }

    setInsideLamination(lamination: Lamination): void {
        this.insideLamination = lamination
        this.insideColorId = lamination.id
    }

    setOutsideLamination(lamination: Lamination): void {
        this.outsideLamination = lamination
        this.outsideColorId = lamination.id
    }

    update(): void {
        this.profileSystemName = this.profile.name
        this.hardwareSystemName = this.furniture.name
        this._updateConnectors()
        this.frame.update()
    }

    updateNode(node: IContext): void {
        if (node.nodeId === this.frame.nodeId) {
            this.frame = node as FrameFilling
        } else {
            this.frame.updateNode(node)
        }
        this.frame.update()
    }

    setImpostOffset(impost: Impost, offset: number): void {
        const impostClone = (this.frame.findNodeById(impost.nodeId) as Impost).clone

        if (impostClone.isVertical) {
            impostClone.start = new Point(impost.start.x - impost.offsetLeft + offset, impost.start.y)
            impostClone.end = new Point(impost.end.x - impost.offsetLeft + offset, impost.end.y)
        } else if (impostClone.isHorizontal) {
            impostClone.start = new Point(impost.start.x, impost.start.y - impost.offsetBottom + offset)
            impostClone.end = new Point(impost.end.x, impost.end.y - impost.offsetBottom + offset)
        }

        this.updateNode(impostClone)
    }

    removeImpost(impost: Impost): void {
        const impostParent = this.frame.findNodeById(impost.parentNodeId) as FrameFilling
        impostParent.removeImpost(impost)
    }

    addImpost(solidFilling: SolidFilling, impostType: ImpostTypes): void {
        const solidFillingParent = this.frame.findNodeById(solidFilling.parentNodeId)
        if (!(solidFillingParent instanceof FrameFilling)) {
            return
        }
        solidFillingParent.addImpost(solidFilling, impostType)
    }

    addConnector(connector: Connector): void {
        connector.normalizePointsOrder()
        this.connectors.push(connector)
        this._updateConnectors()
    }

    replaceConnector(connector: Connector, index: number): void {
        this.connectors[index] = connector
        this._updateConnectors()
    }

    removeConnector(connector: Connector): void {
        if (connector.connectedProductGuid) {
            console.warn('Нельзя удалить конектор к которому присоединен продукт')
            return
        }

        const connectorIndex = this.connectors.findIndex(c => c.nodeId === connector.nodeId)

        this.connectors.splice(connectorIndex, 1)
        this._updateConnectors()
    }

    setOpeningSystem(context: SolidFilling | FrameFilling, openingSystem: OpeningSystem): void {
        const modifiedOpeningSystem = openingSystem.clone()

        const shtulpGroup = this.findAvailableShtulpGroups(context).find(group => {
            const {leftFilling, rightFilling} = group
            if (openingSystem.openSide == OpenSides.ToLeft) {
                return context.nodeId === leftFilling.nodeId
            }
            return context.nodeId === rightFilling.nodeId
        })

        if (shtulpGroup) {
            const {leftFilling, rightFilling, impost} = shtulpGroup

            const setOpeningSystem = (
                mainFillingShtulpOpeningSystem: OpeningSystem,
                oppositeFilling: FrameFilling | SolidFilling,
                oppositeFillingShtulpOpeningSystem: OpeningSystem,
                mainImpostShtulpType: ShtulpTypes
            ): void => {
                if (openingSystem.equals(mainFillingShtulpOpeningSystem)) {
                    oppositeFillingShtulpOpeningSystem.shtulpOpenType = ShtulpOpenTypes.NoShtulpOnLeaf
                    this._setOpeningSystemInner(oppositeFilling, oppositeFillingShtulpOpeningSystem)
                    impost.modelPart = ModelParts.Shtulp
                    impost.shtulpType = mainImpostShtulpType
                } else {
                    if (
                        openingSystem.openSide === mainFillingShtulpOpeningSystem.openSide &&
                        oppositeFillingShtulpOpeningSystem.shtulpOpenType === ShtulpOpenTypes.ShtulpOnLeaf
                    ) {
                        modifiedOpeningSystem.shtulpOpenType = ShtulpOpenTypes.NoShtulpOnLeaf
                        return
                    }

                    if (!Product._isValidShtulpGroup(shtulpGroup) || !(oppositeFilling instanceof FrameFilling)) {
                        return
                    }

                    if (
                        openingSystem.openSide !== mainFillingShtulpOpeningSystem.openSide ||
                        oppositeFilling.shtulpOpenType !== ShtulpOpenTypes.ShtulpOnLeaf
                    ) {
                        modifiedOpeningSystem.shtulpOpenType = ShtulpOpenTypes.NoShtulp
                        oppositeFilling.shtulpOpenType = ShtulpOpenTypes.NoShtulp
                        impost.modelPart = ModelParts.Impost
                        impost.shtulpType = ShtulpTypes.Impost
                    }
                }
            }

            const findOriginalOpeningSystem = (filling: SolidFilling | FrameFilling): OpeningSystem | null | undefined => {
                if (filling instanceof SolidFilling) {
                    return null
                }

                return OPENING_SYSTEMS.find(
                    o =>
                        o.openType === filling.openType &&
                        o.openSide === filling.openSide &&
                        (filling.shtulpOpenType === ShtulpOpenTypes.NoShtulpOnLeaf
                            ? o.shtulpOpenType === ShtulpOpenTypes.NoShtulp
                            : o.shtulpOpenType === filling.shtulpOpenType)
                )
            }

            if (context.nodeId === leftFilling.nodeId) {
                const originalOpeningSystem = findOriginalOpeningSystem(rightFilling)?.clone()
                setOpeningSystem(
                    LEFT_SHTULP_OPENING_SYSTEM,
                    rightFilling,
                    originalOpeningSystem?.openSide === OpenSides.ToRight ? originalOpeningSystem : RIGHT_DOWN_OPENING_SYSTEM.clone(),
                    ShtulpTypes.LeftShtulp
                )
            } else {
                const originalOpeningSystem = findOriginalOpeningSystem(leftFilling)?.clone()
                setOpeningSystem(
                    RIGHT_SHTULP_OPENING_SYSTEM,
                    leftFilling,
                    originalOpeningSystem?.openSide === OpenSides.ToLeft ? originalOpeningSystem : LEFT_DOWN_OPENING_SYSTEM.clone(),
                    ShtulpTypes.RightShtulp
                )
            }
        }
        this._setOpeningSystemInner(context, modifiedOpeningSystem)
        this._fixShtulpGroups(shtulpGroup)
    }

    private _fixShtulpGroups(ignoredShtulpGroup?: ShtulpGroup): void {
        const availableShtulpGroups = this.availableShtulpGroups.filter(g => {
            if (!ignoredShtulpGroup) {
                return true
            }

            return !g.equals(ignoredShtulpGroup)
        })

        const resetImpost = (impost: Impost): void => {
            impost.modelPart = ModelParts.Impost
            impost.shtulpType = ShtulpTypes.Impost
        }

        availableShtulpGroups.forEach(group => {
            const {leftFilling, rightFilling, impost} = group

            if (!Product._isValidShtulpGroup(group)) {
                resetImpost(impost)

                if (leftFilling instanceof FrameFilling && leftFilling.openSide === OpenSides.ToLeft) {
                    leftFilling.shtulpOpenType = ShtulpOpenTypes.NoShtulp
                }

                if (rightFilling instanceof FrameFilling && rightFilling.openSide === OpenSides.ToRight) {
                    rightFilling.shtulpOpenType = ShtulpOpenTypes.NoShtulp
                }
            }
        })

        this.update()
    }

    private static _isValidShtulpGroup(group: ShtulpGroup): boolean {
        const {leftFilling, rightFilling} = group

        if (leftFilling instanceof SolidFilling || rightFilling instanceof SolidFilling) {
            return false
        }
        if (leftFilling.shtulpOpenType === ShtulpOpenTypes.NoShtulp || rightFilling.shtulpOpenType === ShtulpOpenTypes.NoShtulp) {
            return false
        }
        if (leftFilling.shtulpOpenType === ShtulpOpenTypes.ShtulpOnLeaf && rightFilling.shtulpOpenType !== ShtulpOpenTypes.NoShtulpOnLeaf) {
            return false
        }

        if (leftFilling.shtulpOpenType === ShtulpOpenTypes.NoShtulpOnLeaf && rightFilling.shtulpOpenType !== ShtulpOpenTypes.ShtulpOnLeaf) {
            return false
        }

        if (leftFilling.openSide !== OpenSides.ToLeft || rightFilling.openSide !== OpenSides.ToRight) {
            return false
        }

        return true
    }

    private _setOpeningSystemInner(context: SolidFilling | FrameFilling, openingSystem: OpeningSystem): void {
        if (context instanceof SolidFilling) {
            const parent = this.frame.findNodeById(context.parentNodeId)

            if ((parent && parent.parentNodeId !== null) || !parent) {
                return
            }

            if (openingSystem.openType !== OpenTypes.Gluhoe) {
                const newFrameFilling = FrameFilling.buildFromSolidFilling(
                    context,
                    parent as FrameFilling,
                    openingSystem.openType,
                    openingSystem.openSide,
                    openingSystem.shtulpOpenType
                )

                this.updateNode(newFrameFilling)
            }
        } else {
            if (openingSystem.openType === OpenTypes.Gluhoe) {
                const solidFilling = SolidFilling.buildFromFrameFilling(context)
                this.updateNode(solidFilling)
            } else {
                const newFrameFilling = this.frame.findNodeById(context.nodeId) as FrameFilling

                newFrameFilling.openType = openingSystem.openType
                newFrameFilling.openSide = openingSystem.openSide
                newFrameFilling.shtulpOpenType = openingSystem.shtulpOpenType
            }
        }
    }

    findNodeById(nodeId: string): IContext | null {
        const node = this.frame.findNodeById(nodeId)
        if (node) {
            return node
        }

        const connector = this.connectors.find(c => c.nodeId === nodeId)
        if (connector) {
            return connector
        }

        return null
    }

    updateSolidFillings(fillings: Filling[]): void {
        this.frame.allSolidFillings.forEach(solidFilling => {
            const filling = fillings.find(f => f.marking === solidFilling.fillingVendorCode)
            if (filling) {
                solidFilling.filling = filling
            } else {
                // const defaultFilling = fillings.filter(f => f.isDefault).sort((f1, f2) => f1.thickness - f2.thickness)[0] ?? fillings[0]
                // if (defaultFilling) {
                //     solidFilling.fillingVendorCode = defaultFilling.marking
                // }
            }
        })
    }

    get leftmostBeam(): Beam {
        return this._getSideLastBeam(this.frame.leftmostBeam /*, true*/) //TODO: убрана проверка на наличие расширителя (см. стр 754)
    }

    get rightmostBeam(): Beam {
        return this._getSideLastBeam(this.frame.rightmostBeam /*, true*/) //TODO: убрана проверка на наличие расширителя (см. стр 754)
    }

    get topmostBeam(): Beam {
        return this._getSideLastBeam(this.frame.topmostBeam /*, true*/) //TODO: убрана проверка на наличие расширителя (см. стр 754)
    }

    get bottommostBeam(): Beam {
        return this._getSideLastBeam(this.frame.bottommostBeam /*, true*/) //TODO: убрана проверка на наличие расширителя (см. стр 754)
    }

    get leftmostConnectedBeam(): Beam {
        return this._getSideLastBeam(this.frame.leftmostBeam)
    }

    get rightmostConnectedBeam(): Beam {
        return this._getSideLastBeam(this.frame.rightmostBeam)
    }

    get topmostConnectedBeam(): Beam {
        return this._getSideLastBeam(this.frame.topmostBeam)
    }

    get bottommostConnectedBeam(): Beam {
        return this._getSideLastBeam(this.frame.bottommostBeam)
    }

    get rightmostSide(): Line {
        return this.getSideLine(this.rightmostBeam)
    }

    get leftmostSide(): Line {
        return this.getSideLine(this.leftmostBeam)
    }

    get isDoor(): boolean {
        return (
            this.constructionTypeId === ConstructionTypes.BalkonnayaDver ||
            this.constructionTypeId === ConstructionTypes.DverVhodnayaOtkryvanieNaruzhu
        )
    }

    get isMoskitka(): boolean {
        return this.productionTypeId === ProductTypes.Moskitka
    }

    get isEmpty(): boolean {
        return (
            this.productionTypeId === ProductTypes.Steklopaket ||
            this.productionTypeId === ProductTypes.Sendvich ||
            this.productionTypeId === ProductTypes.Steklo
        )
    }

    get isEntranceDoor(): boolean {
        return this.productionTypeId === ProductTypes.EntranceDoor
    }

    get isAluminium(): boolean {
        return this.constructionTypeId === ConstructionTypes.AlyuminievayaKonstrukciya
    }

    get isP400(): boolean {
        return this.frame.allFrameFillings.some(f => f.isP400)
    }

    findHostBeam(connector: Connector): Beam | undefined {
        const beams = this.frame.frameBeams.concat(this.connectors)

        return beams.find(beam => beam.nodeId === connector.connectedBeamGuid)
    }

    getSideLine(beam: Beam): Line {
        if (!beam.isVertical) {
            return beam
        }

        const sideLine = beam.clone
        sideLine.normalizePointsOrder()

        const {topmostBeam, bottommostBeam} = this

        const topmostBeamRect = Rectangle.buildFromPoints(topmostBeam.innerDrawingPoints)
        const bottommostBeamRect = Rectangle.buildFromPoints(bottommostBeam.innerDrawingPoints)

        const isRight = beam.nodeId === this.rightmostBeam.nodeId

        if (isRight) {
            if (sideLine.start.x === bottommostBeamRect.rightBottomPoint.x) {
                sideLine.start = bottommostBeamRect.rightBottomPoint.clone
            }
            if (sideLine.end.x === topmostBeamRect.rightTopPoint.x) {
                sideLine.end = topmostBeamRect.rightTopPoint.clone
            }
        } else {
            if (sideLine.start.x === bottommostBeamRect.leftBottomPoint.x) {
                sideLine.start = bottommostBeamRect.leftBottomPoint.clone
            }
            if (sideLine.end.x === topmostBeamRect.leftTopPoint.x) {
                sideLine.end = topmostBeamRect.leftTopPoint.clone
            }
        }

        return sideLine
    }

    alignFillingsByLightAperture(): void {
        const {allSolidFillings} = this.frame

        const totalWidth = allSolidFillings.reduce((w, s) => w + s.containerPolygon.innerPolygon.rect.size.width, 0)
        const averageWidth = +(totalWidth / allSolidFillings.length).toFixed(1)

        let lastOffset = 0
        allSolidFillings.forEach((solidFilling, index) => {
            const offset = averageWidth - solidFilling.containerPolygon.innerPolygon.rect.size.width + lastOffset
            lastOffset = offset
            const impost = this.frame.impostBeams[index]
            if (impost) {
                impost.start.x += offset
                impost.end.x += offset
            }
        })

        this.update()
    }

    get needAlignFillingsByLightAperture(): boolean {
        const isCondition1 = this.frame.impostBeams.every(i => i.isVertical && i.length === this.height)
        if (!isCondition1) {
            return false
        }
        const {allSolidFillings} = this.frame

        const firstSolidFilling = allSolidFillings[0]

        return !allSolidFillings.every(s =>
            approximatelyEqual(s.containerPolygon.innerPolygon.rect.size.width, firstSolidFilling.containerPolygon.innerPolygon.rect.size.width)
        )
    }

    get availableShtulpGroups(): ShtulpGroup[] {
        const groups: ShtulpGroup[] = []

        this.frame.impostBeams
            .filter(i => i.isVertical)
            .forEach(impost => {
                const {containerPolygon} = impost
                if (containerPolygon.childrenPolygons.length !== 2 || !containerPolygon.isRect) {
                    return
                }

                const fillings = this.frame.innerFillings
                    .filter(f => containerPolygon.childrenPolygons.some(p => p.rect.hasPointInside(f.child.fillingPoint)))
                    .map(f => f.child)

                if (fillings.length !== 2) {
                    return
                }

                groups.push(new ShtulpGroup(fillings[0], fillings[1], impost))
            })

        return groups
    }

    findAvailableShtulpGroups(context: FrameFilling | SolidFilling | Impost): ShtulpGroup[] {
        return this.availableShtulpGroups.filter(g => g.hasNode(context))
    }

    get usedModelParts(): ModelParts[] {
        const partsSet = new Set<ModelParts>()

        this.frame.allBeams.concat(this.frame.allImposts).forEach(beam => partsSet.add(beam.modelPart))

        return Array.from(partsSet)
    }

    getAvailableProfilesFilteredByModelParts(modelParts: ModelParts[]): Profile[] {
        return this._availableProfiles.filter(profile => modelParts.every(mPart => profile.parts.find(part => part.modelPart === mPart)))
    }

    getAvailablePartsByBeam(beam: Beam): GeometryParameters[] {
        return this.profile.parts.filter(part => {
            if (part.modelPart === beam.modelPart) {
                return true
            }
            if (this.isEntranceDoor && beam.nodeId === this.frame.bottommostBeam.nodeId) {
                if (part.modelPart === ModelParts.Porog) {
                    return true
                }
                if (this.frame.frameBeams.some(b => b.modelPart === part.modelPart)) {
                    return true
                }
            }

            return false
        })
    }

    private _updateConnectors(): void {
        this.connectors.forEach(connector => {
            const hostBeam = this.findHostBeam(connector)
            if (!hostBeam) {
                return
            }

            if (connector.isVertical) {
                const isConnectedToRight = connector.start.x >= this.frame.rightmostBeam.start.x

                connector.innerDrawingPoints = new Rectangle(
                    new Point(connector.rect.refPoint.x + (isConnectedToRight ? 0 : -connector.width), connector.rect.refPoint.y),
                    new Size(connector.width, connector.length)
                ).drawingPoints
            } else if (connector.isHorizontal) {
                const isConnectedToTop = connector.start.y >= this.frame.topmostBeam.start.y

                connector.innerDrawingPoints = new Rectangle(
                    new Point(connector.rect.refPoint.x, connector.rect.refPoint.y + (isConnectedToTop ? 0 : -connector.width)),
                    new Size(connector.length, connector.width)
                ).drawingPoints
            }
        })
    }

    private _getSideLastBeam(beam: Beam /*, onlyExtenders = false*/): Beam {
        // let lastBeam = beam

        // this.connectors
        //     .filter(c => (onlyExtenders ? c.connectorType === ConnectorTypes.Extender : true))
        //     .forEach(extender => {
        //         if (extender.connectedBeamGuid === lastBeam.nodeId) {
        //             lastBeam = extender
        //         }
        //     })

        // return lastBeam
        //
        // TODO: Обсудить, нужен ли соеденитель, если уже имеется расширитель.
        // Текущая (закомментированная) логика возвращает концевой балкой расширитель, если он имеется.
        // Если расширителя нет, то концевой балкой является, логично, балка изделия

        return beam
    }

    haveConnectorOnSide(side: string): Connector | undefined {
        type TSidesIndexes = {
            [key: string]: number
        }

        const sidesToIndex: TSidesIndexes = {
            left: 1,
            right: 3,
            top: 2,
            bottom: 0
        }
        return this.connectors.find(connector => this.frame.frameBeams[sidesToIndex[side]].beamGuid === connector.connectedBeamGuid)
    }
}
