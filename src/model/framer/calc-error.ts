import {ErrorTypes} from './error-types'

export interface ICalcErrorJSON {
    errorType?: ErrorTypes
    errorName: string
    errorMessage: string
    errorAdditionalInfo: string
    positionName: string
    productionGuid: string
}

export class CalcError {
    static parse(calcErrorJSON: ICalcErrorJSON): CalcError {
        const calcError = new CalcError()
        Object.assign(calcError, calcErrorJSON)
        return calcError
    }

    errorType?: ErrorTypes
    errorName!: string
    errorMessage!: string
    errorAdditionalInfo!: string
    positionName!: string
    productionGuid!: string

    get displayName(): string {
        return this.errorName.length > 0 ? this.errorName : this.errorMessage
    }

    get displayMessage(): string {
        let msg = this.errorAdditionalInfo
        if (msg.length === 0) {
            msg = this.errorMessage
        }
        if (msg.length === 0) {
            msg = this.errorName
        }
        return msg
    }
}
