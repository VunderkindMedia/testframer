export interface IRegionJSON {
    id: number
    name: string
    kladrRegionId: string
    kladrRegionName: string
}

export class Region {
    static parse(regionJSON: IRegionJSON): Region {
        return new Region(regionJSON.id, regionJSON.name, regionJSON.kladrRegionId, regionJSON.kladrRegionName)
    }

    id: number
    name: string
    kladrRegionId: string
    kladrRegionName: string

    constructor(id: number, name: string, kladrRegionId: string, kladrRegionName: string) {
        this.id = id
        this.name = name
        this.kladrRegionId = kladrRegionId
        this.kladrRegionName = kladrRegionName
    }

    get isMoscow(): boolean {
        return this.kladrRegionId === '77'
    }
}
