export interface IGoodUnitJSON {
    id: number
    name: string
    shortName: string
    factor: number
}

export class GoodUnit implements IGoodUnitJSON {
    static parse(goodUnitJSON: IGoodUnitJSON): GoodUnit {
        return new GoodUnit(goodUnitJSON.id, goodUnitJSON.name, goodUnitJSON.shortName, goodUnitJSON.factor)
    }

    id: number
    name: string
    shortName: string
    factor: number

    constructor(id: number, name: string, shortName: string, factor: number) {
        this.id = id
        this.name = name
        this.shortName = shortName
        this.factor = factor
    }
}
