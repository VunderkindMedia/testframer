export enum HandleColors {
    White = 'Белый',
    Bronze = 'Бронза',
    DarkBronze = 'Темная бронза',
    Gold = 'Золото',
    GoldMatt = 'Золото матовое',
    BrownPerm = 'Коричневая',
    BrownMsk = 'Коричневый',
    Silver = 'Серебро',
    Titanium = 'Титан',
    Champagne = 'Шампань'
}

export const getHandleColorHex = (handleColor: HandleColors): string => {
    switch (handleColor) {
        case HandleColors.Bronze:
        case HandleColors.DarkBronze:
        case HandleColors.Gold:
        case HandleColors.GoldMatt:
            return '#9d8572'
        case HandleColors.BrownPerm:
        case HandleColors.BrownMsk:
            return '#2e160e'
        case HandleColors.Silver:
            return '#c4c4c4'
        case HandleColors.Titanium:
            return '#41403f'
        case HandleColors.Champagne:
            return '#c1ba9f'
        default:
            return '#fff'
    }
}
