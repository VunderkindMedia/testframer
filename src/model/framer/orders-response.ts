import {IOrderJSON, Order} from './order'

export interface IOrdersResponseJSON {
    results?: IOrderJSON[]
    offset: number
    limit: number
    // eslint-disable-next-line @typescript-eslint/naming-convention
    total_count: number
}

export class OrdersResponse {
    static parse(ordersResponseJSON: IOrdersResponseJSON): OrdersResponse {
        const response = new OrdersResponse()

        ordersResponseJSON.results &&
            ordersResponseJSON.results.forEach(orderObj => {
                try {
                    const order = Order.parse(orderObj)
                    response.orders.push(order)
                } catch (e) {
                    console.error(e)
                    console.warn(`Order ${orderObj.id} skipped`)
                }
            })
        response.offset = ordersResponseJSON.offset || 0
        response.limit = ordersResponseJSON.limit
        response.totalCount = ordersResponseJSON.total_count || 0

        return response
    }

    orders: Order[] = []
    offset = 0
    limit = 2
    totalCount = 0
}
