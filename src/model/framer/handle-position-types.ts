export enum HandlePositionTypes {
    Center = 'center',
    Fixed = 'fixed'
}
