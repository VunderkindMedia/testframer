import {Roles} from './user-permissions'

export interface IEmployeeJSON {
    id: number
    filialId: number
    userRoles: Roles[]
    email: string
    name: string
}

export class Employee {
    static parse(employeeJSON: IEmployeeJSON): Employee {
        const {id, filialId, userRoles, email, name} = employeeJSON
        return new Employee(id, filialId, userRoles, email, name)
    }

    constructor(id: number, filialId: number, userRoles: Roles[], email: string, name: string) {
        this.id = id
        this.filialId = filialId
        this.userRoles = userRoles
        this.email = email
        this.name = name
    }

    id?: number
    filialId: number
    userRoles: Roles[]
    email: string
    name: string
}
