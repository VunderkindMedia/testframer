export interface IPricesJSON {
    price: number
    priceWithDiscount: number
}

export class Prices {
    static parse(pricesJSON: IPricesJSON): Prices {
        return new Prices(pricesJSON.price, pricesJSON.priceWithDiscount)
    }

    price: number
    priceWithDiscount: number

    constructor(price: number, priceWithDiscount: number) {
        this.price = price
        this.priceWithDiscount = priceWithDiscount
    }
}

export interface IFinParameterJSON {
    id: number
    sm: number
    name: string
}

export class FinParameter {
    static parse(finParameterJSON: IFinParameterJSON): FinParameter {
        return new FinParameter(finParameterJSON.id, finParameterJSON.sm, finParameterJSON.name)
    }

    id: number
    sm: number
    name: string

    constructor(id: number, sm: number, name: string) {
        this.id = id
        this.sm = sm
        this.name = name
    }
}

export interface IProductResultJSON {
    prices: IPricesJSON
    square: number
    weight: number
    productId: string
    finParameters: IFinParameterJSON[]
}

export class ProductResult {
    static parse(productResultJSON: IProductResultJSON): ProductResult {
        const productResult = new ProductResult()
        Object.assign(productResult, productResultJSON)

        productResult.prices = Prices.parse(productResultJSON.prices)
        productResult.finParameters = productResultJSON.finParameters.map(f => FinParameter.parse(f))

        return productResult
    }

    prices!: Prices
    square!: number
    weight!: number
    productId!: string
    finParameters: FinParameter[] = []
}
