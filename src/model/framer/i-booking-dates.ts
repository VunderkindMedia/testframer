export interface ISBookingDates {
    dates: string[] | null
    isOrderBooked: boolean
}
