export interface IPaymentResultJSON {
    id: number
    dealerId: number
    totalAmegaCost: number
    isPaid: boolean
    paidAt: string
    sberOrderId: string
    sberFormURL: string
}

export class PaymentResult {
    static parse(paymentResultJSON: IPaymentResultJSON): PaymentResult {
        const paymentResult = new PaymentResult()
        Object.assign(paymentResult, paymentResultJSON)
        return paymentResult
    }

    id!: number
    dealerId!: number
    totalAmegaCost!: number
    isPaid!: boolean
    paidAt!: string
    sberOrderId!: string
    sberFormURL!: string
}
