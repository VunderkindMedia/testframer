import {IPointJSON, Point} from '../geometry/point'
import {IContext} from '../i-context'
import {Segment} from '../geometry/segment'
import {ModelParts} from './model-parts'
import {BEAD_WIDTH} from '../../constants/geometry-params'
import {getClone} from '../../helpers/json'
import {GUID} from '../../helpers/guid'
import {IUserParameterJSON, UserParameter} from './user-parameter/user-parameter'

export interface IBeamJSON {
    beamGuid?: string
    profileVendorCode?: string
    modelPart?: ModelParts
    start: IPointJSON
    end: IPointJSON
    userParameters?: IUserParameterJSON[]
    hardcodedUserParameters?: IUserParameterJSON[]
}

export class Beam extends Segment implements IContext {
    static parse(beamJSON: IBeamJSON, parentNodeId: string, defaultModelPart: ModelParts = ModelParts.Rama): Beam {
        const beam = new Beam()
        Object.assign(beam, beamJSON)
        beam._parentNodeId = parentNodeId
        beam.start = Point.parse(beamJSON.start)
        beam.end = Point.parse(beamJSON.end)
        beam.modelPart = beamJSON.modelPart ?? defaultModelPart
        beamJSON.userParameters && (beam.userParameters = beamJSON.userParameters.map(u => UserParameter.parse(u)))
        beamJSON.hardcodedUserParameters && (beam.hardcodedUserParameters = beamJSON.hardcodedUserParameters.map(u => UserParameter.parse(u)))
        return beam
    }

    beamGuid: string = GUID()
    profileVendorCode: string
    modelPart = ModelParts.Rama
    userParameters: UserParameter[] = []
    hardcodedUserParameters: UserParameter[] = []
    protected _parentNodeId!: string
    protected _innerDrawingPoints: Point[] = []
    protected _outerDrawingPoints: Point[] = []
    private _isConnectionPart = false
    private _hasInnerWidth = false

    constructor(start: Point = Point.zero, end: Point = Point.zero, profileVendorCode: string | null | undefined = '') {
        super(start, end)
        this.profileVendorCode = profileVendorCode ?? ''
    }

    get clone(): Beam {
        return Beam.parse(getClone<IBeamJSON>(this), this.parentNodeId)
    }

    get nodeId(): string {
        return this.beamGuid
    }

    set nodeId(nodeId: string) {
        this.beamGuid = nodeId
    }

    get innerDrawingPoints(): Point[] {
        return this._innerDrawingPoints
    }

    set innerDrawingPoints(points: Point[]) {
        this._innerDrawingPoints = points
    }

    get outerDrawingPoints(): Point[] {
        return this._outerDrawingPoints
    }

    set outerDrawingPoints(points: Point[]) {
        this._outerDrawingPoints = points
    }

    get parentNodeId(): string {
        return this._parentNodeId
    }

    set parentNodeId(parentNodeId: string) {
        this._parentNodeId = parentNodeId
    }

    set isConnectionPart(isConnectionPart: boolean) {
        this._isConnectionPart = isConnectionPart
    }

    get isConnectionPart(): boolean {
        return this._isConnectionPart
    }

    get hasInnerWidth(): boolean {
        return this._hasInnerWidth
    }

    set hasInnerWidth(hasInnerWidth: boolean) {
        this._hasInnerWidth = hasInnerWidth
    }

    get innerWidth(): number {
        if (!this.hasInnerWidth) {
            return this.width
        }
        if (this.modelPart === ModelParts.Porog) {
            return this.width
        }
        if (this.modelPart === ModelParts.Impost || this.modelPart === ModelParts.Shtulp) {
            return this.width - 2 * BEAD_WIDTH
        }

        return this.width - BEAD_WIDTH
    }
}
