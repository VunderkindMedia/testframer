export interface ICalendarRequestEvent {
    id: number
    type: string
    orderId?: number
    partnerId?: number
    filialId?: number
    clientId?: number
    executorsIds: number[]
    datetime: string
    note?: string
    durationMin?: number
}
