import {Client, IClientJSON} from '../client'

export interface ICalendarEventJson {
    id: number
    createdAt: string
    updatedAt: string
    orderId: number
    partnerId?: number
    filialId?: number
    client?: IClientJSON
    datetime: string
    note: string
    status: string
    durationMin?: number
}

export interface ICalendarCustomEventJson extends ICalendarEventJson {
    body: string
    type: string
    measuringId?: number
    mountingId?: number
    shippingId?: number
}

export class Event {
    static parse(eventJSON: ICalendarCustomEventJson | ICalendarEventJson): Event {
        const event = new Event()
        Object.assign(event, eventJSON)

        if (eventJSON.client) {
            event.client = Client.parse(eventJSON.client)
        }

        return event
    }

    id = -1
    createdAt = ''
    orderId = -1
    filialId? = -1
    client: Client | null = null
    datetime = ''
    note = ''
    status = ''
}
