import {CalendarEventTypes} from './calendar-event-types'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {ICalendarResponseJson} from './i-calendar-response-json'
import {Event, ICalendarCustomEventJson, ICalendarEventJson} from './event'

export class CalendarEvent {
    constructor(originalEvent: ICalendarEventJson | ICalendarCustomEventJson, eventType: CalendarEventTypes) {
        this.originalEvent = Event.parse(originalEvent)
        this.eventType = eventType
        this.date = getDateDDMMYYYYFormat(new Date(originalEvent.datetime))
    }

    static parseEvents(responseJson: ICalendarResponseJson): CalendarEvent[] {
        const {results} = responseJson
        const events: CalendarEvent[] = []

        results.measurings.forEach(e => events.push(new CalendarEvent(e, CalendarEventTypes.Measuring)))
        results.shippings.forEach(e => events.push(new CalendarEvent(e, CalendarEventTypes.Shipping)))
        results.mountings.forEach(e => events.push(new CalendarEvent(e, CalendarEventTypes.Mounting)))
        results.otherEvents.forEach(e => events.push(new CalendarEvent(e, CalendarEventTypes.OtherEvent)))

        return events
    }

    date: string
    originalEvent: Event
    eventType: CalendarEventTypes
}
