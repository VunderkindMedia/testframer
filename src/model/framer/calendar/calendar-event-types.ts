export enum CalendarEventTypes {
    Measuring = 'measuring',
    Mounting = 'mounting',
    Shipping = 'shipping',
    OtherEvent = 'event'
}

export const getCalendarEventName = (type: CalendarEventTypes): string => {
    switch (type) {
        case CalendarEventTypes.Measuring:
            return 'Замер'
        case CalendarEventTypes.Mounting:
            return 'Монтаж'
        case CalendarEventTypes.Shipping:
            return 'Доставка'
        default:
            return 'Другое'
    }
}
