import {ICalendarEventJson, ICalendarCustomEventJson} from './event'

export interface ICalendarResponseJson {
    offset: number
    limit: number
    // eslint-disable-next-line @typescript-eslint/naming-convention
    total_count: number
    results: {
        measurings: ICalendarEventJson[]
        shippings: ICalendarEventJson[]
        mountings: ICalendarEventJson[]
        otherEvents: ICalendarCustomEventJson[]
    }
}
