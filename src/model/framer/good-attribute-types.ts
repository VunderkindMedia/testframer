export enum GoodAttributeTypes {
    Number = 'number',
    Color = 'color'
}
