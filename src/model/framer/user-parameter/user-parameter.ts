export interface IUserParameterJSON {
    id: number
    value: string
    title?: string
    hex?: string
}

export class UserParameter {
    static parse(userParameterJSON: IUserParameterJSON): UserParameter {
        const {id, value, title, hex} = userParameterJSON
        return new UserParameter(id, value, title, hex)
    }

    constructor(readonly id: number, public value: string, public title: string | undefined, public hex: string | undefined) {
        this.id = id
        this.value = value
        this.title = title
        this.hex = hex
    }
}
