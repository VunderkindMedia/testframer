export interface IUserParameterOptionJSON {
    id: number
    value: string
    hex?: string
}

export class UserParameterOption {
    static parse(userParameterOptionJSON: IUserParameterOptionJSON): UserParameterOption {
        return new UserParameterOption(userParameterOptionJSON.id, userParameterOptionJSON.value, userParameterOptionJSON.hex)
    }

    constructor(readonly id: number, public value: string, public hex?: string) {
        this.id = id
        this.value = value
        this.hex = hex
    }
}
