import {IUserParameterOptionJSON, UserParameterOption} from './user-parameter-option'

export interface IUserParameterConfigJSON {
    id: number
    name: string
    defaultValueId: number
    values: IUserParameterOptionJSON[] | null
    colors: IUserParameterOptionJSON[] | null
}

export class UserParameterConfig {
    static parse(userParameterConfigJSON: IUserParameterConfigJSON): UserParameterConfig {
        const isColor = Boolean(userParameterConfigJSON.colors)
        const values = isColor ? userParameterConfigJSON.colors : userParameterConfigJSON.values

        return new UserParameterConfig(
            userParameterConfigJSON.id,
            userParameterConfigJSON.name,
            userParameterConfigJSON.defaultValueId,
            values?.map(obj => UserParameterOption.parse(obj)) ?? null,
            isColor
        )
    }

    constructor(
        readonly id: number,
        readonly name: string,
        readonly defaultValueId: number,
        readonly values: UserParameterOption[] | null,
        readonly isColor: boolean
    ) {}
}
