import {IUserParameterConfigJSON, UserParameterConfig} from './user-parameter-config'

export interface IPartUserParameterJSON {
    partGuid: string
    userParameters: IUserParameterConfigJSON[]
}

export class PartUserParameter {
    static parse(partUserParametersJSON: IPartUserParameterJSON): PartUserParameter {
        const {partGuid, userParameters} = partUserParametersJSON
        return new PartUserParameter(
            partGuid,
            userParameters.map(p => UserParameterConfig.parse(p))
        )
    }

    constructor(readonly partGuid: string, readonly userParameters: UserParameterConfig[]) {
        this.partGuid = partGuid
        this.userParameters = userParameters
    }
}
