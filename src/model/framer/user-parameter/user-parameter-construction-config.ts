import {IPartUserParameterJSON, PartUserParameter} from './part-user-parameter'

export interface IUserParameterConstructionConfigJSON {
    productGuid: string
    partUserParameters: IPartUserParameterJSON[]
}

export class UserParameterConstructionConfig {
    static parse(userParameterConstructionConfig: IUserParameterConstructionConfigJSON): UserParameterConstructionConfig {
        const {productGuid, partUserParameters} = userParameterConstructionConfig
        return new UserParameterConstructionConfig(
            productGuid,
            partUserParameters.map(p => PartUserParameter.parse(p))
        )
    }

    constructor(readonly productGuid: string, readonly partUserParameters: PartUserParameter[]) {
        this.productGuid = productGuid
        this.partUserParameters = partUserParameters
    }
}
