export enum HandleTypes {
    MacoHarmony = 'Масо Гармония',
    MacoRhapsody = 'Maco Рапсодия',
    Roto = 'Roto',
    OnBothSides = 'С двух сторон',
    OnBothSidesWithKey = 'С двух сторон с ключом',
    Lira = 'Лира',
    Customer = 'Ручка оконная заказчика',
    Locked = 'Ручка с личинкой',
    MacbethPerm = 'МакБет',
    MacbethMsk = 'Ручка МакБет'
}
