import {Order, IOrderJSON} from './order'
import {Employee, IEmployeeJSON} from './employee'
import {Client, IClientJSON} from './client'
import {getClone} from '../../helpers/json'

export interface IMountingJSON {
    id: number
    orderId: number
    order: IOrderJSON
    createdAt: string
    owner: IEmployeeJSON
    client: IClientJSON
    note: string
    executorsIds: number[]
    datetime: string
}

export class Mounting {
    static parse(mountingJSON: IMountingJSON): Mounting {
        const mounting = new Mounting()
        Object.assign(mounting, mountingJSON)

        mountingJSON.order && (mounting.order = Order.parse(mountingJSON.order))
        mounting.owner = mountingJSON.owner ? Employee.parse(mountingJSON.owner) : null
        mounting.client = mountingJSON.client ? Client.parse(mountingJSON.client) : null

        return mounting
    }

    id!: number
    orderId!: number
    order: Order = new Order()
    createdAt!: string
    owner: Employee | null = null
    client: Client | null = null
    note = ''
    executorsIds: number[] = []
    datetime = ''

    get clone(): Mounting {
        return Mounting.parse(getClone<IMountingJSON>(this))
    }
}
