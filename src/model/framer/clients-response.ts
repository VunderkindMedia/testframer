import {Client, IClientJSON} from './client'

export interface IClientsResponseJSON {
    results: IClientJSON[]
    // eslint-disable-next-line @typescript-eslint/naming-convention
    total_count: number
}

export class ClientsResponse {
    static parse(clientsResponseJSON: IClientsResponseJSON): ClientsResponse {
        const response = new ClientsResponse()

        response.clients = clientsResponseJSON.results.map(clientObj => Client.parse(clientObj))
        response.totalCount = clientsResponseJSON.total_count

        return response
    }

    clients: Client[] = []
    totalCount = 0
}
