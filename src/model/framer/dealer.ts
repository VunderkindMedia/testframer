import {BankAccount, IBankAccountJSON} from './bank-account'
import {ContactInfo, IContactInfoJSON} from './contact-info'
import {IServiceJSON, Service} from './service'
import {FactoryTypes} from './factory-types'
import {DealerInfo} from './dealer-info'

export enum DealerConfirmationStatus {
    DealerNotConfirmed = 'DealerNotConfirmed',
    DealerPendingConfirmation = 'DealerPendingConfirmation',
    DealerConfirmed = 'DealerConfirmed'
}

export interface IDealerJSON {
    id: number
    externalId: string
    name: string
    type: number
    bankAccount: IBankAccountJSON
    contactInfo: IContactInfoJSON
    services: IServiceJSON[]
    billsNum: number
    isDeliveryAvailable: boolean
    balance: number
    creditLimitInCents: number
    framerCreditLimitInCents: number
    factoryId: FactoryTypes
    confirmationStatus: DealerConfirmationStatus
}

export class Dealer {
    static parse(dealerJSON: IDealerJSON): Dealer {
        const dealer = new Dealer()
        Object.assign(dealer, dealerJSON)

        dealer.bankAccount = BankAccount.parse(dealerJSON.bankAccount)
        dealer.contactInfo = ContactInfo.parse(dealerJSON.contactInfo)
        dealer.services = (dealerJSON.services ?? []).map(s => Service.parse(s))
        dealer.isDeliveryAvailable = Boolean(dealerJSON.isDeliveryAvailable)
        dealer.confirmationStatus = DealerConfirmationStatus[dealerJSON.confirmationStatus]

        return dealer
    }

    id!: number
    externalId!: string
    name!: string
    type!: number
    bankAccount!: BankAccount
    contactInfo!: ContactInfo
    services: Service[] = []
    billsNum!: number
    isDeliveryAvailable!: boolean
    balance = 0
    creditLimitInCents = 0
    framerCreditLimitInCents = 0
    factoryId!: FactoryTypes
    confirmationStatus!: DealerConfirmationStatus
    info?: DealerInfo
}
