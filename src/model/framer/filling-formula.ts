export enum SolidFillingPartType {
    Glass,
    Frame
}

export interface ISolidFillingPart {
    type: SolidFillingPartType
    value: string
    id: number
    hasArgon?: boolean
    hasPvc?: boolean
    tintFilm?: string
}
