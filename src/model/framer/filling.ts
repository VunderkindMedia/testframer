import {getClone} from '../../helpers/json'

export interface IFillingJSON {
    id: number
    name: string
    marking: string
    thickness: number
    camernost: number
    group: string
    isLowEmission: boolean
    isMultifunctional: boolean
    isDefault: boolean
}

export class Filling {
    static parse(fillingJSON: IFillingJSON): Filling {
        const filling = new Filling()
        Object.assign(filling, fillingJSON)
        return filling
    }

    clone(): Filling {
        return Filling.parse(getClone(this))
    }

    id!: number
    name!: string
    marking!: string
    thickness!: number
    camernost!: number
    group!: string
    isLowEmission!: boolean
    isMultifunctional!: boolean
    isDefault = false

    get isEnergy(): boolean {
        return this.isLowEmission || this.isMultifunctional
    }
}
