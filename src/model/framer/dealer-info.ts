import {IDadataSuggestion} from '../../kladr-manager'

export enum DealerType {
    Legal = 'LEGAL',
    Individual = 'INDIVIDUAL',
    SelfEmployed = 'SELF_EMPLOYED',
    Person = 'PERSON'
}

export const getDealerTypeName = (type: DealerType): string => {
    switch (type) {
        case DealerType.Legal:
            return 'Юридическое лицо'
        case DealerType.Individual:
            return 'ИП'
        case DealerType.SelfEmployed:
            return 'Самозанятый'
        case DealerType.Person:
            return 'Физическое лицо'
        default:
            return ''
    }
}

export interface IDealerInfoJSON {
    inn: string
    name: string
    type: DealerType
    kpp: string
}

export class DealerInfo {
    static parse(dealerInfoJSON: IDealerInfoJSON): DealerInfo {
        return new DealerInfo(dealerInfoJSON.inn, dealerInfoJSON.kpp, dealerInfoJSON.name, dealerInfoJSON.type)
    }

    constructor(
        inn: string,
        kpp: string,
        dealerName: string,
        dealerType: DealerType,
        id?: number,
        contactName?: string,
        contactPhone?: string,
        contactEmail?: string,
        legalAddress?: IDadataSuggestion,
        physicalAddress?: IDadataSuggestion
    ) {
        this.inn = inn
        this.kpp = kpp
        this.dealerName = dealerName
        this.dealerType = dealerType
        this.id = id
        this.contactName = contactName
        this.contactPhone = contactPhone
        this.contactEmail = contactEmail
        this.legalAddress = legalAddress
        this.physicalAddress = physicalAddress
    }

    id?: number
    inn: string
    kpp: string
    dealerName: string
    dealerType: DealerType
    contactName?: string
    contactPhone?: string
    contactEmail?: string
    legalAddress?: IDadataSuggestion
    physicalAddress?: IDadataSuggestion
}
