import {useSelector} from 'react-redux'
import {IAppState} from '../../redux/root-reducer'

export enum ConstructionTypes {
    Okno = 2,
    BalkonnayaDver = 3,
    DverMKOtkryvanieVoVnutr = 4,
    DverMKOtkryvanieNaruzhu = 5,
    DverVhodnayaOtkryvanieVoVnutr = 6,
    DverVhodnayaOtkryvanieNaruzhu = 7,
    MoskitnayaSetka = 8,
    Steklopaket = 9,
    MoskitnayaSetkaDvernaya = 10,
    Sendvich = 11,
    Stvorka = 12,
    AlyuminievayaKonstrukciya = 13,
    DverMKOtkryvanieVoVnutr82mm = 15,
    DverMKOtkryvanieVoVnutr75mm = 16,
    MoskitnayaSetkaRazdvignaya = 17,
    Steklo = 19
}

export const useTypeConstructionVisible = (): string => {
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    switch (currentProduct?.constructionTypeId) {
        case ConstructionTypes.Okno:
            return 'Окно'
        case ConstructionTypes.BalkonnayaDver:
            return 'Балконная дверь'
        case ConstructionTypes.DverMKOtkryvanieVoVnutr:
            return 'Дверь МК открывание во внутрь'
        case ConstructionTypes.DverMKOtkryvanieNaruzhu:
            return 'Дверь МК открывание наружу 82 мм'
        case ConstructionTypes.DverVhodnayaOtkryvanieVoVnutr:
            return 'Дверь входная открывание во внутрь'
        case ConstructionTypes.DverVhodnayaOtkryvanieNaruzhu:
            return 'Дверь входная открывание наружу'
        case ConstructionTypes.MoskitnayaSetka:
            return 'Москитная сетка'
        case ConstructionTypes.Steklopaket:
            return 'Стеклопакет'
        case ConstructionTypes.MoskitnayaSetkaDvernaya:
            return 'Москитная сетка дверная'
        case ConstructionTypes.Sendvich:
            return 'Сэндвич'
        case ConstructionTypes.Stvorka:
            return 'Створка'
        case ConstructionTypes.AlyuminievayaKonstrukciya:
            return 'Алюминиевая конструкция'
        case ConstructionTypes.DverMKOtkryvanieVoVnutr82mm:
            return 'Дверь МК открывание во внутрь 82 мм'
        case ConstructionTypes.DverMKOtkryvanieVoVnutr75mm:
            return 'Дверь МК открывание во внутрь 75 мм'
        case ConstructionTypes.MoskitnayaSetkaRazdvignaya:
            return 'Москитная сетка раздвижная'
        case ConstructionTypes.Steklo:
            return 'Стекло'
        default:
            return ''
    }
}

// idconstructiontype	name
// 2	Окно
// 3	Балконная дверь
// 4	Дверь МК открывание во внутрь
// 6	Дверь входная открывание во внутрь
// 7	Дверь входная открывание наружу
// 8	Москитная сетка
// 9	Стеклопакет
// 10	Москитная сетка дверная
// 11	Сэндвич
// 12	Створка
// 13	Алюминиевая конструкция
// 17	Москитная сетка раздвижная
// 19	Стекло
// 23	Холодильник
// 25	Дверь МК открывание во внутрь легкая
// 27	Аргон
// 28	Уголок образец оконный
// 35	Дверь МК открывание во внутрь
// 36	Дверь МК открывание наружу
// 37	Окно со скрытой створкой
// 38	Витраж
// 39	Перегородка
// 41	мд_Эстет/Премиум
// 42	Хрущевский холодильник
// 43	мд_Эталон дизайн/Стандарт Арт
// 45	мд_Комфорт/Оптима
// 46	мд_Эталон/Стандарт
// 47	мд_Эксклюзив/Премиум Эксклюзив
// 48	мд_Эстет Дизайн/Премиум Арт
// 49	мд_Универсал
// 50	мд_Универсал плюс
// 52	мд_Техно/ТеплоДверь
// 55	Дверь МК из оконного профиля
// 56	Составное заполнение
// 57	Вентрешетка
// 59	мд_Универсал_нестандарт
// 60	Оцинковка
// 61	Вентрешетка IP45
// 62	мд_заполнение
// 63	Образец на петле
