export interface IShippingResponse {
    shippingAvailable: boolean
    cost: number
    shippingToFloor: number
    daysOfWeekForFreeShipping?: string | null
}
