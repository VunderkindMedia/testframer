export interface IBonusScoreJSON {
    potentialScore: number
    reservedScore?: number
    availableScore: number
    earnedScore: number
}

export class BonusScore {
    static parse(bonusScoreJSON: IBonusScoreJSON): BonusScore {
        const {potentialScore, reservedScore, availableScore, earnedScore} = bonusScoreJSON
        return new BonusScore(potentialScore, reservedScore, availableScore, earnedScore)
    }

    constructor(potentialScore: number, reservedScore: number | undefined, availableScore: number, earnedScore: number) {
        this.potentialScore = potentialScore
        this.reservedScore = reservedScore
        this.availableScore = availableScore
        this.earnedScore = earnedScore
    }

    potentialScore: number
    reservedScore?: number
    availableScore: number
    earnedScore: number
}
