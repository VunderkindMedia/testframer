import {Beam, IBeamJSON} from './beam'
import {Point} from '../geometry/point'
import {Polygon} from '../geometry/polygon'
import {CircularLinkedList} from '../circular-linked-list'
import {APPROXIMATE_BEAM_WIDTH} from '../../constants/geometry-params'
import {ModelParts} from './model-parts'
import {ShtulpTypes} from './shtulp-types'
import {IContext} from '../i-context'
import {getCenter, getOrderedPoints} from '../../helpers/point'
import {getClone} from '../../helpers/json'

export interface IImpostJSON extends IBeamJSON {
    shtulpType?: ShtulpTypes
}

export class Impost extends Beam implements IContext {
    static parse(impostJSON: IImpostJSON, parentNodeId: string): Impost {
        const impost = new Impost()
        Object.assign(impost, impostJSON)
        impost._parentNodeId = parentNodeId
        impost.start = Point.parse(impostJSON.start)
        impost.end = Point.parse(impostJSON.end)
        return impost
    }

    modelPart = ModelParts.Impost
    shtulpType = ShtulpTypes.Impost
    private _level!: number
    private _containerPolygon?: Polygon
    private _parentPolygon?: Polygon
    private _dependencyIds: string[] = []

    constructor(start: Point = Point.zero, end: Point = Point.zero, profileVendorCode: string | null | undefined = '') {
        super(start, end, profileVendorCode)
    }

    get clone(): Impost {
        return Impost.parse(getClone(this), this.parentNodeId)
    }

    get offsetLeft(): number {
        return this.start.x - this.containerPolygon.rect.refPoint.x
    }

    get offsetBottom(): number {
        return this.start.y - this.containerPolygon.rect.refPoint.y
    }

    get parentPolygon(): Polygon {
        if (!this._parentPolygon) {
            throw new Error('Не задан _parentPolygon')
        }
        return this._parentPolygon
    }

    set parentPolygon(polygon: Polygon) {
        this._parentPolygon = polygon
    }

    get center(): Point {
        return getCenter([this.start, this.end])
    }

    get containerPolygon(): Polygon {
        if (!this._containerPolygon) {
            throw new Error('_containerPolygon')
        }
        return this._containerPolygon
    }

    set containerPolygon(containerPolygon: Polygon) {
        this._containerPolygon = containerPolygon

        const getPoints = (impostWidth: number): Point[] => {
            const impostParallels = this.getParallels(impostWidth / 2)

            const polygon = this.containerPolygon.innerPolygon

            const points = getOrderedPoints(
                polygon.getIntersectionPoints(impostParallels[0]).concat(polygon.getIntersectionPoints(impostParallels[1]))
            )

            const pointsCircularLinkedList = new CircularLinkedList(points)

            pointsCircularLinkedList.items.forEach(pointItem => {
                const currentPoint = pointItem.data
                const nextPoint = pointItem.next

                const currentSegment = polygon.segments.find(segment => segment.hasPoint(currentPoint))
                const nextSegment = polygon.segments.find(segment => segment.hasPoint(nextPoint))

                if (currentSegment && nextSegment && currentSegment !== nextSegment && currentSegment.hasCommonStartOrEnd(nextSegment)) {
                    const intersectionPoint = currentSegment.findIntersectionPoint(nextSegment)

                    if (!intersectionPoint) {
                        return
                    }

                    const d1 = intersectionPoint.distanceTo(currentPoint)
                    const d2 = intersectionPoint.distanceTo(nextPoint)

                    if (d1 < APPROXIMATE_BEAM_WIDTH || d2 < APPROXIMATE_BEAM_WIDTH) {
                        const index = points.findIndex(p => p.equals(currentPoint))
                        points.splice(index + 1, 0, intersectionPoint)
                    }
                }
            })

            return points
        }

        this._innerDrawingPoints = getPoints(this.innerWidth)
        this._outerDrawingPoints = getPoints(this.width)
    }

    get level(): number {
        return this._level
    }

    set level(level: number) {
        this._level = level
    }

    get dependencyIds(): string[] {
        return this._dependencyIds
    }

    set dependencyIds(dependencyIds: string[]) {
        this._dependencyIds = dependencyIds
    }
}
