import {Beam, IBeamJSON} from './beam'
import {IInnerFillingJSON, InnerFilling} from './inner-filling'
import {Size} from '../size'
import {IPointJSON, Point} from '../geometry/point'
import {ALUMINIUM_B, BEAD_WIDTH, DEFAULT_B, MIN_HEIGHT, MIN_WIDTH, POROG_B} from '../../constants/geometry-params'
import {IContext} from '../i-context'
import {SolidFilling} from './solid-filling'
import {IImpostJSON, Impost} from './impost'
import {OpenSides} from './open-sides'
import {OpenTypes} from './open-types'
import {ImpostTypes} from '../impost-types'
import {CircularLinkedList} from '../circular-linked-list'
import {Polygon} from '../geometry/polygon'
import {Line} from '../geometry/line'
import {Segment} from '../geometry/segment'
import {IProfileJSON, Profile} from './profile'
import {Rectangle} from '../geometry/rectangle'
import {IMoskitkaJSON, Moskitka} from './moskitka'
import {HandlePositionTypes} from './handle-position-types'
import {Furniture, IFurnitureJSON} from './furniture'
import {HandleTypes} from './handle-types'
import {HandleColors} from './handle-colors'
import {ModelParts} from './model-parts'
import {ShtulpOpenTypes} from './shtulp-open-types'
import {ShtulpTypes} from './shtulp-types'
import {getUniquePoints} from '../../helpers/point'
import {getBottommostLine, getLeftmostLine, getRightmostLine, getTopmostLine} from '../../helpers/line'
import {round} from '../../helpers/number'
import {GUID} from '../../helpers/guid'
import {safeWhile} from '../../helpers/safe-while'
import {IUserParameterJSON, UserParameter} from './user-parameter/user-parameter'
import {IUserParameterConfigJSON, UserParameterConfig} from './user-parameter/user-parameter-config'
import {FactoryTypes} from './factory-types'
import {IFillingSizeJSON, FillingSize} from './filling-size'

export interface IFrameFillingJSON {
    frameFillingGuid?: string
    openType: OpenTypes
    openSide: OpenSides
    shtulpOpenType?: ShtulpOpenTypes
    frameBeams: IBeamJSON[]
    impostBeams: IImpostJSON[]
    innerFillings: IInnerFillingJSON[]
    fillingPoint: IPointJSON
    profile?: IProfileJSON
    furniture?: IFurnitureJSON
    moskitka?: IMoskitkaJSON | null
    moskitkaSize?: IFillingSizeJSON
    handlePositionType?: HandlePositionTypes
    handleType?: HandleTypes
    handleColor?: HandleColors
    userParameters?: IUserParameterJSON[]
    hardcodedUserParameters?: IUserParameterJSON[]
    availableUserParameterConfigs?: IUserParameterConfigJSON[]
}

export class FrameFilling implements IContext {
    static parse(frameFillingJSON: IFrameFillingJSON, parentNodeId: string | null, profile: Profile, furniture: Furniture): FrameFilling {
        const frame = new FrameFilling()
        Object.assign(frame, frameFillingJSON)
        frame._parentNodeId = parentNodeId
        frame.profile = profile
        frame.furniture = furniture
        frame.fillingPoint = Point.parse(frameFillingJSON.fillingPoint)
        frame.frameBeams = frameFillingJSON.frameBeams.map(beamJSON => {
            const beam = Beam.parse(beamJSON, frame.nodeId, frame.isRoot ? ModelParts.Rama : ModelParts.Stvorka)
            const params = frame.profile.getFirstAvailableGeometryParameters(beam.profileVendorCode, beam.modelPart)
            beam.profileVendorCode = params.marking
            beam.width = params.a
            return beam
        })
        frame.impostBeams = frameFillingJSON.impostBeams.map(impostJSON => {
            const impost = Impost.parse(impostJSON, frame.nodeId)
            const params = frame.profile.getFirstAvailableGeometryParameters(impost.profileVendorCode, impost.modelPart)
            impost.profileVendorCode = params.marking
            impost.width = params.a * 2
            return impost
        })
        frame.innerFillings = frameFillingJSON.innerFillings.map(fillingJSON => {
            const innerFilling = new InnerFilling()
            fillingJSON.leaf && (innerFilling.leaf = FrameFilling.parse(fillingJSON.leaf, frame.nodeId, profile, furniture))
            fillingJSON.solid && (innerFilling.solid = SolidFilling.parse(fillingJSON.solid, frame.nodeId))
            return innerFilling
        })
        frameFillingJSON.moskitka && (frame.moskitka = Moskitka.parse(frameFillingJSON.moskitka))
        if (!frame.moskitka) {
            frame.moskitka = new Moskitka()
        }
        frameFillingJSON.userParameters && (frame.userParameters = frameFillingJSON.userParameters.map(u => UserParameter.parse(u)))
        frameFillingJSON.hardcodedUserParameters &&
            (frame.hardcodedUserParameters = frameFillingJSON.hardcodedUserParameters.map(u => UserParameter.parse(u)))
        frameFillingJSON.availableUserParameterConfigs &&
            (frame.availableUserParameterConfigs = frameFillingJSON.availableUserParameterConfigs.map(c => UserParameterConfig.parse(c)))
        frameFillingJSON.moskitkaSize && (frame.moskitkaSize = FillingSize.parse(frameFillingJSON.moskitkaSize))

        frame.update()

        return frame
    }

    static buildFromSolidFilling(
        solidFilling: SolidFilling,
        parent: FrameFilling,
        openType: OpenTypes,
        openSide: OpenSides,
        shtulpOpenType: ShtulpOpenTypes
    ): FrameFilling {
        const frameFilling = new FrameFilling()

        frameFilling.profile = parent.profile
        frameFilling.furniture = parent.furniture
        frameFilling.nodeId = solidFilling.nodeId
        frameFilling._parentNodeId = solidFilling.parentNodeId
        frameFilling.openType = openType
        frameFilling.openSide = openSide
        frameFilling.shtulpOpenType = shtulpOpenType
        frameFilling.fillingPoint = solidFilling.fillingPoint
        frameFilling.frameBeams = this.getBeamsFromPolygon(solidFilling.containerPolygon, parent)
        const innerSolidFilling = solidFilling.clone()
        innerSolidFilling.nodeId = GUID()
        frameFilling.innerFillings.push(new InnerFilling(innerSolidFilling))
        frameFilling.moskitka = new Moskitka()

        frameFilling.update()

        return frameFilling
    }

    static getBeamsFromPolygon(polygon: Polygon, parentFrameFilling: FrameFilling, leaf: FrameFilling | null = null): Beam[] {
        const beams: Beam[] = []

        const frameFilling = leaf || parentFrameFilling

        if (!frameFilling) {
            throw Error('Нет рабочего заполнения')
        }

        const firstBeam = frameFilling.frameBeams.find(b => b.modelPart === ModelParts.Stvorka)
        let stvorkaGeometryParameters = firstBeam
            ? parentFrameFilling.profile.getFirstAvailableGeometryParameters(firstBeam.profileVendorCode, firstBeam.modelPart)
            : parentFrameFilling.profile.getGeometryParameters(ModelParts.Stvorka)

        if (parentFrameFilling.isP400) {
            const p400StvorkaGeometryParameters = parentFrameFilling.profile
                .getAllGeometryParameters(ModelParts.Stvorka)
                .find(p => p.marking.toLowerCase().includes('p400'))

            if (p400StvorkaGeometryParameters) {
                stvorkaGeometryParameters = p400StvorkaGeometryParameters
            }
        }

        const porogGeometryParameters = parentFrameFilling.profile.parts.filter(p => p.modelPart === ModelParts.Porog)
        const impostGeometryParameters = parentFrameFilling.profile.parts.filter(p => p.modelPart === ModelParts.Impost)
        const shtulpGeometryParameters = parentFrameFilling.profile.parts.filter(p => p.modelPart === ModelParts.Shtulp)

        const polygonClone = polygon.clone
        polygonClone.segments.forEach(s => {
            if (
                porogGeometryParameters.some(param => param.a === s.width) ||
                parentFrameFilling.isMoskitka ||
                parentFrameFilling.isMoskitkaRoot ||
                parentFrameFilling.isAluminium
            ) {
                return
            }
            s.width += BEAD_WIDTH
        })
        const innerSegments = polygonClone.innerSegments.map((segment, index) => {
            segment.width = polygon.segments[index].width
            const oldBeam = leaf?.frameBeams[index]
            if (oldBeam) {
                segment.id = oldBeam.beamGuid
            }
            return segment
        })

        // TODO: надо учесть что раздивижная влево всегда выше
        // так же надо учесть что если радвижка в противопошложные стороны не должно быть нахлеста
        const getB = (segment: Segment): number => {
            const segmentWidth = segment.width

            const beamWidth = segmentWidth + BEAD_WIDTH

            if (leaf?.isAluminium) {
                if (leaf.openType === OpenTypes.Razdvizhnaya) {
                    const {leftmostBeam, rightmostBeam} = leaf

                    if (segment.id === leftmostBeam.beamGuid) {
                        return segmentWidth === 0 ? leftmostBeam.width / 2 : ALUMINIUM_B
                    } else if (segment.id === rightmostBeam.beamGuid) {
                        return segmentWidth === 0 ? rightmostBeam.width / 2 : ALUMINIUM_B
                    }
                }
                return ALUMINIUM_B
            }

            if (porogGeometryParameters.some(param => param.a === segmentWidth)) {
                return POROG_B
            }

            const impostParameters = impostGeometryParameters.find(param => param.a === beamWidth)
            if (impostParameters && impostParameters.a < DEFAULT_B) {
                return impostParameters.a
            }

            const shtulpParameters = shtulpGeometryParameters.find(param => param.a === beamWidth)
            if (shtulpParameters && shtulpParameters.a < DEFAULT_B) {
                return shtulpParameters.a
            }

            return DEFAULT_B
        }

        new CircularLinkedList(innerSegments).items.forEach((item, index) => {
            const prevLine = item.prev
            const currentLine = item.data
            const nextLine = item.next

            let point1: Point | null
            let point2: Point | null

            if (frameFilling.isMoskitka) {
                point1 = prevLine.findIntersectionPoint(currentLine)
                point2 = nextLine.findIntersectionPoint(currentLine)
            } else {
                const prevLineParallels = prevLine.getParallels(getB(prevLine))
                const currentLineParallels = currentLine.getParallels(getB(currentLine))
                const nextLineParallels = nextLine.getParallels(getB(nextLine))

                point1 = prevLineParallels[1].findIntersectionPoint(currentLineParallels[1])
                point2 = nextLineParallels[1].findIntersectionPoint(currentLineParallels[1])
            }

            if (point1 && point2) {
                const beam = new Beam(point1, point2, stvorkaGeometryParameters.marking)
                beam.modelPart = stvorkaGeometryParameters.modelPart
                beam.width = stvorkaGeometryParameters.a
                beam.parentNodeId = frameFilling.nodeId

                if (leaf) {
                    const oldBeam = leaf.frameBeams[index]
                    const beamParams = leaf.profile.getFirstAvailableGeometryParameters(oldBeam.profileVendorCode, oldBeam.modelPart)
                    beam.nodeId = oldBeam.nodeId
                    beam.width = beamParams.a
                    beam.profileVendorCode = beamParams.marking
                }

                beams.push(beam)
            }
        })

        return beams
    }

    frameFillingGuid = GUID()
    openType!: OpenTypes
    openSide!: OpenSides
    shtulpOpenType = ShtulpOpenTypes.NoShtulp
    frameBeams: Beam[] = []
    impostBeams: Impost[] = []
    innerFillings: InnerFilling[] = []
    fillingPoint!: Point
    profile!: Profile
    furniture!: Furniture
    moskitka: Moskitka | null = null
    moskitkaSize?: FillingSize
    handlePositionType: HandlePositionTypes = HandlePositionTypes.Center
    handleType!: HandleTypes
    handleColor: HandleColors = HandleColors.White
    userParameters: UserParameter[] = []
    hardcodedUserParameters: UserParameter[] = []
    availableUserParameterConfigs?: UserParameterConfig[]
    private _parentNodeId: string | null = null
    private _containerPolygon: Polygon = new Polygon()
    private _fillingPolygons: Polygon[] = []
    private _isMoskitkaRoot = false
    private _isMoskitka = false
    private _isEmpty = false
    private _isEntranceDoor = false
    private _isAluminium = false

    get size(): Size {
        return this.rect.size
    }

    set size(size: Size) {
        if (!this.isRoot) {
            throw new Error('Данный метод предназачен только для рутовой створки!')
        }

        const oldSize = this.size
        const newSize = new Size(Math.max(size.width, MIN_WIDTH), Math.max(size.height, MIN_HEIGHT))

        const scaleX = newSize.width / oldSize.width
        const scaleY = newSize.height / oldSize.height

        const newPoints = (beam: Beam): Point[] => {
            const {start, end} = beam
            return [new Point(round(start.x * scaleX), round(start.y * scaleY)), new Point(round(end.x * scaleX), round(end.y * scaleY))]
        }

        const updateBeamsPoints = (beams: Beam[]): void => {
            beams.forEach(beam => {
                const points = newPoints(beam)
                beam.start = points[0]
                beam.end = points[1]
            })
        }

        updateBeamsPoints(this.impostBeams)
        updateBeamsPoints(this.frameBeams)

        this.update()
    }

    setDefaultHandle(factoryId?: FactoryTypes): void {
        this.handleType = factoryId === FactoryTypes.Moscow ? HandleTypes.MacbethMsk : HandleTypes.MacbethPerm
    }

    setProfile(profile: Profile): void {
        this.profile = profile
        this.innerFillings.forEach(it => it.leaf && it.leaf.setProfile(profile))
        this.update()
    }

    setFurniture(furniture: Furniture): void {
        this.furniture = furniture
        this.innerFillings.forEach(it => it.leaf && it.leaf.setFurniture(furniture))
        this.update()
    }

    update(): void {
        this._defineConnections()
        this._updateBeamsAndImpostsGeometryParams()
        this._updateFrameBeamsDrawingPoints()
        this._updateImpostsAndInnerFillings()
    }

    get beamsCircularLinkedList(): CircularLinkedList<Beam> {
        return new CircularLinkedList(this.frameBeams)
    }

    // это точка относительно родительского продукта
    get refPoint(): Point {
        return this.rect.refPoint
    }

    get rect(): Rectangle {
        return Rectangle.buildFromPoints(this.frameBeams.map(beam => beam.start))
    }

    set containerPolygon(polygon: Polygon) {
        this._containerPolygon = polygon
        this.fillingPoint = polygon.center
    }

    get containerPolygon(): Polygon {
        return this._containerPolygon
    }

    get allSolidFillings(): SolidFilling[] {
        const solidFillings: SolidFilling[] = []

        this.innerFillings.forEach(innerFilling => {
            if (innerFilling.leaf) {
                solidFillings.push(...innerFilling.leaf.allSolidFillings)
            } else if (innerFilling.solid) {
                solidFillings.push(innerFilling.solid)
            }
        })

        return solidFillings
    }

    get allFrameFillings(): FrameFilling[] {
        const frameFillings: FrameFilling[] = [this]

        this.innerFillings.forEach(innerFilling => {
            if (innerFilling.leaf) {
                frameFillings.push(...innerFilling.leaf.allFrameFillings)
            }
        })

        return frameFillings
    }

    get allImposts(): Impost[] {
        const imposts: Impost[] = []

        imposts.push(...this.impostBeams)

        this.innerFillings.forEach(innerFilling => {
            if (innerFilling.leaf) {
                imposts.push(...innerFilling.leaf.allImposts)
            }
        })

        return imposts
    }

    get allBeams(): Beam[] {
        const beams: Beam[] = []

        beams.push(...this.frameBeams)

        this.innerFillings.forEach(innerFilling => {
            if (innerFilling.leaf) {
                beams.push(...innerFilling.leaf.allBeams)
            }
        })

        return beams
    }

    get nodeId(): string {
        return this.frameFillingGuid
    }

    set nodeId(nodeId: string) {
        this.frameFillingGuid = nodeId
    }

    get parentNodeId(): string | null {
        return this._parentNodeId
    }

    set parentNodeId(parentNodeId: string | null) {
        this._parentNodeId = parentNodeId
    }

    get isRoot(): boolean {
        return this._parentNodeId === null
    }

    get allPoints(): Point[] {
        const points: Point[] = []

        this.frameBeams.forEach(beam => {
            points.push(beam.start)
            points.push(beam.end)
        })

        this.impostBeams.forEach(impost => {
            points.push(impost.start)
            points.push(impost.end)
        })

        this.innerFillings.forEach(innerFilling => {
            if (innerFilling.leaf) {
                points.push(...innerFilling.leaf.allPoints)
            }
        })

        return points
    }

    get leftmostBeam(): Beam {
        return getLeftmostLine(this.frameBeams)
    }

    get rightmostBeam(): Beam {
        return getRightmostLine(this.frameBeams)
    }

    get topmostBeam(): Beam {
        return getTopmostLine(this.frameBeams)
    }

    get bottommostBeam(): Beam {
        return getBottommostLine(this.frameBeams)
    }

    set isMoskitkaRoot(isMoskitkaRoot: boolean) {
        this._isMoskitkaRoot = isMoskitkaRoot
        this.innerFillings.forEach(innerFilling => {
            if (innerFilling.leaf) {
                innerFilling.leaf.isMoskitka = isMoskitkaRoot
            }
        })
    }

    get isMoskitkaRoot(): boolean {
        return this._isMoskitkaRoot
    }

    set isMoskitka(isMoskitka: boolean) {
        this._isMoskitka = isMoskitka
    }

    get isMoskitka(): boolean {
        return this._isMoskitka
    }

    set isEmpty(isEmpty: boolean) {
        this._isEmpty = isEmpty
    }

    get isEmpty(): boolean {
        return this._isEmpty
    }

    set isEntranceDoor(isEntranceDoor: boolean) {
        this._isEntranceDoor = isEntranceDoor
    }

    get isEntranceDoor(): boolean {
        return this._isEntranceDoor
    }

    set isAluminium(isAluminium: boolean) {
        this._isAluminium = isAluminium
        this.innerFillings.forEach(innerFilling => innerFilling.leaf && (innerFilling.leaf.isAluminium = isAluminium))
    }

    get isAluminium(): boolean {
        return this._isAluminium
    }

    get isP400(): boolean {
        return this.frameBeams.some(b => b.profileVendorCode.toLowerCase().includes('c640/35'))
    }

    findNodeById(nodeId: string): IContext | null {
        if (this.nodeId === nodeId) {
            return this
        }

        const impost = this.impostBeams.find(impost => impost.nodeId === nodeId)
        if (impost) {
            return impost
        }

        const frameBeam = this.frameBeams.find(beam => beam.nodeId === nodeId)
        if (frameBeam) {
            return frameBeam
        }

        let node: IContext | null = null

        this.innerFillings.forEach(innerFilling => {
            if (innerFilling.solid && innerFilling.solid.nodeId === nodeId) {
                node = innerFilling.solid
            } else if (innerFilling.leaf) {
                const localNode = innerFilling.leaf.findNodeById(nodeId)
                if (localNode) {
                    node = localNode
                }
            }
        })

        return node
    }

    updateNode(node: IContext): void {
        const {nodeId} = node

        // эта часть вызывается только при обновлнеии типа на противоположный
        // ОПАСНО
        this.innerFillings.forEach(innerFilling => {
            if (innerFilling.solid) {
                if (innerFilling.solid.nodeId === nodeId) {
                    innerFilling.solid = undefined
                    innerFilling.leaf = node as FrameFilling
                }
            } else if (innerFilling.leaf) {
                if (innerFilling.leaf.nodeId === nodeId) {
                    innerFilling.leaf = undefined
                    innerFilling.solid = node as SolidFilling
                } else {
                    innerFilling.leaf.updateNode(node)
                }
            }
        })

        if (node instanceof Impost) {
            this.updateImpost(node)
        }
    }

    updateImpost(updatedImpost: Impost): void {
        const oldImpostIndex = this.impostBeams.findIndex(impost => impost.nodeId === updatedImpost.nodeId)
        const oldImpost = this.impostBeams[oldImpostIndex]

        if (!oldImpost) {
            return
        }

        const points: Point[] = []
        oldImpost.containerPolygon.segments.forEach(segment => {
            const intersectionPoint = segment.findIntersectionPoint(updatedImpost)
            if (!intersectionPoint || !segment.hasPoint(intersectionPoint)) {
                return
            }
            intersectionPoint && segment.hasPoint(intersectionPoint) && points.push(intersectionPoint)
        })

        const uniquePoints = getUniquePoints(points)

        if (uniquePoints.length !== 2) {
            return
        }

        oldImpost.start = uniquePoints[0]
        oldImpost.end = uniquePoints[1]

        oldImpost.dependencyIds = []

        this.update()
    }

    addImpost(solidFilling: SolidFilling, impostType: ImpostTypes): void {
        const center = solidFilling.containerPolygon.rect.center
        const x = round(center.x)
        const y = round(center.y)
        const isHorizontalImpost = impostType === ImpostTypes.Horizontal

        const dividingLine: Line = isHorizontalImpost
            ? new Line(new Point(center.x - 1, y), new Point(center.x + 1, y))
            : new Line(new Point(x, center.y - 1), new Point(x, center.y + 1))

        const intersectionPoints: Point[] = []
        solidFilling.containerPolygon.segments.forEach(segment => {
            const point = dividingLine.findIntersectionPoint(segment)
            point && intersectionPoints.push(point)
        })

        intersectionPoints.sort((p1, p2) => center.distanceTo(p1) - center.distanceTo(p2))

        const getPointCoordinate = (point: Point): number => (isHorizontalImpost ? point.x : point.y)

        const point1: Point = intersectionPoints.filter(point => getPointCoordinate(point) < getPointCoordinate(center))[0]
        const point2: Point = intersectionPoints.filter(point => getPointCoordinate(point) > getPointCoordinate(center))[0]

        const impostGeometryParameters = this.profile.getGeometryParameters(ModelParts.Impost)
        const impost = new Impost(point1, point2, impostGeometryParameters.marking)
        impost.modelPart = impostGeometryParameters.modelPart
        if (impost.modelPart === ModelParts.Shtulp) {
            impost.shtulpType = ShtulpTypes.LeftShtulp
        }
        impost.width = impostGeometryParameters.a * 2
        impost.parentNodeId = this.nodeId

        this.impostBeams.push(impost)

        const solidIndex = this.innerFillings.findIndex(innerFilling => {
            if (!innerFilling || !innerFilling.solid) {
                return false
            }

            return innerFilling.solid.nodeId === solidFilling.nodeId
        })
        const newSolidFilling = solidFilling.clone()
        newSolidFilling.nodeId = GUID()
        this.innerFillings.splice(solidIndex + 1, 0, new InnerFilling(newSolidFilling))

        this.update()
    }

    removeImpost(removedImpost: Impost): void {
        const realRemovedImpostIndex = this.impostBeams.findIndex(impost => impost.nodeId === removedImpost.nodeId)
        const realRemovedImpost = this.impostBeams[realRemovedImpostIndex]

        // ищем импост который делит правый или нижний многоугольник образовнынй делением родительского
        // многоугольника удаляемого импоста
        let nearestPolygon = realRemovedImpost.parentPolygon.childrenPolygons[1]

        let foundedNearestPolygon = nearestPolygon

        // поиск ближайшего справа полигона до которого можно расшириться
        safeWhile(
            () => Boolean(nearestPolygon),
            () => {
                if (nearestPolygon && nearestPolygon.segmentIsSide(realRemovedImpost)) {
                    foundedNearestPolygon = nearestPolygon
                }
                nearestPolygon = nearestPolygon.childrenPolygons[0]
            }
        )

        const removedParentPolygons = [foundedNearestPolygon, ...foundedNearestPolygon.allChildrenPolygons]

        const removedImposts = [realRemovedImpost].concat(
            this.impostBeams.filter(impost => Boolean(removedParentPolygons.find(p => p.equals(impost.parentPolygon))))
        )

        const availableIntersectionLines = foundedNearestPolygon.segments.filter(line => !realRemovedImpost.equals(line))
        const intersectionImposts = this.impostBeams
            .slice(realRemovedImpostIndex)
            .filter(impost => !removedImposts.includes(impost))
            .filter(impost => realRemovedImpost.hasCommonStartOrEnd(impost))

        intersectionImposts.forEach(impost => {
            const intersectionPoint = realRemovedImpost.findIntersectionPoint(impost)
            const hasStartPolygon = intersectionPoint?.equals(impost.end)
            const intersectionPoints = impost.findIntersectionPoints(availableIntersectionLines)

            if (hasStartPolygon) {
                impost.end = intersectionPoints.sort((p1, p2) => new Line(impost.start, p1).length - new Line(impost.start, p2).length)[0]
            } else {
                impost.start = intersectionPoints.sort((p1, p2) => new Line(impost.end, p1).length - new Line(impost.end, p2).length)[0]
            }
        })

        const removedFillingPolygons = this._fillingPolygons.filter(polygon => {
            return removedParentPolygons.find(p => p.equals(polygon))
        })
        const newInnerFillingsIndexes = this._fillingPolygons
            .map((polygon, index) => {
                if (!removedFillingPolygons.find(p => p.equals(polygon))) {
                    return index
                }
                return -1
            })
            .filter(index => index !== -1)

        this.impostBeams = this.impostBeams.filter(impost => !removedImposts.includes(impost))
        this.innerFillings = this.innerFillings.filter((innerFilling, index) => newInnerFillingsIndexes.includes(index))

        this.update()
    }

    private _resize(oldRect: Rectangle, newRect: Rectangle): void {
        const oldSize = oldRect.size
        const newSize = newRect.size

        const scaleX = newSize.width / oldSize.width
        const scaleY = newSize.height / oldSize.height

        const minX = Math.min(...this.frameBeams.map(beam => beam.start.x))
        const minY = Math.min(...this.frameBeams.map(beam => beam.start.y))

        const offsetX = minX - minX * scaleX - (oldRect.refPoint.x - newRect.refPoint.x) * scaleX
        const offsetY = minY - minY * scaleY - (oldRect.refPoint.y - newRect.refPoint.y) * scaleY

        const newPoints = (beam: Beam): Point[] => {
            const {start, end} = beam

            return [
                new Point(round(start.x * scaleX + offsetX), round(start.y * scaleY + offsetY)),
                new Point(round(end.x * scaleX + offsetX), round(end.y * scaleY + offsetY))
            ]
        }

        this.impostBeams.forEach(impost => {
            const points = newPoints(impost)
            impost.start = points[0]
            impost.end = points[1]
        })

        this.update()
    }

    private _updateBeamsAndImpostsGeometryParams(): void {
        this.frameBeams.concat(this.impostBeams).forEach(beam => {
            const geometryParameters = this.profile.getFirstAvailableGeometryParameters(beam.profileVendorCode, beam.modelPart)

            beam.profileVendorCode = geometryParameters.marking
            beam.width = beam instanceof Impost ? geometryParameters.a * 2 : geometryParameters.a

            beam.hasInnerWidth = !this.isMoskitkaRoot && !this.isMoskitka
        })
    }

    private _updateFrameBeamsDrawingPoints(): void {
        const getPoints = (
            currentBeam: Beam,
            prevBeam: Beam,
            nextBeam: Beam,
            currentBeamWidth: number,
            prevBeamWidth: number,
            nextBeamWidth: number
        ): Point[] => {
            const points: Point[] = []

            const currentBeamParallel = currentBeam.getParallels(currentBeamWidth)[0]
            const prevBeamParallel = prevBeam.getParallels(prevBeamWidth)[0]
            const nextBeamParallel = nextBeam.getParallels(nextBeamWidth)[0]

            if (currentBeam.modelPart === ModelParts.Porog) {
                const point1 = currentBeamParallel.findIntersectionPoint(nextBeam)
                const point2 = currentBeamParallel.findIntersectionPoint(prevBeam)

                if (point1 && point2) {
                    points.push(currentBeam.start, currentBeam.end, point1, point2)
                }
            } else if (prevBeam.modelPart === ModelParts.Porog) {
                const point1 = currentBeamParallel.findIntersectionPoint(nextBeam)
                const point2 = currentBeamParallel.findIntersectionPoint(prevBeamParallel)
                const point3 = currentBeam.findIntersectionPoint(prevBeamParallel)

                if (point1 && point2 && point3) {
                    points.push(currentBeam.end, point1, point2, point3)
                }
            } else if (nextBeam.modelPart === ModelParts.Porog) {
                const point1 = currentBeam.findIntersectionPoint(nextBeamParallel)
                const point2 = currentBeamParallel.findIntersectionPoint(nextBeamParallel)
                const point3 = currentBeamParallel.findIntersectionPoint(prevBeamParallel)

                if (point1 && point2 && point3) {
                    points.push(currentBeam.start, point1, point2, point3)
                }
            } else {
                const point1 = currentBeamParallel.findIntersectionPoint(nextBeamParallel)
                const point2 = currentBeamParallel.findIntersectionPoint(prevBeamParallel)

                if (point1 && point2) {
                    points.push(currentBeam.start, currentBeam.end, point1, point2)
                }
            }

            return points.length !== 4 ? [] : points
        }

        this.beamsCircularLinkedList.items.forEach(item => {
            const currentBeam = item.data
            const prevBeam = item.prev
            const nextBeam = item.next

            currentBeam.innerDrawingPoints = getPoints(
                currentBeam,
                prevBeam,
                nextBeam,
                currentBeam.innerWidth,
                prevBeam.innerWidth,
                nextBeam.innerWidth
            )
            currentBeam.outerDrawingPoints = getPoints(currentBeam, prevBeam, nextBeam, currentBeam.width, prevBeam.width, nextBeam.width)
        })
    }

    // задаем первоначальные связи
    private _defineConnections(): void {
        const {impostBeams, frameBeams} = this

        const allBeams = [...impostBeams, ...frameBeams]
        // поправляем координаты импостов примыкающих к створке после мастшабирования створки
        impostBeams
            .filter(impost => {
                const dependencies = allBeams.filter(beam => impost.dependencyIds.includes(beam.nodeId))
                return dependencies.length !== 2
            })
            .forEach(impost => {
                const points = impost.findIntersectionPoints(allBeams)

                if (points.length < 2) {
                    console.warn('не найден один из примыкающих испостов')
                }

                if (!points.find(point => point.equals(impost.start))) {
                    impost.start = impost.start.findNearestPoint(points)
                }
                if (!points.find(point => point.equals(impost.end))) {
                    impost.end = impost.end.findNearestPoint(points)
                }

                const startPointConnectedTo = allBeams.find(beam => beam.nodeId !== impost.nodeId && beam.hasPoint(impost.start))

                const endPointConnectedTo = allBeams.find(beam => beam.nodeId !== impost.nodeId && beam.hasPoint(impost.end))

                if (startPointConnectedTo && endPointConnectedTo) {
                    impost.dependencyIds = [startPointConnectedTo.nodeId, endPointConnectedTo.nodeId]
                } else {
                    throw new Error('Не удалось установить связи импостов')
                }
            })
    }

    private _updateImpostsAndInnerFillings(): void {
        this._fixImpostsPoints()

        const {frameBeams, impostBeams} = this

        this.containerPolygon = new Polygon(frameBeams.map(beam => new Segment(beam.start, beam.end, beam.innerWidth)))

        const firstPolygon = this.containerPolygon.clone
        let currentPolygons = [firstPolygon]

        const orderedImposts: Impost[] = []
        let impostLevel = 0
        // сортируем импосты
        safeWhile(
            () => orderedImposts.length <= impostBeams.length,
            () => {
                const nextPolygons: Polygon[] = []
                const impostsLevelGroup: Impost[] = []
                currentPolygons.forEach(currentPolygon => {
                    const segments = impostBeams
                        .filter(impost => !orderedImposts.includes(impost))
                        .map(i => {
                            const impost = i.clone
                            impost.width = i.innerWidth
                            return impost
                        })
                    const data = currentPolygon.getSubPolygonsAndOrderedSegments<Impost>(segments)
                    const imposts: Impost[] = []
                    data.segments.forEach(s => {
                        const impost = impostBeams.find(i => i.nodeId === s.nodeId)
                        impost && imposts.push(impost)
                    })

                    currentPolygon.childrenPolygons = data.polygons
                    impostsLevelGroup.push(...imposts)
                    orderedImposts.push(...imposts)
                    nextPolygons.push(...data.polygons)
                })
                impostsLevelGroup.forEach(impost => (impost.level = impostLevel))
                impostLevel++
                currentPolygons = nextPolygons

                return nextPolygons.length === 0
            }
        )

        this.impostBeams = orderedImposts

        const polygons: Polygon[] = [this.containerPolygon]
        const parentsPolygons: Polygon[] = []

        impostBeams.forEach(impost => {
            const freePolygons = polygons.filter(polygon => !parentsPolygons.includes(polygon))

            freePolygons.forEach(polygon => {
                polygon.childrenPolygons = polygon.getSubPolygons(new Segment(impost.start, impost.end, impost.innerWidth))

                if (polygon.childrenPolygons.length === 2) {
                    impost.parentPolygon = polygon

                    parentsPolygons.push(polygon)

                    polygons.push(...polygon.childrenPolygons)
                }
            })
        })

        // орпеделяем границы полигона в рамках которого может двигаться импост
        impostBeams.forEach(impost => {
            // ищем импост который делит правый или нижний многоугольник образовнынй делением родительского многоугольника
            let nearestPolygon = impost.parentPolygon.childrenPolygons[1]
            let foundedNearestPolygon = nearestPolygon

            // поиск ближайшего справа или снизу полигона до которого можно расшириться
            safeWhile(
                () => Boolean(nearestPolygon),
                () => {
                    if (nearestPolygon && nearestPolygon.segmentIsSide(impost)) {
                        foundedNearestPolygon = nearestPolygon
                    }
                    nearestPolygon = nearestPolygon.childrenPolygons[0]
                }
            )

            const endImpost = impostBeams.find(i => i.parentPolygon.childrenPolygons[0].equals(foundedNearestPolygon))

            impost.containerPolygon = endImpost
                ? impost.parentPolygon.getSubPolygons(new Segment(endImpost.start, endImpost.end, endImpost.innerWidth))[0]
                : impost.parentPolygon

            impost.containerPolygon.childrenPolygons = impost.containerPolygon.getSubPolygons(
                new Segment(impost.start, impost.end, impost.innerWidth)
            )
        })

        const fillingPolygons = firstPolygon.allChildrenPolygons.filter(polygon => polygon.childrenPolygons.length === 0)
        if (fillingPolygons.length === 0) {
            fillingPolygons.push(firstPolygon)
        }
        this._fillingPolygons = fillingPolygons

        this.innerFillings.forEach((innerFilling, index) => {
            if (innerFilling.leaf) {
                const newBeams = FrameFilling.getBeamsFromPolygon(this._fillingPolygons[index], this, innerFilling.leaf)
                const oldRect = Rectangle.buildFromPoints(innerFilling.leaf.frameBeams.map(beam => beam.start))
                const rect = Rectangle.buildFromPoints(newBeams.map(beam => beam.start))
                innerFilling.leaf.frameBeams = newBeams
                innerFilling.leaf._resize(oldRect, rect)
            } else if (innerFilling.solid) {
                innerFilling.solid.containerPolygon = this._fillingPolygons[index]
            }
        })

        this._updateAllPolygons()
    }

    private _fixImpostsPoints(): void {
        const allBeams = [...this.impostBeams, ...this.frameBeams]
        this.impostBeams.forEach(impost => {
            const dependencies = allBeams.filter(b => impost.dependencyIds.includes(b.nodeId))

            if (dependencies.length !== 2) {
                throw new Error('Импост не прикреплен с обоих концов!')
            }

            const points = impost.findIntersectionPoints(dependencies)

            impost.start = points[0]
            impost.end = points[1]
        })
    }

    private _updateAllPolygons(): void {
        const uniquePolygons: Polygon[] = []

        this.impostBeams.forEach(impost => {
            const polygons = [...impost.parentPolygon.allChildrenPolygons, impost.parentPolygon]
            polygons.push(...impost.containerPolygon.allChildrenPolygons, impost.containerPolygon)

            polygons.forEach(polygon => {
                const uniquePolygon = uniquePolygons.find(p => p.equals(polygon))
                if (!uniquePolygon) {
                    uniquePolygons.push(Polygon.buildFromPoints(polygon.points))
                }
            })
        })

        this.impostBeams.forEach(impost => {
            const polygons = [...impost.parentPolygon.allChildrenPolygons, impost.parentPolygon]
            polygons.push(...impost.containerPolygon.allChildrenPolygons, impost.containerPolygon)

            polygons.forEach(polygon => {
                const uniquePolygon = uniquePolygons.find(p => p.equals(polygon))
                if (!uniquePolygon) {
                    return
                }

                polygon.childrenPolygons.forEach(child => {
                    const uniqueChild = uniquePolygons.find(p => p.equals(child))
                    if (!uniqueChild) {
                        return
                    }

                    if (!uniquePolygon.childrenPolygons.find(p => p.equals(uniqueChild))) {
                        uniquePolygon.childrenPolygons.push(uniqueChild)
                    }
                })
            })
        })

        this.impostBeams.forEach(impost => {
            const uniqueParentPolygon = uniquePolygons.find(p => p.equals(impost.parentPolygon))
            const uniqueContainerPolygon = uniquePolygons.find(p => p.equals(impost.containerPolygon))

            if (uniqueParentPolygon) {
                impost.parentPolygon = uniqueParentPolygon
            }
            if (uniqueContainerPolygon) {
                impost.containerPolygon = uniqueContainerPolygon
            }
        })
    }
}
