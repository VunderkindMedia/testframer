import {ISolidFillingJSON, SolidFilling} from './solid-filling'
import {FrameFilling, IFrameFillingJSON} from './frame-filling'

export interface IInnerFillingJSON {
    solid?: ISolidFillingJSON
    leaf?: IFrameFillingJSON
}

export class InnerFilling {
    solid?: SolidFilling
    leaf?: FrameFilling

    constructor(solid?: SolidFilling, leaf?: FrameFilling) {
        this.solid = solid
        this.leaf = leaf
    }

    get child(): SolidFilling | FrameFilling {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        return this.solid ? this.solid : this.leaf!
    }
}
