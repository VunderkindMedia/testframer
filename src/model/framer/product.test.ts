import {Construction} from './construction'
import {TEST_FURNITURE_TEMPLATE, TEST_PROFILE_TEMPLATE} from '../../constants/default-templates'
import {ODN_1} from '../../constants/construction-templates/group-odn'
import {ImpostTypes} from '../impost-types'
import {Point} from '../geometry/point'
import {Line} from '../geometry/line'

const parseConstruction = (template: any): Construction => {
    template.products.forEach((productObj: any) => {
        productObj.profile = TEST_PROFILE_TEMPLATE
        productObj.furniture = TEST_FURNITURE_TEMPLATE
    })
    return Construction.parse(template)
}

interface ITestImpost {
    type: ImpostTypes
    start: Point
    end: Point
}

const addImpost = (construction: Construction, addedImposts: ITestImpost[], addToEnd = true): Construction => {
    const constructionClone = construction.clone
    const product = constructionClone.products[0]

    addedImposts.forEach(addedImpost => {
        const {allSolidFillings} = product.frame

        const index = addToEnd ? allSolidFillings.length - 1 : 0
        const solidFilling = allSolidFillings[index]

        product.addImpost(solidFilling, addedImpost.type)
        product.update()
    })

    return constructionClone
}

test('Тест на добавление/удаление импостов', () => {
    const testAddImposts = (construction: Construction, expectedImposts: ITestImpost[], addToEnd = true): Construction => {
        const startImpostsCount = construction.products[0].frame.allImposts.length
        const startSolidFillingsCount = construction.products[0].frame.allSolidFillings.length

        const constructionClone = addImpost(construction, expectedImposts, addToEnd)

        const {allImposts, allSolidFillings} = constructionClone.products[0].frame

        expect(allImposts.length).toBe(startImpostsCount + expectedImposts.length)
        expect(allSolidFillings.length).toBe(startSolidFillingsCount + expectedImposts.length)

        allImposts.forEach((impost, index) => {
            const expectedImpost = expectedImposts[index]
            impost.normalizePointsOrder()

            expect(impost.start.x).toBe(expectedImpost.start.x)
            expect(impost.start.y).toBe(expectedImpost.start.y)

            expect(impost.end.x).toBe(expectedImpost.end.x)
            expect(impost.end.y).toBe(expectedImpost.end.y)
        })

        return constructionClone
    }

    const removeImpost = (construction: Construction, removedImpost: ITestImpost, expectedImpostsCount: number) => {
        const constructionClone = construction.clone
        const product = constructionClone.products[0]

        const impost = product.frame.allImposts.find(i => {
            i.normalizePointsOrder()
            return i.start.equals(removedImpost.start) && i.end.equals(removedImpost.end)
        })!
        product.removeImpost(impost)
        product.update()

        const {allImposts, allSolidFillings} = product.frame

        expect(allImposts.length).toBe(expectedImpostsCount)
        expect(allSolidFillings.length).toBe(expectedImpostsCount + 1)
    }

    const testExpectedImposts1 = [
        {
            type: ImpostTypes.Horizontal,
            start: new Point(0, 700),
            end: new Point(900, 700)
        },
        {
            type: ImpostTypes.Horizontal,
            start: new Point(0, 350),
            end: new Point(900, 350)
        },
        {
            type: ImpostTypes.Vertical,
            start: new Point(450, 0),
            end: new Point(450, 350)
        }
    ]

    const testExpectedImposts2 = [
        {
            type: ImpostTypes.Vertical,
            start: new Point(450, 0),
            end: new Point(450, 1400)
        },
        {
            type: ImpostTypes.Horizontal,
            start: new Point(450, 700),
            end: new Point(900, 700)
        },
        {
            type: ImpostTypes.Vertical,
            start: new Point(675, 0),
            end: new Point(675, 700)
        }
    ]

    const testExpectedImposts3 = [
        {
            type: ImpostTypes.Vertical,
            start: new Point(450, 0),
            end: new Point(450, 1400)
        },
        {
            type: ImpostTypes.Horizontal,
            start: new Point(0, 1050),
            end: new Point(450, 1050)
        },
        {
            type: ImpostTypes.Horizontal,
            start: new Point(0, 700),
            end: new Point(450, 700)
        }
    ]

    const ODN1 = parseConstruction(ODN_1)

    const construction1 = testAddImposts(ODN1, testExpectedImposts1)
    removeImpost(construction1, testExpectedImposts1[0], 2)
    removeImpost(construction1, testExpectedImposts1[1], 1)
    removeImpost(construction1, testExpectedImposts1[2], 2)

    const construction2 = testAddImposts(ODN1, testExpectedImposts2)
    removeImpost(construction2, testExpectedImposts2[1], 1)

    const construction3 = testAddImposts(ODN1, testExpectedImposts3, false)
    removeImpost(construction3, testExpectedImposts3[0], 2)
})

test('Тест на сдвиг импостов', () => {
    const testSetImpostOffset = (construction: Construction, offsetImpost: ITestImpost, offset: number, expectedImposts: ITestImpost[]) => {
        const constructionClone = construction.clone
        const product = constructionClone.products[0]
        const impost = product.frame.allImposts.find(impost => impost.equals(new Line(offsetImpost.start, offsetImpost.end)))!

        product.setImpostOffset(impost, offset)
        product.update()

        const {allImposts} = product.frame

        expect(allImposts.length).toBe(expectedImposts.length)

        allImposts.forEach((impost, index) => {
            const expectedImpost = expectedImposts[index]
            impost.normalizePointsOrder()

            expect(impost.start.x).toBe(expectedImpost.start.x)
            expect(impost.start.y).toBe(expectedImpost.start.y)

            expect(impost.end.x).toBe(expectedImpost.end.x)
            expect(impost.end.y).toBe(expectedImpost.end.y)
        })
    }

    const ODN1 = parseConstruction(ODN_1)

    const construction1 = addImpost(
        ODN1,
        [
            {
                type: ImpostTypes.Vertical,
                start: new Point(450, 0),
                end: new Point(450, 1400)
            },
            {
                type: ImpostTypes.Horizontal,
                start: new Point(0, 1050),
                end: new Point(450, 1050)
            },
            {
                type: ImpostTypes.Horizontal,
                start: new Point(0, 700),
                end: new Point(450, 700)
            }
        ],
        false
    )

    testSetImpostOffset(
        construction1,
        {
            type: ImpostTypes.Vertical,
            start: new Point(450, 0),
            end: new Point(450, 1400)
        },
        500,
        [
            {
                type: ImpostTypes.Vertical,
                start: new Point(500, 0),
                end: new Point(500, 1400)
            },
            {
                type: ImpostTypes.Horizontal,
                start: new Point(0, 1050),
                end: new Point(500, 1050)
            },
            {
                type: ImpostTypes.Horizontal,
                start: new Point(0, 700),
                end: new Point(500, 700)
            }
        ]
    )

    const construction = ODN1.clone
    const product = construction.products[0]

    product.addImpost(product.frame.allSolidFillings[0], ImpostTypes.Horizontal)
    product.update()
    product.addImpost(product.frame.allSolidFillings[0], ImpostTypes.Horizontal)
    product.update()
    product.addImpost(product.frame.allSolidFillings[1], ImpostTypes.Vertical)
    product.update()

    testSetImpostOffset(
        construction,
        {
            type: ImpostTypes.Horizontal,
            start: new Point(0, 700),
            end: new Point(900, 700)
        },
        400,
        [
            {
                type: ImpostTypes.Horizontal,
                start: new Point(0, 1050),
                end: new Point(900, 1050)
            },
            {
                type: ImpostTypes.Horizontal,
                start: new Point(0, 400),
                end: new Point(900, 400)
            },
            {
                type: ImpostTypes.Vertical,
                start: new Point(450, 400),
                end: new Point(450, 1050)
            }
        ]
    )
})
