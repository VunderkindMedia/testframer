import {getClone} from '../../helpers/json'
import {Affiliate, IAffiliateJSON} from './affiliate'
import {Employee, IEmployeeJSON} from './employee'

export interface IClientJSON {
    id: number
    dealerId: number
    name: string
    phone: string
    email: string
    address: string
    owner?: IEmployeeJSON
    filial?: IAffiliateJSON
}

export class Client {
    static parse(clientJSON: IClientJSON): Client {
        const client = new Client()
        Object.assign(client, clientJSON)

        clientJSON.owner && (client.owner = Employee.parse(clientJSON.owner))
        clientJSON.filial && (client.filial = Affiliate.parse(clientJSON.filial))

        return client
    }

    id = -1
    dealerId = -1
    name = ''
    phone = ''
    email = ''
    address = ''
    owner: Employee | null = null
    filial: Affiliate | null = null

    static get Builder(): Builder {
        return new Builder()
    }

    get clone(): Client {
        return Client.parse(getClone(this))
    }

    get isNew(): boolean {
        return this.id === -1
    }

    equals(client: Client): boolean {
        return (
            this.id === client.id &&
            this.dealerId === client.dealerId &&
            this.name === client.name &&
            this.phone === client.phone &&
            this.email === client.email
        )
    }
}

class Builder {
    private _client = new Client()

    withName(name: string): Builder {
        this._client.name = name
        return this
    }

    withPhone(phone: string): Builder {
        this._client.phone = phone
        return this
    }

    withEmail(email: string): Builder {
        this._client.email = email
        return this
    }

    build(): Client {
        return this._client
    }
}
