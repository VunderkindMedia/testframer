export interface IDeliveryInfoJSON {
    car: string
    carLeft: boolean
    carNumber: string
    deliveryDate: string
    driverPhone: string
}

export class DeliveryInfo {
    static parse(deliveryJSON: IDeliveryInfoJSON): DeliveryInfo {
        return new DeliveryInfo(
            deliveryJSON.car,
            deliveryJSON.carLeft,
            deliveryJSON.carNumber,
            deliveryJSON.deliveryDate,
            deliveryJSON.driverPhone
        )
    }

    car: string
    carLeft: boolean
    carNumber: string
    deliveryDate: string
    driverPhone: string

    constructor(car: string, carLeft: boolean, carNumber: string, deliveryDate: string, driverPhone: string) {
        this.car = car
        this.carLeft = carLeft
        this.carNumber = carNumber
        this.deliveryDate = deliveryDate
        this.driverPhone = driverPhone
    }
}
