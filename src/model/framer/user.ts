export interface IUserJSON {
    email: string
    cityId: number | undefined
    password: string
    confirmPassword: string
}

export class User {
    email: string
    cityId: number | undefined
    password: string
    confirmPassword: string

    constructor(email = '', cityId?: number, password = '', confirmPassword = '') {
        this.email = email
        this.cityId = cityId
        this.password = password
        this.confirmPassword = confirmPassword
    }
}
