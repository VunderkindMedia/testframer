export enum ConnectorTypes {
    Connector = 'connector',
    Extender = 'extender'
}
