export interface IFurnitureJSON {
    id: number
    name: string
    displayName: string
    vars: string
    isDefault: boolean
    isEmpty: boolean
}

export class Furniture {
    static parse(furnitureJSON: IFurnitureJSON): Furniture {
        return new Furniture(
            furnitureJSON.id,
            furnitureJSON.name,
            furnitureJSON.displayName,
            furnitureJSON.vars,
            furnitureJSON.isDefault ?? false,
            furnitureJSON.isEmpty ?? false
        )
    }

    id: number
    name: string
    displayName: string
    vars: string
    isDefault: boolean
    isEmpty: boolean

    constructor(id: number, name: string, displayName: string, vars: string, isDefault: boolean, isEmpty: boolean) {
        this.id = id
        this.name = name
        this.displayName = displayName
        this.vars = vars
        this.isDefault = isDefault
        this.isEmpty = isEmpty
    }

    get handleParams(): number[][] {
        return this.vars.split(';').map(value => value.split(',').map(n => +n))
    }
}
