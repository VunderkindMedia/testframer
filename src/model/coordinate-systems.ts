export enum CoordinateSystems {
    Absolute = 'absolute',
    Relative = 'relative'
}
