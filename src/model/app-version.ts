export interface IAppVersionJSON {
    frontendVersion: string
}

export class AppVersion {
    static parse(appVersionJSON: IAppVersionJSON): AppVersion {
        const appVersion = new AppVersion()
        Object.assign(appVersion, appVersionJSON)
        return appVersion
    }

    frontendVersion!: string
}
