export enum RequestStates {
    Processing = 'processing',
    Succeeded = 'succeeded',
    Failed = 'failed'
}
