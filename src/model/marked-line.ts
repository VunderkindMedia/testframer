import {Line} from './geometry/line'

export class MarkedLine extends Line {
    static from(line: Line): MarkedLine {
        return new MarkedLine(line.start, line.end, line.width)
    }

    markers: string[] = []
}
