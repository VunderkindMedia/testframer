import {Notification, INotificationJSON} from './framer/notifications/notification'
import {AccountNotification, IAccountNotification} from './framer/notifications/account-notification'
import {VersionNotification} from './framer/notifications/version-notification'
import {AppVersion} from './app-version'

export enum NotificationType {
    Account = 'account',
    Version = 'version',
    Notification = 'notification'
}

type TNotificationValueJSON = AccountNotification | VersionNotification | INotificationJSON
type TNotificationValue = AccountNotification | VersionNotification | Notification

interface ICentrifugeNotification {
    type: NotificationType
    value: TNotificationValueJSON
}

export interface ICentrifugeData {
    data: ICentrifugeNotification
}

export class CentrifugeNotification {
    static parse(data: ICentrifugeData): CentrifugeNotification | null {
        const {type, value} = data.data

        switch (type) {
            case NotificationType.Account:
                return new CentrifugeNotification(type, AccountNotification.parse(value as IAccountNotification))
            case NotificationType.Version:
                return new CentrifugeNotification(type, VersionNotification.parse(value as AppVersion))
            case NotificationType.Notification:
                return new CentrifugeNotification(type, Notification.parse(value as INotificationJSON))
        }
    }

    constructor(type: NotificationType, value: TNotificationValue) {
        this.type = type
        this.value = value
    }

    type: NotificationType
    value: TNotificationValue
}
