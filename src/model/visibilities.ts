export enum Visibilities {
    Visible = 'visible',
    Hidden = 'hidden'
}
