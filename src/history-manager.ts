import {updateCurrentOrder} from './redux/orders/actions'
import {setCurrentContext} from './redux/builder/actions'
import {Store} from 'redux'
import {IContext} from './model/i-context'
import {IAppState} from './redux/root-reducer'
import {Construction} from './model/framer/construction'

let instance: HistoryManagerClass | null

interface IConstructionHistoryItem {
    currentConstruction: Construction | null
    currentContext: IContext | null
}

class History<T> {
    private _past: T[] = []
    private _present?: T
    private _future: T[] = []

    addPast(past: T): void {
        this._past = [...this._past, past]
    }

    undo(): T | undefined {
        console.warn('UNDO')
        const present = {...this._present} as T
        const previous = this._past[this._past.length - 1]

        if (previous) {
            this._present = previous
            this._past = this._past.slice(0, this._past.length - 1)
            this._future = [present, ...this._future]
        }

        return this._present
    }

    redo(): T | undefined {
        const present = {...this._present} as T
        const next = this._future[0]

        if (next) {
            this._past = [...this._past, present]
            this._present = next
            this._future = this._future.slice(1)
        }

        return this._present
    }

    get past(): T[] {
        return this._past
    }

    get present(): T | undefined {
        return this._present
    }

    set present(present: T | undefined) {
        if (!present) {
            return
        }
        this._present = present
        if (JSON.stringify(present) === JSON.stringify(this._past[this._past.length - 1])) {
            this._past.pop()
        }
    }

    get future(): T[] {
        return this._future
    }
}

class HistoryManagerClass {
    store!: Store<IAppState>
    private _historyMap = new Map<string, History<IConstructionHistoryItem>>()

    constructor() {
        if (!instance) {
            instance = this
        }

        return instance
    }

    init(store: Store<IAppState>): void {
        this.store = store
    }

    addPast(past: IConstructionHistoryItem): void {
        past.currentConstruction?.id && this.getHistoryByConstructionId(past.currentConstruction?.id).addPast(past)
    }

    setPresent(present: IConstructionHistoryItem): void {
        present.currentConstruction?.id && (this.getHistoryByConstructionId(present.currentConstruction?.id).present = present)
    }

    undo(constructionId: string): void {
        console.log(`undo construction ${constructionId}`)
        this._setCurrentItem(this.getHistoryByConstructionId(constructionId).undo())
    }

    redo(constructionId: string): void {
        console.log(`redo construction ${constructionId}`)
        this._setCurrentItem(this.getHistoryByConstructionId(constructionId).redo())
    }

    clear(): void {
        this._historyMap.clear()
    }

    getHistoryByConstructionId(constructionId: string): History<IConstructionHistoryItem> {
        let history = this._historyMap.get(constructionId)
        if (!history) {
            history = new History<IConstructionHistoryItem>()
            this._historyMap.set(constructionId, history)
        }
        return history
    }

    private _setCurrentItem(item?: IConstructionHistoryItem): void {
        const {currentOrder} = this.store.getState().orders
        const constructionIndex = currentOrder?.constructions?.findIndex(c => c.id === item?.currentConstruction?.id) ?? -1
        if (item && currentOrder && constructionIndex !== -1) {
            const order = currentOrder.clone
            item.currentConstruction && (order.constructions[constructionIndex] = item.currentConstruction)
            this.store.dispatch(updateCurrentOrder(order))
            this.store.dispatch(setCurrentContext(item.currentContext))
        }
    }
}

export const HistoryManager = new HistoryManagerClass()

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.historyManager = HistoryManager
