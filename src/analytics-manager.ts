import {YandexMetricsManager} from './yandex-metrics-manager'
import {CarrotQuestManager} from './carrot-quest-manager'
import {Order} from './model/framer/order'

export interface IAnalyticsEvent {
    name: string
    target: string
    data?: unknown
}

let instance: AnalyticsManagerClass | null

class AnalyticsManagerClass {
    constructor() {
        if (!instance) {
            instance = this
        }

        return instance
    }

    trackRegistered(data?: unknown): void {
        CarrotQuestManager.track('$registered', data)
        YandexMetricsManager.reachGoal('$registered')
    }

    trackAuthorized(): void {
        CarrotQuestManager.track('$authorized')
        YandexMetricsManager.reachGoal('$authorized')
    }

    registerPurchase(order: Order): void {
        const orderAmount = +(order.totalAmegaCostToDisplay / 100).toFixed(2)
        CarrotQuestManager.track('$order_completed', {
            // eslint-disable-next-line @typescript-eslint/naming-convention
            $order_id: order.fullOrderNumber,
            // eslint-disable-next-line @typescript-eslint/naming-convention
            $order_id_human: order.fullOrderNumber,
            // eslint-disable-next-line @typescript-eslint/naming-convention
            $order_amount: orderAmount
        })
        CarrotQuestManager.identify([
            {op: 'add', key: '$orders_count', value: 1},
            {op: 'add', key: '$revenue', value: orderAmount},
            {op: 'update_or_create', key: '$last_payment', value: orderAmount}
        ])
        YandexMetricsManager.registerPurchase(order.id, order.fullOrderNumber, orderAmount)
    }

    trackEvent(event: IAnalyticsEvent): void {
        CarrotQuestManager.track(event.name, event.data)
        YandexMetricsManager.reachGoal(event.target)
    }

    trackClickTransferToProductionButton(): void {
        this.trackEvent({
            name: 'Клик "Передать в производство"',
            target: 'transfer-to-production-button'
        })
    }

    trackClickDownloadKpButtonFromOrdersPage(): void {
        this.trackEvent({
            name: 'Клик "Скачать КП" (с реестра заказов)',
            target: 'orders-page-download-kp-button'
        })
    }

    trackClickDownloadKpButtonFromOrderOrBuilderPage(): void {
        this.trackEvent({
            name: 'Клик "Скачать КП" (с карточки заказа и построителя)',
            target: 'order-page-download-kp-button'
        })
    }

    trackClickNewOrderButton(): void {
        this.trackEvent({
            name: 'Клик "Новый заказ"',
            target: 'new-order-button'
        })
    }

    trackClickCopyingConstruction(): void {
        this.trackEvent({
            name: 'Клик "Копирование конструкции"',
            target: 'copy-construction'
        })
    }

    trackClickInstructionLink(): void {
        this.trackEvent({
            name: 'Клик "Документация -> Инструкции"',
            target: 'instructions-link'
        })
    }

    trackClickFaqLink(): void {
        this.trackEvent({
            name: 'Клик "Документация -> FAQ"',
            target: 'faq-link'
        })
    }

    trackEmailClick(): void {
        this.trackEvent({
            name: 'Клик "Документация -> info@framer.ru"',
            target: 'email-link'
        })
    }

    trackClickMedialibraryButton(): void {
        this.trackEvent({
            name: 'Клик "Медиабиблиотека"',
            target: 'medialibrary-button'
        })
    }

    trackConfirmAccountFromHeaderClick(): void {
        this.trackEvent({
            name: 'Клик "Подтвердить учетную запись" в шапке',
            target: 'header-confirm-account-button'
        })
    }

    trackConfirmAccountClick(): void {
        this.trackEvent({
            name: 'Клик "Подтвердить учетную запись"',
            target: 'profile-page-confirm-account-button'
        })
    }

    trackConfirmAccountFromModalClick(): void {
        this.trackEvent({
            name: 'Клик "Подтвердить учетную запись" в окне',
            target: 'modal-window-confirm-account-button'
        })
    }

    trackDemoLoginClick(city: string, id: string): void {
        this.trackEvent({
            name: `Выбран демо-аккаунт – ${city}`,
            target: `demo-${id}-login`
        })
    }

    trackRegisterFromDemoClick(): void {
        this.trackEvent({
            name: 'Клик в демо-аккаунте "Зарегистрироваться"',
            target: 'header-registration-button'
        })
    }

    trackRegisterFromDemoLimitClick(): void {
        this.trackEvent({
            name: 'Клик в предупреждении демо-аккаунта "Зарегистрироваться"',
            target: 'demo-modal-alert-button'
        })
    }

    trackRegisterFromDemoSendForm(): void {
        this.trackEvent({
            name: 'Регистрация через демо-аккаунт',
            target: 'demo-modal-registration-button'
        })
    }

    trackConfirmFromDemoSendForm(): void {
        this.trackEvent({
            name: 'Подтверждение после демо-аккаунта',
            target: 'demo-modal-confirm-button'
        })
    }

    trackNewOrderFromDemoClick(): void {
        this.trackEvent({
            name: 'Клик "Новый заказ" в демо-аккаунте',
            target: 'demo-new-order-button'
        })
    }

    trackOnboardingStep(step: number, title = ''): void {
        if (!title) {
            this.trackEvent({
                name: 'Начал онбординг',
                target: 'onboarding-start'
            })
        } else {
            this.trackEvent({
                name: `Закончил ${step + 1}/4 онбординг "${title}"`,
                target: `onboarding-finish-${step + 1}`
            })
        }
    }
}

export const AnalyticsManager = new AnalyticsManagerClass()
