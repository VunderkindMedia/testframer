import * as React from 'react'
import {useState, useRef, useEffect, useCallback, CSSProperties} from 'react'
import {BodyPortal} from '../body-portal/body-portal'
import styles from './arrow-tooltip.module.css'

const PLACEMENT_GAP = 10

interface IArrowTooltipProps {
    title: React.ReactElement | string
    placement?: 'top' | 'bottom'
    children: React.ReactElement
}

export function ArrowTooltip(props: IArrowTooltipProps): JSX.Element {
    const {title, placement = 'top', children} = props
    const [isShowTooltip, setIsShowTooltip] = useState(false)
    const [tooltipStyle, setTooltipStyle] = useState<CSSProperties>()
    const [arrowStyle, setArrowStyle] = useState<CSSProperties>()
    const [finalPlacement, setFinalPlacement] = useState(placement)
    const tooltip = useRef<HTMLDivElement>(null)
    const [controlRect, setControlRect] = useState<DOMRect | null>(null)
    const itemRef = useRef<Element | null>(null)

    const getBottomPosition = (controlRect: DOMRect): number => {
        return window.scrollY + controlRect.y + controlRect.height
    }

    const getTopPosition = (controlRect: DOMRect, tooltipRect: DOMRect): number => {
        return window.scrollY + controlRect.y - tooltipRect.height - PLACEMENT_GAP * 2
    }

    useEffect(() => {
        if (!controlRect || !tooltip || !tooltip.current || !isShowTooltip) {
            return
        }

        const tooltipRect = tooltip.current.getBoundingClientRect()
        if (!tooltipRect.width || !tooltipRect.height) {
            return
        }

        let tooltipX = controlRect.x + (controlRect.width - tooltipRect.width) / 2 - PLACEMENT_GAP
        if (tooltipX + tooltipRect.width + PLACEMENT_GAP * 2 > window.innerWidth) {
            tooltipX = window.innerWidth - PLACEMENT_GAP * 2 - tooltipRect.width
        } else if (tooltipX < PLACEMENT_GAP) {
            tooltipX = PLACEMENT_GAP
        }

        let tooltipY

        switch (placement) {
            case 'top': {
                if (controlRect.y < tooltipRect.height + PLACEMENT_GAP * 2) {
                    setFinalPlacement('bottom')
                    tooltipY = getBottomPosition(controlRect)
                } else {
                    tooltipY = getTopPosition(controlRect, tooltipRect)
                }
                break
            }
            case 'bottom': {
                if (controlRect.y + controlRect.height + PLACEMENT_GAP * 2 > window.innerHeight) {
                    setFinalPlacement('top')
                    tooltipY = getTopPosition(controlRect, tooltipRect)
                } else {
                    tooltipY = getBottomPosition(controlRect)
                }
            }
        }

        setTooltipStyle({
            top: `${tooltipY}px`,
            left: `${tooltipX}px`,
            opacity: 1
        })
        setArrowStyle({left: `${controlRect.x - tooltipX - PLACEMENT_GAP + controlRect.width / 2 - 7}px`, opacity: 1})
    }, [controlRect, tooltip, isShowTooltip, placement])

    const handleTouchStart = useCallback((e: TouchEvent) => {
        if (itemRef.current?.contains(e.target as Node)) {
            setControlRect(itemRef.current.getBoundingClientRect())
            setIsShowTooltip(true)
        } else {
            setIsShowTooltip(false)
        }
    }, [])

    useEffect(() => {
        window.addEventListener('touchstart', handleTouchStart)
        return () => {
            window.removeEventListener('touchstart', handleTouchStart)
        }
    }, [handleTouchStart])

    const handleMouseOver = useCallback(() => {
        if (itemRef.current) {
            setControlRect(itemRef.current.getBoundingClientRect())
        }
        setIsShowTooltip(true)
    }, [])

    const handleMouseLeave = useCallback(() => setIsShowTooltip(false), [])

    useEffect(() => {
        const item = itemRef.current

        item?.addEventListener('mouseenter', handleMouseOver)
        item?.addEventListener('mouseleave', handleMouseLeave)

        return () => {
            item?.removeEventListener('mouseenter', handleMouseOver)
            item?.removeEventListener('mouseleave', handleMouseLeave)
        }
    }, [handleMouseOver, handleMouseLeave])

    return (
        <>
            {React.cloneElement(children, {
                ref: itemRef
            })}
            {isShowTooltip && (
                <BodyPortal>
                    <div ref={tooltip} className={styles.content} style={tooltipStyle}>
                        {title}
                        <div className={`${styles.arrow} ${styles[`arrow--${finalPlacement}`]}`} style={arrowStyle} />
                    </div>
                </BodyPortal>
            )}
        </>
    )
}
