import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {NAV_BILL, NAV_KP} from '../../../constants/navigation'
import {Loader} from '../loader/loader'
import {useAppContext} from '../../../hooks/app'
import styles from './progress-bar.module.css'

export function ProgressBar(): JSX.Element | null {
    const shouldShowSpecial = useSelector((state: IAppState) => state.request.showSpecial)
    const requestCount = useSelector((state: IAppState) => state.request.count)
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const {isMobile} = useAppContext()

    if (pathname === NAV_KP || pathname === NAV_BILL) {
        return null
    }

    return (
        <div
            data-test="progress-bar"
            className={`${styles.progressBar} ${requestCount > 0 && styles.visible} ${shouldShowSpecial && styles.showSpecial} ${
                isMobile ? styles.mobile : ''
            }`}>
            <span className={styles.spinner} />
            <div className={styles.loaderWrap}>
                <p>Идёт передача в производство</p>
                <Loader />
            </div>
        </div>
    )
}
