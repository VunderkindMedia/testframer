import * as React from 'react'
import {Good} from '../../../model/framer/good'
import {GoodAttributesEditor} from '../good-attributes-editor/good-attributes-editor'
import {WithTitle} from '../with-title/with-title'
import {QuantityEditor} from '../quantity-editor/quantity-editor'
import {ListItem} from '../../../model/list-item'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ReactComponent as CacheIconSmall} from '../../../resources/images/cache-icon-small.inline.svg'
import {RemoveButton} from '../button/remove-button/remove-button'
import {Select} from '../select/select'
import {getCurrency} from '../../../helpers/currency'
import {useAppContext} from '../../../hooks/app'
import styles from './good-card.module.css'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'

interface IGoodCardProps {
    goodGroupName: string
    good: Good
    availableGoods: Good[]
    className?: string
    onChange: (good: Good) => void
    onRemoveClick: () => void
    isDisabled?: boolean
    dataTest?: string
}

export function GoodCard(props: IGoodCardProps): JSX.Element {
    const {good, availableGoods, isDisabled, className, onChange, onRemoveClick, dataTest} = props
    const {isMobile} = useAppContext()

    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)

    const selectItems = availableGoods.map(g => {
        const listItem = new ListItem(g.id.toString(10), g.name, g)
        listItem.additionalString = `, ${getCurrency(g.pricePerUnitToDisplay)} ${RUB_SYMBOL}`
        return listItem
    })

    let selectedItem = selectItems.find(g => g.data.id === good.goodId)
    if (!selectedItem) {
        selectedItem = new ListItem(good.id.toString(10), good.name, good)
    }

    return (
        <div className={`${styles.card} ${className} ${isMobile ? styles.mobile : ''}`} data-test={dataTest}>
            <div className={styles.body}>
                <WithTitle title="Материал" className={styles.goodTitle}>
                    <Select
                        isDisabled={isDisabled}
                        items={selectItems}
                        selectedItem={selectedItem}
                        isMultiline={isMobile}
                        onSelect={item => {
                            const goodClone = good.clone
                            goodClone.changed = true
                            goodClone.goodId = item.data.id
                            const availableGood = availableGoods.find(g => g.goodId === goodClone.goodId)
                            if (
                                availableGood &&
                                (goodClone.attributes.length !== availableGood.attributes.length ||
                                    !goodClone.attributes.every(a1 => availableGood.attributes.some(a2 => a2.equals(a1))))
                            ) {
                                goodClone.attributes = availableGood.attributes
                            }
                            onChange(goodClone)
                        }}
                    />
                </WithTitle>
                <GoodAttributesEditor
                    isDisabled={isDisabled}
                    attributes={good.attributes}
                    onChange={attributes => {
                        const goodClone = good.clone
                        goodClone.changed = true
                        goodClone.attributes = attributes
                        onChange(goodClone)
                    }}
                />
                <WithTitle title="Цена" titleStyle={{textAlign: 'center'}}>
                    <div className={`${styles.controlWrapper} ${styles.unitPriceWrapper}`}>
                        <span>
                            {getCurrency(good.pricePerUnitToDisplay)}&nbsp;
                            {currentOrder?.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                        </span>
                        {good.unit.shortName && <span>за {good.unit.shortName}</span>}
                    </div>
                </WithTitle>
                <WithTitle title="Кол-во" titleStyle={{textAlign: 'center'}}>
                    <div className={styles.controlWrapper}>
                        <QuantityEditor
                            isDisabled={isDisabled}
                            quantity={good.quantity}
                            min={1}
                            max={999}
                            onChange={quantity => {
                                const goodClone = good.clone
                                if (quantity !== goodClone.quantity) {
                                    goodClone.quantity = quantity
                                    goodClone.changed = true
                                    onChange(goodClone)
                                }
                            }}
                        />
                    </div>
                </WithTitle>
                <WithTitle title="Сумма" titleStyle={{textAlign: 'center'}}>
                    <div className={styles.controlWrapper}>
                        <p className={styles.totalCost}>
                            {good.changed ? (
                                <>&mdash;</>
                            ) : (
                                <>
                                    {getCurrency(good.cost)} {currentOrder?.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                                </>
                            )}
                        </p>
                    </div>
                </WithTitle>
                <div className={`${styles.controlWrapper} ${styles.removeButtonWrapper}`}>
                    <RemoveButton isDisabled={isDisabled} onClick={onRemoveClick} />
                </div>
            </div>
        </div>
    )
}
