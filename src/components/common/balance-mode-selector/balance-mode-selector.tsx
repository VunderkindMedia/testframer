import * as React from 'react'
import {useCallback, useState, useRef} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {setIsCashMode} from '../../../redux/account/actions'
import {useAppContext} from '../../../hooks/app'
import {useDocumentClick} from '../../../hooks/ui'
import {ReactComponent as CacheIcon} from './resources/cache-icon.inline.svg'
import {ReactComponent as RoubleIcon} from './resources/rouble-icon.inline.svg'
import {ReactComponent as ArrowIcon} from './resources/arrow-icon.inline.svg'
import styles from './balance-mode-selector.module.css'

interface IBalanceModeProps {
    className?: string
}

export const BalanceModeSelector = (props: IBalanceModeProps): JSX.Element => {
    const {className} = props
    const isCashMode = useSelector((state: IAppState) => state.account.isCashMode)
    const dispatch = useDispatch()
    const {isMobile} = useAppContext()
    const [isOpened, setIsOpened] = useState(false)
    const ref = useRef<HTMLDivElement | null>(null)

    const setCashMode = useCallback(() => {
        dispatch(setIsCashMode(true))
    }, [dispatch])

    const resetCashMode = useCallback(() => {
        dispatch(setIsCashMode(false))
    }, [dispatch])

    const toggleOpened = useCallback(() => {
        setIsOpened(flag => !flag)
    }, [])

    const closeSelector = useCallback(
        (e: MouseEvent): void => {
            if (ref.current && !ref.current.contains(e.target as HTMLDivElement)) {
                setIsOpened(false)
            }
        },
        [ref]
    )

    useDocumentClick(closeSelector)

    const balanceMode = (
        <button key="balance-mode" className={styles.button} onClick={resetCashMode}>
            <RoubleIcon />
        </button>
    )

    const cashMode = (
        <button key="cash-mode" className={styles.button} onClick={setCashMode}>
            <CacheIcon />
        </button>
    )

    const options = isCashMode ? [cashMode, balanceMode] : [balanceMode, cashMode]

    return (
        <div className={`${styles.container} ${className ?? ''}`}>
            <div
                ref={ref}
                className={`${styles.selector} ${isOpened && styles.opened} ${isCashMode && styles.cashMode} ${isMobile && styles.mobile}`}
                onClick={toggleOpened}>
                {options}
                <ArrowIcon className={`${styles.arrow} ${isOpened && styles.opened}`} />
            </div>
        </div>
    )
}
