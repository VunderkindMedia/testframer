import * as React from 'react'
import {Filling} from '../../../model/framer/filling'
import {Checkbox} from '../checkbox/checkbox'
import {useCallback, useEffect, useMemo, useState} from 'react'
import {ArrowTooltip} from '../arrow-tooltip/arrow-tooltip'
import {EnergyTooltip} from '../energy-tooltip/energy-tooltip'
import {ListItem} from '../../../model/list-item'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {SolidFilling} from '../../../model/framer/solid-filling'
import {WithTitle} from '../with-title/with-title'
import {Select} from '../select/select'
import {useSetFilling, useSetConfigContext} from '../../../hooks/builder'
import {ConfigContexts} from '../../../model/config-contexts'
import {getClassNames} from '../../../helpers/css'
import {useAppContext} from '../../../hooks/app'
import {EditButton} from '../../common/button/edit-button/edit-button'
import {FILLING_SELECTOR_APPLY_TO_ALL} from '../../../constants/local-storage'
import styles from './filling-selector.module.css'
import {FactoryTypes} from '../../../model/framer/factory-types'

export const getThicknessArray = (fillings: Filling[]): number[] => {
    return Array.from(new Set(fillings.map(filling => filling.thickness))).sort()
}

const getCamernostsArray = (fillings: Filling[]): number[] => {
    return Array.from(new Set(fillings.map(filling => filling.camernost))).sort()
}

export function FillingSelector(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const partner = useSelector((state: IAppState) => state.account.partner)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)
    const currentProduct = useSelector((state: IAppState) => {
        return state.builder.configContext === ConfigContexts.ConstructionParams ? state.orders.genericProduct : state.orders.currentProduct
    })
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const [group, setGroup] = useState('')
    const [thickness, setThickness] = useState(0)
    const [camernost, setCamernost] = useState(0)
    const [isMultifunctional, setIsMultifunctional] = useState(false)
    const [isLowEmission, setIsLowEmission] = useState(false)
    const applyToAll = localStorage.getItem(FILLING_SELECTOR_APPLY_TO_ALL)
    const [isApplyToAll, setIsApplyToAll] = useState(applyToAll ? applyToAll === 'true' : true)

    const [groupsArray, setGroupsArray] = useState<string[]>([])

    const {isMobile} = useAppContext()

    const context =
        currentProduct?.isEmpty ||
        (currentProduct && configContext === ConfigContexts.ConstructionParams && currentProduct.frame.innerFillings.length)
            ? currentProduct.frame.innerFillings[0].solid ||
              (currentProduct.frame.innerFillings[0].leaf && currentProduct.frame.innerFillings[0].leaf.innerFillings[0].solid)
            : currentContext

    const fillings = useMemo(() => currentProduct?.availableFillings ?? [], [currentProduct])

    const setFilling = useSetFilling(currentOrder, currentConstruction, currentContext, configContext, fillings, isApplyToAll)
    const selectFilling = useCallback((filling: Filling) => setFilling(filling), [setFilling])
    const setConfigContext = useSetConfigContext()

    const toggleFillingEditor = useCallback(() => {
        setConfigContext(configContext === ConfigContexts.FillingEditor ? ConfigContexts.Filling : ConfigContexts.FillingEditor)
    }, [configContext, setConfigContext])

    const selected = fillings.find(f => context instanceof SolidFilling && f.marking === context.fillingVendorCode) || null

    useEffect(() => {
        localStorage.setItem(FILLING_SELECTOR_APPLY_TO_ALL, String(isApplyToAll))
    }, [isApplyToAll])

    useEffect(() => {
        const groupsArray = Array.from(new Set(fillings.map(filling => filling.group)))
        setGroupsArray(groupsArray)

        const filling = context instanceof SolidFilling ? context.filling : null
        setGroup(selected?.group ?? (filling ? filling.group : groupsArray[0]))
        setThickness(selected?.thickness ?? (filling ? filling.thickness : getThicknessArray(fillings)[0]))
        setCamernost(selected?.camernost ?? (filling ? filling.camernost : getCamernostsArray(fillings)[0]))
        setIsMultifunctional(selected?.isMultifunctional ?? (filling ? filling.isMultifunctional : false))
        setIsLowEmission(selected?.isLowEmission ?? (filling ? filling.isLowEmission : false))
    }, [context, fillings, selected?.camernost, selected?.group, selected?.isLowEmission, selected?.isMultifunctional, selected?.thickness])

    if (fillings.length === 0) {
        return null
    }

    if (!(context instanceof SolidFilling)) {
        return null
    }

    const selectGroup = (group: string): void => {
        let filteredFillings = fillings.filter(filling => filling.group === group)
        const thicknessArray = getThicknessArray(filteredFillings)

        const newThickness = thicknessArray.includes(thickness) ? thickness : thicknessArray[0]

        filteredFillings = filteredFillings.filter(filling => filling.thickness === newThickness)
        const camernostsArray = getCamernostsArray(filteredFillings)

        const newCamernost = camernostsArray.includes(camernost) ? camernost : camernostsArray[0]

        setGroup(group)
        setThickness(newThickness)
        setCamernost(newCamernost)
        setConfigContext(
            group.toLowerCase() !== 'cтеклопакеты' ? ConfigContexts.Filling : selected ? ConfigContexts.Filling : ConfigContexts.FillingEditor
        )
    }

    const groupsSelectItems = groupsArray.map(g => new ListItem(g, g, g))
    const groupSelectedItem = groupsSelectItems.find(item => item.data === group)

    let filteredFillings = fillings.filter(filling => filling.group === group)

    const thicknessArray = getThicknessArray(filteredFillings)

    const thicknessSelectItems = thicknessArray.map(t => new ListItem(t.toString(), `${t.toString()} мм`, t))
    const thicknessSelectItem = thicknessSelectItems.find(item => item.data === thickness)

    filteredFillings = filteredFillings.filter(filling => filling.thickness === thickness)

    const camernostsArray = getCamernostsArray(filteredFillings)

    const camernostsSelectItems = camernostsArray.map(c => new ListItem(c.toString(), `${c.toString()}-камерные`, c))
    const camernostsSelectedItem = camernostsSelectItems.find(item => item.data === camernost)

    filteredFillings = filteredFillings.filter(
        filling =>
            filling.camernost === camernost &&
            (isMultifunctional ? filling.isMultifunctional === isMultifunctional : true) &&
            (isLowEmission ? filling.isLowEmission === isLowEmission : true)
    )

    return (
        <div className={`${styles.selector} ${isMobile ? styles.mobile : ''}`}>
            <div className={styles.header}>
                <WithTitle title="Заполнение" className={`${styles.withTitle} ${styles.fillingWithTitle}`}>
                    <Select items={groupsSelectItems} selectedItem={groupSelectedItem} onSelect={item => selectGroup(item.data)} />
                </WithTitle>
                <WithTitle title="Толщина" className={`${styles.withTitle} ${styles.thicknessWithTitle}`}>
                    <Select items={thicknessSelectItems} selectedItem={thicknessSelectItem} onSelect={item => setThickness(item.data)} />
                </WithTitle>
                {group && group.toLowerCase().includes('стеклопак') && (
                    <WithTitle title="Камерность" className={`${styles.withTitle} ${styles.camernostWithTitle}`}>
                        <Select
                            items={camernostsSelectItems}
                            selectedItem={camernostsSelectedItem}
                            onSelect={item => setCamernost(item.data)}
                        />
                    </WithTitle>
                )}
                <div className={styles.headerCheckboxes}>
                    <Checkbox
                        className={styles.checkbox}
                        isChecked={isMultifunctional}
                        title="Мультифункциональное"
                        onChange={checked => setIsMultifunctional(checked)}
                    />
                    <Checkbox
                        className={styles.checkbox}
                        isChecked={isLowEmission}
                        title="Низкоэмиссионное"
                        onChange={checked => setIsLowEmission(checked)}
                    />
                </div>
            </div>
            <div className={styles.items}>
                {filteredFillings.map(filling => (
                    <div
                        key={filling.id}
                        onClick={() => selectFilling(filling)}
                        className={getClassNames(`${styles.item} blue-hover`, {selected: filling.id === selected?.id})}>
                        <p>
                            {filling.isEnergy && partner?.factoryId === FactoryTypes.Perm && <EnergyTooltip className={styles.energyIcon} />}
                            {filling.marking}
                        </p>
                        <p>{filling.name}</p>
                        {filling.id === selected?.id && filling.group.toLowerCase() === 'стеклопакеты' && (
                            <EditButton className={styles.editFilling} onClick={toggleFillingEditor} />
                        )}
                    </div>
                ))}
            </div>
            {configContext !== ConfigContexts.ConstructionParams && (
                <div className={styles.footer}>
                    <ArrowTooltip title="Изменятся все заполнения" placement="top">
                        <div style={{outline: 'none'}}>
                            <Checkbox isChecked={isApplyToAll} onChange={checked => setIsApplyToAll(checked)} title="Применить ко всем" />
                        </div>
                    </ArrowTooltip>
                </div>
            )}
        </div>
    )
}
