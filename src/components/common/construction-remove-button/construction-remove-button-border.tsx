import * as React from 'react'
import {ModalModel} from '../modal/modal-model'
import {Action} from '../../../model/action'
import {Order} from '../../../model/framer/order'
import {Construction} from '../../../model/framer/construction'
import {useState} from 'react'
import {Modal} from '../modal/modal'
import {Button} from '../button/button'

interface IConstructionRemoveButtonProps {
    order: Order
    construction: Construction
    onChangeOrder: (order: Order) => void
    className?: string
}

export function ConstructionRemoveButtonBorder(props: IConstructionRemoveButtonProps): JSX.Element {
    const {className, order, construction, onChangeOrder} = props

    const [modalModel, setModalModel] = useState<ModalModel | null>(null)

    return (
        <>
            {modalModel && <Modal model={modalModel} />}
            <Button
                isDisabled={order.readonly}
                buttonStyle="with-border"
                className={className}
                onClick={() => {
                    const index = order.constructions.findIndex(c => c.nodeId === construction.nodeId)
                    const position = order.constructions[index].position || index + 1

                    if (order.constructions.length === 1) {
                        setModalModel(
                            ModalModel.Builder.withTitle('Внимание!')
                                .withBody(
                                    <p>
                                        Удаление последней конструкции из
                                        <br /> заказа запрещено
                                    </p>
                                )
                                .withPositiveAction(
                                    new Action('Понятно', () => {
                                        setModalModel(null)
                                    })
                                )
                                .build()
                        )
                    } else {
                        setModalModel(
                            ModalModel.Builder.withTitle(`Подтверждение удаления конструкции ${position}`)
                                .withBody(<p>Удалить конструкцию {position}?</p>)
                                .withPositiveAction(
                                    new Action('Удалить', () => {
                                        const orderClone = order.clone
                                        orderClone.constructions.splice(index, 1)
                                        orderClone.sortedConstructions
                                        orderClone.addAction('Удалили конструкцию', construction)
                                        onChangeOrder(orderClone)
                                        setModalModel(null)
                                    })
                                )
                                .withNegativeAction(new Action('Отменить', () => setModalModel(null)))
                                .build()
                        )
                    }
                }}>
                Удалить конструкцию
            </Button>
        </>
    )
}
