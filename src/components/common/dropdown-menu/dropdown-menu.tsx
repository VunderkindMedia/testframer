import * as React from 'react'
import {useState, useCallback, useRef} from 'react'
import {Button} from '../button/button'
import {ReactComponent as DotsIcon} from './resources/dots-icon.inline.svg'
import {useDocumentClick} from '../../../hooks/ui'
import {ContextMenu} from '../context-menu/context-menu'
import {Action} from '../../../model/action'
import {useAppContext} from '../../../hooks/app'
import styles from './dropdown-menu.module.css'
import {getClassNames} from '../../../helpers/css'

interface IDropdownMenu {
    items: Action[]
}

export function DropdownMenu(props: IDropdownMenu): JSX.Element {
    const {items} = props
    const [isMenuOpened, setIsMenuOpened] = useState(false)
    const menuRef = useRef<HTMLDivElement>(null)

    const {isMobile} = useAppContext()

    const handleClick = useCallback((e: MouseEvent) => {
        if (!menuRef.current?.contains(e.target as Node)) {
            setIsMenuOpened(false)
        }
    }, [])

    const onClick = useCallback(
        (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
            e.stopPropagation()
            setIsMenuOpened(!isMenuOpened)
        },
        [isMenuOpened]
    )

    useDocumentClick(handleClick)

    const listItems = items.map(item => item.name)

    return (
        <div className={getClassNames(styles.dropdownMenu, {[styles.mobile]: isMobile})} ref={menuRef}>
            {isMobile ? (
                <button className={styles.button} onClick={onClick}>
                    <DotsIcon />
                </button>
            ) : (
                <Button className={styles.button} buttonStyle="with-border" onClick={onClick}>
                    <DotsIcon />
                </Button>
            )}
            {isMenuOpened && (
                <ContextMenu
                    items={listItems}
                    className={styles.menu}
                    onSelect={(item, index) => {
                        void items[index].func()
                        setIsMenuOpened(false)
                    }}
                />
            )}
        </div>
    )
}
