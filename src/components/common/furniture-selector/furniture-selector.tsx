import * as React from 'react'
import styles from './furniture-selector.module.css'
import './furniture-selector.css'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {ConfigContexts} from '../../../model/config-contexts'
import {MobileSidebar} from '../../mobile/mobile-sidebar/mobile-sidebar'
import {Visibilities} from '../../../model/visibilities'
import {useRef, useEffect, useCallback} from 'react'
import {useSetFurniture} from '../../../hooks/builder'
import {Furniture} from '../../../model/framer/furniture'
import {getClassNames} from '../../../helpers/css'

export function FurnitureSelector(): JSX.Element | null {
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => {
        return state.builder.configContext === ConfigContexts.ConstructionParams ? state.orders.genericProduct : state.orders.currentProduct
    })
    const configContext = useSelector((state: IAppState) => state.builder.configContext)

    const setFurniture = useSetFurniture(currentDealer, currentOrder, currentConstruction, configContext)

    const selectFurniture = useCallback(
        (furniture: Furniture) => {
            setFurniture(furniture)
            sidebarRef.current?.hide()
        },
        [setFurniture]
    )

    const sidebarRef = useRef<MobileSidebar | null>(null)

    useEffect(() => {
        if (configContext === ConfigContexts.Furniture && sidebarRef.current?.visibility === Visibilities.Hidden) {
            sidebarRef.current.show()
        }
    }, [configContext])

    if (!currentOrder || !currentConstruction || !currentProduct) {
        return null
    }

    const selectedFurniture = currentProduct.furniture

    return (
        <div className={styles.selector}>
            {currentProduct.availableFurniture.map(f => (
                <div
                    key={f.id}
                    className={getClassNames(`${styles.item} blue-hover`, {
                        selected: Boolean(selectedFurniture && selectedFurniture.id === f.id)
                    })}
                    onClick={() => selectFurniture(f)}>
                    <p>{f.displayName}</p>
                </div>
            ))}
        </div>
    )
}
