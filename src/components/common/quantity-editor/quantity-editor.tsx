import * as React from 'react'
import styles from './quantity-editor.module.css'
import {InputNumber} from '../input-number/input-number'
import {useCallback, useEffect, useState} from 'react'
import {useRef} from 'react'
import Timeout = NodeJS.Timeout
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'
import {clamp} from '../../../helpers/number'

interface IQuantityEditorProps {
    quantity: number
    min: number
    max: number
    isDisabled?: boolean
    className?: string
    onChange: (quantity: number) => void
}

export function QuantityEditor(props: IQuantityEditorProps): JSX.Element {
    const {quantity, onChange, min, max, className, isDisabled} = props

    const [innerValue, setInnerValue] = useState(quantity)

    const timerRef = useRef<Timeout | null>(null)

    const onChangeHandler = useCallback(
        (value: number) => {
            const newValue = +clamp(value, min, max).toFixed(0)
            setInnerValue(newValue)

            timerRef.current && clearTimeout(timerRef.current)

            timerRef.current = setTimeout(() => onChange(newValue), DEFAULT_DEBOUNCE_TIME_MS)
        },
        [min, max, onChange]
    )

    useEffect(() => {
        return () => {
            timerRef.current && clearTimeout(timerRef.current)
        }
    }, [])

    return (
        <div className={`${styles.quantityEditor} ${className} ${isDisabled && styles.disabled}`}>
            <div className={`${styles.button} ${styles.minus}`} onClick={() => onChangeHandler(innerValue - 1)} />
            <InputNumber min={min} max={max} step={1} value={innerValue} debounceTimeMs={0} onChange={onChangeHandler} />
            <div className={`${styles.button} ${styles.plus}`} onClick={() => onChangeHandler(innerValue + 1)} />
        </div>
    )
}
