import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {PaginationMenu} from '../pagination-menu/pagination-menu'
import {Button} from '../button/button'
import {DownloadButton} from '../../mobile/download-button/download-button'
import {PageTitle} from '../page-title/page-title'
import {useGoTo} from '../../../hooks/router'
import {NAV_PAYMENT} from '../../../constants/navigation'
import {HINTS} from '../../../constants/hints'
import {OperationTypes} from '../../../constants/operation-types'
import {useLoadPayments, useLoadPaymentsCsv} from '../../../hooks/payments'
import {getPaymentsFilterParams, getPaymentsFilter} from '../../../helpers/payments'
import {WithTitle} from '../with-title/with-title'
import {DateRangeSelector} from '../date-range-selector/date-range-selector'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {getCurrency} from '../../../helpers/currency'
import {useAppContext} from '../../../hooks/app'
import styles from './payment-history.module.css'

interface IPaymentHistoryProps {
    className?: string
}

export const PaymentHistory = (props: IPaymentHistoryProps): JSX.Element => {
    const {className} = props
    const search = useSelector((state: IAppState) => state.router.location.search)
    const paymentsTotalCount = useSelector((state: IAppState) => state.payments.paymentsTotalCount)
    const requestCount = useSelector((state: IAppState) => state.request.count)

    const payments = useLoadPayments()
    const downloadPayments = useLoadPaymentsCsv()
    const goTo = useGoTo()
    const {isMobile} = useAppContext()

    const filterParams = getPaymentsFilterParams(search)
    const {limit, offset, dateFrom, dateTo} = filterParams

    return (
        <div className={`${styles.payments} ${className ?? ''} ${isMobile ? styles.mobile : ''}`}>
            <div className={styles.title}>
                <PageTitle title="История операций" />
                {isMobile && <DownloadButton onClick={downloadPayments} />}
            </div>
            <div className={styles.actionBar}>
                <WithTitle title="Интервал" className={styles.filter}>
                    <DateRangeSelector
                        dataTest="orders-date-range"
                        startDate={dateFrom}
                        endDate={dateTo}
                        className={styles.newOrdersPageSidebarRangeSlider}
                        onChange={(dateFrom, dateTo) => {
                            const filter = getPaymentsFilter({...filterParams, dateFrom, dateTo, offset: 0})
                            goTo(`${NAV_PAYMENT}${filter}`)
                        }}
                    />
                </WithTitle>
                {!isMobile && (
                    <Button buttonStyle="with-fill" onClick={downloadPayments}>
                        Скачать
                    </Button>
                )}
            </div>
            {payments.length > 0 ? (
                <>
                    <div className={`standard-table ${styles.table}`}>
                        <table>
                            <thead>
                                <tr>
                                    <th>Дата создания</th>
                                    <th>Приход</th>
                                    <th>Расход</th>
                                    <th>Бонусы</th>
                                </tr>
                            </thead>
                            <tbody>
                                {payments.map(payment => (
                                    <tr key={payment.id}>
                                        <td>{getDateDDMMYYYYFormat(new Date(payment.createdAt))}</td>
                                        <td>{payment.operationType === OperationTypes.Plus ? getCurrency(payment.amount) : '-'}</td>
                                        <td>{payment.operationType === OperationTypes.Minus ? getCurrency(-payment.amount) : '-'}</td>
                                        <td>{payment.reservedScore ? payment.reservedScore * payment.operationType : '-'}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    <PaginationMenu
                        className={styles.pagination}
                        currentPage={Math.ceil(offset / limit)}
                        itemsOnPage={limit}
                        totalCount={paymentsTotalCount}
                        onSelect={page => {
                            const offset = limit * page
                            const filter = getPaymentsFilter({...filterParams, offset})
                            goTo(`${NAV_PAYMENT}${filter}`)
                        }}
                    />
                </>
            ) : (
                <>{requestCount === 0 && <p>{HINTS.paymentsNotFound}</p>}</>
            )}
        </div>
    )
}
