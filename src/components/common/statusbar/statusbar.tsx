import * as React from 'react'
import {useCallback, useEffect, useRef} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {hideStatusbar} from '../../../redux/statusbar/actions'
import {useDocumentClick} from '../../../hooks/ui'
import {useAppContext} from '../../../hooks/app'
import styles from './statusbar.module.css'
import Timeout = NodeJS.Timeout

export function Statusbar(): JSX.Element {
    const message = useSelector((state: IAppState) => state.statusbar.message)
    const isVisible = useSelector((state: IAppState) => state.statusbar.isVisible)
    const isError = useSelector((state: IAppState) => state.statusbar.isError)

    const timerRef = useRef<Timeout | null>(null)
    const wrapperRef = useRef<HTMLDivElement>(null)

    const dispatch = useDispatch()
    const hideStatusbarAction = useCallback(() => dispatch(hideStatusbar()), [dispatch])
    const {isMobile} = useAppContext()

    const onClickHandler = useCallback(
        (e: MouseEvent) => {
            if (!wrapperRef.current?.contains(e.target as HTMLElement) && isVisible) {
                hideStatusbarAction()
            }
        },
        [isVisible, hideStatusbarAction]
    )

    useDocumentClick(onClickHandler)

    useEffect(() => {
        timerRef.current && clearTimeout(timerRef.current)
        if (isVisible) {
            timerRef.current = setTimeout(hideStatusbarAction, 7000)
        }
    }, [isVisible, hideStatusbarAction])

    return (
        <div
            className={`${styles.statusbar} ${isError && styles.error} ${isVisible && styles.visible} ${isMobile ? styles.mobile : ''}`}
            ref={wrapperRef}>
            <div className={styles.warningIcon} />
            <p>{message}</p>
        </div>
    )
}
