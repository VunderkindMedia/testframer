import * as React from 'react'
import {useState, useCallback, useEffect, CSSProperties, useRef, useMemo} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Tooltip} from './tooltip/tooltip'
import {getStages, IOnboardingStep, IOnboardingStage} from './onboarding-stages'
import {useGoTo} from '../../../hooks/router'
import {useAppContext, useSetOnboardingStep} from '../../../hooks/app'
import {useCreateTestOrder} from '../../../hooks/template-selector'
import {NAV_ORDER, NAV_ORDERS} from '../../../constants/navigation'
import {getDefaultOrdersLink} from '../../../helpers/orders'
import Timer = NodeJS.Timer
import {AnalyticsManager} from '../../../analytics-manager'
import {Colors} from '../../../constants/colors'
import styles from './onboarding.module.css'

const VERTICAL_SPACE = 26
const HORIZONTAL_SPACE = 26
const MAX_SEARCH_COUNT = 5
const TOOLTIP_WIDTH = 396
const TOOLTIP_HEIGHT = 150
const EDGE_SPACE = 10
const DEFAULT_HIGHLIGHT_STYLES: CSSProperties = {width: 1, height: 1, top: -1, left: -1}

export const Onboarding = (): JSX.Element => {
    const onboardingStep = useSelector((state: IAppState) => state.builder.onboarding)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const search = useSelector((state: IAppState) => state.router.location.search)
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const ordersOnPage = useSelector((state: IAppState) => state.orders.ordersOnPage)

    const [stepIndex, setStepIndex] = useState(onboardingStep ?? 0)
    const [step, setStep] = useState<IOnboardingStep | undefined>()
    const [stage, setStage] = useState<IOnboardingStage | undefined>()
    const [tooltipStyles, setTooltipStyles] = useState<CSSProperties>({})
    const [arrowStyles, setArrowStyles] = useState<CSSProperties>({})
    const [highlightStyles, setHighlightStyles] = useState<CSSProperties>(DEFAULT_HIGHLIGHT_STYLES)
    const [isPageReady, setIsPageReady] = useState(false)
    const [isElementReady, setIsElementReady] = useState(false)
    const elementSearchCount = useRef(0)
    const timerRef = useRef<Timer | null>(null)

    const goTo = useGoTo()
    const createOrder = useCreateTestOrder()
    const setOnboardingStep = useSetOnboardingStep()
    const {isMobile} = useAppContext()

    const allStages = useMemo(() => getStages(isMobile), [isMobile])

    const highlightElement = useCallback(() => {
        let currentStep: IOnboardingStep | undefined
        const currentStage = allStages.find(stage =>
            stage.steps.find(step => {
                if (step.id === stepIndex) {
                    currentStep = step
                    return true
                }
                return false
            })
        )

        if (!isPageReady || currentStep === undefined || currentStage === undefined) {
            return
        }

        const {selector, parentSelector, shadowWidth, position, highlightedSelector, focusedSelector} = currentStep
        const element = document.querySelector(selector)

        if (element) {
            const parentElement = parentSelector ? (document.querySelector(parentSelector) as HTMLDivElement) : null

            if (parentElement) {
                parentElement.style.zIndex = '100'
                parentElement.style.overflow = 'visible'
            }

            let {left, top, height, width} = element.getBoundingClientRect()

            const cloneTop = shadowWidth ? top - shadowWidth : top
            const cloneLeft = shadowWidth ? left - shadowWidth : left
            const cloneWidth = shadowWidth ? width + shadowWidth * 2 : width
            const cloneHeight = shadowWidth ? height + shadowWidth * 2 : height

            const highlightStyles: CSSProperties = {
                top: cloneTop,
                left: cloneLeft,
                width: cloneWidth,
                height: cloneHeight
            }

            left -= shadowWidth ? shadowWidth : 0
            top -= shadowWidth ? shadowWidth : 0
            width = shadowWidth ? width + shadowWidth * 2 : width
            height = shadowWidth ? height + shadowWidth * 2 : height

            const tooltipWidth = Math.min(window.innerWidth - 2 * EDGE_SPACE, TOOLTIP_WIDTH)
            const getTooltipLeft = (left: number, right: number): number => {
                const tooltipLeft = left + tooltipWidth + HORIZONTAL_SPACE > window.innerWidth ? right - tooltipWidth : left
                return tooltipLeft < EDGE_SPACE ? EDGE_SPACE : tooltipLeft
            }

            const getElementCenterY = (tooltipLeft: number, left: number, width: number): number => {
                return width / 2 > tooltipWidth ? tooltipWidth / 2 : left + width / 2 - tooltipLeft
            }

            const getElementCenterX = (tooltipTop: number, top: number, height: number): number => {
                return height / 2 > TOOLTIP_HEIGHT ? TOOLTIP_HEIGHT / 2 : top + height / 2 - tooltipTop
            }

            const tooltipStyles: CSSProperties = {width: tooltipWidth}
            const arrowStyles: CSSProperties = {}

            switch (position) {
                case 'bottom': {
                    tooltipStyles.top = top + height + VERTICAL_SPACE
                    tooltipStyles.left = getTooltipLeft(left, left + width)
                    arrowStyles.left = getElementCenterY(tooltipStyles.left, left, width)
                    break
                }
                case 'top': {
                    tooltipStyles.bottom = window.innerHeight - top + VERTICAL_SPACE
                    if (tooltipStyles.bottom + TOOLTIP_HEIGHT + EDGE_SPACE > window.innerHeight) {
                        tooltipStyles.bottom = window.innerHeight - TOOLTIP_HEIGHT - EDGE_SPACE
                    }
                    tooltipStyles.left = getTooltipLeft(left, left + width)
                    arrowStyles.left = getElementCenterY(tooltipStyles.left, left, width)
                    break
                }
                case 'right': {
                    tooltipStyles.top = top
                    tooltipStyles.left = getTooltipLeft(left + width + HORIZONTAL_SPACE, left + width)
                    arrowStyles.top = getElementCenterX(tooltipStyles.top, top, height)
                    break
                }
                case 'left': {
                    tooltipStyles.top = top
                    tooltipStyles.right = window.innerWidth - left + HORIZONTAL_SPACE
                    if (tooltipStyles.right + TOOLTIP_WIDTH + EDGE_SPACE > window.innerWidth) {
                        tooltipStyles.right = window.innerWidth - TOOLTIP_WIDTH - EDGE_SPACE
                    }
                    arrowStyles.top = getElementCenterX(tooltipStyles.top, top, height)
                }
            }

            const highlightedElements = document.querySelectorAll('.onboarding-highlighted')
            for (let i = 0; i < highlightedElements.length; i++) {
                const element = highlightedElements[i] as HTMLDivElement
                if (element.hasAttribute('fill')) {
                    element.style.fill = Colors.White
                } else {
                    element.style.stroke = Colors.Normal
                }
                element.classList.remove('onboarding-highlighted')
            }

            if (highlightedSelector && highlightedSelector.length) {
                highlightedSelector.forEach(s => {
                    const highlightedElements = document.querySelectorAll(s)
                    for (let i = 0; i < highlightedElements.length; i++) {
                        const element = highlightedElements[i] as HTMLDivElement
                        if (element.hasAttribute('fill')) {
                            element.style.fill = Colors.Selected
                        } else {
                            element.style.stroke = Colors.Active
                        }
                        element.classList.add('onboarding-highlighted')
                    }
                })
            }

            const focusedElements = document.querySelectorAll('.onboarding-focused')
            for (let i = 0; i < focusedElements.length; i++) {
                const element = focusedElements[i] as HTMLDivElement
                element.classList.remove('onboarding-focused')
            }
            if (focusedSelector) {
                const focusedElement = document.querySelector(focusedSelector) as HTMLDivElement
                focusedElement.click()
                focusedElement.classList.add('onboarding-focused')
            }

            setTooltipStyles(tooltipStyles)
            setArrowStyles(arrowStyles)
            setHighlightStyles(highlightStyles)
            setStep(currentStep)
            setStage(currentStage)
            setIsElementReady(true)
        } else if (elementSearchCount.current < MAX_SEARCH_COUNT) {
            const timerId = setTimeout(() => {
                elementSearchCount.current++
                highlightElement()
                clearTimeout(timerId)
            }, 1000)
        }
    }, [stepIndex, allStages, elementSearchCount, isPageReady])

    useEffect(() => {
        if (stepIndex !== undefined) {
            document.querySelector('.dimension-line-input-label')?.setAttribute('readonly', 'readonly')
        }
        return document.querySelector('.dimension-line-input-label')?.removeAttribute('readonly')
    }, [stepIndex])

    useEffect(() => {
        if (stepIndex === undefined) {
            return
        }

        const isTemplatesShown = new URLSearchParams(search).has('showTemplate')
        if (isMobile && stepIndex < 1) {
            if (pathname !== NAV_ORDERS || isTemplatesShown) {
                goTo(getDefaultOrdersLink(ordersOnPage))
            } else {
                !isPageReady && setIsPageReady(true)
            }
            return
        }

        if (stepIndex < 2) {
            if (!isTemplatesShown) {
                goTo(`${NAV_ORDER}?showTemplate&category=pvh`)
            } else {
                !isPageReady && setIsPageReady(true)
            }
        }
    }, [stepIndex, search, isPageReady, pathname, ordersOnPage, isMobile, goTo])

    useEffect(() => {
        if (!stepIndex || stepIndex < 2) {
            return
        }
        if (!currentOrder) {
            createOrder()
        } else {
            !isPageReady && setIsPageReady(true)
        }
    }, [stepIndex, currentOrder, isPageReady, createOrder])

    const debounceResize = useCallback(() => {
        setHighlightStyles(DEFAULT_HIGHLIGHT_STYLES)
        setIsElementReady(false)
        timerRef.current && clearTimeout(timerRef.current)
        timerRef.current = setTimeout(() => {
            highlightElement()
        }, 500)
    }, [timerRef, highlightElement])

    useEffect(() => {
        window.addEventListener('resize', debounceResize)
        window.addEventListener('scroll', debounceResize)
        document.body.setAttribute('style', 'overflow: hidden')

        return () => {
            timerRef.current && clearTimeout(timerRef.current)
            window.removeEventListener('resize', debounceResize)
            window.removeEventListener('scroll', debounceResize)
            document.body.removeAttribute('style')
        }
    }, [timerRef, debounceResize])

    useEffect(() => {
        highlightElement()
    }, [stepIndex, highlightElement])

    useEffect(() => {
        if (isMobile && isPageReady && isElementReady) {
            if (stepIndex === 9) {
                if (window.pageYOffset <= screen.width + 51) {
                    window.scrollTo({top: screen.width + 150, behavior: 'smooth'})
                }
            } else {
                window.scrollTo({top: stepIndex >= 6 ? screen.width + 50 : 0, behavior: 'smooth'})
            }
        }
    }, [isElementReady, isPageReady, stepIndex, isMobile])

    useEffect(() => {
        if (!stage?.steps) {
            return
        }

        if (stepIndex === 0) {
            AnalyticsManager.trackOnboardingStep(stage.id)
            return
        }

        const lastStep = stage.steps[stage.steps.length - 1].id
        if (stepIndex === lastStep + 1) {
            AnalyticsManager.trackOnboardingStep(stage.id, stage.title)
        }
    }, [stepIndex, stage])

    const onStepClick = useCallback(
        (index: number) => {
            setHighlightStyles(DEFAULT_HIGHLIGHT_STYLES)
            setIsPageReady(false)
            setIsElementReady(false)
            setStepIndex(index)
            setOnboardingStep(index)
        },
        [setOnboardingStep]
    )

    const onCloseClick = useCallback(() => {
        setOnboardingStep(null)
        goTo(getDefaultOrdersLink(ordersOnPage))
        const lastStage = allStages[allStages.length - 1]
        const lastStep = lastStage.steps[lastStage.steps.length - 1]
        if (stepIndex === lastStep.id) {
            AnalyticsManager.trackOnboardingStep(lastStage.id, lastStage.title)
        }
    }, [setOnboardingStep, goTo, allStages, ordersOnPage, stepIndex])

    const onCategoryClick = useCallback(
        (e: React.MouseEvent, stage: IOnboardingStage) => {
            e.preventDefault()
            setStage(stage)
            onStepClick(stage.steps[0].id)
        },
        [onStepClick]
    )

    const steps = useMemo(() => {
        return stage?.steps.map(s => s.id)
    }, [stage])

    const showNextButton = useMemo(() => {
        const lastStage = allStages[allStages.length - 1]
        const lastStep = lastStage.steps[lastStage.steps.length - 1]
        return step ? step.id < lastStep.id : false
    }, [step, allStages])

    return (
        <div className={`${styles.onboarding} ${isMobile ? styles.mobile : ''}`}>
            <div className={styles.highlighted} style={highlightStyles}></div>
            {stage && step && (
                <>
                    <Tooltip
                        className={`${styles.tooltip} ${isElementReady ? styles.visible : ''}`}
                        title={stage.title}
                        description={step.description}
                        currentStep={stepIndex}
                        steps={steps || []}
                        style={tooltipStyles}
                        arrowStyle={arrowStyles}
                        arrowPlacement={step.position}
                        showNext={showNextButton}
                        onStepClick={onStepClick}
                        onClose={onCloseClick}
                    />
                    {stage.id !== 3 && !isMobile && (
                        <ul className={`${styles.categoryList} ${isElementReady ? styles.visible : ''}`}>
                            {allStages.map(s => (
                                <li key={s.id} className={styles.categoryItem}>
                                    <a
                                        href=""
                                        className={`${styles.categoryLink} ${stage.id === s.id ? styles.active : ''}`}
                                        onClick={e => onCategoryClick(e, s)}>
                                        {s.title}
                                    </a>
                                </li>
                            ))}
                        </ul>
                    )}
                </>
            )}
        </div>
    )
}
