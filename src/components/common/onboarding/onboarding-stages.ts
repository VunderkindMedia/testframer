export interface IOnboardingStep {
    id: number
    description: string
    selector: string
    highlightedSelector?: string[]
    parentSelector?: string
    focusedSelector?: string
    shadowWidth?: number
    position: 'top' | 'bottom' | 'right' | 'left'
}

export interface IOnboardingStage {
    id: number
    title: string
    steps: IOnboardingStep[]
}

const SHADOW_WIDTH = 6
const DIMENSION_SHADOW_WIDTH = 10

export const getStages = (isMobile: boolean): IOnboardingStage[] => {
    const stages: IOnboardingStage[] = [
        {
            id: 0,
            title: 'Начало работы',
            steps: [
                {
                    id: 0,
                    description: 'Начните создание заказа кликом по кнопке "Новый заказ"',
                    selector: '[data-id="new-order-button"]',
                    parentSelector: '[data-id="header"]',
                    shadowWidth: SHADOW_WIDTH,
                    position: 'bottom'
                },
                {
                    id: 1,
                    description: 'Выберите шаблон изделия для старта работы в построителе',
                    selector: isMobile ? '[data-id="templates"] > div:nth-of-type(2)' : '[data-id="templates"]',
                    parentSelector: '[data-id="template-selector"]',
                    position: 'top'
                }
            ]
        },
        {
            id: 1,
            title: 'Построитель',
            steps: [
                {
                    id: 2,
                    description: 'Нажмите на стеклопакет, чтобы изменить заполнение, тип открывания и добавить импост',
                    selector: '.svg-solid-filling',
                    highlightedSelector: ['.svg-root-frame-group > .svg-solid-filling:first-of-type'],
                    position: isMobile ? 'top' : 'right'
                },
                {
                    id: 3,
                    description: 'Нажмите на ручку, чтобы изменить положение и тип ручки',
                    selector: '.svg-handle-clickable',
                    position: isMobile ? 'top' : 'right',
                    shadowWidth: SHADOW_WIDTH,
                    highlightedSelector: ['.svg-handle-base', '.svg-handle']
                },
                {
                    id: 4,
                    description: 'Нажмите на значения габаритов и введите нужное, чтобы изменить размер конструкции',
                    selector: '.dimension-line-input-label',
                    position: 'top',
                    shadowWidth: DIMENSION_SHADOW_WIDTH,
                    focusedSelector: '.dimension-line-input',
                    highlightedSelector: ['.svg-dimension-lines-group:last-of-type > .svg-dimension-line-group:first-child line']
                },
                {
                    id: 5,
                    description: 'Нажмите на створку, чтобы изменить тип открывания и наличие москитной сетки',
                    selector: '.svg-frame-group:not(.svg-root-frame-group)',
                    highlightedSelector: [
                        '.svg-frame-group:not(.svg-root-frame-group) .beam',
                        '.svg-frame-group:not(.svg-root-frame-group) .bead'
                    ],
                    position: isMobile ? 'top' : 'left'
                }
            ]
        },
        {
            id: 2,
            title: 'Панель управления',
            steps: [
                {
                    id: 6,
                    description: 'Нажмите, чтобы выбрать профиль изделия',
                    selector: '[data-id="sidebar-profile"]',
                    parentSelector: isMobile ? undefined : '.svg-construction-drawer-container',
                    position: isMobile ? 'bottom' : 'left',
                    shadowWidth: SHADOW_WIDTH
                },
                {
                    id: 7,
                    description: 'Нажмите, чтобы выбрать производителя фурнитуры',
                    selector: '[data-id="sidebar-furniture"]',
                    parentSelector: isMobile ? undefined : '.svg-construction-drawer-container',
                    position: isMobile ? 'bottom' : 'left',
                    shadowWidth: SHADOW_WIDTH
                },
                {
                    id: 8,
                    description: 'Нажмите, чтобы выбрать цвет ламинации изделия, есть возможность ламинации разными цветами снаружи и внутри',
                    selector: '[data-id="sidebar-lamination"]',
                    parentSelector: isMobile ? undefined : '.svg-construction-drawer-container',
                    position: isMobile ? 'bottom' : 'left',
                    shadowWidth: SHADOW_WIDTH
                }
            ]
        },
        {
            id: 3,
            title: 'Оформить заказ',
            steps: [
                {
                    id: 9,
                    description:
                        'Нажмите, чтобы рассчитать стоимость изделия, проверить на наличие ошибок и узнать актуальную дату производства',
                    selector: '[data-id="order-page-recalculate-button"]',
                    position: 'top'
                },
                {
                    id: 10,
                    description:
                        'Когда изделие будет пересчитано, а аккаунт подтвержден, вы сможете скачать коммерческое предложение для ваших клиентов',
                    selector: '[data-id="order-page-download-kp-button"]',
                    position: 'top'
                },
                {
                    id: 11,
                    description: 'Добавьте свои материалы и услуги. Сумма автоматически добавится к стоимости изделия',
                    selector: isMobile ? '[data-id="construction-goods-services"]' : '[data-id="construction-addons-buttons"]',
                    position: 'top',
                    shadowWidth: SHADOW_WIDTH
                }
            ]
        }
    ]
    if (isMobile) {
        const lastStep = {...stages[3].steps[2], id: 10}
        stages[3].steps.splice(1, 2, lastStep)
    }

    return stages
}
