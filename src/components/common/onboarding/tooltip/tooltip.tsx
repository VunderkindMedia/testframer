import React, {useCallback} from 'react'
import {CSSProperties} from 'react'
import {CloseButton} from '../../button/close-button/close-button'
import {Button} from '../../button/button'
import styles from './tooltip.module.css'

interface ITooltipProps {
    title: string
    description: string
    currentStep: number
    showNext: boolean
    steps: number[]
    className?: string
    style?: CSSProperties
    arrowStyle?: CSSProperties
    arrowPlacement: 'top' | 'bottom' | 'right' | 'left'
    onStepClick?: (step: number) => void
    onClose?: () => void
}

export const Tooltip = (props: ITooltipProps): JSX.Element => {
    const {title, description, currentStep, steps, showNext, className, style, arrowStyle, arrowPlacement, onStepClick, onClose} = props

    const onNextButtonClick = useCallback(() => {
        onStepClick && onStepClick(currentStep + 1)
    }, [onStepClick, currentStep])

    const onStepButtonClick = useCallback(
        (step: number) => {
            onStepClick && onStepClick(step)
        },
        [onStepClick]
    )

    return (
        <div className={`${styles.tooltip} ${className ?? ''}`} style={style}>
            <p className={styles.title}>{title}</p>
            <p className={styles.description}>{description}</p>
            <div className={styles.footer}>
                {steps && (
                    <div className={styles.steps}>
                        {steps.map(s => (
                            <button
                                key={s}
                                className={`${styles.stepButton} ${s === currentStep ? styles.active : ''}`}
                                onClick={() => onStepButtonClick(s)}
                            />
                        ))}
                    </div>
                )}
                {showNext ? (
                    <Button buttonStyle="with-fill" className={styles.nextButton} onClick={onNextButtonClick}>
                        Далее
                    </Button>
                ) : (
                    <Button buttonStyle="with-fill" className={styles.finishButton} onClick={onClose}>
                        Завершить обучение
                    </Button>
                )}
            </div>
            <CloseButton className={styles.closeButton} onClick={onClose} />
            <div className={`${styles.arrow} ${styles[`arrow--${arrowPlacement}`]}`} style={arrowStyle} />
        </div>
    )
}
