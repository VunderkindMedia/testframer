import * as React from 'react'
import {useSelector} from 'react-redux'
import {useCallback, useState, useEffect} from 'react'
import {IAppState} from '../../../redux/root-reducer'
import {usePermissions, useIsDemoUser} from '../../../hooks/permissions'
import {AnalyticsManager} from '../../../analytics-manager'
import {ModalRegistration} from '../authorization-pages/modal-registration/modal-registration'
import {ModalDealers} from '../profile-page/modal-dealers/modal-dealers'
import {DemoLimitModal} from '../demo-limit-modal/demo-limit-modal'

interface IRegistrationModalProps {
    isWarningMode: boolean
    onClose: () => void
}

export const RegistrationModal = (props: IRegistrationModalProps): JSX.Element | null => {
    const {isWarningMode, onClose} = props
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)

    const [registrationStep, setRegistrationStep] = useState(0)
    const [isRegistrationModalOpened, setIsRegistrationModalOpened] = useState(false)
    const [isLimitModalOpened, setIsLimitModalOpened] = useState(false)

    const permissions = usePermissions()
    const isDemoUser = useIsDemoUser()

    const actionDemoLimitModalHandler = useCallback(() => {
        AnalyticsManager.trackRegisterFromDemoLimitClick()
        setIsLimitModalOpened(false)
        setIsRegistrationModalOpened(true)
        setRegistrationStep(1)
    }, [])

    useEffect(() => {
        setIsLimitModalOpened(isWarningMode)
    }, [isWarningMode])

    useEffect(() => {
        if (permissions.isNonQualified && registrationStep === 1) {
            setRegistrationStep(2)
        }
    }, [permissions.isNonQualified, registrationStep])

    useEffect(() => {
        if (isWarningMode) {
            return
        }
        setIsRegistrationModalOpened(true)
        if (isDemoUser) {
            setRegistrationStep(1)
        } else if (permissions.isNonQualified) {
            setRegistrationStep(2)
        }
    }, [isWarningMode, permissions.isNonQualified, isDemoUser])

    if (!isLimitModalOpened && !isRegistrationModalOpened) {
        return null
    }

    if (isLimitModalOpened) {
        return <DemoLimitModal onAction={actionDemoLimitModalHandler} onClose={onClose} />
    }

    if (registrationStep === 1) {
        return <ModalRegistration onClose={onClose} />
    }

    if (registrationStep === 2 && currentDealer) {
        return <ModalDealers onClose={onClose} dealerId={currentDealer.id} />
    }

    return null
}
