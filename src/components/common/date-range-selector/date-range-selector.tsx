import * as React from 'react'
import styles from './date-range-selector.module.css'
import {DateSelector} from '../date-selector/date-selector'
import {getDayEndDate} from '../../../helpers/date'

interface IDateRangeSelectorProps {
    startDate: Date
    endDate: Date
    className?: string
    dataTest?: string
    onChange: (startDate: Date, endDate: Date) => void
}

export function DateRangeSelector(props: IDateRangeSelectorProps): JSX.Element {
    const {startDate, endDate, className, dataTest, onChange} = props

    return (
        <div className={`${styles.dateRangeSelector} ${className}`}>
            <DateSelector
                date={startDate}
                onChange={date => onChange(date, getDayEndDate(endDate))}
                dataTest={dataTest ? `${dataTest}-start` : undefined}
            />
            <p>-</p>
            <DateSelector
                date={endDate}
                onChange={date => onChange(startDate, getDayEndDate(date))}
                dataTest={dataTest ? `${dataTest}-end` : undefined}
            />
        </div>
    )
}
