import * as React from 'react'
import {Card} from '../card/card'
import {CardHeader} from '../card/card-header/card-header'
import styles from './order-params-card.module.css'

interface IOrderParamsCardProps {
    title: string
    hint: string
    children?: JSX.Element
    footer?: JSX.Element
    isDisabled?: boolean
    className?: string
    onClickHeader?: () => void
}

export function OrderParamsCard(props: IOrderParamsCardProps): JSX.Element {
    const {title, hint, children, footer, isDisabled = false, className, onClickHeader} = props

    return (
        <Card className={`${styles.orderParamsCard} ${className} ${isDisabled && styles.disabled}`}>
            <CardHeader title={title} hint={hint} onClick={onClickHeader} />
            <div className={styles.content}>{children}</div>
            {footer && <div className={styles.footer}>{footer}</div>}
        </Card>
    )
}
