import * as React from 'react'
import styles from './search-input.module.css'
import {Input} from '../input/input'
import {CloseButton} from '../button/close-button/close-button'

interface ISearchInputProps {
    value?: string
    placeholder?: string
    debounceTimeMs?: number
    className?: string
    onChange?: (value: string) => void
    onClear?: () => void
    dataTest?: string
}

export function SearchInput(props: ISearchInputProps): JSX.Element {
    const {value, placeholder, debounceTimeMs, className, onChange, onClear, dataTest} = props

    return (
        <div className={`${styles.wrapper} ${className}`}>
            <Input
                dataTest={dataTest}
                placeholder={placeholder ?? ''}
                value={value}
                className={styles.input}
                debounceTimeMs={debounceTimeMs}
                onChange={onChange}
            />
            {value && <CloseButton onClick={onClear} aria-label="Очистить" className={styles.clearButton} />}
        </div>
    )
}
