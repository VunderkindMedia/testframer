import * as React from 'react'
import {WithTitle} from '../with-title/with-title'
import {Input} from '../input/input'
import {RemoveButton} from '../button/remove-button/remove-button'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ReactComponent as CacheIconSmall} from '../../../resources/images/cache-icon-small.inline.svg'
import {ListItem} from '../../../model/list-item'
import {Service} from '../../../model/framer/service'
import {useCallback} from 'react'
import {PriceInput} from '../price-input/price-input'
import {HINTS} from '../../../constants/hints'
import {Select} from '../select/select'
import {OrderParamsCard} from '../order-params-card/order-params-card'
import {DEFAULT_DEBOUNCE_TIME_MS, DEFAULT_MINIMUM_SELECT_ITEMS_WITHOUT_FILTER} from '../../../constants/settings'
import {getCurrency} from '../../../helpers/currency'
import {useAppContext} from '../../../hooks/app'
import styles from './services-card.module.css'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'

interface IServicesCardProps {
    title: string
    constructionId?: string
    isDisabled?: boolean
    services: Service[]
    availableServices: Service[]
    className?: string
    onClickHeader?: () => void
    onChange?: (services: Service[], action: string) => void
}

export function ServicesCard(props: IServicesCardProps): JSX.Element | null {
    const {title, constructionId, isDisabled, services, className, onClickHeader, onChange} = props

    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)

    const availableServices = isDisabled ? [] : props.availableServices
    const {isMobile} = useAppContext()

    const addService = useCallback(
        (service: Service) => {
            service.constructionId = constructionId ?? null
            onChange && onChange(services.concat(service), `Добавили услугу ${service.name}`)
        },
        [constructionId, services, onChange]
    )

    const removeService = useCallback(
        (service: Service) => {
            const newServices = services.slice(0)
            const removedServiceIndex = services.indexOf(service)
            if (removedServiceIndex !== -1) {
                newServices.splice(removedServiceIndex, 1)
                onChange && onChange(newServices, `Удалили услугу ${service.name}`)
            }
        },
        [services, onChange]
    )

    const hasAvailablePartnerServices = availableServices.length !== 0
    const isVisiblePartnerServices = hasAvailablePartnerServices || services.length > 0

    if (!isVisiblePartnerServices) {
        return null
    }

    return (
        <OrderParamsCard
            className={`${className} ${isMobile ? styles.mobile : ''}`}
            title={title}
            hint={HINTS.orderServicesCardTitle}
            onClickHeader={onClickHeader}
            footer={
                (!constructionId && (
                    <p>
                        Общая стоимость:{' '}
                        <b>
                            {getCurrency(services.reduce((s, ss) => s + ss.price, 0))}{' '}
                            {currentOrder?.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                        </b>
                    </p>
                )) ||
                undefined
            }>
            <>
                <p className={styles.subheader}>Свои услуги</p>
                <div className={styles.services}>
                    {services.map((service, idx) => (
                        <div
                            key={`${service.id}-${idx}`}
                            className={styles.service}
                            data-test={`order-page-partner-services-service-card-${idx}`}>
                            <WithTitle title="Услуга" className={styles.nameWrap}>
                                <Input
                                    isDisabled={isDisabled}
                                    className={styles.name}
                                    value={service.name}
                                    pattern=".{5,}"
                                    title="Минимальная длина пять символов"
                                    isRequired={true}
                                    isAnnoyed={true}
                                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                    onChange={(value, isValid) => {
                                        if (!isValid) {
                                            return
                                        }
                                        const servicesCopy = services.slice(0)
                                        servicesCopy[idx].name = value
                                        onChange && onChange(servicesCopy, `Переменовали услугу ${service.name} в ${servicesCopy[idx].name}`)
                                    }}
                                    placeholder="Название услуги"
                                />
                            </WithTitle>
                            <WithTitle title="Цена" className={styles.cost}>
                                <PriceInput
                                    isDisabled={isDisabled}
                                    value={service.price / 100}
                                    min={0}
                                    isAnnoyed={true}
                                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                    currency={currentOrder?.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                                    onChange={value => {
                                        const servicesCopy = services.slice(0)
                                        servicesCopy[idx].price = value * 100
                                        onChange && onChange(servicesCopy, `Сменили цену услуги ${service.name}`)
                                    }}
                                />
                            </WithTitle>
                            <div className={styles.removeButton}>
                                <RemoveButton isDisabled={isDisabled} onClick={() => removeService(service)} />
                            </div>
                        </div>
                    ))}
                </div>
                {hasAvailablePartnerServices && (
                    <Select
                        title="Добавить"
                        dataTest="order-page-partner-services-select"
                        shouldShowFilter={availableServices.length > DEFAULT_MINIMUM_SELECT_ITEMS_WITHOUT_FILTER}
                        shouldShowAddButtonForEmptyFilterResult={true}
                        className={styles.addSelect}
                        onSelect={item => addService(item.data)}
                        items={availableServices.map((s, idx) => new ListItem(idx.toString(10), s.name, s))}
                    />
                )}
            </>
        </OrderParamsCard>
    )
}
