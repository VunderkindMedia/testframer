import * as React from 'react'
import styles from './context-menu.module.css'
import {useEffect, useState} from 'react'
import {useAppContext} from '../../../hooks/app'

interface IContextMenuProps {
    items: string[]
    selectedIndex?: number
    onSelect?: (item: string, index: number) => void
    className?: string
}

export function ContextMenu(props: IContextMenuProps): JSX.Element {
    const {items, selectedIndex, onSelect, className} = props

    const [selected, setSelected] = useState(selectedIndex ?? -1)

    const {isMobile} = useAppContext()

    useEffect(() => {
        if (selectedIndex !== undefined) {
            setSelected(selectedIndex)
        }
    }, [selectedIndex])

    return (
        <div className={`${styles.contextMenu} ${className} ${isMobile ? styles.mobile : ''}`} onClick={e => e.stopPropagation()}>
            {items.map((item, idx) => (
                <div
                    key={idx}
                    className={`${styles.itemWrap} ${selected === idx && styles.selected}`}
                    onClick={() => onSelect && onSelect(item, idx)}>
                    <p className={styles.item}>{item}</p>
                </div>
            ))}
        </div>
    )
}
