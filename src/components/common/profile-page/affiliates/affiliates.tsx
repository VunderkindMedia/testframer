import * as React from 'react'
import {useCallback, useEffect, useRef, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {AddButton} from '../../button/add-button/add-button'
import {PageTitle} from '../../page-title/page-title'
import {Input} from '../../input/input'
import {PriceInput} from '../../price-input/price-input'
import {RemoveButton} from '../../button/remove-button/remove-button'
import {ButtonWithHint} from '../../button/button-with-hint/button-with-hint'
import {Select} from '../../select/select'
import {ListItem} from '../../../../model/list-item'
import {
    createAffiliate,
    deleteAffiliate,
    loadEmployees,
    updateEmployee,
    deleteEmployee,
    updateAffiliateMargin,
    loadAffiliates,
    updateAffiliate
} from '../../../../redux/account/actions'
import {IAppState} from '../../../../redux/root-reducer'
import {Affiliate} from '../../../../model/framer/affiliate'
import {Employee} from '../../../../model/framer/employee'
import {Roles} from '../../../../model/framer/user-permissions'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import {usePermissions} from '../../../../hooks/permissions'
import {HINTS} from '../../../../constants/hints'
import {AnalyticsManager} from '../../../../analytics-manager'
import {Checkbox} from '../../checkbox/checkbox'
import {Margin} from '../../../../model/framer/margin'
import {useAppContext} from '../../../../hooks/app'
import {AffiliateMarginTypes} from '../../../../model/affiliate-margin-types'
import {WithTitle} from '../../with-title/with-title'
import {ConfirmDealerLink} from '../../confirm-dealer-link/confirm-dealer-link'
import {isValidEmail} from '../../../../helpers/validation'
import styles from './affiliates.module.css'

const MAX_TEMPLATE_ROWS = 3

const userRolesItems: {[key: string]: ListItem<null>} = {
    [Roles.ChiefManager]: new ListItem(Roles.ChiefManager, 'Главный менеджер', null),
    [Roles.CommonManager]: new ListItem(Roles.CommonManager, 'Менеджер', null)
}

export function Affiliates(): JSX.Element {
    const affiliates = useSelector((state: IAppState) => state.account.affiliates)
    const employees = useSelector((state: IAppState) => state.account.employees)
    const isSubdealershipAvailable = useSelector((state: IAppState) => state.account.partner?.isSubdealershipAvailable)
    const isSubdealerDiscountsAvailable = useSelector((state: IAppState) => state.account.partner?.isSubdealerDiscountsAvailable)
    const isSubdealerAdditionalMarginsAvailable = useSelector(
        (state: IAppState) => state.account.partner?.isSubdealerAdditionalMarginsAvailable
    )

    const affiliatesForm = useRef<HTMLFormElement>(null)
    const employeesForm = useRef<HTMLFormElement>(null)

    const [affiliatesList, setAffiliatesList] = useState<Affiliate[]>([])
    const [affiliatesErrorIndex, setAffiliatesErrorIndex] = useState<number[]>([])
    const [employeesList, setEmployeesList] = useState<Employee[]>([])
    const [employeesErrorRow, setEmployeesErrorRow] = useState<number>(-1)

    const dispatch = useDispatch()
    const permissions = usePermissions()
    const loadAffiliatesAction = useCallback(() => dispatch(loadAffiliates()), [dispatch])
    const loadEmployeesAction = useCallback(() => dispatch(loadEmployees()), [dispatch])
    const {isMobile} = useAppContext()

    useEffect(() => {
        affiliates && setAffiliatesList(affiliates)
    }, [affiliates])

    useEffect(() => {
        employees && setEmployeesList(employees)
    }, [employees])

    useEffect(() => {
        !affiliates && loadAffiliatesAction()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        !employees && loadEmployeesAction()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const addAffiliateHandler = useCallback(() => {
        AnalyticsManager.trackEvent({
            name: 'Клик "Добавить филиал"',
            target: 'profile-page-add-affiliate-button'
        })
        if (affiliatesList.filter(a => !a.id).length < MAX_TEMPLATE_ROWS) {
            setAffiliatesList([...affiliatesList, new Affiliate()])
        }
    }, [affiliatesList])

    const addEmployeeHandler = useCallback(() => {
        AnalyticsManager.trackEvent({
            name: 'Клик "Добавить сотрудника"',
            target: 'profile-page-add-employee-button'
        })
        if (employeesList.filter(a => !a.id).length < MAX_TEMPLATE_ROWS) {
            setEmployeesList([...employeesList, {name: '', email: '', filialId: 0, userRoles: []}])
        }
    }, [employeesList])

    const updateAffiliateNameHandler = useCallback(
        (name: string, index: number, isExternal: boolean, id?: number) => {
            if (affiliatesForm.current) {
                name = name.trim()
                if (name.length < 2) {
                    if (!affiliatesErrorIndex.includes(index)) {
                        setAffiliatesErrorIndex([...affiliatesErrorIndex, index])
                    }
                    return
                }
            }

            const errorIndex = affiliatesErrorIndex.findIndex(i => i === index)
            if (errorIndex !== -1) {
                setAffiliatesErrorIndex([...affiliatesErrorIndex.slice(0, errorIndex), ...affiliatesErrorIndex.slice(errorIndex + 1)])
            }
            if (id) {
                dispatch(updateAffiliate(id, name, isExternal))
            } else {
                dispatch(createAffiliate(name))
            }
        },
        [dispatch, affiliatesErrorIndex]
    )

    const updateAffiliateExternalHandler = useCallback(
        (name: string, index: number, isExternal: boolean, id: number) => {
            dispatch(updateAffiliate(id, name, isExternal))
        },
        [dispatch]
    )

    const updateEmployeeHandler = useCallback(
        (employee: Employee, index: number) => {
            if (employeesForm.current) {
                const elements = employeesForm.current.elements
                const nameInput = elements[3 * index] as HTMLInputElement
                console.log('nameInput :>> ', nameInput)
                const emailInput = elements[3 * index + 1] as HTMLInputElement
                if (!nameInput.checkValidity() || !isValidEmail(emailInput.value) || !employee.userRoles.length || !employee.filialId) {
                    setEmployeesErrorRow(index)
                    const employees = [...employeesList]
                    employees[index] = employee
                    setEmployeesList(employees)
                    return
                }
            }

            setEmployeesErrorRow(-1)
            dispatch(updateEmployee(employee))
        },
        [dispatch, employeesList]
    )

    const deleteAffiliateHandler = useCallback(
        (index: number) => {
            const id = affiliatesList[index].id
            if (id) {
                dispatch(deleteAffiliate(id))
            } else {
                const affiliates = [...affiliatesList]
                affiliates.splice(index, 1)
                setAffiliatesList(affiliates)
            }
        },
        [affiliatesList, dispatch]
    )

    const deleteEmployeeHandler = useCallback(
        (index: number) => {
            const id = employeesList[index].id
            if (id) {
                dispatch(deleteEmployee(id))
            } else {
                const employees = [...employeesList]
                employees.splice(index, 1)
                setEmployeesList(employees)
            }
        },
        [dispatch, employeesList]
    )

    const affiliatesSelectList = affiliates ? affiliates.map(({id, name}) => new ListItem(String(id), name, null)) : []
    const isSubdealersAvailable = permissions.isBossManager && isSubdealershipAvailable

    const updateMargin = useCallback(
        (id: number, value: number, type: AffiliateMarginTypes): void => {
            const margin = new Margin()
            margin.percent = value
            dispatch(updateAffiliateMargin(id, margin, type))
        },
        [dispatch]
    )

    const renderPriceFields = useCallback(
        (affiliate: Affiliate): JSX.Element | null => {
            const {
                id,
                margin,
                laminationMargin,
                glassMargin,
                tintMargin,
                entranceDoorMargin,
                nonstandardMargin,
                subdealerConstrDiscount,
                subdealerGoodsDiscount
            } = affiliate

            if (!id) {
                return null
            }

            if (!isSubdealerDiscountsAvailable && !isSubdealerAdditionalMarginsAvailable) {
                return (
                    <div className={styles.priceGroup}>
                        <p className={styles.marginsTitle}>Наценка на конструкции</p>
                        <PriceInput
                            value={margin.percent}
                            currency="%"
                            className={styles.price}
                            onChange={value => updateMargin(id, value, AffiliateMarginTypes.Construction)}
                        />
                    </div>
                )
            }

            const priceFields = isSubdealerDiscountsAvailable
                ? [
                      {
                          margin: subdealerConstrDiscount,
                          title: 'на констр.',
                          hint: 'Cкидка на конструкции',
                          type: AffiliateMarginTypes.ConstructionDiscount
                      },
                      {
                          margin: subdealerGoodsDiscount,
                          title: 'на допы',
                          hint: 'Скидка на доп. материалы',
                          type: AffiliateMarginTypes.GoodsDiscount
                      }
                  ]
                : [
                      {
                          margin: margin,
                          title: 'на констр.',
                          hint: 'Наценка на конструкции',
                          type: AffiliateMarginTypes.Construction
                      },
                      {
                          margin: laminationMargin,
                          title: 'на лам.',
                          hint: 'Наценка на ламинацию',
                          type: AffiliateMarginTypes.Lamination
                      },
                      {
                          margin: glassMargin,
                          title: 'на стекл.',
                          hint: 'Наценка на стеклопакеты',
                          type: AffiliateMarginTypes.Glass
                      },
                      {
                          margin: tintMargin,
                          title: 'на тон.',
                          hint: 'Наценка на тонировку',
                          type: AffiliateMarginTypes.Tint
                      },
                      {
                          margin: entranceDoorMargin,
                          title: 'на вх. дв.',
                          hint: 'Наценка на входные двери',
                          type: AffiliateMarginTypes.EntranceDoor
                      },
                      {
                          margin: nonstandardMargin,
                          title: 'на нест.',
                          hint: 'Наценка на нестандарт',
                          type: AffiliateMarginTypes.NonStandard
                      }
                  ]

            return (
                <div className={styles.margins}>
                    {!isMobile && (
                        <WithTitle title="" className={styles.marginsTitleGroup}>
                            <p className={styles.marginsTitle}>{isSubdealerDiscountsAvailable ? 'Скидки' : 'Наценки'}</p>
                        </WithTitle>
                    )}
                    {priceFields.map(marginItem => {
                        const {margin, type, title, hint} = marginItem

                        if (isMobile) {
                            return (
                                <div key={type} className={styles.priceGroup}>
                                    <p className={styles.marginsTitle}>{hint}</p>
                                    <PriceInput
                                        value={margin.percent}
                                        currency="%"
                                        className={styles.price}
                                        onChange={value => updateMargin(id, value, type)}
                                    />
                                </div>
                            )
                        }

                        return (
                            <WithTitle key={type} title={title} hint={hint} className={styles.priceGroup}>
                                <PriceInput
                                    value={margin.percent}
                                    currency="%"
                                    className={styles.price}
                                    onChange={value => updateMargin(id, value, type)}
                                />
                            </WithTitle>
                        )
                    })}
                </div>
            )
        },
        [isSubdealerDiscountsAvailable, isSubdealerAdditionalMarginsAvailable, isMobile, updateMargin]
    )

    return (
        <div className={`${styles.affiliates} ${isMobile ? styles.mobile : ''}`}>
            {permissions.isNonQualified && (
                <ConfirmDealerLink className={styles.permissionError} title="Для управления филиалами и сотрудниками необходимо" />
            )}
            <div className={styles.listTitle}>
                <PageTitle title="Филиалы" className={styles.affiliateTitle} />
                {isSubdealersAvailable && !isMobile && <PageTitle title="Внешние филиалы" />}
            </div>
            {affiliatesList.length > 0 && (
                <form ref={affiliatesForm} className={styles.affiliatesForm}>
                    <ul className={styles.list} data-test="affiliates-list">
                        {affiliatesList.map((affiliate, index) => {
                            const {id, name, isExternal, isMain} = affiliate

                            return (
                                <li key={index} className={styles.item} data-test={`affiliates-item-${id ? id : 'new'}`}>
                                    <div className={styles.title}>
                                        <Input
                                            className={styles.titleInput}
                                            value={name}
                                            isRequired={true}
                                            pattern=".{2,}"
                                            placeholder="Название"
                                            isAnnoyed={affiliatesErrorIndex.includes(index)}
                                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                            onChange={value => updateAffiliateNameHandler(value, index, isExternal, id)}
                                        />
                                        <RemoveButton
                                            className={isMain ? styles.hiddenRemoveButton : ''}
                                            onClick={() => {
                                                deleteAffiliateHandler(index)
                                            }}
                                        />
                                    </div>
                                    {!isMain && id && isSubdealersAvailable && (
                                        <>
                                            <Checkbox
                                                className={styles.external}
                                                isChecked={isExternal}
                                                title={isMobile ? 'Внешний филиал' : ''}
                                                onChange={value => updateAffiliateExternalHandler(name, index, value, id)}
                                            />
                                            {isExternal && renderPriceFields(affiliate)}
                                        </>
                                    )}
                                </li>
                            )
                        })}
                    </ul>
                </form>
            )}
            <ButtonWithHint hint={permissions.isNonQualified ? HINTS.nonQualifiedAddRestriction : ''}>
                <AddButton
                    title="Добавить"
                    dataTest="profile-page-add-affiliate-button"
                    onClick={addAffiliateHandler}
                    className={styles.addButton}
                    isDisabled={permissions.isNonQualified}
                />
            </ButtonWithHint>
            <PageTitle title="Сотрудники" className={styles.listTitle} />
            {employeesList.length > 0 && (
                <form ref={employeesForm} className={styles.employeesForm} onSubmit={e => e.preventDefault()}>
                    <ul className={styles.employeesList} data-test="employees-list">
                        {employeesList.map(({filialId, name, email, userRoles, id}, index) => {
                            const currentEmployee = employeesList[index]
                            return (
                                <li key={index} className={styles.employeesItem} data-test={`employees-item-${id ? id : 'new'}`}>
                                    <Select
                                        className={styles.role}
                                        items={Object.values(userRolesItems)}
                                        placeholder="Должность"
                                        hasError={!userRoles[0] && employeesErrorRow === index}
                                        selectedItem={userRolesItems[userRoles[0]]}
                                        dataTest="employees-role"
                                        onSelect={item => {
                                            currentEmployee.userRoles = [item.id as Roles]
                                            updateEmployeeHandler(currentEmployee, index)
                                        }}
                                    />
                                    <Input
                                        className={styles.name}
                                        value={name}
                                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                        isRequired={true}
                                        placeholder="Имя"
                                        isAnnoyed={employeesErrorRow === index}
                                        pattern=".{2,}"
                                        onChange={value => {
                                            currentEmployee.name = value.trim()
                                            updateEmployeeHandler(currentEmployee, index)
                                        }}
                                    />
                                    <Input
                                        className={styles.email}
                                        type="email"
                                        value={email}
                                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                        isRequired={true}
                                        placeholder="e-mail"
                                        isAnnoyed={employeesErrorRow === index}
                                        hasError={employeesErrorRow === index && !isValidEmail(email)}
                                        onChange={value => {
                                            currentEmployee.email = value.trim()
                                            updateEmployeeHandler(currentEmployee, index)
                                        }}
                                    />
                                    <div className={styles.employeeRow}>
                                        <Select
                                            className={styles.employeeAffiliate}
                                            placeholder="Филиал"
                                            items={affiliatesSelectList}
                                            hasError={!filialId && employeesErrorRow === index}
                                            selectedItem={affiliatesSelectList.find(item => item.id === String(filialId))}
                                            dataTest="employees-affiliate"
                                            onSelect={item => {
                                                currentEmployee.filialId = +item.id
                                                updateEmployeeHandler(currentEmployee, index)
                                            }}
                                        />
                                        <RemoveButton
                                            onClick={() => {
                                                deleteEmployeeHandler(index)
                                            }}
                                        />
                                    </div>
                                </li>
                            )
                        })}
                    </ul>
                </form>
            )}
            <ButtonWithHint hint={permissions.isNonQualified ? HINTS.nonQualifiedAddRestriction : ''}>
                <AddButton
                    title="Добавить"
                    onClick={addEmployeeHandler}
                    dataTest="profile-page-add-employee-button"
                    className={styles.addButton}
                    isDisabled={permissions.isNonQualified}
                />
            </ButtonWithHint>
        </div>
    )
}
