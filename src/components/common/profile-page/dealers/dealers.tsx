import * as React from 'react'
import {useState, useCallback, useEffect, useRef} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {RadioGroup, RadioGroupStyles} from '../../radio-group/radio-group'
import {PageTitle} from '../../page-title/page-title'
import {Button} from '../../button/button'
import {ModalDealers} from '../modal-dealers/modal-dealers'
import {AddButton} from '../../button/add-button/add-button'
import {DealerConfirmationStatus} from '../../../../model/framer/dealer'
import {RUB_SYMBOL} from '../../../../constants/strings'
import {getCurrency} from '../../../../helpers/currency'
import {AnalyticsManager} from '../../../../analytics-manager'
import {FooterLinks} from '../../authorization-pages/footer-links/footer-links'
import {useAppContext} from '../../../../hooks/app'
import {usePermissions} from '../../../../hooks/permissions'
import {setCurrentDealerRemote} from '../../../../redux/account/actions'
import {ReactComponent as CacheIcon} from '../../../../resources/images/cache-icon.inline.svg'
import {Balance} from '../../balance/balance'
import styles from './dealers.module.css'

export function Dealers(): JSX.Element | null {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const partner = useSelector((state: IAppState) => state.account.partner)
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)
    const isCashMode = useSelector((state: IAppState) => state.account.isCashMode)

    const showConfirmModal = new URLSearchParams(search).get('showConfirmModal')
    const showConfirmModalShowedRef = useRef(false)

    useEffect(() => {
        const dealer = partner?.dealers.find(dealer => dealer.confirmationStatus === DealerConfirmationStatus.DealerNotConfirmed)
        if (!dealer || !showConfirmModal || showConfirmModalShowedRef.current) {
            return
        }
        setShowModalId(dealer.id)
        showConfirmModalShowedRef.current = true
    }, [partner, showConfirmModal])

    const dispatch = useDispatch()
    const setCurrentDealerAction = useCallback((dealerId: number) => dispatch(setCurrentDealerRemote(dealerId)), [dispatch])
    const {isMobile} = useAppContext()
    const {isBossManager} = usePermissions()

    const [showModalId, setShowModalId] = useState<number>(-1)

    if (!partner || !currentDealer) {
        return null
    }

    const {dealers, selectedDealerId} = partner
    const disabledDealers = dealers.filter(
        dealer => dealer.confirmationStatus !== DealerConfirmationStatus.DealerConfirmed && dealer.id !== currentDealer.id
    )

    return (
        <div className={`${styles.dealers} ${isMobile ? styles.mobile : ''}`}>
            <PageTitle title="Список контрагентов" className={styles.title} />
            <RadioGroup
                items={dealers.map(dealer => dealer.name)}
                disabledItems={disabledDealers.map(dealer => dealer.name)}
                className={styles.list}
                radioGroupStyle={RadioGroupStyles.Vertical}
                selectedIndex={dealers.findIndex(d => d.id === selectedDealerId)}
                dataTest="dealer-list"
                onSelect={index => setCurrentDealerAction(dealers[index].id)}
                getDecorator={index => (
                    <div className={styles.details}>
                        <p className={styles.balance}>
                            <Balance value={dealers[index].balance} />
                            {isBossManager &&
                                (isCashMode
                                    ? dealers[index].framerCreditLimitInCents > 0 && (
                                          <span>
                                              (Кредитный лимит {getCurrency(dealers[index].creditLimitInCents)} <CacheIcon />)
                                          </span>
                                      )
                                    : dealers[index].creditLimitInCents > 0 &&
                                      `  (Кредитный лимит ${getCurrency(dealers[index].creditLimitInCents)} ${RUB_SYMBOL})`)}
                        </p>
                        {dealers[index].confirmationStatus === DealerConfirmationStatus.DealerNotConfirmed && (
                            <Button
                                dataTest="profile-page-confirm-account-button"
                                buttonStyle="with-border"
                                onClick={() => {
                                    setShowModalId(dealers[index].id)
                                    AnalyticsManager.trackConfirmAccountClick()
                                }}>
                                Подтвердить учетную запись
                            </Button>
                        )}
                        {dealers[index].confirmationStatus === DealerConfirmationStatus.DealerPendingConfirmation && (
                            <span>Ожидается подтверждение</span>
                        )}
                    </div>
                )}
            />
            <div className={styles.total}>
                <p className={styles.totalBalanceTitle}>Общий счёт</p>
                <Balance value={dealers.map(d => d.balance).reduce((totalBalance, balance) => totalBalance + balance, 0)} />
            </div>
            <AddButton title="Добавить" onClick={() => setShowModalId(0)} dataTest="add-dealer" />
            {showModalId !== -1 && <ModalDealers onClose={() => setShowModalId(-1)} dealerId={showModalId} />}
            <FooterLinks
                className={`${styles.offer} ${!isMobile && styles.horizontal}`}
                selectedRegion={partner.filial.city?.kladrRegionId ?? null}
            />
        </div>
    )
}
