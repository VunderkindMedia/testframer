import * as React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {useCallback, useState, useEffect} from 'react'
import {ServiceEditor} from '../../service-editor/service-editor'
import {ProfilePageDraftServices} from '../profile-page-draft-services'
import {AddButton} from '../../button/add-button/add-button'
import {Service} from '../../../../model/framer/service'
import {PageTitle} from '../../page-title/page-title'
import {HINTS} from '../../../../constants/hints'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import {PriceInput} from '../../price-input/price-input'
import {Input} from '../../input/input'
import {getIndex} from '../../../../helpers/guid'
import {useAppContext} from '../../../../hooks/app'
import {addService, removeService, updateService} from '../../../../redux/account/actions'
import {useInstallation, useUpdateInstallation} from '../../../../hooks/services'
import {IAppState} from '../../../../redux/root-reducer'
import styles from './additional-services.module.css'

export function AdditionalServices(): JSX.Element | null {
    const partner = useSelector((state: IAppState) => state.account.partner)

    const installation = useInstallation()
    const updateInstallationCosts = useUpdateInstallation()
    const dispatch = useDispatch()
    const addServiceAction = useCallback((service: Service) => dispatch(addService(service)), [dispatch])
    const updateServiceAction = useCallback((service: Service) => dispatch(updateService(service)), [dispatch])
    const removeServiceAction = useCallback((serviceId: number) => dispatch(removeService(serviceId)), [dispatch])

    const [draftServices, setDraftServices] = useState<Service[]>([new Service(getIndex(), '')])

    useEffect(() => {
        if (!partner) {
            return
        }

        const addedService = partner.services.find(service => draftServices.filter(s => s.name === service.name).length !== 0)

        if (addedService) {
            const draftServicesCopy = draftServices.slice(0)
            const index = draftServicesCopy.findIndex(s => s.name === addedService.name)
            draftServicesCopy.splice(index, 1)
            if (draftServicesCopy.length === 0) {
                draftServicesCopy.push(new Service(getIndex(), ''))
            }
            setDraftServices(draftServicesCopy)
        }
    }, [partner, draftServices])

    const addButtonHandler = useCallback(() => {
        const serviceEditorsCopy = draftServices.slice(0)
        if (serviceEditorsCopy.length < 3) {
            serviceEditorsCopy.push(new Service(getIndex(), ''))
            setDraftServices(serviceEditorsCopy)
        }
    }, [draftServices, setDraftServices])

    const {isMobile} = useAppContext()

    if (!partner) {
        return null
    }

    const {services} = partner

    return (
        <div className={`${isMobile ? styles.mobile : ''}`}>
            {installation && (
                <>
                    <PageTitle title="Расчетные услуги" hint={HINTS.installationTitle} className={styles.title} />
                    <div className={styles.installationList}>
                        {installation.constructionsCosts.map(cost => {
                            const {id, unit, pricePerUnitForOffer} = cost

                            return (
                                <div key={id} className={styles.installationItem} data-test="installation">
                                    <Input className={styles.name} value={installation.name} isReadonly={true} />
                                    <PriceInput
                                        value={pricePerUnitForOffer / 100}
                                        currency={`₽/${unit.shortName}`}
                                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                        className={styles.price}
                                        onChange={(value: number) => {
                                            const costClone = cost.clone
                                            costClone.pricePerUnitForOffer = value * 100
                                            updateInstallationCosts(costClone)
                                        }}
                                    />
                                </div>
                            )
                        })}
                    </div>
                </>
            )}

            <PageTitle title="Услуги" hint={HINTS.additionalServicesTitle} className={styles.title} />
            <div className={styles.list}>
                {services
                    .filter((s: Service) => !s.installationId || s.installationId <= 0)
                    .map(service => (
                        <ServiceEditor
                            key={service.id}
                            className={styles.editor}
                            service={service}
                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                            onChange={service => {
                                if (service.valid) {
                                    updateServiceAction(service)
                                }
                            }}
                            onRemove={serviceId => removeServiceAction(serviceId)}
                        />
                    ))}
                <ProfilePageDraftServices
                    className={styles.editor}
                    draftServices={draftServices}
                    setDraftServices={setDraftServices}
                    addServiceAction={addServiceAction}
                />
            </div>
            <AddButton title="Добавить" dataTest="add-additional-service" onClick={addButtonHandler} />
        </div>
    )
}
