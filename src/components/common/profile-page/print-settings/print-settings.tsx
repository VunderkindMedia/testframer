import * as React from 'react'
import {useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {Input} from '../../input/input'
import {PageTitle} from '../../page-title/page-title'
import {setOfferFields} from '../../../../redux/account/actions'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import styles from './print-settings.module.css'

export function PrintSettings(): JSX.Element | null {
    const partner = useSelector((state: IAppState) => state.account.partner)

    const dispatch = useDispatch()
    const setOfferFieldsAction = useCallback(
        (offerAddonLine1: string, offerAddonLine2: string) => dispatch(setOfferFields(offerAddonLine1, offerAddonLine2)),
        [dispatch]
    )

    if (!partner) {
        return null
    }

    const {offerAddonLine1, offerAddonLine2} = partner

    return (
        <div className={styles.settings}>
            <PageTitle title="Настройка печати КП" className={styles.title} />
            <Input
                dataTest="offerAddonLine1"
                value={offerAddonLine1}
                debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                className={styles.input}
                onChange={value => {
                    setOfferFieldsAction(value.trim(), offerAddonLine2)
                }}
                placeholder="Наименование организации"
            />
            <Input
                dataTest="offerAddonLine2"
                value={offerAddonLine2}
                className={styles.input}
                debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                onChange={value => {
                    setOfferFieldsAction(offerAddonLine1, value.trim())
                }}
                placeholder="Контактная информация"
            />
        </div>
    )
}
