import {ITab} from '../../desktop/tabs-panel/tabs-panel'
import {NAV_PROFILE} from '../../../constants/navigation'

export enum ProfileTabIds {
    Affiliate = 'affiliate',
    AdditionalServices = 'additional-services',
    PartnerGoods = 'partner-goods',
    Margins = 'margins',
    Dealers = 'dealers',
    PrintSettings = 'print-settings'
}

export const profileTabs = {
    defaultTab: ProfileTabIds.Margins,

    get: (isBoss: boolean): ITab[] => {
        const tabs: ITab[] = [
            {
                id: ProfileTabIds.Margins,
                title: 'Наценки'
            },
            {
                id: ProfileTabIds.PrintSettings,
                title: 'Настройка КП'
            },
            {
                id: ProfileTabIds.AdditionalServices,
                title: 'Услуги'
            },
            {
                id: ProfileTabIds.PartnerGoods,
                title: 'Доп. материалы'
            }
        ]

        if (isBoss) {
            tabs.push(
                {
                    id: ProfileTabIds.Affiliate,
                    title: 'Филиалы и сотрудники'
                },
                {
                    id: ProfileTabIds.Dealers,
                    title: 'Контрагенты'
                }
            )
        }
        return tabs
    },

    getFullPath: (tab: string): string => {
        return `${NAV_PROFILE}?activeTab=${tab}`
    }
}
