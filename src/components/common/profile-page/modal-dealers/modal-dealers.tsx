import * as React from 'react'
import {useCallback, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {ModalOverlay} from '../../modal/modal-overlay/modal-overlay'
import {CloseButton} from '../../button/close-button/close-button'
import {IAppState} from '../../../../redux/root-reducer'
import {RequestStates} from '../../../../model/request-states'
import {clearRequestState} from '../../../../redux/request/actions'
import {DealerForm} from '../dealer-form/dealer-form'
import {DealerInfo} from '../../../../model/framer/dealer-info'
import {setDealerInfo} from '../../../../redux/account/actions'
import {useAppContext} from '../../../../hooks/app'
import styles from './modal-dealers.module.css'

interface IModalDealersProps {
    dealerId: number
    onClose: () => void
}

export function ModalDealers(props: IModalDealersProps): JSX.Element {
    const {dealerId, onClose} = props
    const partnerEmail = useSelector((state: IAppState) => state.account.partner?.contactInfo.email ?? '')
    const saveDealerStatus = useSelector((state: IAppState) => state.request.keys['saveDealer'])
    const dispatch = useDispatch()
    const {isMobile} = useAppContext()
    const newDealerInfo = useSelector((state: IAppState): DealerInfo | null => state.account.partner?.newDealerInfo ?? null)

    const onCloseHandler = useCallback(() => {
        if (newDealerInfo) {
            dispatch(setDealerInfo(null))
        }
        onClose()
        if (saveDealerStatus) {
            dispatch(clearRequestState('saveDealer'))
        }
    }, [newDealerInfo, saveDealerStatus, dispatch, onClose])

    useEffect(() => {
        if (saveDealerStatus === RequestStates.Succeeded) {
            onCloseHandler()
        }
    }, [dispatch, onCloseHandler, saveDealerStatus])

    return (
        <ModalOverlay isFullMode={true} className={`${styles.modal} ${isMobile ? styles.mobile : ''}`}>
            <div className={styles.container} data-test="modal-dealer">
                <CloseButton className={styles.closeButton} onClick={onCloseHandler} />
                <DealerForm dealerId={dealerId} partnerEmail={partnerEmail} />
            </div>
        </ModalOverlay>
    )
}
