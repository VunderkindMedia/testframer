import * as React from 'react'
import {Service} from '../../../model/framer/service'
import {ServiceEditor} from '../service-editor/service-editor'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'

export interface IProfilePageDraftServicesProps {
    draftServices: Service[]
    className?: string
    setDraftServices: (services: Service[]) => void
    addServiceAction: (service: Service) => void
}

export function ProfilePageDraftServices(props: IProfilePageDraftServicesProps): JSX.Element {
    const {draftServices, className, setDraftServices, addServiceAction} = props

    return (
        <>
            {draftServices.map(draftService => (
                <ServiceEditor
                    className={className}
                    key={draftService.id}
                    service={draftService}
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    onRemove={() => {
                        const services = draftServices.slice(0)
                        const index = services.findIndex(s => s.id === draftService.id)
                        services.splice(index, 1)
                        setDraftServices(services)
                    }}
                    onChange={service => {
                        const services = draftServices.slice(0)
                        const index = services.findIndex(s => s.id === draftService.id)
                        services[index] = service
                        setDraftServices(services)

                        if (service.valid && service.hasPrice) {
                            addServiceAction(service)
                        }
                    }}
                />
            ))}
        </>
    )
}
