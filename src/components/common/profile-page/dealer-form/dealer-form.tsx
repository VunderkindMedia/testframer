import * as React from 'react'
import {useState, useCallback, useEffect, useRef} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {Input} from '../../input/input'
import {Select} from '../../select/select'
import {Button} from '../../button/button'
import {ListItem} from '../../../../model/list-item'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import {HINTS} from '../../../../constants/hints'
import {checkInn, saveDealer} from '../../../../redux/account/actions'
import {IAppState} from '../../../../redux/root-reducer'
import {DealerInfo, DealerType, getDealerTypeName} from '../../../../model/framer/dealer-info'
import {Dealer} from '../../../../model/framer/dealer'
import {AutocompleteInput} from '../../autocomplete-input/autocomplete-input'
import {IDadataSuggestion, KladrManager} from '../../../../kladr-manager'
import {useAppContext} from '../../../../hooks/app'
import {AnalyticsManager} from '../../../../analytics-manager'
import {Hint} from '../../hint/hint'
import {isRegisteredFromDemo} from '../../../../helpers/account'
import styles from './dealer-form.module.css'

const dealerTypeList = [
    new ListItem(DealerType.Individual, getDealerTypeName(DealerType.Individual), null),
    new ListItem(DealerType.Legal, getDealerTypeName(DealerType.Legal), null),
    new ListItem(DealerType.Person, getDealerTypeName(DealerType.Person), null),
    new ListItem(DealerType.SelfEmployed, getDealerTypeName(DealerType.SelfEmployed), null)
]

interface IDealerFormProps {
    partnerEmail: string
    dealerId: number
}

export function DealerForm(props: IDealerFormProps): JSX.Element {
    const {dealerId, partnerEmail} = props
    const [dealerName, setDealerName] = useState('')
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [isInnChecked, setIsInnChecked] = useState(false)
    const [inn, setInn] = useState('')
    const [isShouldShowFormError, setShouldShowFormError] = useState(false)
    const [isPersonDealerType, setIsPersonDealerType] = useState(false)
    const [legalAddress, setLegalAddress] = useState<IDadataSuggestion | null>(null)
    const [physicalAddress, setPhysicalAddress] = useState<IDadataSuggestion | null>(null)
    const dealerForm = useRef<HTMLFormElement>(null)
    const dispatch = useDispatch()
    const {isMobile} = useAppContext()
    const dealer = useSelector((state: IAppState): Dealer | null => {
        const dealer = state.account.partner?.dealers.find(d => d.id === dealerId)
        return dealer ?? null
    })
    const newDealerInfo = useSelector((state: IAppState): DealerInfo | null => {
        return state.account.partner?.newDealerInfo ?? null
    })
    const dealerInfo = dealer ? dealer.info : newDealerInfo

    useEffect(() => {
        setIsInnChecked(!!dealerInfo)
        if (dealerInfo) {
            const isPersonType = dealerInfo.dealerType === DealerType.SelfEmployed || dealerInfo.dealerType === DealerType.Person

            setIsPersonDealerType(isPersonType)
            if (isPersonType) {
                setName(dealerInfo.dealerName)
            }
            setInn(dealerInfo.inn)
            setDealerName(dealerInfo.dealerName)
        }
    }, [dealerInfo])

    const onInnChangeHandler = useCallback(
        (value: string) => {
            const inn = value.trim()
            setInn(value)
            setIsInnChecked(false)
            setName('')
            setPhone('')
            if (inn && /^\d{10}(\d{2})*$/.exec(inn)) {
                setShouldShowFormError(false)
                dispatch(checkInn(dealerId, inn))
            } else {
                setShouldShowFormError(true)
            }
        },
        [dealerId, dispatch]
    )

    const onSaveHandler = useCallback(() => {
        if (!dealerInfo) {
            return
        }

        let hasError = !dealerForm || !dealerForm.current || !dealerForm.current.checkValidity()
        if (!isPersonDealerType) {
            hasError = hasError || !legalAddress?.data?.house?.length || !physicalAddress?.data?.house?.length
        }

        if (hasError) {
            setShouldShowFormError(true)
            return
        }

        setShouldShowFormError(false)
        const updatedDealer = new DealerInfo(
            dealerInfo.inn,
            dealerInfo.kpp,
            dealerName,
            dealerInfo.dealerType,
            dealer ? dealer.id : undefined,
            name,
            phone,
            partnerEmail,
            legalAddress ?? undefined,
            physicalAddress ?? undefined
        )
        dispatch(saveDealer(updatedDealer))

        if (dealerId) {
            AnalyticsManager.trackConfirmAccountFromModalClick()
        }
        if (isRegisteredFromDemo()) {
            AnalyticsManager.trackConfirmFromDemoSendForm()
        }
    }, [dealerInfo, dealer, dealerName, name, phone, partnerEmail, legalAddress, physicalAddress, isPersonDealerType, dealerId, dispatch])

    const getAutocompleteItems = useCallback(async (query: string) => {
        const suggestions = await KladrManager.getSuggestions(query, null)

        return Promise.resolve(suggestions.map(suggestion => new ListItem(suggestion.data.kladr_id, suggestion.value, suggestion)))
    }, [])

    const getAddressError = useCallback(
        (address: IDadataSuggestion | null): string | null => {
            if (!isShouldShowFormError) {
                return null
            }
            if (!address) {
                return HINTS.emptyAddressError
            }
            return address?.data?.house?.length ? null : HINTS.emptyBuildingAddressError
        },
        [isShouldShowFormError]
    )

    return (
        <form ref={dealerForm} className={`${styles.form} ${isMobile ? styles.mobile : ''}`}>
            <fieldset>
                <legend className={styles.title}>
                    Контрагент <Hint className={styles.hint} text={HINTS.confirmAccountTitle} />
                </legend>
                <Input
                    className={styles.input}
                    type="text"
                    placeholder="ИНН"
                    isRequired={true}
                    isAnnoyed={isShouldShowFormError}
                    maxlength={12}
                    pattern={'\\d{10}(\\d{2})*'}
                    value={inn}
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    dataTest="delaer-inn"
                    onChange={onInnChangeHandler}
                />
                <Select
                    className={styles.type}
                    placeholder="Тип контрагента"
                    items={dealerTypeList}
                    selectedItem={dealerInfo ? new ListItem(dealerInfo.dealerType, getDealerTypeName(dealerInfo.dealerType), null) : undefined}
                    isDisabled={true}
                    dataTest="dealer-type"
                />
                <Input
                    className={styles.input}
                    type="text"
                    isRequired={true}
                    pattern=".{5,}"
                    placeholder={isPersonDealerType ? 'ФИO' : 'Наименование'}
                    isDisabled={isPersonDealerType ? !isInnChecked : true}
                    value={dealerName}
                    isAnnoyed={isShouldShowFormError}
                    dataTest="dealer-name"
                    onChange={value => setDealerName(value)}
                />
                {isInnChecked && !isPersonDealerType && (
                    <>
                        <AutocompleteInput<IDadataSuggestion>
                            className={styles.input}
                            defaultValue={legalAddress?.value}
                            shouldStretchItems={true}
                            isMultiline={true}
                            placeholder="Юридический адрес"
                            error={getAddressError(legalAddress)}
                            onChange={value => value.length === 0 && setLegalAddress(null)}
                            onSelect={item => {
                                setLegalAddress(item.data)
                            }}
                            getAutocompleteItemsFunc={getAutocompleteItems}
                        />
                        <AutocompleteInput<IDadataSuggestion>
                            className={styles.input}
                            defaultValue={physicalAddress?.value}
                            shouldStretchItems={true}
                            isMultiline={true}
                            placeholder="Фактический адрес"
                            error={getAddressError(physicalAddress)}
                            onChange={value => value.length === 0 && setPhysicalAddress(null)}
                            onSelect={item => {
                                setPhysicalAddress(item.data)
                            }}
                            getAutocompleteItemsFunc={getAutocompleteItems}
                        />
                    </>
                )}
            </fieldset>
            <fieldset>
                <legend>Контактная информация</legend>
                <Input
                    className={styles.input}
                    type="text"
                    placeholder="ФИО Контактного лица"
                    isDisabled={!isInnChecked}
                    value={name}
                    isRequired={true}
                    pattern=".{5,}"
                    isAnnoyed={isShouldShowFormError}
                    dataTest="dealer-contact-person"
                    onChange={value => setName(value)}
                />
                <Input
                    className={styles.input}
                    type="text"
                    placeholder="Телефон Контактного лица"
                    isDisabled={!isInnChecked}
                    value={phone}
                    isRequired={true}
                    isAnnoyed={isShouldShowFormError}
                    dataTest="dealer-contacts"
                    onChange={value => setPhone(value)}
                />
            </fieldset>
            <Button
                className={`${styles.addButton} analytics-modal-window-confirm-account-button`}
                buttonStyle="with-fill"
                dataTest="save-dealer"
                isDisabled={!dealerInfo}
                onClick={onSaveHandler}>
                {dealerId ? 'Подтвердить учетную запись' : 'Добавить контрагента'}
            </Button>
        </form>
    )
}
