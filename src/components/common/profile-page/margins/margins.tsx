import * as React from 'react'
import {useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {PriceInput} from '../../price-input/price-input'
import {Margin} from '../../../../model/framer/margin'
import {HINTS} from '../../../../constants/hints'
import {PageTitle} from '../../page-title/page-title'
import {useAppContext} from '../../../../hooks/app'
import {updateGoodsMargin, updateMargin} from '../../../../redux/account/actions'
import styles from './margins.module.css'

export function Margins(): JSX.Element | null {
    const partner = useSelector((state: IAppState) => state.account.partner)

    const dispatch = useDispatch()
    const updateMarginAction = useCallback((margin: Margin) => dispatch(updateMargin(margin)), [dispatch])
    const updateGoodsMarginAction = useCallback((margin: Margin) => dispatch(updateGoodsMargin(margin)), [dispatch])
    const {isMobile} = useAppContext()

    if (!partner) {
        return null
    }

    const {margin, goodsMargin} = partner

    return (
        <div className={`${styles.margins} ${isMobile ? styles.mobile : ''}`}>
            <div className={styles.item}>
                <PageTitle title="На конструкции" hint={HINTS.constructionMargins} className={styles.title} />
                <PriceInput
                    dataTest="construction-margin"
                    value={margin.percent}
                    currency="%"
                    className={styles.price}
                    onChange={value => {
                        const margin = new Margin()
                        margin.percent = value
                        updateMarginAction(margin)
                    }}
                />
            </div>
            <div>
                <PageTitle title="На доп. материалы" hint={HINTS.goodsMargin} className={styles.title} />
                <PriceInput
                    dataTest="good-margin"
                    value={goodsMargin.percent}
                    currency="%"
                    className={styles.price}
                    onChange={value => {
                        const margin = new Margin()
                        margin.percent = value
                        updateGoodsMarginAction(margin)
                    }}
                />
            </div>
        </div>
    )
}
