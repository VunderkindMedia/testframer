import * as React from 'react'
import {useEffect, useCallback, useRef} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {Input} from '../../input/input'
import {PriceInput} from '../../price-input/price-input'
import {RemoveButton} from '../../button/remove-button/remove-button'
import {loadPartnerGoodsTemplates, savePartnerGood, deletePartnerGood} from '../../../../redux/goods/actions'
import {ListItem} from '../../../../model/list-item'
import {Good} from '../../../../model/framer/good'
import {PageTitle} from '../../page-title/page-title'
import {HINTS} from '../../../../constants/hints'
import {Select} from '../../select/select'
import {WithTitle} from '../../with-title/with-title'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import {useAppContext} from '../../../../hooks/app'
import styles from './partner-goods.module.css'

export function PartnerGoods(): JSX.Element {
    const dispatch = useDispatch()
    const goodsForm = useRef<HTMLFormElement>(null)
    const partnerGoods = useSelector((state: IAppState) => state.goods.partnerGoods)
    const partnerGoodsTemplates = useSelector((state: IAppState) => state.goods.partnerGoodsTemplates)
    const loadPartnerGoodsTemplatesAction = useCallback(() => dispatch(loadPartnerGoodsTemplates()), [dispatch])
    const {isMobile} = useAppContext()

    useEffect(() => {
        loadPartnerGoodsTemplatesAction()
    }, [loadPartnerGoodsTemplatesAction])

    const saveGood = useCallback(
        (good: Good) => {
            dispatch(savePartnerGood(good))
        },
        [dispatch]
    )

    const getSelectItems = useCallback((): ListItem<string>[] => {
        return partnerGoodsTemplates.map(({sku, name}) => new ListItem(sku, name, name))
    }, [partnerGoodsTemplates])

    const handleSelect = useCallback(
        (item: ListItem<string>) => {
            const template = partnerGoodsTemplates.find(good => good.sku === item.id)

            if (template) {
                saveGood(template)
            }
        },
        [partnerGoodsTemplates, saveGood]
    )

    const handleInputChange = useCallback(
        (good: Good) => {
            if (goodsForm.current && !goodsForm.current.checkValidity()) {
                return
            }

            saveGood(good)
        },
        [saveGood, goodsForm]
    )

    const handleRemove = useCallback(
        (id: number) => {
            dispatch(deletePartnerGood(id))
        },
        [dispatch]
    )

    return (
        <div className={`${styles.partnerGoods} ${isMobile ? styles.mobile : ''}`}>
            <PageTitle title="Справочник материалов" hint={HINTS.partnerGoodsTitle} className={styles.title} />
            {partnerGoods.length > 0 && (
                <form ref={goodsForm} onSubmit={e => e.preventDefault()}>
                    <ul className={styles.list} data-test="partner-goods-list">
                        {partnerGoods.map(({id, name, sku, unit, pricePerUnit}, index) => (
                            <li key={id} className={styles.item}>
                                <WithTitle title="Артикул" className={styles.field}>
                                    <Input
                                        value={sku}
                                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                        placeholder="Артикул"
                                        className={styles.input}
                                        dataTest="partner-goods-sku"
                                        onChange={value => {
                                            const good = partnerGoods[index].clone
                                            good.sku = value.trim()
                                            handleInputChange(good)
                                        }}
                                    />
                                </WithTitle>
                                <WithTitle title="Название" className={styles.field}>
                                    <Input
                                        value={name}
                                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                        isRequired={true}
                                        pattern=".{5,}"
                                        isAnnoyed={true}
                                        placeholder="Название"
                                        dataTest="partner-goods-title"
                                        className={styles.input}
                                        onChange={value => {
                                            const good = partnerGoods[index].clone
                                            good.name = value.trim()
                                            handleInputChange(good)
                                        }}
                                    />
                                </WithTitle>
                                <WithTitle title="Цена" className={`${styles.field} ${styles.price}`}>
                                    <PriceInput
                                        value={pricePerUnit / 100}
                                        min={0}
                                        isAnnoyed={true}
                                        currency={`₽/${unit.shortName}`}
                                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                        dataTest="partner-goods-price"
                                        onChange={(value: number) => {
                                            const good = partnerGoods[index].clone
                                            good.pricePerUnit = value * 100
                                            handleInputChange(good)
                                        }}
                                    />
                                </WithTitle>
                                <RemoveButton onClick={() => handleRemove(id)} className={styles.removeButton} />
                            </li>
                        ))}
                    </ul>
                </form>
            )}
            <Select
                title="Добавить"
                shouldShowAddButtonForEmptyFilterResult={true}
                items={getSelectItems()}
                dataTest="add-goods"
                onSelect={handleSelect}
            />
        </div>
    )
}
