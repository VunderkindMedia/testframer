import * as React from 'react'
import {Construction} from '../../../model/framer/construction'
import {Product} from '../../../model/framer/product'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {Point} from '../../../model/geometry/point'
import {Line} from '../../../model/geometry/line'
import {Impost} from '../../../model/framer/impost'
import {Svg} from '../svg/svg'
import {Size} from '../../../model/size'
import {Polygon} from '../../../model/geometry/polygon'
import {SvgPolygon} from '../svg/svg-polygon'
import {SvgLine} from '../svg/svg-line'
import styles from './svg-icon-construction-drawer.module.css'

const DEFAULT_MAX_HEIGHT = 38
const DEFAULT_MAX_WIDTH = 35
const DEFAULT_LINE_WIDTH = 2
const DEFAULT_LINE_COLOR = '#C4C4C4'

interface ISvgIconConstructionDrawerProps {
    construction: Construction
    lineWidth?: number
    maxWidth?: number
    maxHeight?: number
    lineColor?: string
}

const getInnerFillingsImposts = (frame: FrameFilling): Impost[] => {
    let imposts: Impost[] = frame.impostBeams

    frame.innerFillings.forEach(innerFilling => {
        if (innerFilling.leaf) {
            imposts = imposts.concat(getInnerFillingsImposts(innerFilling.leaf))
        }
    })

    return imposts
}

const getAllDrawableImposts = (product: Product): Impost[] => {
    let imposts: Impost[] = []

    imposts = imposts.concat(getInnerFillingsImposts(product.frame))

    return imposts
}

export function SvgIconConstructionDrawer(props: ISvgIconConstructionDrawerProps): JSX.Element | null {
    const {
        construction,
        maxWidth = DEFAULT_MAX_WIDTH,
        maxHeight = DEFAULT_MAX_HEIGHT,
        lineWidth = DEFAULT_LINE_WIDTH,
        lineColor = DEFAULT_LINE_COLOR
    } = props

    const {size, isAluminium} = construction

    const frameWidth = isAluminium ? 1 : lineWidth

    const scaleX = maxWidth / size.width
    const scaleY = maxHeight / size.height
    const scale = Math.min(scaleX, scaleY)

    if (scale <= 0) {
        return null
    }

    const width = size.width * scale
    const height = size.height * scale

    const {x: cX, y: cY} = construction

    const drawingPolygons: Polygon[] = []
    const drawingLines: Line[] = []

    construction.products.forEach(product => {
        const getNewPoint = (point: Point): Point => {
            return new Point((point.x - cX) * scale + frameWidth, (point.y - cY) * scale + frameWidth)
        }

        drawingLines.push(...getAllDrawableImposts(product).map(line => new Line(getNewPoint(line.start), getNewPoint(line.end))))

        product.connectors.forEach(connector => {
            drawingPolygons.push(Polygon.buildFromPoints(connector.innerDrawingPoints.map(point => getNewPoint(point))))
        })

        drawingPolygons.push(Polygon.buildFromPoints(product.frame.containerPolygon.points.map(p => getNewPoint(p))))
    })

    const viewboxSize = new Size(width + frameWidth * 2, height + frameWidth * 2)

    return (
        <div className={`${styles.svgIconConstructionDrawer} ${construction.haveEnergyProducts && styles.energy}`}>
            <Svg viewboxSize={viewboxSize} size={new Size(width < height ? viewboxSize.width : width, height)}>
                {drawingPolygons.map((polygon, idx) => (
                    <SvgPolygon key={`${polygon.id}-${idx}`} stroke={lineColor} lineWidth={frameWidth} fill={'transparent'} polygon={polygon} />
                ))}
                {drawingLines.map((line, idx) => (
                    <SvgLine key={`${line.id}-${idx}`} stroke={lineColor} lineWidth={frameWidth} line={line} />
                ))}
            </Svg>
        </div>
    )
}
