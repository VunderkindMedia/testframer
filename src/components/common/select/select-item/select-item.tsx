import * as React from 'react'
import styles from './select-item.module.css'
import {ListItem} from '../../../../model/list-item'
import {useState} from 'react'
import {Checkbox} from '../../checkbox/checkbox'
import {getSuggestionItem} from '../../../../helpers/text'

interface ISelectItemProps<T> {
    item: ListItem<T>
    activeItem: ListItem<T> | null
    level?: number
    isSelected?: boolean
    filter?: string
    isMultiSelect?: boolean
    isMultiline?: boolean
    onSelect?: (item: ListItem<T>) => void
    dataTest?: string
    isAllExpanded?: boolean
}

export function SelectItem<T>(props: ISelectItemProps<T>): JSX.Element | null {
    const {item, activeItem, isSelected, level, filter, isMultiSelect, isMultiline = false, dataTest, onSelect, isAllExpanded} = props

    const [isExpanded, setIsExpanded] = useState(item.alwaysExpanded)

    const currentLevel = level ?? 0

    if (filter && ![item.name, ...item.childrenNames].some(name => name.toLowerCase().includes(filter.toLowerCase()))) {
        return null
    }

    return (
        <div
            data-test={`${dataTest}-level-${currentLevel}`}
            className={`${styles.selectItem} ${isSelected && !isMultiSelect && styles.selected} level-${currentLevel} ${
                item.selectable && styles.selectable
            } ${(isExpanded || isAllExpanded) && styles.expanded}`}>
            <p
                className={`${styles.name} ${isMultiline && styles.multiline}`}
                data-test={`${dataTest}-level-${currentLevel}-title`}
                onClick={() => {
                    if (item.selectable) {
                        setIsExpanded(!isExpanded)
                        onSelect && onSelect(item)
                    }
                }}
                style={{
                    paddingLeft: `${(currentLevel + 1) * 12}px`
                }}>
                {isMultiSelect && <Checkbox className={styles.checkbox} isChecked={isSelected} />}
                {item.listItems.length === 0 && filter ? (
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    <span dangerouslySetInnerHTML={{__html: getSuggestionItem(item.nameWithAdditionalString, filter, styles.query)}} />
                ) : (
                    item.nameWithAdditionalString
                )}
            </p>
            {item.listItems.length > 0 && (
                <div className={styles.items}>
                    {item.listItems.map(item => (
                        <SelectItem
                            key={item.id}
                            dataTest={dataTest}
                            item={item}
                            activeItem={activeItem}
                            isAllExpanded={isAllExpanded}
                            level={currentLevel + 1}
                            filter={filter}
                            isMultiline={isMultiline}
                            onSelect={onSelect}
                        />
                    ))}
                </div>
            )}
        </div>
    )
}
