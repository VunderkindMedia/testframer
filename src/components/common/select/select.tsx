import * as React from 'react'
import {CSSProperties, useCallback, useEffect, useRef, useState} from 'react'
import {ListItem} from '../../../model/list-item'
import {SelectItem} from './select-item/select-item'
import {SearchInput} from '../search-input/search-input'
import {BodyPortal} from '../body-portal/body-portal'
import {getClassNames} from '../../../helpers/css'
import {useDocumentClick, useModalOpen} from '../../../hooks/ui'
import {isPhone} from '../../../helpers/mobile'
import Timeout = NodeJS.Timeout
import styles from './select.module.css'

const FILTER_HEIGHT = 44
const HEADER_HEIGHT = 44
const ITEM_HEIGHT_PX = 32
const MAX_VISIBLE_ITEMS = 8
const VERTICAL_PADDING_PX = 6
const DEFAULT_BODY_HORIZONTAL_PADDING_PX = 12
const BORDER_WIDTH_PX = 1
const BODY_OFFSET_PX = 4
const MAX_BODY_HEIGHT_PX = ITEM_HEIGHT_PX * MAX_VISIBLE_ITEMS + 2 * (VERTICAL_PADDING_PX + BORDER_WIDTH_PX)

function getBodyOptimalStyle<T>(
    selectRect: DOMRect,
    items: ListItem<T>[],
    shouldShowFilter: boolean,
    useMaxWidth: boolean,
    emptyListElement?: JSX.Element,
    emptyListElementHeight?: number
): CSSProperties {
    const itemsAvailableHeight =
        (items.reduce((count, item) => count + item.children.length, 0) + items.length) * ITEM_HEIGHT_PX +
        2 * (VERTICAL_PADDING_PX + BORDER_WIDTH_PX) +
        (shouldShowFilter ? FILTER_HEIGHT : 0)

    const emptyListAvailableHeight =
        2 * (VERTICAL_PADDING_PX + BORDER_WIDTH_PX) +
        (shouldShowFilter ? FILTER_HEIGHT : 0) +
        (emptyListElement && emptyListElementHeight ? emptyListElementHeight : 0)

    const availableHeight = Math.min(MAX_BODY_HEIGHT_PX, Math.max(itemsAvailableHeight, emptyListAvailableHeight))
    const expectedEndY = selectRect.y + selectRect.height + BODY_OFFSET_PX + availableHeight

    const style: CSSProperties = {
        minWidth: `${selectRect.width}px`,
        width: `${selectRect.width}px`,
        maxHeight: `${availableHeight}px`,
        left: `${selectRect.x}px`
    }

    if (expectedEndY <= window.innerHeight) {
        style.top = selectRect.y + selectRect.height + BODY_OFFSET_PX
    } else {
        style.bottom = window.innerHeight - selectRect.y + BODY_OFFSET_PX
        const topOffset = BODY_OFFSET_PX + (isPhone() ? HEADER_HEIGHT : 0)
        if (style.bottom + availableHeight + topOffset > window.innerHeight) {
            style.maxHeight = window.innerHeight - style.bottom - topOffset
        }
    }

    if (useMaxWidth) {
        style.width = 'auto'
        style.maxWidth = `${window.innerWidth - selectRect.right + selectRect.width - DEFAULT_BODY_HORIZONTAL_PADDING_PX}px`
    }

    return style
}

interface ISelectProps<T> {
    items: ListItem<T>[]
    title?: string
    placeholder?: string
    selectedItem?: ListItem<T | null>
    selectedItems?: ListItem<T>[]
    shouldShowFilter?: boolean
    filterPlaceholder?: string
    onSelect?: (item: ListItem<T>) => void
    onMultiSelect?: (items: ListItem<T>[]) => void
    className?: string
    isDisabled?: boolean
    hasError?: boolean
    isValid?: boolean
    isMultiSelect?: boolean
    useMaxWidth?: boolean
    shouldShowAddButtonForEmptyFilterResult?: boolean
    debounceTimeMs?: number
    dataTest?: string
    emptyListElement?: JSX.Element
    emptyListElementHeight?: number
    isMultiline?: boolean
    onChangeFilter?: (filter: string) => void
    onClose?: () => void
}

export function Select<T>(props: ISelectProps<T>): JSX.Element {
    const {
        items,
        title,
        placeholder,
        selectedItem,
        selectedItems,
        onSelect,
        onMultiSelect,
        shouldShowFilter = false,
        filterPlaceholder,
        className,
        isDisabled = false,
        hasError = false,
        isValid = false,
        isMultiSelect = false,
        useMaxWidth = false,
        shouldShowAddButtonForEmptyFilterResult = false,
        dataTest,
        debounceTimeMs,
        emptyListElement,
        emptyListElementHeight,
        isMultiline = false,
        onChangeFilter,
        onClose
    } = props

    const [isOpened, setIsOpened] = useState(false)
    const [activeItem, setActiveItem] = useState<ListItem<T> | null>(null)
    const [filter, setFilter] = useState('')
    const [bodyStyle, setBodyStyle] = useState<CSSProperties>({})
    const [selectedItemList, setSelectedItemList] = useState<ListItem<T>[]>([])

    const selectRef = useRef<HTMLDivElement>(null)
    const selectBodyRef = useRef<HTMLDivElement>(null)
    const timerRef = useRef<Timeout | null>(null)

    const showBody = useCallback(
        (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
            if (!selectRef.current || (items.length === 0 && !emptyListElement)) {
                return
            }

            e.stopPropagation()

            const selectRect = selectRef.current.getBoundingClientRect()
            const bodyOptimalStyle = getBodyOptimalStyle(
                selectRect,
                items,
                shouldShowFilter,
                useMaxWidth,
                emptyListElement,
                emptyListElementHeight
            )

            setBodyStyle(bodyOptimalStyle)
            setIsOpened(true)
        },
        [items, shouldShowFilter, useMaxWidth, emptyListElement, emptyListElementHeight]
    )

    const selectAction = useCallback(
        (item: ListItem<T>) => {
            setActiveItem(item)
            if (item.listItems.length === 0) {
                let newSelectedList: ListItem<T>[] = []
                if (isMultiSelect) {
                    const index = selectedItemList.findIndex(i => i.data === item.data)
                    if (Number(item.data) === -1) {
                        newSelectedList = [item]
                    } else {
                        newSelectedList =
                            index !== -1
                                ? [...selectedItemList.slice(0, index), ...selectedItemList.slice(index + 1)]
                                : [...selectedItemList, item]
                        newSelectedList = newSelectedList.filter(item => Number(item.data) !== -1)
                    }

                    setSelectedItemList(newSelectedList)
                }

                timerRef.current && clearTimeout(timerRef.current)

                timerRef.current = setTimeout(() => {
                    setIsOpened(false)
                    setFilter('')
                    if (isMultiSelect) {
                        onMultiSelect && onMultiSelect(newSelectedList)
                    } else {
                        onSelect && onSelect(item)
                    }
                }, debounceTimeMs ?? 0)
            }
        },
        [isMultiSelect, debounceTimeMs, selectedItemList, onMultiSelect, onSelect]
    )

    const onClickHandler = useCallback(
        (e: MouseEvent | React.MouseEvent<HTMLDivElement, MouseEvent>) => {
            if (!isOpened) {
                return
            }

            if (!selectBodyRef.current?.contains(e.target as HTMLElement)) {
                setIsOpened(false)
                setFilter('')
                onClose && onClose()
            }

            e.stopPropagation()
        },
        [isOpened, onClose]
    )

    useModalOpen(isOpened)
    useDocumentClick(onClickHandler)

    useEffect(() => {
        return () => {
            timerRef.current && clearTimeout(timerRef.current)
        }
    }, [])

    useEffect(() => {
        selectedItems && selectedItems.length > 0 && setSelectedItemList(selectedItems)
    }, [selectedItems])

    useEffect(() => {
        if (!selectRef.current || (items.length === 0 && !emptyListElement)) {
            return
        }

        const selectRect = selectRef.current.getBoundingClientRect()
        const bodyOptimalStyle = getBodyOptimalStyle(selectRect, items, shouldShowFilter, useMaxWidth, emptyListElement, emptyListElementHeight)

        setBodyStyle(bodyOptimalStyle)
    }, [items, shouldShowFilter, useMaxWidth, emptyListElement, emptyListElementHeight])

    const getSelectItems = useCallback(
        (isMultilineItem: boolean, isAllExpanded: boolean) => {
            const filteredItems: JSX.Element[] = []
            items.forEach((item, idx) => {
                if (filter && ![item.name, ...item.childrenNames].some(name => name.toLowerCase().includes(filter.toLowerCase()))) {
                    return
                }

                const isSelected =
                    isMultiSelect && selectedItemList ? selectedItemList.findIndex(s => item.id === s.id) !== -1 : item.id === selectedItem?.id

                filteredItems.push(
                    <SelectItem
                        key={item.id}
                        item={item}
                        isMultiline={isMultilineItem || isMultiline}
                        isAllExpanded={isAllExpanded}
                        activeItem={activeItem}
                        isSelected={isSelected}
                        isMultiSelect={isMultiSelect}
                        filter={filter}
                        dataTest={dataTest ? `${dataTest}-item-${idx}` : ''}
                        onSelect={selectAction}
                    />
                )
            })

            if (filteredItems.length > 0) {
                return filteredItems
            }

            return emptyListElement || null
        },
        [selectedItemList, emptyListElement, activeItem, filter, items, selectAction, selectedItem?.id, isMultiSelect, isMultiline, dataTest]
    )

    const bodyClassName = (className || '')
        .split(' ')
        .filter(c => c.length > 0)
        .map(c => `${c}__select__body`)
        .join(' ')

    const selectedItemName = isMultiSelect && selectedItemList ? selectedItemList.map(s => s.name).join(', ') : selectedItem?.name ?? ''

    const isAllExpanded = filter.length > 0

    const handleChangeFilter = useCallback(
        (value: string) => {
            setFilter(value)
            onChangeFilter && onChangeFilter(value)
        },
        [setFilter, onChangeFilter]
    )

    return (
        <div
            ref={selectRef}
            className={getClassNames(`${styles.select} ${className ?? ''}`, {
                [styles.opened]: isOpened,
                [styles.disabled]: isDisabled,
                [styles.invalid]: hasError,
                [styles.valid]: isValid,
                [styles.openable]: items.length > 0 || !!emptyListElement,
                [styles.add]: shouldShowAddButtonForEmptyFilterResult
            })}
            tabIndex={0}>
            <div className={styles.header} data-test={dataTest} onClick={showBody}>
                <p className={styles.title}>{title ? title : selectedItemName}</p>
                {placeholder && !title && !selectedItemName && <p className={`${styles.title} ${styles.placeholder}`}>{placeholder}</p>}
                <div className={styles.icon} />
            </div>
            {isOpened && (
                <BodyPortal>
                    <div className={styles.wrap} onClick={onClickHandler}>
                        <div
                            className={`${styles.body} ${bodyClassName}`}
                            style={bodyStyle}
                            ref={selectBodyRef}
                            data-test={dataTest ? `${dataTest}-body` : ''}>
                            {shouldShowFilter && (
                                <div className={styles.filter}>
                                    <SearchInput
                                        value={filter}
                                        placeholder={filterPlaceholder}
                                        onChange={handleChangeFilter}
                                        onClear={() => handleChangeFilter('')}
                                    />
                                </div>
                            )}
                            <div className={styles.items}>{getSelectItems(false, isAllExpanded)}</div>
                        </div>
                    </div>
                </BodyPortal>
            )}
            <div className={`${styles.body} ${styles.hidden}`} style={{maxHeight: `${MAX_BODY_HEIGHT_PX}px`}}>
                {shouldShowFilter && (
                    <div className={styles.filter}>
                        <SearchInput
                            value={filter}
                            placeholder={filterPlaceholder}
                            onChange={handleChangeFilter}
                            onClear={() => handleChangeFilter('')}
                        />
                    </div>
                )}
                <div className={styles.items}>{getSelectItems(true, isAllExpanded)}</div>
            </div>
        </div>
    )
}
