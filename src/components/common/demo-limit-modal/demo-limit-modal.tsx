import * as React from 'react'
import {InfoModal} from '../modal/info-modal/info-modal'
import styles from './demo-limit-modal.module.css'

interface IDemoLimitModalProps {
    onAction: () => void
    onClose: () => void
}

export const DemoLimitModal = (props: IDemoLimitModalProps): JSX.Element => {
    return (
        <InfoModal actionTitle="Зарегистрироваться" {...props}>
            <>
                <p className={styles.description}>Для демо-аккаунта доступен расчет только 3-х заказов.</p>
                <p className={styles.message}>
                    Зарегистрируйтесь, чтобы воспользоваться полным функционалом сервиса (в т.ч. считать с учетом скидок) и передавать заказы в
                    производство
                </p>
            </>
        </InfoModal>
    )
}
