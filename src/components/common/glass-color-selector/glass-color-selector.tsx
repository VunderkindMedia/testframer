import * as React from 'react'
import {SolidFilling} from '../../../model/framer/solid-filling'
import {GLASS_COLORS} from '../../../constants/glass-colors'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useSetGlassColor} from '../../../hooks/builder'
import {getClassNames} from '../../../helpers/css'
import {useAppContext} from '../../../hooks/app'
import styles from './glass-color-selector.module.css'
import {ConfigContexts} from '../../../model/config-contexts'

export function GlassColorSelector(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => {
        return state.builder.configContext === ConfigContexts.ConstructionParams ? state.orders.genericProduct : state.orders.currentProduct
    })
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)
    const context =
        currentProduct?.isEmpty ||
        (currentProduct && configContext === ConfigContexts.ConstructionParams && currentProduct.frame.innerFillings.length)
            ? currentProduct.frame.innerFillings[0].solid ||
              (currentProduct.frame.innerFillings[0].leaf && currentProduct.frame.innerFillings[0].leaf.innerFillings[0].solid)
            : currentContext

    const setGlassColor = useSetGlassColor(currentOrder, currentConstruction, configContext, context ?? null)
    const {isMobile} = useAppContext()

    if (!(context instanceof SolidFilling)) {
        return null
    }

    return (
        <div className={`${styles.selector} ${isMobile ? styles.mobile : ''}`}>
            {GLASS_COLORS.map((color, idx) => (
                <div
                    key={idx}
                    onClick={() => setGlassColor(color)}
                    className={getClassNames(`${styles.item} blue-hover`, {selected: color === context.glassColor})}>
                    <p>{color}</p>
                </div>
            ))}
        </div>
    )
}
