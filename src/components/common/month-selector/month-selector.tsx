import * as React from 'react'
import styles from './month-selector.module.css'
import {getDateMMYYYYFormat} from '../../../helpers/date'
import {MonthPicker} from '../month-picker/month-picker'
import {useCallback, useRef, useState} from 'react'
import {useDocumentClick} from '../../../hooks/ui'

interface IMonthSelectorProps {
    date: Date
    onMonthChange: (date: Date) => void
    className?: string
}

export function MonthSelector(props: IMonthSelectorProps): JSX.Element {
    const {date, onMonthChange, className} = props
    const [isPickerVisible, setIsPickerVisible] = useState(false)

    const containerRef = useRef<HTMLDivElement | null>(null)
    const pickerRef = useRef<HTMLDivElement | null>(null)

    const onMonthChangeCallback = useCallback(
        (date: Date) => {
            setIsPickerVisible(false)
            onMonthChange(date)
        },
        [onMonthChange]
    )

    const closePicker = useCallback((e: MouseEvent) => {
        if (containerRef.current?.contains(e.target as Node)) {
            if (!pickerRef.current?.contains(e.target as Node)) {
                setIsPickerVisible(true)
            }
        } else {
            setIsPickerVisible(false)
        }
    }, [])

    useDocumentClick(closePicker)

    return (
        <div ref={containerRef} className={`${styles.monthSelector} ${className} ${isPickerVisible && styles.pickerVisible}`}>
            <p className={styles.date}>{getDateMMYYYYFormat(date)}</p>
            <MonthPicker ref={pickerRef} onSelect={onMonthChangeCallback} date={date} className={styles.picker} />
            <button className={styles.arrow} />
        </div>
    )
}
