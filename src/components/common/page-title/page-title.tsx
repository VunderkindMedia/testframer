import * as React from 'react'
import {Hint} from '../hint/hint'
import styles from './page-title.module.css'

interface IPageTitle {
    title: string
    hint?: string
    className?: string
}

export function PageTitle(props: IPageTitle): JSX.Element {
    const {title, hint, className} = props

    return (
        <div className={`${styles.title} ${className ?? ''}`}>
            <p className={styles.text}>{title}</p>
            {hint && <Hint className={styles.hint} text={hint} />}
        </div>
    )
}
