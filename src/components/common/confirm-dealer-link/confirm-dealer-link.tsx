import * as React from 'react'
import {useState} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {ModalDealers} from '../profile-page/modal-dealers/modal-dealers'
import {AnalyticsManager} from '../../../analytics-manager'
import styles from './confirm-dealer-link.module.css'

interface IConfirmDealerLinkProps {
    title: string
    className?: string
}

export const ConfirmDealerLink = (props: IConfirmDealerLinkProps): JSX.Element | null => {
    const {title, className} = props
    const [isModalVisible, setIsModalVisible] = useState(false)
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)

    if (!currentDealer) {
        return null
    }

    return (
        <>
            <p className={`${styles.error} ${className ?? ''} analytics-profile-page-confirm-account-button`}>
                {title}{' '}
                <a
                    href=""
                    className={styles.link}
                    onClick={e => {
                        e.preventDefault()
                        setIsModalVisible(true)
                        AnalyticsManager.trackConfirmAccountClick()
                    }}>
                    подтвердить учетную запись
                </a>
            </p>
            {isModalVisible && <ModalDealers onClose={() => setIsModalVisible(false)} dealerId={currentDealer.id} />}
        </>
    )
}
