import * as React from 'react'
import styles from './with-title.module.css'
import {CSSProperties, ReactNode} from 'react'
import {ArrowTooltip} from '../arrow-tooltip/arrow-tooltip'

interface IWithTitleProps {
    title: string
    children?: ReactNode
    className?: string
    titleStyle?: CSSProperties
    hint?: string
}

const WithTitle = React.forwardRef<HTMLDivElement, IWithTitleProps>((props: IWithTitleProps, ref) => {
    const {title, hint, children, className, titleStyle} = props

    const titleElement = (
        <p className={styles.title} style={titleStyle}>
            {title}
        </p>
    )

    return (
        <div ref={ref} className={`${className ?? ''}`}>
            {hint ? <ArrowTooltip title={hint}>{titleElement}</ArrowTooltip> : titleElement}
            {React.Children.map(children, child => (
                <>{child}</>
            ))}
        </div>
    )
})

WithTitle.displayName = 'WithTitle'

export {WithTitle}
