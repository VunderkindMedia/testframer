import * as React from 'react'
import styles from './segmented-control.module.css'
import {useCallback, useState} from 'react'
import {useEffect} from 'react'

interface ISegmentedControlProps {
    items: string[]
    selectedIndex?: number
    className?: string
    onSelect?: (index: number) => void
}

export function SegmentedControl(props: ISegmentedControlProps): JSX.Element {
    const {items, selectedIndex, className, onSelect} = props

    const [selected, setSelected] = useState<number>(selectedIndex ?? 0)

    useEffect(() => {
        if (selectedIndex !== undefined) {
            setSelected(selectedIndex)
        }
    }, [selectedIndex])

    const select = useCallback(
        (index: number): void => {
            setSelected(index)
            onSelect && onSelect(index)
        },
        [onSelect]
    )

    return (
        <div className={`${styles.segmentedControl} ${className ?? ''}`}>
            {items.map((item, idx) => (
                <div
                    data-test={`segmented-control-item-${idx}`}
                    key={idx}
                    onClick={() => select(idx)}
                    className={`${styles.item} ${selected === idx && styles.selected}`}>
                    <p>{item}</p>
                </div>
            ))}
        </div>
    )
}
