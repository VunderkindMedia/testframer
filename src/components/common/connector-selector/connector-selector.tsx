import * as React from 'react'
import {Connector} from '../../../model/framer/connector'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {ConnectorParameters} from '../../../model/framer/connector-parameters'
import {ConnectorTypes} from '../../../model/framer/connector-types'
import {Beam} from '../../../model/framer/beam'
import {Sides} from '../../../model/sides'
import {useCallback, useEffect} from 'react'
import {useSetConnectingSide, useSetHostProduct} from '../../../hooks/template-selector'
import {useSetConnectorParams} from '../../../hooks/builder'
import {getClassNames} from '../../../helpers/css'
import {useAppContext} from '../../../hooks/app'
import {UserParametersFragment} from '../builder-page-fragments/user-parameters-fragment/user-parameters-fragment'
import styles from './connector-selector.module.css'

export function ConnectorSelector(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)
    const connectorsParameters = useSelector((state: IAppState) => state.orders.currentConstruction?.connectorsParameters ?? [])
    const connectingSide = useSelector((state: IAppState) => state.templateSelector.connectingSide)
    const hostProduct = useSelector((state: IAppState) => state.templateSelector.hostProduct)

    const setConnectingSide = useSetConnectingSide()
    const setHostProduct = useSetHostProduct()
    const setConnectorParams = useSetConnectorParams(currentOrder, currentConstruction, currentContext, connectingSide, hostProduct)
    const {isMobile} = useAppContext()

    useEffect(() => {
        if (!configContext) {
            setConnectingSide(null)
            setHostProduct(null)
        }
    }, [configContext, setConnectingSide, setHostProduct])

    const select = useCallback((params: ConnectorParameters): void => setConnectorParams(params), [setConnectorParams])

    const connectorParam = connectorsParameters.find(c => currentContext instanceof Connector && c.marking === currentContext.profileVendorCode)

    const currentProfileVendorCode = connectorParam?.marking ?? ''

    let connectedBeam: Beam | null = null
    if (hostProduct && connectingSide) {
        if (connectingSide === Sides.Right) {
            connectedBeam = hostProduct.rightmostConnectedBeam
        } else if (connectingSide === Sides.Left) {
            connectedBeam = hostProduct.leftmostConnectedBeam
        } else if (connectingSide === Sides.Top) {
            connectedBeam = hostProduct.topmostConnectedBeam
        } else if (connectingSide === Sides.Bottom) {
            connectedBeam = hostProduct.bottommostConnectedBeam
        }
    }

    const availableConnectorTypesSet = new Set<ConnectorTypes>()

    if (connectorParam?.connectorType) {
        availableConnectorTypesSet.add(connectorParam.connectorType)
    } else {
        if (connectedBeam instanceof Connector) {
            if (connectedBeam.connectorType === ConnectorTypes.Extender) {
                availableConnectorTypesSet.add(ConnectorTypes.Extender)
                availableConnectorTypesSet.add(ConnectorTypes.Connector)
            }
        } else {
            availableConnectorTypesSet.add(ConnectorTypes.Extender)
            availableConnectorTypesSet.add(ConnectorTypes.Connector)
        }
    }

    const availableConnectorsParameters = connectorsParameters.filter(p => availableConnectorTypesSet.has(p.connectorType))

    return (
        <div className={`${styles.items} ${isMobile ? styles.mobile : ''}`}>
            {availableConnectorsParameters.map(params => (
                <div
                    key={params.id}
                    onClick={() => select(params)}
                    className={getClassNames(`${styles.item} blue-hover`, {selected: currentProfileVendorCode === params.marking})}>
                    <p className={styles.itemTitle}>{params.marking}</p>
                    <p className={styles.itemDescription}>{params.comment}</p>
                </div>
            ))}
            <UserParametersFragment className={styles.params} />
        </div>
    )
}
