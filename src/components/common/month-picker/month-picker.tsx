import * as React from 'react'
import {useState} from 'react'
import styles from './month-picker.module.css'
import {getShortMonthList} from '../../../helpers/date'
import {getClassNames} from '../../../helpers/css'

interface IMonthPickerProps {
    date: Date
    className?: string
    onSelect: (date: Date) => void
}

const MonthPicker = React.forwardRef<HTMLDivElement, IMonthPickerProps>((props: IMonthPickerProps, ref) => {
    const {date, className, onSelect} = props

    const [activeYear, setActiveYear] = useState(date.getFullYear())
    const monthList = getShortMonthList()

    return (
        <div className={`${className} ${styles.picker}`} ref={ref}>
            <p className={styles.year}>{activeYear}</p>
            <p className={styles.arrows}>
                <button
                    className={`${styles.arrowButton} ${styles.next}`}
                    onClick={() => {
                        setActiveYear(activeYear + 1)
                    }}
                />
                <button className={`${styles.arrowButton} ${styles.prev}`} onClick={() => setActiveYear(activeYear - 1)} />
            </p>
            {monthList.map((m, index) => (
                <button
                    className={getClassNames(styles.month, {[styles.active]: date.getFullYear() === activeYear && index === date.getMonth()})}
                    key={index}
                    onClick={() => onSelect(new Date(activeYear, index))}>
                    {m}
                </button>
            ))}
        </div>
    )
})

MonthPicker.displayName = 'MonthPicker'

export {MonthPicker}
