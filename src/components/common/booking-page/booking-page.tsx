import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useOrder} from '../../../hooks/orders'
import {OrderStates} from '../../../model/framer/order-states'
import {BookingPageDateStage} from './booking-page-date-stage/booking-page-date-stage'
import {BookingPageAddressStage} from './booking-page-address-stage/booking-page-address-stage'
import {useBookingOptions} from '../../../hooks/booking'
import {Link} from 'react-router-dom'
import {getOrderLink} from '../../../helpers/orders'
import {useEffect} from 'react'
import {AnalyticsManager} from '../../../analytics-manager'
import {useAppContext} from '../../../hooks/app'
import styles from './booking-page.module.css'

export function BookingPage(): JSX.Element | null {
    const search = useSelector((state: IAppState) => state.router.location.search)

    const pageParams = new URLSearchParams(search)
    const orderIdStr = pageParams.get('orderId')
    const orderId = orderIdStr ? +orderIdStr : -1

    const order = useOrder(orderId)
    const bookingOptions = useBookingOptions(orderId)
    const {isMobile} = useAppContext()

    useEffect(() => {
        AnalyticsManager.trackEvent({
            name: 'Перешли на страницу "Передача в производство"',
            target: 'booking-page'
        })
    }, [])

    if (!order || !bookingOptions) {
        return null
    }

    return (
        <div className={`${styles.bookingPage} ${isMobile ? styles.mobile : ''}`}>
            <p className={styles.orderNumber}>
                Заказ № <Link to={getOrderLink(order)}>{order.fullOrderNumber}</Link>
            </p>
            {order.state === OrderStates.Paid ? (
                <BookingPageDateStage order={order} />
            ) : (
                <BookingPageAddressStage order={order} bookingOptions={bookingOptions} />
            )}
        </div>
    )
}
