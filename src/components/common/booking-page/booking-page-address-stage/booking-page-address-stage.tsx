import * as React from 'react'
import {useEffect, useState, useCallback} from 'react'
import {Order} from '../../../../model/framer/order'
import {ShippingTypes} from '../../../../model/framer/shipping-types'
import {SegmentedControl} from '../../segmented-control/segmented-control'
import {BookingOptions} from '../../../../model/framer/booking-options'
import {RadioGroup, RadioGroupStyles} from '../../radio-group/radio-group'
import {Warehouse} from '../../../../model/framer/warehouse'
import {WithTitle} from '../../with-title/with-title'
import {AutocompleteInput} from '../../autocomplete-input/autocomplete-input'
import {Address} from '../../../../model/address'
import {IDadataSuggestion, KladrManager} from '../../../../kladr-manager'
import {ListItem} from '../../../../model/list-item'
import {MapPlacemark, Ymaps} from '../../ymaps/ymaps'
import {useShippingResponse, useTransferToProductionAction} from '../../../../hooks/booking'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {Button} from '../../button/button'
import {NAV_PAYMENT} from '../../../../constants/navigation'
import {ModalModel} from '../../modal/modal-model'
import {SvgIconConstructionDrawer} from '../../svg-icon-construction-drawer/svg-icon-construction-drawer'
import {GoodsIcon} from '../../goods-icon/goods-icon'
import {OrderStates} from '../../../../model/framer/order-states'
import {Expandable} from '../../expandable/expandable'
import {OrderErrors} from '../../order-errors/order-errors'
import {Action} from '../../../../model/action'
import {ContactInfo} from '../../../../model/framer/contact-info'
import {RUB_SYMBOL} from '../../../../constants/strings'
import {Modal} from '../../modal/modal'
import {CustomInput} from '../../input/custom-input/custom-input'
import {Textarea} from '../../textarea/textarea'
import {useGoTo} from '../../../../hooks/router'
import {useAppContext} from '../../../../hooks/app'
import {WithHint} from '../../with-hint/with-hint'
import {HINTS} from '../../../../constants/hints'
import {GoodSources} from '../../../../model/framer/good-sources'
import {getCurrency, getBonusScore} from '../../../../helpers/currency'
import {CarrotQuestManager} from '../../../../carrot-quest-manager'
import {FactoryTypes} from '../../../../model/framer/factory-types'
import {useIsSubdealerOrder} from '../../../../hooks/orders'
import {ConstructionPreview} from '../../construction-preview/construction-preview'
import {HeaderTitle} from '../../../mobile/header/header-title'
import {PhoneInput} from '../../input/phone-input/phone-input'
import {Hint} from '../../hint/hint'
import styles from './booking-page-address-stage.module.css'
import {usePartnerAccess} from '../../../../hooks/permissions'
import {Checkbox} from '../../checkbox/checkbox'
import {QuantityEditor} from '../../quantity-editor/quantity-editor'

const MOSCOW_KLADR_ID = '77'
const MOSCOW_OBLAST_KLADR_ID = '50'

interface IBookingPageAddressStageProps {
    order: Order
    bookingOptions: BookingOptions
}

export function BookingPageAddressStage(props: IBookingPageAddressStageProps): JSX.Element {
    const {order, bookingOptions} = props

    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)
    const accountBalance = useSelector((state: IAppState) => state.account.balance)
    const framerPoints = useSelector((state: IAppState) => state.account.framerPoints)
    const accountLimit = useSelector((state: IAppState) => state.account.creditLimitInCents)
    const framerCreditLimit = useSelector((state: IAppState) => state.account.framerCreditLimitInCents)
    const isCashMode = useSelector((state: IAppState) => state.account.isCashMode)

    const [shippingType, setShippingType] = useState(order.shippingType)
    const [selectedWarehouse, setSelectedWarehouse] = useState<Warehouse>(bookingOptions.warehouses[0])

    const [contactName, setContactName] = useState('')
    const [contactPhone, setContactPhone] = useState('')
    const [contactNameError, setContactNameError] = useState<string | null>(null)
    const [addressError, setAddressError] = useState<string | null>(null)
    const [contactPhoneError, setContactPhoneError] = useState<string | null>(null)
    const [bonusError, setBonusError] = useState<string | null>(null)
    const [address, setAddress] = useState(Address.empty)
    const [comment, setComment] = useState('')
    const [bonusValue, setBonusValue] = useState(0)
    const [isOnFloor, setOnFloor] = useState(false)
    const [shippingFloor, setShippingFloor] = useState<number>(1)
    const [oversizedProducts, setOversizedProducts] = useState<number>(0)
    const shippingResponse = useShippingResponse(order, bookingOptions, address, shippingFloor, oversizedProducts)

    const transferToProduction = useTransferToProductionAction()
    const goTo = useGoTo()
    const [modalModel, setModalModel] = useState<ModalModel | null>(null)
    const getIsSubdealerOrder = useIsSubdealerOrder()
    const isSubdealerOrder = getIsSubdealerOrder(order)
    const {isMobile} = useAppContext()
    const {isPermPartner} = usePartnerAccess()

    const shippingCost =
        shippingType === ShippingTypes.Delivery && shippingResponse && shippingResponse.shippingAvailable
            ? shippingResponse.cost + shippingResponse.shippingToFloor
            : 0

    const orderCost = (isSubdealerOrder ? order.totalAmegaCost : order.totalAmegaCostToDisplay) + shippingCost

    const getBonusValue = useCallback(() => {
        if (!isPermPartner) {
            return 0
        }
        if (bonusValue > bookingOptions.availableBonusScore) {
            return 0
        }
        return bonusValue * 100
    }, [bonusValue, isPermPartner, bookingOptions.availableBonusScore])

    const checkBonusValue = getBonusValue()

    const balance = isCashMode ? framerPoints + framerCreditLimit - orderCost : accountBalance + accountLimit + checkBonusValue - orderCost

    const isOrderButtonDisabled = useCallback(() => {
        const isValidForm = contactNameError === '' && contactPhoneError === '' && addressError === '' && !bonusError
        if (shippingType === ShippingTypes.Delivery) {
            return !shippingResponse || !isValidForm || !shippingResponse.shippingAvailable
        }
        return bonusError
    }, [shippingType, contactNameError, contactPhoneError, addressError, bonusError, shippingResponse])

    const isDisabledOrderButton = isOrderButtonDisabled()

    const goodsCount = order.goods.filter(g => g.goodSource === GoodSources.Factory).reduce((count, good) => count + good.quantity, 0)
    const constructionsCount = order.constructions.reduce((count, construction) => count + construction.quantity, 0)

    const addressForMap = address.isEmpty ? new Address(bookingOptions.kladrRegionName) : address

    let zoom = 1
    if (addressForMap.region.length !== 0) {
        zoom = 7
    }
    if (addressForMap.city.length !== 0) {
        zoom = 10
    }
    if (addressForMap.street.length !== 0) {
        zoom = 14
    }
    if (addressForMap.building.length !== 0) {
        zoom = 16
    }

    useEffect(() => {
        CarrotQuestManager.track('$order_started')
    }, [])

    useEffect(() => {
        setBonusError(null)
        setBonusValue(0)
    }, [isCashMode])

    const checkNameValidation = useCallback((value: string) => {
        setContactNameError(value.trim().length >= 2 ? '' : 'Укажите имя получателя, минимум 2 символа')
    }, [])

    const checkPhoneValidation = useCallback((isValid: boolean) => {
        setContactPhoneError(isValid ? '' : 'Укажите телефон получателя в формате <nobr>8 (999) 999-99-99<nobr>')
    }, [])

    const checkAddressValidation = useCallback((address: Address) => {
        setAddressError(address.isEmpty ? HINTS.emptyAddressError : address.building.length === 0 ? HINTS.emptyBuildingAddressError : '')
    }, [])

    const getAddressBuilding = useCallback(async (address: Address) => {
        if (address.coordinates) {
            return
        }
        if (address.building.length > 0) {
            const suggestions = await KladrManager.getSuggestionsByAdress(address.toString())

            if (suggestions.length === 0) {
                return
            }
            const addressCurrent = Address.parseFromDadataAddress(suggestions[0].data)
            setAddress(addressCurrent)
        }
    }, [])

    const checkBonusValidation = useCallback(
        (value: string) => {
            if (!value) {
                return setBonusError('Укажите количество бонусов')
            }
            const bonusValue = +value
            if (isNaN(bonusValue) || bonusValue < 0) {
                return setBonusError('Количество бонусов задано некорректно')
            }
            if (bonusValue > bookingOptions.availableBonusScore) {
                return setBonusError('Недостаточно бонусов')
            }
            if (bonusValue % 10) {
                return setBonusError('Количество бонусов должно быть кратно 10')
            }
            setBonusError('')
        },
        [bookingOptions.availableBonusScore]
    )

    const onPhoneInputBlurHandler = useCallback(
        (value: string, isValid: boolean) => {
            checkPhoneValidation(isValid)
        },
        [checkPhoneValidation]
    )

    const onNameChangeHandler = useCallback((value: string) => {
        setContactName(value.trim())
    }, [])

    const onCommentChangeHandler = useCallback((value: string) => {
        setComment(value.trim())
    }, [])

    const onBonusChangeHandler = useCallback((value: string) => {
        setBonusValue(+value)
    }, [])

    const onFloorModal = useCallback(() => {
        if (addressError === '') {
            return
        }
        setModalModel(
            ModalModel.Builder.withTitle('Внимание!')
                // eslint-disable-next-line react/no-unescaped-entities
                .withBody(<p>Для того, чтобы воспользоваться услугой "Подъем на этаж", нужно сначала заполнить адрес доставки.</p>)
                .withPositiveAction(
                    new Action('Понятно', () => {
                        setModalModel(null)
                    })
                )
                .build()
        )
    }, [addressError])

    return (
        <div className={isMobile ? styles.mobile : ''}>
            <HeaderTitle>Выбор адреса</HeaderTitle>
            {modalModel && <Modal model={modalModel} />}
            <div className={styles.header}>
                {bookingOptions.isDeliveryAvailable && (
                    <div className={styles.shippingTypeSelector}>
                        <SegmentedControl
                            selectedIndex={shippingType === ShippingTypes.Pickup ? 0 : 1}
                            onSelect={index => setShippingType(index === 0 ? ShippingTypes.Pickup : ShippingTypes.Delivery)}
                            items={['Самовывоз', 'Доставка']}
                        />
                    </div>
                )}
            </div>
            <div className={styles.body}>
                <div className={`${styles.container} ${shippingType === ShippingTypes.Delivery && styles.withMap}`}>
                    {shippingType === ShippingTypes.Pickup ? (
                        <div className={styles.addressColumn}>
                            <p className={styles.addressColumnTitle}>Выберите склад отгрузки</p>
                            <RadioGroup
                                radioGroupStyle={RadioGroupStyles.Vertical}
                                items={bookingOptions.warehouses.map(warehouse => warehouse.name)}
                                selectedIndex={bookingOptions.warehouses.findIndex(warehouse => warehouse.id === selectedWarehouse.id)}
                                onSelect={index => setSelectedWarehouse(bookingOptions.warehouses[index])}
                            />
                        </div>
                    ) : (
                        <div className={styles.addressColumn}>
                            <p className={styles.addressColumnTitle}>Укажите контактное лицо и адрес</p>
                            <WithTitle title="Имя:" className={styles.withTitle}>
                                <CustomInput
                                    dataTest="booking-page-address-stage-name-input"
                                    type="text"
                                    placeholder="Иванов Иван Иванович"
                                    title="Минимальная длина имени два символа"
                                    value={contactName}
                                    error={contactNameError}
                                    onChange={onNameChangeHandler}
                                    onBlur={checkNameValidation}
                                />
                            </WithTitle>
                            <WithTitle title="Телефон:" className={styles.withTitle}>
                                <PhoneInput
                                    dataTest="booking-page-address-stage-phone-input"
                                    title="Неверный формат номера"
                                    value={contactPhone}
                                    error={contactPhoneError}
                                    onChange={(value, isValid) => {
                                        setContactPhone(value)
                                        checkPhoneValidation(isValid)
                                    }}
                                    onBlur={onPhoneInputBlurHandler}
                                />
                            </WithTitle>
                            <WithTitle title="Адрес:" className={styles.withTitle}>
                                <AutocompleteInput<IDadataSuggestion>
                                    dataTest="booking-page-address-stage-address-input"
                                    defaultValue={address.toString()}
                                    error={addressError}
                                    isMultiline={true}
                                    onChange={value => value.length === 0 && setAddress(Address.empty)}
                                    onBlur={value => {
                                        if (value.length === 0 || (value && value !== address.toString())) {
                                            checkAddressValidation(Address.empty)
                                        }
                                    }}
                                    onSelect={item => {
                                        const address = Address.parseFromDadataAddress(item.data.data)
                                        setAddress(address)
                                        void getAddressBuilding(address)
                                        checkAddressValidation(address)
                                    }}
                                    getAutocompleteItemsFunc={async query => {
                                        const suggestions = await KladrManager.getSuggestions(
                                            query,
                                            currentDealer?.factoryId !== FactoryTypes.Moscow ? bookingOptions.kladrRegionId : null
                                        )

                                        if (
                                            bookingOptions.kladrRegionId === MOSCOW_KLADR_ID ||
                                            bookingOptions.kladrRegionId === MOSCOW_OBLAST_KLADR_ID
                                        ) {
                                            const additionalSuggestions = await KladrManager.getSuggestions(
                                                query,
                                                bookingOptions.kladrRegionId === MOSCOW_KLADR_ID ? MOSCOW_OBLAST_KLADR_ID : MOSCOW_KLADR_ID
                                            )
                                            suggestions.push(...additionalSuggestions)
                                        }

                                        return Promise.resolve(
                                            suggestions.map(suggestion => new ListItem(suggestion.data.kladr_id, suggestion.value, suggestion))
                                        )
                                    }}
                                />
                            </WithTitle>
                            <WithTitle title="Комментарий к доставке:">
                                <Textarea
                                    className={styles.comment}
                                    value={comment}
                                    onChange={onCommentChangeHandler}
                                    rows={2}
                                    maxlength={250}
                                />
                            </WithTitle>
                            {currentDealer?.factoryId === FactoryTypes.Moscow && (
                                <div className={styles.floor}>
                                    <div onClick={onFloorModal}>
                                        <Checkbox
                                            className={styles.checkbox}
                                            title="Подъем на этаж"
                                            isChecked={isOnFloor}
                                            isDisabled={!(addressError === '')}
                                            onChange={checked => setOnFloor(checked)}
                                        />
                                    </div>
                                    <Hint text={HINTS.riseToTheFloor} className={styles.bonusHint} />
                                </div>
                            )}
                            {isOnFloor && (
                                <div>
                                    <div className={styles.textProductWrapper}>
                                        <div className={styles.textWithHint}>
                                            <span className={styles.textWithHintProduct}>Кол-во негабаритных изделий</span>
                                            <Hint text={HINTS.oversizedProduct} className={styles.bonusHint} />
                                        </div>
                                        <span className={styles.textWithHintFloor}>Этаж</span>
                                    </div>
                                    <div className={styles.quantyWrapper}>
                                        <div className={styles.quantityEditorWrapper}>
                                            <QuantityEditor
                                                isDisabled={order.readonly}
                                                quantity={0}
                                                min={0}
                                                max={999}
                                                onChange={quantity => {
                                                    setOversizedProducts(quantity)
                                                }}
                                            />
                                        </div>
                                        <div className={styles.quantityEditorWrapper}>
                                            <QuantityEditor
                                                isDisabled={order.readonly}
                                                quantity={1}
                                                min={1}
                                                max={999}
                                                onChange={quantity => {
                                                    setShippingFloor(quantity)
                                                }}
                                            />
                                        </div>
                                    </div>
                                    <div style={{marginBottom: '18px'}}>
                                        <p className={styles.textRiseFloor}>
                                            Стоимость подъема вручную за 1 негабаритное изделие на 1 этаж составляет 60 рублей (начиная со
                                            второго этажа). Негабаритным изделием считается изделие, которое поднимается вручную. Если лифт
                                            отсутствует, то все изделия в заказе будут считаться негабаритными. Если все изделия из заказа
                                            помещаются в лифт, то количество негабаритных изделий должно равняться нулю. За подъем изделий, вес
                                            которых превышает 110 кг, взимается разовый платеж в размере 300 рублей за изделие, вне зависимости
                                            от способа подъема и количества этажей. Если каждое изделие по габаритам помещается в лифт и меньше
                                            110 кг, то услуга «Подъем на этаж» будет бесплатной.{' '}
                                        </p>
                                    </div>
                                    <div className={styles.textWeightWrapper}>
                                        <span>Кол-во изделий вес которых превышает 110 кг:</span>
                                        <span>{bookingOptions.overweightConstructionsCount}</span>
                                    </div>
                                </div>
                            )}
                            <div className={styles.deliveryCost}>
                                {shippingResponse ? (
                                    <>
                                        <div className={styles.deliveryCostPrice}>
                                            <p>Стоимость доставки:</p>
                                            <span>
                                                {getCurrency(shippingResponse.cost + shippingResponse.shippingToFloor)} {RUB_SYMBOL}
                                            </span>
                                        </div>
                                        {shippingResponse.shippingAvailable ? (
                                            <div>
                                                {shippingResponse.daysOfWeekForFreeShipping && (
                                                    <WithHint className={styles.freeDelivery} hint={HINTS.freeDelivery}>
                                                        <>
                                                            Доставка будет бесплатной по следующим дням:&nbsp;
                                                            {shippingResponse.daysOfWeekForFreeShipping}
                                                        </>
                                                    </WithHint>
                                                )}
                                            </div>
                                        ) : (
                                            <p>Доставка до указанного адреса невозможна, слишком удалено от РЦ</p>
                                        )}
                                    </>
                                ) : (
                                    <div className={styles.deliveryCostPrice}>
                                        <p>Стоимость доставки:</p>
                                        <span>&mdash;</span>
                                    </div>
                                )}
                            </div>
                        </div>
                    )}
                    {currentDealer?.factoryId === FactoryTypes.Perm && !isCashMode && (
                        <div className={styles.bonus}>
                            <div className={styles.bonusHeader}>
                                <span>Оплатить бонусами</span>{' '}
                                <div className={styles.availableBonus}>
                                    Доступно: <span className={styles.bonusValue}>{getBonusScore(bookingOptions.availableBonusScore)} Б</span>
                                    <Hint text={HINTS.availableToReserveBonuses} className={styles.bonusHint} />
                                </div>
                            </div>
                            <div className={styles.bonusForm}>
                                <WithTitle title="Списать" className={styles.bonusTitle}>
                                    <div className={styles.bonusInputWrapper}>
                                        <CustomInput
                                            value={bonusValue.toString()}
                                            type="number"
                                            maxlength={7}
                                            min={0}
                                            max={bookingOptions.availableBonusScore}
                                            className={styles.bonusInput}
                                            error={bonusError}
                                            hideError={true}
                                            onBlur={checkBonusValidation}
                                            onChange={onBonusChangeHandler}
                                        />
                                        Б
                                    </div>
                                </WithTitle>
                                {bonusError ? (
                                    <p className={`${styles.bonusDescription} ${styles.error}`}>{bonusError}</p>
                                ) : (
                                    <p className={styles.bonusDescription}>
                                        Оплатить бонусами можно
                                        <br /> не больше 50% от стоимости заказа
                                    </p>
                                )}
                            </div>
                        </div>
                    )}

                    {balance < 0 ? (
                        <Button
                            buttonStyle="with-fill"
                            className={styles.bookingButton}
                            onClick={() => goTo(`${NAV_PAYMENT}?sum=${-balance}`)}>{`Пополнить баланс на ${getCurrency(
                            -balance
                        )} ${RUB_SYMBOL}`}</Button>
                    ) : (
                        <Button
                            dataTest="booking-page-transfer-to-production-button"
                            buttonStyle="with-fill"
                            className={styles.bookingButton}
                            isDisabled={!!isDisabledOrderButton}
                            onClick={() => {
                                const constructionsWithWarnings = order.constructions.filter(c => c.errors.length > 0)
                                const price = (isSubdealerOrder ? order.totalAmegaCost : order.totalAmegaCostToDisplay) + shippingCost

                                const modalModel = ModalModel.Builder.withTitle('Подтвердите отправку заказа в производство')
                                    .withBody(
                                        <div>
                                            <p>
                                                <b>Заказ № {order.fullOrderNumber}</b>
                                            </p>
                                            <div className={styles.producingModalConstructions}>
                                                {order.constructions.map(construction => (
                                                    <div key={construction.id} className={styles.producingModalConstruction}>
                                                        <ConstructionPreview quantity={construction.quantity}>
                                                            <SvgIconConstructionDrawer construction={construction} maxWidth={200} />
                                                        </ConstructionPreview>
                                                    </div>
                                                ))}
                                                {goodsCount > 0 && (
                                                    <div key="goods" className={styles.producingModalConstruction}>
                                                        <GoodsIcon />
                                                        {goodsCount > 1 && (
                                                            <div className={styles.producingModalConstructionQuantity}>
                                                                <p>x{goodsCount}</p>
                                                            </div>
                                                        )}
                                                    </div>
                                                )}
                                            </div>
                                            <p>
                                                Конструкций: {constructionsCount} шт.&nbsp;
                                                {goodsCount > 0 && `и дополнительных материалов ${goodsCount} шт.`},&nbsp; на сумму&nbsp;
                                                {bonusValue > 0 ? (
                                                    <>
                                                        <span className={styles.producingModalSum}>
                                                            {getCurrency(price - bonusValue * 100)}
                                                        </span>
                                                        &nbsp;
                                                        {RUB_SYMBOL}
                                                        {' + '}
                                                        <span className={styles.producingModalSum}>{getBonusScore(bonusValue)}</span>
                                                        &nbsp;Б
                                                    </>
                                                ) : (
                                                    <>
                                                        <span className={styles.producingModalSum}>{getCurrency(price)}</span>
                                                        &nbsp;
                                                        {RUB_SYMBOL}
                                                    </>
                                                )}
                                                &nbsp;{shippingCost > 0 && '(в том числе доставка)'}
                                                {order.state !== OrderStates.Paid && (
                                                    <>
                                                        <br />
                                                        сумма будет списана с баланса.
                                                    </>
                                                )}
                                                <br />
                                                <br />
                                                <br />
                                                Вам останется выбрать дату отгрузки.
                                            </p>
                                            {currentDealer?.factoryId === FactoryTypes.Perm && shippingType === ShippingTypes.Pickup && (
                                                <div className={styles.warn} style={{marginTop: '30px'}}>
                                                    <div className="warn-icon" />
                                                    <p>
                                                        Период бесплатного хранения заказа на складе самовывоза составляет 3 календарных дня,
                                                        включая день поступления на склад. По истечении бесплатного периода, стоимость хранения
                                                        одного изделия составляет 200,00 рублей в день.{' '}
                                                    </p>
                                                </div>
                                            )}
                                            {constructionsWithWarnings.length > 0 && (
                                                <div className={styles.producingModalWarnings}>
                                                    <Expandable
                                                        header={
                                                            <div className={styles.producingModalWarningsHeader}>
                                                                <div className={styles.producingModalWarningsHeaderIcon} />
                                                                <p className={styles.producingModalWarningsHeaderTitle}>
                                                                    Обратите внимание, некоторые конструкции содержат рекомендации
                                                                </p>
                                                            </div>
                                                        }>
                                                        <div className={styles.producingModalWarningsBody}>
                                                            <OrderErrors order={order} />
                                                        </div>
                                                    </Expandable>
                                                </div>
                                            )}
                                        </div>
                                    )
                                    .withPositiveAction(
                                        new Action(
                                            'Передать в производство',
                                            () => {
                                                transferToProduction(
                                                    order,
                                                    selectedWarehouse.id,
                                                    shippingType,
                                                    address,
                                                    ContactInfo.Builder.withName(contactName).withPhone(contactPhone).build(),
                                                    comment,
                                                    bonusValue,
                                                    isCashMode
                                                )
                                                setModalModel(null)
                                            },
                                            'booking-page-transfer-to-production-modal-button'
                                        )
                                    )
                                    .withNegativeAction(new Action('Отменить', () => setModalModel(null)))
                                    .build()

                                setModalModel(modalModel)
                            }}>
                            Передать в производство
                        </Button>
                    )}
                </div>
                <div className={`${styles.mapColumn} ${shippingType === ShippingTypes.Delivery && styles.withMap}`}>
                    {!isMobile && shippingType === ShippingTypes.Pickup && (
                        <Ymaps
                            selectedPlacemark={selectedWarehouse as MapPlacemark}
                            placemarks={bookingOptions.warehouses.map(warehouse => warehouse as MapPlacemark)}
                            className={styles.ymaps}
                            onSelect={placemark => setSelectedWarehouse(placemark as Warehouse)}
                        />
                    )}
                    {shippingType === ShippingTypes.Delivery && (
                        <Ymaps
                            zoom={zoom}
                            address={addressForMap}
                            onClickCoords={async coordinates1 => {
                                const suggestions = await KladrManager.getSuggestionsByCoords(coordinates1)

                                if (suggestions.length === 0) {
                                    return
                                }
                                const address = Address.parseFromDadataAddress(suggestions[0].data)
                                setAddress(address)
                                checkAddressValidation(address)
                            }}
                        />
                    )}
                </div>
            </div>
        </div>
    )
}
