import * as React from 'react'
import {useEffect, useState} from 'react'
import {ShippingTypes} from '../../../../model/framer/shipping-types'
import {Order} from '../../../../model/framer/order'
import {Address} from '../../../../model/address'
import {useBook, useBookingDates} from '../../../../hooks/booking'
import {useShowStatusbar} from '../../../../hooks/statusbar'
import {useGoBack} from '../../../../hooks/router'
import {DatePicker} from '../../date-picker/date-picker'
import {ModalModel} from '../../modal/modal-model'
import {Action} from '../../../../model/action'
import {ContactInfo} from '../../../../model/framer/contact-info'
import {HINTS} from '../../../../constants/hints'
import {Modal} from '../../modal/modal'
import {getDateDDMMYYYYFormat, getDateYYYYMMDDFormat} from '../../../../helpers/date'
import {HeaderTitle} from '../../../mobile/header/header-title'
import {WithHint} from '../../with-hint/with-hint'
import styles from './booking-page-date-stage.module.css'

interface IBookingPageDateStageProps {
    order: Order
}

export function BookingPageDateStage(props: IBookingPageDateStageProps): JSX.Element | null {
    const {order} = props
    const {shippingType, warehouse, dadataAddress, contactPerson} = order

    const address = dadataAddress ? Address.parseFromDadataAddress(dadataAddress) : null
    const bookingDates = useBookingDates(order.id)
    const showStatusbar = useShowStatusbar()
    const goBack = useGoBack()
    const book = useBook()
    const [modalModel, setModalModel] = useState<ModalModel | null>(null)

    useEffect(() => {
        if (bookingDates?.isOrderBooked) {
            goBack()
            showStatusbar('Ваш заказ забронирован', false)
            return
        }
        if (bookingDates?.dates === undefined) {
            return
        }
        if (!bookingDates?.dates || bookingDates?.dates?.length === 0) {
            goBack()
            showStatusbar(
                'Возникла ошибка при получении дат бронирования, пожалуйста, попробуйте снова через несколько минут (заказ был переведен в статус Оплачен)',
                true
            )
            return
        }
    }, [bookingDates, showStatusbar, goBack])

    const availableDates = (bookingDates?.dates ?? []).map(dateStr => new Date(dateStr))

    if (!bookingDates) {
        return null
    }

    return (
        <div data-test="booking-page-date-stage">
            <HeaderTitle>Выбор даты</HeaderTitle>
            {modalModel && <Modal model={modalModel} />}
            <div className={styles.header}>
                {warehouse && shippingType === ShippingTypes.Pickup && <p>Склад отгрузки: {warehouse.name}</p>}
                {address && shippingType === ShippingTypes.Delivery && <p>Доставка по адресу: {address.toString()}</p>}
            </div>
            <div className={styles.body}>
                <WithHint hint={shippingType === ShippingTypes.Pickup ? HINTS.bookingDate : HINTS.deliveryDate}>
                    Выберите дату отгрузки
                </WithHint>
                <DatePicker
                    defaultSelectedDate={availableDates[0]}
                    availableDates={availableDates}
                    shouldMarkCurrentDay={false}
                    onSelectDate={date => {
                        const selectedDateString = getDateDDMMYYYYFormat(date)
                        const formattedSelectedDateString = getDateYYYYMMDDFormat(date)

                        if (shippingType === ShippingTypes.Pickup && warehouse) {
                            setModalModel(
                                ModalModel.Builder.withTitle('Подтвердите дату и склад отгрузки')
                                    .withBody(
                                        <p>
                                            Заказ № {order.fullOrderNumber}
                                            <br />
                                            <br />
                                            Дата отгрузки: {selectedDateString}
                                            <br />
                                            <br />
                                            Склад отгрузки: {warehouse.name}
                                        </p>
                                    )
                                    .withPositiveAction(
                                        new Action(
                                            `Оформить заказ на ${selectedDateString}`,
                                            () => {
                                                book(order, formattedSelectedDateString, warehouse.id, ShippingTypes.Pickup, null, null)
                                                setModalModel(null)
                                            },
                                            'booking-page-date-stage-modal-book-button'
                                        )
                                    )
                                    .withNegativeAction(new Action('Отменить', () => setModalModel(null)))
                                    .build()
                            )
                        } else if (shippingType === ShippingTypes.Delivery && address && contactPerson) {
                            setModalModel(
                                ModalModel.Builder.withTitle('Подтвердите дату и адрес доставки')
                                    .withBody(
                                        <p>
                                            Заказ № {order.fullOrderNumber}
                                            <br />
                                            <br />
                                            Дата доставки: {selectedDateString}
                                            <br />
                                            <br />
                                            Адрес доставки: {address.toString()}
                                        </p>
                                    )
                                    .withPositiveAction(
                                        new Action(
                                            `Оформить доставку на ${selectedDateString}`,
                                            () => {
                                                book(
                                                    order,
                                                    formattedSelectedDateString,
                                                    0,
                                                    ShippingTypes.Delivery,
                                                    address,
                                                    ContactInfo.Builder.withName(contactPerson.name).withPhone(contactPerson.phone).build()
                                                )
                                                setModalModel(null)
                                            },
                                            'booking-page-date-stage-modal-book-button'
                                        )
                                    )
                                    .withNegativeAction(new Action('Отменить', () => setModalModel(null)))
                                    .build()
                            )
                        }
                    }}
                />
            </div>
        </div>
    )
}
