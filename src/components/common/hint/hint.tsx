import * as React from 'react'
import styles from './hint.module.css'
import {ArrowTooltip} from '../arrow-tooltip/arrow-tooltip'
import {ReactComponent as HintIcon} from './resources/hint_icon.inline.svg'

interface IHintProps {
    text: string
    className?: string
    isDark?: boolean
}

export function Hint(props: IHintProps): JSX.Element {
    const {text, className, isDark = false} = props

    return (
        <ArrowTooltip title={text} placement="top">
            <div className={`${styles.hint} ${className ?? ''} ${isDark && styles.dark}`}>
                <HintIcon />
            </div>
        </ArrowTooltip>
    )
}
