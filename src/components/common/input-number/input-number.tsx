import * as React from 'react'
import {CSSProperties} from 'react'
import {Input} from '../input/input'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'

interface IInputNumberProps {
    value?: number
    min?: number
    max?: number
    step?: number
    className?: string
    style?: CSSProperties
    isDisabled?: boolean
    isAnnoyed?: boolean
    isRequired?: boolean
    isReadonly?: boolean
    useAutoSelect?: boolean
    useAutofocus?: boolean
    debounceTimeMs?: number
    useAutoWidth?: boolean
    dataTest?: string
    previousValue?: string
    onClick?: () => void
    onChange?: (value: number, isValid: boolean) => void
    onBlur?: (value: number, isValid: boolean) => void
    onKeyPressEnter?: (value: number, isValid: boolean) => void
    onKeyPressEsc?: () => void
}

const InputNumber = React.forwardRef<HTMLInputElement, IInputNumberProps>((props: IInputNumberProps, ref) => {
    const {
        value,
        min = 0,
        max = Number.MAX_SAFE_INTEGER,
        step = 1,
        className,
        style,
        isDisabled,
        isAnnoyed,
        isRequired,
        isReadonly,
        useAutoSelect,
        useAutofocus,
        debounceTimeMs,
        useAutoWidth,
        dataTest,
        previousValue,
        onClick,
        onChange,
        onBlur,
        onKeyPressEnter,
        onKeyPressEsc
    } = props

    return (
        <Input
            ref={ref}
            type="number"
            isDisabled={isDisabled}
            isAnnoyed={isAnnoyed}
            isRequired={isRequired}
            isReadonly={isReadonly}
            value={value !== undefined ? value.toString(10) : undefined}
            defaultValue={value !== undefined ? value.toString(10) : undefined}
            min={min ?? 0}
            max={max ?? 999999}
            step={step ?? 1}
            placeholder={String(min ?? 0)}
            className={className}
            style={{...style}}
            useAutoSelect={useAutoSelect}
            useAutofocus={useAutofocus}
            debounceTimeMs={debounceTimeMs ?? DEFAULT_DEBOUNCE_TIME_MS}
            useAutoWidth={useAutoWidth}
            dataTest={dataTest}
            previousValue={previousValue}
            onClick={onClick}
            onChange={onChange ? (value, isValid) => onChange && onChange(+value, isValid) : undefined}
            onBlur={onBlur ? (value, isValid) => onBlur && onBlur(+value, isValid) : undefined}
            onKeyPressEnter={onKeyPressEnter ? (value, isValid) => onKeyPressEnter && onKeyPressEnter(+value, isValid) : undefined}
            onKeyPressEsc={onKeyPressEsc}
        />
    )
})

InputNumber.displayName = 'InputNumber'

export {InputNumber}
