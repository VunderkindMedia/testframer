import * as React from 'react'
import {Button} from '../button/button'
import {ModalModel} from './modal-model'
import {CloseButton} from '../button/close-button/close-button'
import {ModalOverlay} from './modal-overlay/modal-overlay'
import {useAppContext} from '../../../hooks/app'
import styles from './modal.module.css'

interface IModalProps {
    model: ModalModel
    isFullMode?: boolean
}

export function Modal(props: IModalProps): JSX.Element | null {
    const {model, isFullMode = false} = props
    const {isMobile} = useAppContext()

    return (
        <ModalOverlay
            className={`${styles.modal} ${styles.modalOverlay} ${isMobile ? styles.mobile : ''} ${model.className}`}
            isFullMode={isFullMode}>
            <div className={styles.content}>
                <CloseButton
                    className={styles.closeButton}
                    onClick={model.onCloseFunc || model.negativeAction?.func || model.positiveAction?.func}
                />
                {model.title && <p className={styles.title}>{model.title}</p>}
                <div className={styles.body}>{model.body}</div>
                {(model.positiveAction || model.negativeAction) && (
                    <div className={styles.actions}>
                        {model.positiveAction && (
                            <Button
                                dataTest={model.positiveAction.dataTest}
                                className={`${styles.actionButton} ${styles.positiveButton}`}
                                buttonStyle="with-fill"
                                isNowrap={false}
                                onClick={() => model.positiveAction?.func()}>
                                {model.positiveAction.name}
                            </Button>
                        )}
                        {model.negativeAction && (
                            <Button
                                dataTest={model.negativeAction.dataTest}
                                className={`${styles.actionButton} ${styles.negativeButton}`}
                                onClick={() => model.negativeAction?.func()}>
                                {model.negativeAction.name}
                            </Button>
                        )}
                    </div>
                )}
            </div>
        </ModalOverlay>
    )
}
