import React from 'react'
import {ModalOverlay} from '../../modal/modal-overlay/modal-overlay'
import {CloseButton} from '../../button/close-button/close-button'
import {Button} from '../../button/button'
import {useAppContext} from '../../../../hooks/app'
import styles from './info-modal.module.css'

interface IInfoModalProps {
    children: React.ReactElement
    actionTitle: string
    onAction: () => void
    onClose: () => void
}

export const InfoModal = (props: IInfoModalProps): JSX.Element => {
    const {children, actionTitle, onAction, onClose} = props

    const {isMobile} = useAppContext()

    return (
        <ModalOverlay isFullMode={true} className={`${styles.modal} ${isMobile ? styles.mobile : ''}`}>
            <div className={styles.content}>
                <p className={styles.title}>Внимание!</p>
                {children}
                <Button buttonStyle="with-fill" onClick={onAction}>
                    {actionTitle}
                </Button>
                <CloseButton className={styles.closeButton} onClick={onClose} />
            </div>
        </ModalOverlay>
    )
}
