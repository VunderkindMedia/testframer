import * as React from 'react'
import {BodyPortal} from '../../body-portal/body-portal'
import {useModalOpen} from '../../../../hooks/ui'
import {ReactNode} from 'react'
import styles from './modal-overlay.module.css'

interface IModalOverlay {
    className?: string
    children?: ReactNode
    onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
    isFullMode?: boolean
    isTransparent?: boolean
}

export const ModalOverlay = (props: IModalOverlay): JSX.Element => {
    const {className, children, onClick, isFullMode = false, isTransparent = false} = props
    useModalOpen(true)

    return (
        <BodyPortal>
            <div
                className={`${styles.modalOverlay} ${className} ${isFullMode && styles.fullMode} ${isTransparent && styles.transparent}`}
                onClick={onClick}>
                {children}
            </div>
        </BodyPortal>
    )
}
