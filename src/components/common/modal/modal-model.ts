import {Action} from '../../../model/action'
import * as React from 'react'

export class ModalModel {
    className = ''
    title = ''
    body: JSX.Element | null = null
    positiveAction?: Action
    negativeAction?: Action
    onCloseFunc?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void

    static get Builder(): Builder {
        return new Builder()
    }
}

class Builder {
    private _model = new ModalModel()

    withTitle(title: string): Builder {
        this._model.title = title
        return this
    }

    withBody(body: JSX.Element): Builder {
        this._model.body = body
        return this
    }

    withPositiveAction(action: Action): Builder {
        this._model.positiveAction = action
        return this
    }

    withNegativeAction(action: Action): Builder {
        this._model.negativeAction = action
        return this
    }

    withOnCloseFunc(onCloseFunc: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void): Builder {
        this._model.onCloseFunc = onCloseFunc
        return this
    }

    withClassName(className: string): Builder {
        this._model.className = className
        return this
    }

    build(): ModalModel {
        return this._model
    }
}
