import * as React from 'react'

interface IGoodsIconProps {
    lineColor?: string
}

export function GoodsIcon(props: IGoodsIconProps): JSX.Element {
    const lineColor = props.lineColor ?? '#C4C4C4'

    return (
        <svg width="27" height="38" viewBox="0 0 27 38" fill="none">
            <rect x="-1" y="1" width="17" height="24" transform="matrix(-1 0 0 1 21 4)" stroke={lineColor} strokeWidth="2" />
            <path d="M22.4648 5L25.1315 1H1.86852L4.53518 5H22.4648Z" stroke={lineColor} strokeWidth="2" />
            <path d="M5 29.2088V5.7912L1.95105 1H1V34H1.95105L5 29.2088Z" stroke={lineColor} strokeWidth="2" />
            <path d="M22 29.2088V5.7912L25.0489 1H26V34H25.0489L22 29.2088Z" stroke={lineColor} strokeWidth="2" />
            <rect x="-1" y="1" width="25" height="3" transform="matrix(-1 0 0 1 25 33)" stroke={lineColor} strokeWidth="2" />
            <rect x="11" y="8" width="8" height="2" fill={lineColor} />
            <rect x="8" y="8" width="2" height="2" fill={lineColor} />
            <rect x="11" y="13" width="8" height="2" fill={lineColor} />
            <rect x="8" y="13" width="2" height="2" fill={lineColor} />
            <rect x="11" y="18" width="8" height="2" fill={lineColor} />
            <rect x="8" y="18" width="2" height="2" fill={lineColor} />
            <rect x="11" y="23" width="8" height="2" fill={lineColor} />
            <rect x="8" y="23" width="2" height="2" fill={lineColor} />
        </svg>
    )
}
