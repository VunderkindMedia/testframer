import * as React from 'react'
import {useCallback} from 'react'
import {Input, IInputProps} from '../input'
import styles from './custom-input.module.css'

interface ICustomInputProps extends IInputProps {
    onBlur?: (value: string) => void
    error?: null | string
    hideError?: boolean
}

const CustomInput = React.forwardRef<HTMLInputElement, ICustomInputProps>((props: ICustomInputProps, ref): JSX.Element => {
    const {className, error = null, onBlur, hideError = false} = props

    const onBlurHandler = useCallback(
        (value: string) => {
            onBlur && onBlur(value)
        },
        [onBlur]
    )

    const getClassName = useCallback(() => {
        if (error === null) {
            return styles.input
        }
        return `${styles.input} ${error ? styles.error : styles.success}`
    }, [error])

    const inputProps: IInputProps = {
        ...props,
        className: getClassName(),
        isAnnoyed: false,
        noValidate: true,
        onBlur: onBlurHandler
    }
    return (
        <div className={`${styles.inputWrapper} ${className}`}>
            <Input ref={ref} {...inputProps} />
            {!hideError && error && <span className={styles.error}>{error}</span>}
        </div>
    )
})

CustomInput.displayName = 'CustomInput'

export {CustomInput}
