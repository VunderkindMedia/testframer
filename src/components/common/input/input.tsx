import * as React from 'react'
import styles from './input.module.css'
import {useEffect, useImperativeHandle, useRef} from 'react'
import Timeout = NodeJS.Timeout
import {useCallback} from 'react'
import {BodyPortal} from '../body-portal/body-portal'
import {CSSProperties} from 'react'

export interface IInputProps {
    value?: string
    defaultValue?: string
    placeholder?: string
    type?: string
    name?: string
    isRequired?: boolean
    pattern?: string
    title?: string
    className?: string
    style?: CSSProperties
    isReadonly?: boolean
    isDisabled?: boolean
    isAnnoyed?: boolean
    min?: number
    max?: number
    step?: number
    maxlength?: number
    useAutoWidth?: boolean
    useAutoSelect?: boolean
    useAutofocus?: boolean
    debounceTimeMs?: number
    onClick?: (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => void
    onChange?: (value: string, isValid: boolean) => void
    onBlur?: (value: string, isValid: boolean) => void
    onKeyPressEnter?: (value: string, isValid: boolean) => void
    onKeyPressEsc?: () => void
    dataTest?: string
    previousValue?: string
    noValidate?: boolean
    hasError?: boolean
}

const Input = React.forwardRef<HTMLInputElement, IInputProps>((props: IInputProps, ref) => {
    const {
        value,
        defaultValue,
        placeholder,
        type,
        className,
        style,
        useAutoWidth,
        useAutoSelect,
        useAutofocus,
        isReadonly,
        isDisabled,
        isAnnoyed,
        isRequired,
        pattern,
        title,
        min,
        max,
        step = 1,
        maxlength,
        debounceTimeMs,
        onClick,
        onChange,
        onBlur,
        onKeyPressEnter,
        onKeyPressEsc,
        dataTest,
        previousValue,
        noValidate = false,
        hasError = false
    } = props

    const safeValueRef = useRef(value?.toString() ?? '')
    const inputRef = useRef<HTMLInputElement | null>(null)
    const inputFakeRef = useRef<HTMLDivElement>(null)
    const timerRef = useRef<Timeout | null>(null)

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    useImperativeHandle(ref, () => inputRef.current!)

    const syncWidth = useCallback(
        (value: string | null = null) => {
            if (!useAutoWidth || !inputFakeRef.current || !inputRef.current) {
                return
            }
            inputFakeRef.current.innerHTML = value ?? inputRef.current.value
            const {paddingLeft, paddingRight, borderTopWidth} = getComputedStyle(inputRef.current)
            const width =
                inputFakeRef.current.getBoundingClientRect().width + parseInt(paddingLeft) + parseInt(paddingRight) + parseInt(borderTopWidth)

            inputRef.current.style.width = `${width}px`
            inputRef.current.style.minWidth = `${width}px`
        },
        [useAutoWidth, inputFakeRef.current?.innerHTML]
    )

    useEffect(() => {
        syncWidth()
    }, [syncWidth])

    useEffect(() => {
        if (inputRef.current === document.activeElement) {
            return
        }
        useAutofocus && inputRef.current?.focus()
    }, [useAutofocus])

    useEffect(() => {
        if (!inputRef.current || value === null || value === undefined) {
            return
        }

        inputRef.current.value = value
        syncWidth()
    }, [value, syncWidth, inputRef.current?.value])

    useEffect(() => {
        if (!inputRef.current || value === null || value === undefined) {
            return
        }

        if (previousValue && value !== previousValue) {
            inputRef.current.value = value
        }
        syncWidth()
    }, [value, previousValue, syncWidth])

    useEffect(() => {
        return () => {
            timerRef.current && clearTimeout(timerRef.current)
        }
    }, [])

    const onChangeHandler = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            timerRef.current && clearTimeout(timerRef.current)

            let value = e.target.value

            if (step && type === 'number') {
                value = (Math.ceil(+value / step) * step).toString()
            }
            syncWidth(value.length === 0 && type === 'number' ? safeValueRef.current : value)

            timerRef.current = setTimeout(() => {
                onChange && onChange(value, Boolean(inputRef.current?.checkValidity()))
            }, debounceTimeMs ?? 0)
        },
        [debounceTimeMs, onChange, syncWidth, type]
    )

    const onKeyDownHandler = useCallback(
        (e: React.KeyboardEvent<HTMLInputElement>) => {
            if (e.key === '.' && inputRef.current) {
                safeValueRef.current = inputRef.current.value
            }

            if (e.key === 'Enter' && inputRef.current) {
                onKeyPressEnter && onKeyPressEnter(inputRef.current.value, inputRef.current.checkValidity())
                inputRef.current?.blur()
            } else if (e.key === 'Escape') {
                inputRef.current?.blur()
                onKeyPressEsc && onKeyPressEsc()
            }
        },
        [onKeyPressEnter, onKeyPressEsc]
    )

    const onBlurHandler = useCallback(() => {
        if (onChange && !noValidate) {
            return
        }
        if (inputRef.current) {
            const isValid = inputRef.current.checkValidity()
            if (onBlur && (isValid || noValidate)) {
                onBlur && onBlur(inputRef.current.value, isValid)
            }
        }
        syncWidth()
    }, [noValidate, onBlur, onChange, syncWidth])

    return (
        <>
            <input
                ref={inputRef}
                required={isRequired ?? false}
                type={type ?? 'text'}
                min={min}
                max={max}
                step={step}
                maxLength={maxlength}
                defaultValue={defaultValue ?? ''}
                placeholder={placeholder ?? ''}
                pattern={pattern}
                data-test={dataTest}
                title={title}
                className={`${styles.input} ${isAnnoyed && styles.annoyed} ${className} ${hasError && styles.error}`}
                style={{...style}}
                readOnly={isReadonly}
                disabled={isDisabled}
                onClick={onClick}
                onChange={onChangeHandler}
                onKeyDown={onKeyDownHandler}
                onFocus={e => {
                    useAutoSelect && e.target.select()
                    syncWidth()
                }}
                onBlur={onBlurHandler}
            />
            <BodyPortal>
                <div ref={inputFakeRef} className={`${styles.input} ${styles.inputFake} ${props.className}`} />
            </BodyPortal>
        </>
    )
})

Input.displayName = 'Input'

export {Input}
