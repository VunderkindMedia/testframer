import * as React from 'react'
import InputMask from 'react-input-mask'
import {IInputProps} from '../input'
import {useCallback} from 'react'
import styles from '../input.module.css'
import customInputStyles from '../custom-input/custom-input.module.css'

const removeCharacters = (str: string): string => str.replace(/[^\d]/g, '')

interface IPhoneInputProps extends IInputProps {
    error?: string | null
}

export function PhoneInput(props: IPhoneInputProps): JSX.Element {
    const {value, onChange, onBlur, dataTest, error = null} = props

    const checkValidation = useCallback((value: string): boolean => {
        return /8\s\(9[0-9]{2}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/.test(value)
    }, [])

    const onChangeHandler = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            const value = e.target.value
            const replacedValue = removeCharacters(value)
            onChange && onChange(replacedValue, checkValidation(replacedValue))
        },
        [onChange, checkValidation]
    )

    const onBlurHandler = useCallback(
        (e: React.FocusEvent<HTMLInputElement>) => {
            onBlur && onBlur(e.target.value, checkValidation(e.target.value))
        },
        [onBlur, checkValidation]
    )

    return (
        <>
            <InputMask
                onPaste={(e: React.ClipboardEvent<HTMLInputElement>) => {
                    let text = removeCharacters(e.clipboardData.getData('text'))

                    if (text.length === 11 && text.startsWith('79')) {
                        text = text.replace('79', '89')
                    }

                    const isValid = text.length === 11 && text.startsWith('89')

                    if (isValid) {
                        e.preventDefault()
                        e.stopPropagation()
                        onChange && onChange(text, isValid)
                    }
                }}
                name="phone"
                mask={value ? '8 (\\999) 999-99-99' : ''}
                data-test={dataTest}
                alwaysShowMask={false}
                className={`${styles.input} ${customInputStyles.input} ${
                    error === null ? '' : error ? customInputStyles.error : customInputStyles.success
                }`}
                value={value}
                autoComplete="phone"
                placeholder="8 (999) 999-99-99"
                onChange={onChangeHandler}
                onBlur={onBlurHandler}
            />
            {/* eslint-disable-next-line @typescript-eslint/naming-convention */}
            {error && <span dangerouslySetInnerHTML={{__html: error}} className={customInputStyles.error} />}
        </>
    )
}
