import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {RadioGroup} from '../radio-group/radio-group'
import {Moskitka} from '../../../model/framer/moskitka'
import {ColorItem} from '../color-item/color-item'
import {getHexMoskitkaColor, getMoskitkaColorName, MoskitkaColors} from '../../../model/framer/moskitka-colors'
import {getProductLamination} from '../../../helpers/lamination'
import {useAppContext} from '../../../hooks/app'
import styles from './moskitka-configurator.module.css'
import {useUpdateMoskitka} from '../../../hooks/builder'
import {FactoryTypes} from '../../../model/framer/factory-types'

export function MoskitkaConfigurator(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const lamination = useSelector((state: IAppState) => state.lamination.all)
    const partner = useSelector((state: IAppState) => state.account.partner)

    const updateMoskitka = useUpdateMoskitka(currentOrder, currentContext)

    const {isMobile} = useAppContext()

    if (
        !currentOrder ||
        !currentConstruction ||
        !(currentContext instanceof FrameFilling) ||
        !currentContext.moskitka ||
        currentContext.isRoot
    ) {
        return null
    }

    const [currentOutsideLamination] = getProductLamination(currentConstruction.products[0], lamination)

    const selectColor = (color: MoskitkaColors): void => {
        if (!currentContext.moskitka) {
            return
        }
        const moskitka = currentContext.moskitka.clone
        moskitka.color = color
        updateMoskitka(moskitka)
    }

    return (
        <div className={`${styles.configurator} ${isMobile ? styles.mobile : ''}`}>
            <RadioGroup
                className={styles.radioGroup}
                items={['Обычная', 'Антикошка']}
                selectedIndex={currentContext.moskitka.catproof ? 1 : 0}
                onSelect={index => {
                    const moskitka = currentContext.moskitka?.clone ?? new Moskitka()
                    moskitka.catproof = index !== 0
                    updateMoskitka(moskitka)
                }}
            />
            <div className={styles.colors}>
                <ColorItem
                    isSelected={currentContext.moskitka.color === MoskitkaColors.White}
                    onClick={() => selectColor(MoskitkaColors.White)}
                    name={getMoskitkaColorName(MoskitkaColors.White)}
                    color="#fff"
                />
                <ColorItem
                    isSelected={currentContext.moskitka.color === MoskitkaColors.Brown}
                    onClick={() => selectColor(MoskitkaColors.Brown)}
                    name={getMoskitkaColorName(MoskitkaColors.Brown)}
                    color={getHexMoskitkaColor(MoskitkaColors.Brown, currentOutsideLamination.hex)}
                />
                {partner?.factoryId === FactoryTypes.Perm && (
                    <ColorItem
                        isSelected={currentContext.moskitka.color === MoskitkaColors.ConstructionColor}
                        onClick={() => selectColor(MoskitkaColors.ConstructionColor)}
                        name={getMoskitkaColorName(MoskitkaColors.ConstructionColor)}
                        color={`${currentOutsideLamination.hex}`}
                    />
                )}
            </div>
            <p className={styles.footer}>Параметры применяются ко всем москитным сеткам в конструкции</p>
        </div>
    )
}
