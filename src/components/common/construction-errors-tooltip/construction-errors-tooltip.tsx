import * as React from 'react'
import './construction-errors-tooltip.css'
import {Construction} from '../../../model/framer/construction'
import {ErrorTypes} from '../../../model/framer/error-types'
import {ArrowTooltip} from '../arrow-tooltip/arrow-tooltip'

interface IConstructionErrorsTooltipProps {
    construction: Construction
}

export function ConstructionErrorsTooltip(props: IConstructionErrorsTooltipProps): JSX.Element | null {
    const {construction} = props

    if (construction.errors.length === 0) {
        return null
    }

    const errors = construction.errors.filter(e => e.errorType === ErrorTypes.Error)
    const warnings = construction.errors.filter(e => e.errorType === ErrorTypes.Information)

    return (
        <div className="construction-errors-tooltip">
            {errors.length > 0 && (
                <ArrowTooltip
                    placement="bottom"
                    title={
                        <div className="construction-errors-tooltip__items">
                            {errors.map((error, idx) => (
                                <p key={idx} className="construction-errors-tooltip__item">
                                    {error.displayMessage}
                                </p>
                            ))}
                        </div>
                    }>
                    <div className="error-icon" />
                </ArrowTooltip>
            )}
            {warnings.length > 0 && (
                <ArrowTooltip
                    placement="bottom"
                    title={
                        <div className="construction-errors-tooltip__items">
                            {warnings.map((warning, idx) => (
                                <p key={idx} className="construction-errors-tooltip__item">
                                    {warning.displayMessage}
                                </p>
                            ))}
                        </div>
                    }>
                    <div className="warn-icon" />
                </ArrowTooltip>
            )}
        </div>
    )
}
