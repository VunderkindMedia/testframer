import * as React from 'react'
import {useCallback, useEffect, useLayoutEffect, useRef, useState} from 'react'
import './svg-construction-drawer.css'
import {Construction} from '../../../model/framer/construction'
import {Point} from '../../../model/geometry/point'
import {IContext} from '../../../model/i-context'
import {Size} from '../../../model/size'
import {DimensionLine} from '../../../model/dimension-line'
import {InputNumber} from '../input-number/input-number'
import {ArrowTooltip} from '../arrow-tooltip/arrow-tooltip'
import {Rectangle} from '../../../model/geometry/rectangle'
import {Lamination} from '../../../model/framer/lamination'
import {HostProductInfo} from '../../../model/host-product-info'
import {Svg} from '../svg/svg'
import {SvgFrameFilling} from '../svg/svg-frame-filling'
import {SvgDimensionLines} from '../svg/svg-dimension-lines'
import {SvgConnector} from '../svg/svg-connector'
import {PointsOfView} from '../../../model/points-of-view'
import {getDimensionLinesOffsets} from '../../../helpers/line'
import {getProductLamination} from '../../../helpers/lamination'
import {getHexMoskitkaColor, MoskitkaColors} from '../../../model/framer/moskitka-colors'

const DRAW_TYPES = ['line', 'rect', 'polygon', 'text']
const DEFAULT_FONT_SIZE = 14

export interface ISvgConstructionDrawerProps {
    currentConstruction: Construction
    fontSize?: number
    fillingTextColor?: string
    lineOffset: number
    bracketWidth: number
    lineWidth: number
    lineBoldWidth: number
    currentContext?: IContext | null
    highlightedContext?: IContext | null
    currentDimensionLine?: DimensionLine | null
    highlightedDimensionLine?: DimensionLine | null
    isReadonly?: boolean
    windowSize: Size
    drawerSize?: Size
    lamination: Lamination[]
    onSelectContext?: (context: IContext | null) => void
    onHoverContext?: (context: IContext | null) => void
    onSelectDimensionLine?: (dimensionLine: DimensionLine | null) => void
    onHoverDimensionLine?: (dimensionLine: DimensionLine | null) => void
    onDrawHostProducts?: (hostProductInfos: HostProductInfo[]) => void
    onDrawAreaChange?: (rect: Rectangle) => void
    onChangeDimensionLineValue?: (dLine: DimensionLine, value: number, context: IContext | null | undefined) => void
    onClickOutConstruction?: () => void
}

interface ILineValue {
    id: string | null
    value: number | null
}

// а может вынести отрисовку размерных лиинй отдльено?
export function SvgConstructionDrawer(props: ISvgConstructionDrawerProps): JSX.Element {
    const {
        isReadonly,
        currentConstruction,
        currentDimensionLine,
        highlightedDimensionLine,
        currentContext,
        highlightedContext,
        fontSize = DEFAULT_FONT_SIZE,
        fillingTextColor,
        lineOffset,
        bracketWidth,
        lineWidth,
        lineBoldWidth,
        onSelectContext,
        onHoverContext,
        onSelectDimensionLine,
        onHoverDimensionLine,
        lamination,
        drawerSize,
        windowSize,
        onDrawAreaChange,
        onDrawHostProducts,
        onChangeDimensionLineValue,
        onClickOutConstruction
    } = props

    const [stageSize, setStageSize] = useState(new Size(1, 1))
    const [lastUpdatedLine, setLastUpdatedLine] = useState<ILineValue>({id: null, value: null})
    const drawingRef = useRef<HTMLDivElement>(null)
    const drawingWrapRef = useRef<HTMLDivElement>(null)

    const productContext = currentConstruction.products.find(p => Boolean(currentContext && p.findNodeById(currentContext.nodeId)))
    const dimensionLines = currentConstruction.getDimensionLines()
    const [verticalOffset, horizontalOffset] = getDimensionLinesOffsets(dimensionLines, lineOffset, bracketWidth)

    const {x, y, size: constructionSize} = currentConstruction

    let scale = Math.min(
        (stageSize.width - verticalOffset) / constructionSize.width,
        (stageSize.height - horizontalOffset) / constructionSize.height
    )
    if (scale > 1) {
        scale = 1
    }

    const xOffset = verticalOffset / scale
    const yOffset = horizontalOffset / scale

    const rectWidth = constructionSize.width * scale + xOffset * scale
    const rectHeight = constructionSize.height * scale + yOffset * scale

    dimensionLines.forEach(dLine => {
        const lineOffsetLocal = lineOffset / scale
        dLine.bracketWidth = bracketWidth
        dLine.scale = scale

        if (dLine.isHorizontal) {
            dLine.start.y *= lineOffsetLocal
            dLine.end.y *= lineOffsetLocal
        } else if (dLine.isVertical) {
            dLine.start.x *= lineOffsetLocal
            dLine.end.x *= lineOffsetLocal
        }
    })

    //TODO: все эти DOM проверки создают затыки в производительности, подумать как оптимизирвоать
    const isDimensionLinePart = useCallback((element: HTMLElement) => {
        return element.dataset.dline || element.parentElement?.dataset.dline
    }, [])

    const onClickHandler = useCallback(
        (e: MouseEvent): void => {
            const target = e.target as HTMLElement
            if (isDimensionLinePart(target)) {
                return
            }
            if (!drawingRef.current?.contains(target) || !DRAW_TYPES.includes(target.tagName)) {
                onClickOutConstruction && onClickOutConstruction()
            }
        },
        [onClickOutConstruction, isDimensionLinePart]
    )

    const onMouseMove = useCallback(
        (e: MouseEvent): void => {
            const target = e.target as HTMLElement
            if (highlightedContext) {
                !DRAW_TYPES.includes(target.tagName) && onHoverContext && onHoverContext(null)
            }
            if (highlightedDimensionLine) {
                if (isDimensionLinePart(target)) {
                    return
                }
                !DRAW_TYPES.includes(target.tagName) && onHoverDimensionLine && onHoverDimensionLine(null)
            }
        },
        [highlightedContext, highlightedDimensionLine, onHoverContext, onHoverDimensionLine, isDimensionLinePart]
    )

    useEffect(() => {
        if (drawingWrapRef.current && !drawerSize) {
            setStageSize(new Size(drawingWrapRef.current.clientWidth, drawingWrapRef.current.clientHeight))
        }

        window.addEventListener('click', onClickHandler)

        return () => {
            window.removeEventListener('click', onClickHandler)
        }
    }, [drawerSize, onClickHandler])

    useEffect(() => {
        document.addEventListener('mousemove', onMouseMove)

        return () => {
            document.removeEventListener('mousemove', onMouseMove)
        }
    }, [onMouseMove])

    useEffect(() => {
        if (!drawingWrapRef.current || drawerSize) {
            return
        }

        setStageSize(new Size(drawingWrapRef.current.clientWidth, drawingWrapRef.current.clientHeight))
    }, [windowSize, drawerSize])

    useEffect(() => {
        if (!drawingWrapRef.current) {
            return
        }
        if (stageSize.height > 1 && stageSize.width > 1) {
            const newStageSize = new Size(drawingWrapRef.current.clientWidth, drawingWrapRef.current.clientHeight)
            if (!stageSize.equals(newStageSize)) {
                setStageSize(newStageSize)
            }
        }
    }, [currentConstruction, stageSize])

    useEffect(() => {
        if (drawerSize && !stageSize.equals(drawerSize)) {
            setStageSize(drawerSize)
        }
    }, [drawerSize, stageSize])

    useEffect(() => {
        onSelectDimensionLine && onSelectDimensionLine(null)
        onHoverDimensionLine && onHoverDimensionLine(null)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentConstruction])

    useEffect(() => {
        return () => {
            onSelectContext && onSelectContext(null)
            onHoverContext && onHoverContext(null)
            onSelectDimensionLine && onSelectDimensionLine(null)
            onHoverDimensionLine && onHoverDimensionLine(null)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useLayoutEffect(() => {
        const hostProductInfos: HostProductInfo[] = []

        currentConstruction.products.forEach(product => {
            const pRectWidth = product.width * scale
            const pRectHeight = product.height * scale

            const pX = (product.refPoint.x - x + xOffset) * scale + (stageSize.width - rectWidth) / 2
            const pY = (product.refPoint.y - y + yOffset) * scale + (stageSize.height - rectHeight) / 2

            const originalProduct = currentConstruction.products.find(p => p.productGuid === product.productGuid)
            if (!originalProduct) {
                return
            }

            hostProductInfos.push(new HostProductInfo(originalProduct, new Rectangle(new Point(pX, pY), new Size(pRectWidth, pRectHeight))))
        })

        onDrawAreaChange &&
            onDrawAreaChange(
                new Rectangle(
                    new Point((stageSize.width - rectWidth) / 2, (stageSize.height - rectHeight) / 2),
                    new Size(rectWidth, rectHeight)
                )
            )
        onDrawHostProducts && onDrawHostProducts(hostProductInfos)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentConstruction, stageSize])

    const getDimensionLineInputRect = useCallback(
        (dLine: DimensionLine | null | undefined): Rectangle => {
            if (!dLine) {
                return new Rectangle(new Point(-1000, -1000))
            }

            const realDLine = dimensionLines.find(dl => dl.nodeId === dLine.nodeId)

            if (!realDLine) {
                return new Rectangle(new Point(-1000, -1000))
            }

            const dimensionLineRect = Rectangle.buildFromPoints([...realDLine.points, ...realDLine.bracketPoints1, ...realDLine.bracketPoints2])

            const offsetX = (stageSize.width - constructionSize.width * scale + verticalOffset) / 2
            const offsetY = (stageSize.height - constructionSize.height * scale + horizontalOffset) / 2

            dimensionLineRect.refPoint.x = dimensionLineRect.refPoint.x * scale - (realDLine.isHorizontal ? x * scale : 0) + offsetX
            dimensionLineRect.refPoint.y = dimensionLineRect.refPoint.y * scale - (realDLine.isVertical ? y * scale : 0) + offsetY

            dimensionLineRect.size.width *= scale
            dimensionLineRect.size.height *= scale

            return dimensionLineRect
        },
        [stageSize, constructionSize, scale, horizontalOffset, verticalOffset, x, y, dimensionLines]
    )

    console.log('%c SvgConstructionDrawer render', 'color: #00f')

    const [outsideLamination, insideLamination] = getProductLamination(currentConstruction.products[0], lamination)

    let solidFillingCounter = 1
    let frameFillingCounter = 1

    const viewBoxSize = new Size(constructionSize.width + xOffset + lineWidth, constructionSize.height + yOffset + lineWidth)
    const svgSize = stageSize.width > viewBoxSize.width && stageSize.height > viewBoxSize.height ? viewBoxSize : stageSize

    return (
        <div className={`svg-construction-drawer-wrap ${isReadonly && 'disabled'}`} ref={drawingWrapRef}>
            <div className="svg-container svg-construction-drawer" ref={drawingRef}>
                {
                    <Svg viewboxSize={viewBoxSize} size={svgSize}>
                        {currentConstruction.products.map(product => (
                            <SvgFrameFilling
                                key={product.productGuid}
                                product={product}
                                isEntranceDoor={product.isEntranceDoor}
                                pointOfView={currentConstruction.defaultPointOfView}
                                defaultPointOfView={currentConstruction.defaultPointOfView}
                                currentContext={currentContext}
                                highlightedContext={highlightedContext}
                                frameFilling={product.frame}
                                insideLaminationColor={
                                    currentConstruction.isBrownMoskitka
                                        ? getHexMoskitkaColor(MoskitkaColors.Brown, insideLamination.hex)
                                        : insideLamination.hex
                                }
                                outsideLaminationColor={
                                    currentConstruction.isBrownMoskitka
                                        ? getHexMoskitkaColor(MoskitkaColors.Brown, outsideLamination.hex)
                                        : outsideLamination.hex
                                }
                                scale={scale}
                                xOffset={-x + xOffset}
                                yOffset={-y + yOffset}
                                lineWidth={lineWidth / scale}
                                fontSize={fontSize}
                                solidFillingCounter={() => solidFillingCounter++}
                                frameFillingCounter={() => frameFillingCounter++}
                                textColor={fillingTextColor}
                                onSelectContext={onSelectContext}
                                onHoverContext={onHoverContext}
                            />
                        ))}
                        {currentConstruction.products.map(product =>
                            product.connectors.map(connector => {
                                return (
                                    <SvgConnector
                                        key={connector.nodeId}
                                        currentContext={currentContext}
                                        highlightedContext={highlightedContext}
                                        onSelectContext={onSelectContext}
                                        onHoverContext={onHoverContext}
                                        connector={connector}
                                        xOffset={-x + xOffset}
                                        yOffset={-y + yOffset}
                                        scale={scale}
                                        lamination={
                                            currentConstruction.defaultPointOfView === PointsOfView.Inside
                                                ? insideLamination
                                                : outsideLamination
                                        }
                                        lineWidth={lineWidth / scale}
                                    />
                                )
                            })
                        )}
                        <SvgDimensionLines
                            dimensionLines={dimensionLines.filter(l => l.isVertical)}
                            xOffset={xOffset}
                            yOffset={-y + yOffset}
                            productContext={productContext}
                            currentDimensionLine={currentDimensionLine}
                            highlightedDimensionLine={highlightedDimensionLine}
                            lineWidth={lineWidth / scale}
                            lineBoldWidth={lineBoldWidth / scale}
                            fontSize={fontSize}
                        />
                        <SvgDimensionLines
                            dimensionLines={dimensionLines.filter(l => l.isHorizontal)}
                            xOffset={-x + xOffset}
                            yOffset={yOffset}
                            productContext={productContext}
                            currentDimensionLine={currentDimensionLine}
                            highlightedDimensionLine={highlightedDimensionLine}
                            lineWidth={lineWidth / scale}
                            lineBoldWidth={lineBoldWidth / scale}
                            fontSize={fontSize}
                        />
                    </Svg>
                }
            </div>
            {dimensionLines.map(dLine => {
                const isVisible = productContext ? dLine.markers.includes(productContext.nodeId) : true
                if (!isVisible) {
                    return null
                }

                const labelRect = getDimensionLineInputRect(dLine)
                const isSelected = currentDimensionLine?.equals(dLine)
                const rotateAngle = dLine.isHorizontal || isSelected ? 0 : -90
                const isDisabled = dLine.markers.length === 0
                const number = +dLine.origin.length.toFixed(2)

                const labelProps = {
                    style: {
                        zIndex: isSelected ? 2 : 1,
                        bottom: `${labelRect.refPoint.y}px`,
                        left: `${labelRect.refPoint.x}px`,
                        width: `${labelRect.size.width}px`,
                        height: `${labelRect.size.height}px`
                    },
                    onClick: () => onSelectDimensionLine && onSelectDimensionLine(dLine),
                    onMouseOver: () => {
                        if (highlightedDimensionLine && highlightedDimensionLine.equals(dLine)) {
                            return
                        }
                        onHoverDimensionLine && onHoverDimensionLine(dLine)
                    }
                }

                const inputNumberProps = {
                    value: number,
                    useAutoWidth: true,
                    useAutoSelect: true,
                    useAutofocus: isSelected,
                    className: 'dimension-line-input',
                    previousValue: number.toString(),
                    style: {
                        transform: `rotate(${rotateAngle}deg)`
                    }
                }

                if (isDisabled) {
                    return (
                        <ArrowTooltip key={dLine.id} title="Для изменения ширины конструкции отредактируйте размеры изделий">
                            <label className="dimension-line-input-label" {...labelProps} data-dline={true}>
                                <InputNumber {...inputNumberProps} isReadonly={true} />
                            </label>
                        </ArrowTooltip>
                    )
                }

                let lastValue = number
                const applyValueHandler = (value: number): void => {
                    setLastUpdatedLine({id: dLine.id, value})
                    if (lastValue === value) {
                        return
                    }
                    lastValue = value
                    value !== number && onChangeDimensionLineValue && onChangeDimensionLineValue(dLine, value, currentContext)
                }

                if (dLine.id === lastUpdatedLine.id) {
                    inputNumberProps.previousValue = (lastUpdatedLine.value || number).toString()
                }

                return (
                    <label className="dimension-line-input-label" key={dLine.id} {...labelProps} data-dline={true}>
                        <InputNumber
                            {...inputNumberProps}
                            onKeyPressEnter={(value, valid) => {
                                if (!valid) {
                                    return
                                }

                                applyValueHandler(value)
                            }}
                            onBlur={(value, valid) => {
                                onSelectDimensionLine && onSelectDimensionLine(null)
                                if (!valid) {
                                    return
                                }

                                applyValueHandler(value)
                            }}
                            onKeyPressEsc={() => onSelectDimensionLine && onSelectDimensionLine(null)}
                        />
                    </label>
                )
            })}
        </div>
    )
}
