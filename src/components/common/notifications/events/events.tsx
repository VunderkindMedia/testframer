import * as React from 'react'
import {useCallback} from 'react'
import {useDispatch} from 'react-redux'
import {Link} from 'react-router-dom'
import {Notification} from '../../../../model/framer/notifications/notification'
import {markAllNotificationsAsRead, markNotificationAsRead, markNotificationAsUnread} from '../../../../redux/account/actions'
import {getFullDateFormat} from '../../../../helpers/date'
import {EmptyMessage} from '../empty-message/empty-message'
import {useAppContext} from '../../../../hooks/app'
import {getOrderLink} from '../../../../helpers/orders'
import styles from './events.module.css'

interface IEventsProps {
    notifications: Notification[]
}

export const Events = (props: IEventsProps): JSX.Element => {
    const {notifications} = props
    const dispatch = useDispatch()
    const {isMobile} = useAppContext()

    const markAllAsRead = useCallback(() => {
        dispatch(markAllNotificationsAsRead())
    }, [dispatch])

    const markAsRead = useCallback(
        (id: number) => {
            dispatch(markNotificationAsRead(String(id)))
        },
        [dispatch]
    )

    const markAsUnread = useCallback(
        (id: number) => {
            dispatch(markNotificationAsUnread(String(id)))
        },
        [dispatch]
    )

    const navigateToOrder = useCallback(
        (n: Notification) => {
            !n.readAt && markAsRead(n.id)
        },
        [markAsRead]
    )

    if (notifications.length > 0) {
        return (
            <div className={`${styles.events} ${isMobile && styles.mobile}`}>
                <div className={styles.list}>
                    {notifications.map(n => (
                        <div key={n.id} className={`${styles.row} ${!n.readAt && styles.unread}`}>
                            {n.additionalData && (
                                <Link to={getOrderLink(n.additionalData)} className={styles.text} onClick={() => navigateToOrder(n)}>
                                    {/* eslint-disable-next-line @typescript-eslint/naming-convention */}
                                    <span className={styles.description} dangerouslySetInnerHTML={{__html: n.description}} />
                                    <span className={styles.date}>{getFullDateFormat(new Date(n.createdAt))}</span>
                                </Link>
                            )}
                            <button
                                className={`${styles.toggleRead} ${!n.readAt && styles.unread}`}
                                onClick={() => {
                                    n.readAt ? markAsUnread(n.id) : markAsRead(n.id)
                                }}
                            />
                        </div>
                    ))}
                </div>
                <button className={styles.readAllButton} onClick={markAllAsRead}>
                    Прочитать все
                </button>
            </div>
        )
    }
    return <EmptyMessage message="На данный момент у Вас нет уведомлений" description="Все изменения статусов заказов будут появляться здесь" />
}
