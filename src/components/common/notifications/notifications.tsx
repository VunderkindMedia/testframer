import * as React from 'react'
import {useSelector} from 'react-redux'
import {useState, useCallback, useRef, useEffect} from 'react'
import {IAppState} from '../../../redux/root-reducer'
import {NotificationsModal} from './notifications-modal/notifications-modal'
import {NotificationButton} from './notification-button/notification-button'
import {useNews, useNotifications} from '../../../hooks/account'
import {useDocumentClick} from '../../../hooks/ui'
import {useAppContext} from '../../../hooks/app'
import {ModalOverlay} from '../../common/modal/modal-overlay/modal-overlay'
import styles from './notifications.module.css'

interface INotificationsProps {
    className?: string
}

export const Notifications = (props: INotificationsProps): JSX.Element => {
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const search = useSelector((state: IAppState) => state.router.location.search)
    const lastReadNewsId = useSelector((state: IAppState) => state.account.partner?.lastReadNewsId)

    const {className} = props
    const [isNotificationsOpened, setIsNotificationsOpened] = useState(false)
    const buttonRef = useRef<HTMLButtonElement>(null)
    const containerRef = useRef<HTMLDivElement>(null)

    const closeHandler = useCallback((e: MouseEvent) => {
        if (!buttonRef.current?.contains(e.target as Node) && !containerRef.current?.contains(e.target as Node)) {
            setIsNotificationsOpened(false)
        }
    }, [])

    useDocumentClick(closeHandler)
    const notifications = useNotifications()
    const news = useNews()
    const {isMobile} = useAppContext()

    const toggleNotifications = useCallback(() => {
        setIsNotificationsOpened(!isNotificationsOpened)
    }, [isNotificationsOpened])

    useEffect(() => {
        setIsNotificationsOpened(false)
    }, [pathname, search])

    return (
        <div className={className ?? ''}>
            <NotificationButton
                ref={buttonRef}
                hasNotifications={!!notifications.filter(n => !n.readAt).length || !!news.filter(n => n.id > (lastReadNewsId ?? 0)).length}
                isActive={isNotificationsOpened}
                className={styles.notificationButton}
                onClick={toggleNotifications}
            />
            {isNotificationsOpened && (
                <ModalOverlay className={styles.modalOverlay} isTransparent={true} isFullMode={true}>
                    <NotificationsModal
                        className={`${styles.modal} ${isMobile && styles.mobile}`}
                        ref={containerRef}
                        notifications={notifications}
                        news={news}
                        lastReadNewsId={lastReadNewsId ?? 0}
                    />
                </ModalOverlay>
            )}
        </div>
    )
}
