import * as React from 'react'
import {useEffect} from 'react'
import {useDispatch} from 'react-redux'
import {loadNews} from '../../../../redux/cms/actions'
import {getDateDDMMMMYYYYFormat} from '../../../../helpers/date'
import styles from './news.module.css'
import {EmptyMessage} from '../empty-message/empty-message'
import {News} from '../../../../model/framer/news'

interface INewsProps {
    news: News[]
}

export const NewsPanel = (props: INewsProps): JSX.Element | null => {
    const {news} = props
    const dispatch = useDispatch()

    useEffect(() => {
        !news && dispatch(loadNews())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch])

    if (news === null) {
        return null
    }

    if (news.length > 0) {
        return (
            <div className={styles.container}>
                {news.map(({id, title, publishedAt, content, images}) => {
                    return (
                        <div key={id} className={styles.news}>
                            <p className={styles.date}>{getDateDDMMMMYYYYFormat(new Date(publishedAt))} г.</p>
                            <p className={styles.title}>{title}</p>
                            {/* eslint-disable-next-line @typescript-eslint/naming-convention  */}
                            <div className={styles.content} dangerouslySetInnerHTML={{__html: content}} />
                            {images.length > 0 &&
                                images.map(({id, url, alternativeText}) => (
                                    <img key={id} src={url} alt={alternativeText} className={styles.image} />
                                ))}
                        </div>
                    )
                })}
            </div>
        )
    }

    return <EmptyMessage message="На данный момент нет обновлений" />
}
