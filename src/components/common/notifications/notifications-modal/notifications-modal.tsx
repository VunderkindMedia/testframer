import * as React from 'react'
import {useCallback, useState} from 'react'
import {Notification} from '../../../../model/framer/notifications/notification'
import {NewsPanel} from '../news/news'
import {Events} from '../events/events'
import {AnalyticsManager} from '../../../../analytics-manager'
import {useAppContext} from '../../../../hooks/app'
import styles from './notifications-modal.module.css'
import {News} from '../../../../model/framer/news'
import {useDispatch} from 'react-redux'
import {lastReadNews, loadPartner} from '../../../../redux/account/actions'

interface INotificationModalProps {
    notifications: Notification[]
    news: News[]
    className?: string
    lastReadNewsId: number
}

const NotificationsModal = React.forwardRef<HTMLDivElement, INotificationModalProps>((props: INotificationModalProps, ref) => {
    const {notifications, className, news, lastReadNewsId} = props
    const [activeTab, setActiveTab] = useState(1)
    const {isMobile} = useAppContext()
    const dispatch = useDispatch()
    const updateNews = useCallback(() => {
        dispatch(lastReadNews(news.length))
        dispatch(loadPartner())
    }, [dispatch])

    const changeTab = useCallback((e: React.MouseEvent, id: number) => {
        e.preventDefault()
        setActiveTab(id)

        if (id === 2) {
            updateNews()
            AnalyticsManager.trackEvent({
                name: 'Клик "Что нового?"',
                target: 'news-button'
            })
        }
    }, [])

    const tabs = [
        {id: 1, title: 'Оповещения', count: notifications.filter(n => !n.readAt).length},
        {id: 2, title: 'Что нового?', count: news.filter(n => n.id > lastReadNewsId).length}
    ]
    return (
        <div ref={ref} className={`${styles.modal} ${isMobile && styles.mobile} ${className ?? ''}`}>
            <div className={styles.tabs}>
                {tabs.map(tab => {
                    return (
                        <a
                            href=""
                            className={`${styles.tab} ${tab.id === activeTab && styles.active}`}
                            key={tab.id}
                            onClick={e => changeTab(e, tab.id)}>
                            {tab.title} {!!tab.count && <span className={styles.count}>{tab.count}</span>}
                        </a>
                    )
                })}
            </div>
            <div className={styles.content}>
                {activeTab === 1 && <Events notifications={notifications} />}
                {activeTab === 2 && <NewsPanel news={news} />}
            </div>
        </div>
    )
})

NotificationsModal.displayName = 'NotificationsModal'

export {NotificationsModal}
