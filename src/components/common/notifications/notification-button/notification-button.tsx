import * as React from 'react'
import {ReactComponent as NotificationIcon} from './resources/notification_icon.inline.svg'
import styles from './notification-button.module.css'

interface INotificationButtonProps {
    hasNotifications: boolean
    isActive: boolean
    className?: string
    onClick: () => void
}

const NotificationButton = React.forwardRef<HTMLButtonElement, INotificationButtonProps>((props: INotificationButtonProps, ref) => {
    const {hasNotifications, isActive, className, onClick} = props

    return (
        <button
            ref={ref}
            className={`${styles.button} ${hasNotifications && styles.withNotifications} ${isActive && styles.active} ${className ?? ''}`}
            onClick={onClick}>
            <NotificationIcon />
        </button>
    )
})

NotificationButton.displayName = 'NotificationButton'

export {NotificationButton}
