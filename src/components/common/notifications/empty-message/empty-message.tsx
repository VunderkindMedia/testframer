import * as React from 'react'
import {useAppContext} from '../../../../hooks/app'
import styles from './empty-message.module.css'

interface IEmptyMessage {
    message: string
    description?: string
}

export const EmptyMessage = (props: IEmptyMessage): JSX.Element => {
    const {message, description} = props
    const {isMobile} = useAppContext()
    return (
        <div className={`${styles.emptyMessage} ${isMobile && styles.mobile}`}>
            <div className={styles.icon}></div>
            <p className={styles.message}>{message}</p>
            {description && <p className={styles.description}>{description}</p>}
        </div>
    )
}
