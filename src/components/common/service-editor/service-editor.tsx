import * as React from 'react'
import {useCallback, useEffect, useRef, useState} from 'react'
import {Service} from '../../../model/framer/service'
import {PriceInput} from '../price-input/price-input'
import {Input} from '../input/input'
import {RemoveButton} from '../button/remove-button/remove-button'
import {useAppContext} from '../../../hooks/app'
import styles from './service-editor.module.css'
import Timeout = NodeJS.Timeout

interface IServiceEditorProps {
    service: Service
    onChange?: (service: Service) => void
    onRemove?: (serviceId: number) => void
    debounceTimeMs?: number
    isDisabled?: boolean
    className?: string
}

export function ServiceEditor(props: IServiceEditorProps): JSX.Element {
    const {service, onChange, onRemove, debounceTimeMs, isDisabled, className} = props
    const serviceFormRef = useRef<HTMLFormElement>(null)
    const serviceFormSubmitButtonRef = useRef<HTMLInputElement>(null)
    const [isAnnoyed, setIsAnnoyed] = useState(false)

    const timerRef = useRef<Timeout | null>(null)
    const nameInputRef = useRef<HTMLInputElement>(null)
    const priceInputRef = useRef<HTMLInputElement>(null)
    const {isMobile} = useAppContext()

    const onChangeHandler = useCallback((): void => {
        timerRef.current && clearTimeout(timerRef.current)

        timerRef.current = setTimeout(() => {
            if (!nameInputRef.current || !priceInputRef.current) {
                return
            }
            const srvc = service.clone
            srvc.hasPrice = service.hasPrice
            srvc.name = nameInputRef.current.value
            const priceStr = priceInputRef.current.value
            if (priceStr.length > 0) {
                srvc.price = +priceStr * 100
                srvc.hasPrice = true
            }
            setIsAnnoyed(true)
            onChange && onChange(srvc)
        }, debounceTimeMs ?? 0)
    }, [debounceTimeMs, onChange, service])

    useEffect(() => {
        return () => {
            timerRef.current && clearTimeout(timerRef.current)
        }
    }, [])

    return (
        <form
            ref={serviceFormRef}
            data-test="service-editor"
            data-id={service.id}
            onSubmit={e => e.preventDefault()}
            className={`${styles.serviceEditor} ${className && className} ${isMobile ? styles.mobile : ''}`}>
            <div className={styles.nameWrap}>
                <Input
                    ref={nameInputRef}
                    isDisabled={isDisabled}
                    defaultValue={service.name}
                    onChange={onChangeHandler}
                    pattern=".{5,}"
                    title="Минимальная длина пять символов"
                    isRequired={true}
                    isAnnoyed={isAnnoyed}
                    className={styles.input}
                    placeholder="Название услуги"
                />
            </div>
            <div className={styles.priceWrap}>
                <PriceInput
                    ref={priceInputRef}
                    className={styles.priceInput}
                    isDisabled={isDisabled}
                    value={service.hasPrice ? service.price / 100 : undefined}
                    min={0}
                    currency={'₽'}
                    isRequired={true}
                    isAnnoyed={isAnnoyed}
                    onChange={onChangeHandler}
                />
                <RemoveButton className={styles.removeButton} isDisabled={isDisabled} onClick={() => onRemove && onRemove(service.id)} />
            </div>
            <input
                ref={serviceFormSubmitButtonRef}
                type="submit"
                style={{
                    display: 'none'
                }}
            />
        </form>
    )
}
