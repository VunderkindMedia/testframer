import * as React from 'react'
import {useCallback} from 'react'
import {getAllDatesFromRange, getDateDDMMYYYYFormat, getWeekdayName, getTimeHHMMFormat} from '../../../helpers/date'
import {EventType} from '../../desktop/events-page/event-type/event-type'
import {CalendarEvent} from '../../../model/framer/calendar/calendar-event'
import {useAppContext} from '../../../hooks/app'
import styles from './calendar-list.module.css'

interface ICalendarListProps {
    startDate: Date
    endDate: Date
    events: Map<string, CalendarEvent[]>
    onClick?: (date: Date) => void
}

export function CalendarList(props: ICalendarListProps): JSX.Element {
    const {startDate, endDate, events, onClick} = props

    const dates = getAllDatesFromRange(startDate, endDate)

    const {isMobile} = useAppContext()

    const handleRowClick = useCallback(
        (date: Date) => {
            onClick && onClick(date)
        },
        [onClick]
    )

    return (
        <div className={`${styles.list} ${isMobile ? styles.mobile : ''}`}>
            <div className={`standard-table ${styles.table}`}>
                <table cellSpacing={0}>
                    <tbody>
                        {dates.map((date, index) => {
                            const dateStr = getDateDDMMYYYYFormat(date)
                            const dayEvents = events.get(dateStr) ?? []
                            const isLastDay = index === dates.length - 1

                            if (dayEvents.length > 1) {
                                const firstEvent = dayEvents[0]
                                const event = firstEvent.originalEvent

                                return (
                                    <React.Fragment key={dateStr}>
                                        <tr key={dateStr} onClick={() => handleRowClick(date)}>
                                            <td className={`${styles.dayNumber} ${isLastDay ? styles.lastRow : ''}`} rowSpan={dayEvents.length}>
                                                {date.getDate()}
                                            </td>
                                            <td className={`${styles.dayName} ${isLastDay ? styles.lastRow : ''}`} rowSpan={dayEvents.length}>
                                                {getWeekdayName(date)}
                                            </td>
                                            <td className={styles.type}>
                                                <EventType type={firstEvent.eventType} className={styles.eventType} />
                                            </td>
                                            {!isMobile && (
                                                <>
                                                    <td>{getTimeHHMMFormat(new Date(event.datetime))}</td>
                                                    <td>{event.client && event.client.address}</td>
                                                    <td>{event.client && event.client.name}</td>
                                                    <td>{event.client && event.client.phone}</td>
                                                </>
                                            )}
                                        </tr>
                                        {dayEvents.slice(1).map(e => {
                                            const event = e.originalEvent

                                            return (
                                                <tr key={event.id} onClick={() => handleRowClick(date)}>
                                                    <td className={styles.type}>
                                                        <EventType type={e.eventType} className={styles.eventType} />
                                                    </td>
                                                    {!isMobile && (
                                                        <>
                                                            <td>{getTimeHHMMFormat(new Date(event.datetime))}</td>
                                                            <td>{event.client && event.client.address}</td>
                                                            <td>{event.client && event.client.name}</td>
                                                            <td>{event.client && event.client.phone}</td>
                                                        </>
                                                    )}
                                                </tr>
                                            )
                                        })}
                                    </React.Fragment>
                                )
                            }

                            return (
                                <tr key={dateStr} onClick={() => handleRowClick(date)}>
                                    <td className={styles.dayNumber}>{date.getDate()}</td>
                                    <td>{getWeekdayName(date)}</td>
                                    <td className={styles.type}>
                                        {dayEvents[0]?.eventType && <EventType type={dayEvents[0].eventType} className={styles.eventType} />}
                                    </td>
                                    {!isMobile && <td colSpan={4}></td>}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
