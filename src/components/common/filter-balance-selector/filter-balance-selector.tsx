import * as React from 'react'
import {useCallback, useState, useRef} from 'react'
import {useAppContext} from '../../../hooks/app'
import {useDocumentClick} from '../../../hooks/ui'
import {ReactComponent as CacheIcon} from './resources/cache-icon.inline.svg'
import {ReactComponent as RoubleIcon} from './resources/rouble-icon.inline.svg'
import {ReactComponent as ArrowIcon} from './resources/arrow-icon.inline.svg'
import styles from './filter-balance-selector.module.css'
import {IOrdersFilterParams} from '../../../helpers/orders'

interface IBalanceModeProps {
    className?: string
    filterParams: IOrdersFilterParams
    onChangeFilterPrams: (params: IOrdersFilterParams) => void
}
export const ROUBLES_FILTER_ITEM = 'rubles'
export const FRAME_POINTS_FILTER_ITEM = 'framerPoints'

export const FilterBalanceSelector = (props: IBalanceModeProps): JSX.Element => {
    const {className, filterParams, onChangeFilterPrams} = props
    const {isMobile} = useAppContext()
    const [isOpened, setIsOpened] = useState(false)
    const ref = useRef<HTMLDivElement | null>(null)

    const setRoubleMode = useCallback(() => {
        onChangeFilterPrams({...filterParams, framePoints: ROUBLES_FILTER_ITEM})
    }, [onChangeFilterPrams])

    const setFrameMode = useCallback(() => {
        onChangeFilterPrams({...filterParams, framePoints: FRAME_POINTS_FILTER_ITEM})
    }, [onChangeFilterPrams])

    const allCashMode = useCallback(() => {
        onChangeFilterPrams({...filterParams, framePoints: ''})
    }, [onChangeFilterPrams])

    const toggleOpened = useCallback(() => {
        setIsOpened(flag => !flag)
    }, [])

    const closeSelector = useCallback(
        (e: MouseEvent): void => {
            if (ref.current && !ref.current.contains(e.target as HTMLDivElement)) {
                setIsOpened(false)
            }
        },
        [ref]
    )

    useDocumentClick(closeSelector)

    const roubleMode = (
        <button key="balance-mode" className={styles.button} onClick={setRoubleMode}>
            <RoubleIcon />
        </button>
    )

    const cashMode = (
        <button key="cash-mode" className={styles.button} onClick={setFrameMode}>
            <CacheIcon />
        </button>
    )
    const allMode = (
        <button key="all-mode" className={styles.button} onClick={allCashMode}>
            Все
        </button>
    )

    const options =
        filterParams.framePoints === ROUBLES_FILTER_ITEM
            ? [roubleMode, cashMode, allMode]
            : filterParams.framePoints === FRAME_POINTS_FILTER_ITEM
            ? [cashMode, roubleMode, allMode]
            : [allMode, roubleMode, cashMode]

    return (
        <div className={`${styles.container} ${className ?? ''}`}>
            <div ref={ref} className={`${styles.selector} ${isOpened && styles.opened} ${isMobile && styles.mobile}`} onClick={toggleOpened}>
                {options}
                <ArrowIcon className={`${styles.arrow} ${isOpened && styles.opened}`} />
            </div>
        </div>
    )
}
