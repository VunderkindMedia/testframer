import * as React from 'react'
import {useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {useState, useEffect} from 'react'
import {SearchInput} from '../search-input/search-input'
import {setClientSearchQuery} from '../../../redux/clients/actions'
import {Button} from '../button/button'
import {IAppState} from '../../../redux/root-reducer'
import {useGoTo} from '../../../hooks/router'
import {NAV_CLIENTS} from '../../../constants/navigation'
import {getClientsFilter} from '../../../helpers/clients'
import {useAppContext} from '../../../hooks/app'
import styles from './search-form.module.css'

interface ISearchFormProps {
    className?: string
}

export function SearchForm(props: ISearchFormProps): JSX.Element {
    const {className} = props
    const [query, setQuery] = useState<string>('')
    const searchQuery = useSelector((state: IAppState) => state.clients.searchQuery)
    const dispatch = useDispatch()
    const clientsOnPage = useSelector((state: IAppState) => state.clients.clientsOnPage)
    const goTo = useGoTo()
    const {isMobile} = useAppContext()

    useEffect(() => {
        setQuery(searchQuery)
    }, [searchQuery])

    const searchHandler = useCallback(
        (value: string) => {
            setQuery(value.trim())
            const filter = getClientsFilter(0, clientsOnPage)
            goTo(`${NAV_CLIENTS}${filter}`)
            dispatch(setClientSearchQuery(value))
        },
        [goTo, dispatch, clientsOnPage]
    )

    const formHandler = useCallback(
        (e: React.SyntheticEvent<HTMLFormElement>) => {
            e.preventDefault()
            searchHandler(query)
        },
        [searchHandler, query]
    )

    const searchInputHandler = useCallback(
        (value: string) => {
            searchHandler(value)
        },
        [searchHandler]
    )

    const clearHandler = useCallback(() => {
        searchHandler('')
    }, [searchHandler])

    return (
        <form className={`${styles.form} ${className ?? ''}`} onSubmit={formHandler}>
            <SearchInput placeholder="Поиск" value={query} onChange={searchInputHandler} onClear={clearHandler} />
            {!isMobile && (
                <Button buttonStyle="with-border" type="submit" className={styles.button}>
                    Найти
                </Button>
            )}
        </form>
    )
}
