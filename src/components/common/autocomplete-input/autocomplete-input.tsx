import * as React from 'react'
import {useCallback, useEffect, useRef, useState} from 'react'
import {OpenStates} from '../../../model/open-states'
import {ListItem} from '../../../model/list-item'
import Timer = NodeJS.Timer
import {clamp} from '../../../helpers/number'
import {arraysHaveSameItems} from '../../../helpers/array'
import {getClassNames} from '../../../helpers/css'
import {useDocumentClick} from '../../../hooks/ui'
import {useAppContext} from '../../../hooks/app'
import InputStyles from '../input/input.module.css'
import styles from './autocomplete-input.module.css'

interface IAutocompleteInputProps<T> {
    defaultValue?: string
    isDisabled?: boolean
    getAutocompleteItemsFunc?: (query: string) => Promise<ListItem<T>[]>
    onChange?: (value: string) => void
    onBlur?: (value: string) => void
    onSelect?: (item: ListItem<T>) => void
    dataTest?: string
    placeholder?: string
    shouldStretchItems?: boolean
    isMultiline?: boolean
    className?: string
    error?: string | null
}

export function AutocompleteInput<T>(props: IAutocompleteInputProps<T>): JSX.Element {
    const {
        defaultValue,
        isDisabled,
        onSelect,
        onBlur,
        getAutocompleteItemsFunc,
        dataTest,
        placeholder,
        className,
        shouldStretchItems = false,
        isMultiline = false,
        error = null
    } = props

    const containerRef = useRef<HTMLDivElement>(null)
    const itemsRef = useRef<HTMLDivElement>(null)
    const inputRef = useRef<HTMLInputElement>(null)
    const timerRef = useRef<Timer | null>(null)
    const [autocompleteItems, setAutocompleteItems] = useState<ListItem<T>[]>([])
    const [autocompleteItemIndex, setAutocompleteItemIndex] = useState(-1)
    const [openState, setOpenState] = useState<OpenStates>(OpenStates.Closed)
    const [inputQuery, setInputQuery] = useState(defaultValue ?? '')
    const [maxWidth, setMaxWidth] = useState('')

    const select = useCallback(
        (item: ListItem<T>): void => {
            setOpenState(OpenStates.Closed)
            setAutocompleteItems([])

            inputRef.current && (inputRef.current.value = item.name)

            onSelect && onSelect(item)
        },
        [onSelect]
    )

    const onClickHandler = useCallback(
        (e: MouseEvent): void => {
            const target = e.target as HTMLElement
            if (containerRef.current && containerRef.current.contains(target)) {
                if (openState !== OpenStates.Opened && autocompleteItems.length > 0) {
                    setOpenState(OpenStates.Opened)
                }
                if (target.dataset.index === undefined) {
                    return
                }
                const index = +target.dataset.index
                !isNaN(index) && select(autocompleteItems[index])
            } else {
                setOpenState(OpenStates.Closed)
            }
        },
        [autocompleteItems, openState, select]
    )

    useDocumentClick(onClickHandler)
    const {isMobile} = useAppContext()

    useEffect(() => {
        if (!containerRef.current) {
            return
        }
        setMaxWidth(`${window.innerWidth - containerRef.current.getBoundingClientRect().x - (isMobile ? 24 : 36)}px`)
    }, [isMobile])

    useEffect(() => {
        setAutocompleteItemIndex(-1)
    }, [autocompleteItems])

    useEffect(() => {
        if (!inputRef.current) {
            return
        }
        if (defaultValue === undefined) {
            return
        }

        inputRef.current.value = defaultValue
    }, [defaultValue])

    const resetTimer = (): void => {
        timerRef.current && clearTimeout(timerRef.current)
    }

    const updateItems = async (query: string): Promise<void> => {
        if (!getAutocompleteItemsFunc) {
            return
        }

        const listItems = await getAutocompleteItemsFunc(query)

        const queryItemIndex = listItems.findIndex(item => item.name.toLowerCase() === query.toLowerCase())

        if (queryItemIndex !== -1 && listItems.length === 1) {
            select(listItems[queryItemIndex])
            return
        }

        if (!arraysHaveSameItems(listItems, autocompleteItems, (i1, i2) => i1.id === i2.id)) {
            setAutocompleteItems(listItems)
            setOpenState(listItems.length > 0 ? OpenStates.Opened : OpenStates.Closed)
        }
    }

    const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        resetTimer()

        const value = e.target.value
        setInputQuery(value)
        props.onChange && props.onChange(value)

        timerRef.current = setTimeout(() => {
            void updateItems(value)
        }, 1000)
    }

    const onBlurHandler = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            onBlur && onBlur(e.target.value)
        },
        [onBlur]
    )

    const updateCurrentItem = (newAutocompleteItemIndex: number): void => {
        let newIndex = clamp(newAutocompleteItemIndex, -2, autocompleteItems.length)
        if (newIndex === autocompleteItems.length) {
            newIndex = -1
        } else if (newIndex === -2) {
            newIndex = autocompleteItems.length - 1
        }

        if (!inputRef.current) {
            return
        }

        if (0 <= newIndex && newIndex < autocompleteItems.length) {
            inputRef.current.value = autocompleteItems[newIndex].name
        } else if (newIndex === -1) {
            inputRef.current.value = inputQuery
        }
        setAutocompleteItemIndex(newIndex)

        const item = itemsRef.current?.querySelector(`[data-index="${newIndex}"]`)
        if (item && itemsRef.current) {
            const containerRect = itemsRef.current.getBoundingClientRect()
            const itemRect = item.getBoundingClientRect()
            const isVisible = containerRect.top <= itemRect.top && itemRect.bottom <= containerRect.bottom

            !isVisible && item.scrollIntoView({block: 'nearest'})
        }
    }

    const onKeyPress = async (e: React.KeyboardEvent<HTMLInputElement>): Promise<void> => {
        if (e.key === 'ArrowDown') {
            e.preventDefault()
            updateCurrentItem(autocompleteItemIndex + 1)
        } else if (e.key === 'ArrowUp') {
            e.preventDefault()
            updateCurrentItem(autocompleteItemIndex - 1)
        } else if (e.key === 'Enter') {
            e.preventDefault()

            if (inputRef.current) {
                const query = inputRef.current.value
                resetTimer()
                await updateItems(query)
            }
        }
    }

    return (
        <div className={`${className ?? ''}`}>
            <div
                className={getClassNames(`${styles.autocompleteInput} `, {
                    [styles.opened]: openState === OpenStates.Opened,
                    [styles.disabled]: !!isDisabled,
                    [styles.stretchItems]: shouldStretchItems,
                    [styles.multiline]: isMultiline
                })}
                ref={containerRef}>
                <input
                    data-test={dataTest}
                    type="text"
                    ref={inputRef}
                    disabled={isDisabled}
                    placeholder={placeholder ?? ''}
                    className={`${InputStyles.input} ${error === null ? '' : error ? styles.error : styles.success}`}
                    defaultValue={defaultValue ?? ''}
                    onKeyPress={onKeyPress}
                    onFocus={async () => {
                        if (!inputRef.current) {
                            return
                        }
                        const query = inputRef.current.value
                        if (query.length !== 0 && autocompleteItems.length === 0) {
                            resetTimer()
                            await updateItems(query)
                        }
                    }}
                    onKeyDown={onKeyPress}
                    onChange={onChange}
                    onBlur={onBlurHandler}
                />
                <div ref={itemsRef} className={styles.items} style={{maxWidth: maxWidth}}>
                    {autocompleteItems.map((item, idx) => (
                        <div
                            key={idx}
                            data-index={idx}
                            data-test={`autocomplete-input-suggestion-item-${idx}`}
                            className={`${styles.item} ${idx === autocompleteItemIndex && styles.selected}`}>
                            <p data-index={idx}>{item.name}</p>
                        </div>
                    ))}
                </div>
            </div>
            {error && <span className={styles.error}>{error}</span>}
        </div>
    )
}
