import * as React from 'react'
import {useCallback, useRef, useEffect} from 'react'
import Timeout = NodeJS.Timeout
import styles from './textarea.module.css'

interface ITextarea {
    value?: string
    placeholder?: string
    isRequired?: boolean
    isReadonly?: boolean
    isDisabled?: boolean
    isAnnoyed?: boolean
    maxlength?: number
    rows?: number
    className?: string
    debounceTimeMs?: number
    onChange?: (value: string, isValid: boolean) => void
}

export function Textarea(props: ITextarea): JSX.Element {
    const {value, placeholder, isRequired, isReadonly, isDisabled, isAnnoyed, maxlength, rows, className, debounceTimeMs, onChange} = props
    const textareaRef = useRef<HTMLTextAreaElement>(null)
    const timerRef = useRef<Timeout | null>(null)

    const onChangeHandler = useCallback(
        (e: React.ChangeEvent<HTMLTextAreaElement>) => {
            timerRef.current && clearTimeout(timerRef.current)

            const value = e.target.value
            timerRef.current = setTimeout(() => {
                onChange && onChange(value, Boolean(textareaRef.current?.checkValidity()))
            }, debounceTimeMs ?? 0)
        },
        [onChange, debounceTimeMs]
    )

    useEffect(() => {
        return () => {
            timerRef.current && clearTimeout(timerRef.current)
        }
    }, [])

    useEffect(() => {
        if (!textareaRef.current || value === null || value === undefined) {
            return
        }

        textareaRef.current.value = value
    }, [value])

    return (
        <textarea
            ref={textareaRef}
            required={isRequired ?? false}
            maxLength={maxlength}
            rows={rows}
            placeholder={placeholder ?? ''}
            readOnly={isReadonly}
            disabled={isDisabled}
            className={`${styles.textarea} ${isAnnoyed && styles.annoyed} ${className}`}
            onChange={onChangeHandler}></textarea>
    )
}
