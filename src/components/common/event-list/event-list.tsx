import * as React from 'react'
import {useAppContext} from '../../../hooks/app'
import {getTimeHHMMFormat} from '../../../helpers/date'
import {CalendarEvent} from '../../../model/framer/calendar/calendar-event'
import {CalendarEventTypes} from '../../../model/framer/calendar/calendar-event-types'
import {EventType} from '../../desktop/events-page/event-type/event-type'
import styles from './event-list.module.css'

interface IEventListProps {
    events: CalendarEvent[]
    type: CalendarEventTypes
}

export const EventList = (props: IEventListProps): JSX.Element | null => {
    const {events, type} = props

    const {isMobile} = useAppContext()

    if (events.length === 0) {
        return null
    }

    return (
        <div className={`${styles.eventList} ${isMobile ? styles.mobile : ''} `}>
            <EventType type={type} className={styles.type} />
            {events.map(({originalEvent}, index) => {
                const {datetime, client} = originalEvent
                return (
                    <p key={index} className={styles.row}>
                        <span className={styles.time}>{getTimeHHMMFormat(new Date(datetime))}</span>
                        {client && (
                            <>
                                <span className={styles.address}>{client.address}</span>
                                <span className={styles.client}>{client.name}</span>
                                <span className={styles.phone}>{client.phone}</span>
                            </>
                        )}
                    </p>
                )
            })}
        </div>
    )
}
