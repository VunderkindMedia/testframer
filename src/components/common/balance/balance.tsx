import * as React from 'react'
import {RUB_SYMBOL} from '../../../constants/strings'
import {getCurrency} from '../../../helpers/currency'
import {ReactComponent as CacheIcon} from '../balance-mode-selector/resources/cache-icon.inline.svg'
import styles from './balance.module.css'

interface IBalanceProps {
    value: number
    className?: string
    isHideSymbol?: boolean
    isFrameSymbol?: boolean
}

export const Balance = (props: IBalanceProps): JSX.Element => {
    const {value, className, isHideSymbol = false, isFrameSymbol = false} = props

    return (
        <span className={`${styles.balance} ${className ?? ''} ${value < 0 && styles.negative}`}>
            {getCurrency(value)} {!isHideSymbol && <span>{RUB_SYMBOL}</span>}{' '}
            {isFrameSymbol && (
                <span>
                    <CacheIcon />
                </span>
            )}
        </span>
    )
}
