import * as React from 'react'
import {useCallback, useEffect} from 'react'
import {BottomSheet} from '../bottom-sheet/bottom-sheet'
import {ServicesCard} from '../services-card/services-card'
import {GoodsCard} from '../goods-card/goods-card'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {ConfigContexts} from '../../../model/config-contexts'
import {useAddGood2, useUpdateCurrentOrder, useSaveOrder} from '../../../hooks/orders'
import {useSetConfigContext} from '../../../hooks/builder'
import {useLoadConstructionAvailableServices} from '../../../hooks/services'

export function ConstructionGoodsAndServicesSheets(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const isVisibleServiceCard = useSelector((state: IAppState) => state.builder.configContext === ConfigContexts.Services)
    const isVisibleGoodsCard = useSelector((state: IAppState) => state.builder.configContext === ConfigContexts.Goods)
    const factoryGoodGroups = useSelector((state: IAppState) => state.goods.factoryGoodGroups)
    const allFactoryGoods = useSelector((state: IAppState) => state.goods.factoryGoods)
    const partnerGoodGroups = useSelector((state: IAppState) => state.goods.partnerGoodGroups)
    const allPartnerGoods = useSelector((state: IAppState) => state.goods.partnerGoods)
    const constructionAvailableServices = useSelector((state: IAppState) => state.services.constructionAvailableServices)

    const addGood2 = useAddGood2()
    const setConfigContext = useSetConfigContext()
    const saveOrder = useSaveOrder()
    const updateOrder = useUpdateCurrentOrder()
    const loadConstructionAvailableServices = useLoadConstructionAvailableServices()

    useEffect(() => {
        if (!currentOrder?.id || !currentConstruction?.id || !isVisibleServiceCard) {
            return
        }
        if (constructionAvailableServices[currentConstruction.id]) {
            return
        }
        loadConstructionAvailableServices(currentOrder.id, currentConstruction.id)
    }, [currentOrder?.id, currentConstruction?.id, isVisibleServiceCard, constructionAvailableServices, loadConstructionAvailableServices])

    const hideGoodsCard = useCallback(() => {
        isVisibleGoodsCard && setConfigContext(null)
        currentOrder?.changed && saveOrder(currentOrder)
    }, [currentOrder, isVisibleGoodsCard, setConfigContext, saveOrder])

    const hideServicesCard = useCallback(() => {
        isVisibleServiceCard && setConfigContext(null)
        currentOrder?.changed && saveOrder(currentOrder)
    }, [currentOrder, isVisibleServiceCard, setConfigContext, saveOrder])

    if (!currentOrder) {
        return null
    }

    const constructionIndex = currentConstruction ? currentOrder.constructions.findIndex(c => c.id === currentConstruction.id) + 1 : -1

    const constructionServices = currentOrder.services.filter(s => s.constructionId === currentConstruction?.id)
    const orderServices = currentOrder.services.filter(s => s.constructionId !== currentConstruction?.id)
    const constructionGoods = currentOrder.goods.filter(g => g.constructionId === currentConstruction?.id)
    const orderGoods = currentOrder.goods.filter(g => g.constructionId !== currentConstruction?.id)

    return (
        <>
            <BottomSheet isVisible={isVisibleServiceCard} onHide={hideServicesCard}>
                <ServicesCard
                    title={`Услуги к конструкции ${constructionIndex}`}
                    isDisabled={currentOrder.readonly}
                    onClickHeader={() => setConfigContext(null)}
                    constructionId={currentConstruction?.id}
                    services={constructionServices}
                    availableServices={
                        currentConstruction && constructionAvailableServices[currentConstruction.id]
                            ? constructionAvailableServices[currentConstruction.id]
                            : []
                    }
                    onChange={(services, action) => {
                        const orderClone = currentOrder.clone
                        orderClone.services = [...orderServices, ...services]
                        orderClone.addAction(action)
                        updateOrder(orderClone)
                    }}
                />
            </BottomSheet>
            <BottomSheet isVisible={isVisibleGoodsCard} onHide={hideGoodsCard}>
                <GoodsCard
                    title={`Доп. материалы к конструкции ${constructionIndex}`}
                    isDisabled={currentOrder.readonly}
                    constructionId={currentConstruction?.id}
                    onClickHeader={() => setConfigContext(null)}
                    goods={constructionGoods}
                    allFactoryGoodGroups={factoryGoodGroups}
                    allFactoryGoods={allFactoryGoods}
                    allPartnerGoodGroups={partnerGoodGroups}
                    allPartnerGoods={allPartnerGoods}
                    onAddGood={good => addGood2(currentOrder, good)}
                    onChange={(goods, action) => {
                        const orderClone = currentOrder.clone
                        orderClone.goods = [...orderGoods, ...goods]
                        orderClone.addAction(action)
                        updateOrder(orderClone)
                    }}
                />
            </BottomSheet>
        </>
    )
}
