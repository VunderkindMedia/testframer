import * as React from 'react'
import styles from './selector-with-icon.module.css'
import {ArrowTooltip} from '../arrow-tooltip/arrow-tooltip'
import {getClassNames} from '../../../helpers/css'

interface ISelectorIcon {
    src: string
    hint: string
    onClick?: () => void
    selected?: boolean
}

interface ISelectorWithIconProps {
    icons: ISelectorIcon[]
}

export function SelectorWithIcon(props: ISelectorWithIconProps): JSX.Element {
    const {icons} = props

    return (
        <div className={styles.selector}>
            {icons.map(icon => (
                <ArrowTooltip key={icon.hint} title={icon.hint} placement="bottom">
                    <div
                        className={getClassNames(`${styles.icon} blue-hover`, {selected: Boolean(icon.selected)})}
                        onClick={e => {
                            e.stopPropagation()
                            icon.onClick && icon.onClick()
                        }}>
                        <img src={icon.src} alt="" draggable={false} />
                    </div>
                </ArrowTooltip>
            ))}
        </div>
    )
}
