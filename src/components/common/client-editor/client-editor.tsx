import * as React from 'react'
import {Client} from '../../../model/framer/client'
import {useCallback, useState, useRef, useEffect} from 'react'
import {WithTitle} from '../with-title/with-title'
import {Input} from '../input/input'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'
import styles from './client-editor.module.css'
import Timeout = NodeJS.Timeout

interface IClientEditorProps {
    client: Client
    className?: string
    onChange: (client: Client) => void
}

export function ClientEditor(props: IClientEditorProps): JSX.Element {
    const {client, className, onChange} = props
    const formRef = useRef<HTMLFormElement | null>(null)
    const timerRef = useRef<Timeout | null>(null)
    const [isFormValid, setIsFormValid] = useState(true)
    const nameInputRef = useRef<HTMLInputElement>(null)
    const phoneInputRef = useRef<HTMLInputElement>(null)
    const addressInputRef = useRef<HTMLInputElement>(null)
    const emailInputRef = useRef<HTMLInputElement>(null)

    useEffect(() => {
        return () => {
            timerRef.current && clearTimeout(timerRef.current)
        }
    }, [])

    const handleClientUpdate = useCallback(() => {
        timerRef.current && clearTimeout(timerRef.current)

        if (!formRef.current || !formRef.current.checkValidity()) {
            return
        }

        timerRef.current = setTimeout(() => {
            if (!formRef.current || !nameInputRef.current || !phoneInputRef.current || !addressInputRef.current || !emailInputRef.current) {
                return
            }

            const isValid = formRef.current.checkValidity()
            setIsFormValid(isValid)

            if (!isValid) {
                return
            }

            const clientClone = client.clone
            clientClone.name = nameInputRef.current.value.trim()
            clientClone.email = emailInputRef.current.value.trim()
            clientClone.phone = phoneInputRef.current.value.trim()
            clientClone.address = addressInputRef.current.value.trim()

            onChange(clientClone)
        }, DEFAULT_DEBOUNCE_TIME_MS)
    }, [onChange, client])

    const {name, phone, email, address} = client

    return (
        <form ref={formRef} className={`${styles.form} ${className ?? ''}`} onSubmit={e => e.preventDefault()}>
            <WithTitle title="Имя" className={styles.inputGroup}>
                <Input
                    ref={nameInputRef}
                    className={styles.input}
                    placeholder="Иванов Иван Иванович"
                    isRequired={true}
                    isAnnoyed={!isFormValid}
                    title="Минимальная длина имени два символа"
                    pattern=".{2,}"
                    value={name}
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    onChange={handleClientUpdate}
                />
            </WithTitle>
            <WithTitle title="Телефон" className={styles.inputGroup}>
                <Input
                    ref={phoneInputRef}
                    className={styles.input}
                    title="Неверный формат номера"
                    placeholder="+79990000000"
                    value={phone}
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    onChange={handleClientUpdate}
                />
            </WithTitle>
            <WithTitle title="Эл. почта" className={styles.inputGroup}>
                <Input
                    ref={emailInputRef}
                    className={styles.input}
                    type="email"
                    placeholder="ivan@mail.ru"
                    isAnnoyed={!isFormValid}
                    value={email}
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    onChange={handleClientUpdate}
                />
            </WithTitle>
            <WithTitle title="Адрес" className={styles.inputGroup}>
                <Input
                    ref={addressInputRef}
                    className={styles.input}
                    placeholder="Адрес"
                    value={address}
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    onChange={handleClientUpdate}
                />
            </WithTitle>
        </form>
    )
}
