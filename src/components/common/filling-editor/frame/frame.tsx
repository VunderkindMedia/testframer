import * as React from 'react'
import {useCallback} from 'react'
import {ReactComponent as FrameIcon} from './resources/frame_icon.inline.svg'
import {ISolidFillingPart} from '../../../../model/framer/filling-formula'
import styles from './frame.module.css'

interface IFrameProps {
    fillingPart: ISolidFillingPart
    isSelected?: boolean
    className?: string
    onSelect?: (part: ISolidFillingPart) => void
}

export const Frame = (props: IFrameProps): JSX.Element => {
    const {fillingPart, isSelected, className, onSelect} = props

    const onFillingPartClick = useCallback(() => {
        onSelect && onSelect(fillingPart)
    }, [fillingPart, onSelect])

    return <FrameIcon className={`${styles.frame} ${className && className} ${isSelected && styles.selected}`} onClick={onFillingPartClick} />
}
