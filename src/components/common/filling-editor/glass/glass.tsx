import * as React from 'react'
import {useCallback} from 'react'
import {ReactComponent as GlassIcon} from './resources/glass_icon.inline.svg'
import {ISolidFillingPart} from '../../../../model/framer/filling-formula'
import styles from './glass.module.css'

interface IGlassProps {
    fillingPart: ISolidFillingPart
    isSelected?: boolean
    className?: string
    onSelect?: (part: ISolidFillingPart) => void
}

export const Glass = (props: IGlassProps): JSX.Element => {
    const {fillingPart, isSelected = false, className, onSelect} = props

    const onFillingPartClick = useCallback(() => {
        onSelect && onSelect(fillingPart)
    }, [fillingPart, onSelect])

    return <GlassIcon className={`${styles.glass} ${className && className} ${isSelected && styles.selected}`} onClick={onFillingPartClick} />
}
