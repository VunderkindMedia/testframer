import * as React from 'react'
import {useCallback, useEffect, useMemo, useState} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {WithTitle} from '../with-title/with-title'
import {Select} from '../select/select'
import {Checkbox} from '../checkbox/checkbox'
import {ListItem} from '../../../model/list-item'
import {SolidFilling} from '../../../model/framer/solid-filling'
import {ConfigContexts} from '../../../model/config-contexts'
import {FillingPreview} from './filling-preview/filling-preview'
import {FRAME_WIDTH_ITEMS, GLASS_TYPES_ITEMS, TINT_FILM_ITEMS} from '../../../constants/filling-settings'
import {ISolidFillingPart, SolidFillingPartType} from '../../../model/framer/filling-formula'
import {useSetConfigContext, useSetFillingVendorCode} from '../../../hooks/builder'
import {getThicknessArray} from '../filling-selector/filling-selector'
import styles from './filling-editor.module.css'
import {FactoryTypes} from '../../../model/framer/factory-types'

export const FillingEditor = (): JSX.Element | null => {
    const solidFilling = useSelector((state: IAppState) =>
        state.orders.currentProduct?.isEmpty ? state.orders.currentProduct.frame.innerFillings[0].solid : state.builder.currentContext
    )
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)
    const currentProduct = useSelector((state: IAppState) => {
        return state.builder.configContext === ConfigContexts.ConstructionParams ? state.orders.genericProduct : state.orders.currentProduct
    })

    const [fillingPart, setFillingPart] = useState<ISolidFillingPart>()
    const [selectedGlass, setSelectedGlass] = useState<ListItem<string> | undefined>()
    const [selectedTintFilm, setSelectedTintFilm] = useState<ListItem<string> | undefined>()
    const [selectedFrameWidth, setSelectedFrameWidth] = useState<ListItem<string> | undefined>()
    const [hasArgon, setHasArgon] = useState(false)
    const [hasPvc, setHasPvc] = useState(false)

    const setFillingVendorCode = useSetFillingVendorCode()
    const setConfigContext = useSetConfigContext()
    const onPartTypeChange = useCallback(
        (item: ListItem<string>) => {
            if (!fillingPart) {
                return
            }
            const updatedFillingPart = {...fillingPart, value: item.id}
            setFillingVendorCode(updatedFillingPart)
        },
        [fillingPart, setFillingVendorCode]
    )

    const onFillingPartSelect = useCallback((part: ISolidFillingPart) => {
        setFillingPart(part)
    }, [])

    const onTintFilmSelect = useCallback(
        (item: ListItem<string>) => {
            if (!fillingPart) {
                return
            }
            const updatedFillingPart = {...fillingPart, tintFilm: item.id}
            setFillingVendorCode(updatedFillingPart)
        },
        [fillingPart]
    )

    const onOptionsChange = useCallback(
        (isChecked: boolean, key: string) => {
            if (!(solidFilling instanceof SolidFilling)) {
                return
            }
            const frame = solidFilling.formula.find(f => f.type === SolidFillingPartType.Frame)
            if (!frame) {
                return
            }
            const updatedFillingPart = {...frame, [key]: isChecked}
            setFillingVendorCode(updatedFillingPart)
        },
        [solidFilling, setFillingVendorCode]
    )

    const onGlassColorClick = useCallback(
        (e: React.SyntheticEvent) => {
            e.preventDefault()
            setConfigContext(ConfigContexts.GlassColorEditor)
        },
        [setConfigContext]
    )

    useEffect(() => {
        if (!fillingPart) {
            return
        }
        if (fillingPart.type === SolidFillingPartType.Glass) {
            setSelectedGlass(GLASS_TYPES_ITEMS.find(g => g.id === fillingPart.value))
        } else if (fillingPart.type === SolidFillingPartType.Frame) {
            setSelectedFrameWidth(FRAME_WIDTH_ITEMS.find(f => f.id === fillingPart.value))
        }
    }, [fillingPart])

    useEffect(() => {
        if (!(solidFilling instanceof SolidFilling)) {
            return
        }
        const formula = solidFilling.formula
        const glass = formula.find(part => part.id === fillingPart?.id)
        const frame = formula.find(f => f.type === SolidFillingPartType.Frame)
        setSelectedTintFilm(TINT_FILM_ITEMS.find(tint => tint.id === glass?.tintFilm))
        setHasArgon(frame?.hasArgon || false)
        setHasPvc(frame?.hasPvc || false)
        if (!fillingPart) {
            setFillingPart(formula[0])
        } else {
            const index = formula.findIndex(f => f.id === fillingPart.id)
            if (index !== -1) {
                formula[index].value !== fillingPart.value && setFillingPart(formula[index])
            }
        }
    }, [solidFilling, fillingPart])

    const fillings = useMemo(() => currentProduct?.availableFillings ?? [], [currentProduct])

    if (!(solidFilling instanceof SolidFilling) || configContext !== ConfigContexts.FillingEditor) {
        return null
    }

    const filteredFillings = fillings.filter(filling => filling.group === 'Стеклопакеты')
    const thicknessArray = getThicknessArray(filteredFillings)

    return (
        <div className={styles.fillingEditor}>
            <p className={styles.name}>{solidFilling.filling?.name}</p>
            <p className={styles.vendorCode}>{solidFilling.fillingVendorCode}</p>
            <p className={styles.thickness}>Допустимые толщины заполнений: {thicknessArray.join(', ')}</p>
            {fillingPart && fillingPart.type === SolidFillingPartType.Frame ? (
                <WithTitle title="Толщина рамки" className={styles.selector}>
                    <Select items={FRAME_WIDTH_ITEMS} selectedItem={selectedFrameWidth} onSelect={onPartTypeChange} />
                </WithTitle>
            ) : (
                <>
                    <WithTitle title="Стекло" className={styles.selector}>
                        <Select items={GLASS_TYPES_ITEMS} selectedItem={selectedGlass} onSelect={onPartTypeChange} />
                    </WithTitle>
                    {(fillingPart?.id === 0 || fillingPart?.id === solidFilling.formula.length - 1) && (
                        <WithTitle title="Тонировочная пленка" className={styles.selector}>
                            <Select items={TINT_FILM_ITEMS} selectedItem={selectedTintFilm} onSelect={onTintFilmSelect} />
                        </WithTitle>
                    )}
                </>
            )}
            {solidFilling.filling?.isMultifunctional && (
                <WithTitle title="Цвет стекла" className={styles.glassColorSelector}>
                    <a href="#" className={styles.glassColor} onClick={onGlassColorClick}>
                        {solidFilling.glassColor}
                    </a>
                </WithTitle>
            )}
            <FillingPreview className={styles.preview} selectedPart={fillingPart} filling={solidFilling} onSelect={onFillingPartSelect} />
            <Checkbox
                className={styles.checkbox}
                title="Заполнение аргоном"
                isChecked={hasArgon}
                onChange={value => onOptionsChange(value, 'hasArgon')}
            />
            {currentDealer?.factoryId !== FactoryTypes.Moscow && (
                <Checkbox
                    className={styles.checkbox}
                    title="ПВХ рамка"
                    isChecked={hasPvc}
                    onChange={value => onOptionsChange(value, 'hasPvc')}
                />
            )}
        </div>
    )
}
