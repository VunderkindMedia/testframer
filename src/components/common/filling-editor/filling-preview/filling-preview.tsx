import * as React from 'react'
import {Glass} from '../glass/glass'
import {Frame} from '../frame/frame'
import {SolidFilling} from '../../../../model/framer/solid-filling'
import {SolidFillingPartType, ISolidFillingPart} from '../../../../model/framer/filling-formula'
import styles from './filling-preview.module.css'

interface IFillingPreviewProps {
    filling: SolidFilling
    selectedPart?: ISolidFillingPart
    className?: string
    onSelect?: (part: ISolidFillingPart) => void
}

export const FillingPreview = (props: IFillingPreviewProps): JSX.Element => {
    const {filling, selectedPart, className, onSelect} = props
    const formula = filling.formula

    return (
        <div className={`${styles.preview} ${className && className}`}>
            <div className={styles.image}>
                {formula.map(part =>
                    part.type === SolidFillingPartType.Frame ? (
                        <Frame
                            key={part.id}
                            className={styles.element}
                            isSelected={part.id === selectedPart?.id}
                            fillingPart={part}
                            onSelect={onSelect}
                        />
                    ) : (
                        <Glass
                            key={part.id}
                            className={styles.element}
                            isSelected={part.id === selectedPart?.id}
                            fillingPart={part}
                            onSelect={onSelect}
                        />
                    )
                )}
            </div>
            <p className={styles.label}>
                {formula.map((f, index) => (
                    <span key={`${filling.fillingVendorCode}-${f.value}-${index}`}>{f.value}</span>
                ))}
            </p>
        </div>
    )
}
