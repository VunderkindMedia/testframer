import * as React from 'react'
import {useCallback} from 'react'
import {HandleTypes} from '../../../model/framer/handle-types'
import {HandleColors} from '../../../model/framer/handle-colors'
import {Handle} from '../../../model/handle'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {FactoryTypes} from '../../../model/framer/factory-types'
import {useSetHandle} from '../../../hooks/builder'
import macoHarmonySilver from './resources/maco/harmony/silver.png'
import macoHarmonyChampagne from './resources/maco/harmony/champagne.png'
import macoHarmonyBrown from './resources/maco/harmony/brown.png'
import macoHarmonyBronze from './resources/maco/harmony/bronze.png'
import macoHarmonyTitanium from './resources/maco/harmony/titanium.png'
import macoHarmonyWhite from './resources/maco/harmony/white.png'
import macoRhapsodySilver from './resources/maco/rhapsody/silver.png'
import macoRhapsodyChampagne from './resources/maco/rhapsody/champagne.png'
import macoRhapsodyBrown from './resources/maco/rhapsody/brown.png'
import macoRhapsodyBronze from './resources/maco/rhapsody/bronze.png'
import macoRhapsodyTitanium from './resources/maco/rhapsody/titanium.png'
import merkurijWhite from './resources/merkurij_white.png'
import rotoLineBronze from './resources/roto/line/bronze.png'
import rotoLineBrown from './resources/roto/line/brown.png'
import rotoLineSilver from './resources/roto/line/silver.png'
import rotoLineChampagne from './resources/roto/line/champagne.png'
import rotoLineTitan from './resources/roto/line/titan.png'
import rotoLineWhite from './resources/roto/line/white.png'
import {Dealer} from '../../../model/framer/dealer'
import {getClassNames} from '../../../helpers/css'
import {Furniture} from '../../../model/framer/furniture'
import {FURNITURE_ROTO} from '../../../constants/furniture-templates'
import {useAppContext} from '../../../hooks/app'
import styles from './handle-selector.module.css'

const HARMONY_HANDLES = [
    {
        type: HandleTypes.MacoHarmony,
        color: HandleColors.Silver,
        img: macoHarmonySilver
    },
    {
        type: HandleTypes.MacoHarmony,
        color: HandleColors.Champagne,
        img: macoHarmonyChampagne
    },
    {
        type: HandleTypes.MacoHarmony,
        color: HandleColors.BrownMsk,
        img: macoHarmonyBrown
    },
    {
        type: HandleTypes.MacoHarmony,
        color: HandleColors.Bronze,
        img: macoHarmonyBronze
    },
    {
        type: HandleTypes.MacoHarmony,
        color: HandleColors.Titanium,
        img: macoHarmonyTitanium
    }
]

const ROTO_HANDLES = [
    {
        type: HandleTypes.Roto,
        color: HandleColors.White,
        img: rotoLineWhite
    },
    {
        type: HandleTypes.Roto,
        color: HandleColors.Silver,
        img: rotoLineSilver
    },
    {
        type: HandleTypes.Roto,
        color: HandleColors.Champagne,
        img: rotoLineChampagne
    },
    {
        type: HandleTypes.Roto,
        color: HandleColors.BrownMsk,
        img: rotoLineBrown
    },
    {
        type: HandleTypes.Roto,
        color: HandleColors.Bronze,
        img: rotoLineBronze
    },
    {
        type: HandleTypes.Roto,
        color: HandleColors.DarkBronze,
        img: rotoLineBronze
    },
    {
        type: HandleTypes.Roto,
        color: HandleColors.Titanium,
        img: rotoLineTitan
    }
    // {
    //     type: HandleTypes.Roto,
    //     color: HandleColors.Gold,
    //     img: rotoLineLatun
    // },
    // {
    //     type: HandleTypes.Roto,
    //     color: HandleColors.GoldMatt,
    //     img: rotoLineLatun
    // }
]

const RHAPSODY_HANDLES = [
    {
        type: HandleTypes.MacoRhapsody,
        color: HandleColors.Silver,
        img: macoRhapsodySilver
    },
    {
        type: HandleTypes.MacoRhapsody,
        color: HandleColors.Champagne,
        img: macoRhapsodyChampagne
    },
    {
        type: HandleTypes.MacoRhapsody,
        color: HandleColors.BrownPerm,
        img: macoRhapsodyBrown
    },
    {
        type: HandleTypes.MacoRhapsody,
        color: HandleColors.Bronze,
        img: macoRhapsodyBronze
    },
    {
        type: HandleTypes.MacoRhapsody,
        color: HandleColors.Titanium,
        img: macoRhapsodyTitanium
    }
]

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/explicit-function-return-type
export const useHandles = (dealer: Dealer | null, furniture?: Furniture | null) => {
    let handles = RHAPSODY_HANDLES
    if (dealer?.factoryId === FactoryTypes.Moscow) {
        if (furniture?.displayName === FURNITURE_ROTO.displayName) {
            handles = ROTO_HANDLES
        } else {
            handles = HARMONY_HANDLES
        }
    }
    const availableHandles = [
        {
            type: dealer?.factoryId === FactoryTypes.Moscow ? HandleTypes.MacbethMsk : HandleTypes.MacbethPerm,
            color: HandleColors.White,
            img: merkurijWhite
        },
        {
            type: HandleTypes.MacoHarmony,
            color: HandleColors.White,
            img: macoHarmonyWhite
        },
        ...handles,
        {
            type: HandleTypes.OnBothSides,
            color: HandleColors.White,
            img: merkurijWhite
        },
        {
            type: HandleTypes.OnBothSidesWithKey,
            color: HandleColors.White,
            img: merkurijWhite
        }
    ]

    if (dealer?.factoryId === FactoryTypes.Perm) {
        availableHandles.push(
            {
                type: HandleTypes.Lira,
                color: HandleColors.White,
                img: merkurijWhite
            },
            {
                type: HandleTypes.Customer,
                color: HandleColors.White,
                img: merkurijWhite
            },
            {
                type: HandleTypes.Locked,
                color: HandleColors.White,
                img: merkurijWhite
            }
        )
    }

    if (dealer?.factoryId === FactoryTypes.Moscow) {
        availableHandles.push({
            type: HandleTypes.Customer,
            color: HandleColors.White,
            img: merkurijWhite
        })
    }

    return availableHandles
}

export function HandleSelector(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)

    const handles = useHandles(currentDealer, currentProduct?.furniture)
    const setHandle = useSetHandle(currentOrder, currentConstruction, currentContext)

    const selectHandle = useCallback((handleType: HandleTypes, handleColor: HandleColors) => setHandle(handleType, handleColor), [setHandle])
    const {isMobile} = useAppContext()

    if (!(currentContext instanceof Handle)) {
        return null
    }

    const {type, color} = currentContext

    return (
        <div className={`${styles.handles} ${isMobile ? styles.mobile : ''}`}>
            {handles.map(handle => (
                <div
                    key={`${handle.type}-${handle.color}`}
                    onClick={() => selectHandle(handle.type, handle.color)}
                    className={getClassNames(`${styles.handle} blue-hover`, {selected: type === handle.type && color === handle.color})}>
                    <img src={handle.img} alt="" draggable={'false'} />
                    <p>
                        {handle.type}
                        <br />
                        {handle.color.toLowerCase()}
                    </p>
                </div>
            ))}
        </div>
    )
}
