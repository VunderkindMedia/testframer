import * as ReactDOM from 'react-dom'
import {ReactNode} from 'react'

interface IBodyPortalProps {
    children?: ReactNode
}

export function BodyPortal(props: IBodyPortalProps): JSX.Element {
    return ReactDOM.createPortal(props.children, document.body)
}
