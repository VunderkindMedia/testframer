import * as React from 'react'
import {Hint} from '../hint/hint'
import styles from './with-hint.module.css'

interface IWithHintProps {
    children: JSX.Element | string
    hint?: string
    className?: string
}

export function WithHint(props: IWithHintProps): JSX.Element {
    const {children, hint, className} = props

    return (
        <div className={`${styles.withHint} ${className ?? ''}`}>
            {children}
            {hint && <Hint text={hint} className={styles.hint} />}
        </div>
    )
}
