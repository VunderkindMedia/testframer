import * as React from 'react'
import {ReactNode, useCallback, useState} from 'react'
import './svg-construction-drawer-container.css'
import {SvgConstructionDrawer} from '../svg-construction-drawer/svg-construction-drawer'
import {HostProductInfo} from '../../../model/host-product-info'
import {Construction} from '../../../model/framer/construction'
import {IContext} from '../../../model/i-context'
import {DimensionLine} from '../../../model/dimension-line'
import {Size} from '../../../model/size'
import {Lamination} from '../../../model/framer/lamination'
import {Rectangle} from '../../../model/geometry/rectangle'
import {ConnectButton} from '../connect-button/connect-button'
import {Sides} from '../../../model/sides'
import {Action} from '../../../model/action'
import {Product} from '../../../model/framer/product'
import {ConfigContexts} from '../../../model/config-contexts'
import {Beam} from '../../../model/framer/beam'
import {Connector} from '../../../model/framer/connector'
import {ConnectorTypes} from '../../../model/framer/connector-types'
import {isMobileBrowser} from '../../../helpers/mobile'

const LINE_OFFSET = 29
const BRACKET_WIDTH = 14
const DEFAULT_STROKE_WIDTH = 1
const DEFAULT_STROKE_BOLD_WIDTH = 2

export interface ISvgConstructionDrawerContainerProps {
    currentConstruction: Construction
    currentContext: IContext | null
    highlightedContext: IContext | null
    currentDimensionLine: DimensionLine | null
    highlightedDimensionLine: DimensionLine | null
    onSelectContext?: (context: IContext | null) => void
    onHoverContext?: (context: IContext | null) => void
    onSelectDimensionLine?: (dimensionLine: DimensionLine | null) => void
    onHoverDimensionLine?: (dimensionLine: DimensionLine | null) => void
    isReadonly: boolean
    isRestrictedMode?: boolean
    windowSize: Size
    drawerSize?: Size
    lamination: Lamination[]
    setConfigContext: (context: ConfigContexts | null) => void
    setHostProductAction: (product: Product | null) => void
    setConnectingSideAction: (side: Sides | null) => void
    setCurrentProductAction: (product: Product) => void
    setTemplateFilterAction: (filter: (construction: Construction) => boolean) => void
    children?: ReactNode
    showTemplateSelector: () => void
    onChangeDimensionLineValue?: (dLine: DimensionLine, value: number, context: IContext | null | undefined) => void
    onClickOutConstruction?: () => void
}

export function SvgConstructionDrawerContainer(props: ISvgConstructionDrawerContainerProps): JSX.Element {
    const {
        currentConstruction,
        currentContext,
        highlightedContext,
        currentDimensionLine,
        highlightedDimensionLine,
        onSelectContext,
        onHoverContext,
        onSelectDimensionLine,
        onHoverDimensionLine,
        isReadonly,
        isRestrictedMode = false,
        windowSize,
        drawerSize,
        lamination,
        setConfigContext,
        setHostProductAction,
        setConnectingSideAction,
        setCurrentProductAction,
        setTemplateFilterAction,
        showTemplateSelector,
        onChangeDimensionLineValue,
        onClickOutConstruction
    } = props

    const [drawingRect, setDrawingRect] = useState(new Rectangle())
    const [hostProductInfos, setHostProductInfos] = useState<HostProductInfo[]>([])

    const buttonHorizontalOffset = (drawingRect.refPoint.x + 56) / 2
    const buttonVerticalOffset = (drawingRect.refPoint.y + 56) / 2

    const buttons: JSX.Element[] = []

    const showExtenderSelector = useCallback(
        (product: Product, side: Sides) => {
            setHostProductAction(product)
            setConnectingSideAction(side)
            onSelectContext && onSelectContext(null)
            setConfigContext(ConfigContexts.Connector)
        },
        [setHostProductAction, setConnectingSideAction, setConfigContext, onSelectContext]
    )

    const moskitkaFilter = (c: Construction): boolean => {
        if (currentConstruction.isMoskitka) {
            return c.isMoskitka
        }

        return !c.isMoskitka
    }

    const emptyFilter = (c: Construction): boolean => {
        if (currentConstruction.isEmpty) {
            return c.isEmpty
        }

        return !c.isEmpty
    }

    const showConnectorContextMenuItem = (hostBeam: Beam): boolean => {
        if (hostBeam instanceof Connector) {
            return hostBeam.connectorType === ConnectorTypes.Extender
        }
        return true
    }

    !isReadonly &&
        !isRestrictedMode &&
        hostProductInfos.forEach(info => {
            const {leftmostBeam, rightmostBeam, topmostBeam, bottommostBeam} = info.product

            if (leftmostBeam && leftmostBeam.isVertical && !leftmostBeam.isConnectionPart) {
                const actions: Action[] = []
                if (!info.product.isEntranceDoor) {
                    actions.push(
                        new Action('Изделие', () => {
                            setCurrentProductAction(info.product)
                            setConnectingSideAction(Sides.Left)
                            setTemplateFilterAction(
                                c =>
                                    c.products.length === 1 &&
                                    !c.products[0].isEntranceDoor &&
                                    c.rightmostBeam.isVertical &&
                                    moskitkaFilter(c) &&
                                    (info.product.isAluminium ? c.isAluminium : true) &&
                                    emptyFilter(c)
                            )
                            showTemplateSelector()
                        })
                    )
                }
                if (!currentConstruction.isMoskitka && showConnectorContextMenuItem(info.product.leftmostConnectedBeam)) {
                    actions.push(new Action('Соединитель', () => showExtenderSelector(info.product, Sides.Left)))
                }
                buttons.push(
                    <div
                        key={`l-${info.product.productGuid}`}
                        className="add-button-wrap"
                        style={{
                            transform: 'translate(-50%, -50%)',
                            bottom: `${56 / 2 + info.visualRect.center.y}px`,
                            left: buttonHorizontalOffset
                        }}>
                        <ConnectButton side={Sides.Left} actions={actions} />
                    </div>
                )
            }

            if (rightmostBeam && rightmostBeam.isVertical && !rightmostBeam.isConnectionPart) {
                const actions: Action[] = []
                if (!info.product.isEntranceDoor) {
                    actions.push(
                        new Action('Изделие', () => {
                            setCurrentProductAction(info.product)
                            setConnectingSideAction(Sides.Right)
                            setTemplateFilterAction(
                                c =>
                                    c.products.length === 1 &&
                                    !c.products[0].isEntranceDoor &&
                                    c.leftmostBeam.isVertical &&
                                    moskitkaFilter(c) &&
                                    (info.product.isAluminium ? c.isAluminium : true) &&
                                    emptyFilter(c)
                            )
                            showTemplateSelector()
                        })
                    )
                }
                if (!currentConstruction.isMoskitka && showConnectorContextMenuItem(info.product.rightmostConnectedBeam)) {
                    actions.push(new Action('Соединитель', () => showExtenderSelector(info.product, Sides.Right)))
                }
                buttons.push(
                    <div
                        key={`r-${info.product.productGuid}`}
                        className="add-button-wrap"
                        style={{
                            transform: 'translate(50%, -50%)',
                            bottom: `${56 / 2 + info.visualRect.center.y}px`,
                            right: buttonHorizontalOffset
                        }}>
                        <ConnectButton side={Sides.Right} actions={actions} />
                    </div>
                )
            }

            if (
                topmostBeam &&
                topmostBeam.isHorizontal &&
                !topmostBeam.isConnectionPart &&
                !currentConstruction.allConnectors.find(c => c.connectedBeamGuid === topmostBeam.nodeId)
            ) {
                const actions: Action[] = []
                if (!info.product.isEntranceDoor && info.product === currentConstruction.products[0]) {
                    actions.push(
                        new Action('Изделие', () => {
                            setCurrentProductAction(info.product)
                            setConnectingSideAction(Sides.Top)
                            setTemplateFilterAction(
                                c =>
                                    c.products.length === 1 &&
                                    !c.products[0].isEntranceDoor &&
                                    c.bottommostBeam.isHorizontal &&
                                    moskitkaFilter(c) &&
                                    (info.product.isAluminium ? c.isAluminium : true) &&
                                    emptyFilter(c)
                            )
                            showTemplateSelector()
                        })
                    )
                }
                if (!currentConstruction.isMoskitka && showConnectorContextMenuItem(info.product.topmostConnectedBeam)) {
                    actions.push(new Action('Соединитель', () => showExtenderSelector(info.product, Sides.Top)))
                }
                buttons.push(
                    <div
                        key={`t-${info.product.productGuid}`}
                        className="add-button-wrap"
                        style={{
                            transform: 'translate(50%, -50%)',
                            top: buttonVerticalOffset,
                            left: `${56 / 2 + info.visualRect.center.x}px`
                        }}>
                        <ConnectButton side={Sides.Top} actions={actions} />
                    </div>
                )
            }

            if (
                bottommostBeam &&
                bottommostBeam.isHorizontal &&
                !bottommostBeam.isConnectionPart &&
                !currentConstruction.allConnectors.find(c => c.connectedBeamGuid === bottommostBeam.nodeId)
            ) {
                const actions: Action[] = []
                if (!info.product.isEntranceDoor && info.product === currentConstruction.products[0]) {
                    actions.push(
                        new Action('Изделие', () => {
                            setCurrentProductAction(info.product)
                            setConnectingSideAction(Sides.Bottom)
                            setTemplateFilterAction(
                                c =>
                                    c.products.length === 1 &&
                                    !c.products[0].isEntranceDoor &&
                                    c.topmostBeam.isHorizontal &&
                                    moskitkaFilter(c) &&
                                    (info.product.isAluminium ? c.isAluminium : true) &&
                                    emptyFilter(c)
                            )
                            showTemplateSelector()
                        })
                    )
                }
                if (!currentConstruction.isMoskitka && showConnectorContextMenuItem(info.product.bottommostConnectedBeam)) {
                    actions.push(new Action('Соединитель', () => showExtenderSelector(info.product, Sides.Bottom)))
                }
                buttons.push(
                    <div
                        key={`b-${info.product.productGuid}`}
                        className="add-button-wrap"
                        style={{
                            transform: 'translate(50%, 50%)',
                            bottom: buttonVerticalOffset,
                            left: `${56 / 2 + info.visualRect.center.x}px`
                        }}>
                        <ConnectButton side={Sides.Bottom} actions={actions} />
                    </div>
                )
            }
        })

    return (
        <div className="svg-construction-drawer-container" data-test="svg-construction-drawer-container">
            <SvgConstructionDrawer
                currentConstruction={currentConstruction}
                lineOffset={LINE_OFFSET}
                bracketWidth={BRACKET_WIDTH}
                lineWidth={DEFAULT_STROKE_WIDTH}
                lineBoldWidth={DEFAULT_STROKE_BOLD_WIDTH}
                currentContext={currentContext}
                highlightedContext={highlightedContext}
                currentDimensionLine={currentDimensionLine}
                highlightedDimensionLine={highlightedDimensionLine}
                onSelectContext={onSelectContext}
                onHoverContext={onHoverContext}
                onSelectDimensionLine={onSelectDimensionLine}
                onHoverDimensionLine={isMobileBrowser() ? onSelectDimensionLine : onHoverDimensionLine}
                isReadonly={isReadonly}
                windowSize={windowSize}
                drawerSize={drawerSize}
                lamination={lamination}
                onDrawAreaChange={rect => setDrawingRect(rect)}
                onDrawHostProducts={infos => setHostProductInfos(infos)}
                onChangeDimensionLineValue={onChangeDimensionLineValue}
                onClickOutConstruction={onClickOutConstruction}
            />
            {buttons}
            {props.children}
        </div>
    )
}
