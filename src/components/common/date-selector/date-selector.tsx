import * as React from 'react'
import {DatePicker} from '../date-picker/date-picker'
import {CSSProperties, useCallback, useEffect, useRef, useState} from 'react'
import {ReactComponent as CalendarIcon} from '../../../resources/images/calendar_icon.inline.svg'
import {Input} from '../input/input'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {useDocumentClick} from '../../../hooks/ui'
import {useAppContext} from '../../../hooks/app'
import styles from './date-selector.module.css'

const DATE_PICKER_OFFSET_PX = 4
const DATE_PICKER_HEIGHT = 36

interface IDateSelectorProps {
    date?: Date | null
    dataTest?: string
    isDisabled?: boolean
    onChange?: (date: Date) => void
    onToggle?: (isActive: boolean) => void
}

export function DateSelector(props: IDateSelectorProps): JSX.Element {
    const {date, dataTest, isDisabled = false, onChange, onToggle} = props

    const containerRef = useRef<HTMLDivElement>(null)
    const [isActive, setIsActive] = useState<boolean>(false)

    const datePickerRef = useRef<HTMLDivElement | null>(null)
    const [datePickerStyle, setDatePickerStyle] = useState<CSSProperties | null>(null)
    const {isMobile} = useAppContext()

    const dateStr = date ? getDateDDMMYYYYFormat(date) : ''

    const handleClick = useCallback((e: MouseEvent) => {
        if (!containerRef.current?.contains(e.target as Node)) {
            setIsActive(false)
        }
    }, [])

    useDocumentClick(handleClick)

    useEffect(() => {
        if (!isActive) {
            setDatePickerStyle(null)
            return
        }
        if (!containerRef.current || !datePickerRef.current) {
            return
        }

        const containerRect = containerRef.current.getBoundingClientRect()
        const datePickerRect = datePickerRef.current.getBoundingClientRect()

        const expectedEndY = containerRect.bottom + DATE_PICKER_OFFSET_PX + datePickerRect.height
        const expectedStartY = containerRect.y - DATE_PICKER_OFFSET_PX - datePickerRect.height
        const expectedEndX = containerRect.x + datePickerRect.width

        const style: CSSProperties = {}

        const innerHeight = isMobile ? document.body.scrollHeight : window.innerHeight

        if (expectedEndY <= innerHeight || expectedStartY < 0) {
            style.top = isMobile ? DATE_PICKER_HEIGHT + DATE_PICKER_OFFSET_PX : containerRect.bottom + DATE_PICKER_OFFSET_PX
        } else {
            style.bottom = isMobile ? DATE_PICKER_HEIGHT + DATE_PICKER_OFFSET_PX : window.innerHeight - containerRect.y + DATE_PICKER_OFFSET_PX
        }

        if (expectedEndX <= window.innerWidth) {
            style.left = isMobile ? 0 : containerRect.x
        } else {
            style.right = isMobile ? 0 : window.innerWidth - containerRect.right
        }

        setDatePickerStyle(style)
    }, [isActive, isMobile])

    useEffect(() => {
        onToggle && onToggle(isActive)
    }, [isActive, onToggle])

    const toggleDatePickerHandler = useCallback(() => {
        if (isDisabled) {
            return
        }
        !isActive && setIsActive(!isActive)
    }, [isActive, isDisabled])

    return (
        <div
            ref={containerRef}
            className={`${styles.dateSelector} ${isActive && datePickerStyle && styles.active} ${date && styles.hasDate} ${
                isMobile && styles.mobile
            } ${isDisabled && styles.disabled}`}
            onClick={toggleDatePickerHandler}>
            <Input type="text" isReadonly={true} isDisabled={isDisabled} className={styles.input} value={dateStr} dataTest={dataTest} />
            <CalendarIcon className={styles.icon} />
            <DatePicker
                ref={datePickerRef}
                style={datePickerStyle ?? {}}
                className={styles.datePicker}
                defaultSelectedDate={date}
                shouldAlwaysSyncWithProps={true}
                onSelectDate={date => {
                    setIsActive(false)
                    onChange && onChange(date)
                }}
            />
        </div>
    )
}
