import * as React from 'react'
import styles from './loader.module.css'

export function Loader(): JSX.Element {
    return (
        <div className={styles.loader}>
            <div className={`${styles.profile} ${styles.profile1}`} />
            <div className={`${styles.profile} ${styles.profile2}`} />
            <div className={`${styles.profile} ${styles.profile3}`} />
            <div className={`${styles.profile} ${styles.profile4}`} />

            <svg width="289" height="74" viewBox="0 0 289 74" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="91.2498" y="22.5" width="11" height="11" rx="5.5" fill="#F2F2F2" stroke="#515257" />
                <rect x="94.2498" y="25.5" width="5" height="5" rx="2.5" fill="#F2F2F2" stroke="#515257" />
                <rect x="108.5" y="6.5" width="5" height="5" rx="2.5" fill="#F2F2F2" stroke="#515257" />
                <path d="M92.2487 30.9994L92.2487 66" stroke="#515257" strokeLinejoin="round" />
                <path d="M101.249 31.0002L107.5 66.0005" stroke="#515257" strokeLinejoin="round" />
                <path d="M13.2228 31.0002L19.4741 66.0005" stroke="#515257" strokeLinejoin="round" />
                <path d="M193.223 31.0002L199.474 66.0005" stroke="#515257" strokeLinejoin="round" />
                <path d="M108.644 9.36719L96.0087 22.7963" stroke="#515257" strokeLinejoin="round" />
                <path d="M110.349 11.1953L101.354 25.7414" stroke="#515257" strokeLinejoin="round" />
                <rect x="3.24976" y="22.5" width="11" height="11" rx="5.5" fill="#F2F2F2" stroke="#515257" />
                <rect x="6.24976" y="25.5" width="5" height="5" rx="2.5" fill="#F2F2F2" stroke="#515257" />
                <rect x="22.5" y="6.5" width="6" height="6" rx="3" fill="#F2F2F2" stroke="#515257" />
                <path d="M4.24875 30.9994L4.24875 66" stroke="#515257" strokeLinejoin="round" />
                <path d="M23.0643 10.5L9.06431 22.4998" stroke="#515257" strokeLinejoin="round" />
                <path d="M24.5643 12.5L14.0643 26" stroke="#515257" strokeLinejoin="round" />
                <path d="M122.629 4.00036L112.384 6.74551" stroke="#515257" strokeLinejoin="round" />
                <path d="M122.629 4L113.174 8.93339" stroke="#515257" strokeLinejoin="round" />
                <path d="M129.201 2.67778L128.286 1.12478L120.606 4.63837" stroke="#515257" strokeLinejoin="round" />
                <path d="M129.201 6.35542L128.286 7.90842L120.606 4.39483" stroke="#515257" strokeLinejoin="round" />
                <path d="M41.6092 16.1967L41.593 14.3941L33.1853 13.5971" stroke="#515257" strokeLinejoin="round" />
                <path d="M39.7701 19.3815L38.2009 20.2688L33.3068 13.3861" stroke="#515257" strokeLinejoin="round" />
                <rect x="183.25" y="22.5" width="11" height="11" rx="5.5" fill="#F2F2F2" stroke="#515257" />
                <rect x="186.25" y="25.5" width="5" height="5" rx="2.5" fill="#F2F2F2" stroke="#515257" />
                <rect x="204.5" y="12.5" width="5" height="5" rx="2.5" fill="#F2F2F2" stroke="#515257" />
                <path d="M184.249 30.9994L184.249 66" stroke="#515257" strokeLinejoin="round" />
                <path d="M204.5 14.5L189.064 22.4998" stroke="#515257" strokeLinejoin="round" />
                <path d="M205 17L194 27.5001" stroke="#515257" strokeLinejoin="round" />
                <path d="M217 23.0005L209.5 15.5003" stroke="#515257" strokeLinejoin="round" />
                <path d="M217 23L208 17.2783" stroke="#515257" strokeLinejoin="round" />
                <rect x="0.5" y="56.5" width="288" height="17" rx="8.5" fill="#F2F2F2" stroke="#515257" />
                <rect x="3.5" y="59.5" width="11" height="11" rx="5.5" fill="#F2F2F2" stroke="#515257" />
                <rect
                    x="6.5"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point1}`}
                />
                <rect x="92.1854" y="59.5" width="11" height="11" rx="5.5" fill="#F2F2F2" stroke="#515257" />
                <rect
                    x="95.1854"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point5}`}
                />
                <rect
                    x="27.1854"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point2}`}
                />
                <rect
                    x="50.1854"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point3}`}
                />
                <rect
                    x="73.1854"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point4}`}
                />
                <rect
                    x="117.5"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point6}`}
                />
                <rect
                    x="140.5"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point7}`}
                />
                <rect
                    x="163.5"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point8}`}
                />
                <rect x="183.5" y="59.5" width="11" height="11" rx="5.5" fill="#F2F2F2" stroke="#515257" />
                <rect
                    x="186.5"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point9}`}
                />
                <rect
                    x="208.815"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point10}`}
                />
                <rect
                    x="231.815"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point11}`}
                />
                <rect
                    x="254.815"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point12}`}
                />
                <rect x="274.5" y="59.5" width="11" height="11" rx="5.5" fill="#F2F2F2" stroke="#515257" />
                <rect
                    x="277.5"
                    y="62.5"
                    width="5"
                    height="5"
                    rx="2.5"
                    fill="#FFFF00"
                    stroke="#515257"
                    className={`${styles.point} ${styles.point13}`}
                />
                <path d="M61.5 24.5H47.5V54.5H61.5V24.5Z" stroke="#515257" />
                <rect x="-0.5" y="0.5" width="18" height="34" transform="matrix(-1 0 0 1 63 22)" stroke="#515257" />
                <path d="M137.5 24.5H123.5V54.5H137.5V24.5Z" stroke="#515257" />
                <rect x="-0.5" y="0.5" width="18" height="34" transform="matrix(-1 0 0 1 139 22)" stroke="#515257" />
                <path d="M162.5 24.5H148.5V54.5H162.5V24.5Z" stroke="#515257" />
                <rect x="-0.5" y="0.5" width="18" height="34" transform="matrix(-1 0 0 1 164 22)" stroke="#515257" />
                <rect x="0.5" y="-0.5" width="35" height="2" transform="matrix(1 8.74228e-08 8.74228e-08 -1 127.25 5)" stroke="#515257" />
                <path d="M232.5 24.5H218.5V54.5H232.5V24.5Z" stroke="#515257" />
                <rect x="-0.5" y="0.5" width="35" height="34" transform="matrix(-1 0 0 1 251 22)" stroke="#515257" />
                <path d="M249.5 24.5H236.5V54.5H249.5V24.5Z" stroke="#515257" />
                <path d="M234.5 22V57" stroke="#515257" />
                <path d="M219 19.5352L222.536 15.9996" stroke="#515257" />
                <path d="M221 27L224.536 30.5355" stroke="#515257" />
                <path d="M217.25 15L217.25 17.5" stroke="#515257" />
                <path d="M211.5 23.25L209 23.25" stroke="#515257" />
                <path d="M210 28.5352L213.536 24.9996" stroke="#515257" />
                <rect x="28.683" y="9.68301" width="6" height="2" transform="rotate(30 28.683 9.68301)" fill="#F2F2F2" stroke="#515257" />
            </svg>
        </div>
    )
}
