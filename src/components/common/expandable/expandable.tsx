import * as React from 'react'
import {ReactNode, useCallback, useEffect, useRef, useState} from 'react'
import './expandable.css'
import Timeout = NodeJS.Timeout

interface IExpandableProps {
    header: JSX.Element
    isExpanded?: boolean
    onChange?: (expanded: boolean) => void
    children?: ReactNode
    className?: string
    updateAnchor?: string
}

export function Expandable(props: IExpandableProps): JSX.Element {
    const {header, onChange, children, className, updateAnchor} = props

    const bodyRef = useRef<HTMLDivElement>(null)
    const fakeBodyRef = useRef<HTMLDivElement>(null)
    const defaultExpandedState = useRef(props.isExpanded ?? false)
    const [isExpanded, setIsExpanded] = useState<boolean>(props.isExpanded ?? false)
    const [maxHeight, setMaxHeight] = useState(0)

    const timerRef = useRef<Timeout | null>(null)

    useEffect(() => {
        setIsExpanded(props.isExpanded ?? defaultExpandedState.current)
    }, [updateAnchor, props.isExpanded])

    useEffect(() => {
        setIsExpanded(Boolean(props.isExpanded))
    }, [props.isExpanded])

    useEffect(() => {
        if (bodyRef.current) {
            bodyRef.current.style.maxHeight = isExpanded ? `${maxHeight}px` : '0'
        }
    }, [isExpanded, maxHeight])

    useEffect(() => {
        if (!fakeBodyRef.current) {
            return
        }
        timerRef.current = setInterval(() => {
            const height = fakeBodyRef.current?.clientHeight ?? 0
            if (height === 0) {
                return
            }
            timerRef.current && clearInterval(timerRef.current)
            setMaxHeight(height)
        }, 16)
    }, [props.children, maxHeight])

    useEffect(() => {
        return () => {
            timerRef.current && clearInterval(timerRef.current)
        }
    }, [])

    const changeState = useCallback(
        (value: boolean) => {
            setIsExpanded(value)
            onChange && onChange(value)
        },
        [onChange]
    )

    return (
        <div className={`expandable ${isExpanded && 'expanded'} ${className}`}>
            <div
                className="expandable__header"
                onClick={e => {
                    e.stopPropagation()
                    changeState(!isExpanded)
                }}>
                {header}
            </div>
            <div className="expandable__body" ref={bodyRef}>
                {React.Children.map(children, child => (
                    <>{child}</>
                ))}
            </div>
            <div className="expandable__fake__body-wrapper">
                <div className="expandable__body expandable__fake__body" ref={fakeBodyRef}>
                    {React.Children.map(children, child => (
                        <>{child}</>
                    ))}
                </div>
            </div>
        </div>
    )
}
