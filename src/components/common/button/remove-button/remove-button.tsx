import * as React from 'react'
import {ReactComponent as TrashIcon} from './resources/trash_icon.inline.svg'
import {IconButton} from '../../icon-control/icon-button'

interface IRemoveButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
}

export function RemoveButton(props: IRemoveButtonProps): JSX.Element {
    const {onClick, className, isDisabled} = props

    return (
        <IconButton className={className} isDisabled={isDisabled} onClick={onClick}>
            <TrashIcon />
        </IconButton>
    )
}
