import * as React from 'react'
import {ReactComponent as GeoIcon} from './resources/geo_icon.inline.svg'
import {IconButton} from '../../icon-control/icon-button'

interface IGeoButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
}

const GeoButton = React.forwardRef<HTMLButtonElement, IGeoButtonProps>((props: IGeoButtonProps, ref) => {
    const {onClick, className, isDisabled} = props

    return (
        <IconButton className={className} buttonStyle="with-border" isDisabled={isDisabled} onClick={onClick} ref={ref}>
            <GeoIcon />
        </IconButton>
    )
})

GeoButton.displayName = 'GeoButton'

export {GeoButton}
