import * as React from 'react'
import {ReactComponent as CopyIcon} from './resources/copy_icon.inline.svg'
import {IconButton} from '../../icon-control/icon-button'

interface IСopyingButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
    dataTest?: string
    dataId?: string
}

export function СopyingButton(props: IСopyingButtonProps): JSX.Element {
    const {onClick, className, isDisabled, dataTest, dataId} = props

    return (
        <IconButton className={className} isDisabled={isDisabled} onClick={onClick} dataTest={dataTest} dataId={dataId}>
            <CopyIcon />
        </IconButton>
    )
}
