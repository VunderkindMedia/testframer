import * as React from 'react'
import styles from './visibility-button.module.css'
import {getClassNames} from '../../../../helpers/css'

interface IVisibilityButtonProps {
    isVisible: boolean
    className?: string
    dataTest?: string
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

const VisibilityButton = React.forwardRef<HTMLButtonElement, IVisibilityButtonProps>((props: IVisibilityButtonProps, ref) => {
    const {isVisible, className, dataTest, onClick} = props

    return (
        <button
            ref={ref}
            type="button"
            data-test={dataTest}
            className={getClassNames(`${styles.button} ${className}`, {
                [styles.visible]: isVisible,
                [styles.hidden]: !isVisible
            })}
            onClick={onClick}
        />
    )
})

VisibilityButton.displayName = 'VisibilityButton'

export {VisibilityButton}
