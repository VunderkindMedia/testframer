import * as React from 'react'
import {Button} from '../button'
import {ReactComponent as FilterIcon} from './resources/filter_icon.inline.svg'
import styles from './filter-button.module.css'

interface IFilterButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

export function FilterButton(props: IFilterButtonProps): JSX.Element {
    return (
        <Button buttonStyle="with-border" onClick={props.onClick}>
            <div className={styles.icon}>
                <FilterIcon />
            </div>
        </Button>
    )
}
