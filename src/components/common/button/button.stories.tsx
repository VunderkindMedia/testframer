import * as React from 'react'
import {Button, IButtonProps} from './button'
import {action} from '@storybook/addon-actions'

export default {
    title: 'Button',
    component: Button,
    argTypes: {
        buttonStyle: {
            table: {
                type: {summary: 'with-border | with-fill'}
            },
            control: {
                type: 'select',
                options: ['with-border', 'with-fill', null]
            }
        },
        isDisabled: {
            table: {
                type: {summary: 'boolean'},
                defaultValue: {summary: false}
            },
            control: {type: 'boolean'}
        },
        className: {
            table: {
                type: {summary: 'string'}
            },
            control: {disable: true}
        },
        onClick: {
            table: {
                type: {summary: '(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void'}
            },
            control: {disable: true}
        },
        style: {
            table: {
                type: {summary: 'CSSProperties'}
            },
            control: {disable: true}
        },
        type: {
            table: {
                type: {summary: 'button | submit'}
            },
            control: {
                disable: true
            }
        },
        children: {
            table: {
                type: {summary: 'ReactNode'}
            },
            control: {type: 'text'}
        },
        isNowrap: {
            table: {
                type: {summary: 'boolean'},
                defaultValue: {summary: false}
            },
            control: {type: 'boolean'}
        }
    },
    args: {
        buttonStyle: 'with-border',
        isDisabled: false,
        className: '',
        onClick: '',
        style: {},
        type: 'button',
        children: 'Текст кнопки',
        isNowrap: false
    }
}

export const Default = (props: IButtonProps): JSX.Element => {
    return (
        <Button {...props} onClick={action('onClick')}>
            {props.children}
        </Button>
    )
}
