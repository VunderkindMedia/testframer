import * as React from 'react'
import {ReactComponent as PenIcon} from './resources/pen_icon.inline.svg'
import {IconButton} from '../../icon-control/icon-button'

interface IEditButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
}

export function EditButton(props: IEditButtonProps): JSX.Element {
    const {onClick, className, isDisabled} = props

    return (
        <IconButton className={className} isDisabled={isDisabled} onClick={onClick}>
            <PenIcon />
        </IconButton>
    )
}
