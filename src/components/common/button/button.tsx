import * as React from 'react'
import styles from './button.module.css'
import {CSSProperties, ReactNode} from 'react'

export interface IButtonProps {
    buttonStyle?: 'with-border' | 'with-fill'
    isDisabled?: boolean
    className?: string
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    style?: CSSProperties
    type?: 'button' | 'submit'
    children?: ReactNode
    dataTest?: string
    dataId?: string
    isNowrap?: boolean
}

const Button = React.forwardRef<HTMLButtonElement, IButtonProps>((props: IButtonProps, ref) => {
    const {buttonStyle, isDisabled = false, className, style, onClick, type = 'button', children, dataTest, dataId, isNowrap = true} = props

    return (
        <button
            ref={ref}
            type={type}
            data-test={dataTest}
            data-id={dataId}
            className={`${styles.button} ${buttonStyle && styles[buttonStyle]} ${isNowrap ? styles.nowrap : ''} ${className}`}
            disabled={isDisabled}
            style={style}
            onClick={onClick}>
            {typeof children === 'string' ? <span className={styles.title}>{children}</span> : <div className={styles.title}>{children}</div>}
        </button>
    )
})

Button.displayName = 'Button'

export {Button}
