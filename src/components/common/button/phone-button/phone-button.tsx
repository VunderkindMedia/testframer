import * as React from 'react'
import {ReactComponent as PhoneIcon} from './resources/phone_icon.inline.svg'
import {IconLink} from '../../icon-control/icon-link'

interface IPhoneButtonProps {
    phone?: string
    className?: string
}

const PhoneButton = React.forwardRef<HTMLAnchorElement, IPhoneButtonProps>((props: IPhoneButtonProps, ref): JSX.Element => {
    const {phone, className} = props

    return (
        <IconLink className={className} style="with-border" href={phone ? `tel:${phone}` : ''} ref={ref}>
            <PhoneIcon />
        </IconLink>
    )
})

PhoneButton.displayName = 'PhoneButton'

export {PhoneButton}
