import * as React from 'react'
import {ArrowTooltip} from '../../arrow-tooltip/arrow-tooltip'

export interface IButtonWithHintProps {
    hint: string
    className?: string
    children: JSX.Element
}

export function ButtonWithHint(props: IButtonWithHintProps): JSX.Element {
    const {hint, children, className} = props

    if (hint) {
        return (
            <ArrowTooltip title={hint} placement="top">
                <div className={className}>{children}</div>
            </ArrowTooltip>
        )
    }

    return children
}
