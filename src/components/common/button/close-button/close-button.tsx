import * as React from 'react'
import styles from './close-button.module.css'
import {ReactComponent as CloseIcon} from './resources/close_icon.inline.svg'
import {CSSProperties} from 'react'

interface ICloseButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
    style?: CSSProperties
}

const CloseButton = React.forwardRef<HTMLButtonElement, ICloseButtonProps>((props: ICloseButtonProps, ref) => {
    const {onClick, className, isDisabled, style} = props

    return (
        <button ref={ref} className={`${styles.button} ${className}`} onClick={onClick} disabled={isDisabled} style={style}>
            <CloseIcon />
        </button>
    )
})

CloseButton.displayName = 'CloseButton'

export {CloseButton}
