import * as React from 'react'
import {ReactComponent as TrackIcon} from './resources/truck.inline.svg'
import {IconButton} from '../../icon-control/icon-button'

interface ITruckButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
}

export function TruckButton(props: ITruckButtonProps): JSX.Element {
    const {onClick, className, isDisabled} = props

    return (
        <IconButton className={className} isDisabled={isDisabled} onClick={onClick}>
            <TrackIcon />
        </IconButton>
    )
}
