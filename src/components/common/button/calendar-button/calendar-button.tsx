import * as React from 'react'
import {useState, useCallback, useRef} from 'react'
import {ReactComponent as CalendarIcon} from '../../../../resources/images/calendar_icon.inline.svg'
import {DatePicker} from '../../date-picker/date-picker'
import styles from './calendar-button.module.css'
import {getClassNames} from '../../../../helpers/css'
import {useDocumentClick} from '../../../../hooks/ui'

interface ICalendarButton {
    date?: Date
    direction?: 'left' | 'right'
    onSelect?: (date: Date) => void
}

export function CalendarButton(props: ICalendarButton): JSX.Element {
    const {date, direction = 'left', onSelect} = props
    const [isOpened, setIsOpened] = useState(false)
    const containerRef = useRef<HTMLDivElement>(null)

    const closeHandler = useCallback((e: MouseEvent) => {
        if (!containerRef.current?.contains(e.target as Node)) {
            setIsOpened(false)
        }
    }, [])

    useDocumentClick(closeHandler)

    return (
        <div className={styles.wrapper} ref={containerRef}>
            <button className={getClassNames(styles.button, {[styles.active]: isOpened})} onClick={() => setIsOpened(!isOpened)}>
                <CalendarIcon className={styles.icon} />
            </button>
            <DatePicker
                className={getClassNames(`${styles.datePicker} ${styles[direction]}`, {[styles.opened]: isOpened})}
                defaultSelectedDate={date}
                shouldAlwaysSyncWithProps={true}
                onSelectDate={date => {
                    onSelect && onSelect(date)
                    setIsOpened(false)
                }}
            />
        </div>
    )
}
