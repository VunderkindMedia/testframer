import * as React from 'react'
import {Button} from '../button'
import styles from './down-button.module.css'

interface IDownButtonProps {
    className?: string
    onClick: () => void
}

export function DownButton(props: IDownButtonProps): JSX.Element {
    const {onClick, className} = props

    return <Button className={`${styles.button} ${className ?? ''}`} onClick={onClick} />
}
