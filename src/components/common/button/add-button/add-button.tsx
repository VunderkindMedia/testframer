import * as React from 'react'
import styles from './add-button.module.css'
import {Button} from '../button'
import {ReactComponent as PlusIcon} from './resources/plus_icon.inline.svg'

interface IAddButtonProps {
    title?: string
    isDisabled?: boolean
    className?: string
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    dataTest?: string
    dataId?: string
    style?: 'primary' | 'secondary'
    size?: 'big' | 'small'
}

const AddButton = React.forwardRef<HTMLButtonElement, IAddButtonProps>((props: IAddButtonProps, ref): JSX.Element => {
    const {title, className, isDisabled, onClick, dataTest, dataId, style = 'primary', size = 'small'} = props

    return (
        <Button
            ref={ref}
            dataTest={dataTest}
            dataId={dataId}
            className={`${styles.addButton} ${styles[style]} ${styles[size]} ${className ?? ''}`}
            isDisabled={isDisabled}
            onClick={onClick}>
            <div className={styles.title}>
                <div className={styles.icon}>
                    <PlusIcon />
                </div>
                {title && <p>{title}</p>}
            </div>
        </Button>
    )
})

AddButton.displayName = 'AddButton'

export {AddButton}
