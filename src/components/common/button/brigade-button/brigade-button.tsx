import * as React from 'react'
import {ReactComponent as BrigadeIcon} from './resources/brigade_icon.inline.svg'
import {IconButton} from '../../icon-control/icon-button'

interface IBrigadeButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
}

export function BrigadeButton(props: IBrigadeButtonProps): JSX.Element {
    const {onClick, className, isDisabled} = props

    return (
        <IconButton className={className} buttonStyle="with-border" isDisabled={isDisabled} onClick={onClick}>
            <BrigadeIcon />
        </IconButton>
    )
}
