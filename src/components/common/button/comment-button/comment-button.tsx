import * as React from 'react'
import {ReactComponent as CommentIcon} from './resources/comment_icon.inline.svg'
import {IconButton} from '../../icon-control/icon-button'

interface ICommentButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
}

export function CommentButton(props: ICommentButtonProps): JSX.Element {
    const {onClick, className, isDisabled} = props

    return (
        <IconButton className={className} buttonStyle="with-border" isDisabled={isDisabled} onClick={onClick}>
            <CommentIcon />
        </IconButton>
    )
}
