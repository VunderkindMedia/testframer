import React from 'react'
import {InfoModal} from '../../common/modal/info-modal/info-modal'
import styles from './energy-info-modal.module.css'

interface IEnergyInfoModalProps {
    onAction: () => void
    onClose: () => void
}

export const EnergyInfoModal = (props: IEnergyInfoModalProps): JSX.Element => {
    return (
        <InfoModal actionTitle="Понятно" {...props}>
            <>
                <div className={styles.icon}></div>
                <p className={styles.description}>После внесенных Вами изменений, данная конструкция больше не участвует в программе ENERGY</p>
            </>
        </InfoModal>
    )
}
