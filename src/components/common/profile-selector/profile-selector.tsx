import * as React from 'react'
import {useCallback, useEffect, useRef, useState} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {ConfigContexts} from '../../../model/config-contexts'
import {MobileSidebar} from '../../mobile/mobile-sidebar/mobile-sidebar'
import {EnergyTooltip} from '../energy-tooltip/energy-tooltip'
import {useSetProfile} from '../../../hooks/builder'
import {Profile} from '../../../model/framer/profile'
import {getClassNames} from '../../../helpers/css'
import styles from './profile-selector.module.css'

export function ProfileSelector(): JSX.Element | null {
    const currentProduct = useSelector((state: IAppState) => {
        return state.builder.configContext === ConfigContexts.ConstructionParams ? state.orders.genericProduct : state.orders.currentProduct
    })
    const configContext = useSelector((state: IAppState) => state.builder.configContext)

    const setProfile = useSetProfile(configContext)

    const selectProfile = useCallback(
        (profile: Profile) => {
            setProfile(profile)
            sidebarRef.current?.hide()
        },
        [setProfile]
    )

    const sidebarRef = useRef<MobileSidebar | null>(null)

    const [selectedBrand, setSelectedBrand] = useState('')

    useEffect(() => {
        if (currentProduct && currentProduct.availableProfiles.length > 0) {
            const profile = currentProduct.availableProfiles.find(p => currentProduct.profile && p.id === currentProduct.profile.id)
            setSelectedBrand(profile ? profile.brand : '')
        }
    }, [currentProduct])

    if (!currentProduct) {
        return null
    }

    const brands = Array.from(new Set(currentProduct.availableProfiles.map(p => p.brand)))

    // const profiles = currentProduct.availableProfiles.filter(p => p.brand === selectedBrand)
    //
    // const brands = currentProduct.availableProfiles.filter(p => p.brand === brand)
    //
    const profiles = currentProduct.availableProfiles.filter(p => p.brand === selectedBrand)

    return (
        <div className={styles.selector} onClick={e => e.stopPropagation()}>
            <div className={styles.brands}>
                {brands.map(brand => (
                    <div
                        key={brand}
                        onClick={() => setSelectedBrand(brand)}
                        className={getClassNames(styles.brand, {[styles.selected]: selectedBrand === brand})}>
                        <p>{brand}</p>
                    </div>
                ))}
            </div>
            <div className={styles.profiles}>
                {profiles
                    .slice(0)
                    .sort((p1, p2) => p1.thickness - p2.thickness)
                    .map(profile => (
                        <div
                            key={profile.id}
                            onClick={e => {
                                e.stopPropagation()
                                selectProfile(profile)
                            }}
                            className={getClassNames(`${styles.profile} blue-hover`, {
                                selected: currentProduct.profile && profile.id === currentProduct.profile.id
                            })}>
                            <p className={styles.name}>
                                {profile.isEnergyProfile && <EnergyTooltip className={styles.energyIcon} />}
                                {profile.displayName ? profile.displayName : profile.name}
                            </p>
                            <p>
                                Ширина: <span>{profile.thickness}</span>
                            </p>
                            <p>
                                Камер: <span>{profile.camernost}</span>
                            </p>
                        </div>
                    ))}
            </div>
        </div>
    )
}
