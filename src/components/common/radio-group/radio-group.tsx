import * as React from 'react'
import styles from './radio-group.module.css'
import {useCallback, useEffect, useState} from 'react'
import {getClassNames} from '../../../helpers/css'

export enum RadioGroupStyles {
    Vertical = 'vertical',
    Horizontal = 'horizontal'
}

interface IRadioGroupProps {
    items: string[]
    selectedIndex?: number
    onSelect?: (index: number) => void
    radioGroupStyle?: RadioGroupStyles
    className?: string
    getDecorator?: (index: number) => JSX.Element
    isDisabled?: boolean
    disabledItems?: string[]
    dataTest?: string
}

export function RadioGroup(props: IRadioGroupProps): JSX.Element {
    const {
        items,
        selectedIndex,
        onSelect,
        radioGroupStyle,
        className = '',
        getDecorator,
        isDisabled = false,
        disabledItems = [],
        dataTest
    } = props

    const [selected, setSelected] = useState<number>(selectedIndex ?? 0)

    useEffect(() => {
        if (selectedIndex !== undefined) {
            setSelected(selectedIndex)
        }
    }, [selectedIndex])

    const select = useCallback(
        (index: number) => {
            setSelected(index)
            onSelect && onSelect(index)
        },
        [onSelect]
    )

    return (
        <div
            data-test={dataTest}
            className={getClassNames(`${styles.radioGroup} ${className}`, {
                [styles.vertical]: radioGroupStyle === RadioGroupStyles.Vertical
            })}>
            {items.map((item, idx) => (
                <label key={idx} className={styles.item} data-test="radio-group-item">
                    <input
                        type="radio"
                        className={styles.input}
                        onChange={() => select(idx)}
                        disabled={isDisabled || disabledItems.includes(item)}
                        checked={selected === idx}
                    />
                    <span>{item}</span>
                    {getDecorator && getDecorator(idx)}
                </label>
            ))}
        </div>
    )
}
