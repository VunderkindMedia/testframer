import * as React from 'react'
import styles from './date-picker.module.css'
import {CSSProperties, useCallback, useState, useEffect} from 'react'
import {getDayStartDate, getDateDDMMYYYYFormat, getIsoWeekday, getNextMonthDate, getPreviousMonthDate} from '../../../helpers/date'
import {useAppContext} from '../../../hooks/app'

const RU_LOCALE = 'ru'

enum DayTypes {
    Normal = 'normal',
    Available = 'available',
    Unavailable = 'unavailable'
}

export interface IDatePickerProps {
    defaultSelectedDate?: Date | null
    onSelectDate: (date: Date) => void
    availableDates?: Date[]
    shouldAlwaysSyncWithProps?: boolean
    shouldMarkCurrentDay?: boolean
    className?: string
    style?: CSSProperties
}

const DatePicker = React.forwardRef<HTMLDivElement, IDatePickerProps>((props: IDatePickerProps, ref) => {
    const {defaultSelectedDate, onSelectDate, availableDates, shouldAlwaysSyncWithProps, className, shouldMarkCurrentDay, style} = props

    const getInitialDate = useCallback((): Date => getDayStartDate(defaultSelectedDate ?? new Date()), [defaultSelectedDate])

    const [activeDate, setActiveDate] = useState<Date>(getInitialDate())
    const [selectedDate, setSelectedDate] = useState<Date>(getInitialDate())
    const {isMobile} = useAppContext()

    useEffect(() => {
        if (
            shouldAlwaysSyncWithProps &&
            defaultSelectedDate &&
            getDateDDMMYYYYFormat(defaultSelectedDate) !== getDateDDMMYYYYFormat(selectedDate)
        ) {
            const initialDate = getDayStartDate(defaultSelectedDate ?? new Date())
            setActiveDate(initialDate)
            setSelectedDate(initialDate)
        }
    }, [shouldAlwaysSyncWithProps, defaultSelectedDate, selectedDate])

    const getDayType = useCallback(
        (dayDate: Date): DayTypes => {
            if (availableDates) {
                const formatter = new Intl.DateTimeFormat(RU_LOCALE, {
                    year: 'numeric',
                    month: '2-digit',
                    day: '2-digit'
                })
                const dates = availableDates.map(date => formatter.format(date))
                const dateDate = formatter.format(dayDate)
                const isAvailable = dates.some(date => date === dateDate)

                return isAvailable ? DayTypes.Available : DayTypes.Unavailable
            }

            return DayTypes.Normal
        },
        [availableDates]
    )

    const handleOnSelectDate = useCallback(
        (date: Date): void => {
            if (getDayType(date) === DayTypes.Unavailable) {
                return
            }
            setSelectedDate(date)

            onSelectDate && onSelectDate(date)
        },
        [onSelectDate, getDayType]
    )

    const handleDaysContainerClick = useCallback(
        (e: React.MouseEvent<HTMLDivElement>): void => {
            const target = e.target as HTMLElement
            const day = +target.innerHTML
            if (!day) {
                return
            }
            const selectedDay = new Date(activeDate)
            selectedDay.setDate(day)

            handleOnSelectDate(selectedDay)
        },
        [handleOnSelectDate, activeDate]
    )

    const activeDateMonthName = new Intl.DateTimeFormat(RU_LOCALE, {month: 'long'}).format(activeDate)
    const activeDateMonth = activeDate.getMonth() + 1
    const activeDateYear = activeDate.getFullYear()
    const activeDateDaysInMonth = new Date(activeDateYear, activeDateMonth, 0).getDate()

    const selectedDateMonth = selectedDate.getMonth() + 1
    const selectedDateYear = selectedDate.getFullYear()
    const selectedDateDay = selectedDate.getDate()

    const currentDate = new Date()
    const isCurrentMonth = currentDate.getFullYear() === activeDateYear && currentDate.getMonth() + 1 === activeDateMonth

    const isSelectedMonth = selectedDateYear === activeDateYear && selectedDateMonth === activeDateMonth

    const currentDay = shouldMarkCurrentDay && isCurrentMonth ? currentDate.getDate() : -1
    const selectedDay = isSelectedMonth ? +selectedDateDay : -1

    const monthDates: Array<Date | null> = []
    const monthStartIsoWeekday = getIsoWeekday(new Date(activeDateYear, activeDateMonth - 1, 1))
    for (let i = 1; i < monthStartIsoWeekday; i++) {
        monthDates.push(null)
    }
    for (let i = 1; i <= activeDateDaysInMonth; i++) {
        const monthDate = new Date(activeDate)
        monthDate.setDate(i)
        monthDates.push(monthDate)
    }

    return (
        <div
            className={`${styles.datePicker} ${className ?? ''} ${isMobile ? styles.mobile : ''}`}
            ref={ref}
            style={style}
            data-test="date-picker">
            <div className={styles.header}>
                <div
                    className={`${styles.navButton} ${styles.prev}`}
                    data-test="date-picker-prev-button"
                    onClick={() => setActiveDate(getPreviousMonthDate(activeDate))}
                />
                <p className={styles.title}>{`${activeDateMonthName} ${activeDateYear}`}</p>
                <div
                    className={styles.navButton}
                    data-test="date-picker-next-button"
                    onClick={() => setActiveDate(getNextMonthDate(activeDate))}
                />
            </div>
            <div className={styles.days} onClick={handleDaysContainerClick}>
                <div className={`${styles.day} ${styles.weekday}`}>Пн</div>
                <div className={`${styles.day} ${styles.weekday}`}>Вт</div>
                <div className={`${styles.day} ${styles.weekday}`}>Ср</div>
                <div className={`${styles.day} ${styles.weekday}`}>Чт</div>
                <div className={`${styles.day} ${styles.weekday}`}>Пт</div>
                <div className={`${styles.day} ${styles.weekday}`}>Сб</div>
                <div className={`${styles.day} ${styles.weekday}`}>Вс</div>
                {monthDates.map((dayDate, index) => {
                    if (dayDate) {
                        const day = dayDate.getDate()
                        const dayType = getDayType(dayDate)

                        return (
                            <div
                                data-test={dayType === DayTypes.Available ? 'date-picker-available-day' : `date-picker-${day}`}
                                key={index}
                                className={`${styles.day}
                                     ${day === currentDay ? styles.current : ''}
                                     ${styles[dayType]}
                                     ${day === selectedDay ? styles.selected : ''}`}>
                                {day}
                            </div>
                        )
                    }
                    return <div key={index} className={`${styles.day} ${styles.empty}`} />
                })}
            </div>
        </div>
    )
})

DatePicker.displayName = 'DatePicker'

export {DatePicker}
