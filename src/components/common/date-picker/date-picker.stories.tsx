import React from 'react'
import {DatePicker as DatePickerComponent, IDatePickerProps} from './date-picker'
import {action} from '@storybook/addon-actions'
import {getAllDatesFromRange, getMonthEndDate} from '../../../helpers/date'

export default {
    title: 'DatePicker',
    component: DatePickerComponent,
    argTypes: {
        defaultSelectedDate: {
            table: {
                type: {summary: 'Date'}
            },
            control: {type: 'date'}
        },
        onSelectDate: {
            table: {
                type: {summary: '(date: Date) => void'}
            },
            control: {disable: true}
        },
        availableDates: {
            table: {
                type: {summary: 'Date[]'}
            },
            control: {disable: true}
        },
        shouldAlwaysSyncWithProps: {
            table: {
                type: {summary: 'boolean'}
            },
            control: {disable: true}
        },
        shouldMarkCurrentDay: {
            table: {
                type: {summary: 'boolean'}
            },
            control: {type: 'boolean'}
        },
        className: {
            table: {
                type: {summary: 'string'}
            },
            control: {disable: true}
        },
        style: {
            table: {
                type: {summary: 'CSSProperties'}
            },
            control: {disable: true}
        }
    },
    args: {
        defaultSelectedDate: new Date(),
        onSelectDate: '',
        availableDates: getAllDatesFromRange(new Date(), getMonthEndDate(new Date())),
        shouldAlwaysSyncWithProps: false,
        shouldMarkCurrentDay: true,
        className: '',
        style: {}
    }
}

export const DatePicker = (props: IDatePickerProps): JSX.Element => {
    return <DatePickerComponent {...props} onSelectDate={action('onSelectDate')} />
}
