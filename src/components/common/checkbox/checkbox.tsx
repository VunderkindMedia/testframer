import * as React from 'react'
import styles from './checkbox.module.css'
import {useEffect, useState} from 'react'
import {getClassNames} from '../../../helpers/css'

interface ICheckboxProps {
    isChecked?: boolean
    onChange?: (checked: boolean) => void
    title?: string
    isDisabled?: boolean
    isRequired?: boolean
    hasError?: boolean
    className?: string
}

export function Checkbox(props: ICheckboxProps): JSX.Element {
    const {onChange, title, isDisabled = false, isRequired = false, hasError = false, className} = props

    const [isChecked, setIsChecked] = useState(props.isChecked ?? false)

    useEffect(() => {
        setIsChecked(props.isChecked ?? false)
    }, [props.isChecked])

    return (
        <label className={getClassNames(`${styles.checkbox} ${className ?? ''}`, {[styles.error]: hasError})}>
            <input
                type="checkbox"
                className={styles.input}
                checked={isChecked}
                disabled={isDisabled}
                required={isRequired}
                onChange={e => {
                    e.stopPropagation()
                    if (isDisabled) {
                        return
                    }
                    onChange && onChange(!isChecked)
                    setIsChecked(!isChecked)
                }}
            />
            {title && <span className={styles.title}>{title}</span>}
        </label>
    )
}
