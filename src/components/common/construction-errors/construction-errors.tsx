import * as React from 'react'
import styles from './construction-errors.module.css'
import {Construction} from '../../../model/framer/construction'
import {SvgIconConstructionDrawer} from '../svg-icon-construction-drawer/svg-icon-construction-drawer'
import {ErrorTypes} from '../../../model/framer/error-types'

interface IConstructionErrorsProps {
    constructionIndex: number
    construction: Construction
    shouldShowGoToButton: boolean
    onSelectConstruction?: (construction: Construction) => void
    className?: string
}

export function ConstructionErrors(props: IConstructionErrorsProps): JSX.Element | null {
    const {construction, constructionIndex, onSelectConstruction, shouldShowGoToButton, className} = props

    if (construction.errors.length === 0) {
        return null
    }

    return (
        <div className={`${styles.constructionErrors} ${className ?? ''}`}>
            <div className={styles.header}>
                <SvgIconConstructionDrawer construction={construction} maxWidth={120} maxHeight={35} />
                <p>Конструкция {constructionIndex}</p>
            </div>
            {construction.errors.map((error, idx) => (
                <div key={idx} className={styles.error}>
                    <div className={`${error.errorType === ErrorTypes.Error ? 'error-icon' : 'warn-icon'}`} />
                    <div className={styles.errorText}>
                        <p className={styles.errorName}>{error.displayName}</p>
                        <p className={styles.errorMessage}>{error.displayMessage}</p>
                    </div>
                </div>
            ))}
            {shouldShowGoToButton && onSelectConstruction && (
                <p className={styles.constructionErrorsButton} onClick={() => onSelectConstruction(construction)}>
                    Перейти
                </p>
            )}
        </div>
    )
}
