import React, {useEffect, useCallback, useRef, useState} from 'react'
import {EnergyConfiguration, EnergyConfigurationTypes} from '../energy-configuration/energy-configuration'
import {EnergyFeature} from '../energy-feature/energy-feature'
import basicIImage from './resources/basic-i.png'
import basicCImage from './resources/basic-c.png'
import expertIImage from './resources/expert-i.png'
import expertCImage from './resources/expert-c.png'
import video from './resources/video.mp4'
import {useAppContext} from '../../../../hooks/app'
import styles from './energy-products.module.css'

const energyProducts = [
    <div className={styles.product} id="basic-i" key="basic-i">
        <div className={styles.productFeatures}>
            <EnergyConfiguration type={EnergyConfigurationTypes.BasicI} size="l" className={styles.productTitle} />
            <p className={`${styles.feature} ${styles.silverGlass}`}>Стекло с серебром</p>
            <hr className={`${styles.line} ${styles.glass}`} />
            <p className={styles.featureDescription}>Сокращает теплопотери на 30%</p>
            <p className={`${styles.feature} ${styles.profile}`}>5 камерный профиль</p>
            <hr className={`${styles.line} ${styles.profile}`} />
            <p className={styles.featureDescription}>Увеличивает теплозащиту на 20% в сравнении с 3-камерными профилями</p>
        </div>
        <div className={styles.productImage}>
            <img src={basicIImage} alt="Basic I" width="590" height="590" />
        </div>
    </div>,
    <div className={styles.product} id="basic-c" key="basic-c">
        <div className={styles.productFeatures}>
            <EnergyConfiguration type={EnergyConfigurationTypes.BasicC} size="l" className={styles.productTitle} />
            <p className={`${styles.feature} ${styles.multiGlass}`}>Мультифункциональное стекло</p>
            <hr className={`${styles.line} ${styles.glass}`} />
            <p className={styles.featureDescription}>
                Сокращает теплопотери на 30%
                <br />
                Защищает от вредных УФ-лучей
            </p>
            <p className={`${styles.feature} ${styles.profile}`}>5 камерный профиль</p>
            <hr className={`${styles.line} ${styles.profile}`} />
            <p className={styles.featureDescription}>Увеличивает теплозащиту на 20% в сравнении с 3-камерными профилями</p>
        </div>
        <div className={styles.productImage}>
            <img src={basicCImage} alt="Basic C" width="590" height="590" />
        </div>
    </div>,
    <div className={styles.product} id="expert-i" key="expert-i">
        <div className={styles.productFeatures}>
            <EnergyConfiguration type={EnergyConfigurationTypes.ExpertI} size="l" className={`${styles.productTitle} ${styles.expert}`} />
            <p className={`${styles.feature} ${styles.silverGlass}`}>Стекло с серебром</p>
            <hr className={`${styles.line} ${styles.glass}`} />
            <p className={`${styles.featureDescription} ${styles.expert}`}>Сокращает теплопотери на 30%</p>
            <p className={`${styles.feature} ${styles.frame}`}>Тёплая рамка</p>
            <hr className={`${styles.line} ${styles.frame}`} />
            <p className={`${styles.featureDescription} ${styles.expert}`}>Позволяет предотвратить образование конденсата и наледи</p>
            <p className={`${styles.feature} ${styles.profile}`}>Профиль шириной 76 мм</p>
            <hr className={`${styles.line} ${styles.profileWidth}`} />
            <p className={`${styles.featureDescription} ${styles.expert}`}>Дает возможность установки более широкого и теплого стеклопакета</p>
        </div>
        <div className={styles.productImage}>
            <img src={expertIImage} alt="Expert I" width="590" height="590" />
        </div>
    </div>,
    <div className={styles.product} id="expert-c" key="expert-c">
        <div className={styles.productFeatures}>
            <EnergyConfiguration type={EnergyConfigurationTypes.ExpertC} size="l" className={`${styles.productTitle} ${styles.expert}`} />
            <p className={`${styles.feature} ${styles.multiGlass}`}>Мультифункциональное стекло</p>
            <hr className={`${styles.line} ${styles.glass}`} />
            <p className={`${styles.featureDescription} ${styles.expert}`}>
                Сокращает теплопотери на 30%
                <br />
                Защищает от вредных УФ-лучей
            </p>
            <p className={`${styles.feature} ${styles.frame}`}>Тёплая рамка</p>
            <hr className={`${styles.line} ${styles.frame} ${styles.expertC}`} />
            <p className={`${styles.featureDescription} ${styles.expert}`}>Позволяет предотвратить образование конденсата и наледи</p>
            <p className={`${styles.feature} ${styles.profile}`}>Профиль шириной 76 мм</p>
            <hr className={`${styles.line} ${styles.profileWidth} ${styles.expertC}`} />
            <p className={`${styles.featureDescription} ${styles.expert}`}>Дает возможность установки более широкого и теплого стеклопакета</p>
        </div>
        <div className={styles.productImage}>
            <img src={expertCImage} alt="Expert C" width="590" height="590" />
        </div>
    </div>
]

type TEnergyProducts = 'basic-i' | 'basic-c' | 'expert-i' | 'expert-c'

const initState: {[key in TEnergyProducts]: boolean} = {
    'basic-i': false,
    'basic-c': false,
    'expert-i': false,
    'expert-c': false
}

export const EnergyProducts = (): JSX.Element => {
    const videoRef = useRef<HTMLVideoElement | null>(null)
    const [isScrollThrottled, setIsScrollThrottled] = useState(false)
    const [openStates, setOpenStates] = useState(initState)
    const {isMobile} = useAppContext()

    const playVideo = useCallback(() => {
        if (isScrollThrottled || !videoRef || !videoRef.current) {
            return
        }
        setIsScrollThrottled(true)
        setTimeout(() => {
            const video = videoRef.current as HTMLMediaElement
            const container = document.querySelector('.app__content > div') as HTMLDivElement
            if (
                !video.autoplay &&
                container.scrollTop <= video.offsetTop &&
                container.scrollTop + window.innerHeight >= video.offsetTop + video.offsetHeight
            ) {
                video.autoplay = true
                // eslint-disable-next-line @typescript-eslint/no-floating-promises
                video.play()
            }
            setIsScrollThrottled(false)
        }, 500)
    }, [isScrollThrottled, setIsScrollThrottled])

    const toggle = useCallback(
        (key: TEnergyProducts, value: boolean) => {
            setOpenStates({...openStates, [key]: value})
        },
        [openStates]
    )

    useEffect(() => {
        if (isMobile) {
            return
        }
        const container = document.querySelector('.app__content > div') as HTMLDivElement
        container.addEventListener('scroll', playVideo)

        return () => !isMobile && container.removeEventListener('scroll', playVideo)
    }, [playVideo, isMobile])

    const energyFeatures = [
        <EnergyFeature
            type={EnergyConfigurationTypes.BasicI}
            thermalProtection={2}
            sunProtection={2}
            condensationProtection={2}
            profile={70}
            frame="Алюминиевая"
            glass="Низкоэмиссионное"
            application="Общеобразовательные,<br/> дошкольные учреждения"
            link="basic-i"
            key="feature-basic-i"
            isOpened={openStates['basic-i']}
            onToggle={value => toggle('basic-i', value)}
        />,
        <EnergyFeature
            type={EnergyConfigurationTypes.BasicC}
            thermalProtection={2}
            sunProtection={3}
            condensationProtection={2}
            profile={70}
            frame="Алюминиевая"
            glass="Мультифункциональное"
            application="ТРЦ, Магазины"
            link="basic-c"
            key="feature-basic-c"
            isOpened={openStates['basic-c']}
            onToggle={value => toggle('basic-c', value)}
        />,
        <EnergyFeature
            type={EnergyConfigurationTypes.ExpertI}
            thermalProtection={3}
            sunProtection={2}
            condensationProtection={3}
            profile={76}
            frame="ПВХ"
            glass="Низкоэмиссионное"
            application="Многоквартирные дома, ИЖС"
            link="expert-i"
            key="feature-expert-i"
            isOpened={openStates['expert-i']}
            onToggle={value => toggle('expert-i', value)}
        />,
        <EnergyFeature
            type={EnergyConfigurationTypes.ExpertC}
            thermalProtection={3}
            sunProtection={3}
            condensationProtection={3}
            profile={76}
            frame="ПВХ"
            glass="Мультифункциональное"
            application="Многоквартирные дома, ИЖС, солнечные стороны, 1-е этажи"
            link="expert-c"
            key="feature-expert-c"
            isOpened={openStates['expert-c']}
            onToggle={value => toggle('expert-c', value)}
        />
    ]

    return (
        <div className={`${styles.page} ${isMobile && styles.mobile}`}>
            <div className={styles.heading}>
                <h2 className={`${styles.title} ${styles.headingTitle}`}>
                    <span>Окна</span> <span>Energy</span>
                </h2>
                <p className={styles.headingText}>
                    Серия энергоэффективных окон ENERGY разработана специально для климатических условий Поволжья и Урала. Каждая модель серии
                    обладает уникальными теплотехническими характеристиками.
                    <br /> Выбери свою модель ENERGY.
                </p>
                {!isMobile && <div className={styles.cards}>{energyFeatures}</div>}
            </div>
            {!isMobile && (
                <div className={styles.video}>
                    <video ref={videoRef} src={video} muted loop />
                </div>
            )}
            <div className={styles.products}>
                <h2 className={`${styles.title} ${styles.productsTitle}`}>
                    <span>Окна</span> <span>Energy</span>
                </h2>
                {!isMobile && energyProducts}
                {isMobile &&
                    energyProducts.map((product, i) => (
                        <div key={product.key} className={styles.container}>
                            {energyFeatures[i]}
                            {product.key && openStates[product.key as TEnergyProducts] && product}
                        </div>
                    ))}
            </div>
        </div>
    )
}
