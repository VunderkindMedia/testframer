import React from 'react'
import {Link} from 'react-router-dom'
import {useLocation} from 'react-router'
import {NAV_ENERGY_PROGRAM, NAV_ENERGY_PRODUCTS} from '../../../constants/navigation'
import {EnergyProgram} from './energy-program/energy-program'
import {EnergyProducts} from './energy-products/energy-products'
import {useAppContext} from '../../../hooks/app'
import styles from './energy-page.module.css'

const tabs = [
    {
        id: 1,
        path: NAV_ENERGY_PROGRAM,
        title: 'Механика проведения бонусной программы ENERGY',
        content: <EnergyProgram />
    },
    {
        id: 2,
        path: NAV_ENERGY_PRODUCTS,
        title: 'Комплектация окон ENERGY',
        content: <EnergyProducts />
    }
]
export const EnergyPage = (): JSX.Element => {
    const location = useLocation()
    const activeTab = tabs.find(({path}) => path === location.pathname)
    const {isMobile} = useAppContext()

    return (
        <div className={`${styles.page} ${isMobile && styles.mobile}`}>
            <div className={styles.tabs}>
                {tabs.map(({id, path, title}) => (
                    <Link key={id} to={path} className={`${styles.tab} ${path === activeTab?.path ? styles.active : ''}`}>
                        {title}
                    </Link>
                ))}
            </div>
            <div>{activeTab?.content}</div>
        </div>
    )
}
