import React, {useMemo} from 'react'
import styles from './value-indicator.module.css'

type TFeatureType = 'thermal' | 'sun' | 'condensation'

interface IValueIndicatorProps {
    type: TFeatureType
    value: number
    className?: string
}

const featureNames = {
    thermal: 'Теплозащита',
    sun: 'Солнцезащита',
    condensation: 'Защита от конденсата, наледи'
}

export const ValueIndicator = (props: IValueIndicatorProps): JSX.Element => {
    const {type, value, className} = props

    const values = useMemo(() => Array.from({length: 3}, (v, k) => k + 1), [])

    return (
        <div className={className ?? ''}>
            <p className={styles.title}>{featureNames[type]}</p>
            <div className={styles.indicator}>
                {values.map(i => (i <= value ? <div key={i} className={`${styles.point} ${styles[type]}`}></div> : ''))}
            </div>
        </div>
    )
}
