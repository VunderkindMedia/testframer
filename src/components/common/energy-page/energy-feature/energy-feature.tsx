import React, {useCallback} from 'react'
import {EnergyConfiguration, EnergyConfigurationTypes} from '../energy-configuration/energy-configuration'
import {ValueIndicator} from './value-indicator/value-indicator'
import {useAppContext} from '../../../../hooks/app'
import styles from './energy-feature.module.css'

interface IEnergyFeatureProps {
    type: EnergyConfigurationTypes
    thermalProtection: number
    sunProtection: number
    condensationProtection: number
    profile: number
    frame: string
    glass: string
    application: string
    link: string
    isOpened?: boolean
    onToggle?: (value: boolean) => void
}

export const EnergyFeature = (props: IEnergyFeatureProps): JSX.Element => {
    const {
        type,
        thermalProtection,
        sunProtection,
        condensationProtection,
        profile,
        frame,
        glass,
        application,
        link,
        isOpened = true,
        onToggle
    } = props
    const {isMobile} = useAppContext()

    const clickHandler = useCallback(
        (e: React.SyntheticEvent) => {
            e.preventDefault()
            const el = document.querySelector(`#${link}`) as HTMLDivElement
            const container = document.querySelector('.app__content > div') as HTMLDivElement
            const headerHeight = window.getComputedStyle(container).getPropertyValue('--header-height').replace(/\D/g, '')
            container.scrollTo({top: el.offsetTop - +headerHeight, behavior: 'smooth'})
        },
        [link]
    )

    return (
        <div className={`${styles.container} ${isMobile && styles.mobile} ${isMobile && !isOpened && styles.closed}`}>
            <EnergyConfiguration type={type} size="m" className={styles.type} />
            {isMobile && <button className={`${styles.toggle} ${isOpened && styles.open}`} onClick={() => onToggle && onToggle(!isOpened)} />}
            {(!isMobile || (isMobile && isOpened)) && (
                <>
                    <ValueIndicator type="thermal" value={thermalProtection} className={styles.indicator} />
                    <ValueIndicator type="sun" value={sunProtection} className={styles.indicator} />
                    <ValueIndicator type="condensation" value={condensationProtection} className={styles.indicator} />
                    <ul className={styles.list}>
                        <li>
                            Профиль<span className={styles.space}></span>
                            {profile}
                        </li>
                        <li>
                            Рамка<span className={styles.space}></span>
                            {frame}
                        </li>
                        <li>
                            Стекло<span className={styles.space}></span>
                            {glass}
                        </li>
                    </ul>
                    <p className={styles.application}>
                        <b>Применение</b>
                        {/* eslint-disable-next-line @typescript-eslint/naming-convention */}
                        <span dangerouslySetInnerHTML={{__html: application}}></span>
                    </p>
                    {!isMobile && (
                        <a href="" className={styles.link} onClick={clickHandler}>
                            Подробнее
                        </a>
                    )}
                </>
            )}
        </div>
    )
}
