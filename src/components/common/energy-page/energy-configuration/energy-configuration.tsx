import React from 'react'
import styles from './energy-configuration.module.css'

export enum EnergyConfigurationTypes {
    BasicI = 'Basic I',
    BasicC = 'Basic C',
    ExpertI = 'Expert I',
    ExpertC = 'Expert C'
}

interface IEnergyConfigurationProps {
    size: 'm' | 'l'
    type: EnergyConfigurationTypes
    className?: string
}

export const EnergyConfiguration = (props: IEnergyConfigurationProps): JSX.Element => {
    const {size, type, className} = props
    const typeName = type.replace(/\s/g, '-').toLowerCase()

    return <span className={`${styles.configuration} ${styles[size]} ${styles[typeName]} ${className ?? ''}`}>{type}</span>
}
