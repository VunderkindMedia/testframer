import * as React from 'react'
import icon1 from './resources/icon_1.svg'
import icon2 from './resources/icon_2.svg'
import icon3 from './resources/icon_3.svg'
import {Button} from '../../button/button'
import {useAppContext} from '../../../../hooks/app'
import {getClassNames} from '../../../../helpers/css'
import {HeaderTitle} from '../../../mobile/header/header-title'
import {useIsDemoUser} from '../../../../hooks/permissions'
import styles from './stage-one.module.css'

interface IStageOneProps {
    onNext: () => void
}

export function StageOne(props: IStageOneProps): JSX.Element {
    const {onNext} = props

    const {isMobile} = useAppContext()
    const isDemoUser = useIsDemoUser()

    return (
        <div className={getClassNames(styles.stage, {[styles.mobile]: isMobile})}>
            {isMobile ? <HeaderTitle>Добро пожаловать во framer!</HeaderTitle> : <p className={styles.header}>Добро пожаловать во framer!</p>}
            <p className={styles.title}>Здесь вы сможете</p>
            <div className={styles.content}>
                <div className={styles.icon}>
                    <img src={icon1} alt="" draggable={false} width="116" height="85" />
                    <p className={styles.description}>
                        Рассчитать точную
                        <br /> стоимость окон
                    </p>
                </div>
                <div className={styles.icon}>
                    <img src={icon2} alt="" draggable={false} width="116" height="85" />
                    <p className={styles.description}>Формировать коммерческие предложения для Ваших клиентов</p>
                </div>
                <div className={styles.icon}>
                    <img src={icon3} alt="" draggable={false} width="116" height="85" />
                    <p className={styles.description}>
                        Отправить заказы
                        <br /> в производство и отслеживать их статус
                    </p>
                </div>
            </div>
            {isDemoUser && (
                <div className={styles.demoMessage}>
                    <p>
                        Вы вошли в демо-аккаунт, здесь вы можете ознакомиться с функционалом сервиса.
                        <br /> Цены на конструкции приблизительные (без учета скидок для оптовых покупателей).
                    </p>
                    <p>Для того, чтобы пополнить баланс и передать заказ в производство, необходимо зарегистрироваться</p>
                </div>
            )}
            <Button className={styles.nextButton} buttonStyle="with-fill" onClick={onNext}>
                Далее
            </Button>
        </div>
    )
}
