import * as React from 'react'
import {Button} from '../../button/button'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {useGoTo} from '../../../../hooks/router'
import {NAV_PROFILE} from '../../../../constants/navigation'
import {ProfileTabIds} from '../../profile-page/tabs'
import {getDefaultOrdersLink} from '../../../../helpers/orders'
import styles from './stage-two.module.css'
import {useAppContext} from '../../../../hooks/app'
import {getClassNames} from '../../../../helpers/css'

export function StageTwo(): JSX.Element {
    const ordersOnPage = useSelector((state: IAppState) => state.orders.ordersOnPage)

    const goTo = useGoTo()
    const {isMobile} = useAppContext()

    return (
        <div className={getClassNames(styles.stage, {[styles.mobile]: isMobile})}>
            <p className={styles.title}>Для начала работы необходимо настроить пару моментов:</p>
            <div className={`${styles.step} ${styles.step1}`}>
                <div>
                    <div className={styles.bigNumber} />
                </div>
                <div>
                    <p className={styles.stepText}>
                        <span>
                            Укажите наценку на конструкции для расчета конечной
                            <br />
                            стоимости заказа для ваших клиентов.
                        </span>
                        <br />
                    </p>
                </div>
            </div>
            <div className={`${styles.step} ${styles.step2}`}>
                <div>
                    <div className={styles.bigNumber} />
                </div>
                <div>
                    <p className={styles.stepText}>
                        <span>
                            Для удобства работы в КП можно добавлять услуги, о которых
                            <br />
                            Вы договорились с клиентом.
                        </span>
                        <br />
                        <br />
                        Это может быть все, что угодно: монтаж конструкций, вывоз
                        <br />
                        мусора, установка отливов, отделка комнаты.
                        <br />
                        <br />
                        Для удобства указания наиболее востребованных услуг
                        <br />
                        предлагаем Вам заполнить справочник, чтобы впоследствии
                        <br />
                        иметь возможность их выбирать.
                    </p>
                </div>
            </div>
            <div className={styles.buttons}>
                <Button buttonStyle="with-border" onClick={() => goTo(`${NAV_PROFILE}?activeTab=${ProfileTabIds.Margins}`)}>
                    Указать наценку и добавить услуги
                </Button>
                <button className={styles.skipButton} onClick={() => goTo(getDefaultOrdersLink(ordersOnPage))}>
                    Пропустить
                </button>
            </div>
        </div>
    )
}
