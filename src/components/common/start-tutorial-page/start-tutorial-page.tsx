import * as React from 'react'
import {useEffect, useCallback} from 'react'
import {TUTORIAL_SHOWED} from '../../../constants/local-storage'
import {useAppContext, useSetOnboardingStep} from '../../../hooks/app'
import {StageOne} from './stage-one/stage-one'
import {getClassNames} from '../../../helpers/css'
import styles from './start-tutorial-page.module.css'

export function StartTutorialPage(): JSX.Element {
    const {isMobile} = useAppContext()
    const setOnboardingStep = useSetOnboardingStep()

    useEffect(() => {
        localStorage.setItem(TUTORIAL_SHOWED, 'yes')

        return () => {
            setOnboardingStep(0)
        }
    }, [setOnboardingStep])

    const goToNextHandler = useCallback(() => {
        setOnboardingStep(0)
        window.scroll(0, 0)
    }, [setOnboardingStep])

    return (
        <div className={getClassNames(styles.page, {[styles.mobile]: isMobile})}>
            <StageOne onNext={goToNextHandler} />
        </div>
    )
}
