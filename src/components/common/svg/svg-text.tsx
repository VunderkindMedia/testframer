import * as React from 'react'
import {Point} from '../../../model/geometry/point'

interface ISvgTextProps {
    text: string
    position: Point
    rotateAngle?: number
    textColor?: string
    fontFamily?: string
    textAnchor?: 'start' | 'middle' | 'end'
    fontSize: number
}

export function SvgText(props: ISvgTextProps): JSX.Element {
    const {text, position, rotateAngle, fontSize, fontFamily, textAnchor, textColor} = props

    return (
        <text
            x={position.x}
            y={position.y}
            height={fontSize}
            fill={textColor ?? '#000'}
            alignmentBaseline={'middle'}
            transform={`translate(${position.x} ${position.y})
                   scale(1 -1)
                   translate(-${position.x} -${position.y})
                   rotate(${rotateAngle ?? 0} ${position.x},${position.y})`}
            textAnchor={textAnchor ?? 'start'}
            fontSize={fontSize}
            fontFamily={fontFamily ?? 'Roboto'}
            style={{whiteSpace: 'pre'}}>
            {text}
        </text>
    )
}
