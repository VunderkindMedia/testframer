import * as React from 'react'
import {Polygon} from '../../../model/geometry/polygon'
import {getSvgPoints} from '../../../helpers/point'

interface ISvgPolygonProps {
    polygon: Polygon
    className?: string
    fill?: string
    stroke?: string
    lineWidth?: number
    opacity?: number
    onClick?: (e: React.MouseEvent<SVGPolygonElement>) => void
    onHover?: (e: React.MouseEvent<SVGPolygonElement>) => void
}

export function SvgPolygon(props: ISvgPolygonProps): JSX.Element {
    const {polygon, className, fill, lineWidth, stroke, onClick, onHover} = props

    return (
        <polygon
            points={getSvgPoints(polygon.points)}
            className={className}
            fill={fill ?? '#fff'}
            stroke={stroke ?? '#000'}
            strokeWidth={lineWidth ?? 1}
            onClick={onClick}
            onMouseOver={onHover}
        />
    )
}
