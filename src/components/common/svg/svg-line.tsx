import * as React from 'react'
import {Line} from '../../../model/geometry/line'
import {Point} from '../../../model/geometry/point'

interface ISvgLineProps {
    line: Line
    lineWidth?: number
    stroke?: string
    rotateAngle?: number
    rotatePoint?: Point
}

export function SvgLine(props: ISvgLineProps): JSX.Element {
    const {line, lineWidth, stroke, rotateAngle, rotatePoint} = props

    return (
        <line
            x1={line.start.x}
            y1={line.start.y}
            x2={line.end.x}
            y2={line.end.y}
            transform={`rotate(${rotateAngle ?? 0}, ${rotatePoint?.x ?? 0}, ${rotatePoint?.y ?? 0})`}
            strokeWidth={lineWidth ?? 1}
            stroke={stroke ?? '#000'}
        />
    )
}
