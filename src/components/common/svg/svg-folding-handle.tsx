import * as React from 'react'
import {Handle} from '../../../model/handle'
import {SvgRect} from './svg-rect'
import {getHandleColorHex} from '../../../model/framer/handle-colors'
import {Rectangle} from '../../../model/geometry/rectangle'
import {Point} from '../../../model/geometry/point'
import {Size} from '../../../model/size'
import {ModelParts} from '../../../model/framer/model-parts'
import {ISvgHandleProps} from './svg-handle'
import {getColor} from '../../../helpers/color'

export function SvgFoldingHandle(props: ISvgHandleProps): JSX.Element | null {
    const {frameFilling, currentContext, highlightedContext, lineWidth, xOffset, yOffset, onSelectContext, onHoverContext, isClickable} = props

    const handle = new Handle(frameFilling)
    const {hostBeam} = handle

    if (!hostBeam) {
        return null
    }

    const SIZE_MULTIPLIER = 3
    const HANDLE_BASE_WIDTH = 10 * SIZE_MULTIPLIER
    const HANDLE_BASE_HEIGHT = 44 * SIZE_MULTIPLIER
    const HANDLE_BASE_RADIUS = 2 * SIZE_MULTIPLIER

    const HANDLE_WIDTH = 3 * SIZE_MULTIPLIER
    const HANDLE_HEIGHT = 20 * SIZE_MULTIPLIER
    const HANDLE_RADIUS = 2 * SIZE_MULTIPLIER

    const CLICKABLE_WIDTH = frameFilling.profile.getGeometryParameters(ModelParts.Stvorka).a
    const CLICKABLE_HEIGHT = HANDLE_BASE_HEIGHT

    const x = handle.refPoint.x + xOffset
    const y = handle.refPoint.y + yOffset

    const rotatePoint = new Point(x, y)

    return (
        <>
            <SvgRect
                className="svg-handle-base"
                rectangle={
                    new Rectangle(new Point(x - HANDLE_BASE_WIDTH / 2, y - HANDLE_BASE_HEIGHT), new Size(HANDLE_BASE_WIDTH, HANDLE_BASE_HEIGHT))
                }
                fill={getColor(
                    getHandleColorHex(handle.color),
                    handle.nodeId === highlightedContext?.nodeId,
                    handle.nodeId === currentContext?.nodeId
                )}
                rotateAngle={handle.angle}
                rotatePoint={rotatePoint}
                borderRadiusX={HANDLE_BASE_RADIUS}
                borderRadiusY={HANDLE_BASE_RADIUS}
                lineWidth={lineWidth}
            />
            <SvgRect
                className="svg-handle"
                rectangle={
                    new Rectangle(
                        new Point(x - HANDLE_WIDTH / 2, y - (HANDLE_BASE_HEIGHT + HANDLE_HEIGHT) / 2),
                        new Size(HANDLE_WIDTH, HANDLE_HEIGHT)
                    )
                }
                fill="#000"
                rotateAngle={handle.angle}
                rotatePoint={rotatePoint}
                borderRadiusX={HANDLE_RADIUS * 2}
                borderRadiusY={HANDLE_RADIUS}
                lineWidth={lineWidth}
            />
            <SvgRect
                className={`svg-handle-clickable ${isClickable && 'clickable'}`}
                rectangle={
                    new Rectangle(new Point(x - CLICKABLE_WIDTH / 2, y - HANDLE_BASE_HEIGHT), new Size(CLICKABLE_WIDTH, CLICKABLE_HEIGHT))
                }
                onClick={() => isClickable && onSelectContext && onSelectContext(handle)}
                onHover={() => isClickable && onHoverContext && onHoverContext(handle)}
                fill="transparent"
                stroke="transparent"
                rotateAngle={handle.angle}
                rotatePoint={rotatePoint}
            />
        </>
    )
}
