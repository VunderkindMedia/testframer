import * as React from 'react'
import {IContext} from '../../../model/i-context'
import {SolidFilling} from '../../../model/framer/solid-filling'
import {SvgPolygon} from './svg-polygon'
import {getColor} from '../../../helpers/color'
import {getInnerColorUserParameter} from '../../../helpers/builder'

interface ISvgSolidFillingProps {
    currentContext?: IContext | null
    highlightedContext?: IContext | null
    lineWidth: number
    solidFilling: SolidFilling
    xOffset: number
    yOffset: number
    onSelectContext?: (context: IContext | null) => void
    onHoverContext?: (context: IContext | null) => void
}

export function SvgSolidFilling(props: ISvgSolidFillingProps): JSX.Element {
    const {currentContext, highlightedContext, solidFilling, lineWidth, xOffset, yOffset, onSelectContext, onHoverContext} = props

    const colorParam = getInnerColorUserParameter(solidFilling)

    return (
        <SvgPolygon
            className="clickable svg-solid-filling"
            polygon={solidFilling.containerPolygon.innerPolygon.withOffset(xOffset, yOffset)}
            fill={getColor(
                colorParam?.hex || 'transparent',
                solidFilling.nodeId === highlightedContext?.nodeId,
                solidFilling.nodeId === currentContext?.nodeId
            )}
            lineWidth={lineWidth}
            opacity={0.8}
            onClick={() => onSelectContext && onSelectContext(solidFilling)}
            onHover={() => onHoverContext && onHoverContext(solidFilling)}
        />
    )
}
