import * as React from 'react'
import {Size} from '../../../model/size'
import {SvgRect} from './svg-rect'
import {Rectangle} from '../../../model/geometry/rectangle'
import {Point} from '../../../model/geometry/point'

interface ISvgProps {
    viewboxSize: Size
    size: Size
    children?: React.ReactNode
}

export function Svg(props: ISvgProps): JSX.Element | null {
    const {viewboxSize, size, children} = props

    if (viewboxSize.width <= 0 || viewboxSize.height <= 0 || size.width <= 0 || size.height <= 0) {
        return null
    }

    return (
        <svg width={size.width} height={size.height} viewBox={`0 0 ${viewboxSize.width} ${viewboxSize.height}`}>
            <defs>
                <pattern id="aluminium" x={0} y={0} width={20} height={20} patternUnits="userSpaceOnUse">
                    <SvgRect rectangle={new Rectangle(new Point(0, 0), new Size(10, 10))} opacity={1} fill="red" stroke={'transparent'} />
                    <SvgRect rectangle={new Rectangle(new Point(10, 10), new Size(10, 10))} opacity={1} fill="red" stroke={'transparent'} />
                </pattern>
            </defs>
            {children}
        </svg>
    )
}
