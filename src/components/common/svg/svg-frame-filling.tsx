import * as React from 'react'
import {SvgGroup} from './svg-group'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {IContext} from '../../../model/i-context'
import {Polygon} from '../../../model/geometry/polygon'
import {SvgPolygon} from './svg-polygon'
import {SvgSolidFilling} from './svg-solid-filling'
import {SvgOpeningSystem} from './svg-opening-system'
import {SvgHinges} from './svg-hinges'
import {SvgSolidFillingText} from './svg-solid-filling-text'
import {SvgMoskitka} from './svg-moskitka'
import {ModelParts} from '../../../model/framer/model-parts'
import {Impost} from '../../../model/framer/impost'
import {SvgHandle} from './svg-handle'
import {Point} from '../../../model/geometry/point'
import {PointsOfView} from '../../../model/points-of-view'
import {SolidFilling} from '../../../model/framer/solid-filling'
import {getColor} from '../../../helpers/color'
import {useCallback} from 'react'
import {Rectangle} from '../../../model/geometry/rectangle'
import {Size} from '../../../model/size'
import {SvgRect} from './svg-rect'
import {Product} from '../../../model/framer/product'
import {Beam} from '../../../model/framer/beam'
import {BEAD_WIDTH} from '../../../constants/geometry-params'

interface ISvgFrameFillingProps {
    pointOfView: PointsOfView
    product: Product
    frameFilling: FrameFilling
    insideLaminationColor: string
    outsideLaminationColor: string
    currentContext?: IContext | null
    highlightedContext?: IContext | null
    scale: number
    xOffset: number
    yOffset: number
    lineWidth: number
    fontSize: number
    solidFillingCounter: () => number
    frameFillingCounter: () => number
    textColor: string | undefined
    onSelectContext?: (context: IContext | null) => void
    onHoverContext?: (context: IContext | null) => void
    shtulp?: Impost
    defaultPointOfView?: PointsOfView
    isEntranceDoor: boolean
}

export function SvgFrameFilling(props: ISvgFrameFillingProps): JSX.Element {
    const {
        currentContext,
        highlightedContext,
        defaultPointOfView,
        product,
        frameFilling,
        insideLaminationColor,
        outsideLaminationColor,
        fontSize,
        textColor,
        scale,
        xOffset,
        yOffset,
        lineWidth,
        solidFillingCounter,
        frameFillingCounter,
        onSelectContext,
        onHoverContext,
        shtulp,
        isEntranceDoor
    } = props

    const {isAluminium} = product

    const getAluminiumPattern = useCallback(
        (id: string, fill: string) => {
            const radius = 0.5 / scale
            const patternSize = 10 / scale

            return (
                <defs>
                    <pattern id={`${id}`} x={0} y={0} width={patternSize} height={patternSize} patternUnits="userSpaceOnUse">
                        <SvgRect
                            rectangle={new Rectangle(new Point(0, 0), new Size(patternSize, patternSize))}
                            fill={fill}
                            stroke="transparent"
                        />
                        <circle cx={radius} cy={radius} r={radius} fill="#000" />
                        <circle cx={patternSize / 2 + radius} cy={patternSize / 2 + radius} r={radius} fill="#000" />
                    </pattern>
                </defs>
            )
        },
        [scale]
    )

    const getBeadsFillingColor = (points: Point[]): string => {
        const contexts: string[] = [frameFilling.nodeId]

        const hasCommonSide = (polygon: Polygon): boolean => points.filter(point => polygon.hasPointOnSide(point)).length === 2

        frameFilling.impostBeams.forEach(impost => {
            if (hasCommonSide(Polygon.buildFromPoints(impost.innerDrawingPoints))) {
                contexts.push(impost.nodeId)
            }
        })

        return getColor(
            defaultPointOfView === PointsOfView.Inside ? insideLaminationColor : outsideLaminationColor,
            Boolean(highlightedContext?.nodeId && contexts.includes(highlightedContext?.nodeId)),
            Boolean(currentContext?.nodeId && contexts.includes(currentContext?.nodeId))
        )
    }

    const moskitkaCornersPolygons: Polygon[] = []

    if (frameFilling.isMoskitkaRoot && frameFilling.containerPolygon.isRect) {
        const {leftBottomPoint, leftTopPoint, rightBottomPoint, rightTopPoint} = frameFilling.rect

        const beamWidth = frameFilling.frameBeams[0].width
        const c = 1.6

        moskitkaCornersPolygons.push(
            Polygon.buildFromPoints([
                leftBottomPoint,
                leftBottomPoint.withOffset(0, beamWidth * c),
                leftBottomPoint.withOffset(beamWidth, beamWidth * c),
                leftBottomPoint.withOffset(beamWidth, beamWidth),
                leftBottomPoint.withOffset(beamWidth * c, beamWidth),
                leftBottomPoint.withOffset(beamWidth * c, 0)
            ]).withOffset(xOffset, yOffset),
            Polygon.buildFromPoints([
                leftTopPoint,
                leftTopPoint.withOffset(0, -beamWidth * c),
                leftTopPoint.withOffset(beamWidth, -beamWidth * c),
                leftTopPoint.withOffset(beamWidth, -beamWidth),
                leftTopPoint.withOffset(beamWidth * c, -beamWidth),
                leftTopPoint.withOffset(beamWidth * c, 0)
            ]).withOffset(xOffset, yOffset),
            Polygon.buildFromPoints([
                rightBottomPoint,
                rightBottomPoint.withOffset(0, beamWidth * c),
                rightBottomPoint.withOffset(-beamWidth, beamWidth * c),
                rightBottomPoint.withOffset(-beamWidth, beamWidth),
                rightBottomPoint.withOffset(-beamWidth * c, beamWidth),
                rightBottomPoint.withOffset(-beamWidth * c, 0)
            ]).withOffset(xOffset, yOffset),
            Polygon.buildFromPoints([
                rightTopPoint,
                rightTopPoint.withOffset(0, -beamWidth * c),
                rightTopPoint.withOffset(-beamWidth, -beamWidth * c),
                rightTopPoint.withOffset(-beamWidth, -beamWidth),
                rightTopPoint.withOffset(-beamWidth * c, -beamWidth),
                rightTopPoint.withOffset(-beamWidth * c, 0)
            ]).withOffset(xOffset, yOffset)
        )
    }

    const solidFillings: SolidFilling[] = []
    const leafFillings: FrameFilling[] = []

    frameFilling.innerFillings.forEach(innerFilling => {
        innerFilling.solid && solidFillings.push(innerFilling.solid)
        innerFilling.leaf && leafFillings.push(innerFilling.leaf)
    })

    const moskitkaView = (
        <SvgMoskitka
            frameFilling={frameFilling}
            outsideLaminationColor={outsideLaminationColor}
            scale={scale}
            xOffset={xOffset}
            yOffset={yOffset}
        />
    )

    const solidFillingsView = (
        <>
            {solidFillings.map(solidFilling => (
                <SvgSolidFilling
                    key={solidFilling.nodeId}
                    currentContext={currentContext}
                    highlightedContext={highlightedContext}
                    solidFilling={solidFilling}
                    lineWidth={lineWidth}
                    xOffset={xOffset}
                    yOffset={yOffset}
                    onSelectContext={onSelectContext}
                    onHoverContext={onHoverContext}
                />
            ))}
        </>
    )

    const aluminiumCornersPolygons: Polygon[] = []

    if (isAluminium && frameFilling.containerPolygon.isRect) {
        const {leftBottomPoint, leftTopPoint, rightBottomPoint, rightTopPoint} = frameFilling.rect

        const beamWidth = frameFilling.frameBeams[0].width - BEAD_WIDTH

        aluminiumCornersPolygons.push(
            Polygon.buildFromPoints([
                leftBottomPoint,
                leftBottomPoint.withOffset(0, beamWidth),
                leftBottomPoint.withOffset(beamWidth, beamWidth),
                leftBottomPoint.withOffset(beamWidth, 0)
            ]).withOffset(xOffset, yOffset),
            Polygon.buildFromPoints([
                leftTopPoint,
                leftTopPoint.withOffset(0, -beamWidth),
                leftTopPoint.withOffset(beamWidth, -beamWidth),
                leftTopPoint.withOffset(beamWidth, 0)
            ]).withOffset(xOffset, yOffset),
            Polygon.buildFromPoints([
                rightBottomPoint,
                rightBottomPoint.withOffset(0, beamWidth),
                rightBottomPoint.withOffset(-beamWidth, beamWidth),
                rightBottomPoint.withOffset(-beamWidth, 0)
            ]).withOffset(xOffset, yOffset),
            Polygon.buildFromPoints([
                rightTopPoint,
                rightTopPoint.withOffset(0, -beamWidth),
                rightTopPoint.withOffset(-beamWidth, -beamWidth),
                rightTopPoint.withOffset(-beamWidth, 0)
            ]).withOffset(xOffset, yOffset)
        )
    }

    const aluminiumCornersView = (
        <>
            {aluminiumCornersPolygons.map(polygon => {
                const {id} = polygon

                return (
                    <SvgGroup key={id}>
                        {getAluminiumPattern(
                            `pattern-${id}`,
                            getColor(
                                defaultPointOfView === PointsOfView.Inside ? insideLaminationColor : outsideLaminationColor,
                                frameFilling.nodeId === highlightedContext?.nodeId,
                                frameFilling.nodeId === currentContext?.nodeId
                            )
                        )}
                        <SvgPolygon key={id} polygon={polygon} fill={`url(#pattern-${id})`} lineWidth={lineWidth} className="non-clickable" />
                    </SvgGroup>
                )
            })}
        </>
    )

    const openingSystemView = (
        <SvgOpeningSystem frameFilling={frameFilling} lineWidth={lineWidth} xOffset={xOffset} yOffset={yOffset} shtulp={shtulp} />
    )

    const beadsView = (
        <>
            {!frameFilling.isMoskitka &&
                !frameFilling.isEmpty &&
                solidFillings.map(solidFilling =>
                    solidFilling.beads.map(bead => (
                        <SvgPolygon
                            key={bead.id}
                            className="bead"
                            polygon={bead.withOffset(xOffset, yOffset)}
                            fill={getBeadsFillingColor(bead.points)}
                            lineWidth={lineWidth}
                        />
                    ))
                )}
        </>
    )

    const solidFillingsTextsView = (
        <>
            {solidFillings.map(solidFilling => (
                <SvgSolidFillingText
                    key={solidFilling.nodeId}
                    solidFilling={solidFilling}
                    parent={frameFilling}
                    scale={scale}
                    xOffset={xOffset}
                    yOffset={yOffset}
                    fontSize={fontSize}
                    solidFillingCounter={solidFillingCounter}
                    frameFillingCounter={frameFillingCounter}
                    textColor={textColor}
                    currentContext={currentContext}
                    highlightedContext={highlightedContext}
                />
            ))}
        </>
    )

    const getBeams = (beams: Beam[] | Impost[]): JSX.Element => {
        return (
            <>
                {beams
                    .filter(beam => beam.innerDrawingPoints.length > 2 && beam.width > 0)
                    .map(beam => {
                        const isImpost = beam instanceof Impost
                        const fill = isImpost
                            ? getColor(
                                  defaultPointOfView === PointsOfView.Inside ? insideLaminationColor : outsideLaminationColor,
                                  beam.nodeId === highlightedContext?.nodeId,
                                  beam.nodeId === currentContext?.nodeId
                              )
                            : getColor(
                                  defaultPointOfView === PointsOfView.Inside ? insideLaminationColor : outsideLaminationColor,
                                  frameFilling.nodeId === highlightedContext?.nodeId || highlightedContext?.nodeId === beam.nodeId,
                                  frameFilling.nodeId === currentContext?.nodeId || currentContext?.nodeId === beam.nodeId
                              )

                        const beamSvg = (
                            <SvgPolygon
                                key={beam.nodeId}
                                className="clickable beam"
                                polygon={Polygon.buildFromPoints(beam.innerDrawingPoints.map(point => point.withOffset(xOffset, yOffset)))}
                                fill={isAluminium ? `url(#pattern-${beam.nodeId})` : fill}
                                lineWidth={lineWidth}
                                onClick={() => onSelectContext && onSelectContext(beam)}
                                onHover={() => onHoverContext && onHoverContext(beam)}
                            />
                        )

                        if (!isAluminium) {
                            return beamSvg
                        }

                        return (
                            <SvgGroup key={beam.nodeId}>
                                {getAluminiumPattern(`pattern-${beam.nodeId}`, fill)}
                                {beamSvg}
                            </SvgGroup>
                        )
                    })}
            </>
        )
    }

    const impostsView = <>{getBeams(frameFilling.impostBeams.slice(0).reverse())}</>
    const frameBeamsView = <>{getBeams(frameFilling.frameBeams.filter(beam => beam.innerDrawingPoints.length > 2 && beam.width > 0))}</>

    const moskitkaCornersView = (
        <>
            {moskitkaCornersPolygons.map(polygon => (
                <SvgPolygon key={polygon.id} polygon={polygon} fill={insideLaminationColor} lineWidth={lineWidth} />
            ))}
        </>
    )

    const hingesView = <SvgHinges frameFilling={frameFilling} lineWidth={lineWidth} xOffset={xOffset} yOffset={yOffset} />

    const handleView = (
        <SvgHandle
            frameFilling={frameFilling}
            currentContext={currentContext}
            highlightedContext={highlightedContext}
            lineWidth={lineWidth}
            xOffset={xOffset}
            yOffset={yOffset}
            onSelectContext={onSelectContext}
            onHoverContext={onHoverContext}
            isEntranceDoor={isEntranceDoor}
        />
    )

    const framesView = (
        <>
            {leafFillings.map(leafFilling => (
                <SvgFrameFilling
                    key={leafFilling.nodeId}
                    {...props}
                    frameFilling={leafFilling}
                    shtulp={frameFilling.impostBeams.find(i => i.modelPart === ModelParts.Shtulp)}
                />
            ))}
        </>
    )

    // if (pointOfView === PointsOfView.Outside) {
    //     return (
    //         <SvgGroup className={`svg-frame-group ${frameFilling.isRoot && 'svg-root-frame-group'}`}>
    //             {framesView}
    //             {moskitkaView}
    //             {solidFillingsView}
    //             {openingSystemView}
    //             {beadsView}
    //             {solidFillingsTextsView}
    //             {impostsView}
    //             {frameBeamsView}
    //             {moskitkaCornersView}
    //             {hingesView}
    //             {handleView}
    //         </SvgGroup>
    //     )
    // }

    return (
        <SvgGroup className={`svg-frame-group ${frameFilling.isRoot && 'svg-root-frame-group'}`}>
            {moskitkaView}
            {solidFillingsView}
            {openingSystemView}
            {beadsView}
            {solidFillingsTextsView}
            {impostsView}
            {frameBeamsView}
            {aluminiumCornersView}
            {moskitkaCornersView}
            {hingesView}
            {handleView}
            {framesView}
        </SvgGroup>
    )
}
