import * as React from 'react'
import {DimensionLine} from '../../../model/dimension-line'
import {IContext} from '../../../model/i-context'
import {SvgGroup} from './svg-group'
import {SvgDimensionLine} from './svg-dimension-line'

interface ISvgDimensionLinesProps {
    dimensionLines: DimensionLine[]
    xOffset: number
    yOffset: number
    productContext?: IContext | null
    currentDimensionLine: DimensionLine | null | undefined
    highlightedDimensionLine: DimensionLine | null | undefined
    lineWidth: number
    lineBoldWidth: number
    fontSize: number
}

export function SvgDimensionLines(props: ISvgDimensionLinesProps): JSX.Element {
    const {
        dimensionLines,
        xOffset,
        yOffset,
        productContext,
        currentDimensionLine,
        highlightedDimensionLine,
        lineWidth,
        lineBoldWidth,
        fontSize
    } = props

    if (currentDimensionLine) {
        dimensionLines.sort((l1, l2) => {
            if (l1.origin.equals(currentDimensionLine.origin)) {
                return 1
            } else if (l2.origin.equals(currentDimensionLine.origin)) {
                return -1
            }

            return 0
        })
    }
    if (highlightedDimensionLine) {
        dimensionLines.sort((l1, l2) => {
            if (l1.origin.equals(highlightedDimensionLine.origin)) {
                return 1
            } else if (l2.origin.equals(highlightedDimensionLine.origin)) {
                return -1
            }

            return 0
        })
    }

    return (
        <SvgGroup className="svg-dimension-lines-group">
            {dimensionLines.map(dLine => (
                <SvgDimensionLine
                    key={dLine.id}
                    dimensionLine={dLine}
                    currentDimensionLine={currentDimensionLine}
                    highlightedDimensionLine={highlightedDimensionLine}
                    productContext={productContext}
                    xOffset={xOffset}
                    yOffset={yOffset}
                    lineWidth={lineWidth}
                    lineBoldWidth={lineBoldWidth}
                    fontSize={fontSize}
                />
            ))}
        </SvgGroup>
    )
}
