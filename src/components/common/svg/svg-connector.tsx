import * as React from 'react'
import {Connector} from '../../../model/framer/connector'
import {Lamination} from '../../../model/framer/lamination'
import {SvgPolygon} from './svg-polygon'
import {Polygon} from '../../../model/geometry/polygon'
import {IContext} from '../../../model/i-context'
import {Rectangle} from '../../../model/geometry/rectangle'
import {Size} from '../../../model/size'
import {getColor} from '../../../helpers/color'

const MIN_VISIBLE_CONNECTOR_CLICKABLE_AREA_WIDTH_PX = 24

interface ISvgConnectorProps {
    connector: Connector
    currentContext?: IContext | null
    highlightedContext?: IContext | null
    xOffset: number
    yOffset: number
    scale: number
    lamination: Lamination
    lineWidth: number
    onSelectContext?: (context: IContext | null) => void
    onHoverContext?: (context: IContext | null) => void
}

export function SvgConnector(props: ISvgConnectorProps): JSX.Element {
    const {connector, currentContext, highlightedContext, xOffset, yOffset, lamination, lineWidth, scale, onSelectContext, onHoverContext} =
        props

    let connectorClickableAreaWidth = MIN_VISIBLE_CONNECTOR_CLICKABLE_AREA_WIDTH_PX / scale
    if (connectorClickableAreaWidth <= connector.width) {
        connectorClickableAreaWidth = connector.width
    }

    const connectorPolygon = Polygon.buildFromPoints(connector.innerDrawingPoints)
    const connectorRectangle = Rectangle.buildFromPoints(connector.innerDrawingPoints)

    const offset = -(connectorClickableAreaWidth - connector.width) / 2

    let invisibleRect: Rectangle
    if (connector.isVertical) {
        invisibleRect = new Rectangle(
            connectorRectangle.leftBottomPoint.withOffset(offset, 0),
            new Size(connectorClickableAreaWidth, connector.length)
        )
    } else {
        invisibleRect = new Rectangle(
            connectorRectangle.leftBottomPoint.withOffset(0, offset),
            new Size(connector.length, connectorClickableAreaWidth)
        )
    }

    return (
        <>
            <SvgPolygon
                polygon={connectorPolygon.withOffset(xOffset, yOffset)}
                className="clickable svg-connector"
                lineWidth={lineWidth}
                fill={getColor(lamination.hex, connector.nodeId === highlightedContext?.nodeId, connector.nodeId === currentContext?.nodeId)}
                onClick={() => onSelectContext && onSelectContext(connector)}
                onHover={() => onHoverContext && onHoverContext(connector)}
            />
            <SvgPolygon
                polygon={Polygon.buildFromPoints(invisibleRect.drawingPoints).withOffset(xOffset, yOffset)}
                className="clickable svg-connector-clickable-area"
                lineWidth={lineWidth * 2}
                stroke={'transparent'}
                fill={'transparent'}
                onClick={() => onSelectContext && onSelectContext(connector)}
                onHover={() => onHoverContext && onHoverContext(connector)}
            />
        </>
    )
}
