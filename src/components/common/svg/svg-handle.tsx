import * as React from 'react'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {IContext} from '../../../model/i-context'
import {OpenTypes} from '../../../model/framer/open-types'
import {ShtulpOpenTypes} from '../../../model/framer/shtulp-open-types'
import {SvgStandardHandle} from './svg-standard-handle'
import {SvgDoorHandle} from './svg-door-handle'
import {SvgFoldingHandle} from './svg-folding-handle'

export interface ISvgHandleProps {
    frameFilling: FrameFilling
    currentContext?: IContext | null
    highlightedContext?: IContext | null
    lineWidth: number
    xOffset: number
    yOffset: number
    onSelectContext?: (context: IContext | null) => void
    onHoverContext?: (context: IContext | null) => void
    isEntranceDoor: boolean
    isClickable?: boolean
}

export function SvgHandle(props: ISvgHandleProps): JSX.Element | null {
    const {frameFilling, isEntranceDoor} = props
    let {isClickable = true} = props
    const {openType} = frameFilling

    if (openType === OpenTypes.Gluhoe || frameFilling.isMoskitka || frameFilling.shtulpOpenType === ShtulpOpenTypes.ShtulpOnLeaf) {
        return null
    }

    if (frameFilling.isAluminium) {
        isClickable = false
    }

    if (
        isEntranceDoor &&
        (frameFilling.shtulpOpenType === ShtulpOpenTypes.NoShtulpOnLeaf || frameFilling.shtulpOpenType === ShtulpOpenTypes.NoShtulp)
    ) {
        return <SvgDoorHandle {...props} isClickable={isClickable} />
    }

    if (openType === OpenTypes.Razdvizhnaya) {
        return <SvgFoldingHandle {...props} isClickable={isClickable} />
    }

    return <SvgStandardHandle {...props} isClickable={isClickable} />
}
