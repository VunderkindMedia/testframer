import * as React from 'react'

interface ISvgGroupProps {
    className?: string
    children?: React.ReactNode
}

export function SvgGroup(props: ISvgGroupProps): JSX.Element {
    const {className, children} = props

    return <g className={className ?? ''}>{children}</g>
}
