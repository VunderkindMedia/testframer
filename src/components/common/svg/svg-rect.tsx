import * as React from 'react'
import {Rectangle} from '../../../model/geometry/rectangle'
import {Point} from '../../../model/geometry/point'

interface ISvgRectProps {
    rectangle: Rectangle
    className?: string
    fill?: string
    stroke?: string
    lineWidth?: number
    opacity?: number
    rotateAngle?: number
    rotatePoint?: Point
    borderRadiusX?: number
    borderRadiusY?: number
    onClick?: (e: React.MouseEvent) => void
    onHover?: (e: React.MouseEvent) => void
}

export function SvgRect(props: ISvgRectProps): JSX.Element {
    const {rectangle, className, fill, lineWidth, stroke, rotateAngle, rotatePoint, borderRadiusX, borderRadiusY, opacity, onClick, onHover} =
        props

    return (
        <rect
            className={className ?? ''}
            x={rectangle.refPoint.x}
            y={rectangle.refPoint.y}
            width={rectangle.size.width}
            height={rectangle.size.height}
            transform={`rotate(${rotateAngle ?? 0}, ${rotatePoint?.x ?? 0}, ${rotatePoint?.y ?? 0})`}
            rx={borderRadiusX ?? 0}
            ry={borderRadiusY ?? 0}
            fill={fill ?? '#fff'}
            stroke={stroke ?? '#000'}
            strokeWidth={lineWidth ?? 1}
            opacity={opacity ?? 1}
            onClick={e => {
                e.stopPropagation()
                onClick && onClick(e)
            }}
            onMouseOver={e => {
                e.stopPropagation()
                onHover && onHover(e)
            }}
        />
    )
}
