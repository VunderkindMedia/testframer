import * as React from 'react'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {OpenTypes} from '../../../model/framer/open-types'
import {Polygon} from '../../../model/geometry/polygon'
import {OpenSides} from '../../../model/framer/open-sides'
import {Line} from '../../../model/geometry/line'
import {SvgLine} from './svg-line'
import {ShtulpOpenTypes} from '../../../model/framer/shtulp-open-types'
import {SvgRect} from './svg-rect'
import {Rectangle} from '../../../model/geometry/rectangle'
import {Size} from '../../../model/size'
import {Impost} from '../../../model/framer/impost'
import {ShtulpTypes} from '../../../model/framer/shtulp-types'
import {getBottommostLine, getLeftmostLine, getRightmostLine, getTopmostLine} from '../../../helpers/line'
import {clamp} from '../../../helpers/number'

interface ISvgOpeningSystemProps {
    lineWidth: number
    frameFilling: FrameFilling
    xOffset: number
    yOffset: number
    shtulp?: Impost
}

const STROKE_COLOR = '#8AD0E5'

export function SvgOpeningSystem(props: ISvgOpeningSystemProps): JSX.Element | null {
    const {frameFilling, lineWidth, xOffset, yOffset, shtulp} = props

    const {openType, openSide, shtulpOpenType} = frameFilling

    if (openType === OpenTypes.Gluhoe || frameFilling.isMoskitka) {
        return null
    }

    const innerPolygon = new Polygon(frameFilling.frameBeams).innerPolygon

    const leftmostLine = getLeftmostLine(innerPolygon.segments).withOffset(xOffset, yOffset)
    const rightmostLine = getRightmostLine(innerPolygon.segments).withOffset(xOffset, yOffset)
    const topmostLine = getTopmostLine(innerPolygon.segments).withOffset(xOffset, yOffset)
    const bottommostLine = getBottommostLine(innerPolygon.segments).withOffset(xOffset, yOffset)

    const drawingLines: Line[] = []

    if (openType === OpenTypes.PovorotnoOtkidnaya || openType === OpenTypes.Otkidnaya) {
        drawingLines.push(new Line(bottommostLine.start, topmostLine.rect.center))
        drawingLines.push(new Line(topmostLine.rect.center, bottommostLine.end))
    }

    if (openType === OpenTypes.PovorotnoOtkidnaya || openType === OpenTypes.Povorotnaya) {
        if (openSide === OpenSides.ToLeft) {
            drawingLines.push(new Line(leftmostLine.start, rightmostLine.rect.center))
            drawingLines.push(new Line(rightmostLine.rect.center, leftmostLine.end))
        } else if (openSide === OpenSides.ToRight) {
            drawingLines.push(new Line(rightmostLine.start, leftmostLine.rect.center))
            drawingLines.push(new Line(leftmostLine.rect.center, rightmostLine.end))
        }
    }

    if (openType === OpenTypes.Razdvizhnaya && innerPolygon.isRect) {
        const centerLine = new Line(leftmostLine.rect.center, rightmostLine.rect.center)
        const start = leftmostLine.rect.center.withOffset(centerLine.length / 3, 0)
        const end = rightmostLine.rect.center.withOffset(-centerLine.length / 3, 0)
        const horizontalLine = new Line(start, end)
        drawingLines.push(horizontalLine)
        const offsetX = horizontalLine.length / 2.1
        const offsetY = offsetX * 0.3 // tan(a) = a / b
        if (openSide === OpenSides.ToLeft) {
            drawingLines.push(new Line(horizontalLine.start, horizontalLine.start.withOffset(offsetX, offsetY)))
            drawingLines.push(new Line(horizontalLine.start, horizontalLine.start.withOffset(offsetX, -offsetY)))
        } else if (openSide === OpenSides.ToRight) {
            drawingLines.push(new Line(horizontalLine.end, horizontalLine.end.withOffset(-offsetX, offsetY)))
            drawingLines.push(new Line(horizontalLine.end, horizontalLine.end.withOffset(-offsetX, -offsetY)))
        }
    }

    let shtulpIconRectangle: Rectangle | null = null
    if (shtulpOpenType === ShtulpOpenTypes.ShtulpOnLeaf) {
        const shtulpIconWidth = clamp(innerPolygon.rect.size.width / 2, 50, 100)
        const shtulpIconHeight = (shtulpIconWidth * 4) / 5

        shtulpIconRectangle = new Rectangle(
            frameFilling.containerPolygon.center.withOffset(xOffset - shtulpIconWidth / 2, yOffset - shtulpIconHeight / 2),
            new Size(shtulpIconWidth, shtulpIconHeight)
        )

        if (shtulp && shtulp.shtulpType !== ShtulpTypes.Impost) {
            const sizeDif = shtulpIconHeight - shtulpIconWidth
            const startPoint = shtulpIconRectangle.refPoint.withOffset(
                shtulp.shtulpType === ShtulpTypes.LeftShtulp ? shtulpIconWidth : 0,
                sizeDif
            )
            drawingLines.push(new Line(startPoint, startPoint.withOffset(0, shtulpIconWidth - sizeDif)))
        }
    }

    return (
        <>
            {drawingLines.map(line => (
                <SvgLine key={line.id} stroke={STROKE_COLOR} lineWidth={lineWidth} line={line} />
            ))}
            {shtulpIconRectangle && (
                <SvgRect stroke={STROKE_COLOR} fill={'transparent'} lineWidth={lineWidth} rectangle={shtulpIconRectangle} />
            )}
        </>
    )
}
