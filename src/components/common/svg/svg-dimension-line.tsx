import * as React from 'react'
import {DimensionLine} from '../../../model/dimension-line'
import {IContext} from '../../../model/i-context'
import {Colors} from '../../../constants/colors'
import {SvgGroup} from './svg-group'
import {SvgLine} from './svg-line'
import {Rectangle} from '../../../model/geometry/rectangle'
import {SvgPolygon} from './svg-polygon'
import {Polygon} from '../../../model/geometry/polygon'

interface ISvgDimensionLineProps {
    dimensionLine: DimensionLine
    currentDimensionLine: DimensionLine | null | undefined
    highlightedDimensionLine: DimensionLine | null | undefined
    xOffset: number
    yOffset: number
    productContext?: IContext | null
    lineWidth: number
    lineBoldWidth: number
    fontSize: number
}

export function SvgDimensionLine(props: ISvgDimensionLineProps): JSX.Element | null {
    const {dimensionLine, currentDimensionLine, highlightedDimensionLine, xOffset, yOffset, productContext, lineWidth, lineBoldWidth} = props

    const isVisible = productContext ? dimensionLine.markers.includes(productContext.nodeId) : true

    if (!isVisible) {
        return null
    }

    const selected = (dLine: DimensionLine): boolean => Boolean(currentDimensionLine?.equals(dLine))
    const highlighted = (dLine: DimensionLine): boolean => Boolean(highlightedDimensionLine?.equals(dLine))

    const getLineColor = (dLine: DimensionLine): string => {
        return selected(dLine) || highlighted(dLine) ? Colors.Active : Colors.Normal
    }

    const getBracketColor = (dLine: DimensionLine): string => {
        return selected(dLine) || highlighted(dLine) ? Colors.Active : Colors.Black
    }

    const getStrokeWidth = (dLine: DimensionLine): number => {
        if (selected(dLine)) {
            return lineBoldWidth
        }
        return lineWidth
    }

    return (
        <SvgGroup className="svg-dimension-line-group">
            <SvgLine
                line={dimensionLine.withOffset(xOffset, yOffset)}
                lineWidth={getStrokeWidth(dimensionLine)}
                stroke={getLineColor(dimensionLine)}
            />
            <SvgLine
                line={dimensionLine.bracketLine1.withOffset(xOffset, yOffset)}
                lineWidth={getStrokeWidth(dimensionLine)}
                stroke={getBracketColor(dimensionLine)}
            />
            <SvgLine
                line={dimensionLine.bracketLine2.withOffset(xOffset, yOffset)}
                lineWidth={getStrokeWidth(dimensionLine)}
                stroke={getBracketColor(dimensionLine)}
            />
            <SvgPolygon
                stroke={'transparent'}
                fill={'transparent'}
                className="clickable"
                polygon={Polygon.buildFromPoints(
                    Rectangle.buildFromPoints([
                        dimensionLine.start,
                        dimensionLine.end,
                        dimensionLine.bracketLine1.start,
                        dimensionLine.bracketLine1.end,
                        dimensionLine.bracketLine2.start,
                        dimensionLine.bracketLine2.end
                    ]).drawingPoints.map(p => p.withOffset(xOffset, yOffset))
                )}
            />
        </SvgGroup>
    )
}
