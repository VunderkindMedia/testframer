import * as React from 'react'
import {ISvgHandleProps} from './svg-handle'
import {ModelParts} from '../../../model/framer/model-parts'
import {Handle} from '../../../model/handle'
import {Point} from '../../../model/geometry/point'
import {SvgRect} from './svg-rect'
import {Rectangle} from '../../../model/geometry/rectangle'
import {Size} from '../../../model/size'
import {getHandleColorHex} from '../../../model/framer/handle-colors'
import {OpenSides} from '../../../model/framer/open-sides'
import {getColor} from '../../../helpers/color'

export function SvgDoorHandle(props: ISvgHandleProps): JSX.Element {
    const {frameFilling, currentContext, highlightedContext, lineWidth, xOffset, yOffset, onSelectContext, onHoverContext, isClickable} = props

    const SIZE_MULTIPLIER = 3
    const HANDLE_BASE_WIDTH = 10 * SIZE_MULTIPLIER
    const HANDLE_BASE_HEIGHT = 44 * SIZE_MULTIPLIER
    const HANDLE_BASE_RADIUS = 5 * SIZE_MULTIPLIER

    const KEYHOLE_Y_OFFSET = -HANDLE_BASE_RADIUS

    const HANDLE_WIDTH = 7 * SIZE_MULTIPLIER
    const HANDLE_HEIGHT = 42 * SIZE_MULTIPLIER
    const HANDLE_RADIUS = 2 * SIZE_MULTIPLIER

    const KEYHOLE_WIDTH = 3 * SIZE_MULTIPLIER

    const KEYHOLE_WIDTH_2 = SIZE_MULTIPLIER
    const KEYHOLE_HEIGHT_2 = 3 * SIZE_MULTIPLIER

    const CLICKABLE_WIDTH = frameFilling.profile.getGeometryParameters(ModelParts.Stvorka).a
    const CLICKABLE_HEIGHT = HANDLE_BASE_HEIGHT

    const handle = new Handle(frameFilling)

    const x = handle.refPoint.x + xOffset
    const y = handle.refPoint.y + yOffset - KEYHOLE_Y_OFFSET

    const handleX = frameFilling.openSide === OpenSides.ToRight ? x - HANDLE_WIDTH / 2 : x + HANDLE_WIDTH / 2 - HANDLE_HEIGHT

    return (
        <>
            <SvgRect
                className="svg-handle-base"
                rectangle={
                    new Rectangle(
                        new Point(x - HANDLE_BASE_WIDTH / 2, y - HANDLE_BASE_HEIGHT / 2),
                        new Size(HANDLE_BASE_WIDTH, HANDLE_BASE_HEIGHT)
                    )
                }
                fill={getColor(
                    getHandleColorHex(handle.color),
                    handle.nodeId === highlightedContext?.nodeId,
                    handle.nodeId === currentContext?.nodeId
                )}
                borderRadiusX={HANDLE_BASE_RADIUS}
                borderRadiusY={HANDLE_BASE_RADIUS}
                lineWidth={lineWidth}
            />
            <SvgRect
                className="svg-handle"
                rectangle={new Rectangle(new Point(handleX, y + HANDLE_BASE_RADIUS), new Size(HANDLE_HEIGHT, HANDLE_WIDTH))}
                fill={getColor(
                    getHandleColorHex(handle.color),
                    handle.nodeId === highlightedContext?.nodeId,
                    handle.nodeId === currentContext?.nodeId
                )}
                rotatePoint={new Point(x - HANDLE_WIDTH / 2, y + HANDLE_BASE_RADIUS + 2 * HANDLE_RADIUS)}
                borderRadiusX={HANDLE_RADIUS}
                borderRadiusY={HANDLE_RADIUS * 2}
                onClick={() => onSelectContext && onSelectContext(handle)}
                onHover={() => onHoverContext && onHoverContext(handle)}
                lineWidth={lineWidth}
            />
            <SvgRect
                className="svg-keyhole"
                rectangle={
                    new Rectangle(
                        new Point(x - KEYHOLE_WIDTH / 2, y - KEYHOLE_WIDTH / 2 + KEYHOLE_Y_OFFSET),
                        new Size(KEYHOLE_WIDTH, KEYHOLE_WIDTH)
                    )
                }
                fill="#000"
                borderRadiusX={KEYHOLE_WIDTH / 2}
                borderRadiusY={KEYHOLE_WIDTH / 2}
                lineWidth={lineWidth}
            />
            <SvgRect
                className="svg-keyhole"
                rectangle={
                    new Rectangle(
                        new Point(
                            x - KEYHOLE_WIDTH / 2 + (KEYHOLE_WIDTH - KEYHOLE_WIDTH_2) / 2,
                            y - KEYHOLE_HEIGHT_2 / 2 - KEYHOLE_WIDTH + KEYHOLE_Y_OFFSET
                        ),
                        new Size(KEYHOLE_WIDTH_2, KEYHOLE_HEIGHT_2)
                    )
                }
                fill="#000"
                lineWidth={lineWidth}
            />
            <SvgRect
                className={`svg-handle-clickable ${isClickable && 'clickable'}`}
                rectangle={
                    new Rectangle(
                        new Point(x - CLICKABLE_WIDTH / 2, y + HANDLE_BASE_HEIGHT / 2 - CLICKABLE_HEIGHT),
                        new Size(CLICKABLE_WIDTH, CLICKABLE_HEIGHT)
                    )
                }
                onClick={() => isClickable && onSelectContext && onSelectContext(handle)}
                onHover={() => isClickable && onHoverContext && onHoverContext(handle)}
                fill="transparent"
                stroke="transparent"
            />
        </>
    )
}
