import * as React from 'react'
import {SolidFilling} from '../../../model/framer/solid-filling'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {BEAD_WIDTH} from '../../../constants/geometry-params'
import {SvgText} from './svg-text'
import {Point} from '../../../model/geometry/point'
import {IContext} from '../../../model/i-context'
import {getBottommostLine} from '../../../helpers/line'
import {getFillingTitleColor} from '../../../helpers/color'
import {getInnerColorUserParameter} from '../../../helpers/builder'

interface ISvgSolidFillingTextProps {
    solidFilling: SolidFilling
    parent: FrameFilling
    scale: number
    xOffset: number
    yOffset: number
    fontSize: number
    textColor?: string
    currentContext?: IContext | null
    highlightedContext?: IContext | null
    solidFillingCounter: () => number
    frameFillingCounter: () => number
}

export function SvgSolidFillingText(props: ISvgSolidFillingTextProps): JSX.Element {
    const {
        solidFilling,
        parent,
        scale,
        xOffset,
        yOffset,
        textColor,
        currentContext,
        highlightedContext,
        solidFillingCounter,
        frameFillingCounter
    } = props

    const bottommostSegment = getBottommostLine(solidFilling.containerPolygon.innerPolygon.segments)
    const {rect: bottommostSegmentRect} = bottommostSegment
    const availableWidth = bottommostSegmentRect.size.width

    const fontSizeLocal = props.fontSize / scale

    let text: string
    if (!parent.isRoot && parent.nodeId === solidFilling.parentNodeId) {
        text = `C-${frameFillingCounter()}    ${solidFilling.fillingVendorCode}`
    } else {
        text = `Г-${solidFillingCounter()}    ${solidFilling.fillingVendorCode}`
    }

    const maxFontSize = (availableWidth / text.length) * 1.5 // ширина текста примерно в 1.7 раз меньше высоты
    const fontSize = Math.ceil(Math.min(maxFontSize, fontSizeLocal))

    const OFFSET_X = 0.5 * fontSize + BEAD_WIDTH
    const OFFSET_Y = fontSize + BEAD_WIDTH

    const x = bottommostSegmentRect.refPoint.x + OFFSET_X + xOffset
    const y = bottommostSegmentRect.refPoint.y + bottommostSegmentRect.size.height + OFFSET_Y + yOffset

    const colorParam = getInnerColorUserParameter(solidFilling)

    const svgTextColor =
        textColor ??
        getFillingTitleColor(
            colorParam?.hex,
            solidFilling.nodeId === highlightedContext?.nodeId,
            solidFilling.nodeId === currentContext?.nodeId
        )

    return <SvgText text={text} textColor={svgTextColor} position={new Point(x, y)} fontSize={fontSize} />
}
