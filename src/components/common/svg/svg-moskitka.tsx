import * as React from 'react'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {getHexMoskitkaColor} from '../../../model/framer/moskitka-colors'
import {useRef} from 'react'
import {SvgGroup} from './svg-group'
import {SvgRect} from './svg-rect'
import {Rectangle} from '../../../model/geometry/rectangle'
import {Point} from '../../../model/geometry/point'
import {Size} from '../../../model/size'
import {SvgPolygon} from './svg-polygon'
import {GUID} from '../../../helpers/guid'

interface ISvgMoskitkaProps {
    frameFilling: FrameFilling
    outsideLaminationColor: string
    scale: number
    xOffset: number
    yOffset: number
}

export function SvgMoskitka(props: ISvgMoskitkaProps): JSX.Element | null {
    const {frameFilling, outsideLaminationColor, scale, xOffset, yOffset} = props

    const patternIdRef = useRef(`pattern-${GUID()}`)

    if (!frameFilling?.moskitka?.set) {
        return null
    }

    const color = getHexMoskitkaColor(frameFilling.moskitka.color, outsideLaminationColor)

    const cellSize = 3 / scale

    return (
        <SvgGroup className="svg-moskitka-group">
            <defs>
                <pattern id={patternIdRef.current} x={0} y={0} width={cellSize * 2} height={cellSize * 2} patternUnits="userSpaceOnUse">
                    <SvgRect
                        rectangle={new Rectangle(new Point(0, 0), new Size(cellSize, cellSize))}
                        opacity={0.2}
                        fill={color}
                        stroke={'transparent'}
                    />
                    <SvgRect
                        rectangle={new Rectangle(new Point(cellSize, cellSize), new Size(cellSize, cellSize))}
                        opacity={0.2}
                        fill={color}
                        stroke={'transparent'}
                    />
                </pattern>
            </defs>
            <SvgPolygon
                polygon={frameFilling.containerPolygon.withOffset(xOffset, yOffset)}
                fill={`url(#${patternIdRef.current})`}
                stroke={'transparent'}
                className="clickable svg-moskitka"
            />
        </SvgGroup>
    )
}
