import * as React from 'react'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {OpenTypes} from '../../../model/framer/open-types'
import {OpenSides} from '../../../model/framer/open-sides'
import {Handle} from '../../../model/handle'
import {getHandleColorHex} from '../../../model/framer/handle-colors'
import {Rectangle} from '../../../model/geometry/rectangle'
import {SvgPolygon} from './svg-polygon'
import {Polygon} from '../../../model/geometry/polygon'
import {Size} from '../../../model/size'
import {Point} from '../../../model/geometry/point'

interface ISvgHingesProps {
    frameFilling: FrameFilling
    lineWidth: number
    xOffset: number
    yOffset: number
}

const WIDTH = 11
const HEIGHT = 80
const HEIGHT2 = 20

export function SvgHinges(props: ISvgHingesProps): JSX.Element | null {
    const {frameFilling, lineWidth, xOffset, yOffset} = props

    const {openType, openSide} = frameFilling

    if (openType === OpenTypes.Gluhoe || frameFilling.isMoskitka || openType === OpenTypes.Razdvizhnaya) {
        return null
    }

    const drawingRects: Rectangle[] = []

    const handle = new Handle(frameFilling)
    const handleColorHex = getHandleColorHex(handle.color)

    if (openType === OpenTypes.Otkidnaya && openSide === OpenSides.ToBottom) {
        const hostBeam = frameFilling.bottommostBeam
        const hostBeamRect = hostBeam.rect
        const x = hostBeamRect.refPoint.x + xOffset
        const y = hostBeamRect.refPoint.y + yOffset - WIDTH
        drawingRects.push(
            new Rectangle(new Point(x, y), new Size(HEIGHT, WIDTH)),
            new Rectangle(new Point(x, y), new Size(HEIGHT2, WIDTH)),
            new Rectangle(new Point(x + HEIGHT - HEIGHT2, y), new Size(HEIGHT2, WIDTH)),

            new Rectangle(new Point(x + hostBeam.length - HEIGHT, y), new Size(HEIGHT, WIDTH)),
            new Rectangle(new Point(x + hostBeam.length - HEIGHT, y), new Size(HEIGHT2, WIDTH)),
            new Rectangle(new Point(x + hostBeam.length - HEIGHT2, y), new Size(HEIGHT2, WIDTH))
        )
    } else {
        const isRight = openSide === OpenSides.ToRight
        const isOtkindoe = openType === OpenTypes.PovorotnoOtkidnaya || openType === OpenTypes.Otkidnaya
        const hostBeam = isRight ? frameFilling.rightmostBeam : frameFilling.leftmostBeam

        if (!hostBeam.isVertical) {
            throw new Error('Запрещено размещать петли на косой балке')
        }

        const hostBeamRect = hostBeam.rect
        const x = hostBeamRect.refPoint.x + (isRight ? 0 : -WIDTH) + xOffset
        const y = hostBeamRect.refPoint.y + yOffset

        drawingRects.push(new Rectangle(new Point(x, y), new Size(WIDTH, HEIGHT)), new Rectangle(new Point(x, y), new Size(WIDTH, HEIGHT2)))
        !isOtkindoe && drawingRects.push(new Rectangle(new Point(x, y + HEIGHT - HEIGHT2), new Size(WIDTH, HEIGHT2)))

        drawingRects.push(
            new Rectangle(new Point(x, y + hostBeam.length - HEIGHT), new Size(WIDTH, HEIGHT)),
            new Rectangle(new Point(x, y + hostBeam.length - HEIGHT), new Size(WIDTH, HEIGHT2)),
            new Rectangle(new Point(x, y + hostBeam.length - HEIGHT2), new Size(WIDTH, HEIGHT2))
        )
    }

    return (
        <>
            {drawingRects.map(rect => (
                <SvgPolygon
                    key={rect.id}
                    className="svg-hinges-part"
                    polygon={Polygon.buildFromPoints(rect.drawingPoints)}
                    stroke="#000"
                    fill={handleColorHex}
                    lineWidth={lineWidth}
                />
            ))}
        </>
    )
}
