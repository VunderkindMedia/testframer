import * as React from 'react'
import {useCallback, useEffect, useState} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Lamination} from '../../../model/framer/lamination'
import {ConfigContexts} from '../../../model/config-contexts'
import {ColorItem} from '../color-item/color-item'
import {Checkbox} from '../checkbox/checkbox'
import {Expandable} from '../expandable/expandable'
import {getHexMoskitkaColor, getMoskitkaColorName, MoskitkaColors} from '../../../model/framer/moskitka-colors'
import {useSetLamination, useSetMoskitkaColor} from '../../../hooks/builder'
import {getProductLamination} from '../../../helpers/lamination'
import {LAMINATION_SELECTOR_APPLY_TO_BOTH_SIDES, LAMINATION_SELECTOR_DESCRIPTION_EXPANDED} from '../../../constants/local-storage'
import {useAppContext} from '../../../hooks/app'
import styles from './lamination-selector.module.css'
import {FactoryTypes} from '../../../model/framer/factory-types'

export function LaminationSelector(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const partner = useSelector((state: IAppState) => state.account.partner)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const lamination = useSelector((state: IAppState) => state.lamination.all)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)
    const [localConfigContext, setLocalConfigContext] = useState(ConfigContexts.LaminationOutside)

    const [isApplyToBothSide, setIsApplyToBothSide] = useState(localStorage.getItem(LAMINATION_SELECTOR_APPLY_TO_BOTH_SIDES) === 'true')
    const [isLaminationSelectorDescriptionExpanded, setIsLaminationSelectorDescriptionExpanded] = useState(
        localStorage.getItem(LAMINATION_SELECTOR_DESCRIPTION_EXPANDED) !== 'false'
    )

    const setLamination = useSetLamination(currentOrder, currentConstruction, localConfigContext, isApplyToBothSide)
    const setMoskitkaColor = useSetMoskitkaColor(currentOrder, currentConstruction)
    const {isMobile} = useAppContext()

    const selectLamination = useCallback(
        (lamination: Lamination) => {
            setLamination(lamination)
        },
        [setLamination]
    )

    useEffect(() => {
        if (configContext) {
            setLocalConfigContext(configContext)
        }
    }, [configContext])

    useEffect(() => {
        localStorage.setItem(LAMINATION_SELECTOR_DESCRIPTION_EXPANDED, String(isLaminationSelectorDescriptionExpanded))
    }, [isLaminationSelectorDescriptionExpanded])

    useEffect(() => {
        localStorage.setItem(LAMINATION_SELECTOR_APPLY_TO_BOTH_SIDES, String(isApplyToBothSide))
    }, [isApplyToBothSide])

    if (!currentOrder || !currentConstruction) {
        return null
    }

    const [currentOutsideLamination, currentInsideLamination] = getProductLamination(currentConstruction.products[0], lamination)
    const currentLamination = localConfigContext === ConfigContexts.LaminationOutside ? currentOutsideLamination : currentInsideLamination

    return (
        <div className={`${styles.selector} ${isMobile ? styles.mobile : ''}`}>
            <Expandable
                className={styles.descriptionExpandable}
                isExpanded={isLaminationSelectorDescriptionExpanded}
                onChange={setIsLaminationSelectorDescriptionExpanded}
                header={<p className={styles.descriptionTitle}>Описание добавления ламинации</p>}>
                <p className={styles.description}>
                    В этом разделе вы можете указать необходимость ламинации конструкции. Обращаем ваше внимание на следующие момент, - сендвичи
                    будут произведены с указанием цвета &quot;в цвет конструкции&quot; и, при указании ламинации, покрыты пленками
                    соответствующих цветов.
                    <br />
                    Также, при использовании внутренней ламинации, не забудьте выбрать тип и цвет оконной ручки, - это можно сделать кликнув по
                    ручке. При этом, накладки на петли будут автоматически покрашены в цвет оконной ручки.
                </p>
            </Expandable>
            {
                <>
                    <div className={styles.colors}>
                        {currentConstruction?.isMoskitka && (
                            <ColorItem
                                isSelected={currentConstruction.isBrownMoskitka}
                                onClick={() => {
                                    setIsApplyToBothSide(true)
                                    setMoskitkaColor(MoskitkaColors.Brown)
                                }}
                                name={getMoskitkaColorName(MoskitkaColors.Brown)}
                                color={getHexMoskitkaColor(MoskitkaColors.Brown, currentOutsideLamination.hex)}
                            />
                        )}
                        {partner?.factoryId === FactoryTypes.Moscow && currentConstruction?.isMoskitka
                            ? lamination
                                  .filter(i => i.name === 'Белый')
                                  .map(l => (
                                      <ColorItem
                                          key={l.id}
                                          onClick={() => selectLamination(l)}
                                          isSelected={l.id === currentLamination.id && !currentConstruction.isBrownMoskitka}
                                          name={l.name}
                                          color={l.hex}
                                      />
                                  ))
                            : lamination.map(l => (
                                  <ColorItem
                                      key={l.id}
                                      onClick={() => selectLamination(l)}
                                      isSelected={l.id === currentLamination.id && !currentConstruction.isBrownMoskitka}
                                      name={l.name}
                                      color={l.hex}
                                  />
                              ))}
                    </div>
                    {!currentConstruction.isBrownMoskitka && (
                        <div className={styles.footer}>
                            <div style={{outline: 'none'}}>
                                <Checkbox
                                    isChecked={isApplyToBothSide}
                                    onChange={checked => setIsApplyToBothSide(checked)}
                                    title="Применить с двух сторон"
                                />
                            </div>
                        </div>
                    )}
                </>
            }
        </div>
    )
}
