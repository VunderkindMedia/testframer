import * as React from 'react'
import {useCallback, useRef, useState} from 'react'
import styles from './connect-button.module.css'
import {Action} from '../../../model/action'
import {ContextMenu} from '../context-menu/context-menu'
import {Sides} from '../../../model/sides'
import {AddButton} from '../button/add-button/add-button'
import {useDocumentClick} from '../../../hooks/ui'
import {getClassNames} from '../../../helpers/css'

interface IConnectButton {
    actions: Action[]
    side: Sides
}

export function ConnectButton(props: IConnectButton): JSX.Element {
    const {actions, side} = props

    const [isVisibleContextMenu, setIsVisibleContextMenu] = useState(false)
    const containerRef = useRef<HTMLDivElement>(null)

    const listItems = actions.map(a => a.name)

    const onClickHandler = useCallback((e: MouseEvent) => {
        if (containerRef.current && e.target && !containerRef.current.contains(e.target as Node)) {
            setIsVisibleContextMenu(false)
        }
    }, [])

    useDocumentClick(onClickHandler)

    return (
        <div
            className={getClassNames(`${styles.connectButton} ${styles[side]}`, {[styles.contextMenuVisible]: isVisibleContextMenu})}
            ref={containerRef}>
            <AddButton
                style="secondary"
                size="big"
                onClick={e => {
                    e.stopPropagation()
                    if (actions.length === 1) {
                        void actions[0].func()
                    } else {
                        setIsVisibleContextMenu(true)
                    }
                }}
            />
            <ContextMenu
                items={listItems}
                className={styles.contextMenu}
                onSelect={(item, index) => {
                    void actions[index].func()
                    setIsVisibleContextMenu(false)
                }}
            />
        </div>
    )
}
