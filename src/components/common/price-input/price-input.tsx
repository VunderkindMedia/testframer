import * as React from 'react'
import styles from './price-input.module.css'
import {InputNumber} from '../input-number/input-number'
import {useCallback} from 'react'
import {clamp} from '../../../helpers/number'

interface IPriceInputProps {
    value?: number
    currency: string | JSX.Element
    debounceTimeMs?: number
    isDisabled?: boolean
    isAnnoyed?: boolean
    isRequired?: boolean
    min?: number
    max?: number
    className?: string
    dataTest?: string
    onChange?: (value: number) => void
    onKeyPressEnter?: (value: number, isValid: boolean) => void
}
const PriceInput = React.forwardRef<HTMLInputElement, IPriceInputProps>((props: IPriceInputProps, ref) => {
    const {
        value,
        currency,
        min = 0,
        max = Number.MAX_SAFE_INTEGER,
        debounceTimeMs,
        onChange,
        onKeyPressEnter,
        isDisabled,
        isAnnoyed,
        isRequired,
        className,
        dataTest
    } = props

    const onChangeHandler = useCallback(
        (value: number) => {
            const newValue = +clamp(value, min, max)

            onChange && onChange(newValue)
        },
        [min, max, onChange]
    )

    return (
        <div className={`${styles.input} ${className}`}>
            <InputNumber
                ref={ref}
                value={value}
                min={min}
                max={max}
                debounceTimeMs={debounceTimeMs}
                isDisabled={isDisabled}
                isRequired={isRequired}
                isAnnoyed={isAnnoyed}
                dataTest={dataTest}
                onChange={onChangeHandler}
                onKeyPressEnter={onKeyPressEnter}
            />
            <p>{currency}</p>
        </div>
    )
})

PriceInput.displayName = 'PriceInput'

export {PriceInput}
