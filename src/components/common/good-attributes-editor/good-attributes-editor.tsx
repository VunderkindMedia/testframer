import * as React from 'react'
import './good-attributes-editor.css'
import {GoodAttribute} from '../../../model/framer/good-attribute'
import {GoodAttributeTypes} from '../../../model/framer/good-attribute-types'
import {InputNumber} from '../input-number/input-number'
import {WithTitle} from '../with-title/with-title'
import {ListItem} from '../../../model/list-item'
import {Select} from '../select/select'

interface IGoodAttributesEditorProps {
    attributes: GoodAttribute[]
    isDisabled?: boolean
    onChange: (attributes: GoodAttribute[]) => void
}

const getAttributeEditor = (attribute: GoodAttribute, onChange: (attribute: GoodAttribute) => void): JSX.Element | null => {
    const {type} = attribute

    if (type === GoodAttributeTypes.Number) {
        return (
            <WithTitle title={attribute.caption}>
                <InputNumber
                    value={attribute.value}
                    min={attribute.minValue}
                    max={attribute.maxValue}
                    step={attribute.step}
                    onChange={value => {
                        const newAttribute = attribute.clone
                        if (attribute.maxValue && value > attribute.maxValue) {
                            value = attribute.maxValue
                        }
                        if (attribute.minValue && value < attribute.minValue) {
                            value = attribute.minValue
                        }
                        newAttribute.value = value
                        onChange(newAttribute)
                    }}
                />
            </WithTitle>
        )
    }

    if (type === GoodAttributeTypes.Color && attribute.enumValues) {
        const selectItems = attribute.enumValues.map(enumValue => new ListItem(enumValue.value.toString(10), enumValue.name, enumValue))

        const selectedItem = selectItems.find(item => item.data.value === attribute.value)

        return (
            <WithTitle title={attribute.caption}>
                <Select
                    items={selectItems}
                    selectedItem={selectedItem}
                    // decorator={item => <div className="good-color" style={{background: item.data.hexColor}}/>}
                    onSelect={item => {
                        const newAttribute = attribute.clone
                        newAttribute.value = item.data.value
                        onChange(newAttribute)
                    }}
                />
            </WithTitle>
        )
    }

    return null
}

export function GoodAttributesEditor(props: IGoodAttributesEditorProps): JSX.Element | null {
    const {attributes, isDisabled, onChange} = props

    if (attributes.length === 0) {
        return null
    }

    return (
        <div className={`good-attributes-editor ${isDisabled && 'disabled'}`}>
            {attributes.map((attribute, index) => (
                <React.Fragment key={index}>
                    {getAttributeEditor(attribute, newAttribute => {
                        const newAttributes = attributes.slice(0)
                        newAttributes[index] = newAttribute
                        onChange(newAttributes)
                    })}
                </React.Fragment>
            ))}
        </div>
    )
}
