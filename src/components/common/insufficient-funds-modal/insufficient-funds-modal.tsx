import * as React from 'react'
import styles from './insufficient-funds-modal.module.css'
import {Modal} from '../modal/modal'
import {Order} from '../../../model/framer/order'
import {ModalModel} from '../modal/modal-model'
import {Action} from '../../../model/action'
import {OrderStates} from '../../../model/framer/order-states'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Balance} from '../balance/balance'
import {NAV_BOOKING, NAV_PAYMENT} from '../../../constants/navigation'
import {AnalyticsManager} from '../../../analytics-manager'
import {useGoTo} from '../../../hooks/router'

export const useShouldShowInsufficientFundsModal = (order?: Order | null): boolean => {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const accountBalance = useSelector((state: IAppState) => state.account.balance)
    const accountLimit = useSelector((state: IAppState) => state.account.creditLimitInCents)
    const framerCreditLimit = useSelector((state: IAppState) => state.account.framerCreditLimitInCents)
    const framerPoints = useSelector((state: IAppState) => state.account.framerPoints)
    const isCashMode = useSelector((state: IAppState) => state.account.isCashMode)

    order = order ?? currentOrder

    if (!order || order.state === OrderStates.Paid) {
        return false
    }
    return isCashMode
        ? framerPoints + framerCreditLimit - order.totalAmegaCostToDisplay < 0
        : accountBalance + accountLimit - order.totalAmegaCostToDisplay < 0
}

interface IInsufficientFundsModalProps {
    onClose: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

export function InsufficientFundsModal(props: IInsufficientFundsModalProps): JSX.Element | null {
    const {onClose} = props

    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const accountBalance = useSelector((state: IAppState) => state.account.balance)
    const framerPoints = useSelector((state: IAppState) => state.account.framerPoints)
    const isCashMode = useSelector((state: IAppState) => state.account.isCashMode)

    const goTo = useGoTo()

    if (!currentOrder) {
        return null
    }
    return (
        <Modal
            model={ModalModel.Builder.withClassName(styles.modal)
                .withBody(
                    <div>
                        <div className={styles.header}>
                            <div className={styles.icon} />
                            <p className={styles.title}>Внимание! Недостаточно средств</p>
                        </div>
                        <p className={styles.text}>
                            Для передачи в производство заказов Вам необходимо иметь <b>достаточно средств</b> на лицевом счете во Framer
                            (доступные средства Вы можете увидеть в левом верхнем углу{' '}
                            <Balance value={isCashMode ? framerPoints : accountBalance} isHideSymbol={isCashMode} isFrameSymbol={isCashMode} />
                            ).
                            <br />
                            <br />
                            Как только средств будет достаточно, Вы сможете передать свои заказы в производство и выбрать дату готовности.
                            <br />
                            <br />
                            Для ознакомления с условиями отгрузки нажмите «Продолжить».
                        </p>
                    </div>
                )
                .withPositiveAction(new Action('Пополнить баланс', () => goTo(NAV_PAYMENT)))
                .withNegativeAction(
                    new Action('Продолжить', () => {
                        goTo(`${NAV_BOOKING}?orderId=${currentOrder.id}`)
                        AnalyticsManager.trackClickTransferToProductionButton()
                    })
                )
                .withOnCloseFunc(onClose)
                .build()}
        />
    )
}
