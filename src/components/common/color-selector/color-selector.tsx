import * as React from 'react'
import {useCallback} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {ColorItem} from '../color-item/color-item'
import {useCurrentContextUserParameterConfigs} from '../../../hooks/builder'
import {useUpdateUserParameter} from '../../../hooks/orders'
import {UserParameterOption} from '../../../model/framer/user-parameter/user-parameter-option'
import styles from './color-selector.module.css'

export const ColorSelector = (): JSX.Element | null => {
    const colorParameters = useSelector((state: IAppState) => state.builder.userColorParameters)
    const currentContextUserParameterConfigs = useCurrentContextUserParameterConfigs()
    const currentContext = useSelector((state: IAppState) =>
        state.orders.currentProduct?.isEmpty ? state.orders.currentProduct.frame.innerFillings[0].solid : state.builder.currentContext
    )

    const updateUserParameter = useUpdateUserParameter()

    const isColorSelected = useCallback(
        (color: UserParameterOption) => {
            if (!currentContext || !colorParameters) {
                return
            }
            const param = currentContext.userParameters?.find(c => c.id === colorParameters.id)
            return param?.value === color.value
        },
        [colorParameters, currentContext]
    )

    if (!currentContextUserParameterConfigs || currentContextUserParameterConfigs.length === 0 || !colorParameters) {
        return null
    }

    const {values} = colorParameters

    return (
        <div className={styles.selector}>
            <div className={styles.colors}>
                {values &&
                    values.map((color, index) => {
                        return color.hex ? (
                            <ColorItem
                                key={`${color.id}-${index}`}
                                onClick={() => updateUserParameter(color, undefined)}
                                isSelected={isColorSelected(color)}
                                name={color.value}
                                color={color.hex}
                            />
                        ) : (
                            ''
                        )
                    })}
            </div>
        </div>
    )
}
