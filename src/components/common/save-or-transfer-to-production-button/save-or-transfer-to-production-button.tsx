import * as React from 'react'
import {useIsExternalUser, usePermissions, useIsDemoUser} from '../../../hooks/permissions'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {ButtonWithHint} from '../button/button-with-hint/button-with-hint'
import {HINTS} from '../../../constants/hints'
import {Button} from '../button/button'
import {NAV_BOOKING} from '../../../constants/navigation'
import {AnalyticsManager} from '../../../analytics-manager'
import {useEffect, useRef, useState} from 'react'
import {InsufficientFundsModal, useShouldShowInsufficientFundsModal} from '../insufficient-funds-modal/insufficient-funds-modal'
import {useRecalculateOrder} from '../../../hooks/orders'
import {useGoTo} from '../../../hooks/router'
import {HeaderControls} from '../../mobile/header/header-controls'
import {OrderStates} from '../../../model/framer/order-states'

interface ISaveOrTransferToProductionButtonProps {
    className?: string
}

export function SaveOrTransferToProductionButton(props: ISaveOrTransferToProductionButtonProps): JSX.Element | null {
    const {className} = props

    const bottomButtonRef = useRef<HTMLButtonElement>(null)
    const [isVisibleTopButton, setIsVisibleTopButton] = useState(false)
    const [isVisibleInsufficientFundsModal, setIsVisibleInsufficientFundsModal] = useState(false)
    const [isRecalculateInProgress, setIsRecalculateInProgress] = useState(false)

    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const hasShowTemplate = useSelector((state: IAppState) => new URLSearchParams(state.router.location.search).has('showTemplate'))
    const configContext = useSelector((state: IAppState) => state.builder.configContext)
    const isRestrictedMode = useSelector((state: IAppState) => state.settings.isRestrictedMode)
    const isCashMode = useSelector((state: IAppState) => state.account.isCashMode)

    const shouldShowInsufficientFundsModal = useShouldShowInsufficientFundsModal()
    const permission = usePermissions()
    const isExternalUser = useIsExternalUser()
    const recalculateOrder = useRecalculateOrder()
    const isDemoUser = useIsDemoUser()
    const goTo = useGoTo()

    useEffect(() => {
        configContext && setIsVisibleTopButton(true)
    }, [configContext])

    useEffect(() => {
        const observer = new IntersectionObserver(
            (entries: IntersectionObserverEntry[]) => {
                setIsVisibleTopButton(!!configContext || !entries[0].isIntersecting)
            },
            {threshold: 0.5}
        )
        if (bottomButtonRef.current) {
            observer.observe(bottomButtonRef.current)
        }

        return () => {
            observer.disconnect()
        }
    }, [configContext])

    if (!currentOrder) {
        return null
    }

    return (
        <>
            {isVisibleTopButton &&
                !currentOrder.readonly &&
                !hasShowTemplate &&
                (currentOrder.changed || currentOrder.state === OrderStates.NeedsRecalc) && (
                    <HeaderControls>
                        <Button
                            buttonStyle="with-fill"
                            isDisabled={currentOrder.readonly || isRecalculateInProgress}
                            onClick={() => {
                                setIsRecalculateInProgress(true)
                                recalculateOrder(currentOrder, () => {
                                    setIsRecalculateInProgress(false)
                                })
                            }}>
                            Пересчитать
                        </Button>
                    </HeaderControls>
                )}
            {!currentOrder.changed &&
            !currentOrder.needRecalc &&
            currentOrder.producible &&
            !isVisibleTopButton &&
            currentOrder?.calculatedWithFramerPoints === isCashMode ? (
                isExternalUser ? null : (
                    !isRestrictedMode && (
                        <ButtonWithHint
                            hint={
                                permission.isNonQualified
                                    ? HINTS.nonQualifiedRestriction
                                    : isDemoUser
                                    ? HINTS.demoAccessProducingRestriction
                                    : ''
                            }>
                            <Button
                                dataTest="order-page-to-production-button"
                                buttonStyle="with-fill"
                                className={className}
                                isDisabled={permission.isNonQualified || isDemoUser || permission.isCommonManager}
                                onClick={e => {
                                    e.stopPropagation()
                                    if (shouldShowInsufficientFundsModal) {
                                        setIsVisibleInsufficientFundsModal(true)
                                    } else {
                                        goTo(`${NAV_BOOKING}?orderId=${currentOrder.id}`)
                                    }
                                    AnalyticsManager.trackClickTransferToProductionButton()
                                }}>
                                Передать в производство
                            </Button>
                        </ButtonWithHint>
                    )
                )
            ) : (
                <ButtonWithHint hint={currentOrder.readonly ? 'Редактирование недоступно' : ''}>
                    <Button
                        ref={bottomButtonRef}
                        dataTest="order-page-recalculate-button"
                        dataId="order-page-recalculate-button"
                        className={className}
                        buttonStyle={currentOrder.changed ? 'with-fill' : 'with-border'}
                        isDisabled={currentOrder.readonly}
                        onClick={e => {
                            e.stopPropagation()
                            setIsRecalculateInProgress(true)
                            recalculateOrder(currentOrder, () => {
                                setIsRecalculateInProgress(false)
                            })
                            setIsVisibleTopButton(false)
                        }}>
                        Пересчитать
                    </Button>
                </ButtonWithHint>
            )}
            {isVisibleInsufficientFundsModal && <InsufficientFundsModal onClose={() => setIsVisibleInsufficientFundsModal(false)} />}
        </>
    )
}
