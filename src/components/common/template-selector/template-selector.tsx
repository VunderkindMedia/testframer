import * as React from 'react'
import {useCallback, useEffect, useRef, useState} from 'react'
import {Construction} from '../../../model/framer/construction'
import {Order} from '../../../model/framer/order'
import {Sides} from '../../../model/sides'
import {Product} from '../../../model/framer/product'
import {TEMPLATES_BY_CATEGORY} from './templates'
import {Template} from '../../../model/template'
import {ArrowTooltip} from '../arrow-tooltip/arrow-tooltip'
import {useSelector} from 'react-redux'
import {useAppContext} from '../../../hooks/app'
import {getClone} from '../../../helpers/json'
import {IProfileJSON} from '../../../model/framer/profile'
import {IFurnitureJSON} from '../../../model/framer/furniture'
import {getPreparedText} from '../../../helpers/text'
import {CloseButton} from '../button/close-button/close-button'
import {requiredFurnitureMaco} from '../../../helpers/furniture'
import {useModalOpen} from '../../../hooks/ui'
import {usePartnerAccess} from '../../../hooks/permissions'
import {IAppState} from '../../../redux/root-reducer'
import styles from './template-selector.module.css'
import {BuildTypes} from '../../../model/build-types'
import {FactoryTypes} from '../../../model/framer/factory-types'
import {ITemplateTab, TemplateTabs} from './template-tabs/template-tabs'
import {Partner} from '../../../model/framer/partner'

export interface ITemplateSelectorProps {
    partner?: Partner | null
    currentTemplate: Template | null
    templates: Template[]
    currentOrder: Order | null
    currentConstruction: Construction | null
    currentProduct: Product | null
    isVisibleTemplateSelector: boolean
    connectingSide: Sides | null
    currentCategory: string
    templateFilter: (construction: Construction) => boolean
    onSelect: (template: Template | null) => void
    hideTemplateSelector: () => void
    selectTemplateCategory: (category: string) => void
}

export function TemplateSelector(props: ITemplateSelectorProps): JSX.Element | null {
    const {
        partner,
        currentTemplate,
        templates,
        currentOrder,
        currentConstruction,
        currentProduct,
        isVisibleTemplateSelector,
        connectingSide,
        currentCategory,
        templateFilter,
        onSelect,
        hideTemplateSelector,
        selectTemplateCategory
    } = props
    const search = useSelector((state: IAppState) => state.router.location.search)
    const selectorRef = useRef<HTMLDivElement | null>(null)
    const [showTopSelector, setShowTopSelector] = useState(false)

    useModalOpen(isVisibleTemplateSelector)
    const {isMobile} = useAppContext()
    const {isPermPartner} = usePartnerAccess()

    useEffect(() => {
        selectorRef?.current?.scroll(0, 0)
    }, [selectorRef, search])

    useEffect(() => {
        const pageParams = new URLSearchParams(search)
        setShowTopSelector(pageParams.has('steps'))
    }, [search])

    const templateRequiredFurnitureMaco = useCallback((template: Template): boolean => {
        return template.data?.products.some(p => requiredFurnitureMaco(p)) ?? false
    }, [])

    const templateIsDisabled = useCallback(
        (template: Template): boolean => {
            if (!connectingSide || !template.data || !currentConstruction || !currentProduct) {
                return false
            }

            const defaultProfile = currentProduct.availableProfiles.find(p => p.isDefault) ?? currentProduct.availableProfiles[0]
            const defaultFurniture = currentProduct.availableFurniture.find(p => p.isDefault) ?? currentProduct.availableFurniture[0]

            if (!defaultProfile || !defaultFurniture) {
                return false
            }

            const profileJson = getClone<IProfileJSON>(defaultProfile)
            const furnitureJson = getClone<IFurnitureJSON>(defaultFurniture)

            template.data.products.forEach(productObj => {
                productObj.profile = profileJson
                productObj.furniture = furnitureJson
            })

            const construction = Construction.parse(template.data)

            if (!templateFilter(construction)) {
                return true
            }

            const hostConstructionClone = currentConstruction.clone
            const hostProductClone = hostConstructionClone.products.find(p => p.productGuid === currentProduct.productGuid)

            if (connectingSide && hostProductClone && hostConstructionClone && hostConstructionClone.products.length > 1) {
                // TODO: затыки в производительности
                // нужно просто запилить метод который без реального присоединения будет давать координаты
                // присоединямого изделия

                //hostConstructionClone.connectProduct(hostProductClone, construction.products[0], connectingSide)
                const connectedProduct = hostConstructionClone.products.find(p => p.productGuid === construction.products[0].productGuid)

                return hostConstructionClone.products
                    .filter(p => p.productGuid !== connectedProduct?.productGuid)
                    .some(p => connectedProduct && p.frame.containerPolygon.hasIntersectionWith(connectedProduct.frame.containerPolygon))
            }

            return false
        },
        [connectingSide, currentConstruction, currentProduct, templateFilter]
    )

    if (!isVisibleTemplateSelector) {
        return null
    }

    let padding = 0
    if (!isMobile) {
        const widthSum = templates.map(s => s.size.width).reduce((totalWidth, width) => totalWidth + width, 0)
        const clientArea = document.body.clientWidth - 48
        if (widthSum > clientArea && widthSum / 2 <= clientArea && templates[0].size.height * 2 <= document.body.clientHeight) {
            padding = (clientArea - widthSum / 2) / 2
        }
    }

    const templateTabs: ITemplateTab[] = [
        {
            id: 'energy',
            title: 'Окна ENERGY',
            onClick: () => selectTemplateCategory('energy')
        },
        {
            id: 'pvh',
            title: 'ПВХ',
            onClick: () => selectTemplateCategory('pvh')
        }
    ]

    if (partner?.isColdAlAvailable) {
        templateTabs.push({
            id: 'aluminium',
            title: 'Алюминий',
            onClick: () => selectTemplateCategory('aluminium')
        })
    }

    return (
        <div
            ref={selectorRef}
            className={`${styles.templateSelector} ${isMobile && styles.mobile}`}
            data-test="template-selector"
            data-id="template-selector">
            {!isMobile && (
                <>
                    {showTopSelector && (
                        <div className={styles.header}>
                            <p className={styles.headerTitle}>Шаблон изделия:</p>
                            <div className={styles.groupIcons}>
                                {TEMPLATES_BY_CATEGORY[currentCategory]
                                    .filter(t => !t.isHidden)
                                    .map(template => (
                                        <ArrowTooltip key={template.groupTitle} title={template.title} placement="bottom">
                                            <div
                                                onClick={() => onSelect(template)}
                                                className={`${styles.groupIcon} white-hover ${
                                                    template.contains(templates) && styles.selected
                                                }`}>
                                                <img src={template.groupImg} alt="" draggable={false} />
                                            </div>
                                        </ArrowTooltip>
                                    ))}
                            </div>
                        </div>
                    )}
                    <p className={styles.title}>{currentTemplate?.groupTitle ?? 'Выберите шаблон'}</p>
                </>
            )}
            {isPermPartner && !showTopSelector && <TemplateTabs tabs={templateTabs} activeTab={currentCategory} className={styles.tabs} />}
            <div className={styles.templates} style={{padding: `0 ${padding}px`}}>
                <div className={styles.rows} data-id="templates">
                    {templates
                        .filter(t => !t.isHidden)
                        .map(template => {
                            const isDisabled = templateIsDisabled(template)

                            const element = (
                                <div
                                    key={template.id}
                                    onClick={() => !isDisabled && onSelect(template)}
                                    data-test={template.id}
                                    className={`${styles.item} ${template.id} ${isDisabled && styles.inactive}`}
                                    style={{
                                        height: `${template.size.height}px`,
                                        width: `${template.size.width}px`,
                                        paddingBottom: `${template.paddingBottom}px`
                                    }}>
                                    <img src={template.img} alt="" draggable={false} />
                                    <p style={{marginTop: `${template.textMarginTop}px`}}>{getPreparedText(template.title)}</p>
                                    {process.env.BUILD_TYPE === BuildTypes.Dev && (
                                        <div className={styles.debugInfo}>
                                            {template.buildTypeFilter && <p>{template.buildTypeFilter}</p>}
                                            {template.factoryTypeFilter && (
                                                <p>{template.factoryTypeFilter === FactoryTypes.Perm ? 'Пермь' : 'Москва'}</p>
                                            )}
                                        </div>
                                    )}
                                </div>
                            )

                            if (isDisabled) {
                                return (
                                    <ArrowTooltip
                                        key={`a-${template.id}`}
                                        title="Можно присоединять только отдельные прямоугольные изделия (окна и/или двери)"
                                        placement="bottom">
                                        {element}
                                    </ArrowTooltip>
                                )
                            }
                            if (templateRequiredFurnitureMaco(template)) {
                                return (
                                    <ArrowTooltip key={`a-${template.id}`} title="Будет расчитан только с фурнитурой Maco" placement="bottom">
                                        {element}
                                    </ArrowTooltip>
                                )
                            }

                            return element
                        })}
                </div>
            </div>
            {currentOrder && currentOrder.constructions.length > 0 && !isMobile && (
                <CloseButton
                    className={styles.closeButton}
                    style={{transform: `translateY(${templates === TEMPLATES_BY_CATEGORY[currentCategory] || isMobile ? 0 : '56px'})`}}
                    onClick={hideTemplateSelector}
                />
            )}
        </div>
    )
}
