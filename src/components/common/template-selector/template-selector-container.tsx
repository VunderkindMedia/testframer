import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useCallback, useEffect, useState} from 'react'
import {TemplateSelector} from './template-selector'
import {TEMPLATES, TEMPLATES_BY_CATEGORY} from './templates'
import {useSelectTemplate, useShowTemplateSelector, useHideTemplateSelector, useSelectTemplateCategory} from '../../../hooks/template-selector'
import {Template} from '../../../model/template'

export function TemplateSelectorContainer(): JSX.Element | null {
    const partner = useSelector((state: IAppState) => state.account.partner)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const connectingSide = useSelector((state: IAppState) => state.templateSelector.connectingSide)
    const templateFilter = useSelector((state: IAppState) => state.templateSelector.templateFilter)
    const currentTemplate = useSelector((state: IAppState) => state.templateSelector.currentTemplate)
    const search = useSelector((state: IAppState) => state.router.location.search)

    const hideTemplateSelector = useHideTemplateSelector()
    const selectTemplate = useSelectTemplate()
    const showTemplateSelector = useShowTemplateSelector()
    const selectTemplateCategory = useSelectTemplateCategory()

    const [templates, setTemplates] = useState<Template[]>([])
    const [isVisibleTemplateSelector, setIsVisibleTemplateSelector] = useState(false)
    const [currentCategory, setCurrentCategory] = useState('')

    useEffect(() => {
        const needToHide = (template: Template): boolean => {
            if (template.buildTypeFilter) {
                return template.buildTypeFilter !== process.env.BUILD_TYPE
            }
            if (template.factoryTypeFilter) {
                return template.factoryTypeFilter !== partner?.factoryId
            }
            return false
        }

        TEMPLATES.forEach(template => {
            template.allTemplates.forEach(template => {
                template.isHidden = needToHide(template)
            })
            template.isHidden = needToHide(template) || template.templates.length === 0
        })
    }, [partner])

    const getTemplates = useCallback((templates: Template[], groupIds: string[], step: number): Template | undefined => {
        let groupTemplate = templates.find(t => t.id === groupIds[step])
        if (groupTemplate && groupTemplate?.depth < groupIds.length) {
            groupTemplate = getTemplates(groupTemplate.templates, groupIds, step + 1)
        }
        return groupTemplate
    }, [])

    const parseTemplateParams = useCallback(
        (search: string) => {
            const pageParams = new URLSearchParams(search)
            setIsVisibleTemplateSelector(pageParams.has('showTemplate'))
            let steps: string | null = null
            let category: string | null = null

            if (!pageParams.has('showTemplate') || !pageParams.has('category')) {
                return
            }

            category = pageParams.get('category')
            if (!category) {
                return
            }
            setCurrentCategory(category)
            const currentTemplates = TEMPLATES_BY_CATEGORY[category] || []

            if (pageParams.has('steps')) {
                steps = pageParams.get('steps')
            }
            if (steps) {
                const groupIds = steps.split(',')
                const groupTemplate = getTemplates(currentTemplates, groupIds, 0)
                groupTemplate && selectTemplate(groupTemplate, category)
                groupTemplate && setTemplates(groupTemplate.templates)
            } else {
                selectTemplate(null, category)
                setTemplates(currentTemplates)
            }
        },
        [selectTemplate, getTemplates]
    )

    useEffect(() => {
        parseTemplateParams(search)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [search])

    const templateSelectHandler = useCallback(
        (template: Template | null) => {
            const pageParams = new URLSearchParams(search)
            const category = pageParams.get('category') || ''
            selectTemplate(template, category)
            showTemplateSelector(template)
        },
        [selectTemplate, showTemplateSelector, search]
    )

    const props = {
        partner,
        currentTemplate,
        templates,
        currentOrder,
        currentConstruction,
        currentProduct,
        isVisibleTemplateSelector,
        connectingSide,
        currentCategory,
        templateFilter,
        onSelect: templateSelectHandler,
        hideTemplateSelector,
        selectTemplateCategory
    }

    return <TemplateSelector {...props} />
}
