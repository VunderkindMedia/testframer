import * as React from 'react'
import {useCallback} from 'react'
import styles from './template-tabs.module.css'

export interface ITemplateTab {
    id: string
    title: string
    onClick: () => void
}

interface ITemplateTabsProps {
    tabs: ITemplateTab[]
    activeTab: string
    className?: string
}

export const TemplateTabs = (props: ITemplateTabsProps): JSX.Element | null => {
    const {tabs, activeTab, className} = props

    const clickHandler = useCallback((e: React.MouseEvent, onClick: () => void) => {
        e.preventDefault()
        onClick()
    }, [])

    if (!tabs.length) {
        return null
    }

    return (
        <ul className={`${styles.tabs} ${className && className}`}>
            {tabs.map(({id, title, onClick}) => (
                <li key={id} className={`${styles.tab} ${activeTab === id && styles.active}`}>
                    <a href="" onClick={e => clickHandler(e, onClick)}>
                        {title}
                    </a>
                </li>
            ))}
        </ul>
    )
}
