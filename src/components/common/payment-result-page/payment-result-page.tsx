import * as React from 'react'
import {useCallback, useEffect} from 'react'
import styles from './payment-result-page.module.css'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Link} from 'react-router-dom'
import {getDefaultOrdersLink} from '../../../helpers/orders'
import {RUB_SYMBOL} from '../../../constants/strings'
import {checkout, setPaymentState} from '../../../redux/account/actions'
import {getCurrency} from '../../../helpers/currency'

export function PaymentResultPage(): JSX.Element | null {
    const location = useSelector((state: IAppState) => state.router.location)
    const ordersOnPage = useSelector((state: IAppState) => state.orders.ordersOnPage)
    const paymentState = useSelector((state: IAppState) => state.account.paymentState)

    const dispatch = useDispatch()
    const resetPaymentState = useCallback(() => dispatch(setPaymentState(null)), [dispatch])
    const checkoutAction = useCallback((reservationId: number) => dispatch(checkout(reservationId)), [dispatch])

    const pageParams = new URLSearchParams(location.search)

    const isSuccess = pageParams.get('s') === '1'
    const reservationId = pageParams.get('r')

    useEffect(() => {
        if (reservationId) {
            checkoutAction(+reservationId)
        }

        return () => {
            resetPaymentState()
        }
    }, [reservationId, checkoutAction, resetPaymentState])

    if (isSuccess === null) {
        return null
    }

    if (!paymentState) {
        return null
    }

    const paidAtStr = new Intl.DateTimeFormat('ru', {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit'
    }).format(new Date(paymentState.createdAt))

    const sum = getCurrency(paymentState.amount)
    const isPaymentDestinationOrders = paymentState.reservationType === 1

    return (
        <div className={`${styles.page} ${isSuccess ? styles.success : ''}`}>
            <div className={styles.content}>
                <div className={styles.card}>
                    <div className={styles.shortInfo}>
                        <div className={styles.icon} />
                        {isSuccess ? (
                            <p>
                                Успешная
                                <br />
                                оплата!
                            </p>
                        ) : (
                            <p>Error!</p>
                        )}
                    </div>
                    <div className={styles.detailInfo}>
                        <p className={styles.title}>Вам было выслано письмо с информацией об оплате.</p>
                        <div className={styles.texts}>
                            <div>
                                <p>Дата платежа:</p>
                                <p>Сумма:</p>
                            </div>
                            <div>
                                <p>{paidAtStr}</p>
                                <p>
                                    {sum} {RUB_SYMBOL}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                {isPaymentDestinationOrders && (
                    <p className={styles.ordersLink}>
                        Статус заказа можно увидеть в<Link to={getDefaultOrdersLink(ordersOnPage)}>Ваших заказах</Link>.
                    </p>
                )}
            </div>
        </div>
    )
}
