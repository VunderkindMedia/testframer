import * as React from 'react'
import {connect} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Construction} from '../../../model/framer/construction'
import {ReactNode} from 'react'
import {setCurrentOrder} from '../../../redux/orders/actions'
import {bindActionCreators} from 'redux'
import {Order} from '../../../model/framer/order'
import {resetTemplateSelector} from '../../../redux/template-selector/actions'
import {HistoryManager} from '../../../history-manager'

interface IBuilderPageErrorBoundaryProps {
    currentConstruction: Construction | null
    setCurrentOrder: (order: Order | null) => void
    hideTemplateSelector: () => void
    children?: ReactNode
}

class BuilderPageErrorBoundary extends React.Component<IBuilderPageErrorBoundaryProps> {
    componentDidCatch(): void {
        this.props.currentConstruction?.id && HistoryManager.undo(this.props.currentConstruction?.id)
    }

    componentWillUnmount(): void {
        this.props.hideTemplateSelector()
        this.props.setCurrentOrder(null)
        HistoryManager.clear()
    }

    render(): React.ReactNode {
        return this.props.children
    }
}

export const BuilderPageErrorBoundaryConnected = connect(
    (state: IAppState) => ({
        currentConstruction: state.orders.currentConstruction
    }),
    dispatch => ({
        setCurrentOrder: bindActionCreators(setCurrentOrder, dispatch),
        hideTemplateSelector: bindActionCreators(resetTemplateSelector, dispatch)
    })
)(BuilderPageErrorBoundary)
