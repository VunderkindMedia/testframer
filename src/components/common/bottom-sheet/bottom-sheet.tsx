import * as React from 'react'
import {ModalOverlay} from '../modal/modal-overlay/modal-overlay'
import {ReactNode, useCallback, useEffect, useState} from 'react'
import {getClassNames} from '../../../helpers/css'
import {BodyPortal} from '../body-portal/body-portal'
import {useAppContext} from '../../../hooks/app'
import styles from './bottom-sheet.module.css'

interface IBottomSheetProps {
    isVisible: boolean
    children?: ReactNode
    onHide?: () => void
}

export function BottomSheet(props: IBottomSheetProps): JSX.Element {
    const {children, onHide} = props

    const [isLocalVisible, setIsLocalVisible] = useState(false)
    const {isMobile} = useAppContext()

    const hide = useCallback(() => setIsLocalVisible(false), [])

    useEffect(() => {
        setIsLocalVisible(props.isVisible)
    }, [props.isVisible])

    const isVisible = props.isVisible && isLocalVisible

    const closeHandler = useCallback(() => {
        onHide && onHide()
    }, [onHide])

    useEffect(() => {
        isVisible && window.addEventListener('click', closeHandler)

        return () => {
            window.removeEventListener('click', closeHandler)
        }
    }, [isVisible, closeHandler])

    return (
        <>
            {isVisible && <ModalOverlay className={styles.overlay} onClick={hide} />}
            <BodyPortal>
                <div
                    onClick={e => e.stopPropagation()}
                    className={getClassNames(styles.sheet, {[styles.mobile]: isMobile, [styles.visible]: isVisible})}>
                    {children}
                </div>
            </BodyPortal>
        </>
    )
}
