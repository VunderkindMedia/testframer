import * as React from 'react'
import {isWhiteColor} from '../../../helpers/color'
import {getPreparedText} from '../../../helpers/text'
import styles from './color-item.module.css'

interface IColorItemProps {
    name: string
    color: string
    isSelected?: boolean
    onClick?: () => void
}

export function ColorItem(props: IColorItemProps): JSX.Element {
    const {name, color, isSelected, onClick} = props

    return (
        <div onClick={() => onClick && onClick()} className={`${styles.colorItem} blue-hover ${isSelected && 'selected'}`}>
            <div
                className={`${styles.icon} ${isWhiteColor(color) && styles.white}`}
                style={{
                    backgroundColor: color
                }}
            />
            <p>{getPreparedText(name)}</p>
        </div>
    )
}
