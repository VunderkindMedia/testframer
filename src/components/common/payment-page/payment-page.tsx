import * as React from 'react'
import {useCallback, useEffect, useState} from 'react'
import {PriceInput} from '../price-input/price-input'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {deposit, loadBill} from '../../../redux/account/actions'
import {WithTitle} from '../with-title/with-title'
import {Button} from '../button/button'
import {PageTitle} from '../page-title/page-title'
import {RUB_SYMBOL} from '../../../constants/strings'
import {usePermissions, useIsExternalUser} from '../../../hooks/permissions'
import {useAppContext} from '../../../hooks/app'
import visa from './resources/card1.svg'
import masterCard from './resources/card2.svg'
import visaElectron from './resources/card3.svg'
import maestro from './resources/card4.svg'
import secureCode from './resources/card5.svg'
import mir from './resources/card6.svg'
import {HeaderTitle} from '../../mobile/header/header-title'
import {ConfirmDealerLink} from '../confirm-dealer-link/confirm-dealer-link'
import {PaymentHistory} from '../payment-history/payment-history'
import styles from './payment-page.module.css'

export function PaymentPage(): JSX.Element | null {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)

    const permission = usePermissions()
    const isExternalUser = useIsExternalUser()
    const {isMobile} = useAppContext()
    const dispatch = useDispatch()

    const loadBillAction = useCallback((sum: number) => dispatch(loadBill(sum)), [dispatch])
    const depositAction = useCallback((amount: number) => dispatch(deposit(amount)), [dispatch])

    const [amount, setAmount] = useState(0)

    useEffect(() => {
        const pageParams = new URLSearchParams(search)

        const sumStr = pageParams.get('sum')

        const sum = sumStr ? +sumStr : 0

        if (sum) {
            setAmount(sum)
        }
    }, [search])

    if (!currentDealer || isExternalUser) {
        return null
    }

    const isLegalPerson = currentDealer.type === 0

    return (
        <div className={`${styles.page} ${isMobile ? styles.mobile : ''}`}>
            {isMobile ? <HeaderTitle>Пополнение баланса</HeaderTitle> : <PageTitle title="Пополнение баланса" className={styles.title} />}
            <div className={styles.content}>
                {permission.isNonQualified && (
                    <ConfirmDealerLink className={styles.permissionError} title="Для пополнения баланса необходимо" />
                )}

                <div className={styles.costGroup}>
                    <WithTitle title="Сумма пополнения:">
                        <PriceInput
                            className={styles.costInput}
                            value={amount / 100}
                            onChange={value => setAmount(value * 100)}
                            currency={RUB_SYMBOL}
                            isDisabled={permission.isNonQualified}
                        />
                    </WithTitle>
                </div>
                <Button
                    buttonStyle="with-fill"
                    className={styles.payButton}
                    isDisabled={isNaN(amount) || amount <= 0}
                    onClick={() => {
                        if (isLegalPerson) {
                            loadBillAction(amount)
                            return
                        }
                        depositAction(amount)
                    }}>
                    {isLegalPerson ? 'Сформировать счет на оплату' : 'Пополнить банковской картой'}
                </Button>
                {!isLegalPerson && (
                    <div className={styles.cards}>
                        <div>
                            <img src={visa} alt="" draggable={false} />
                            <img src={masterCard} alt="" draggable={false} />
                            <img src={visaElectron} alt="" draggable={false} />
                            <img src={maestro} alt="" draggable={false} />
                        </div>
                        <div>
                            <img src={secureCode} alt="" draggable={false} />
                            <img src={mir} alt="" draggable={false} />
                        </div>
                    </div>
                )}
                <PaymentHistory className={styles.historyTitle} />
            </div>
        </div>
    )
}
