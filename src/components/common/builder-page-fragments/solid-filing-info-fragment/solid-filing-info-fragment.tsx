import * as React from 'react'
import {OpeningSystemSelector} from '../../../desktop/new-builder-page-sidebar/opening-system-selector/opening-system-selector'
import {FillingMarkingSidebarItem} from '../../../desktop/new-builder-page-sidebar/filling-marking-sidebar-item/filling-marking-sidebar-item'
import {FillingGlassColorSidebarItem} from '../../../desktop/new-builder-page-sidebar/filling-glass-color-sidebar-item/filling-glass-color-sidebar-item'
import {ImpostSelectorFragment} from '../impost-selector-fragment/impost-selector-fragment'
import {UserParametersFragment} from '../user-parameters-fragment/user-parameters-fragment'
import {FrameSizeInfoFragment} from '../frame-size-info-fragment/frame-size-info-fragment'

export function SolidFilingInfoFragment(): JSX.Element {
    return (
        <>
            <OpeningSystemSelector />
            <FillingMarkingSidebarItem />
            <FillingGlassColorSidebarItem />
            <ImpostSelectorFragment />
            <UserParametersFragment />
            <FrameSizeInfoFragment />
        </>
    )
}
