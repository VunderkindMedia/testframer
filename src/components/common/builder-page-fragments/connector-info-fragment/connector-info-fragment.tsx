import * as React from 'react'
import {
    NewBuilderPageSidebarItem,
    NewBuilderPageSidebarItemText
} from '../../../desktop/new-builder-page-sidebar/new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {RemoveSidebarItem} from '../../../desktop/new-builder-page-sidebar/remove-sidebar-item/remove-sidebar-item'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {Connector} from '../../../../model/framer/connector'
import {ConnectorTypes} from '../../../../model/framer/connector-types'
import {useRemoveConnector, useSetConfigContext} from '../../../../hooks/builder'
import {ConfigContexts} from '../../../../model/config-contexts'
import {isRemovableConnector} from '../../../../helpers/builder'
import {Button} from '../../button/button'
import {UserParametersFragment} from '..//user-parameters-fragment/user-parameters-fragment'

export function ConnectorInfoFragment(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const setConfigContext = useSetConfigContext()

    const removeConnector = useRemoveConnector(currentOrder, currentContext)

    if (!(currentContext instanceof Connector)) {
        return null
    }

    const isRemovable = isRemovableConnector(currentProduct, currentContext)

    return (
        <>
            <NewBuilderPageSidebarItem>
                <NewBuilderPageSidebarItemText>
                    {currentContext.connectorType === ConnectorTypes.Extender ? 'Расширитель' : 'Соединитель'}:&nbsp;
                    <Button onClick={() => setConfigContext(ConfigContexts.Connector)}>{currentContext.profileVendorCode}</Button>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>
            {isRemovable && <RemoveSidebarItem onClick={removeConnector} />}
            <UserParametersFragment />
        </>
    )
}
