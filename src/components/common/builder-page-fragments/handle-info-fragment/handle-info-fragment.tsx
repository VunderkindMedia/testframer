import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {useSetConfigContext, useSetHandlePositionType} from '../../../../hooks/builder'
import {useCallback, useRef} from 'react'
import {MobileSidebar} from '../../../mobile/mobile-sidebar/mobile-sidebar'
import {HandlePositionTypes} from '../../../../model/framer/handle-position-types'
import {Handle} from '../../../../model/handle'
import {
    NewBuilderPageSidebarItem,
    NewBuilderPageSidebarItemText
} from '../../../desktop/new-builder-page-sidebar/new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {SelectorWithIcon} from '../../selector-with-icon/selector-with-icon'
import {ConfigContexts} from '../../../../model/config-contexts'
import fixedHandleSmall from './resources/icon_1.svg'
import centredHandleSmall from './resources/icon_2.svg'
import {Button} from '../../button/button'

export function HandleInfoFragment(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const setHandlePositionType = useSetHandlePositionType(currentOrder, currentContext)
    const sidebarRef = useRef<MobileSidebar | null>(null)

    const setConfigContext = useSetConfigContext()

    const selectHandlePositionType = useCallback(
        (handlePositionType: HandlePositionTypes) => {
            setHandlePositionType(handlePositionType)
            sidebarRef.current?.hide()
        },
        [setHandlePositionType]
    )

    if (!currentOrder || !(currentContext instanceof Handle)) {
        return null
    }

    const {type, color} = currentContext

    return (
        <>
            <NewBuilderPageSidebarItem>
                <NewBuilderPageSidebarItemText style={{marginBottom: '8px'}}>
                    Положение ручки:&nbsp;
                    <span>{currentContext.handlePositionType === HandlePositionTypes.Fixed ? 'Фиксированное' : 'Центральное'}</span>
                </NewBuilderPageSidebarItemText>
                <SelectorWithIcon
                    icons={[
                        {
                            src: fixedHandleSmall,
                            hint: 'Фиксированное',
                            selected: currentContext.handlePositionType === HandlePositionTypes.Fixed,
                            onClick: () => selectHandlePositionType(HandlePositionTypes.Fixed)
                        },
                        {
                            src: centredHandleSmall,
                            hint: 'Центральное',
                            selected: currentContext.handlePositionType === HandlePositionTypes.Center,
                            onClick: () => selectHandlePositionType(HandlePositionTypes.Center)
                        }
                    ]}
                />
            </NewBuilderPageSidebarItem>
            {!currentProduct?.isEntranceDoor && (
                <NewBuilderPageSidebarItem>
                    <NewBuilderPageSidebarItemText>
                        Тип ручки:&nbsp;
                        <Button onClick={() => setConfigContext(ConfigContexts.HandleSelector)}>
                            {type}, {color.toLowerCase()}
                        </Button>
                    </NewBuilderPageSidebarItemText>
                </NewBuilderPageSidebarItem>
            )}
        </>
    )
}
