import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {useRemoveImpost, useSetImpostOffset} from '../../../../hooks/builder'
import {Impost} from '../../../../model/framer/impost'
import {
    NewBuilderPageSidebarItem,
    NewBuilderPageSidebarItemText
} from '../../../desktop/new-builder-page-sidebar/new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import verticalImpost from './resources/vertical__offset_icon.svg'
import horizontalImpost from './resources/horizontal_offset_icon.svg'
import {InputNumber} from '../../input-number/input-number'
import {BeamMarkingSidebarItem} from '../../../desktop/new-builder-page-sidebar/beam-marking-sidebar-item/beam-marking-sidebar-item'
import {RemoveSidebarItem} from '../../../desktop/new-builder-page-sidebar/remove-sidebar-item/remove-sidebar-item'
import styles from './impost-info-fragment.module.css'

export function ImpostInfoFragment(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const setImpostOffset = useSetImpostOffset(currentOrder, currentContext)
    const removeImpost = useRemoveImpost(currentOrder, currentContext)

    if (!(currentContext instanceof Impost)) {
        return null
    }

    return (
        <>
            <NewBuilderPageSidebarItem>
                <NewBuilderPageSidebarItemText>
                    Отступ {currentContext.isVertical ? 'слева' : 'снизу'}:
                    <img
                        src={currentContext.isVertical ? verticalImpost : horizontalImpost}
                        alt=""
                        style={{margin: '0 8px'}}
                        draggable={false}
                    />
                    <InputNumber
                        className={styles.sizeInput}
                        style={{marginRight: '8px'}}
                        value={currentContext.isVertical ? currentContext.offsetLeft : currentContext.offsetBottom}
                        onKeyPressEnter={setImpostOffset}
                    />
                    <span>мм</span>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>
            <BeamMarkingSidebarItem />
            <RemoveSidebarItem onClick={removeImpost} />
        </>
    )
}
