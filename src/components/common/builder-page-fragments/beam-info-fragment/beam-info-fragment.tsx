import * as React from 'react'
import {BeamMarkingSidebarItem} from '../../../desktop/new-builder-page-sidebar/beam-marking-sidebar-item/beam-marking-sidebar-item'

export function BeamInfoFragment(): JSX.Element | null {
    return <BeamMarkingSidebarItem />
}
