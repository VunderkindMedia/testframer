import * as React from 'react'
import {
    NewBuilderPageSidebarItem,
    NewBuilderPageSidebarItemText
} from '../../../desktop/new-builder-page-sidebar/new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {useDispatch} from 'react-redux'
import {WithTitle} from '../../with-title/with-title'
import {ListItem} from '../../../../model/list-item'
import {Select} from '../../select/select'
import {useUpdateUserParameter, useUserParametersContext} from '../../../../hooks/orders'
import {useCurrentContextUserParameterConfigs, useSetConfigContext} from '../../../../hooks/builder'
import {Button} from '../../button/button'
import {getClassNames} from '../../../../helpers/css'
import {isWhiteColor} from '../../../../helpers/color'
import {getColorSelectorConfigContext} from '../../../../helpers/builder'
import {setUserColorParameters} from '../../../../redux/builder/actions'
import {ArrowTooltip} from '../../../common/arrow-tooltip/arrow-tooltip'
import styles from './user-parameters-fragment.module.css'

interface IUserParametersFragmentProps {
    className?: string
}

export function UserParametersFragment(props: IUserParametersFragmentProps): JSX.Element | null {
    const {className} = props
    const currentContext = useUserParametersContext()
    const setConfigContext = useSetConfigContext()
    const dispatch = useDispatch()
    const updateUserParameter = useUpdateUserParameter()
    const currentContextUserParameterConfigs = useCurrentContextUserParameterConfigs()

    if (!currentContextUserParameterConfigs || currentContextUserParameterConfigs.length === 0) {
        return null
    }

    const currentUserParameters = currentContext?.userParameters || []

    return (
        <>
            <NewBuilderPageSidebarItem className={`${className ?? ''}`}>
                <div className={styles.container}>
                    {currentContextUserParameterConfigs.map(config => {
                        if (config.isColor && config.values !== null) {
                            const param = currentUserParameters.find(p => p.id === config.id)
                            const color = config.values.find(v => v.value === param?.value)

                            if (!color) {
                                return null
                            }

                            return (
                                <NewBuilderPageSidebarItemText key={config.id}>
                                    <ArrowTooltip title={`${config.name}: ${color.value}`} placement="top">
                                        <Button
                                            onClick={() => {
                                                setConfigContext(getColorSelectorConfigContext(config))
                                                dispatch(setUserColorParameters(config))
                                            }}>
                                            <div className={styles.laminationButton}>
                                                {config.name}
                                                <div
                                                    className={getClassNames(styles.laminationIcon, {
                                                        [styles.whiteIcon]: !!color.hex && isWhiteColor(color.hex)
                                                    })}
                                                    style={{backgroundColor: color.hex, marginRight: '8px'}}
                                                />
                                            </div>
                                        </Button>
                                    </ArrowTooltip>
                                </NewBuilderPageSidebarItemText>
                            )
                        }

                        if (config.values === null) {
                            return null
                        }

                        const options = config.values.map(option => new ListItem(option.id.toString(), option.value, option, config.id))
                        const selectedItem = options.find(item =>
                            currentUserParameters.some(up => up.value === item.data.value && up.id === item.configId)
                        )

                        return (
                            <WithTitle key={config.id} title={config.name}>
                                <Select
                                    selectedItem={selectedItem}
                                    items={options}
                                    onSelect={item => updateUserParameter(item.data, config.name)}
                                />
                            </WithTitle>
                        )
                    })}
                </div>
            </NewBuilderPageSidebarItem>
        </>
    )
}
