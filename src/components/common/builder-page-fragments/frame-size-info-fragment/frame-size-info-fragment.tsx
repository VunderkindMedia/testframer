import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {
    NewBuilderPageSidebarItem,
    NewBuilderPageSidebarItemText
} from '../../../desktop/new-builder-page-sidebar/new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {SolidFilling} from '../../../../model/framer/solid-filling'
import {FrameFilling} from '../../../../model/framer/frame-filling'

export const FrameSizeInfoFragment = (): JSX.Element | null => {
    const currentContext = useSelector((state: IAppState) =>
        state.orders.currentProduct?.isEmpty ? state.orders.currentProduct.frame.innerFillings[0].solid : state.builder.currentContext
    )

    const hasMoskitkaSize = currentContext instanceof SolidFilling && currentContext.fillingSize
    const hasFillingSize = currentContext instanceof FrameFilling && currentContext.moskitkaSize

    if (!hasMoskitkaSize && !hasFillingSize) {
        return null
    }

    const fillingSize = hasFillingSize ? (currentContext as FrameFilling)?.moskitkaSize : (currentContext as SolidFilling)?.fillingSize

    if (!fillingSize) {
        return null
    }

    const fillingTitle = hasFillingSize ? 'Размер москитной сетки' : 'Размер стеклопакета'
    const {size, bevels} = fillingSize
    const bevelsSize = bevels.map(({x, y}) => `${x}x${y}`)

    return (
        <NewBuilderPageSidebarItem>
            <NewBuilderPageSidebarItemText style={{marginBottom: '8px'}}>{fillingTitle}:</NewBuilderPageSidebarItemText>
            <NewBuilderPageSidebarItemText>
                {size.x} x {size.y} мм {bevelsSize.length > 0 && `(угол ${bevelsSize.join('; ')})`}
            </NewBuilderPageSidebarItemText>
        </NewBuilderPageSidebarItem>
    )
}
