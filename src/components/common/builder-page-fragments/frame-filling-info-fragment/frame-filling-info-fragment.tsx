import * as React from 'react'
import {OpeningSystemSelector} from '../../../desktop/new-builder-page-sidebar/opening-system-selector/opening-system-selector'
import {BeamMarkingSidebarItem} from '../../../desktop/new-builder-page-sidebar/beam-marking-sidebar-item/beam-marking-sidebar-item'
import {MoskitkaSelector} from '../../../desktop/new-builder-page-sidebar/moskitka-selector/moskitka-selector'
import {UserParametersFragment} from '../user-parameters-fragment/user-parameters-fragment'
import {FrameSizeInfoFragment} from '../frame-size-info-fragment/frame-size-info-fragment'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'

export function FrameFillingInfoFragment(): JSX.Element {
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)

    const isProductionTypeId = currentProduct?.productionTypeId === 1197

    return (
        <>
            <OpeningSystemSelector />
            <BeamMarkingSidebarItem />
            <UserParametersFragment />
            {!isProductionTypeId && <MoskitkaSelector />}
            <FrameSizeInfoFragment />
        </>
    )
}
