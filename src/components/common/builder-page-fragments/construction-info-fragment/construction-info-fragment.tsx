import * as React from 'react'
import {useCallback, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {loadLamination} from '../../../../redux/lamination/actions'
import {useUpdateCurrentOrder} from '../../../../hooks/orders'
import {useSetConfigContext} from '../../../../hooks/builder'
import {getProductLamination} from '../../../../helpers/lamination'
import {
    NewBuilderPageSidebarItem,
    NewBuilderPageSidebarItemText
} from '../../../desktop/new-builder-page-sidebar/new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {MoskitkaTypeSelector} from '../../moskitka-type-selector/moskitka-type-selector'
import {LaminationSidebarItem} from '../../../desktop/new-builder-page-sidebar/lamination-sidebar-item/lamination-sidebar-item'
import {FillingMarkingSidebarItem} from '../../../desktop/new-builder-page-sidebar/filling-marking-sidebar-item/filling-marking-sidebar-item'
import {FillingGlassColorSidebarItem} from '../../../desktop/new-builder-page-sidebar/filling-glass-color-sidebar-item/filling-glass-color-sidebar-item'
import {ConfigContexts} from '../../../../model/config-contexts'
import {Button} from '../../button/button'
import {ArrowTooltip} from '../../arrow-tooltip/arrow-tooltip'
import {QuantityEditor} from '../../quantity-editor/quantity-editor'
import styles from './construction-info-fragment.module.css'
import {setBuilderMode} from '../../../../redux/builder/actions'
import {BuilderModes} from '../../../../model/builder-modes'
import {ConstructionGoodsAndServices} from '../../../mobile/construction-goods-and-services/construction-goods-and-services'
import {useAppContext} from '../../../../hooks/app'
import {FactoryTypes} from '../../../../model/framer/factory-types'
import {UserParametersFragment} from '../user-parameters-fragment/user-parameters-fragment'
import {MAX_CONSTRUCTION_COUNT} from '../../../../constants/settings'
import {FrameSizeInfoFragment} from '../frame-size-info-fragment/frame-size-info-fragment'
import {useTypeConstructionVisible} from '../../../../model/framer/construction-types'

export function ConstructionInfoFragment(): JSX.Element | null {
    const partner = useSelector((state: IAppState) => state.account.partner)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const builderMode = useSelector((state: IAppState) => state.builder.mode)
    const lamination = useSelector((state: IAppState) => state.lamination.all)
    const isRestrictedMode = useSelector((state: IAppState) => state.settings.isRestrictedMode)

    const {isMobile} = useAppContext()

    const dispatch = useDispatch()
    const loadLaminationAction = useCallback(() => dispatch(loadLamination()), [dispatch])

    const updateCurrentOrder = useUpdateCurrentOrder()
    const setConfigContext = useSetConfigContext()
    const typeConstructionFunction = useTypeConstructionVisible()

    useEffect(() => {
        if (lamination.length === 0) {
            loadLaminationAction()
        }
    }, [lamination.length, loadLaminationAction])

    if (!currentOrder || !currentConstruction || !currentProduct || lamination.length === 0) {
        return null
    }

    const [currentOutsideLamination, currentInsideLamination] = getProductLamination(currentProduct, lamination)

    const views: JSX.Element[] = []

    if (currentProduct.isMoskitka) {
        if (currentConstruction.products[0] === currentProduct) {
            views.push(
                <NewBuilderPageSidebarItem key="energy">
                    <NewBuilderPageSidebarItemText>Тип конструкции: {typeConstructionFunction}</NewBuilderPageSidebarItemText>
                </NewBuilderPageSidebarItem>
            )
        }
        views.push(
            <NewBuilderPageSidebarItem key="moskitka-type" border="none">
                <NewBuilderPageSidebarItemText>
                    Тип:&nbsp;<span>{currentProduct.frame.innerFillings[0].leaf?.moskitka?.catproof ? 'Антикошка' : 'Обычная'}</span>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>,
            <NewBuilderPageSidebarItem key="moskitka-type-selector" style={{paddingTop: 0}}>
                <MoskitkaTypeSelector />
            </NewBuilderPageSidebarItem>
        )
        views.push(
            <LaminationSidebarItem
                key="lamination"
                className={styles.sidebarItem}
                construction={currentConstruction}
                outsideLamination={currentOutsideLamination}
                insideLamination={currentInsideLamination}
            />
        )
    } else if (currentProduct.isEmpty) {
        const filling = currentProduct.frame.innerFillings[0].solid?.filling

        views.push(
            <NewBuilderPageSidebarItem key="filling-group">
                <NewBuilderPageSidebarItemText>
                    Тип:&nbsp;<span>{filling?.group}</span>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>,
            <NewBuilderPageSidebarItem key="filling-thickness">
                <NewBuilderPageSidebarItemText>
                    Толщина:&nbsp;<span>{filling?.thickness} мм</span>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>,
            <NewBuilderPageSidebarItem key="filling-camernost">
                <NewBuilderPageSidebarItemText>
                    Камерность:&nbsp;<span>{filling?.camernost}-камерные</span>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>,
            <FillingMarkingSidebarItem key="filling-marking" />,
            <FillingGlassColorSidebarItem key="filling-color" />,
            <UserParametersFragment key="user-parameters" />,
            <FrameSizeInfoFragment key="frame-size" />
        )
        if (filling?.isMultifunctional) {
            views.push(
                <NewBuilderPageSidebarItem key="filling-is-multifunctional">
                    <NewBuilderPageSidebarItemText>Мультифункционалльное</NewBuilderPageSidebarItemText>
                </NewBuilderPageSidebarItem>
            )
        }
        if (filling?.isLowEmission) {
            views.push(
                <NewBuilderPageSidebarItem key="filling-is-low-emission">
                    <NewBuilderPageSidebarItemText>Низкоэмиссионное</NewBuilderPageSidebarItemText>
                </NewBuilderPageSidebarItem>
            )
        }
    } else if (currentProduct) {
        if (currentConstruction && currentConstruction.energyProductTypeName) {
            views.push(
                <NewBuilderPageSidebarItem key="energy">
                    <NewBuilderPageSidebarItemText>Комплектация: {currentConstruction.energyProductTypeName}</NewBuilderPageSidebarItemText>
                </NewBuilderPageSidebarItem>
            )
        }
        if (currentConstruction.products[0] === currentProduct) {
            views.push(
                <NewBuilderPageSidebarItem key="type-constructions">
                    <NewBuilderPageSidebarItemText>Тип конструкции: {typeConstructionFunction}</NewBuilderPageSidebarItemText>
                </NewBuilderPageSidebarItem>
            )
        }
        views.push(
            <NewBuilderPageSidebarItem key="profile">
                <NewBuilderPageSidebarItemText dataId="sidebar-profile">
                    Профиль:&nbsp;
                    <Button onClick={() => setConfigContext(ConfigContexts.Profile)}>
                        {currentProduct.profile.displayName ? currentProduct.profile.displayName : currentProduct.profile.name}
                    </Button>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>,
            <NewBuilderPageSidebarItem key="furniture">
                <NewBuilderPageSidebarItemText dataId="sidebar-furniture">
                    Фурнитура:&nbsp;
                    <Button onClick={() => setConfigContext(ConfigContexts.Furniture)}>{currentProduct.furniture.displayName}</Button>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>
        )
        if (!(partner?.factoryId === FactoryTypes.Perm && currentProduct.profile.name === 'Rehau Blitz New') && !currentProduct.isAluminium) {
            views.push(
                <LaminationSidebarItem
                    key="lamination"
                    construction={currentConstruction}
                    outsideLamination={currentOutsideLamination}
                    insideLamination={currentInsideLamination}
                />
            )
        }
        views.push(<UserParametersFragment key="user-parameters" />)
    }

    return (
        <>
            {views}
            {currentConstruction && currentConstruction.allConnectors.length > 0 && (
                <NewBuilderPageSidebarItem>
                    <NewBuilderPageSidebarItemText>
                        Соединители:&nbsp;
                        <Button
                            onClick={() => dispatch(setBuilderMode(builderMode === BuilderModes.Connector ? null : BuilderModes.Connector))}>
                            {currentConstruction.allConnectors.length} шт.
                        </Button>
                    </NewBuilderPageSidebarItemText>
                </NewBuilderPageSidebarItem>
            )}
            {currentOrder && currentConstruction?.needAlignDoorImpost && !currentContext && (
                <NewBuilderPageSidebarItem border="none">
                    <Button
                        className={styles.button}
                        isDisabled={currentOrder.readonly}
                        buttonStyle="with-border"
                        onClick={() => {
                            const order = currentOrder.clone
                            const construction = order.constructions.find(c => c.id === currentConstruction.id)

                            if (!construction) {
                                return
                            }

                            construction.alignDoorImpost()

                            order.addAction('Выровняли импост балконной двери', construction)
                            updateCurrentOrder(order)
                        }}>
                        Выровнять импост в б/б
                    </Button>
                </NewBuilderPageSidebarItem>
            )}
            {currentOrder && currentConstruction?.needAlignFillingsByLightAperture && !currentContext && (
                <NewBuilderPageSidebarItem border={isMobile ? 'short' : 'none'}>
                    <Button
                        className={styles.button}
                        isDisabled={currentOrder.readonly}
                        buttonStyle="with-border"
                        onClick={() => {
                            const order = currentOrder.clone
                            const construction = order.constructions.find(c => c.id === currentConstruction.id)

                            if (!construction) {
                                return
                            }

                            construction.alignFillingsByLightAperture()

                            order.addAction('Выровняли заполнения по световому проему', construction)
                            updateCurrentOrder(order)
                        }}>
                        Выровнять по световому проему
                    </Button>
                </NewBuilderPageSidebarItem>
            )}
            {isMobile && (
                <NewBuilderPageSidebarItem>
                    <ConstructionGoodsAndServices order={currentOrder} construction={currentConstruction} shouldShowRealCost={false} />
                </NewBuilderPageSidebarItem>
            )}
            <NewBuilderPageSidebarItem className={styles.weightSquareItem}>
                <NewBuilderPageSidebarItemText>
                    Вес:&nbsp;
                    <ArrowTooltip title="Вес одной конструкции" placement="top">
                        <span style={{outline: 'none', marginRight: '20px'}}>{currentConstruction.weight} кг</span>
                    </ArrowTooltip>
                    Площадь:&nbsp;
                    <ArrowTooltip title="Площадь одной конструкции" placement="top">
                        <span style={{outline: 'none'}}>
                            {currentConstruction.square} м<sup>2</sup>
                        </span>
                    </ArrowTooltip>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>
            {!isRestrictedMode && (
                <NewBuilderPageSidebarItem className={styles.quantityWrapper}>
                    <NewBuilderPageSidebarItemText>
                        Количество:&nbsp;
                        <ArrowTooltip title={`Количество конструкций может быть от 1 до ${MAX_CONSTRUCTION_COUNT} шт.`} placement="top">
                            <div style={{outline: 'none'}}>
                                <QuantityEditor
                                    className={styles.quantityEditor}
                                    quantity={currentConstruction ? currentConstruction.quantity : 0}
                                    min={1}
                                    max={MAX_CONSTRUCTION_COUNT}
                                    onChange={value => {
                                        if (!currentOrder || !currentConstruction) {
                                            return
                                        }
                                        const order = currentOrder.clone
                                        const construction = order.findConstructionById(currentConstruction.id)
                                        if (construction) {
                                            construction.quantity = value
                                            order.addAction(`Задали количество конструкций ${value}`, construction)
                                            updateCurrentOrder(order)
                                        }
                                    }}
                                />
                            </div>
                        </ArrowTooltip>
                        &nbsp;шт.
                    </NewBuilderPageSidebarItemText>
                </NewBuilderPageSidebarItem>
            )}
        </>
    )
}
