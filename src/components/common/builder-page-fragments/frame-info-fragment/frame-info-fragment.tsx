import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {useRemoveProduct, useSetProductOffset} from '../../../../hooks/builder'
import {FrameFilling} from '../../../../model/framer/frame-filling'
import {
    NewBuilderPageSidebarItem,
    NewBuilderPageSidebarItemText
} from '../../../desktop/new-builder-page-sidebar/new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {InputNumber} from '../../input-number/input-number'
import {RemoveSidebarItem} from '../../../desktop/new-builder-page-sidebar/remove-sidebar-item/remove-sidebar-item'
import {UserParametersFragment} from '../user-parameters-fragment/user-parameters-fragment'
import productOffsetIcon from './resources/product_offset_icon.svg'
import styles from './frame-info-fragment.module.css'

export function FrameInfoFragment(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const removeProduct = useRemoveProduct(currentOrder, currentConstruction, currentProduct)
    const setProductOffset = useSetProductOffset(currentOrder, currentConstruction, currentProduct)

    if (!currentOrder || !currentConstruction || !currentProduct || !(currentContext instanceof FrameFilling)) {
        return null
    }

    const connector = currentConstruction.allConnectors.find(c => c.connectedProductGuid === currentProduct.productGuid)

    if (!connector) {
        return null
    }

    const hostProduct = currentConstruction.products.find(p => p.connectors.includes(connector))

    if (!hostProduct) {
        return null
    }

    const productOffset = currentConstruction.getProductsRelativeOffset(hostProduct, currentProduct)

    return (
        <>
            <NewBuilderPageSidebarItem>
                <NewBuilderPageSidebarItemText>
                    Переместить:
                    <img
                        src={productOffsetIcon}
                        alt=""
                        style={{
                            marginLeft: '8px',
                            marginRight: '8px',
                            transform: `rotate(${connector.isVertical ? 0 : -90}deg)`
                        }}
                        draggable={false}
                    />
                    <InputNumber
                        className={styles.sizeInput}
                        style={{marginRight: '8px'}}
                        value={productOffset}
                        onKeyPressEnter={setProductOffset}
                    />
                    <span>мм</span>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>
            <UserParametersFragment />
            <RemoveSidebarItem onClick={removeProduct} />
        </>
    )
}
