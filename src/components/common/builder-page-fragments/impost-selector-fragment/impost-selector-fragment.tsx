import * as React from 'react'
import {ImpostTypes} from '../../../../model/impost-types'
import {SelectorWithIcon} from '../../selector-with-icon/selector-with-icon'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {useAddImpost} from '../../../../hooks/builder'
import {Product} from '../../../../model/framer/product'
import verticalImpostSmall from './resources/vertical_impost_icon.svg'
import horizontalImpostSmall from './resources/horizontal_impost_icon.svg'
import {
    NewBuilderPageSidebarItem,
    NewBuilderPageSidebarItemText
} from '../../../desktop/new-builder-page-sidebar/new-builder-page-sidebar-item/new-builder-page-sidebar-item'

interface IImpostSelectorTexts {
    verticalText: string
    horizontalText: string
    typeName: string
    typeNameSecondForm: string
}

function getImpostSelectorTexts(product: Product): IImpostSelectorTexts {
    if (product.isMoskitka) {
        return {
            verticalText: 'Вертикальная',
            horizontalText: 'Горизонтальная',
            typeName: 'Перемычка',
            typeNameSecondForm: 'Перемычку'
        }
    }

    return {
        verticalText: 'Вертикальный',
        horizontalText: 'Горизонтальный',
        typeName: 'Импост',
        typeNameSecondForm: 'Импост'
    }
}

export function ImpostSelectorFragment(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const addImpost = useAddImpost(currentOrder, currentProduct, currentContext)

    if (!currentOrder || !currentProduct || currentProduct.isEmpty || currentProduct.isAluminium) {
        return null
    }

    const texts = getImpostSelectorTexts(currentProduct)

    return (
        <NewBuilderPageSidebarItem>
            <NewBuilderPageSidebarItemText style={{marginBottom: '8px'}}>
                Добавить {texts.typeNameSecondForm.toLowerCase()}:
            </NewBuilderPageSidebarItemText>
            <SelectorWithIcon
                icons={[
                    {
                        src: horizontalImpostSmall,
                        hint: `${texts.horizontalText} ${texts.typeName.toLowerCase()}`,
                        onClick: () => addImpost(ImpostTypes.Horizontal)
                    },
                    {
                        src: verticalImpostSmall,
                        hint: `${texts.verticalText} ${texts.typeName.toLowerCase()}`,
                        onClick: () => addImpost(ImpostTypes.Vertical)
                    }
                ]}
            />
        </NewBuilderPageSidebarItem>
    )
}
