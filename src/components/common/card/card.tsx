import * as React from 'react'
import {CSSProperties, ReactNode, useRef} from 'react'
import styles from './card.module.css'

interface ICardProps {
    className?: string
    style?: CSSProperties
    children?: ReactNode
    dataId?: string
    onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
    onTransitionEnd?: (e: React.TransitionEvent<HTMLDivElement>) => void
}

export function Card(props: ICardProps): JSX.Element {
    const {className, style, children, dataId, onClick, onTransitionEnd} = props

    const containerRef = useRef<HTMLDivElement | null>(null)

    return (
        <div
            ref={containerRef}
            className={`${styles.card} ${className ?? ''}`}
            style={style}
            data-id={dataId}
            onClick={onClick}
            onTransitionEnd={onTransitionEnd}>
            {children}
        </div>
    )
}
