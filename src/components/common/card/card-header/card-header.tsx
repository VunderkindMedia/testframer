import * as React from 'react'
import {Hint} from '../../hint/hint'
import {getClassNames} from '../../../../helpers/css'
import {useAppContext} from '../../../../hooks/app'
import styles from './card-header.module.css'

interface ICardHeaderProps {
    title: string
    hint?: string
    className?: string
    hasNavigation?: boolean
    onClick?: () => void
}

export function CardHeader(props: ICardHeaderProps): JSX.Element {
    const {title, hint, className, hasNavigation = false, onClick} = props

    const {isMobile} = useAppContext()

    return (
        <div
            className={getClassNames(`${styles.header} ${className ?? ''}`, {
                [styles.clickable]: Boolean(onClick),
                [styles.navigation]: hasNavigation,
                [styles.mobile]: isMobile
            })}
            onClick={onClick}>
            <p className={styles.title}>{title}</p>
            {hint && <Hint text={hint} />}
        </div>
    )
}
