import * as React from 'react'
import {SelectorWithIcon} from '../selector-with-icon/selector-with-icon'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useUpdateCurrentOrder} from '../../../hooks/orders'
import icon1 from './resources/icon_1.svg'
import icon2 from './resources/icon_2.svg'

export function MoskitkaTypeSelector(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)

    const updateCurrentOrder = useUpdateCurrentOrder()

    if (!currentOrder || !currentConstruction || !currentProduct) {
        return null
    }

    const leaf = currentProduct.frame.innerFillings[0].leaf

    if (!leaf) {
        return null
    }

    const {moskitka} = leaf

    if (!moskitka) {
        return null
    }

    const select = (catproof: boolean): void => {
        const newMoskitka = moskitka.clone
        newMoskitka.catproof = catproof

        const order = currentOrder.clone
        const construction = order.findConstructionById(currentConstruction.id)
        const product = order.findProductByProductGuid(currentProduct.productGuid)
        if (construction && product && product.frame.innerFillings[0].leaf?.moskitka) {
            product.frame.innerFillings[0].leaf.moskitka = newMoskitka

            order.addAction(`Задали тип москитки ${catproof ? 'антикошка' : 'обычная'}`, construction, product)
            updateCurrentOrder(order)
        }
    }

    return (
        <SelectorWithIcon
            icons={[
                {
                    src: icon1,
                    hint: 'Обычная',
                    selected: !moskitka.catproof,
                    onClick: () => select(false)
                },
                {
                    src: icon2,
                    hint: 'Антикошка',
                    selected: moskitka.catproof,
                    onClick: () => select(true)
                }
            ]}
        />
    )
}
