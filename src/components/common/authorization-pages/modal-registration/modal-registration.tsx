import * as React from 'react'
import {ModalOverlay} from '../../modal/modal-overlay/modal-overlay'
import {CloseButton} from '../../button/close-button/close-button'
import {useAppContext} from '../../../../hooks/app'
import {RegistrationForm} from '../registration-form/registration-form'
import styles from './modal-registration.module.css'

interface IModalRegistrationProps {
    onClose: () => void
}

export const ModalRegistration = (props: IModalRegistrationProps): JSX.Element => {
    const {onClose} = props

    const {isMobile} = useAppContext()

    return (
        <ModalOverlay isFullMode={true} className={`${styles.modal} ${isMobile ? styles.mobile : ''}`}>
            <div className={styles.container}>
                <p className={styles.description}>
                    Для продолжения работы необходимо зарегистрироваться. К сожалению, возможности демо-доступа ограничены
                </p>
                <p className={styles.title}>Регистрация</p>
                <RegistrationForm isModal={true} />
                <CloseButton className={styles.closeButton} onClick={onClose} />
            </div>
        </ModalOverlay>
    )
}
