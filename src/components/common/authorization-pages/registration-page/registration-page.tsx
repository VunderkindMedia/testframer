import * as React from 'react'
import {Link} from '../../link/link'
import {PromoLayout} from '../promo-layout/promo-layout'
import {NAV_ROOT} from '../../../../constants/navigation'
import {useAppContext} from '../../../../hooks/app'
import {RegistrationForm} from '../registration-form/registration-form'
import styles from './registration-page.module.css'

export function RegistrationPage(): JSX.Element {
    const {isMobile} = useAppContext()

    return (
        <PromoLayout>
            <p className={`${styles.title} ${isMobile && styles.mobile}`}>Регистрация</p>
            <RegistrationForm className={styles.form} />
            <p className={styles.login}>
                Уже есть аккаунт? <Link title="Войти" path={NAV_ROOT} />
            </p>
        </PromoLayout>
    )
}
