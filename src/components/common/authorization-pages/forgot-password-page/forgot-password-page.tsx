import * as React from 'react'
import {useCallback, useRef} from 'react'
import {useDispatch} from 'react-redux'
import {AuthorizationLayout} from '../authorization-layout/authorization-layout'
import {Input} from '../../input/input'
import {Button} from '../../button/button'
import {Link} from '../../link/link'
import {NAV_ROOT} from '../../../../constants/navigation'
import {forgotPassword} from '../../../../redux/account/actions'
import {useAppContext} from '../../../../hooks/app'
import styles from './forgot-password-page.module.css'

export function ForgotPasswordPage(): JSX.Element {
    const emailInputRef = useRef<HTMLInputElement>(null)
    const dispatch = useDispatch()

    const onSubmitForm = useCallback(
        (e: React.SyntheticEvent<HTMLFormElement>) => {
            e.preventDefault()

            dispatch(forgotPassword(emailInputRef.current?.value ?? ''))
        },
        [dispatch]
    )

    const {isMobile} = useAppContext()

    return (
        <AuthorizationLayout>
            <p className={`${styles.title} ${isMobile && styles.mobile}`}>
                Для сброса пароля введите свою эл. почту, которая была привязана к аккаунту
            </p>
            <form className={`${styles.form} ${isMobile && styles.mobile}`} onSubmit={onSubmitForm}>
                <Input ref={emailInputRef} type="email" className={styles.input} isRequired={true} placeholder="Эл. почта" />
                <Button type="submit" className={styles.button} buttonStyle="with-fill">
                    Далее
                </Button>
            </form>
            <Link title="Вернуться назад" path={NAV_ROOT} className={styles.backLink} />
        </AuthorizationLayout>
    )
}
