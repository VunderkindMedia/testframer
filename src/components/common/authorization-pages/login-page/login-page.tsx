import * as React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {login} from '../../../../redux/account/actions'
import {useCallback, useRef, useState} from 'react'
import {Input} from '../../input/input'
import {Button} from '../../button/button'
import {Link} from '../../link/link'
import {NAV_FORGOT_PASSWORD, NAV_REGISTRATION} from '../../../../constants/navigation'
import {useAppContext} from '../../../../hooks/app'
import {PromoLayout} from '../promo-layout/promo-layout'
import {Select} from '../../select/select'
import {DEMO_ACCESS, DEMO_FACTORIES} from '../../../../constants/demo'
import {ListItem} from '../../../../model/list-item'
import {FactoryTypes} from '../../../../model/framer/factory-types'
import {IAppState} from '../../../../redux/root-reducer'
import styles from './login-page.module.css'

export function LoginPage(): JSX.Element {
    const demoAccount = useSelector((state: IAppState) => new URLSearchParams(state.router.location.search).get('demo_account'))
    const [isDemoAccess, setIsDemoAccess] = useState(false)
    const [selectedFactory, setSelectedFactory] = useState<ListItem<FactoryTypes> | null>(null)

    const loginInputRef = useRef<HTMLInputElement>(null)
    const passwordInputRef = useRef<HTMLInputElement>(null)

    const dispatch = useDispatch()
    const loginAction = useCallback(
        (username: string, password: string, isDemo: boolean) => dispatch(login(username, password, isDemo)),
        [dispatch]
    )

    const signIn = useCallback(
        (e: React.FormEvent<HTMLFormElement>) => {
            e.preventDefault()
            if (loginInputRef.current && passwordInputRef.current) {
                loginAction(loginInputRef.current.value, passwordInputRef.current.value, false)
            }
        },
        [loginAction]
    )

    const demoSignIn = useCallback(
        (factory: ListItem<FactoryTypes>) => {
            setSelectedFactory(factory)
            const {login, password} = DEMO_ACCESS[factory.data]
            loginAction(login, password, true)
        },
        [loginAction]
    )

    const {isMobile} = useAppContext()

    if (demoAccount) {
        const demoUser = Object.values(DEMO_ACCESS).find(v => v.login === demoAccount)
        demoUser && loginAction(demoUser.login, demoUser.password, true)
    }

    return (
        <PromoLayout className={`${isMobile && styles.mobile}`}>
            {isDemoAccess ? (
                <>
                    <p className={styles.title}>Демо доступ</p>
                    <Select
                        className={styles.select}
                        items={DEMO_FACTORIES}
                        onSelect={item => demoSignIn(item)}
                        selectedItem={selectedFactory ?? undefined}
                        placeholder="Завод"
                    />
                </>
            ) : (
                <>
                    <p className={styles.title}>Вход в систему</p>
                    <form className={styles.form} data-test="login-form" onSubmit={signIn}>
                        <Input
                            ref={loginInputRef}
                            type="text"
                            dataTest="email-input"
                            className={styles.emailInput}
                            isRequired={true}
                            defaultValue={''}
                            placeholder="Эл. почта"
                        />
                        <Input
                            ref={passwordInputRef}
                            type="password"
                            dataTest="password-input"
                            className={styles.passwordInput}
                            isRequired={true}
                            defaultValue={''}
                            placeholder="Пароль"
                        />
                        <Link title="Забыли пароль?" className={styles.resetPassword} path={NAV_FORGOT_PASSWORD} />
                        <Button type="submit" dataTest="submit-button" buttonStyle="with-fill" className={styles.submitButton}>
                            Войти
                        </Button>
                    </form>
                </>
            )}
            <p className={styles.registration}>
                Еще нет аккаунта? <Link title="Зарегистрироваться" dataTest="registration-link" path={NAV_REGISTRATION} />
            </p>
            {isDemoAccess ? (
                <Button onClick={() => setIsDemoAccess(false)} className={styles.demoLoginButton}>
                    Войти
                </Button>
            ) : (
                <Button onClick={() => setIsDemoAccess(true)} className={styles.demoLoginButton}>
                    Демо доступ
                </Button>
            )}
        </PromoLayout>
    )
}
