import * as React from 'react'
import {useState, useCallback} from 'react'
import {CustomInput} from '../../input/custom-input/custom-input'
import {VisibilityButton} from '../../button/visibility-button/visibility-button'
import styles from './password-input.module.css'

interface IPasswordInputProps {
    className?: string
    placeholder?: string
    dataTest?: string
    error?: null | string
    onChange?: (value: string) => void
    onBlur?: (value: string) => void
    shouldShowSwitchButton?: boolean
}

const PasswordInput = React.forwardRef<HTMLInputElement, IPasswordInputProps>((props: IPasswordInputProps, ref) => {
    const {className, placeholder, shouldShowSwitchButton = true, onChange, onBlur, dataTest, error} = props
    const [isPasswordVisible, setIsPasswordVisible] = useState(false)

    const onVisibilityButtonClick = useCallback(() => {
        setIsPasswordVisible(!isPasswordVisible)
    }, [isPasswordVisible])

    return (
        <div className={`${styles.passwordInput} ${className ?? ''}`}>
            <CustomInput
                ref={ref}
                type={isPasswordVisible ? 'text' : 'password'}
                maxlength={20}
                placeholder={placeholder ?? ''}
                dataTest={dataTest ?? ''}
                error={error}
                className={styles.input}
                onChange={onChange}
                onBlur={onBlur}
            />
            {shouldShowSwitchButton && (
                <VisibilityButton
                    className={styles.button}
                    isVisible={isPasswordVisible}
                    dataTest={dataTest ? `${dataTest}-visibility-button` : ''}
                    onClick={onVisibilityButtonClick}
                />
            )}
        </div>
    )
})

PasswordInput.displayName = 'PasswordInput'

export {PasswordInput}
