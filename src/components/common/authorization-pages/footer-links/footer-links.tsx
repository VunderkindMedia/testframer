import * as React from 'react'
import {Link} from '../../link/link'
import {useAppContext} from '../../../../hooks/app'
import styles from './footer-links.module.css'

export const getDocsDir = (kladrRegionId: string | null): string => {
    if (
        kladrRegionId &&
        (kladrRegionId === '59' ||
            kladrRegionId === '66' ||
            kladrRegionId === '18' ||
            kladrRegionId === '16' ||
            kladrRegionId === '43' ||
            kladrRegionId === '02' ||
            kladrRegionId === '72')
    ) {
        return '/s/docs/perm/'
    }
    return '/s/docs/elektrostal/'
}

interface IFooterLinks {
    selectedRegion: string | null
    className?: string
}

export function FooterLinks(props: IFooterLinks): JSX.Element {
    const {selectedRegion, className} = props

    const {isMobile} = useAppContext()

    return (
        <p className={`${styles.offer} ${isMobile ? styles.mobile : ''} ${className ?? ''}`}>
            <Link className={styles.link} href={`${getDocsDir(selectedRegion)}Оферта физ лица.pdf`} title="Договор оферты для физических лиц" />
            <Link className={styles.link} href={`${getDocsDir(selectedRegion)}Оферта юр лица.pdf`} title="Договор оферты для юридических лиц" />
        </p>
    )
}
