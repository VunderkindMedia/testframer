import * as React from 'react'
import {useCallback, useState, useEffect, useRef} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {CustomInput} from '../../input/custom-input/custom-input'
import {PasswordInput} from '../password-input/password-input'
import {Button} from '../../button/button'
import {Link} from '../../link/link'
import {Select} from '../../select/select'
import {Checkbox} from '../../checkbox/checkbox'
import {IAppState} from '../../../../redux/root-reducer'
import {ListItem} from '../../../../model/list-item'
import {User} from '../../../../model/framer/user'
import {Region} from '../../../../model/framer/region'
import {getDocsDir} from '../footer-links/footer-links'
import {loadRegions, register} from '../../../../redux/account/actions'
import {useAppContext} from '../../../../hooks/app'
import {useIsDemoUser} from '../../../../hooks/permissions'
import {FooterLinks} from '../footer-links/footer-links'
import {isValidEmail, isValidPassword} from '../../../../helpers/validation'
import {HINTS} from '../../../../constants/hints'
import styles from './registration-form.module.css'

interface IRegistrationFormProps {
    className?: string
    isModal?: boolean
}

export const RegistrationForm = (props: IRegistrationFormProps): JSX.Element => {
    const {className, isModal = false} = props
    const emailInputRef = useRef<HTMLInputElement>(null)
    const passwordInputRef = useRef<HTMLInputElement>(null)
    const passwordConfirmInputRef = useRef<HTMLInputElement>(null)

    const [selectedRegion, setSelectedRegion] = useState<Region | null>(null)
    const [isAcceptConditions, setIsAcceptConditions] = useState(false)
    const [emailError, setEmailError] = useState<string | null>(null)
    const [passwordError, setPasswordError] = useState<string | null>(null)
    const [passwordConfirmError, setPasswordConfirmError] = useState<string | null>(null)
    const [regionError, setRegionError] = useState<string | null>(null)
    const [hasCheckboxError, setHasCheckboxError] = useState(false)

    const regions = useSelector((state: IAppState) => state.account.regions)
    const dispatch = useDispatch()
    const isDemoUser = useIsDemoUser()

    const loadRegionsAction = useCallback(() => {
        dispatch(loadRegions)
    }, [dispatch])

    const validateEmail = useCallback((value: string): boolean => {
        let error = ''
        if (!value.length) {
            error = 'Укажите эл. почту'
        } else if (!isValidEmail(value)) {
            error = 'Некорректный адрес эл. почты'
        }
        setEmailError(error)
        return !error
    }, [])

    const validatePassword = useCallback((value: string): boolean => {
        const error = isValidPassword(value) ? '' : HINTS.passwordError
        setPasswordError(error)
        return !error
    }, [])

    const validateConfirmPassword = useCallback((value: string): boolean => {
        let error = ''
        if (isValidPassword(value)) {
            const password = passwordInputRef.current?.value?.trim() ?? ''
            const confirmPassword = passwordConfirmInputRef.current?.value?.trim() ?? ''
            error = password !== confirmPassword ? 'Пароли не совпадают' : ''
        } else {
            error = HINTS.passwordError
        }
        setPasswordConfirmError(error)
        return !error
    }, [])

    const validateRegion = useCallback((): boolean => {
        const error = selectedRegion ? '' : 'Выберите регион'
        setRegionError(error)
        return !error
    }, [selectedRegion])

    const onSubmitForm = useCallback(
        (e: React.SyntheticEvent) => {
            e.preventDefault()

            const email = emailInputRef.current?.value?.trim() ?? ''
            const password = passwordInputRef.current?.value?.trim() ?? ''
            const confirmPassword = passwordConfirmInputRef.current?.value?.trim() ?? ''

            const isValidEmail = validateEmail(email)
            const isValidPassword = validatePassword(password)
            const isValidConfirmPassword = validateConfirmPassword(confirmPassword)
            const isRegionValid = validateRegion()

            if (!isAcceptConditions) {
                setHasCheckboxError(true)
                return
            }

            const isFormValid = isValidEmail && isValidPassword && isValidConfirmPassword && isRegionValid
            if (isFormValid && selectedRegion) {
                const user = new User(email, selectedRegion.id, password, confirmPassword)
                dispatch(register(user, isDemoUser))
            }
        },
        [validateEmail, validatePassword, validateConfirmPassword, validateRegion, selectedRegion, isDemoUser, isAcceptConditions, dispatch]
    )

    const onSelectRegion = useCallback((item: ListItem<Region>) => {
        setSelectedRegion(item.data)
        setRegionError(item.data ? '' : 'Выберите регион')
    }, [])

    const onCheckConditions = useCallback((checked: boolean) => {
        setIsAcceptConditions(checked)
        setHasCheckboxError(!checked)
    }, [])

    useEffect(() => {
        loadRegionsAction()
    }, [loadRegionsAction])

    const {isMobile} = useAppContext()

    return (
        <form
            className={`${styles.form} ${isMobile && styles.mobile} ${isModal && styles.modal} ${className ?? ''}`}
            onSubmit={onSubmitForm}
            noValidate>
            <CustomInput
                ref={emailInputRef}
                type="email"
                className={styles.input}
                isRequired={true}
                placeholder="Эл. почта"
                error={emailError}
                onBlur={validateEmail}
            />
            <PasswordInput
                ref={passwordInputRef}
                placeholder="Придумайте пароль"
                className={styles.input}
                dataTest="password-input"
                error={passwordError}
                onBlur={validatePassword}
            />
            <PasswordInput
                ref={passwordConfirmInputRef}
                placeholder="Повторите пароль"
                className={styles.input}
                shouldShowSwitchButton={false}
                error={passwordConfirmError}
                onBlur={validateConfirmPassword}
            />
            <Select
                className={`${styles.select}`}
                items={regions.map(region => new ListItem(`${region.kladrRegionId}-${region.name}`, region.name, region))}
                onSelect={onSelectRegion}
                hasError={!!regionError}
                isValid={regionError === ''}
                placeholder="Регион"
                selectedItem={selectedRegion !== null ? new ListItem(selectedRegion.id.toString(), selectedRegion.name, null) : undefined}
                onClose={validateRegion}
            />
            {regionError && <p className={styles.formError}>{regionError}</p>}
            <Button type="submit" buttonStyle="with-fill" dataTest="registration-button" className={styles.button}>
                Зарегистрироваться
            </Button>
            <p className={styles.description}>Нажимая кнопку «Зарегистрироваться»:</p>
            <p className={styles.conditions}>
                <Checkbox isRequired={true} hasError={hasCheckboxError} className={styles.checkbox} onChange={onCheckConditions} />
                <span className={styles.conditionsText}>
                    Я даю своё согласие Фрамеру на обработку моей персональной информации на условиях, определенных{' '}
                    <Link
                        className={styles.link}
                        href={`${getDocsDir(selectedRegion?.kladrRegionId ?? null)}Политика конфиденциальности.pdf`}
                        title="Политикой конфиденциальности"
                    />
                    .
                </span>
            </p>
            {isModal && <FooterLinks className={styles.links} selectedRegion={null} />}
        </form>
    )
}
