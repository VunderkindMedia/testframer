import * as React from 'react'
import {useCallback, useEffect, useRef} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {AuthorizationLayout} from '../authorization-layout/authorization-layout'
import {Button} from '../../button/button'
import {PasswordInput} from '../password-input/password-input'
import {showStatusbar, hideStatusbar} from '../../../../redux/statusbar/actions'
import {resetPassword} from '../../../../redux/account/actions'
import {IAppState} from '../../../../redux/root-reducer'
import {useAppContext} from '../../../../hooks/app'
import {isValidPassword} from '../../../../helpers/validation'
import {HINTS} from '../../../../constants/hints'
import styles from './reset-password-page.module.css'

export function ResetPasswordPage(): JSX.Element {
    const passwordInputRef = useRef<HTMLInputElement>(null)
    const passwordConfirmInputRef = useRef<HTMLInputElement>(null)

    const search = useSelector((state: IAppState) => state.router.location.search)
    const dispatch = useDispatch()

    useEffect(() => {
        const resetCode = new URLSearchParams(search).get('c')
        if (!resetCode) {
            dispatch(showStatusbar('Неверная ссылка', true))
        }
        return () => {
            dispatch(hideStatusbar())
        }
    }, [dispatch, search])

    const onSubmitForm = useCallback(
        (e: React.FormEvent<HTMLFormElement>) => {
            e.preventDefault()

            const password = passwordInputRef.current?.value?.trim() ?? ''
            const confirmPassword = passwordConfirmInputRef.current?.value?.trim() ?? ''

            if (password !== confirmPassword) {
                dispatch(showStatusbar('Пароли не совпадают', true))
                return
            }

            if (!isValidPassword(password) || !isValidPassword(confirmPassword)) {
                dispatch(showStatusbar(HINTS.passwordError, true))
                return
            }

            const resetCode = new URLSearchParams(search).get('c')

            if (!resetCode) {
                dispatch(showStatusbar('Неверная ссылка', true))
                return
            }

            dispatch(hideStatusbar())
            dispatch(resetPassword(resetCode, password, confirmPassword))
        },
        [search, dispatch]
    )

    const {isMobile} = useAppContext()

    return (
        <AuthorizationLayout>
            <p className={`${styles.title} ${isMobile && styles.mobile}`}>Введите новый пароль</p>
            <form className={`${styles.form} ${isMobile && styles.mobile}`} onSubmit={onSubmitForm}>
                <PasswordInput ref={passwordInputRef} placeholder="Придумайте пароль" className={styles.passwordInput} />
                <PasswordInput
                    ref={passwordConfirmInputRef}
                    className={styles.repeatPasswordInput}
                    placeholder="Повторите пароль"
                    shouldShowSwitchButton={false}
                />
                <Button type="submit" className={styles.button} buttonStyle="with-fill">
                    Далее
                </Button>
            </form>
        </AuthorizationLayout>
    )
}
