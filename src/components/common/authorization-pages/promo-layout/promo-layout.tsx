import * as React from 'react'
import {useState, useEffect, useCallback} from 'react'
import logo from '../../../../resources/images/logo.svg'
import {useAppContext} from '../../../../hooks/app'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {FooterLinks} from '../footer-links/footer-links'
import {PromoSlider} from '../../promo-slider/promo-slider'
import styles from './promo-layout.module.css'

interface IPromoLayoutProps {
    children?: React.ReactNode
    className?: string
}

export function PromoLayout(props: IPromoLayoutProps): JSX.Element {
    const {children, className} = props
    const browserSize = useSelector((state: IAppState) => state.browser.windowSize)
    const {isMobile} = useAppContext()
    const [isAutoPlay, setIsAutoPlay] = useState(!isMobile)

    const scrollToSlider = (): void => {
        window.scrollTo({
            top: window.document.body.clientHeight,
            behavior: 'smooth'
        })
    }

    const isPortrait = isMobile && browserSize.height > browserSize.width

    const handleScrollEvent = useCallback(() => {
        if (window.scrollY === window.document.body.scrollHeight - browserSize.height) {
            setIsAutoPlay(true)
        }
    }, [browserSize.height])

    useEffect(() => {
        isMobile && setIsAutoPlay(false)
    }, [isMobile, browserSize.height])

    useEffect(() => {
        if (!isPortrait) {
            return
        }

        window.addEventListener('scroll', handleScrollEvent)

        return () => {
            window.removeEventListener('scroll', handleScrollEvent)
        }
    }, [isPortrait, handleScrollEvent])

    return (
        <div className={`${styles.layout} ${isMobile && styles.mobile} ${className ?? ''}`}>
            <div className={styles.content} style={{minHeight: `${browserSize.height}px`}}>
                <div className={styles.form}>
                    <img src={logo} alt="Framer" className={styles.logo} />
                    {children}
                </div>
                <div className={styles.footer}>
                    <FooterLinks selectedRegion={null} />
                    {isPortrait && <button className={styles.arrowButton} onClick={scrollToSlider} />}
                </div>
            </div>
            <PromoSlider isAutoPlay={isAutoPlay} className={styles.slider} style={{minHeight: `${browserSize.height}px`}} />
        </div>
    )
}
