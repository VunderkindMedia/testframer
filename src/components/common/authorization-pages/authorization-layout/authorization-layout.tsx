import * as React from 'react'
import logo from '../../../../resources/images/logo.svg'
import {useAppContext} from '../../../../hooks/app'
import styles from './authorization-layout.module.css'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'

interface IAuthorizationLayoutProps {
    children?: React.ReactNode
    footer?: React.ReactNode
}

export function AuthorizationLayout(props: IAuthorizationLayoutProps): JSX.Element {
    const {children, footer} = props
    const browserSize = useSelector((state: IAppState) => state.browser.windowSize)

    const {isMobile} = useAppContext()

    return (
        <div
            className={`${styles.authorizationLayout} ${isMobile && styles.mobile} ${!!footer && styles.withFooter}`}
            style={{minHeight: `${browserSize.height}px`}}>
            <div className={styles.content}>
                <img src={logo} alt="Framer" className={styles.logo} />
                {children}
            </div>
            {footer ? <div className={styles.footer}>{footer}</div> : ''}
        </div>
    )
}
