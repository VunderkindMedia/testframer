import * as React from 'react'
import {useEffect, useState} from 'react'
import {ModalModel} from '../modal/modal-model'
import {Modal} from '../modal/modal'
import {getOrderStateName} from '../../../model/framer/order-states'
import {Action} from '../../../model/action'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'

export function OrderReadonlyModal(): JSX.Element | null {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)

    const [isVisible, setIsVisible] = useState(false)

    useEffect(() => {
        const pageParams = new URLSearchParams(search)
        if (!pageParams.has('showTemplate') && currentOrder && currentOrder.readonly) {
            setIsVisible(true)
        }
    }, [currentOrder, search])

    if (!currentOrder || !isVisible) {
        return null
    }

    return (
        <Modal
            model={ModalModel.Builder.withTitle('Режим просмотра')
                .withBody(
                    <p>
                        Заказ №{currentOrder.fullOrderNumber} находится в статусе &quot;{getOrderStateName(currentOrder.state)}&quot; и поэтому
                        недоступен для редактирования.
                    </p>
                )
                .withPositiveAction(new Action('Закрыть', () => setIsVisible(false), 'close-order-view-mode-modal'))
                .build()}
        />
    )
}
