import * as React from 'react'
import styles from './order-errors.module.css'
import {Order} from '../../../model/framer/order'
import {ConstructionErrors} from '../construction-errors/construction-errors'
import {Construction} from '../../../model/framer/construction'

interface IOrderErrorsProps {
    order: Order
    currentConstruction?: Construction | null
    onSelectConstruction?: (construction: Construction) => void
}

export function OrderErrors(props: IOrderErrorsProps): JSX.Element {
    const {order, currentConstruction, onSelectConstruction} = props

    return (
        <div className={styles.orderErrors}>
            {order.constructions.map((construction, idx) => (
                <ConstructionErrors
                    key={construction.id}
                    className={styles.constructionErrors}
                    construction={construction}
                    constructionIndex={construction.position || idx + 1}
                    shouldShowGoToButton={construction.id !== currentConstruction?.id}
                    onSelectConstruction={onSelectConstruction}
                />
            ))}
        </div>
    )
}
