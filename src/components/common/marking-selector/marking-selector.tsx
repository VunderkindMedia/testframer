import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Beam} from '../../../model/framer/beam'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {useSetMarking} from '../../../hooks/builder'
import {getClassNames} from '../../../helpers/css'
import {useAppContext} from '../../../hooks/app'
import styles from './marking-selector.module.css'

export function MarkingSelector(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const setMarking = useSetMarking(currentOrder, currentContext)
    const {isMobile} = useAppContext()

    if (!currentOrder || !currentProduct) {
        return null
    }

    if (!(currentContext instanceof Beam || currentContext instanceof FrameFilling)) {
        return null
    }

    const beam =
        currentContext instanceof Beam ? currentContext : currentContext.frameBeams.find(b => b.nodeId !== currentContext.bottommostBeam.nodeId)

    if (!beam) {
        return null
    }

    const parts = currentProduct.getAvailablePartsByBeam(beam)

    return (
        <div className={`${styles.selector} ${isMobile ? styles.mobile : ''}`}>
            {parts.map(part => (
                <div
                    key={part.id}
                    onClick={() => setMarking(part)}
                    className={getClassNames(`${styles.item} blue-hover`, {selected: part.marking === beam.profileVendorCode})}>
                    <p className={styles.itemTitle}>{part.marking}</p>
                    <p className={styles.itemDescription}>{part.comment}</p>
                </div>
            ))}
        </div>
    )
}
