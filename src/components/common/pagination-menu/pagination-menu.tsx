import * as React from 'react'
import styles from './pagination-menu.module.css'
import {generateNumberArray} from '../../../helpers/number'

interface IPaginationMenuProps {
    currentPage: number
    itemsOnPage: number
    totalCount: number
    className?: string
    onSelect?: (page: number) => void
}

const MAX_VISIBLE_MENU_ITEMS = 7

export function PaginationMenu(props: IPaginationMenuProps): JSX.Element | null {
    const pagesCount = props.itemsOnPage ? Math.ceil(props.totalCount / props.itemsOnPage) : 0
    const {currentPage, className} = props

    let pagesIndexes = pagesCount > 0 ? generateNumberArray(pagesCount) : []

    let startIndex = 0

    if (pagesCount > MAX_VISIBLE_MENU_ITEMS && props.currentPage > 3) {
        startIndex = currentPage - 3
    }

    if (pagesCount > MAX_VISIBLE_MENU_ITEMS && currentPage > pagesCount - MAX_VISIBLE_MENU_ITEMS + 2) {
        startIndex = pagesCount - MAX_VISIBLE_MENU_ITEMS
    }

    pagesIndexes = pagesIndexes.slice(startIndex, startIndex + MAX_VISIBLE_MENU_ITEMS)

    const select = (index: number): void => {
        if (index < 0 || index === currentPage || index >= pagesCount) {
            return
        }
        props.onSelect && props.onSelect(index)
    }

    if (pagesCount <= 1) {
        return null
    }

    return (
        <div className={`${styles.menu} ${className}`}>
            <div className={styles.prevButton} onClick={() => select(props.currentPage - 1)} />
            {pagesIndexes.map(index => (
                <p key={index} onClick={() => select(index)} className={`${styles.item} ${props.currentPage === index && styles.selected} `}>
                    {index + 1}
                </p>
            ))}
            <div className={styles.nextButton} onClick={() => select(props.currentPage + 1)} />
        </div>
    )
}
