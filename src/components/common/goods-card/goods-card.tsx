import * as React from 'react'
import {useCallback} from 'react'
import styles from './goods-card.module.css'
import {Good} from '../../../model/framer/good'
import {GoodGroup} from '../../../model/framer/good-group'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ReactComponent as CacheIconSmall} from '../../../resources/images/cache-icon-small.inline.svg'
import {GoodSources} from '../../../model/framer/good-sources'
import {GoodsList} from './goods-list/goods-list'
import {HINTS} from '../../../constants/hints'
import {OrderParamsCard} from '../order-params-card/order-params-card'
import {getCurrency} from '../../../helpers/currency'
import {DEFAULT_MINIMUM_SELECT_ITEMS_WITHOUT_FILTER} from '../../../constants/settings'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'

interface IGoodsCardProps {
    title: string
    constructionId?: string
    allFactoryGoodGroups: GoodGroup[]
    allFactoryGoods: Good[]
    allPartnerGoodGroups: GoodGroup[]
    allPartnerGoods: Good[]
    goods: Good[]
    isDisabled?: boolean
    className?: string
    onClickHeader?: () => void
    onClickOutside?: (e: MouseEvent) => void
    onAddGood?: (good: Good) => void
    onChange?: (goods: Good[], action: string) => void
}

export function GoodsCard(props: IGoodsCardProps): JSX.Element | null {
    const {
        title,
        constructionId,
        allFactoryGoodGroups,
        allPartnerGoodGroups,
        goods,
        isDisabled,
        className,
        onClickHeader,
        onAddGood,
        onChange
    } = props

    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)

    const allFactoryGoods = isDisabled ? [] : props.allFactoryGoods
    const allPartnerGoods = isDisabled ? [] : props.allPartnerGoods

    const addGood = useCallback(
        (good: Good) => {
            good.constructionId = constructionId ?? null
            onAddGood && onAddGood(good)
        },
        [constructionId, onAddGood]
    )

    const updateGood = useCallback(
        (good: Good) => {
            const newGoods = goods.slice(0)
            const updatedGoodIndex = newGoods.findIndex(g => g.id === good.id)
            if (updatedGoodIndex !== -1) {
                newGoods[updatedGoodIndex] = good
                onChange && onChange(newGoods, `Обновили ${goods[updatedGoodIndex].toString()} до ${good.toString()}`)
            }
        },
        [goods, onChange]
    )

    const removeGood = useCallback(
        (good: Good) => {
            const newGoods = goods.slice(0)
            const removedGoodIndex = goods.indexOf(good)
            if (removedGoodIndex !== -1) {
                newGoods.splice(removedGoodIndex, 1)
                onChange && onChange(newGoods, `Удалили ${good.toString()}`)
            }
        },
        [goods, onChange]
    )

    const hasAvailablePartnerGoods = allPartnerGoods.length !== 0
    const partnerGoods = goods.filter(g => g.goodSource === GoodSources.Partner)

    const hasAvailableFactoryGoods = allFactoryGoods.length !== 0
    const factoryGoods = goods.filter(g => g.goodSource === GoodSources.Factory)

    const isVisiblePartnerGoods = hasAvailablePartnerGoods || partnerGoods.length > 0
    const isVisibleFactoryGoodsList = hasAvailableFactoryGoods || factoryGoods.length > 0

    if (!isVisiblePartnerGoods && !isVisibleFactoryGoodsList) {
        return null
    }

    return (
        <OrderParamsCard
            className={className}
            title={title}
            hint={HINTS.orderGoodsCardTitle}
            isDisabled={isDisabled}
            onClickHeader={onClickHeader}
            footer={
                (!constructionId && (
                    <p>
                        Общая стоимость:{' '}
                        <span>
                            {getCurrency(goods.reduce((s, g) => s + g.cost, 0))}{' '}
                            {currentOrder?.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                        </span>
                    </p>
                )) ||
                undefined
            }>
            <>
                {isVisiblePartnerGoods && (
                    <GoodsList
                        title="Свои материалы"
                        dataTest="order-page-partner-goods"
                        isDisabled={isDisabled}
                        shouldShowFilter={allPartnerGoods.length > DEFAULT_MINIMUM_SELECT_ITEMS_WITHOUT_FILTER}
                        goods={partnerGoods}
                        availableGoods={allPartnerGoods}
                        availableGoodGroups={allPartnerGoodGroups}
                        addGood={addGood}
                        updateGood={updateGood}
                        removeGood={removeGood}
                    />
                )}
                {isVisibleFactoryGoodsList && (
                    <GoodsList
                        title="Материалы завода"
                        dataTest="order-page-factory-goods"
                        rootGroupName="Группа"
                        className={isVisiblePartnerGoods ? styles.borderedGoods : ''}
                        isDisabled={isDisabled}
                        shouldShowFilter={true}
                        goods={factoryGoods}
                        availableGoods={allFactoryGoods}
                        availableGoodGroups={allFactoryGoodGroups}
                        addGood={addGood}
                        updateGood={updateGood}
                        removeGood={removeGood}
                    />
                )}
            </>
        </OrderParamsCard>
    )
}
