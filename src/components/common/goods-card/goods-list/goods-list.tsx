import * as React from 'react'
import styles from './goods-list.module.css'
import {GoodCard} from '../../good-card/good-card'
import {Good} from '../../../../model/framer/good'
import {GoodGroup} from '../../../../model/framer/good-group'
import {ListItem} from '../../../../model/list-item'
import {Select} from '../../select/select'
import {RUB_SYMBOL} from '../../../../constants/strings'
import {getCurrency} from '../../../../helpers/currency'
import {useAppContext} from '../../../../hooks/app'

interface IGoodsListProps {
    title: string
    rootGroupName?: string
    className?: string
    isDisabled?: boolean
    shouldShowFilter?: boolean
    goods: Good[]
    availableGoods: Good[]
    availableGoodGroups: GoodGroup[]
    addGood: (good: Good) => void
    updateGood: (good: Good) => void
    removeGood: (good: Good) => void
    dataTest?: string
}

const getTopLevelGoodGroup = (good: Good, goodGroups: GoodGroup[]): GoodGroup | null => {
    const goodGroup = goodGroups.find(g => g.id === good.goodGroupId)
    if (goodGroup) {
        const topLevelGroup = goodGroups.find(g => g.id === goodGroup.parentGroupId)
        return topLevelGroup ?? goodGroup
    }
    return null
}

const getAvailableGoods = (topLevelGroup: GoodGroup | null, goodsGroups: GoodGroup[], allGoods: Good[]): Good[] => {
    const goods: Good[] = []

    if (topLevelGroup) {
        const availableGroupIds = goodsGroups
            .filter(g => g.parentGroupId === topLevelGroup.id)
            .map(g => g.id)
            .concat(topLevelGroup.id)

        return allGoods.filter(good => availableGroupIds.includes(good.goodGroupId))
    }

    return goods
}

const getGoodListItem = (good: Good): ListItem<GoodGroup | Good> => {
    const listItem = new ListItem(`good-${good.goodId}`, good.name, good)
    listItem.additionalString = `, ${getCurrency(good.pricePerUnitToDisplay)} ${RUB_SYMBOL}`
    return listItem
}

const getGoodSelectorItems = (goodsGroups: GoodGroup[], allGoods: Good[]): ListItem<GoodGroup | Good>[] => {
    function getChildListItems(parent: GoodGroup): ListItem<GoodGroup | Good>[] {
        const children = goodsGroups
            .filter(g => g.parentGroupId === parent.id)
            .map(group => {
                const listItem = new ListItem<GoodGroup | Good>(`group-${group.id}`, group.name, group)
                listItem.listItems = getChildListItems(group)
                return listItem
            })

        if (children.length === 0) {
            return allGoods.filter(g => g.goodGroupId === parent.id).map(getGoodListItem)
        }

        return children
    }

    return goodsGroups
        .filter(group => group.parentGroupId === 0)
        .map(group => {
            const listItem = new ListItem<GoodGroup | Good>(`group-${group.id}`, group.name, group)
            listItem.listItems = getChildListItems(group)
            return listItem
        })
}

export function GoodsList(props: IGoodsListProps): JSX.Element {
    const {
        title,
        rootGroupName,
        className,
        isDisabled,
        shouldShowFilter,
        goods,
        availableGoods,
        availableGoodGroups,
        addGood,
        updateGood,
        removeGood,
        dataTest
    } = props

    const {isMobile} = useAppContext()
    const rootItems: ListItem<GoodGroup | Good | null>[] = []
    if (rootGroupName) {
        const listItem = new ListItem<GoodGroup | Good | null>('groups', rootGroupName, null)
        listItem.selectable = false
        listItem.alwaysExpanded = true
        listItem.listItems = getGoodSelectorItems(availableGoodGroups, availableGoods)
        listItem.parentsIds = []
        rootItems.push(listItem)
    } else {
        const listItems = availableGoods.map(getGoodListItem)
        rootItems.push(...listItems)
    }

    return (
        <div className={className}>
            <p className={styles.header}>{title}</p>
            <div className={styles.items}>
                {goods.map((good, idx) => {
                    const topLevelGoodGroup = getTopLevelGoodGroup(good, availableGoodGroups)
                    return (
                        <GoodCard
                            className={styles.item}
                            dataTest={`${dataTest}-good-card-${idx}`}
                            key={good.id}
                            isDisabled={isDisabled}
                            goodGroupName={topLevelGoodGroup?.name ?? 'Неизвестная группа'}
                            good={good}
                            availableGoods={getAvailableGoods(topLevelGoodGroup, availableGoodGroups, availableGoods)}
                            onChange={good => updateGood(good)}
                            onRemoveClick={() => removeGood(good)}
                        />
                    )
                })}
            </div>
            {availableGoods.length > 0 && (
                <Select
                    dataTest={`${dataTest}-select`}
                    shouldShowFilter={shouldShowFilter}
                    title="Добавить"
                    shouldShowAddButtonForEmptyFilterResult={true}
                    className={styles.addSelect}
                    isMultiline={isMobile}
                    onSelect={item => item.data instanceof Good && addGood(item.data)}
                    items={rootItems}
                />
            )}
        </div>
    )
}
