import * as React from 'react'
import {ReactNode} from 'react'
import {Button} from '../button/button'
import styles from './icon-control.module.css'

interface IIconButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
    buttonStyle?: 'with-border' | 'with-fill'
    children?: ReactNode
    dataTest?: string
    dataId?: string
}

const IconButton = React.forwardRef<HTMLButtonElement, IIconButtonProps>((props: IIconButtonProps, ref) => {
    const {children, onClick, className, isDisabled, buttonStyle, dataTest, dataId} = props

    return (
        <Button
            ref={ref}
            dataTest={dataTest}
            dataId={dataId}
            className={`${styles.button} ${buttonStyle && styles[buttonStyle]} ${className ?? ''}`}
            isDisabled={isDisabled}
            onClick={onClick}>
            <div className={styles.icon}>{children}</div>
        </Button>
    )
})

IconButton.displayName = 'IconButton'

export {IconButton}
