import * as React from 'react'
import {ReactNode} from 'react'
import styles from './icon-control.module.css'

interface IIconLinkProps {
    href?: string
    className?: string
    style?: 'with-border' | 'with-fill'
    children?: ReactNode
}

const IconLink = React.forwardRef<HTMLAnchorElement, IIconLinkProps>((props: IIconLinkProps, ref): JSX.Element => {
    const {href, children, className, style} = props

    return (
        <a ref={ref} className={`${styles.button} ${style && styles[style]} ${className ?? ''} ${!href ? styles.disabled : ''}`} href={href}>
            <div className={styles.icon}>{children}</div>
        </a>
    )
})

IconLink.displayName = 'IconLink'

export {IconLink}
