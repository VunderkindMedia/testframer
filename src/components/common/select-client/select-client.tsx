import * as React from 'react'
import {useState} from 'react'
import {Select} from '../select/select'
import {AddButton} from '../button/add-button/add-button'
import {Client} from '../../../model/framer/client'
import {Order} from '../../../model/framer/order'
import {useSetClientToOrder} from '../../../hooks/orders'
import {ListItem} from '../../../model/list-item'
import {useAppContext} from '../../../hooks/app'
import styles from './select-client.module.css'

interface ISelectClient {
    order: Order
    clients: Client[]
    className?: string
}

export function SelectClient(props: ISelectClient): JSX.Element {
    const {order, clients, className} = props
    const setClientToOrder = useSetClientToOrder()
    const [clientFilter, setClientFilter] = useState('')
    const {isTablet} = useAppContext()

    const clientsListItems = clients.map(c => new ListItem(`${c.id}`, c.name, c))
    const selectedClientItem = clientsListItems.find(i => i.data.id === order.client?.id) ?? new ListItem('-1', '—', null)

    return (
        <Select
            className={`${styles.select} ${isTablet ? styles.tablet : ''} ${className ?? ''}`}
            shouldShowFilter={true}
            useMaxWidth={true}
            emptyListElement={
                <div className={styles.addClient}>
                    <AddButton
                        title="Добавить"
                        onClick={() => {
                            const name = clientFilter.trim()
                            if (name.length < 2) {
                                return
                            }
                            const client = new Client()
                            client.name = name
                            setClientToOrder(order, client)
                        }}
                    />
                </div>
            }
            emptyListElementHeight={36}
            onChangeFilter={filter => {
                setClientFilter(filter)
            }}
            items={clientsListItems}
            selectedItem={selectedClientItem}
            onSelect={item => setClientToOrder(order, item.data)}
        />
    )
}
