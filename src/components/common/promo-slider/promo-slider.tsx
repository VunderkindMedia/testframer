import * as React from 'react'
import {useState, useRef, useEffect, useCallback} from 'react'
import Slider from 'react-slick'
import {StatisticsSlide} from './statistics-slide/statistics-slide'
import {TextSlide} from './text-slide/text-slide'
import {SliderDot} from './slider-dot/slider-dot'
import {useAppContext} from '../../../hooks/app'
import './slick-carousel.css'
import styles from './promo-slider.module.css'

interface IPromoSliderProps {
    isAutoPlay?: boolean
    className?: string
    style?: React.CSSProperties
}

export const PromoSlider = (props: IPromoSliderProps): JSX.Element => {
    const {className, style, isAutoPlay = true} = props
    const [currentSlide, setCurrentSlide] = useState(0)
    const slider = useRef<Slider | null>(null)
    const {isMobile} = useAppContext()

    const settings = {
        dots: false,
        infinite: true,
        autoplay: isAutoPlay,
        pauseOnHover: false,
        autoplaySpeed: 6000,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        className: styles.slider,
        arrows: false,
        beforeChange: (prev: number, next: number) => {
            slider.current && slider.current.slickPause()
            setCurrentSlide(next)
        },
        afterChange: () => {
            slider.current && slider.current.slickPlay()
        }
    }

    useEffect(() => {
        isAutoPlay && slider.current && slider.current.slickPlay()
    }, [isAutoPlay])

    const changeSlide = useCallback((index: number) => {
        slider.current && slider.current.slickGoTo(index)
    }, [])

    return (
        <div className={`${className} ${styles.slider} ${isMobile ? styles.mobile : ''}`} style={style}>
            <Slider ref={(ref: Slider) => (slider.current = ref)} {...settings}>
                <StatisticsSlide />
                <TextSlide
                    title={
                        <>
                            Рассчитать точную
                            <br /> стоимость конструкций
                        </>
                    }
                    className={styles.calculationSlide}
                />
                <TextSlide title="Формировать коммерческие предложения для Ваших клиентов" className={styles.kp} />
                <TextSlide title="Отправить заказы в производство и отслеживать их статус" className={styles.tracking} />
            </Slider>
            <div className={styles.dots}>
                {[...Array(4).keys()].map(index => {
                    return <SliderDot key={index} isActive={isAutoPlay && currentSlide === index} onClick={() => changeSlide(index)} />
                })}
            </div>
        </div>
    )
}
