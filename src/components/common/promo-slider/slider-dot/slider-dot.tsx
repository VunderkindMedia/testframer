import * as React from 'react'
import styles from './slider-dot.module.css'

interface ISlideDotProps {
    isActive: boolean
    onClick: () => void
}

export const SliderDot = (props: ISlideDotProps): JSX.Element => {
    const {isActive, onClick} = props

    return (
        <button className={`${styles.button} ${isActive && styles.active}`} onClick={onClick}>
            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="11" cy="11" r="10" className={styles.track}></circle>
                <circle cx="11" cy="11" r="10" className={styles.activeTrack}></circle>
                <circle cx="11" cy="11" r="6" fill="white" />
            </svg>
        </button>
    )
}
