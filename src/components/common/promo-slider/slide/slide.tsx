import * as React from 'react'
import {useAppContext} from '../../../../hooks/app'
import styles from './slide.module.css'

interface ISlide {
    children?: React.ReactNode
    className?: string
}

export const Slide = (props: ISlide): JSX.Element => {
    const {children, className} = props
    const {isMobile} = useAppContext()

    return <div className={`${styles.slide} ${isMobile ? styles.mobile : ''} ${className}`}>{children}</div>
}
