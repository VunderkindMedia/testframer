import * as React from 'react'
import {Slide} from '../slide/slide'
import amegaLogo from '../resources/amega-logo.svg'
import exprofLogo from '../resources/exprof-logo.png'
import kbeLogo from '../resources/kbe-logo.png'
import wintechLogo from '../resources/wintech-logo.png'
import plaswinLogo from '../resources/plaswin-logo.png'
import proplexLogo from '../resources/proplex-logo.png'
import rehauLogo from '../resources/rehau-logo.png'
import {useAppContext} from '../../../../hooks/app'
import styles from './statistics-slide.module.css'

export const StatisticsSlide = (): JSX.Element => {
    const {isMobile} = useAppContext()

    return (
        <Slide className={`${styles.slide} ${isMobile ? styles.mobile : ''}`}>
            <div className={styles.header}>
                <div className={styles.text}>
                    <p className={styles.welcome}>Добро пожаловать во Framer!</p>
                    <p className={styles.title}>
                        Сервис полного цикла, поможет построить, оплатить
                        <br /> и передать в производство окна и конструкции онлайн
                    </p>
                </div>
                {isMobile ? (
                    <img src={amegaLogo} className={styles.amegaLogo} width="100" height="43" alt="Amega" />
                ) : (
                    <img src={amegaLogo} className={styles.amegaLogo} width="143" height="61" alt="Amega" />
                )}
            </div>
            <div className={styles.statistics}>
                <div>
                    <p className={styles.value}>
                        24/<span className={styles.schedule}>7</span>
                    </p>
                    <p className={styles.description}>Посчитать, оплатить, оформить заказ</p>
                </div>
                <div>
                    <p className={styles.value}>
                        2 <span>завода</span>
                    </p>
                    <p className={styles.description}>
                        Электросталь
                        <br /> и Пермь
                    </p>
                </div>
                <div>
                    <p className={styles.value}>200+</p>
                    <p className={styles.description}>
                        Городов для доставки
                        <br /> в 11 регионах РФ
                    </p>
                </div>
                <div>
                    <p className={styles.value}>3 300</p>
                    <p className={styles.description}>
                        Изделий в сутки
                        <br /> на двух заводах
                    </p>
                </div>
                <div>
                    <p className={styles.value}>1 500+</p>
                    <p className={styles.description}>Пользователей уже попробовали Framer</p>
                </div>
                <div>
                    <p className={styles.value}>100+</p>
                    <p className={styles.description}>Пользователей оформили первый заказ</p>
                </div>
            </div>
            <ul className={styles.brandList}>
                <li>
                    <img className={styles.kbe} src={kbeLogo} width="68" height="27" alt="KBE" />
                </li>
                <li>
                    <img src={exprofLogo} width="92" height="27" alt="Exprof" />
                </li>
                <li>
                    <img src={rehauLogo} width="59" height="27" alt="Rehau" />
                </li>
                <li>
                    <img src={proplexLogo} width="89" height="23" alt="Proplex" />
                </li>
                <li>
                    <img src={wintechLogo} width="104" height="13" alt="Wintech" />
                </li>
                <li>
                    <img src={plaswinLogo} width="108" height="28" alt="Plaswin" />
                </li>
            </ul>
        </Slide>
    )
}
