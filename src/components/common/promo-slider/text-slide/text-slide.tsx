import * as React from 'react'
import {Slide} from '../slide/slide'
import {useAppContext} from '../../../../hooks/app'
import styles from './text-slide.module.css'

interface ITextSlide {
    title: React.ReactNode
    className?: string
}

export const TextSlide = (props: ITextSlide): JSX.Element => {
    const {title, className} = props
    const {isMobile} = useAppContext()

    return (
        <Slide className={`${styles.slide} ${isMobile ? styles.mobile : ''} ${className ?? ''}`}>
            <>
                <p className={styles.welcome}>Добро пожаловать во Framer!</p>
                <p>Здесь вы сможете</p>
                <p className={styles.title}>{title}</p>
            </>
        </Slide>
    )
}
