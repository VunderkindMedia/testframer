import * as React from 'react'
import {useState} from 'react'
import {RadioGroup} from '../radio-group/radio-group'
import {DiscountTypes} from '../../../model/framer/discount-types'
import {Order} from '../../../model/framer/order'
import {Discount} from '../../../model/framer/discount'
import {PriceInput} from '../price-input/price-input'
import {WithTitle} from '../with-title/with-title'
import {RUB_SYMBOL} from '../../../constants/strings'
import {HINTS} from '../../../constants/hints'
import {useAppContext} from '../../../hooks/app'
import {WithHint} from '../with-hint/with-hint'
import styles from './order-discount-editor.module.css'

interface IOrderDiscountEditorProps {
    order: Order
    onChangeDiscount: (discount: Discount) => void
}

export function OrderDiscountEditor(props: IOrderDiscountEditorProps): JSX.Element {
    const {order, onChangeDiscount} = props

    const [discountType, setDiscountType] = useState<DiscountTypes>(order.discount.discountType)
    const {isMobile} = useAppContext()

    return (
        <div className={isMobile ? styles.mobile : ''}>
            <WithHint className={styles.header} hint={HINTS.orderDiscount}>
                Скидка на заказ
            </WithHint>
            <RadioGroup
                className={styles.type}
                dataTest="order-discount-radio-group"
                items={['В рублях', 'В процентах']}
                isDisabled={order.readonly}
                selectedIndex={order.discount.discountType === DiscountTypes.Fixed ? 0 : 1}
                onSelect={index => {
                    setDiscountType(index === 0 ? DiscountTypes.Fixed : DiscountTypes.Percentage)
                }}
            />
            <WithTitle title={discountType === DiscountTypes.Fixed ? 'В рублях:' : 'В процентах:'}>
                <PriceInput
                    currency={discountType === DiscountTypes.Fixed ? RUB_SYMBOL : '%'}
                    isDisabled={order.readonly}
                    value={discountType === DiscountTypes.Fixed ? order.discount.fixed / 100 : order.discount.percent}
                    dataTest="order-discount-input"
                    onChange={value => {
                        const discount = new Discount()
                        if (discountType === DiscountTypes.Fixed) {
                            discount.fixed = value * 100
                        } else {
                            discount.percent = value
                        }
                        onChangeDiscount(discount)
                    }}
                />
            </WithTitle>
        </div>
    )
}
