import React from 'react'
import {ArrowTooltip} from '../arrow-tooltip/arrow-tooltip'
import styles from './energy-tooltip.module.css'

interface IEnergyTooltipProps {
    className?: string
}
export const EnergyTooltip = (props: IEnergyTooltipProps): JSX.Element => {
    const {className} = props

    return (
        <ArrowTooltip title="Здесь скоро появится информационный блок о программе Energy">
            <span className={`${styles.icon} ${className ?? ''}`} onClick={e => e.stopPropagation()}></span>
        </ArrowTooltip>
    )
}
