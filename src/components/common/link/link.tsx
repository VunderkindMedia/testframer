import * as React from 'react'
import {Link as RouterLink} from 'react-router-dom'
import styles from './link.module.css'

interface ILinkPropsInterface {
    title: string
    href?: string
    path?: string
    className?: string
    dataTest?: string
}

const Link = (props: ILinkPropsInterface): JSX.Element => {
    const {title, href, path, className, dataTest} = props

    return (
        <>
            {path ? (
                <RouterLink className={`${styles.link} ${className}`} to={path} data-test={dataTest}>
                    {title}
                </RouterLink>
            ) : (
                <a className={`${styles.link} ${className}`} href={href} data-test={dataTest} target="_blank" rel="noopener noreferrer">
                    {title}
                </a>
            )}
        </>
    )
}

export {Link}
