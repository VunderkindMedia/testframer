import * as React from 'react'
import styles from './construction-preview.module.css'

interface IConstructionQuantity {
    quantity: number
    children: JSX.Element
    isAutoWidth?: boolean
}

export function ConstructionPreview(props: IConstructionQuantity): JSX.Element {
    const {quantity, children, isAutoWidth} = props
    return (
        <div className={`${styles.preview} ${isAutoWidth ? styles.autoWidth : ''}`}>
            {children}
            {quantity > 1 && <div className={styles.quantity}>{quantity}</div>}
        </div>
    )
}
