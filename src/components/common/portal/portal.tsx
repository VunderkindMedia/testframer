import * as ReactDOM from 'react-dom'
import {ReactNode} from 'react'

interface IPortalProps {
    selector: string
    children?: ReactNode
}

export function Portal(props: IPortalProps): JSX.Element | null {
    const {selector, children} = props

    const element = document.querySelector(selector)
    if (!element) {
        return null
    }

    return ReactDOM.createPortal(children, element)
}
