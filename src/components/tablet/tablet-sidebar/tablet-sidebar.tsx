import * as React from 'react'
import styles from './tablet-sidebar.module.css'
import {ReactNode} from 'react'
import {ModalOverlay} from '../../common/modal/modal-overlay/modal-overlay'
import {BodyPortal} from '../../common/body-portal/body-portal'
import {Button} from '../../common/button/button'
import {ReactComponent as NextIcon} from './resources/next_icon.inline.svg'

interface ITabletSidebarProps {
    children?: ReactNode
}

interface ITabletSidebarState {
    visible: boolean
    opened: boolean
}

export class TabletSidebar extends React.Component<ITabletSidebarProps, ITabletSidebarState> {
    private _containerRef = React.createRef<HTMLDivElement>()

    constructor(props: ITabletSidebarProps) {
        super(props)

        this.state = {
            visible: false,
            opened: false
        }
    }

    show(): void {
        this.setState({visible: true})
    }

    hide(): void {
        this.setState({visible: false})
    }

    private _onClickHandler = (e: MouseEvent): void => {
        if (!this.state.opened) {
            return
        }
        const target = e.target as HTMLElement
        if (target.closest('*[class*="wrap"]')) {
            return
        }
        if (!this._containerRef.current?.contains(target)) {
            this.setState({visible: false})
        }
    }

    componentDidMount(): void {
        window.addEventListener('click', this._onClickHandler)
    }

    componentWillUnmount(): void {
        window.removeEventListener('click', this._onClickHandler)
    }

    get isVisible(): boolean {
        return this.state.visible
    }

    render(): JSX.Element {
        return (
            <>
                {this.state.visible && <ModalOverlay />}
                <BodyPortal>
                    <div
                        className={`${styles.sidebar} ${this.state.visible && styles.visible}`}
                        ref={this._containerRef}
                        onTransitionEnd={() => this.setState({opened: this.state.visible})}>
                        {this.props.children}
                        <div className={styles.buttonWrap}>
                            <Button className={styles.closeButton} onClick={() => this.setState({visible: false})}>
                                <div className={styles.title}>
                                    <NextIcon className={styles.icon} />
                                    Скрыть
                                </div>
                            </Button>
                        </div>
                    </div>
                </BodyPortal>
            </>
        )
    }
}
