import * as React from 'react'
import {WithTitle} from '../../common/with-title/with-title'
import {DropdownMenu} from '../../common/dropdown-menu/dropdown-menu'
import {ListItem, ListItemBody, ListItemHeader} from '../list-item'
import {Action} from '../../../model/action'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {Mounting} from '../../../model/framer/mounting'
import {getOrderStateName} from '../../../model/framer/order-states'
import styles from './mounting-list-item.module.css'

interface IMountingListItemProps {
    mounting: Mounting
    onEdit: (id: number) => void
    onDelete: (mounting: Mounting) => void
    onDownload: (mounting: Mounting) => void
}

export const MountingListItem = (props: IMountingListItemProps): JSX.Element => {
    const {mounting, onEdit, onDelete, onDownload} = props
    const {id, order, datetime, createdAt, client, owner} = mounting

    const editAction = new Action('Редактировать', () => {
        onEdit(id)
    })

    const items = [
        editAction,
        new Action('Удалить', () => {
            onDelete(mounting)
        }),
        new Action('Скачать', () => {
            onDownload(mounting)
        })
    ]

    return (
        <div>
            <ListItem state={order.state} onClick={editAction.func}>
                <ListItemHeader>
                    <p className={styles.orderNumber}>{order.fullOrderNumber}</p>
                    <p className={styles.orderState}>{getOrderStateName(order.state)}</p>
                    <DropdownMenu items={items} />
                </ListItemHeader>
                <ListItemBody>
                    <div className={styles.bodyRow}>
                        <WithTitle title="Дата монтажа">
                            <p className={styles.date}>{getDateDDMMYYYYFormat(new Date(datetime))}</p>
                        </WithTitle>
                    </div>
                    <div className={`${styles.bodyRow} ${styles.datesRow}`}>
                        <WithTitle title="Дата создания">
                            <p className={styles.date}>{getDateDDMMYYYYFormat(new Date(createdAt))}</p>
                        </WithTitle>
                        <WithTitle title="Дата отгрузки">
                            <p className={styles.date}>
                                {order.shippingDate.length > 0 ? (
                                    getDateDDMMYYYYFormat(new Date(order.shippingDate))
                                ) : order.estShippingDate.length === 0 ? (
                                    <>&mdash;</>
                                ) : (
                                    <> ~ {getDateDDMMYYYYFormat(new Date(order.estShippingDate))}</>
                                )}
                            </p>
                        </WithTitle>
                    </div>
                    <div className={styles.bodyRow}>
                        <WithTitle title="Менеджер">
                            <p className={styles.name}>{owner?.name || owner?.email}</p>
                        </WithTitle>
                    </div>
                    <div className={styles.bodyRow}>
                        <WithTitle title="Клиент">
                            <p className={styles.name}>{client?.name ?? <>&mdash;</>}</p>
                        </WithTitle>
                    </div>
                </ListItemBody>
            </ListItem>
        </div>
    )
}
