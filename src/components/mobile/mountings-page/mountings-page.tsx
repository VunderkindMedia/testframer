import * as React from 'react'
import {useCallback} from 'react'
import {useSelector} from 'react-redux'
import {PaginationMenu} from '../../common/pagination-menu/pagination-menu'
import {MountingListItem} from '../mounting-list-item/mounting-list-item'
import {getMountingsFilterParams, getMountingsFilter, getMountingLink} from '../../../helpers/mountings'
import {IAppState} from '../../../redux/root-reducer'
import {useGoTo} from '../../../hooks/router'
import {NAV_MOUNTINGS} from '../../../constants/navigation'
import {HINTS} from '../../../constants/hints'
import {HeaderTitle} from '../header/header-title'
import {MountingsFilter} from '../mountings-filter/mountings-filter'
import {useDeleteMountingModal, useSetMountingsFilters, useMountingsFromSearch, useDownloadMountingsReport} from '../../../hooks/mountings'
import {Modal} from '../../common/modal/modal'
import {Mounting} from '../../../model/framer/mounting'
import {useResetScrollOnSearchChange} from '../../../hooks/ui'
import {usePermissions} from '../../../hooks/permissions'
import {ConfirmDealerLink} from '../../common/confirm-dealer-link/confirm-dealer-link'
import styles from './mounting-page.module.css'

export const MountingsPage = (): JSX.Element => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const mountingsTotalCount = useSelector((state: IAppState) => state.mountings.totalCount)
    const requestCount = useSelector((state: IAppState) => state.request.count)

    const permissions = usePermissions()
    const mountings = useMountingsFromSearch()
    const setMountingsFilters = useSetMountingsFilters()
    const goTo = useGoTo()
    const downloadMountingsReport = useDownloadMountingsReport()
    const [modalModel, showModal] = useDeleteMountingModal()
    useResetScrollOnSearchChange()

    const editMountingHandler = useCallback(
        (id: number) => {
            goTo(getMountingLink(id))
        },
        [goTo]
    )

    const deleteMountingHandler = useCallback(
        (mounting: Mounting) => {
            showModal(mounting)
        },
        [showModal]
    )

    const downloadMountingHandler = useCallback(
        (mounting: Mounting) => {
            downloadMountingsReport(mounting)
        },
        [downloadMountingsReport]
    )

    const filterParams = getMountingsFilterParams(search)
    const {offset, limit} = filterParams

    return (
        <div className={styles.page}>
            {modalModel && <Modal model={modalModel} />}
            <HeaderTitle>Монтажи</HeaderTitle>
            <div className={styles.content}>
                {permissions.isNonQualified && (
                    <ConfirmDealerLink className={styles.permissionError} title="Для управления монтажами необходимо" />
                )}
                <MountingsFilter filters={filterParams} onChange={setMountingsFilters} className={styles.filters} />
                {mountings.length > 0 ? (
                    <>
                        {mountings.map(mounting => (
                            <MountingListItem
                                key={mounting.id}
                                mounting={mounting}
                                onEdit={editMountingHandler}
                                onDelete={deleteMountingHandler}
                                onDownload={downloadMountingHandler}
                            />
                        ))}
                    </>
                ) : (
                    <>{requestCount === 0 && <p>{HINTS.mountingsNotFound}</p>}</>
                )}
                <PaginationMenu
                    className={styles.paginationMenu}
                    currentPage={Math.ceil(offset / limit)}
                    itemsOnPage={limit}
                    totalCount={mountingsTotalCount}
                    onSelect={page => {
                        const offset = limit * page
                        const filter = getMountingsFilter({...filterParams, offset})
                        goTo(`${NAV_MOUNTINGS}${filter}`)
                    }}
                />
            </div>
        </div>
    )
}
