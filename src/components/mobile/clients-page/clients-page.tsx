import * as React from 'react'
import {NAV_CLIENTS, NAV_CLIENT} from '../../../constants/navigation'
import {SearchForm} from '../../common/search-form/search-form'
import {PaginationMenu} from '../../common/pagination-menu/pagination-menu'
import {useEffect} from 'react'
import {getClientsFilter, getClientsFilterParams} from '../../../helpers/clients'
import {HINTS} from '../../../constants/hints'
import {HeaderTitle} from '../header/header-title'
import {HeaderControls} from '../header/header-controls'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useClientsFromSearch, useSetCurrentClient, useDeleteClientModal} from '../../../hooks/clients'
import {useResetScrollOnSearchChange} from '../../../hooks/ui'
import {useGoTo} from '../../../hooks/router'
import {ClientListItem} from '../client-list-item/client-list-item'
import {Button} from '../../common/button/button'
import {Modal} from '../../common/modal/modal'
import styles from './clients-page.module.css'

export function ClientsPage(): JSX.Element {
    const requestCount = useSelector((state: IAppState) => state.request.count)
    const clientsTotalCount = useSelector((state: IAppState) => {
        return state.clients.searchQuery ? state.clients.filtered.length : state.clients.totalCount
    })
    const search = useSelector((state: IAppState) => state.router.location.search)
    const clients = useClientsFromSearch()
    const setCurrentClient = useSetCurrentClient()
    const goTo = useGoTo()
    const [modalModel, showModal] = useDeleteClientModal()
    useResetScrollOnSearchChange()

    const {offset, limit} = getClientsFilterParams(search)

    useEffect(() => {
        return () => {
            setCurrentClient(null)
        }
    }, [setCurrentClient])

    return (
        <div className={styles.page}>
            {modalModel && <Modal model={modalModel} />}
            <HeaderTitle>Клиенты</HeaderTitle>
            <HeaderControls>
                <Button
                    buttonStyle="with-fill"
                    onClick={() => {
                        setCurrentClient(null)
                        goTo(NAV_CLIENT)
                    }}>
                    Новый клиент
                </Button>
            </HeaderControls>
            <SearchForm className={styles.form} />
            {clients.length > 0
                ? clients.slice(offset, clients.length < offset + limit ? clients.length : offset + limit).map(client => (
                      <ClientListItem
                          key={client.id}
                          client={client}
                          onEdit={() => {
                              setCurrentClient(client)
                              goTo(`${NAV_CLIENT}?clientId=${client.id}`)
                          }}
                          onDelete={() => showModal(client)}
                      />
                  ))
                : requestCount === 0 && <p className={styles.emptyResult}>{HINTS.clientsNotFound}</p>}
            <PaginationMenu
                className={styles.menu}
                currentPage={Math.ceil(offset / limit)}
                itemsOnPage={limit}
                totalCount={clientsTotalCount}
                onSelect={page => goTo(`${NAV_CLIENTS}${getClientsFilter(limit * page, limit)}`)}
            />
        </div>
    )
}
