import * as React from 'react'
import {useEffect, useRef, useCallback, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {MobileSvgConstructionDrawerContainer} from '../mobile-svg-construction-drawer-container/mobile-svg-construction-drawer-container'
import {useSetConnectingSide, useSetHostProduct, useSetTemplateFilter, useShowTemplateSelector} from '../../../hooks/template-selector'
import {useSetCurrentProduct} from '../../../hooks/orders'
import {
    useSetConfigContext,
    useSetCurrentContext,
    useSetCurrentDimensionLine,
    useSetHighlightedContext,
    useSetHighlightedDimensionLine
} from '../../../hooks/builder'
import {ConstructionConfigurator} from '../construction-configurator/construction-configurator'
import {SaveOrTransferToProductionButton} from '../../common/save-or-transfer-to-production-button/save-or-transfer-to-production-button'
import {OrderDatePriceInfo} from '../order-date-price-info/order-date-price-info'
import {ConfigContexts} from '../../../model/config-contexts'
import {getClassNames} from '../../../helpers/css'
import {Connector} from '../../../model/framer/connector'
import {useShowStatusbar} from '../../../hooks/statusbar'
import {useChangeDimensionLineValueHandler} from '../../../helpers/builder'
import {setBuilderMode} from '../../../redux/builder/actions'
import {BuilderModes} from '../../../model/builder-modes'
import {UndoRedo} from '../../desktop/undo-redo/undo-redo'
import {EnergyInfoModal} from '../../common/energy-info-modal/energy-info-modal'
import {useEffectSkipFirst} from '../../../hooks/skip-first'
import styles from './construction-tab-content.module.css'

export function ConstructionTabContent(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const builderMode = useSelector((state: IAppState) => state.builder.mode)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)
    const highlightedContext = useSelector((state: IAppState) => state.builder.highlightedContext)
    const currentDimensionLine = useSelector((state: IAppState) => state.builder.currentDimensionLine)
    const highlightedDimensionLine = useSelector((state: IAppState) => state.builder.highlightedDimensionLine)
    const isReadonly = useSelector((state: IAppState) => Boolean(state.orders.currentOrder && state.orders.currentOrder.readonly))
    const windowSize = useSelector((state: IAppState) => state.browser.windowSize)
    const lamination = useSelector((state: IAppState) => state.lamination.all)

    const pageRef = useRef<HTMLDivElement>(null)

    const [showEnergyInfo, setShowEnergyInfo] = useState(false)

    const dispatch = useDispatch()
    const changeDimensionLineValueHandler = useChangeDimensionLineValueHandler()
    const showTemplateSelector = useShowTemplateSelector()
    const setCurrentProduct = useSetCurrentProduct()
    const setConfigContext = useSetConfigContext()
    const setTemplateFilter = useSetTemplateFilter()
    const setConnectingSide = useSetConnectingSide()
    const setHostProduct = useSetHostProduct()
    const setCurrentContext = useSetCurrentContext()
    const setHighlightedContext = useSetHighlightedContext()
    const setCurrentDimensionLine = useSetCurrentDimensionLine()
    const setHighlightedDimensionLine = useSetHighlightedDimensionLine()
    const showStatusbar = useShowStatusbar()

    useEffect(() => {
        if (configContext === ConfigContexts.Connector && !(currentContext instanceof Connector)) {
            showStatusbar('Выберите соединитель', false)
        }
    }, [currentContext, configContext, showStatusbar])

    useEffect(() => {
        if (builderMode === BuilderModes.Connector) {
            window.scroll({
                top: 0,
                behavior: 'smooth'
            })
        }
    }, [builderMode])

    const onInfoModalClose = useCallback(() => {
        setShowEnergyInfo(false)
    }, [])

    useEffectSkipFirst(() => {
        if (!currentConstruction?.haveEnergyProducts) {
            setShowEnergyInfo(true)
        }
    }, [currentConstruction?.haveEnergyProducts])

    if (!currentOrder || !currentConstruction || !currentProduct) {
        return null
    }

    return (
        <div ref={pageRef} className={getClassNames(styles.content, {[styles.connectorMode]: builderMode === BuilderModes.Connector})}>
            <div className={styles.builder}>
                <MobileSvgConstructionDrawerContainer
                    showTemplateSelector={showTemplateSelector}
                    currentConstruction={currentConstruction}
                    currentContext={currentContext}
                    highlightedContext={highlightedContext}
                    currentDimensionLine={currentDimensionLine}
                    highlightedDimensionLine={highlightedDimensionLine}
                    onSelectContext={setCurrentContext}
                    onHoverContext={setHighlightedContext}
                    onSelectDimensionLine={setCurrentDimensionLine}
                    onHoverDimensionLine={setHighlightedDimensionLine}
                    setCurrentProductAction={setCurrentProduct}
                    setConfigContext={setConfigContext}
                    setHostProductAction={setHostProduct}
                    setConnectingSideAction={setConnectingSide}
                    setTemplateFilterAction={setTemplateFilter}
                    isReadonly={isReadonly}
                    windowSize={windowSize}
                    lamination={lamination}
                    onChangeDimensionLineValue={changeDimensionLineValueHandler}
                    onClickOutConstruction={() => {
                        setCurrentContext(null)
                        dispatch(setBuilderMode(null))
                    }}
                />
                <UndoRedo className={styles.undoRedo} />
                {currentConstruction?.haveEnergyProducts && <div className={styles.energyIcon}>{currentConstruction?.energyProductType}</div>}
            </div>
            <ConstructionConfigurator />
            <div className={styles.footer}>
                <OrderDatePriceInfo order={currentOrder} priceTitle="Весь заказ" />
                <SaveOrTransferToProductionButton className={styles.saveTransferButton} />
            </div>
            {showEnergyInfo && <EnergyInfoModal onAction={onInfoModalClose} onClose={onInfoModalClose} />}
        </div>
    )
}
