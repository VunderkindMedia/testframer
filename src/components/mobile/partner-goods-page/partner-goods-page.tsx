import * as React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {OrderStates} from '../../../model/framer/order-states'
import {WithTitle} from '../../common/with-title/with-title'
import {DateRangeSelector} from '../../common/date-range-selector/date-range-selector'
import {SearchInput} from '../../common/search-input/search-input'
import {Select} from '../../common/select/select'
import {STATES_ITEMS} from '../../../constants/states-items'
import {arraysHaveSameItems} from '../../../helpers/array'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'
import {getGoodsFilterParams, getOrdersFilter} from '../../../helpers/orders'
import {useSetPartnerGoodsFilterParams, usePartnerGoodsFromSearch} from '../../../hooks/orders'
import {HeaderTitle} from '../header/header-title'
import {PartnerGoodsListItem} from '../partner-goods-list-item/partner-goods-list-item'
import {PaginationMenu} from '../../common/pagination-menu/pagination-menu'
import {useGoTo} from '../../../hooks/router'
import {NAV_PARTNER_GOODS} from '../../../constants/navigation'
import {HINTS} from '../../../constants/hints'
import {useResetScrollOnSearchChange} from '../../../hooks/ui'
import {useCallback, useEffect} from 'react'
import {Good} from '../../../model/framer/good'
import {deselectGoods, loadPartnerGoodsReport, selectGoods} from '../../../redux/orders/actions'
import {AnalyticsManager} from '../../../analytics-manager'
import {HeaderControls} from '../header/header-controls'
import {DownloadButton} from '../download-button/download-button'
import styles from './partner-goods-page.module.css'

export function PartnerGoodsPage(): JSX.Element {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const requestCount = useSelector((state: IAppState) => state.request.count)
    const selectedPartnerGoods = useSelector((state: IAppState) => state.orders.selectedPartnerGoods)

    const goTo = useGoTo()
    const setFilterParams = useSetPartnerGoodsFilterParams()
    const partnerGoods = usePartnerGoodsFromSearch()
    const filterParams = getGoodsFilterParams(search)
    const dispatch = useDispatch()
    const selectGoodsAction = useCallback((goods: Good[]) => dispatch(selectGoods(goods)), [dispatch])
    const deselectGoodsAction = useCallback((goods: Good[]) => dispatch(deselectGoods(goods)), [dispatch])
    const loadPartnerGoodsReportAction = useCallback((filter: string) => dispatch(loadPartnerGoodsReport(filter)), [dispatch])
    useResetScrollOnSearchChange()

    const {dateFrom, dateTo, states, offset, limit} = filterParams
    const selectedItem = STATES_ITEMS.find(i => arraysHaveSameItems(i.data, states, (s1, s2) => s1 === s2))

    useEffect(() => {
        if (!selectedItem) {
            setFilterParams({...filterParams, states: STATES_ITEMS[0].data})
        }
    }, [selectedItem, filterParams, setFilterParams])

    return (
        <div className={styles.page}>
            <HeaderTitle>Отчет по материалам</HeaderTitle>
            <HeaderControls>
                <DownloadButton
                    isDisabled={selectedPartnerGoods.length === 0}
                    onClick={() => {
                        loadPartnerGoodsReportAction(`${search}&goods=${selectedPartnerGoods.map(g => g.id).join(',')}`)
                        AnalyticsManager.trackEvent({
                            name: 'Клик "Скачать отчет (по доп. материалам)"',
                            target: 'download-partner-goods-report-button'
                        })
                    }}
                />
            </HeaderControls>
            <Select<OrderStates[]>
                className={styles.filter}
                items={STATES_ITEMS}
                selectedItem={selectedItem}
                onSelect={item => setFilterParams({...filterParams, states: item.data})}
            />
            <WithTitle title="Дата монтажа" className={styles.filter}>
                <DateRangeSelector
                    startDate={dateFrom}
                    endDate={dateTo}
                    onChange={(startDate, endDate) => setFilterParams({...filterParams, dateFrom: startDate, dateTo: endDate})}
                />
            </WithTitle>
            <WithTitle title="Номер" className={styles.number}>
                <SearchInput
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    value={filterParams.orderNumber}
                    placeholder="Поиск"
                    onChange={value => setFilterParams({...filterParams, orderNumber: value})}
                    onClear={() => setFilterParams({...filterParams, orderNumber: ''})}
                />
            </WithTitle>
            {partnerGoods.length > 0
                ? partnerGoods
                      .slice(offset, partnerGoods.length < offset + limit ? partnerGoods.length : offset + limit)
                      .map(good => (
                          <PartnerGoodsListItem
                              key={good.id}
                              good={good}
                              isSelected={selectedPartnerGoods.some(g => g.id === good.id)}
                              onChangeSelectedState={selected => (selected ? selectGoodsAction([good]) : deselectGoodsAction([good]))}
                          />
                      ))
                : requestCount === 0 && <p>{HINTS.partnerGoodsNotFound}</p>}
            <PaginationMenu
                className={styles.menu}
                currentPage={Math.ceil(offset / limit)}
                itemsOnPage={limit}
                totalCount={partnerGoods.length}
                onSelect={page => goTo(`${NAV_PARTNER_GOODS}${getOrdersFilter({...filterParams, offset: limit * page})}`)}
            />
        </div>
    )
}
