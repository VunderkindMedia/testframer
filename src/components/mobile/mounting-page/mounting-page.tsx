import * as React from 'react'
import {Modal} from '../../common/modal/modal'
import {
    useUpdateMountingDate,
    useUpdateMountingNote,
    useUpdateClientAddress,
    useDeleteMountingModal,
    useDownloadMountingsReport,
    useMountingFromSearch
} from '../../../hooks/mountings'
import {useGoTo} from '../../../hooks/router'
import {HeaderTitle} from '../header/header-title'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {Button} from '../../common/button/button'
import {CalendarButton} from '../../common/button/calendar-button/calendar-button'
import {WithTitle} from '../../common/with-title/with-title'
import {Input} from '../../common/input/input'
import {Textarea} from '../../common/textarea/textarea'
import {NAV_MOUNTINGS} from '../../../constants/navigation'
import {HeaderControls} from '../header/header-controls'
import {DownloadButton} from '../download-button/download-button'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'
import styles from './mounting-page.module.css'

export const MountingPage = (): JSX.Element | null => {
    const mounting = useMountingFromSearch()
    const updateMountingDate = useUpdateMountingDate()
    const updateClientAddress = useUpdateClientAddress()
    const updateMountingNote = useUpdateMountingNote()
    const downloadMountingsReport = useDownloadMountingsReport()
    const goTo = useGoTo()
    const [modalModel, showModal] = useDeleteMountingModal()

    if (!mounting) {
        return null
    }

    const {order, createdAt, client, note, datetime} = mounting

    return (
        <div className={styles.mounting}>
            {modalModel && <Modal model={modalModel} />}
            <HeaderTitle>{order.fullOrderNumber}</HeaderTitle>
            <HeaderControls>
                <DownloadButton
                    onClick={() => {
                        downloadMountingsReport(mounting)
                    }}
                />
            </HeaderControls>
            <p className={styles.date}>
                <span>Дата создания</span>
                <span className={styles.dateCreated}>{getDateDDMMYYYYFormat(new Date(createdAt))}</span>
            </p>

            <div className={styles.date}>
                <span>Дата монтажа</span>
                <span className={styles.datetime}>{getDateDDMMYYYYFormat(new Date(datetime))}</span>
                <CalendarButton date={new Date(datetime)} direction="right" onSelect={date => updateMountingDate(mounting, date)} />
            </div>
            {client && (
                <WithTitle title="Адрес">
                    <Input
                        className={styles.input}
                        value={client.address}
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        onChange={value => updateClientAddress(mounting, value)}
                    />
                </WithTitle>
            )}
            <WithTitle title="Комментарий">
                <Textarea
                    className={styles.input}
                    value={note}
                    rows={3}
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    onChange={value => updateMountingNote(mounting, value)}
                />
            </WithTitle>
            <Button
                className={styles.deleteButton}
                buttonStyle="with-border"
                onClick={() => {
                    showModal(mounting, () => {
                        goTo(NAV_MOUNTINGS)
                    })
                }}>
                Удалить
            </Button>
        </div>
    )
}
