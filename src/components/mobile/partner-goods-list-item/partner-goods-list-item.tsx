import * as React from 'react'
import {Good} from '../../../model/framer/good'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {WithTitle} from '../../common/with-title/with-title'
import {getOrderStateName} from '../../../model/framer/order-states'
import {getTextOrMdash} from '../../../helpers/text'
import {ListItem, ListItemHeader, ListItemBody} from '../list-item'
import {Checkbox} from '../../common/checkbox/checkbox'
import styles from './partner-goods-list-item.module.css'

interface IPartnerGoodsListItemProps {
    isSelected: boolean
    onChangeSelectedState: (selected: boolean) => void
    good: Good
}

export function PartnerGoodsListItem(props: IPartnerGoodsListItemProps): JSX.Element | null {
    const {good, isSelected, onChangeSelectedState} = props

    const {order} = good

    if (!order) {
        return null
    }

    return (
        <ListItem state={order.state} onClick={() => onChangeSelectedState(!isSelected)}>
            <ListItemHeader className={styles.header}>
                <Checkbox className={styles.checkbox} onChange={onChangeSelectedState} isChecked={isSelected} />
                <p>{order.fullOrderNumber}</p>
            </ListItemHeader>
            <ListItemBody>
                <div className={styles.row}>
                    <WithTitle title="Дата создания">{getDateDDMMYYYYFormat(new Date(order.createdAt))}</WithTitle>
                    <WithTitle title="Статус заказа">{getOrderStateName(order.state)}</WithTitle>
                </div>
                <div className={styles.row}>
                    <WithTitle title="Наименование материала">
                        <p>{good.name}</p>
                    </WithTitle>
                    <WithTitle title="Кол-во">
                        <p>{good.quantity}</p>
                    </WithTitle>
                </div>
                <WithTitle title="Параметры материала" className={styles.params}>
                    {getTextOrMdash(good.attributes.map(a => a.visibleDescription).join(', '))}
                </WithTitle>
                <div className={styles.row}>
                    <WithTitle title="Дата отгрузки">
                        {order.shippingDate.length > 0 ? (
                            getDateDDMMYYYYFormat(new Date(order.shippingDate))
                        ) : order.estShippingDate.length === 0 ? (
                            <>&mdash;</>
                        ) : (
                            <> ~ {getDateDDMMYYYYFormat(new Date(order.estShippingDate))}</>
                        )}
                    </WithTitle>
                    <WithTitle title="Дата монтажа">
                        {order.mountingDate ? getDateDDMMYYYYFormat(new Date(order.mountingDate)) : <>&mdash;</>}
                    </WithTitle>
                </div>
            </ListItemBody>
        </ListItem>
    )
}
