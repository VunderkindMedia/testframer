import * as React from 'react'
import {useCallback, useState} from 'react'
import {NAV_ORDERS} from '../../../constants/navigation'
import {PaginationMenu} from '../../common/pagination-menu/pagination-menu'
import {OrdersFilter} from '../orders-filter/orders-filter'
import {getOrdersFilter, getOrdersFilterParams} from '../../../helpers/orders'
import {useOrdersFromSearch, useShowRealCost, useSetOrderFilterParams} from '../../../hooks/orders'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useGoTo} from '../../../hooks/router'
import {HINTS} from '../../../constants/hints'
import {useUpdateAppVersion} from '../../../hooks/app'
import {OrderListItem} from '../order-list-item/order-list-item'
import {HeaderTitle} from '../header/header-title'
import {HeaderControls} from '../header/header-controls'
import {useResetScrollOnSearchChange} from '../../../hooks/ui'
import {useGoToNewOrder} from '../../../hooks/orders'
import {Button} from '../../common/button/button'
import {RegistrationModal} from '../../common/registration-modal/registration-modal'
import styles from './orders-page.module.css'

export function OrdersPage(): JSX.Element {
    const [isDemoLimitModalOpened, setIsDemoLimitModalOpened] = useState(false)
    const orders = useOrdersFromSearch()
    const requestCount = useSelector((state: IAppState) => state.request.count)
    const ordersTotalCount = useSelector((state: IAppState) => state.orders.totalCount)
    const search = useSelector((state: IAppState) => state.router.location.search)
    const isFramerPointsAvailable = useSelector((state: IAppState) => state.account.partner?.isFramerPointsAvailable || false)

    const [shouldShowRealCost, setShouldShowRealCost] = useShowRealCost()
    const goTo = useGoTo()
    const setFilterParams = useSetOrderFilterParams()
    useUpdateAppVersion()
    useResetScrollOnSearchChange()
    const goToNewOrder = useGoToNewOrder()

    const {states, dateFrom, dateTo, orderNumber, offset, limit} = getOrdersFilterParams(search)

    const newOrderHandler = useCallback(() => {
        const isNavAvailable = goToNewOrder()
        !isNavAvailable && setIsDemoLimitModalOpened(true)
    }, [goToNewOrder])

    const closeLimitModalHandler = useCallback(() => {
        setIsDemoLimitModalOpened(false)
    }, [])

    return (
        <div className={styles.page}>
            <HeaderTitle>Заказы</HeaderTitle>
            <HeaderControls>
                <Button buttonStyle="with-fill" dataId="new-order-button" onClick={newOrderHandler}>
                    Новый заказ
                </Button>
            </HeaderControls>
            <div className={styles.content}>
                <OrdersFilter
                    orderStates={states}
                    startDate={dateFrom}
                    endDate={dateTo}
                    orderNumber={orderNumber}
                    onChange={(states, dateFrom, dateTo, orderNumber) => {
                        setFilterParams({
                            states,
                            dateFrom,
                            dateTo,
                            orderNumber,
                            offset: 0,
                            limit
                        })
                    }}
                />
                {orders.length > 0 ? (
                    <>
                        {orders.map(order => (
                            <OrderListItem
                                key={order.id}
                                order={order}
                                shouldShowRealCost={shouldShowRealCost}
                                isFramerPointsAvailable={isFramerPointsAvailable}
                                onShowRealCost={show => setShouldShowRealCost(show)}
                            />
                        ))}
                    </>
                ) : (
                    <>{requestCount === 0 && <p>{HINTS.ordersNotFound}</p>}</>
                )}
                <PaginationMenu
                    className={styles.paginationMenu}
                    currentPage={Math.ceil(offset / limit)}
                    itemsOnPage={limit}
                    totalCount={ordersTotalCount}
                    onSelect={page => {
                        const offset = limit * page
                        const filter = getOrdersFilter({
                            states,
                            dateFrom,
                            dateTo,
                            orderNumber,
                            offset,
                            limit
                        })
                        goTo(`${NAV_ORDERS}${filter}`)
                    }}
                />
            </div>
            {isDemoLimitModalOpened && <RegistrationModal isWarningMode={true} onClose={closeLimitModalHandler} />}
        </div>
    )
}
