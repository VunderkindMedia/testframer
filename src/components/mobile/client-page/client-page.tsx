import * as React from 'react'
import {useCallback} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {HeaderTitle} from '../header/header-title'
import {ClientEditor} from '../../common/client-editor/client-editor'
import {useClientFromSearch, useClientOrders, useUpdateClient, useCreateClient} from '../../../hooks/clients'
import {Client} from '../../../model/framer/client'
import {getTextOrMdash} from '../../../helpers/text'
import {PageTitle} from '../../common/page-title/page-title'
import {HINTS} from '../../../constants/hints'
import {ClientOrderListItem} from '../client-order-list-item/client-order-list-item'
import styles from './client-page.module.css'

export const ClientPage = (): JSX.Element | null => {
    const requestCount = useSelector((state: IAppState) => state.request.count)
    const client = useClientFromSearch()
    const orders = useClientOrders()
    const updateClient = useUpdateClient()
    const createClient = useCreateClient()

    const handleClientChange = useCallback(
        (client: Client) => {
            if (client.isNew) {
                createClient(client)
            } else {
                updateClient(client)
            }
        },
        [updateClient, createClient]
    )

    if (!client) {
        return null
    }

    const {owner, filial} = client

    return (
        <div className={styles.page}>
            <HeaderTitle>{client.isNew ? 'Новый клиент' : 'Клиент'}</HeaderTitle>
            <ClientEditor client={client} onChange={handleClientChange} />
            {!client.isNew && (
                <div className={styles.details}>
                    <p className={styles.owner}>Менеджер: {getTextOrMdash(owner ? owner.name || owner.email : '')}</p>
                    <p>Филиал: {getTextOrMdash(filial ? filial.name : '')}</p>
                </div>
            )}
            {!client.isNew && (
                <>
                    <PageTitle title="Сделки" className={styles.title} />
                    {orders.length > 0 ? (
                        orders.map(order => <ClientOrderListItem key={order.id} order={order} />)
                    ) : (
                        <>{requestCount === 0 && <p>{HINTS.clientOrdersNotFound}</p>}</>
                    )}
                </>
            )}
        </div>
    )
}
