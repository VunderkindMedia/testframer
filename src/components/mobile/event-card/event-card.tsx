import * as React from 'react'
import {CalendarEvent} from '../../../model/framer/calendar/calendar-event'
import {EventList} from '../../common/event-list/event-list'
import {CalendarEventTypes} from '../../../model/framer/calendar/calendar-event-types'
import {Card} from '../../common/card/card'
import {CardHeader} from '../../common/card/card-header/card-header'
import {getDateDDMMMMYYYYFormat, getWeekdayName} from '../../../helpers/date'
import styles from './event-card.module.css'

interface IEventCardProps {
    events: CalendarEvent[]
    date: Date | null
    onHide?: () => void
}

export const EventCard = (props: IEventCardProps): JSX.Element => {
    const {events, date, onHide} = props

    return (
        <Card className={styles.card}>
            <CardHeader
                title={`${getDateDDMMMMYYYYFormat(date)} (${date && getWeekdayName(date)})`}
                onClick={() => {
                    onHide && onHide()
                }}
            />
            <div className={styles.events}>
                <EventList events={events} type={CalendarEventTypes.Mounting} />
            </div>
        </Card>
    )
}
