import * as React from 'react'
import {useEffect, useCallback} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {SelectClient} from '../../common/select-client/select-client'
import {IAppState} from '../../../redux/root-reducer'
import {
    useAddGood2,
    useSaveOrder,
    useUpdateOrderDiscount,
    useLoadOffer,
    useSetCurrentConstruction,
    useLoadPassport
} from '../../../hooks/orders'
import {getClassNames} from '../../../helpers/css'
import {GoodsCard} from '../../common/goods-card/goods-card'
import {ServicesCard} from '../../common/services-card/services-card'
import {ConfigContexts} from '../../../model/config-contexts'
import {useLoadConstructionAvailableServices} from '../../../hooks/services'
import {OrderConstructionCard} from '../order-construction-card/order-construction-card'
import {OrderDiscountEditor} from '../../common/order-discount-editor/order-discount-editor'
import {HINTS} from '../../../constants/hints'
import {Button} from '../../common/button/button'
import {ButtonWithHint} from '../../common/button/button-with-hint/button-with-hint'
import {usePermissions} from '../../../hooks/permissions'
import {AnalyticsManager} from '../../../analytics-manager'
import {getOrderStateName, getOrderStateColor} from '../../../model/framer/order-states'
import {Order} from '../../../model/framer/order'
import {SaveOrTransferToProductionButton} from '../../common/save-or-transfer-to-production-button/save-or-transfer-to-production-button'
import {OrderDatePriceInfo} from '../order-date-price-info/order-date-price-info'
import {Card} from '../../common/card/card'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {DateSelector} from '../../common/date-selector/date-selector'
import {WithTitle} from '../../common/with-title/with-title'
import {ConstructionPreview} from '../../common/construction-preview/construction-preview'
import {SvgIconConstructionDrawer} from '../../common/svg-icon-construction-drawer/svg-icon-construction-drawer'
import {Colors} from '../../../constants/colors'
import {ErrorTypes} from '../../../model/framer/error-types'
import {useAddMounting} from '../../../hooks/mountings'
import {useClients} from '../../../hooks/clients'
import {updateMountingDate} from '../../../redux/orders/actions'
import {ConfirmDealerLink} from '../../common/confirm-dealer-link/confirm-dealer-link'
import styles from './order-tab-content.module.css'
import {FactoryTypes} from '../../../model/framer/factory-types'
import {Link} from '../../common/link/link'

export function OrderTabContent(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const partner = useSelector((state: IAppState) => state.account.partner)
    const factoryGoodGroups = useSelector((state: IAppState) => state.goods.factoryGoodGroups)
    const allFactoryGoods = useSelector((state: IAppState) => state.goods.factoryGoods)
    const partnerGoodGroups = useSelector((state: IAppState) => state.goods.partnerGoodGroups)
    const allPartnerGoods = useSelector((state: IAppState) => state.goods.partnerGoods)
    const orderAvailableServices = useSelector((state: IAppState) => state.services.orderAvailableServices)
    const isVisibleServiceCard = useSelector((state: IAppState) => state.builder.configContext === ConfigContexts.Services)
    const constructionAvailableServices = useSelector((state: IAppState) => state.services.constructionAvailableServices)

    const setCurrentConstruction = useSetCurrentConstruction()
    const addGood2 = useAddGood2()
    const updateOrderDiscount = useUpdateOrderDiscount()
    const saveOrder = useSaveOrder()
    const loadConstructionAvailableServices = useLoadConstructionAvailableServices()
    const loadOffer = useLoadOffer()
    const permission = usePermissions()
    const clients = useClients()
    const loadPassport = useLoadPassport()

    useEffect(() => {
        if (currentConstruction || !currentOrder) {
            return
        }
        setCurrentConstruction(currentOrder.constructions[0])
    }, [currentOrder, currentConstruction, setCurrentConstruction])

    const currentOrderId = currentOrder?.id

    const currentConstructionId = currentConstruction?.id

    useEffect(() => {
        if (!currentOrderId || !currentConstructionId || !isVisibleServiceCard) {
            return
        }
        if (constructionAvailableServices[currentConstructionId]) {
            return
        }
        loadConstructionAvailableServices(currentOrderId, currentConstructionId)
    }, [currentOrderId, currentConstructionId, isVisibleServiceCard, constructionAvailableServices, loadConstructionAvailableServices])

    const dispatch = useDispatch()
    const addMounting = useAddMounting()

    const addMountingAction = useCallback(
        (order: Order, mountingDate: Date) => {
            if (order.mountingDate) {
                dispatch(updateMountingDate(order, mountingDate))
            } else {
                addMounting(order, mountingDate)
            }
        },
        [addMounting, dispatch]
    )

    if (!currentOrder) {
        return null
    }

    const isCheckDateShipping = new Date().getTime() > new Date(currentOrder.shippingDate).getTime() + 86400000

    const orderGoods = currentOrder.goods.filter(g => !g.constructionId)
    const constructionGoods = currentOrder.goods.filter(g => g.constructionId)
    const orderServices = currentOrder.services.filter(s => !s.constructionId)
    const constructionServices = currentOrder.services.filter(s => s.constructionId)

    return (
        <div className={styles.content}>
            <Card className={`${styles.card} ${styles.constrictionsCard}`}>
                <WithTitle title="Клиент" className={styles.selectClientWrapper}>
                    <SelectClient className={styles.selectClient} clients={clients} order={currentOrder} />
                </WithTitle>
                <p className={styles.details}>
                    <span className={styles.state} style={{background: getOrderStateColor(currentOrder.state)}}>
                        {getOrderStateName(currentOrder.state)}
                    </span>
                    {getDateDDMMYYYYFormat(currentOrder.createdAt ? new Date(currentOrder.createdAt) : null)}
                </p>
                <p className={`${styles.title} ${styles.constructionTitle}`}>Конструкции</p>
                <div className={styles.tabs}>
                    {currentOrder.sortedConstructions.map(construction => {
                        const errors = construction.errors.filter(error => error.errorType === ErrorTypes.Error)

                        return (
                            <div
                                key={construction.id}
                                onClick={() => setCurrentConstruction(construction)}
                                className={getClassNames(styles.tab, {[styles.active]: construction.id === currentConstructionId})}>
                                <ConstructionPreview quantity={construction.quantity} isAutoWidth={true}>
                                    <SvgIconConstructionDrawer
                                        construction={construction}
                                        maxWidth={24}
                                        maxHeight={24}
                                        lineColor={errors.length ? Colors.Red : Colors.Gray}
                                    />
                                </ConstructionPreview>
                            </div>
                        )
                    })}
                </div>
                {currentConstruction && <OrderConstructionCard order={currentOrder} construction={currentConstruction} />}
            </Card>
            <GoodsCard
                className={styles.card}
                title="Доп. материалы к заказу"
                isDisabled={currentOrder.readonly}
                goods={orderGoods}
                allFactoryGoodGroups={factoryGoodGroups}
                allFactoryGoods={allFactoryGoods}
                allPartnerGoodGroups={partnerGoodGroups}
                allPartnerGoods={allPartnerGoods}
                onAddGood={good => addGood2(currentOrder, good)}
                onChange={(goods, action) => {
                    const orderClone = currentOrder.clone
                    orderClone.goods = [...constructionGoods, ...goods]
                    orderClone.addAction(action)
                    saveOrder(orderClone)
                }}
            />
            <ServicesCard
                className={styles.card}
                title="Услуги к заказу"
                isDisabled={currentOrder.readonly}
                services={orderServices}
                availableServices={orderAvailableServices}
                onChange={(services, action) => {
                    const orderClone = currentOrder.clone
                    orderClone.services = [...constructionServices, ...services]
                    orderClone.addAction(action)
                    saveOrder(orderClone)
                }}
            />
            {permission.isNonQualified ? (
                <ConfirmDealerLink className={styles.permissionError} title="Для добавления событий необходимо" />
            ) : (
                <div className={styles.mountingDate}>
                    <p className={styles.itemTitle}>Дата монтажа</p>
                    <DateSelector
                        date={currentOrder.mountingDate ? new Date(currentOrder.mountingDate) : null}
                        isDisabled={currentOrder.isCancelled}
                        onChange={date => addMountingAction(currentOrder, date)}
                        onToggle={state => {
                            state && setTimeout(() => window.scroll(0, window.document.body.scrollHeight), 0)
                        }}
                    />
                </div>
            )}
            <OrderDiscountEditor order={currentOrder} onChangeDiscount={discount => updateOrderDiscount(currentOrder, discount)} />
            <OrderDatePriceInfo order={currentOrder} priceTitle="Итого" className={styles.datePrice} />
            {currentOrder.deliveryInfo[0] && (
                <div style={{marginTop: '4px'}}>
                    <p className={styles.itemTitle}>Информация о доставке</p>
                    {currentOrder.deliveryInfo.map((elem, i) => {
                        return (
                            // eslint-disable-next-line react/jsx-key
                            <div className={styles.shipping}>
                                <p className={styles.itemTitle}>Рейс {i + 1}</p>
                                <span>Телефон водителя: {elem.driverPhone}</span>
                                <span>Гос. номер машины: {elem.carNumber}</span>
                            </div>
                        )
                    })}
                </div>
            )}
            {currentOrder.windrawOrderNumber && partner?.factoryId === FactoryTypes.Perm && isCheckDateShipping && (
                <>
                    <Button
                        dataTest="order-tab-sidebar-download-passport-button"
                        dataId="order-tab-sidebar-download-passport-button"
                        buttonStyle="with-border"
                        isDisabled={permission.isNonQualified}
                        className={styles.button}
                        onClick={e => {
                            e.stopPropagation()
                            loadPassport(currentOrder)
                        }}>
                        Скачать паспорта качества
                    </Button>
                    <div className={styles.linkSertificates}>
                        <Link
                            className={`${styles.link}`}
                            href="https://amegazavod.ru/sertificates_okna/?_ga=2.11026932.1769129912.1620883560-293434870.1617623849"
                            title="Сертификаты"
                        />
                    </div>
                </>
            )}
            <SaveOrTransferToProductionButton className={styles.button} />
            <ButtonWithHint hint={permission.isNonQualified ? HINTS.nonQualifiedRestriction : ''}>
                <Button
                    dataTest="order-page-download-kp-button"
                    buttonStyle="with-border"
                    isDisabled={permission.isNonQualified}
                    className={styles.button}
                    onClick={e => {
                        e.stopPropagation()
                        loadOffer(currentOrder)
                        AnalyticsManager.trackClickDownloadKpButtonFromOrderOrBuilderPage()
                        currentOrder.addAction(`Скачали КП в заказе ${currentOrder.id}`)
                    }}>
                    Скачать КП
                </Button>
            </ButtonWithHint>
        </div>
    )
}
