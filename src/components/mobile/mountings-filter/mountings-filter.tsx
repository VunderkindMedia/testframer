import * as React from 'react'
import {useEffect} from 'react'
import {OrderStates} from '../../../model/framer/order-states'
import {WithTitle} from '../../common/with-title/with-title'
import {Select} from '../../common/select/select'
import {DateRangeSelector} from '../../common/date-range-selector/date-range-selector'
import {SearchInput} from '../../common/search-input/search-input'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'
import {STATES_ITEMS} from '../../../constants/states-items'
import {ListItem} from '../../../model/list-item'
import {arraysHaveSameItems} from '../../../helpers/array'
import {useEmployees, useAffiliates} from '../../../hooks/account'
import {useClients} from '../../../hooks/clients'
import {IMountingsFilterParams} from '../../../helpers/mountings'
import styles from './mountings-filter.module.css'

interface IMountingsFilterProps {
    filters: IMountingsFilterParams
    className?: string
    onChange: (params: IMountingsFilterParams) => void
}

export const MountingsFilter = (props: IMountingsFilterProps): JSX.Element => {
    const {filters, className, onChange} = props

    const employeesList = useEmployees()
    const affiliatesList = useAffiliates()
    const clientsList = useClients()

    const {number, from, to, states, dateFrom, dateTo, owners, filials, clients} = filters

    const selectedStateItem = STATES_ITEMS.find(i => arraysHaveSameItems(i.data, states, (s1, s2) => s1 === s2))
    const employeesListItems = employeesList?.length
        ? [new ListItem('Все', 'Все', -1), ...employeesList.map(({id, name}) => new ListItem(String(id), name, id))]
        : []
    const affiliatesListItems = affiliatesList?.length
        ? [new ListItem('Все', 'Все', -1), ...affiliatesList.map(({id, name}) => new ListItem(String(id), name, id))]
        : []
    const clientsListItems = [new ListItem('Все', 'Все', -1), ...clientsList.map(({id, name}) => new ListItem(String(id), name, id))]

    useEffect(() => {
        if (!selectedStateItem) {
            onChange({...filters, states: STATES_ITEMS[0].data})
        }
    }, [selectedStateItem, filters, onChange])

    return (
        <div className={`${styles.filters} ${className ?? ''}`}>
            <Select<OrderStates[]>
                items={STATES_ITEMS}
                className={styles.states}
                selectedItem={selectedStateItem}
                onSelect={item => onChange({...filters, states: item.data})}
            />
            <WithTitle title="Дата создания" className={`${styles.filter} ${styles.createdDate}`}>
                <DateRangeSelector
                    className={styles.rangeSlider}
                    startDate={from}
                    endDate={to}
                    onChange={(startDate, endDate) => onChange({...filters, from: startDate, to: endDate})}
                />
            </WithTitle>
            <WithTitle title="Номер" className={styles.filter}>
                <SearchInput
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    value={number}
                    placeholder="Поиск"
                    onChange={value => onChange({...filters, number: value})}
                    onClear={() => onChange({...filters, number: ''})}
                />
            </WithTitle>
            {employeesListItems.length > 0 && (
                <WithTitle title="Менеджер" className={styles.filter}>
                    <Select<number | undefined>
                        items={employeesListItems}
                        isMultiSelect={true}
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        selectedItems={
                            owners.length ? employeesListItems.filter(e => e.data && owners.includes(e.data)) : [employeesListItems[0]]
                        }
                        onMultiSelect={items => {
                            const employeesIds: number[] = []
                            items.map(({data}) => {
                                if (data && data !== -1) {
                                    employeesIds.push(data)
                                }
                            })
                            onChange({...filters, owners: employeesIds})
                        }}
                    />
                </WithTitle>
            )}
            <WithTitle title="Клиент" className={styles.filter}>
                <Select<number | undefined>
                    items={clientsListItems}
                    isMultiSelect={true}
                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                    shouldShowFilter={true}
                    selectedItems={clients.length ? clientsListItems.filter(({data}) => data && clients.includes(data)) : [clientsListItems[0]]}
                    onMultiSelect={items => {
                        const clientIds: number[] = []
                        items.map(item => {
                            if (item.data && item.data !== -1) {
                                clientIds.push(item.data)
                            }
                        })
                        onChange({...filters, clients: clientIds})
                    }}
                />
            </WithTitle>
            <WithTitle title="Дата монтажа" className={`${styles.filter} ${styles.mountingDate}`}>
                <DateRangeSelector
                    className={styles.rangeSlider}
                    startDate={dateFrom}
                    endDate={dateTo}
                    onChange={(startDate, endDate) => onChange({...filters, dateFrom: startDate, dateTo: endDate})}
                />
            </WithTitle>
            {affiliatesListItems.length > 0 && (
                <WithTitle title="Филиалы" className={styles.filter}>
                    <Select<number | undefined>
                        isMultiSelect={true}
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        items={affiliatesListItems}
                        selectedItems={
                            filials?.length ? affiliatesListItems.filter(fl => fl.data && filials.includes(fl.data)) : [affiliatesListItems[0]]
                        }
                        onMultiSelect={items => {
                            const filialIds: number[] = []
                            items.map(item => {
                                if (item.data && item.data !== -1) {
                                    filialIds.push(item.data)
                                }
                            })
                            onChange({...filters, filials: filialIds})
                        }}
                    />
                </WithTitle>
            )}
        </div>
    )
}
