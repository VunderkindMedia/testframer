import * as React from 'react'
import {Order} from '../../../model/framer/order'
import {ListItem, ListItemBody, ListItemHeader} from '../list-item'
import {getCurrency} from '../../../helpers/currency'
import {WithTitle} from '../../common/with-title/with-title'
import {getOrderStateName} from '../../../model/framer/order-states'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {ErrorTypes} from '../../../model/framer/error-types'
import {SvgIconConstructionDrawer} from '../../common/svg-icon-construction-drawer/svg-icon-construction-drawer'
import {Colors} from '../../../constants/colors'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ConstructionPreview} from '../../common/construction-preview/construction-preview'
import styles from './client-order-list-item.module.css'

interface IClientOrderListItemProps {
    order: Order
}

export function ClientOrderListItem(props: IClientOrderListItemProps): JSX.Element {
    const {order} = props

    return (
        <ListItem state={order.state}>
            <ListItemHeader>
                <p className={styles.orderNumber}>{order.fullOrderNumber}</p>
                <p className={styles.orderState}>{getOrderStateName(order.state)}</p>
            </ListItemHeader>
            <ListItemBody isEnergy={order.haveEnergyProducts}>
                <div className={styles.bodyRow}>
                    <WithTitle title="Сумма">
                        <p className={styles.price}>
                            {order.totalCost === 0 ? (
                                <span>&mdash;</span>
                            ) : (
                                <>
                                    {getCurrency(order.totalCost)} <span className={styles.currency}>{RUB_SYMBOL}</span>
                                </>
                            )}
                        </p>
                    </WithTitle>
                </div>
                <div className={`${styles.bodyRow} ${styles.constructionsRow}`}>
                    {order.constructions.map(construction => {
                        const errors = construction.errors.filter(error => error.errorType === ErrorTypes.Error)
                        return (
                            <ConstructionPreview quantity={construction.quantity} isAutoWidth={true} key={construction.id}>
                                <SvgIconConstructionDrawer
                                    construction={construction}
                                    maxWidth={Infinity}
                                    maxHeight={24}
                                    lineColor={errors.length > 0 ? Colors.Red : Colors.Gray}
                                />
                            </ConstructionPreview>
                        )
                    })}
                </div>
                <div className={styles.bodyRow}>
                    <WithTitle title="Дата заказа">
                        <p className={styles.date}>{getDateDDMMYYYYFormat(new Date(order.createdAt))}</p>
                    </WithTitle>
                </div>
            </ListItemBody>
        </ListItem>
    )
}
