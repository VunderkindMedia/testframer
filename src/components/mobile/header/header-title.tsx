import * as React from 'react'
import {ReactNode} from 'react'
import {Portal} from '../../common/portal/portal'

interface IHeaderTitleProps {
    children?: ReactNode
}

export function HeaderTitle(props: IHeaderTitleProps): JSX.Element {
    return <Portal selector="#header-title">{props.children}</Portal>
}
