import * as React from 'react'
import {useState, useCallback} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Menu} from '../menu/menu'
import {useModalOpen} from '../../../hooks/ui'
import {RegistrationModal} from '../../common/registration-modal/registration-modal'
import {usePermissions, useIsDemoUser} from '../../../hooks/permissions'
import {AnalyticsManager} from '../../../analytics-manager'
import {Notifications} from '../../common/notifications/notifications'
import styles from './header.module.css'

export function Header(): JSX.Element {
    const isCashMode = useSelector((state: IAppState) => state.account.isCashMode)
    const [isMenuOpened, setIsMenuOpened] = useState(false)
    const [isRegistrationModalOpened, setIsRegistrationModalOpened] = useState(false)
    useModalOpen(isMenuOpened)

    const permissions = usePermissions()
    const isDemoUser = useIsDemoUser()

    const openConfirmModalHandler = useCallback(() => {
        AnalyticsManager.trackConfirmAccountFromHeaderClick()
        setIsRegistrationModalOpened(true)
    }, [])

    const openRegistrationModalHandler = useCallback(() => {
        AnalyticsManager.trackRegisterFromDemoClick()
        setIsRegistrationModalOpened(true)
    }, [])

    const closeRegistrationModalHandler = useCallback(() => {
        setIsRegistrationModalOpened(false)
    }, [])

    const openMenuHandler = useCallback(() => {
        setIsMenuOpened(true)
    }, [])

    const closeMenuHandler = useCallback(() => {
        setIsMenuOpened(false)
    }, [])

    return (
        <div className={`${styles.header}  ${isCashMode && styles.cashMode}`} id="mobile-header">
            {isDemoUser ? (
                <button className={styles.actionButton} onClick={openRegistrationModalHandler}>
                    Зарегистрироваться
                </button>
            ) : (
                permissions.isNonQualified && (
                    <button className={`${styles.actionButton} 'analytics-header-confirm-account-button'`} onClick={openConfirmModalHandler}>
                        Подтвердить учетную запись
                    </button>
                )
            )}
            {isRegistrationModalOpened && <RegistrationModal isWarningMode={false} onClose={closeRegistrationModalHandler} />}
            <div className={styles.container}>
                <button className={styles.menuButton} onClick={openMenuHandler} />
                <Menu isOpened={isMenuOpened} onClose={closeMenuHandler} />
                <div className={styles.title} id="header-title" />
                <Notifications className={styles.notificationButton} />
                <div className={styles.controls} id="header-controls" />
            </div>
        </div>
    )
}
