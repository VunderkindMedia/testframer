import * as React from 'react'
import {ReactNode} from 'react'
import {Portal} from '../../common/portal/portal'

interface IHeaderControlsProps {
    children?: ReactNode
}

export function HeaderControls(props: IHeaderControlsProps): JSX.Element {
    return (
        <Portal selector="#header-controls">
            <div style={{marginLeft: '20px'}}>{props.children}</div>
        </Portal>
    )
}
