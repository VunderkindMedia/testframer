import * as React from 'react'
import './action-button.css'

interface IActionButtonProps {
    icon?: string
    disabledIcon?: string
    title?: string
    isDisabled?: boolean
    onClick?: () => void
}

export function ActionButton(props: IActionButtonProps): JSX.Element {
    const {icon, disabledIcon, title, isDisabled, onClick} = props

    return (
        <div className={`action-button ${isDisabled && 'disabled'}`} onClick={onClick}>
            {icon && <img src={isDisabled ? disabledIcon : icon} alt="+" draggable={false} />}
            {title && <p>{title}</p>}
        </div>
    )
}
