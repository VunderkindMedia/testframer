import * as React from 'react'
import {ReactNode} from 'react'
import styles from './list-item-header.module.css'

interface IListItemHeaderProps {
    className?: string
    children?: ReactNode
}

export function ListItemHeader(props: IListItemHeaderProps): JSX.Element {
    const {children, className} = props

    return <div className={`${styles.header} ${className ?? ''}`}>{children}</div>
}
