import * as React from 'react'
import {ReactNode, useEffect, useRef} from 'react'
import {OrderStates, getOrderCardColor} from '../../../model/framer/order-states'
import styles from './list-item.module.css'

interface IListItemProps {
    children: ReactNode
    state?: OrderStates
    onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
}

export function ListItem(props: IListItemProps): JSX.Element {
    const {children, state, onClick} = props
    const item = useRef<HTMLDivElement>(null)

    useEffect(() => {
        state && item && item.current && item.current.style.setProperty('--card-color', getOrderCardColor(state))
    }, [item, state])

    return (
        <div ref={item} className={styles.listItem} onClick={onClick}>
            {children}
        </div>
    )
}
