import * as React from 'react'
import {ReactNode} from 'react'
import styles from './list-item-body.module.css'

interface IListItemBodyProps {
    children?: ReactNode
    isEnergy?: boolean
}

export function ListItemBody(props: IListItemBodyProps): JSX.Element {
    const {children, isEnergy} = props

    return (
        <div className={`${styles.body} ${isEnergy && styles.energy}`}>
            <div className={styles.bodyBackground} />
            {children}
        </div>
    )
}
