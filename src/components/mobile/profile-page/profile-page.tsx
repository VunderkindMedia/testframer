import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {ProfileTabIds} from '../../common/profile-page/tabs'
import {Margins} from '../../common/profile-page/margins/margins'
import {PrintSettings} from '../../common/profile-page/print-settings/print-settings'
import {AdditionalServices} from '../../common/profile-page/additional-services/additional-services'
import {PartnerGoods} from '../../common/profile-page/partner-goods/partner-goods'
import {Affiliates} from '../../common/profile-page/affiliates/affiliates'
import {Dealers} from '../../common/profile-page/dealers/dealers'
import {HeaderTitle} from '../header/header-title'
import {useResetScrollOnSearchChange} from '../../../hooks/ui'
import {useSetActiveTab, useLoadProfileSettingsAction} from '../../../hooks/account'
import styles from './profile-page.module.css'

export function ProfilePage(): JSX.Element {
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)

    useLoadProfileSettingsAction()
    useResetScrollOnSearchChange()
    const activeTab = useSetActiveTab()

    return (
        <div className={styles.page}>
            {activeTab === ProfileTabIds.Margins && (
                <>
                    <HeaderTitle>Наценки</HeaderTitle>
                    <Margins />
                </>
            )}
            {activeTab === ProfileTabIds.PrintSettings && (
                <>
                    <HeaderTitle>Настройка КП</HeaderTitle>
                    <PrintSettings />
                </>
            )}
            {activeTab === ProfileTabIds.AdditionalServices && (
                <>
                    <HeaderTitle>Услуги</HeaderTitle>
                    <AdditionalServices />
                </>
            )}
            {activeTab === ProfileTabIds.PartnerGoods && (
                <>
                    <HeaderTitle>Доп. материалы</HeaderTitle>
                    <PartnerGoods />
                </>
            )}
            {activeTab === ProfileTabIds.Affiliate && (
                <>
                    <HeaderTitle>Филиалы и сотрудники</HeaderTitle>
                    <Affiliates />
                </>
            )}
            {activeTab === ProfileTabIds.Dealers && currentDealer && (
                <>
                    <HeaderTitle>Контрагенты</HeaderTitle>
                    <Dealers />
                </>
            )}
        </div>
    )
}
