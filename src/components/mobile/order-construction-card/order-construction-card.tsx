import * as React from 'react'
import {
    useShowRealCost,
    useIsSubdealerOrder,
    useUpdateCurrentOrder,
    useSaveOrder,
    useSetCurrentConstruction,
    useAddNewConstruction
} from '../../../hooks/orders'
import {Order} from '../../../model/framer/order'
import {Construction} from '../../../model/framer/construction'
import {getCurrency} from '../../../helpers/currency'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ReactComponent as CacheIconSmall} from '../../../resources/images/cache-icon-small.inline.svg'
import {ReactComponent as CacheIconSmallGrey} from '../../../resources/images/cache-icon-small-grey.inline.svg'
import {VisibilityButton} from '../../common/button/visibility-button/visibility-button'
import {ConstructionGoodsAndServices} from '../construction-goods-and-services/construction-goods-and-services'
import {ConstructionRemoveButton} from '../../common/construction-remove-button/construction-remove-button'
import styles from './order-construction-card.module.css'
import {clamp} from '../../../helpers/number'
import {СopyingButton} from '../../common/button/copying-button/copying-button'
import {GUID} from '../../../helpers/guid'
import {useCallback} from 'react'

interface IOrderConstructionCard {
    order: Order
    construction: Construction
}

export function OrderConstructionCard(props: IOrderConstructionCard): JSX.Element {
    const {order, construction} = props

    const setCurrentConstruction = useSetCurrentConstruction()
    const updateCurrentOrder = useUpdateCurrentOrder()
    const getIsSubdealerOrder = useIsSubdealerOrder()
    const saveOrder = useSaveOrder()
    const addNewConstruction = useAddNewConstruction()

    const isSubdealerOrder = getIsSubdealerOrder(order)
    const [shouldShowRealCost, setShouldShowRealCost] = useShowRealCost()

    const copyingConstruction = useCallback(
        (construction: Construction) => {
            if (!order) {
                return
            }
            const productIdOld: string[] = []
            const productIdNew: string[] = []
            const beamOld: string[] = []
            const beamNew: string[] = []
            const connectorOld: string[] = []
            const connectorNew: string[] = []

            construction.id = GUID()
            construction.products.map((product, idx) => {
                productIdOld[idx] = product.productGuid
                product.productGuid = GUID()
                productIdNew[idx] = product.productGuid
                product.frame.frameFillingGuid = GUID()
                product.frame.frameBeams.forEach((beam, idx) => {
                    beamOld[idx] = beam.beamGuid
                    beam.beamGuid = GUID()
                    beamNew[idx] = beam.beamGuid
                    beam.parentNodeId = product.frame.frameFillingGuid
                })
                product.connectors.forEach((connector, idx) => {
                    connectorOld[idx] = connector.connectedBeamGuid
                    beamOld.forEach((item, idx) => {
                        if (connector.connectedBeamGuid === item) {
                            connector.connectedBeamGuid = beamNew[idx]
                        }
                    })
                    connector.parentNodeId = product.productGuid
                    connector.beamGuid = GUID()
                    connectorNew[idx] = connector.beamGuid
                    connectorOld.forEach((item, idx) => {
                        if (connector.connectedBeamGuid === item) {
                            connector.connectedBeamGuid = connectorNew[idx - 1]
                        }
                    })
                })
                product.frame.impostBeams.forEach(impostBeam => {
                    impostBeam.beamGuid = GUID()
                    impostBeam.parentNodeId = product.frame.frameFillingGuid
                })
                product.frame.innerFillings.forEach(innerFilling => {
                    if (innerFilling.solid) {
                        innerFilling.solid.solidFillingGuid = GUID()
                        innerFilling.solid.parentNodeId = product.frame.frameFillingGuid
                    }
                    if (innerFilling.leaf) {
                        innerFilling.leaf.frameFillingGuid = GUID()
                        innerFilling.leaf.innerFillings.forEach(innerFilling => {
                            if (innerFilling.solid) {
                                innerFilling.solid.solidFillingGuid = GUID()
                                if (innerFilling.leaf) innerFilling.solid.parentNodeId = innerFilling.leaf.frameFillingGuid
                            }
                        })
                    }
                })
            })
            construction.products.forEach(product => {
                product.connectors.forEach(connector => {
                    productIdOld.forEach((item, idx) => {
                        if (connector.connectedProductGuid === item) {
                            connector.connectedProductGuid = productIdNew[idx]
                        }
                    })
                })
            })
            if (order) {
                construction.position = order.constructions.length + 1
            }
            addNewConstruction(construction)
        },
        [addNewConstruction]
    )

    const copyConstruction = (): void => {
        order.constructions.length
        const constructionClone = construction.clone
        copyingConstruction(constructionClone)
    }

    return (
        <div>
            <ul className={styles.details}>
                <ConstructionRemoveButton
                    className={styles.constructionRemoveButton}
                    order={order}
                    construction={construction}
                    onChangeOrder={newOrder => {
                        const lastIndex = order.constructions.findIndex(c => c.nodeId === construction?.nodeId) ?? -1
                        const nextConstruction = newOrder.constructions[clamp(lastIndex - 1, 0, newOrder.constructions.length - 1)]

                        setCurrentConstruction(nextConstruction)
                        updateCurrentOrder(newOrder)
                        saveOrder(newOrder)
                    }}
                />
                <СopyingButton
                    onClick={copyConstruction}
                    className={styles.constructionCopyingButton}
                    dataTest="copy-construction"
                    dataId="copy-construction"
                />
                {construction.energyProductTypeName && <li>Комплектация: {construction.energyProductTypeName}</li>}
                <li>
                    Профиль:{' '}
                    {construction.products[0].profile.displayName
                        ? construction.products[0].profile.displayName
                        : construction.products[0].profile.name}
                </li>
                <li>Фурнитура: {construction.products[0].furniture.displayName}</li>
                <li>Заполнение: {[...new Set(construction.products[0].frame.allSolidFillings.map(f => f.fillingVendorCode))].join(', ')}</li>
                <li>
                    {construction.size.width} x {construction.size.height} мм
                </li>
                <li>
                    {construction.weight} кг, {construction.square} кв.м
                </li>
                <li className={styles.constructionCost}>
                    <div>
                        <p>
                            Цена: {getCurrency(construction.totalCost / construction.quantity)}&nbsp;
                            {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}{' '}
                            {shouldShowRealCost && (
                                <span className={styles.grey}>
                                    {isSubdealerOrder && order.filial?.isExternal ? (
                                        <>
                                            / {getCurrency(construction.amegaCostToDisplay / construction.quantity)}&nbsp;
                                            {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL} (
                                            {getCurrency(construction.amegaCost / construction.quantity)}&nbsp;{RUB_SYMBOL})
                                        </>
                                    ) : (
                                        <>
                                            ({getCurrency(construction.amegaCostToDisplay / construction.quantity)}&nbsp;
                                            {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                        </>
                                    )}
                                </span>
                            )}
                        </p>
                        <p>
                            Сумма: {getCurrency(construction.totalCost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}{' '}
                            {shouldShowRealCost && (
                                <span className={styles.grey}>
                                    {isSubdealerOrder && order.filial?.isExternal ? (
                                        <>
                                            / {getCurrency(construction.amegaCostToDisplay)}&nbsp;
                                            {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL} (
                                            {getCurrency(construction.amegaCost)}
                                            &nbsp;{order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                        </>
                                    ) : (
                                        <>
                                            ({getCurrency(construction.amegaCostToDisplay)}&nbsp;
                                            {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                        </>
                                    )}
                                </span>
                            )}
                        </p>
                    </div>
                    <VisibilityButton
                        className={styles.visibilityButton}
                        isVisible={shouldShowRealCost}
                        onClick={() => setShouldShowRealCost(!shouldShowRealCost)}
                    />
                </li>
            </ul>
            <ConstructionGoodsAndServices order={order} construction={construction} shouldShowRealCost={shouldShowRealCost} />
        </div>
    )
}
