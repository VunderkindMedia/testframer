import * as React from 'react'
import {HINTS} from '../../../constants/hints'
import {Order} from '../../../model/framer/order'
import {ShippingTypes} from '../../../model/framer/shipping-types'
import {getDateDDMonthFormat, getDateDDMonthYYYYFormat} from '../../../helpers/date'
import {getCurrency} from '../../../helpers/currency'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ReactComponent as CacheIcon} from '../../../resources/images/cache-icon.inline.svg'
import {WithHint} from '../../common/with-hint/with-hint'
import styles from './order-date-price-info.module.css'

interface IOrderDatePriceInfoProps {
    order: Order
    priceTitle: string
    className?: string
}

export function OrderDatePriceInfo(props: IOrderDatePriceInfoProps): JSX.Element {
    const {order, priceTitle, className} = props
    const {estShippingDate, shippingDate, shippingType, shippingAddress, warehouse, contactPerson, windrawOrderNumber} = order

    const date = shippingDate.length > 0 ? shippingDate : estShippingDate
    const isEstimated = shippingDate.length === 0

    const shippingHint = isEstimated ? HINTS.orderShippingOrientalDate : HINTS.orderShippingExactDate

    let shippingInfo: JSX.Element | null = null

    if (shippingType === ShippingTypes.Pickup && warehouse) {
        shippingInfo = (
            <WithHint hint={HINTS.orderWarehouseAddress}>
                <p className={styles.text}>
                    Склад самовывоза:&nbsp;
                    <span>{date.length > 0 ? `${getDateDDMonthYYYYFormat(new Date(date))}, ${warehouse.name}` : '~'}</span>
                </p>
            </WithHint>
        )
    } else if (shippingType === ShippingTypes.Delivery && shippingAddress && !shippingAddress.isEmpty) {
        shippingInfo = (
            <>
                <WithHint hint={HINTS.orderShippingAddress}>
                    <p className={styles.text}>
                        Адрес доставки:&nbsp;
                        <span>{date.length > 0 ? `${getDateDDMonthYYYYFormat(new Date(date))}, ${shippingAddress.toString()}` : '~'}</span>
                    </p>
                </WithHint>
                <p className={styles.text}>
                    Получатель:&nbsp;
                    <span>
                        {contactPerson?.name}, {contactPerson?.phone}
                    </span>
                </p>
            </>
        )
    }

    return (
        <div className={className ?? ''}>
            <WithHint hint={HINTS.orderFinalPrice}>
                <p className={styles.text}>
                    {priceTitle}:&nbsp;
                    {order.totalCost === 0 ? (
                        <>
                            <span className="number-filter">&mdash;</span>
                        </>
                    ) : (
                        <>
                            <span className="number-filter">{getCurrency(order.totalCost)}</span>{' '}
                            <span>{order.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}</span>
                        </>
                    )}
                </p>
            </WithHint>
            {windrawOrderNumber && shippingInfo ? (
                shippingInfo
            ) : (
                <WithHint hint={shippingHint}>
                    <p className={styles.text}>
                        Дата готовности:&nbsp;
                        <span>
                            {date.length === 0 ? (
                                <>&mdash;</>
                            ) : (
                                <>
                                    {isEstimated ? '~' : ''} {getDateDDMonthFormat(new Date(date))}
                                </>
                            )}
                        </span>
                    </p>
                </WithHint>
            )}
        </div>
    )
}
