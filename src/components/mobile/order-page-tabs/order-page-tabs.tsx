import * as React from 'react'
import styles from './order-page-tabs.module.css'
import {useCallback, useEffect, useRef, useState} from 'react'
import {getClassNames} from '../../../helpers/css'
import {clamp} from '../../../helpers/number'
import {ITab} from '../../desktop/tabs-panel/tabs-panel'
import {AddButton} from '../../common/button/add-button/add-button'

const WIDTH = 52

interface IOrderPageTabsProps {
    tabs: ITab[]
    activeTabIndex: number
    hasAdditionTab: boolean
    onUpdateTabs: (tabs: ITab[]) => void
    onAddTab?: () => void
}

export function OrderPageTabs(props: IOrderPageTabsProps): JSX.Element {
    const {tabs, activeTabIndex, hasAdditionTab, onUpdateTabs, onAddTab} = props

    const containerRef = useRef<HTMLDivElement>(null)

    const tabsCount = hasAdditionTab ? tabs.length + 1 : tabs.length

    const [itemWidth, setItemWidth] = useState(WIDTH)
    const [visibleCount, setVisibleCount] = useState(1)
    const [offset, setOffset] = useState(0)

    useEffect(() => {
        const visibleCount = containerRef.current ? Math.floor(containerRef.current.clientWidth / itemWidth) : 4
        setVisibleCount(visibleCount)
    }, [itemWidth])

    useEffect(() => {
        if (visibleCount === 1) {
            return
        }

        if (tabsCount > visibleCount && containerRef.current) {
            const availableWidth = containerRef.current.clientWidth
            const width = availableWidth / Math.floor(containerRef.current.clientWidth / WIDTH)
            setItemWidth(width)
        }
    }, [visibleCount, tabsCount])

    const scrollTo = useCallback(
        (value: number) => {
            if (!containerRef.current) {
                return
            }

            const minScrollValue = -(tabsCount * itemWidth - containerRef.current.clientWidth)

            const val = clamp(value, minScrollValue, 0)
            setOffset(val)
        },
        [tabsCount, itemWidth]
    )

    useEffect(() => {
        if (tabsCount <= visibleCount) {
            setOffset(0)
        } else {
            scrollTo(-activeTabIndex * itemWidth)
        }
    }, [tabsCount, visibleCount, activeTabIndex, scrollTo, itemWidth])

    const prev = useCallback((): void => {
        scrollTo(offset + visibleCount * itemWidth)
    }, [scrollTo, offset, visibleCount, itemWidth])

    const next = useCallback((): void => {
        scrollTo(offset - visibleCount * itemWidth)
    }, [scrollTo, offset, visibleCount, itemWidth])

    return (
        <div className={getClassNames(styles.container, {[styles.scrollable]: tabsCount > visibleCount})}>
            <button className={`${styles.navButton} ${styles.prevButton}`} onClick={prev} />
            <div className={styles.tabsWrapper} ref={containerRef}>
                <div className={styles.tabs} style={{transform: `translateX(${offset}px)`}}>
                    {tabs.map(tab => (
                        <div
                            key={tab.id}
                            onClick={() => {
                                const newTabs = tabs.slice(0)
                                newTabs.forEach(t => (t.active = t.id === tab.id))
                                onUpdateTabs(newTabs)
                            }}
                            className={getClassNames(styles.tab, {[styles.active]: Boolean(tab.active)})}
                            style={{minWidth: itemWidth}}>
                            {tab.active ? tab.activeIcon : tab.icon}
                        </div>
                    ))}
                    {hasAdditionTab && <AddButton className={styles.addConstruction} style="primary" onClick={onAddTab} />}
                </div>
            </div>
            <button className={`${styles.navButton} ${styles.nextButton}`} onClick={next} />
        </div>
    )
}
