import * as React from 'react'
import styles from './construction-goods-and-services.module.css'
import {getCurrency} from '../../../helpers/currency'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ReactComponent as CacheIconSmall} from '../../../resources/images/cache-icon-small.inline.svg'
import {ReactComponent as CacheIconSmallGrey} from '../../../resources/images/cache-icon-small-grey.inline.svg'
import {EditButton} from '../../common/button/edit-button/edit-button'
import {ConfigContexts} from '../../../model/config-contexts'
import {GoodSources} from '../../../model/framer/good-sources'
import {Order} from '../../../model/framer/order'
import {Construction} from '../../../model/framer/construction'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useSetConfigContext} from '../../../hooks/builder'
import {ConstructionGoodsAndServicesSheets} from '../../common/construction-goods-and-services-sheets/construction-goods-and-services-sheets'

interface IConstructionGoodsAndServicesProps {
    order: Order
    construction: Construction
    shouldShowRealCost: boolean
}

export function ConstructionGoodsAndServices(props: IConstructionGoodsAndServicesProps): JSX.Element {
    const {order, construction, shouldShowRealCost} = props

    const allFactoryGoods = useSelector((state: IAppState) => state.goods.factoryGoods)
    const allPartnerGoods = useSelector((state: IAppState) => state.goods.partnerGoods)

    const setConfigContext = useSetConfigContext()

    const constructionGoods = order.goods.filter(g => g.constructionId === construction.id)
    const constructionServices = order.services.filter(s => s.constructionId === construction.id)
    const hasAvailableGoods = allFactoryGoods.length > 0 || allPartnerGoods.length > 0

    const goodsCost = constructionGoods.reduce((s, g) => s + g.cost, 0)
    const goodsAmegaCostToDisplay = constructionGoods.reduce((s, g) => s + g.amegaCostToDisplay, 0)
    const servicesCost = constructionServices.reduce((s, service) => s + service.price, 0)

    return (
        <>
            <div data-id="construction-goods-services">
                <div>
                    <p className={styles.title}>
                        Доп. материалы{' '}
                        {goodsCost ? (
                            <span className={styles.totalCost}>
                                {getCurrency(goodsCost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}{' '}
                                {shouldShowRealCost && (
                                    <span className={styles.grey}>
                                        ({getCurrency(goodsAmegaCostToDisplay)}{' '}
                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                    </span>
                                )}
                            </span>
                        ) : (
                            ''
                        )}
                        <EditButton
                            isDisabled={order.readonly || (constructionGoods.length === 0 && !hasAvailableGoods)}
                            onClick={() => setConfigContext(ConfigContexts.Goods)}
                        />
                    </p>
                    {constructionGoods.length > 0 && (
                        <ul className={styles.details}>
                            {constructionGoods.map((good, idx) => (
                                <li key={good.id} className={styles.addition}>
                                    <span>
                                        {idx + 1}. {good.name}
                                        {good.attributes.length === 1 && good.attributes[0].name === 'thick'
                                            ? ` (${good.attributes[0].value}мм) `
                                            : good.attributes.length === 2 &&
                                              good.attributes[0].name === 'width' &&
                                              good.attributes[1].name === 'height'
                                            ? ` (${good.attributes[0].value}мм x ${good.attributes[1].value}мм) `
                                            : ''}
                                        x {good.quantity}
                                        {shouldShowRealCost && good.goodSource === GoodSources.Factory && (
                                            <span className={styles.grey}>
                                                ({getCurrency(good.amegaCostToDisplay / good.quantity)}&nbsp;
                                                {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                            </span>
                                        )}
                                    </span>
                                    <span className={styles.additionCost}>
                                        {getCurrency(good.cost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}{' '}
                                        {shouldShowRealCost && good.goodSource === GoodSources.Factory && (
                                            <span className={styles.grey}>
                                                ({getCurrency(good.amegaCostToDisplay)}{' '}
                                                {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                            </span>
                                        )}
                                    </span>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
                <div className={styles.services}>
                    <p className={styles.title}>
                        Услуги
                        {servicesCost ? (
                            <span className={styles.totalCost}>
                                {getCurrency(servicesCost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                            </span>
                        ) : (
                            ''
                        )}
                        <EditButton isDisabled={order.readonly} onClick={() => setConfigContext(ConfigContexts.Services)} />
                    </p>
                    {constructionServices.length > 0 && (
                        <ul className={styles.details}>
                            {constructionServices.map((service, idx) => (
                                <li key={`${service.id}-${idx}`} className={styles.addition}>
                                    <span>
                                        {idx + 1}. {service.name}
                                    </span>
                                    <span className={styles.additionCost}>
                                        {getCurrency(service.price)}&nbsp;{order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                                    </span>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
            </div>
            <ConstructionGoodsAndServicesSheets />
        </>
    )
}
