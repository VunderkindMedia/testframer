import * as React from 'react'
import {useState, useCallback, useRef} from 'react'
import {Order} from '../../../model/framer/order'
import {
    useDuplicateOrder,
    useIsSubdealerOrder,
    useLoadOffer,
    useRecalculateOrder,
    useRemoveOrderModal,
    useLoadOrderXml,
    useIsNewOrderAvailable,
    useCancelOrderModal
} from '../../../hooks/orders'
import {getOrderStateName, OrderStates} from '../../../model/framer/order-states'
import {getCurrency} from '../../../helpers/currency'
import {getAmegaCostTooltip} from '../../../helpers/account'
import {WithTitle} from '../../common/with-title/with-title'
import {VisibilityButton} from '../../common/button/visibility-button/visibility-button'
import {SvgIconConstructionDrawer} from '../../common/svg-icon-construction-drawer/svg-icon-construction-drawer'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {ErrorTypes} from '../../../model/framer/error-types'
import {Colors} from '../../../constants/colors'
import {Action} from '../../../model/action'
import {getOrderLink} from '../../../helpers/orders'
import {useGoTo} from '../../../hooks/router'
import {Modal} from '../../common/modal/modal'
import {AnalyticsManager} from '../../../analytics-manager'
import {useIsExternalUser, usePermissions, useIsExportToXmlAvailable, useIsDemoUser} from '../../../hooks/permissions'
import {NAV_BOOKING} from '../../../constants/navigation'
import {InsufficientFundsModal, useShouldShowInsufficientFundsModal} from '../../common/insufficient-funds-modal/insufficient-funds-modal'
import {ListItem, ListItemBody, ListItemHeader} from '../list-item'
import {DropdownMenu} from '../../common/dropdown-menu/dropdown-menu'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ArrowTooltip} from '../../common/arrow-tooltip/arrow-tooltip'
import {ReactComponent as CacheIcon} from '../../../resources/images/cache-icon.inline.svg'
import styles from './order-list-item.module.css'

interface IOrderListItemProps {
    order: Order
    shouldShowRealCost: boolean
    isFramerPointsAvailable: boolean
    onShowRealCost: (show: boolean) => void
}

export function OrderListItem(props: IOrderListItemProps): JSX.Element {
    const {order, shouldShowRealCost, isFramerPointsAvailable, onShowRealCost} = props

    const [modalModel, showModal] = useRemoveOrderModal(order)
    const [cancelModalModel, showCancelModal] = useCancelOrderModal(order)
    const [isVisibleInsufficientFundsModal, setIsVisibleInsufficientFundsModal] = useState(false)
    const tooltipRef = useRef<HTMLDivElement | null>(null)

    const goTo = useGoTo()
    const recalculateOrder = useRecalculateOrder()
    const getIsSubdealerOrder = useIsSubdealerOrder()
    const loadOffer = useLoadOffer()
    const loadOrderXml = useLoadOrderXml()
    const isSubdealerOrder = getIsSubdealerOrder(order)
    const isExternalUser = useIsExternalUser()
    const isExportToXmlAvailable = useIsExportToXmlAvailable()
    const shouldShowInsufficientFundsModal = useShouldShowInsufficientFundsModal(order)
    const duplicateOrder = useDuplicateOrder()
    const permission = usePermissions()
    const isDemoUser = useIsDemoUser()
    const isNewOrderAvailable = useIsNewOrderAvailable()

    const goToOrder = useCallback(() => {
        goTo(getOrderLink(order))
    }, [order, goTo])

    const items = [new Action('Редактировать', goToOrder)]

    if (isNewOrderAvailable) {
        items.push(
            new Action('Повторить заказ', () => {
                duplicateOrder(order.id)
            })
        )
    }

    if ([OrderStates.CalcErrors, OrderStates.NeedsRecalc, OrderStates.Draft].includes(order.state) && !order.readonly) {
        items.push(
            new Action('Пересчитать', () => {
                recalculateOrder(order)
            })
        )
    }
    if (!order.readonly) {
        items.push(new Action('Удалить', showModal))
    }
    if (order.totalAmegaCostToDisplay !== 0 && !permission.isNonQualified) {
        items.push(
            new Action('Скачать КП', () => {
                loadOffer(order)
                AnalyticsManager.trackClickDownloadKpButtonFromOrdersPage()
                order.addAction(`Скачали КП в заказе ${order.id}`)
            })
        )
    }
    if (order.producible && !isExternalUser && !permission.isNonQualified && !isDemoUser && !permission.isCommonManager) {
        items.push(
            new Action('Передать в производство', () => {
                if (shouldShowInsufficientFundsModal) {
                    setIsVisibleInsufficientFundsModal(true)
                } else {
                    goTo(`${NAV_BOOKING}?orderId=${order.id}`)
                }
                AnalyticsManager.trackClickTransferToProductionButton()
            })
        )
    }
    if (isExportToXmlAvailable) {
        items.push(
            new Action('Сохранить XML', () => {
                loadOrderXml(order)
            })
        )
    }

    if (order.isCancelAvailable) {
        items.push(
            new Action('Отменить заказ', () => {
                showCancelModal()
            })
        )
    }

    const openOrder = useCallback(
        (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
            if (tooltipRef.current) {
                const target = e.target as HTMLElement
                !tooltipRef.current.contains(target) && goToOrder()
            } else {
                goToOrder()
            }
        },
        [goToOrder, tooltipRef]
    )

    return (
        <>
            {modalModel && <Modal model={modalModel} />}
            {cancelModalModel && <Modal model={cancelModalModel} />}
            {isVisibleInsufficientFundsModal && <InsufficientFundsModal onClose={() => setIsVisibleInsufficientFundsModal(false)} />}
            <ListItem state={order.state} onClick={openOrder}>
                <ListItemHeader>
                    <p className={styles.orderNumber}>{order.orderNumber}</p>
                    <p className={styles.orderState}>{getOrderStateName(order.state)}</p>
                    <DropdownMenu items={items} />
                </ListItemHeader>
                <ListItemBody isEnergy={order.haveEnergyProducts}>
                    <div className={styles.bodyRow}>
                        <WithTitle title="Сумма" className={styles.sum}>
                            <div className={styles.price}>
                                {order.totalCost === 0 ? (
                                    <span>&mdash;</span>
                                ) : (
                                    <>
                                        <span className={styles.black}>
                                            {getCurrency(order.totalCost)}{' '}
                                            <span className={styles.currency}>
                                                {order.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                                            </span>
                                        </span>

                                        {shouldShowRealCost ? (
                                            isSubdealerOrder && order.filial?.isExternal ? (
                                                <div className={styles.amegaPrice} ref={tooltipRef}>
                                                    {' / '}
                                                    {getCurrency(order.totalAmegaCostToDisplay)}{' '}
                                                    <span className={styles.currency}>
                                                        {order.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                                                    </span>
                                                    {' / '}
                                                    {isFramerPointsAvailable ? (
                                                        <ArrowTooltip title={getAmegaCostTooltip(order, true)}>
                                                            <span>
                                                                {getCurrency(order.totalAmegaCost)}{' '}
                                                                <span className={styles.currency}>
                                                                    {order.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                                                                </span>{' '}
                                                            </span>
                                                        </ArrowTooltip>
                                                    ) : (
                                                        <>
                                                            {getCurrency(order.totalAmegaCost)}{' '}
                                                            <span className={styles.currency}>
                                                                {order.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                                                            </span>{' '}
                                                        </>
                                                    )}
                                                </div>
                                            ) : (
                                                <div className={styles.amegaPrice} ref={tooltipRef}>
                                                    {' / '}
                                                    {isFramerPointsAvailable ? (
                                                        <ArrowTooltip title={getAmegaCostTooltip(order, false)}>
                                                            <span>
                                                                {getCurrency(order.totalAmegaCostToDisplay)}
                                                                <span className={styles.currency}>
                                                                    {' '}
                                                                    {order.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                                                                </span>
                                                            </span>
                                                        </ArrowTooltip>
                                                    ) : (
                                                        <>
                                                            {getCurrency(order.totalAmegaCostToDisplay)}{' '}
                                                            <span className={styles.currency}>
                                                                {order.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                                                            </span>
                                                        </>
                                                    )}
                                                </div>
                                            )
                                        ) : (
                                            ''
                                        )}
                                    </>
                                )}
                            </div>
                        </WithTitle>
                        <VisibilityButton
                            className={styles.costVisibilityButton}
                            isVisible={shouldShowRealCost}
                            onClick={e => {
                                e.stopPropagation()
                                onShowRealCost(!shouldShowRealCost)
                            }}
                        />
                    </div>
                    <div className={`${styles.bodyRow} ${styles.constructionsRow}`}>
                        {order.constructions.map(construction => {
                            const error = construction.errors.filter(e => e.errorType === ErrorTypes.Error)
                            return (
                                <SvgIconConstructionDrawer
                                    key={construction.id}
                                    construction={construction}
                                    maxWidth={Infinity}
                                    maxHeight={24}
                                    lineColor={error.length > 0 ? Colors.Red : Colors.Gray}
                                />
                            )
                        })}
                    </div>
                    {order.windrawOrderNumber && (
                        <div className={styles.bodyRow}>
                            <WithTitle title="Номер производства">
                                <p className={styles.number}>{order.windrawOrderNumber}</p>
                                {order.windrawGoodsOrderNumber && <p className={styles.number}>{order.windrawGoodsOrderNumber}</p>}
                            </WithTitle>
                        </div>
                    )}
                    <div className={`${styles.bodyRow} ${styles.datesRow}`}>
                        <WithTitle title="Дата заказа">
                            <p className={styles.date}>{getDateDDMMYYYYFormat(new Date(order.createdAt))}</p>
                        </WithTitle>
                        <WithTitle title="Дата готовности">
                            <p className={styles.date}>
                                {order.shippingDate.length > 0 ? (
                                    `${getDateDDMMYYYYFormat(new Date(order.shippingDate))}`
                                ) : (
                                    <>
                                        {order.estShippingDate.length === 0 ? (
                                            <>&mdash;</>
                                        ) : (
                                            <> ~ {getDateDDMMYYYYFormat(new Date(order.estShippingDate))}</>
                                        )}
                                    </>
                                )}
                            </p>
                        </WithTitle>
                    </div>
                    <div className={styles.bodyRow}>
                        <WithTitle title="Клиент">
                            <p className={styles.clientName}>{order.client?.name ?? <>&mdash;</>}</p>
                        </WithTitle>
                    </div>
                </ListItemBody>
            </ListItem>
        </>
    )
}
