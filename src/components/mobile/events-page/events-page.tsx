import * as React from 'react'
import {useState} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useEventsFromSearchByDay, useSetEventsFilters} from '../../../hooks/events'
import {getEventsFilterParams} from '../../../helpers/events'
import {CalendarList} from '../../common/calendar-list/calendar-list'
import {getDayEndDate, getDateDDMMYYYYFormat} from '../../../helpers/date'
import {HeaderTitle} from '../header/header-title'
import {BottomSheet} from '../../common/bottom-sheet/bottom-sheet'
import {EventCard} from '../event-card/event-card'
import {EventsFilter} from '../events-filter/events-filter'
import {CalendarEvent} from '../../../model/framer/calendar/calendar-event'
import styles from './events-page.module.css'

export const EventsPage = (): JSX.Element => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const [selectedDate, setSelectedDate] = useState<Date | null>(null)

    const events = useEventsFromSearchByDay()
    const filterParams = getEventsFilterParams(search)
    const setMountingsFilter = useSetEventsFilters()

    const getDayEvents = React.useCallback(
        (date: Date | null): CalendarEvent[] => {
            if (!date) {
                return []
            }
            const dateStr = getDateDDMMYYYYFormat(date)
            return events.get(dateStr) ?? []
        },
        [events]
    )

    const dayEvents = getDayEvents(selectedDate)

    return (
        <div className={styles.page}>
            <HeaderTitle>Календарь</HeaderTitle>
            <div className={styles.content}>
                <EventsFilter filterParams={filterParams} onChangeFilterParams={setMountingsFilter} />
                <CalendarList
                    startDate={filterParams.from}
                    endDate={getDayEndDate(filterParams.to)}
                    events={events}
                    onClick={date => {
                        getDayEvents(date).length && setSelectedDate(date)
                    }}
                />
            </div>
            <BottomSheet
                isVisible={!!selectedDate}
                onHide={() => {
                    setSelectedDate(null)
                }}>
                <EventCard events={dayEvents ?? []} date={selectedDate} onHide={() => setSelectedDate(null)} />
            </BottomSheet>
        </div>
    )
}
