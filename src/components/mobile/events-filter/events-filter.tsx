import * as React from 'react'
import {WithTitle} from '../../common/with-title/with-title'
import {Select} from '../../common/select/select'
import {ListItem} from '../../../model/list-item'
import {useEmployees, useAffiliates} from '../../../hooks/account'
import {useClients} from '../../../hooks/clients'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'
import {IEventsFilterParams} from '../../../helpers/events'
import {MonthSelector} from '../../common/month-selector/month-selector'
import {getMonthEndDate, getMonthStartDate} from '../../../helpers/date'
import styles from './events-filter.module.css'

interface IEventsFilterProps {
    filterParams: IEventsFilterParams
    className?: string
    onChangeFilterParams: (params: IEventsFilterParams) => void
}

export const EventsFilter = (props: IEventsFilterProps): JSX.Element => {
    const {filterParams, className, onChangeFilterParams} = props
    const {filials, clients, owners} = filterParams

    const employeesList = useEmployees()
    const affiliatesList = useAffiliates()
    const clientsList = useClients()

    const affiliatesListItems = affiliatesList?.length
        ? [new ListItem('Все', 'Все', -1), ...affiliatesList.map(({id, name}) => new ListItem(String(id), name, id))]
        : []

    const clientsListItems = [new ListItem('Все', 'Все', -1), ...clientsList.map(({id, name}) => new ListItem(String(id), name, id))]
    const employeesListItems = employeesList?.length
        ? [new ListItem('Все', 'Все', -1), ...employeesList.map(({id, name}) => new ListItem(String(id), name, id))]
        : []

    return (
        <div className={className ?? ''}>
            {affiliatesListItems.length > 0 && (
                <WithTitle title="Филиал" className={styles.filter}>
                    <Select<number | undefined>
                        isMultiSelect={true}
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        items={affiliatesListItems}
                        selectedItems={
                            filials?.length ? affiliatesListItems.filter(fl => fl.data && filials.includes(fl.data)) : [affiliatesListItems[0]]
                        }
                        onMultiSelect={items => {
                            const filialIds: number[] = []
                            items.map(item => {
                                if (item.data && item.data !== -1) {
                                    filialIds.push(item.data)
                                }
                            })
                            onChangeFilterParams({...filterParams, filials: filialIds})
                        }}
                    />
                </WithTitle>
            )}
            {employeesListItems.length > 0 && (
                <WithTitle title="Менеджер" className={styles.filter}>
                    <Select<number | undefined>
                        items={employeesListItems}
                        isMultiSelect={true}
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        selectedItems={
                            owners.length ? employeesListItems.filter(e => e.data && owners.includes(e.data)) : [employeesListItems[0]]
                        }
                        onMultiSelect={items => {
                            const employeesIds: number[] = []
                            items.map(({data}) => {
                                if (data && data !== -1) {
                                    employeesIds.push(data)
                                }
                            })
                            onChangeFilterParams({...filterParams, owners: employeesIds})
                        }}
                    />
                </WithTitle>
            )}
            {clientsListItems.length > 0 && (
                <WithTitle title="Клиент" className={styles.filter}>
                    <Select<number | undefined>
                        items={clientsListItems}
                        isMultiSelect={true}
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        shouldShowFilter={true}
                        selectedItems={
                            clients.length ? clientsListItems.filter(({data}) => data && clients.includes(data)) : [clientsListItems[0]]
                        }
                        onMultiSelect={items => {
                            const clientIds: number[] = []
                            items.map(item => {
                                if (item.data && item.data !== -1) {
                                    clientIds.push(item.data)
                                }
                            })
                            onChangeFilterParams({...filterParams, clients: clientIds})
                        }}
                    />
                </WithTitle>
            )}
            <WithTitle title="Месяц" className={styles.filter}>
                <MonthSelector
                    date={filterParams.from}
                    onMonthChange={date => {
                        onChangeFilterParams({...filterParams, from: getMonthStartDate(date), to: getMonthEndDate(date)})
                    }}
                    className={styles.monthSelector}
                />
            </WithTitle>
        </div>
    )
}
