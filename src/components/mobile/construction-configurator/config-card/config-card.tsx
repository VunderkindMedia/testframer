import * as React from 'react'
import {useEffect, useState} from 'react'
import {Card} from '../../../common/card/card'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {getConfiguratorViewWithParamsByConfigContext, IConfiguratorViewWithParams} from '../../../../helpers/builder'
import {useSetConfigContext} from '../../../../hooks/builder'
import {BottomSheet} from '../../../common/bottom-sheet/bottom-sheet'
import {CardHeader} from '../../../common/card/card-header/card-header'
import styles from './config-card.module.css'

export function ConfigCard(): JSX.Element {
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)

    const [isVisible, setIsVisible] = useState(false)
    const [config, setConfig] = useState<IConfiguratorViewWithParams | null>(null)

    const setConfigContext = useSetConfigContext()

    useEffect(() => {
        const config = getConfiguratorViewWithParamsByConfigContext(currentProduct, currentContext, configContext, true)

        if (!config) {
            setIsVisible(false)
            return
        }

        setConfig(config)
        setIsVisible(true)
    }, [currentProduct, currentContext, configContext])

    const currentSubView = config?.subView?.find(s => s.context === configContext)
    const isSubView = currentSubView && currentSubView.context === configContext

    return (
        <BottomSheet
            isVisible={isVisible}
            onHide={() => {
                setConfigContext(null)
                setIsVisible(false)
            }}>
            <Card className={`${styles.card} ${isSubView ? styles.subview : ''}`}>
                {currentSubView && isSubView ? (
                    <>
                        <CardHeader
                            title={currentSubView.title ?? ''}
                            hasNavigation={true}
                            onClick={() => {
                                currentSubView.rootContext && setConfigContext(currentSubView.rootContext)
                            }}
                        />
                        <div className={styles.content}>{currentSubView.view}</div>
                    </>
                ) : (
                    <>
                        <CardHeader title={config?.title ?? ''} onClick={() => setIsVisible(false)} />
                        <div className={styles.content}>{config?.view}</div>
                    </>
                )}
            </Card>
        </BottomSheet>
    )
}
