import * as React from 'react'
import {useCallback, useRef} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {getOrderPageUrl, getConstructionPosition} from '../../../helpers/orders'
import {NewBuilderPageSidebarItem} from '../../desktop/new-builder-page-sidebar/new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {MainWidgets, useMainWidget} from '../../desktop/new-builder-page-sidebar/new-builder-page-sidebar'
import {ConstructionInfoFragment} from '../../common/builder-page-fragments/construction-info-fragment/construction-info-fragment'
import {SolidFilingInfoFragment} from '../../common/builder-page-fragments/solid-filing-info-fragment/solid-filing-info-fragment'
import {ImpostInfoFragment} from '../../common/builder-page-fragments/impost-info-fragment/impost-info-fragment'
import {FrameInfoFragment} from '../../common/builder-page-fragments/frame-info-fragment/frame-info-fragment'
import {FrameFillingInfoFragment} from '../../common/builder-page-fragments/frame-filling-info-fragment/frame-filling-info-fragment'
import {HandleInfoFragment} from '../../common/builder-page-fragments/handle-info-fragment/handle-info-fragment'
import {ConfigCard} from './config-card/config-card'
import {getClassNames} from '../../../helpers/css'
import {ConstructionRemoveButton} from '../../common/construction-remove-button/construction-remove-button'
import {useUpdateCurrentOrder} from '../../../hooks/orders'
import {useReplaceTo} from '../../../hooks/router'
import {clamp} from '../../../helpers/number'
import {DownButton} from '../../common/button/down-button/down-button'
import {ConnectorInfoFragment} from '../../common/builder-page-fragments/connector-info-fragment/connector-info-fragment'
import styles from './construction-configurator.module.css'
import {BeamInfoFragment} from '../../common/builder-page-fragments/beam-info-fragment/beam-info-fragment'
import {BackSidebarItem} from '../../desktop/new-builder-page-sidebar/back-sidebar-item/back-sidebar-item'
import {useSetCurrentContext} from '../../../hooks/builder'
import {ORDER_TAB_ID} from '../../../constants/navigation'
import {useTypeConstructionVisible} from '../../../model/framer/construction-types'

export function ConstructionConfigurator(): JSX.Element {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const containerRef = useRef<HTMLDivElement>(null)

    const setCurrentContext = useSetCurrentContext()
    const mainWidget = useMainWidget(currentConstruction, currentProduct, currentContext)
    const updateCurrentOrder = useUpdateCurrentOrder()
    const replaceTo = useReplaceTo()
    const typeConstructionFunction = useTypeConstructionVisible()

    let shouldShowBackButton = true
    let mainView: JSX.Element | null

    if (mainWidget === MainWidgets.SolidFilingInfo) {
        mainView = <SolidFilingInfoFragment />
    } else if (mainWidget === MainWidgets.FrameInfo) {
        mainView = (
            <>
                <NewBuilderPageSidebarItem key="type-constructions">
                    <span>Тип конструкции: {typeConstructionFunction}</span>
                </NewBuilderPageSidebarItem>
                <FrameInfoFragment />
            </>
        )
    } else if (mainWidget === MainWidgets.FrameFillingInfo) {
        mainView = <FrameFillingInfoFragment />
    } else if (mainWidget === MainWidgets.ImpostInfo) {
        mainView = <ImpostInfoFragment />
    } else if (mainWidget === MainWidgets.BeamInfo) {
        mainView = <BeamInfoFragment />
    } else if (mainWidget === MainWidgets.HandleInfo) {
        mainView = <HandleInfoFragment />
    } else if (mainWidget === MainWidgets.ConnectorInfo) {
        mainView = <ConnectorInfoFragment />
    } else {
        shouldShowBackButton = false
        const getTitle = (): string => {
            if (!currentConstruction) {
                return ''
            }

            const position = getConstructionPosition(currentConstruction, currentOrder?.constructions ?? [])
            if (currentConstruction.isMoskitka) {
                return `Москитная сетка ${position}`
            }
            if (currentConstruction.isEmpty) {
                return `Заполнение ${position}`
            }

            return `Конструкция ${position}`
        }

        mainView = (
            <>
                <NewBuilderPageSidebarItem className={styles.removeButton}>
                    <p>{getTitle()}</p>
                    {currentOrder && currentConstruction && (
                        <ConstructionRemoveButton
                            order={currentOrder}
                            construction={currentConstruction}
                            onChangeOrder={order => {
                                const lastIndex = currentOrder?.constructions.findIndex(c => c.nodeId === currentConstruction?.nodeId) ?? -1
                                const nextConstruction = order.constructions[clamp(lastIndex - 1, 0, order.constructions.length - 1)]

                                const tabId = nextConstruction?.nodeId ?? ORDER_TAB_ID
                                replaceTo(
                                    getOrderPageUrl(
                                        currentOrder.id,
                                        order.constructions.map(c => c.nodeId),
                                        tabId
                                    )
                                )
                                updateCurrentOrder(order)
                            }}
                        />
                    )}
                </NewBuilderPageSidebarItem>
                <ConstructionInfoFragment />
            </>
        )
    }

    const scrollToConstructionInfo = useCallback(() => {
        if (containerRef.current) {
            window.scroll({
                top: containerRef.current.offsetTop,
                behavior: 'smooth'
            })
        }
    }, [])

    return (
        <div
            className={getClassNames(styles.configurator, {[styles.readonly]: Boolean(currentOrder?.readonly)})}
            ref={containerRef}
            onClick={e => {
                if (containerRef.current !== e.target && containerRef.current?.contains(e.target as HTMLElement)) {
                    e.stopPropagation()
                }
            }}>
            {shouldShowBackButton && (
                <BackSidebarItem border="short" onClick={() => setCurrentContext(null)}>
                    Назад к общим параметрам
                </BackSidebarItem>
            )}
            {mainView}
            <ConfigCard />
            {currentContext !== null && <DownButton className={styles.scrollButton} onClick={scrollToConstructionInfo} />}
        </div>
    )
}
