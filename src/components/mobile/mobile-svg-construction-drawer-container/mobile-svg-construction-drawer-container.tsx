import * as React from 'react'
import '../../common/svg-construction-drawer-container/svg-construction-drawer-container.css'
import {useCallback, useState} from 'react'
import {Construction} from '../../../model/framer/construction'
import {Size} from '../../../model/size'
import {
    ISvgConstructionDrawerContainerProps,
    SvgConstructionDrawerContainer
} from '../../common/svg-construction-drawer-container/svg-construction-drawer-container'
import {useRef} from 'react'
import {Point} from '../../../model/geometry/point'
import {useEffect} from 'react'
import {getDimensionLinesOffsets} from '../../../helpers/line'
import {clamp} from '../../../helpers/number'
import styles from './mobile-svg-construction-drawer-container.module.css'

const LINE_OFFSET = 29
const BRACKET_WIDTH = 14
const CONTAINER_PADDING = 56

const MIN_CONSTRUCTION_HEIGHT_PX = 150

export function MobileSvgConstructionDrawerContainer(props: ISvgConstructionDrawerContainerProps): JSX.Element {
    const {currentConstruction, windowSize} = props

    const scrollableContainerRef = useRef<HTMLDivElement>(null)
    const scrollableBodyRef = useRef<HTMLDivElement>(null)

    const lastScrollPoint = useRef(new Point(0, 0))

    const getConstructionDrawerSize = useCallback(
        (construction: Construction): Size => {
            const dimensionLines = construction.getDimensionLines()
            const [verticalOffset, horizontalOffset] = getDimensionLinesOffsets(dimensionLines, LINE_OFFSET, BRACKET_WIDTH)

            const MAX_HEIGHT = windowSize.height - CONTAINER_PADDING * 2

            const containerWidth = windowSize.width - CONTAINER_PADDING * 2

            let scale =
                construction.width + verticalOffset > construction.height + horizontalOffset
                    ? (containerWidth - verticalOffset) / construction.width
                    : (containerWidth - horizontalOffset) / construction.height

            if (construction.height * scale < MIN_CONSTRUCTION_HEIGHT_PX) {
                scale = MIN_CONSTRUCTION_HEIGHT_PX / construction.height
            }
            const width = Math.floor(construction.width * scale + verticalOffset)
            const height = Math.floor(construction.height * scale + horizontalOffset)

            return new Size(clamp(width, containerWidth, width), clamp(height, height, MAX_HEIGHT))
        },
        [windowSize]
    )

    const [drawerSize, setDrawerSize] = useState(getConstructionDrawerSize(currentConstruction))

    useEffect(() => {
        setDrawerSize(getConstructionDrawerSize(currentConstruction))
    }, [currentConstruction, getConstructionDrawerSize])

    useEffect(() => {
        if (scrollableContainerRef.current && scrollableBodyRef.current) {
            const containerRect = scrollableContainerRef.current.getBoundingClientRect()
            const bodyRect = scrollableBodyRef.current.getBoundingClientRect()

            const x = Math.floor((bodyRect.width - containerRect.width) / 2)
            const y = Math.floor((bodyRect.height - containerRect.height) / 2)

            lastScrollPoint.current = new Point(x, y)
            scrollableContainerRef.current.scroll(x, y)
        }
    }, [currentConstruction?.id])

    return (
        <div className={styles.container}>
            <div
                ref={scrollableContainerRef}
                style={{
                    overflow: 'auto',
                    width: `${windowSize.width}px`,
                    height: `${drawerSize.height + CONTAINER_PADDING * 2}px`
                }}>
                <div
                    ref={scrollableBodyRef}
                    style={{
                        width: `${drawerSize.width + CONTAINER_PADDING * 2}px`,
                        height: `${drawerSize.height + CONTAINER_PADDING * 2}px`
                    }}>
                    <SvgConstructionDrawerContainer {...props} drawerSize={drawerSize} />
                </div>
            </div>
        </div>
    )
}
