import * as React from 'react'
import {useCallback, useEffect, useState} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Card} from '../../common/card/card'
import {getOrderPageUrl} from '../../../helpers/orders'
import {getTabs} from '../../../helpers/tabs'
import {useGoTo} from '../../../hooks/router'
import {OrderErrors} from '../../common/order-errors/order-errors'
import {BottomSheet} from '../../common/bottom-sheet/bottom-sheet'
import {CardHeader} from '../../common/card/card-header/card-header'
import styles from './order-errors-control.module.css'
import {ErrorTypes} from '../../../model/framer/error-types'

interface IOrderErrorsProps {
    shouldShowErrors?: boolean
}

export function OrderErrorsControl(props: IOrderErrorsProps): JSX.Element | null {
    const {shouldShowErrors = false} = props

    const search = useSelector((state: IAppState) => state.router.location.search)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)

    const goTo = useGoTo()
    const [isVisible, setIsVisible] = useState(false)
    const hide = useCallback(() => setIsVisible(false), [])

    useEffect(() => {
        if (
            shouldShowErrors &&
            currentOrder &&
            !currentOrder.changed &&
            currentOrder.constructions.some(c => c.errors.some(e => e.errorType === ErrorTypes.Error))
        ) {
            window.scroll({
                top: 0,
                behavior: 'smooth'
            })
            setIsVisible(true)
        }
    }, [currentOrder, shouldShowErrors])

    const hasErrors = currentOrder?.constructions.some(c => c.errors.length !== 0)
    if (!currentOrder || !hasErrors) {
        return null
    }

    return (
        <div
            className={styles.button}
            onClick={e => {
                e.stopPropagation()
                setIsVisible(true)
            }}>
            <div className={styles.icon} />
            <p>Ошибки и предупреждения</p>
            <BottomSheet isVisible={isVisible} onHide={hide}>
                <Card>
                    <CardHeader title="Ошибки и предупреждения" onClick={hide} />
                    <div className={styles.content}>
                        <OrderErrors
                            order={currentOrder}
                            currentConstruction={currentConstruction}
                            onSelectConstruction={construction => goTo(getOrderPageUrl(currentOrder.id, getTabs(search), construction.id))}
                        />
                    </div>
                </Card>
            </BottomSheet>
        </div>
    )
}
