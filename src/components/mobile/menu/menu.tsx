import * as React from 'react'
import {useState, useCallback, useEffect, useRef} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {NAV_CLIENTS, NAV_ORDER, NAV_PARTNER_GOODS, NAV_PROFILE, NAV_PAYMENT, NAV_EVENTS} from '../../../constants/navigation'
import {Link} from 'react-router-dom'
import {profileTabs} from '../../common/profile-page/tabs'
import {useOrdersPageLink, usePartnerGoodsPageLink, useMountingsPageLink, useEventsPageLink, usePaymentsPageLink} from '../../../hooks/router'
import {AnalyticsManager} from '../../../analytics-manager'
import {usePermissions, usePartnerAccess} from '../../../hooks/permissions'
import {SessionManager} from '../../../session-manager'
import {getClassNames} from '../../../helpers/css'
import {getCurrency, getBonusScore} from '../../../helpers/currency'
import {ReactComponent as ProfileIcon} from '../../common/menu/resources/profile_icon.inline.svg'
import {ReactComponent as OrdersIcon} from '../../common/menu/resources/orders_icon.inline.svg'
import {ReactComponent as ClientsIcon} from '../../common/menu/resources/clients_icon.inline.svg'
import {ReactComponent as GoodsPageIcon} from '../../common/menu/resources/goods_page_icon.inline.svg'
import {ReactComponent as FaqIcon} from '../../common/menu/resources/faq_icon.inline.svg'
import {ReactComponent as LogoutIcon} from './resources/logout.inline.svg'
import {ReactComponent as MountingsIcon} from '../../common/menu/resources/mountings_icon.inline.svg'
import {ReactComponent as CalendarIcon} from '../../common/menu/resources/calendar_icon.inline.svg'
import {clamp} from '../../../helpers/number'
import {isIOS} from '../../../helpers/mobile'
import {getDiscountForPartner} from '../../../helpers/account'
import {Hint} from '../../common/hint/hint'
import {HINTS} from '../../../constants/hints'
import {NAV_ENERGY_PROGRAM} from '../../../constants/navigation'
import {BuildTypes} from '../../../model/build-types'
import {BalanceModeSelector} from '../../common/balance-mode-selector/balance-mode-selector'
import styles from './menu.module.css'

const PADDING = 0
const MIN_TRANSITION_DURATION = 100
const DEFAULT_TRANSITION_DURATION = 400

interface IMenuProps {
    isOpened: boolean
    onClose: () => void
}

export function Menu(props: IMenuProps): JSX.Element {
    const {onClose} = props

    const [translateX, setTranslateX] = useState(0)
    const [isOpened, setIsOpened] = useState(props.isOpened)
    const [transitionDuration, setTransitionDuration] = useState(DEFAULT_TRANSITION_DURATION)

    const disabledTouchRef = useRef(isIOS())
    const widthRef = useRef(0)
    const containerRef = useRef<HTMLInputElement | null>(null)

    const url = useSelector((state: IAppState) => `${state.router.location.pathname}${state.router.location.search}`)
    const isVisibleChatButton = useSelector((state: IAppState) => state.chat.isVisibleButton)
    const isOpenedChat = useSelector((state: IAppState) => state.chat.isOpened)
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)
    const login = useSelector((state: IAppState) => state.account.login)
    const partner = useSelector((state: IAppState) => state.account.partner)
    const accountBalance = useSelector((state: IAppState) => state.account.balance)
    const framerPoints = useSelector((state: IAppState) => state.account.framerPoints)
    const isCashMode = useSelector((state: IAppState) => state.account.isCashMode)
    const bonusScore = useSelector((state: IAppState) => state.account.bonusScore)
    const nextDiscount = useSelector((state: IAppState) => state.account.nextDiscount)
    const isFramerPointsAvailable = useSelector((state: IAppState) => state.account.partner?.isFramerPointsAvailable)
    const showBalance = useSelector((state: IAppState) => state.account.showBalance)

    const isDraggingRef = useRef(false)

    const ordersPageLink = useOrdersPageLink()
    const partnerGoodsPageLink = usePartnerGoodsPageLink()
    const mountingsPageLink = useMountingsPageLink()
    const eventsPageLink = useEventsPageLink()
    const paymentsPageLink = usePaymentsPageLink()
    const permission = usePermissions()
    const {isExternal, isPermPartner} = usePartnerAccess()
    const [openedItem, setOpenedItem] = useState('')

    const updateWidth = useCallback(() => {
        if (widthRef.current || !containerRef.current) {
            return
        }
        widthRef.current = containerRef.current.getBoundingClientRect().width - PADDING
        setTranslateX(-widthRef.current)
        setIsOpened(false)
    }, [])

    useEffect(() => {
        if (props.isOpened) {
            updateWidth()
            setTransitionDuration(DEFAULT_TRANSITION_DURATION)
            setTranslateX(0)
            setIsOpened(true)
        }
    }, [updateWidth, props.isOpened])

    const close = useCallback(() => {
        setTransitionDuration(DEFAULT_TRANSITION_DURATION)
        setTranslateX(-widthRef.current)
        setIsOpened(false)
        onClose()
    }, [onClose])

    const toggleSubmenu = useCallback((item: string) => setOpenedItem(openedItem !== item ? item : ''), [openedItem])

    const menuItem = useCallback(
        (title: string, to: string, icon: JSX.Element, url?: string) => {
            return (
                <div className={getClassNames(styles.item, {[styles.active]: pathname.includes(url ?? to)})}>
                    <Link className={styles.link} to={to} onClick={close}>
                        <div className={styles.icon}>{icon}</div>
                        <p className={styles.title}>{title}</p>
                    </Link>
                </div>
            )
        },
        [pathname, close]
    )

    useEffect(() => {
        if (!isOpened) {
            return
        }
        const closeMenu = (e: MouseEvent): void => {
            if (translateX !== 0) {
                return
            }
            if (!containerRef.current || !containerRef.current.contains(e.target as Node)) {
                e.preventDefault()
                close()
            }
        }

        document.addEventListener('click', closeMenu)

        return () => {
            document.removeEventListener('click', closeMenu)
        }
    }, [translateX, isOpened, close])

    const offsetXRef = useRef(0)
    const touchStartXRef = useRef(0)
    const touchEndXRef = useRef(0)
    const touchStartTimeRef = useRef(0)

    const onTouchStart = useCallback(
        (e: TouchEvent) => {
            if (disabledTouchRef.current) {
                return
            }
            updateWidth()
            const touch = e.touches[0]
            if (!containerRef.current || !touch) {
                return
            }

            touchStartTimeRef.current = e.timeStamp
            touchStartXRef.current = touch.clientX
            offsetXRef.current = containerRef.current.getBoundingClientRect().right - PADDING
            setTransitionDuration(0)
        },
        [updateWidth]
    )

    const onTouchMove = useCallback(
        (e: TouchEvent) => {
            if (disabledTouchRef.current) {
                return
            }

            const touch = e.touches[0]
            if (!containerRef.current || !touch) {
                return
            }

            !isOpened && e.preventDefault()

            isDraggingRef.current = true

            touchEndXRef.current = touch.clientX

            const translateX = clamp(
                -widthRef.current + touchEndXRef.current - touchStartXRef.current + offsetXRef.current,
                -widthRef.current,
                0
            )
            setTranslateX(translateX)
        },
        [isOpened]
    )

    const onTouchEnd = useCallback(
        (e: TouchEvent) => {
            if (disabledTouchRef.current) {
                return
            }

            if (!isDraggingRef.current || !containerRef.current) {
                return
            }

            isDraggingRef.current = false

            const containerRight = containerRef.current.getBoundingClientRect().right
            const containerWidth = containerRef.current.getBoundingClientRect().width
            const distance = touchEndXRef.current - touchStartXRef.current

            const velocity = Math.abs(distance) / (e.timeStamp - touchStartTimeRef.current)
            const time = (containerWidth - containerRight) / velocity

            setTransitionDuration(clamp(time, MIN_TRANSITION_DURATION, DEFAULT_TRANSITION_DURATION))
            if (distance > 0) {
                setTranslateX(0)
                setIsOpened(true)
            } else {
                setTranslateX(-widthRef.current)
                setIsOpened(false)
                onClose()
            }
        },
        [onClose]
    )

    useEffect(() => {
        containerRef.current?.addEventListener('touchstart', onTouchStart, {passive: false})
        containerRef.current?.addEventListener('touchmove', onTouchMove, {passive: false})

        window.addEventListener('touchend', onTouchEnd)
        window.addEventListener('touchcancel', onTouchEnd)

        return () => {
            containerRef.current?.removeEventListener('touchstart', onTouchStart)
            containerRef.current?.removeEventListener('touchmove', onTouchMove)

            window.removeEventListener('touchend', onTouchEnd)
            window.removeEventListener('touchcancel', onTouchEnd)
        }
    }, [onTouchStart, onTouchMove, onTouchEnd])

    const {availableScore, potentialScore, earnedScore} = bonusScore

    const widthPrizeProgress = (): number => {
        const widthPrize = earnedScore / (5000 / 100)
        return widthPrize > 100 ? 100 : widthPrize
    }

    const earnedScoreFinal = (): number => {
        const total = 5000 - earnedScore
        if (total < 0 || total === 0) {
            return 0
        } else {
            return total
        }
    }

    const earnedScoreFinalF = earnedScoreFinal()

    return (
        <div
            ref={containerRef}
            className={styles.draggable}
            style={{
                paddingRight: isOpened ? `calc((100% - ${widthRef.current}px)/2)` : `${PADDING}px`,
                transitionDuration: `${transitionDuration}ms`,
                transform: widthRef.current ? `translateX(${translateX}px)` : `translateX(calc(-100% + ${PADDING}px))`
            }}>
            <div className={styles.wrapper}>
                <div className={styles.menu}>
                    <div className={styles.topBar}>
                        <button className={styles.closeButton} onClick={close} />
                        {!isExternal && (
                            <div className={styles.balance}>
                                {isCashMode ? (
                                    <span>{getCurrency(framerPoints)}</span>
                                ) : (
                                    <span className={accountBalance < 0 ? styles.negativeBalance : ''}>
                                        {getCurrency(accountBalance)} {!isFramerPointsAvailable && 'руб'}
                                    </span>
                                )}
                                {isFramerPointsAvailable && <BalanceModeSelector className={styles.balanceMode} />}
                            </div>
                        )}
                    </div>
                    {showBalance && isPermPartner && !isCashMode && (
                        <div className={`${styles.bonusScore} ${styles.item} ${openedItem === 'bonus' && styles.opened}`}>
                            <p className={styles.bonusTitle} onClick={() => toggleSubmenu('bonus')}>
                                Бонусы ENERGY{' '}
                                <span
                                    className={`${styles.currentValue} ${styles.availableBonuses} ${openedItem !== 'bonus' && styles.visible}`}>
                                    {getBonusScore(availableScore)} Б
                                </span>
                            </p>
                            <div className={`${styles.bonusList} ${styles.submenu}`}>
                                <div className={`${styles.bonusRow} ${styles.availableBonuses}`}>
                                    Доступные
                                    <Hint text={HINTS.availableBonuses} className={styles.hint} isDark={true} />
                                    <span className={styles.value}>{getBonusScore(availableScore)} Б</span>
                                </div>
                                <div className={`${styles.bonusRow} ${styles.potentialBonuses}`}>
                                    Потенциальные
                                    <Hint text={HINTS.potentialBonuses} className={styles.hint} isDark={true} />
                                    <span className={styles.value}>{getBonusScore(potentialScore)} Б</span>
                                </div>
                                <Link to={NAV_ENERGY_PROGRAM} className={styles.detailsLink} onClick={close}>
                                    Подробнее
                                </Link>
                                <div className={styles.progressBar}>
                                    <div
                                        className={`${styles.progress} ${earnedScoreFinalF === 0 ? styles.greenPrize : styles.redPrize}`}
                                        style={{width: `${widthPrizeProgress()}%`}}></div>
                                </div>
                                <div
                                    className={`${styles.rowPrize} ${styles.prizeBonuses} ${
                                        earnedScoreFinalF === 0 ? styles.greenPrizeText : styles.redPrizeText
                                    }`}>
                                    <div className={`${styles.bonusRow}`}>
                                        {earnedScoreFinalF === 0 ? 'Вы участвуете в розыгрыше' : 'До участия в розыгрыше'}
                                        {earnedScoreFinalF !== 0 && (
                                            <span className={`${styles.value} `}>{getBonusScore(earnedScoreFinalF)} Б</span>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                    {currentDealer && (
                        <div className={styles.dealer}>
                            <div className={styles.avatar}>{currentDealer.name[0]}</div>
                            <p className={styles.dealerInfo}>
                                <span className={styles.dealerName}>
                                    {currentDealer.name} {login ?? ''}
                                </span>
                                {!isExternal && partner && (
                                    <span className={styles.discount}>{getDiscountForPartner(partner, nextDiscount)}</span>
                                )}
                            </p>
                        </div>
                    )}
                    <div
                        className={getClassNames(`${styles.item} ${styles.itemProfile}`, {
                            [styles.opened]: openedItem === 'profile',
                            [styles.active]: pathname.includes(NAV_PROFILE)
                        })}>
                        <div className={styles.link} onClick={() => toggleSubmenu('profile')}>
                            <div className={styles.icon}>
                                <ProfileIcon />
                            </div>
                            <p className={styles.title}>Профиль</p>
                        </div>
                        <div className={styles.submenu}>
                            {profileTabs.get(permission.isNonQualifiedOrBossManager).map(tab => {
                                const {id, title} = tab
                                const tabUrl = profileTabs.getFullPath(id)
                                return (
                                    <Link
                                        key={id}
                                        to={tabUrl}
                                        className={getClassNames(`${styles.submenuLink}`, {[styles.active]: url === tabUrl})}
                                        onClick={close}>
                                        {title}
                                    </Link>
                                )
                            })}
                            {!isExternal && (
                                <Link
                                    key="payment"
                                    to={paymentsPageLink}
                                    className={getClassNames(`${styles.submenuLink}`, {[styles.active]: url.includes(NAV_PAYMENT)})}
                                    onClick={close}>
                                    Пополнить баланс
                                </Link>
                            )}
                        </div>
                    </div>
                    {menuItem('Заказы', ordersPageLink, <OrdersIcon />, NAV_ORDER)}
                    {menuItem('Монтажи', mountingsPageLink, <MountingsIcon />)}
                    {menuItem('Клиенты', NAV_CLIENTS, <ClientsIcon />)}
                    {menuItem('Отчет по материалам', partnerGoodsPageLink, <GoodsPageIcon />, NAV_PARTNER_GOODS)}
                    {process.env.BUILD_TYPE !== BuildTypes.Prod && menuItem('Календарь', eventsPageLink, <CalendarIcon />, NAV_EVENTS)}
                    <div
                        className={getClassNames(`${styles.item} ${styles.itemFaq}`, {
                            [styles.opened]: openedItem === 'documentation'
                        })}>
                        <div className={styles.link} onClick={() => toggleSubmenu('documentation')}>
                            <div className={styles.icon}>
                                <FaqIcon />
                            </div>
                            <p className={styles.title}>Помощь</p>
                        </div>
                        <div className={styles.submenu}>
                            <a
                                href="https://library.framer.ru"
                                target="_blank"
                                rel="noopener noreferrer"
                                className={styles.submenuLink}
                                onClick={() => AnalyticsManager.trackClickMedialibraryButton()}>
                                Медиабиблиотека
                            </a>
                            <a
                                href="https://docs.framer.ru/article/o-servise-framer"
                                target="_blank"
                                rel="noreferrer"
                                className={styles.submenuLink}
                                onClick={() => AnalyticsManager.trackClickInstructionLink()}>
                                Инструкции
                            </a>
                            <a
                                href="https://docs.framer.ru/faqs/faq-topic-1"
                                target="_blank"
                                rel="noreferrer"
                                className={styles.submenuLink}
                                onClick={() => AnalyticsManager.trackClickFaqLink()}>
                                FAQ
                            </a>
                            <a
                                href="mailto:info@framer.ru"
                                target="_blank"
                                rel="noopener noreferrer"
                                className={styles.submenuLink}
                                onClick={() => AnalyticsManager.trackEmailClick()}>
                                info@framer.ru
                            </a>
                            <a href="tel:88003010920" target="_blank" rel="noopener noreferrer" className={styles.submenuLink}>
                                8 (800) 301-09-20
                            </a>
                        </div>
                    </div>
                </div>
                <button className={styles.logoutButton} onClick={() => SessionManager.resetSession()}>
                    <div className={styles.icon}>
                        <LogoutIcon />
                    </div>
                    <p className={styles.title}>Выход</p>
                </button>
                {isVisibleChatButton && (
                    <button className={`${styles.chat} ${isOpenedChat && styles.hidden}`} onClick={() => window.carrotquest?.open()}>
                        Задать вопрос
                    </button>
                )}
            </div>
            <div className={styles.dragZone} />
        </div>
    )
}
