import * as React from 'react'
import {useEffect, useCallback} from 'react'
import {HeaderTitle} from '../header/header-title'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {OrderPageTabs} from '../order-page-tabs/order-page-tabs'
import {useListenOrderPageNavigation, useOrderTabs, useCheckOrderUserParameters} from '../../../hooks/orders'
import {getOrderPageUrl} from '../../../helpers/orders'
import {useGoTo} from '../../../hooks/router'
import {OrderTabContent} from '../order-tab-content/order-tab-content'
import {ConstructionTabContent} from '../construction-tab-content/construction-tab-content'
import {TemplateSelectorContainer} from '../../common/template-selector/template-selector-container'
import {ReactComponent as BackIcon} from './resources/back_icon.inline.svg'
import {useGoToPreviousStep, useShowTemplateSelector} from '../../../hooks/template-selector'
import {OrderErrorsControl} from '../order-errors/order-errors-control'
import styles from './order-page.module.css'
import {ORDER_TAB_ID} from '../../../constants/navigation'

export function OrderPage(): JSX.Element {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const hasShowTemplate = useSelector((state: IAppState) => new URLSearchParams(state.router.location.search).has('showTemplate'))
    const tabs = useOrderTabs(24, 24)
    const goBack = useGoToPreviousStep()
    const goTo = useGoTo()
    const showTemplateSelector = useShowTemplateSelector()
    useListenOrderPageNavigation()

    const checkUserParameter = useCheckOrderUserParameters()

    useEffect(() => {
        currentOrder?.id && checkUserParameter()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentOrder?.id])

    useEffect(() => {
        hasShowTemplate && window.scrollTo(0, 0)
    }, [hasShowTemplate])

    const activeTabIndex = tabs.findIndex(t => t.active)
    const activeTab = tabs[activeTabIndex]

    const openTemplateSelector = useCallback(() => {
        showTemplateSelector(null)
    }, [showTemplateSelector])

    return (
        <div>
            <HeaderTitle>
                {hasShowTemplate ? (
                    <div className={styles.header}>
                        <button className={styles.button} onClick={() => goBack()}>
                            <BackIcon />
                        </button>
                        Выберите шаблон
                    </div>
                ) : (
                    currentOrder?.fullOrderNumber ?? ''
                )}
            </HeaderTitle>
            {currentOrder && (
                <>
                    <OrderPageTabs
                        tabs={tabs}
                        activeTabIndex={activeTabIndex}
                        onAddTab={openTemplateSelector}
                        hasAdditionTab={!currentOrder.readonly}
                        onUpdateTabs={updatedTabs => {
                            const activeTabId = updatedTabs.find(t => t.active)?.id ?? ORDER_TAB_ID
                            goTo(
                                getOrderPageUrl(
                                    currentOrder.id,
                                    updatedTabs.map(t => t.id),
                                    activeTabId
                                )
                            )
                        }}
                    />
                    <OrderErrorsControl shouldShowErrors={Boolean(currentConstruction && currentConstruction.id === activeTab?.id)} />
                    <div className={styles.content}>
                        {activeTab?.id === ORDER_TAB_ID && <OrderTabContent />}
                        {currentConstruction && currentConstruction.id === activeTab?.id && <ConstructionTabContent />}
                    </div>
                </>
            )}
            <TemplateSelectorContainer />
        </div>
    )
}
