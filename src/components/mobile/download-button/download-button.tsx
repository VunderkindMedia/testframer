import * as React from 'react'
import {Button} from '../../common/button/button'
import styles from './download-button.module.css'

interface IGeoButtonProps {
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    className?: string
    isDisabled?: boolean
}

const DownloadButton = React.forwardRef<HTMLButtonElement, IGeoButtonProps>((props: IGeoButtonProps, ref) => {
    const {onClick, className, isDisabled} = props

    return <Button ref={ref} className={`${styles.button} ${className ?? ''}`} isDisabled={isDisabled} onClick={onClick} />
})

DownloadButton.displayName = 'DownloadButton'

export {DownloadButton}
