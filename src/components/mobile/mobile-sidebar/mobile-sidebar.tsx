import * as React from 'react'
import {ReactNode} from 'react'
import './mobile-sidebar.css'
import {Visibilities} from '../../../model/visibilities'
import {Colors} from '../../../constants/colors'

interface IMobileSidebarProps {
    title: string
    className?: string
    headerColor?: Colors
    bodyColor?: Colors
    children?: ReactNode
    onBackClick?: () => void
    onAfterClose?: () => void
    onChangeVisibility?: (visibility: Visibilities) => void
}

interface IMobileSidebarState {
    visibility: Visibilities
    opened: boolean
}

export class MobileSidebar extends React.Component<IMobileSidebarProps, IMobileSidebarState> {
    private _containerRef: HTMLDivElement | null = null
    private _bodyRef: HTMLDivElement | null = null

    constructor(props: IMobileSidebarProps) {
        super(props)

        this.state = {
            visibility: Visibilities.Hidden,
            opened: false
        }
    }

    resetScroll(): void {
        this._bodyRef?.scroll(0, 0)
    }

    show(): void {
        this.setState({
            visibility: Visibilities.Visible
        })

        this.props.onChangeVisibility && this.props.onChangeVisibility(Visibilities.Visible)
    }

    hide(): void {
        this.setState({
            visibility: Visibilities.Hidden
        })

        this.props.onChangeVisibility && this.props.onChangeVisibility(Visibilities.Hidden)
    }

    get visibility(): Visibilities {
        return this.state.visibility
    }

    componentDidMount(): void {
        if (!this._containerRef) {
            return
        }
        this._containerRef.onwheel = (e: MouseEvent) => {
            e.stopPropagation()
        }
    }

    render(): JSX.Element {
        const {title, children, headerColor, bodyColor, onBackClick, className, onAfterClose} = this.props

        const headerBgColor = headerColor ?? Colors.White
        const bodyBgColor = bodyColor ?? Colors.Bg

        return (
            <div
                className={`mobile-sidebar ${this.state.visibility} ${className}`}
                onTransitionEnd={() => {
                    this.setState({
                        opened: this.visibility === Visibilities.Visible
                    })
                    if (this.visibility === Visibilities.Hidden) {
                        onAfterClose && onAfterClose()
                    }
                }}
                onClick={e => e.stopPropagation()}
                ref={element => (this._containerRef = element)}>
                <div
                    className="mobile-sidebar__header"
                    style={{background: headerBgColor}}
                    onClick={() => {
                        onBackClick ? onBackClick() : this.hide()
                    }}>
                    <p>{title}</p>
                </div>
                <div className="mobile-sidebar__body" ref={element => (this._bodyRef = element)} style={{background: bodyBgColor}}>
                    {React.Children.map(children, child => (
                        <>{child}</>
                    ))}
                </div>
            </div>
        )
    }
}
