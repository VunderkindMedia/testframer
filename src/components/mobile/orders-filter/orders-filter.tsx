import * as React from 'react'
import {useEffect} from 'react'
import styles from './orders-filter.module.css'
import {useCallback} from 'react'
import {AllOrderStates, OrderStates} from '../../../model/framer/order-states'
import {SearchInput} from '../../common/search-input/search-input'
import {WithTitle} from '../../common/with-title/with-title'
import {DateRangeSelector} from '../../common/date-range-selector/date-range-selector'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'
import {getPreviousMonthDate} from '../../../helpers/date'
import {Select} from '../../common/select/select'
import {arraysHaveSameItems} from '../../../helpers/array'
import {STATES_ITEMS} from '../../../constants/states-items'

interface IOrdersFilterProps {
    orderStates: OrderStates[]
    startDate: Date
    endDate: Date
    orderNumber: string
    onChange?: (states: OrderStates[], dateFrom: Date, dateTo: Date, orderNumber: string) => void
}

const newStartDate = (): Date => getPreviousMonthDate(new Date())
const newEndDate = (): Date => new Date()

export function OrdersFilter(props: IOrdersFilterProps): JSX.Element {
    const {orderStates, startDate, endDate, orderNumber, onChange} = props

    const onChangeCallback = useCallback(
        (states: OrderStates[], dateFrom: Date, dateTo: Date, orderNumber: string) => {
            onChange && onChange(states, dateFrom, dateTo, orderNumber)
        },
        [onChange]
    )

    const selectedItem = STATES_ITEMS.find(item => arraysHaveSameItems(item.data, orderStates, (i1, i2) => i1 === i2))

    useEffect(() => {
        if (!selectedItem && onChange) {
            onChange(STATES_ITEMS[0].data, startDate, endDate, orderNumber)
        }
    }, [selectedItem, startDate, endDate, orderNumber, onChange])

    return (
        <div className={styles.ordersFilter}>
            <Select
                className={styles.statesFilter}
                items={STATES_ITEMS}
                selectedItem={selectedItem}
                onSelect={item => onChangeCallback(item.data, startDate, endDate, orderNumber)}
            />
            <div className={styles.datesFilter}>
                <WithTitle title="Фильтр по дате">
                    <DateRangeSelector
                        className={styles.rangeSelector}
                        startDate={startDate}
                        endDate={endDate}
                        onChange={(startDate, endDate) => onChangeCallback(orderStates, startDate, endDate, orderNumber)}
                    />
                </WithTitle>
            </div>
            <div className={styles.search}>
                <WithTitle title="Поиск">
                    <SearchInput
                        className={styles.searchInput}
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        value={orderNumber}
                        onChange={value => onChangeCallback(orderStates, startDate, endDate, value)}
                        onClear={() => onChangeCallback(orderStates, startDate, endDate, '')}
                    />
                </WithTitle>
            </div>
            <button className={styles.resetButton} onClick={() => onChangeCallback(AllOrderStates, newStartDate(), newEndDate(), '')}>
                Сбросить
            </button>
        </div>
    )
}
