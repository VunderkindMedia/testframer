import * as React from 'react'
import {ListItem, ListItemHeader, ListItemBody} from '../list-item'
import {getTextOrMdash} from '../../../helpers/text'
import {Client} from '../../../model/framer/client'
import {WithTitle} from '../../common/with-title/with-title'
import {Action} from '../../../model/action'
import {DropdownMenu} from '../../common/dropdown-menu/dropdown-menu'
import styles from './client-list-item.module.css'

interface IClientListItemProps {
    client: Client
    onEdit: () => void
    onDelete: () => void
}

export function ClientListItem(props: IClientListItemProps): JSX.Element {
    const {client, onEdit, onDelete} = props

    const editAction = new Action('Редактировать', () => {
        onEdit()
    })

    const menuItems = [
        editAction,
        new Action('Удалить', () => {
            onDelete()
        })
    ]

    return (
        <ListItem onClick={editAction.func}>
            <ListItemHeader>
                <p>{getTextOrMdash(client.name)}</p>
                <DropdownMenu items={menuItems} />
            </ListItemHeader>
            <ListItemBody>
                <div className={styles.row}>
                    <WithTitle title="Телефон:">
                        <p>{getTextOrMdash(client.phone)}</p>
                    </WithTitle>
                    <WithTitle title="Эл. почта:" className={styles.email}>
                        <p>{getTextOrMdash(client.email)}</p>
                    </WithTitle>
                </div>
                <WithTitle title="Адрес:">
                    <p>{getTextOrMdash(client.address)}</p>
                </WithTitle>
            </ListItemBody>
        </ListItem>
    )
}
