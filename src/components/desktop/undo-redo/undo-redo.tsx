import * as React from 'react'
import styles from './undo-redo.module.css'
import {ReactComponent as UndoIcon} from './resources/undo.inline.svg'
import {ReactComponent as RedoIcon} from './resources/redo.inline.svg'
import {IconButton} from '../../common/icon-control/icon-button'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useCallback, useEffect, useState} from 'react'
import {HistoryManager} from '../../../history-manager'
import {isRedo, isUndo} from '../../../helpers/keyboard'
import {ArrowTooltip} from '../../common/arrow-tooltip/arrow-tooltip'
import {useUpdateCurrentOrder} from '../../../hooks/orders'

interface IUndoRedoProps {
    className?: string
}

export function UndoRedo(props: IUndoRedoProps): JSX.Element {
    const {className} = props

    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)

    const updateCurrentOrder = useUpdateCurrentOrder()

    const [isDisabledUndo, setIsDisabledUndo] = useState(true)
    const [isDisabledRedo, setIsDisabledRedo] = useState(true)

    useEffect(() => {
        if (!currentConstruction) {
            setIsDisabledUndo(true)
            setIsDisabledRedo(true)
            return
        }
        const history = HistoryManager.getHistoryByConstructionId(currentConstruction.id)
        setIsDisabledUndo(history.past.length === 0)
        setIsDisabledRedo(history.future.length === 0)
    }, [currentConstruction])

    const onKeyDownHandler = useCallback(
        (e: KeyboardEvent): void => {
            if (isUndo(e)) {
                currentConstruction?.id && HistoryManager.undo(currentConstruction.id)
            } else if (isRedo(e)) {
                currentConstruction?.id && HistoryManager.redo(currentConstruction.id)
            }
        },
        [currentConstruction?.id]
    )

    useEffect(() => {
        document.addEventListener('keydown', onKeyDownHandler)

        return () => {
            document.removeEventListener('keydown', onKeyDownHandler)
        }
    }, [onKeyDownHandler])

    return (
        <div className={`${styles.buttons} ${className ?? ''}`}>
            <ArrowTooltip title="Отменить последнее действие (Ctrl+Z)">
                <div>
                    <IconButton
                        isDisabled={isDisabledUndo}
                        onClick={() => {
                            currentConstruction?.id && HistoryManager.undo(currentConstruction.id)
                            currentOrder?.addAction('Отменили последнее действие', currentConstruction)
                            if (currentOrder) updateCurrentOrder(currentOrder)
                        }}>
                        <UndoIcon />
                    </IconButton>
                </div>
            </ArrowTooltip>
            <ArrowTooltip title="Повторить последнее действие (Ctrl+Y)">
                <div>
                    <IconButton
                        isDisabled={isDisabledRedo}
                        onClick={() => {
                            currentConstruction?.id && HistoryManager.redo(currentConstruction.id)
                            currentOrder?.addAction('Повторили последнее действие', currentConstruction)
                            if (currentOrder) updateCurrentOrder(currentOrder)
                        }}>
                        <RedoIcon />
                    </IconButton>
                </div>
            </ArrowTooltip>
        </div>
    )
}
