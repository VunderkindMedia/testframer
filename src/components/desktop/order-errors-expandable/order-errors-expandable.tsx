import * as React from 'react'
import {useEffect, useState} from 'react'
import './order-errors-expandable.css'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {OrderErrors} from '../../common/order-errors/order-errors'
import {getOrderPageUrl} from '../../../helpers/orders'
import {useGoTo} from '../../../hooks/router'
import {getTabs} from '../../../helpers/tabs'

export function OrderErrorsExpandable(): JSX.Element | null {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)

    const [isVisible, setIsVisible] = useState(false)
    const goTo = useGoTo()

    useEffect(() => {
        if (currentOrder && !currentOrder.changed) {
            setIsVisible(true)
        }
    }, [currentOrder])

    if (!currentOrder) {
        return null
    }

    const hasErrors = currentOrder.constructions.some(construction => construction.errors.length > 0)

    if (!hasErrors) {
        return null
    }

    return (
        <div className={`order-errors-expandable ${isVisible && 'visible'}`}>
            <div className="order-errors-expandable__body">
                <OrderErrors
                    order={currentOrder}
                    currentConstruction={currentConstruction}
                    onSelectConstruction={construction => goTo(getOrderPageUrl(currentOrder.id, getTabs(search), construction.id))}
                />
            </div>
            <div className="order-errors-expandable__header" onClick={() => setIsVisible(!isVisible)}>
                <div className="error-icon animate" />
                <p>Ошибки и предупреждения</p>
            </div>
        </div>
    )
}
