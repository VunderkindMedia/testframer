import * as React from 'react'
import styles from './new-sidebar-item.module.css'
import {CSSProperties, ReactNode} from 'react'

export interface INewSidebarItemProps {
    onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
    className?: string
    style?: CSSProperties
    children?: ReactNode
    border?: 'long' | 'short' | 'none'
}

export function NewSidebarItem(props: INewSidebarItemProps): JSX.Element {
    const {className, style, children, border = 'long', onClick} = props

    return (
        <div className={`${styles.newSidebarItem} ${styles[border]} ${className ?? ''}`} style={style} onClick={onClick}>
            {children}
        </div>
    )
}
