import * as React from 'react'
import styles from './new-sidebar.module.css'
import {NewSidebarItem} from './new-sidebar-item/new-sidebar-item'
import {Card} from '../../common/card/card'
import {ReactNode} from 'react'

interface INewSidebarProps {
    headerTitle?: string
    children?: ReactNode
    className?: string
}

export function NewSidebar(props: INewSidebarProps): JSX.Element {
    const {headerTitle, children, className} = props

    return (
        <div className={`${styles.newSidebar} ${className}`}>
            <Card className={styles.card}>
                {headerTitle && (
                    <NewSidebarItem className={styles.header}>
                        <p>{headerTitle}</p>
                    </NewSidebarItem>
                )}
                <div className={styles.body}>{children}</div>
            </Card>
        </div>
    )
}
