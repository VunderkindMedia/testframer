export interface IBillPageData {
    bankName: string
    bik: string
    billNumber: string
    inn: string
    kpp: string
    recipient: string
    billNumber2: string
    provider: string
    shipper: string
    head: string
    headSign: string | null
    accountant: string
    accountantSign: string | null
    stamp: string
}
