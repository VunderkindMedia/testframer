import * as React from 'react'
import styles from './bill-page.module.css'
import {useCallback, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {loadDealer} from '../../../redux/account/actions'
import {FactoryTypes} from '../../../model/framer/factory-types'
import {IBillPageData} from './I-bill-page-data'
import {RUB_SYMBOL} from '../../../constants/strings'
import {getBillCurrency, numToPhrase} from '../../../helpers/currency'
import stamp_msk from './resources/stamp_msk.png'
import stamp_perm from './resources/stamp_perm.png'

export const getBillPageData = (factoryType: FactoryTypes): IBillPageData => {
    if (factoryType === FactoryTypes.Moscow) {
        return {
            bankName: 'ТОЧКА ПАО БАНКА "ФК ОТКРЫТИЕ"',
            bik: '044525999',
            billNumber: '30101 810 8 4525 0000999',
            inn: '7719479145',
            kpp: '771901001',
            recipient: 'ООО "АМЕГА"',
            billNumber2: '40702 810 1 0250 0085613',
            provider:
                'ООО "АМЕГА", ИНН 7719479145, КПП 771901001, 107023, Москва г, Мажоров пер, дом 14, строение 14, офис 14512, тел.: +7 (495) 1459111',
            shipper:
                'ООО "АМЕГА", ИНН 7719479145, КПП 771901001, 107023, Москва г, Мажоров пер, дом 14, строение 14, офис 14512, тел.: +7 (495) 1459111',
            head: 'Михайлин В. Г.',
            headSign: null,
            accountant: 'Гундова О. К.',
            accountantSign: null,
            stamp: stamp_msk
        }
    }

    return {
        bankName: 'Филиал ТОЧКА ПАО БАНКА "ФК ОТКРЫТИЕ"',
        bik: '044525999',
        billNumber: '30101 810 8 4525 0000999',
        inn: '5905258887',
        kpp: '590501001',
        recipient: 'Общество с ограниченной ответственностью "Амега"',
        billNumber2: '40702 810 7 0250 0062913',
        provider:
            'Общество с ограниченной ответственностью "Амега", ИНН 5905258887, КПП 590501001, 614055, Пермский край, Пермь г, Промышленная ул, дом № 123, корпус а, тел.: 294-94-52',
        shipper:
            'Общество с ограниченной ответственностью "Амега", ИНН 5905258887, КПП 590501001, 614055, Пермский край, Пермь г, Промышленная ул, дом № 123, корпус а, тел.: 294-94-52',
        head: 'Соломенников О. В.',
        headSign: null,
        accountant: 'Чигирева Е. Н.',
        accountantSign: null,
        stamp: stamp_perm
    }
}

// ВНИМАНИЕ!!! ВСЕГДА ПРОВЕРЯЙ В РЕЖИМЕ ИНКОГНИТО
export function BillPage(): JSX.Element | null {
    const location = useSelector((state: IAppState) => state.router.location)
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)

    const dispatch = useDispatch()
    const loadDealerAction = useCallback((dealerId: number) => dispatch(loadDealer(dealerId)), [dispatch])

    useEffect(() => {
        if (currentDealer) {
            document.body.classList.add(styles.body, 'printable')
        } else {
            const pageParams = new URLSearchParams(location.search)
            const dealerId = pageParams.get('dealerId')
            dealerId && loadDealerAction(+dealerId)
        }

        return () => {
            document.body.classList.remove(styles.body, 'printable')
        }
    }, [currentDealer, loadDealerAction, location.search])

    const pageParams = new URLSearchParams(location.search)
    const sumStr = pageParams.get('sum')
    const sum = sumStr ? parseInt(sumStr, 10) : 0

    if (!currentDealer) {
        return null
    }

    const data = getBillPageData(currentDealer.factoryId)

    return (
        <div className={styles.page}>
            <div className="a4">
                <p className={styles.attention}>
                    Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате
                    <br />
                    обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается по факту
                    <br />
                    прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и паспорта.
                </p>
                <table className={styles.headerTable}>
                    <tbody>
                        <tr>
                            <td>
                                <p>{data.bankName}</p>
                                <p>
                                    <span>Банк получателя</span>
                                </p>
                            </td>
                            <td>
                                <p className={styles.noWrap}>БИК</p>
                                <p className={styles.noWrap}>Сч. №</p>
                            </td>
                            <td>
                                <p>{data.bik}</p>
                                <p>{data.billNumber}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className={styles.flex}>
                                    <p className={styles.borderRight}>ИНН {data.inn}</p>
                                    <p>КПП {data.kpp}</p>
                                </div>
                                <div className={styles.borderTop}>
                                    <p>{data.recipient}</p>
                                    <p>
                                        <span>Получатель</span>
                                    </p>
                                </div>
                            </td>
                            <td>
                                <p className={styles.noWrap}>Сч. №</p>
                            </td>
                            <td>
                                <p>{data.billNumber2}</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p className={styles.title}>
                    {currentDealer.factoryId === FactoryTypes.Perm
                        ? `Счет на оплату № ${currentDealer.externalId}-${currentDealer.billsNum} от `
                        : `Счет на оплату № ${currentDealer.billsNum} от `}
                    {new Intl.DateTimeFormat('ru', {
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric'
                    }).format(new Date())}
                </p>
                <table className={styles.table}>
                    <tbody>
                        <tr>
                            <td>Поставщик:</td>
                            <td>{data.provider}</td>
                        </tr>
                        <tr>
                            <td>Грузоотправитель:</td>
                            <td>{data.shipper}</td>
                        </tr>
                        <tr>
                            <td>Покупатель:</td>
                            <td>{currentDealer.name}</td>
                        </tr>
                        <tr>
                            <td>Грузополучатель:</td>
                            <td>{currentDealer.name}</td>
                        </tr>
                    </tbody>
                </table>
                <table className={styles.totalTable}>
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Артикул</th>
                            <th>Товары (работы, услуги)</th>
                            <th>Кол-во</th>
                            <th>Ед.</th>
                            <th>Цена</th>
                            <th style={{width: '105px'}}>Сумма</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>-</td>
                            <td>Предварительная оплата за металлопластиковые конструкции</td>
                            <td>1</td>
                            <td>шт</td>
                            <td>{getBillCurrency(sum)}</td>
                            <td>{getBillCurrency(sum)}</td>
                        </tr>
                    </tbody>
                </table>
                <div className={styles.totalSumTableWrap}>
                    <table className={styles.totalSumTable}>
                        <tbody>
                            <tr>
                                <td>Итого:</td>
                                <td style={{width: '105px'}}>{getBillCurrency(sum)}</td>
                            </tr>
                            <tr>
                                <td>В том числе НДС:</td>
                                <td style={{width: '105px'}}>{getBillCurrency((sum / 1.2) * 0.2)}</td>
                            </tr>
                            <tr>
                                <td>Всего к оплате:</td>
                                <td>{getBillCurrency(sum)}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p className={styles.summary}>
                    Всего наименований 1, на сумму {getBillCurrency(sum)} {RUB_SYMBOL}
                </p>
                <div className={styles.price}>
                    <p>{numToPhrase(+(sum / 100).toFixed(2))}</p>
                    <p />
                </div>
                <div>
                    <div className={styles.signatures}>
                        <img src={data.stamp} alt="" />
                        <table className={styles.signature}>
                            <tbody>
                                <tr>
                                    <td>
                                        <p className={styles.signatureTitle}>Руководитель</p>
                                    </td>
                                    <td>
                                        <div>
                                            <p className={styles.signatureName}>{data.head}</p>
                                            <p className={styles.signatureHint}>расшифровка подписи</p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p className={styles.signatureTitle}>Главный (старший) бухгалтер </p>
                                    </td>
                                    <td>
                                        <div>
                                            <p className={styles.signatureName}>{data.accountant}</p>
                                            <p className={styles.signatureHint}>расшифровка подписи</p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}
