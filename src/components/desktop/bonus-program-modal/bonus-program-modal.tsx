import * as React from 'react'
import {Link} from 'react-router-dom'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Hint} from '../../common/hint/hint'
import {HINTS} from '../../../constants/hints'
import {getBonusScore} from '../../../helpers/currency'
import {NAV_ENERGY_PROGRAM} from '../../../constants/navigation'
import {ReactComponent as KvadrRedIcon} from '../../../resources/images/quad-red.inline.svg'
import {ReactComponent as KvadrGreenIcon} from '../../../resources/images/quad-green.inline.svg'
import styles from './bonus-program-modal.module.css'

interface IBonusProgramModalProps {
    className?: string
}

const BonusProgramModal = React.forwardRef<HTMLDivElement, IBonusProgramModalProps>((props: IBonusProgramModalProps, ref): JSX.Element => {
    const {className} = props
    const {availableScore, potentialScore, earnedScore} = useSelector((state: IAppState) => state.account.bonusScore)

    const widthPrizeProgress = (): number => {
        const widthPrize = earnedScore / (5000 / 100)
        return widthPrize > 100 ? 100 : widthPrize
    }

    const earnedScoreFinal = (): number => {
        const total = 5000 - earnedScore
        if (total < 0 || total === 0) {
            return 0
        } else {
            return total
        }
    }

    const earnedScoreFinalF = earnedScoreFinal()

    return (
        <div className={`${styles.modal} ${className ?? ''}`} ref={ref}>
            <Link to={NAV_ENERGY_PROGRAM} className={styles.title}>
                Бонусная программа ENERGY
            </Link>
            <div className={styles.list}>
                <div className={`${styles.row} ${styles.availableBonuses}`}>
                    Доступные
                    <Hint text={HINTS.availableBonuses} className={styles.hint} />
                </div>
                <span className={`${styles.value} ${styles.availableBonuses}`}>{getBonusScore(availableScore)} Б</span>
                <div className={`${styles.row} ${styles.potentialBonuses}`}>
                    Потенциальные
                    <Hint text={HINTS.potentialBonuses} className={styles.hint} />
                </div>
                <span className={`${styles.value} ${styles.potentialBonuses}`}>{getBonusScore(potentialScore)} Б</span>
            </div>
            <div>
                <div className={styles.progressBar}>
                    <div
                        className={`${styles.progress} ${earnedScoreFinalF === 0 ? styles.greenPrize : styles.redPrize}`}
                        style={{width: `${widthPrizeProgress()}%`}}></div>
                </div>
            </div>
            <div
                className={`${styles.rowPrize} ${styles.prizeBonuses} ${
                    earnedScoreFinalF === 0 ? styles.greenPrizeText : styles.redPrizeText
                }`}>
                <div className={styles.kvadr}>
                    <p style={{marginRight: '3px'}}>{earnedScoreFinalF === 0 ? 'Вы участвуете в розыгрыше' : 'До участия в розыгрыше'}</p>
                    {earnedScoreFinalF === 0 ? <KvadrGreenIcon /> : <KvadrRedIcon />}
                </div>
                {earnedScoreFinalF !== 0 && <span className={`${styles.value} `}>{getBonusScore(earnedScoreFinalF)} Б</span>}
            </div>
        </div>
    )
})

BonusProgramModal.displayName = 'BonusProgramModal'

export {BonusProgramModal}
