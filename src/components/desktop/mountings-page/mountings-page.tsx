import * as React from 'react'
import {PageTitle} from '../../common/page-title/page-title'
import {Button} from '../../common/button/button'
import {HINTS} from '../../../constants/hints'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {MountingsTable} from './mountings-table/mountings-table'
import {useMountingsFromSearch} from '../../../hooks/mountings'
import {getMountingsFilterParams, getMountingsFilter, getMountingLink} from '../../../helpers/mountings'
import {MountingsPageSidebar} from './mountings-page-sidebar/mountings-page-sidebar'
import {useCallback, useRef} from 'react'
import {TabletSidebar} from '../../tablet/tablet-sidebar/tablet-sidebar'
import {useAppContext} from '../../../hooks/app'
import {FilterButton} from '../../common/button/filter-button/filter-button'
import {Mounting} from '../../../model/framer/mounting'
import {setCurrentMounting} from '../../../redux/mountings/actions'
import {useGoTo} from '../../../hooks/router'
import {NAV_MOUNTINGS} from '../../../constants/navigation'
import {useDeleteMountingModal, useSetMountingsFilters, useDownloadMountingsReport} from '../../../hooks/mountings'
import {Modal} from '../../common/modal/modal'
import {PaginationMenu} from '../../common/pagination-menu/pagination-menu'
import {usePermissions} from '../../../hooks/permissions'
import {ConfirmDealerLink} from '../../common/confirm-dealer-link/confirm-dealer-link'
import styles from './mountings-page.module.css'

export function MountingsPage(): JSX.Element {
    const requestCount = useSelector((state: IAppState) => state.request.count)
    const currentMounting = useSelector((state: IAppState) => state.mountings.current)
    const mountingsTotalCount = useSelector((state: IAppState) => state.mountings.totalCount)
    const search = useSelector((state: IAppState) => state.router.location.search)

    const tabletSidebarRef = useRef<TabletSidebar | null>(null)

    const permissions = usePermissions()
    const mountings = useMountingsFromSearch()
    const dispatch = useDispatch()
    const goTo = useGoTo()
    const setMountingsFilter = useSetMountingsFilters()
    const downloadMountingsReport = useDownloadMountingsReport()

    const [modalModel, showModal] = useDeleteMountingModal()
    const setCurrentMountingAction = useCallback((mounting: Mounting) => dispatch(setCurrentMounting(mounting)), [dispatch])
    const {isTablet} = useAppContext()

    const filterParams = getMountingsFilterParams(search)
    const {offset, limit} = filterParams

    return (
        <div className={styles.page}>
            {modalModel && <Modal model={modalModel} />}
            <div className={styles.content}>
                {permissions.isNonQualified && (
                    <ConfirmDealerLink className={styles.permissionError} title="Для управления монтажами необходимо" />
                )}
                <div className={styles.header}>
                    <PageTitle title="Монтажи" className={styles.title} />
                    {currentMounting && (
                        <>
                            <Button buttonStyle="with-border" onClick={() => goTo(getMountingLink(currentMounting.id))}>
                                Редактировать
                            </Button>
                            <Button buttonStyle="with-border" onClick={() => showModal(currentMounting)}>
                                Удалить
                            </Button>
                            <Button buttonStyle="with-border" onClick={() => downloadMountingsReport(currentMounting)}>
                                Скачать
                            </Button>
                            {isTablet && <FilterButton onClick={() => tabletSidebarRef.current?.show()} />}
                        </>
                    )}
                </div>
                <div className={styles.tableContainer}>
                    {mountings.length > 0 ? (
                        <MountingsTable mountings={mountings} currentMounting={currentMounting} setCurrentMounting={setCurrentMountingAction} />
                    ) : (
                        <>{requestCount === 0 && <p>{HINTS.mountingsNotFound}</p>}</>
                    )}
                </div>
                <PaginationMenu
                    currentPage={Math.ceil(offset / limit)}
                    itemsOnPage={limit}
                    totalCount={mountingsTotalCount}
                    onSelect={page => {
                        const offset = limit * page
                        const filter = getMountingsFilter({...filterParams, offset})
                        goTo(`${NAV_MOUNTINGS}${filter}`)
                    }}
                />
            </div>
            {isTablet ? (
                <TabletSidebar ref={tabletSidebarRef}>
                    <MountingsPageSidebar filterParams={filterParams} onChangeFilterPrams={setMountingsFilter} />
                </TabletSidebar>
            ) : (
                <MountingsPageSidebar filterParams={filterParams} onChangeFilterPrams={setMountingsFilter} />
            )}
        </div>
    )
}
