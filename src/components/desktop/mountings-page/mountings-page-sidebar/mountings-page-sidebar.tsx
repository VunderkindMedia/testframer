import * as React from 'react'
import {useEffect} from 'react'
import {OrderStates} from '../../../../model/framer/order-states'
import {ListItem} from '../../../../model/list-item'
import {WithTitle} from '../../../common/with-title/with-title'
import {DateRangeSelector} from '../../../common/date-range-selector/date-range-selector'
import {SearchInput} from '../../../common/search-input/search-input'
import {NewSidebar} from '../../new-sidebar/new-sidebar'
import {NewSidebarItem} from '../../new-sidebar/new-sidebar-item/new-sidebar-item'
import {Select} from '../../../common/select/select'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import {STATES_ITEMS} from '../../../../constants/states-items'
import {arraysHaveSameItems} from '../../../../helpers/array'
import {IMountingsFilterParams} from '../../../../helpers/mountings'
import {useClients} from '../../../../hooks/clients'
import {useEmployees, useAffiliates} from '../../../../hooks/account'
import styles from './mountings-page-sidebar.module.css'

interface IPartnerGoodsPageSidebarProps {
    filterParams: IMountingsFilterParams
    onChangeFilterPrams: (params: IMountingsFilterParams) => void
}

export function MountingsPageSidebar(props: IPartnerGoodsPageSidebarProps): JSX.Element {
    const {filterParams, onChangeFilterPrams} = props
    const {from, to, dateFrom, dateTo, states, filials, clients, owners} = filterParams

    const employeesList = useEmployees()
    const affiliatesList = useAffiliates()
    const clientsList = useClients()

    const affiliatesListItems = affiliatesList?.length
        ? [new ListItem('Все', 'Все', -1), ...affiliatesList.map(({id, name}) => new ListItem(String(id), name, id))]
        : []
    const selectedStateItem = STATES_ITEMS.find(i => arraysHaveSameItems(i.data, states, (s1, s2) => s1 === s2))
    const clientsListItems = [new ListItem('Все', 'Все', -1), ...clientsList.map(({id, name}) => new ListItem(String(id), name, id))]
    const employeesListItems = employeesList?.length
        ? [new ListItem('Все', 'Все', -1), ...employeesList.map(({id, name}) => new ListItem(String(id), name, id))]
        : []

    useEffect(() => {
        if (!selectedStateItem) {
            onChangeFilterPrams({...filterParams, states: STATES_ITEMS[0].data})
        }
    }, [selectedStateItem, filterParams, onChangeFilterPrams])

    return (
        <NewSidebar headerTitle="Фильтр" className={styles.sidebar}>
            <NewSidebarItem className={styles.createdDate}>
                <WithTitle title="Дата создания">
                    <DateRangeSelector
                        className={styles.rangeSlider}
                        startDate={from}
                        endDate={to}
                        onChange={(startDate, endDate) => onChangeFilterPrams({...filterParams, from: startDate, to: endDate})}
                    />
                </WithTitle>
            </NewSidebarItem>
            <NewSidebarItem className={styles.name}>
                <WithTitle title="Номер">
                    <SearchInput
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        value={filterParams.number}
                        placeholder="Поиск"
                        onChange={value => onChangeFilterPrams({...filterParams, number: value})}
                        onClear={() => onChangeFilterPrams({...filterParams, number: ''})}
                    />
                </WithTitle>
            </NewSidebarItem>
            <NewSidebarItem className={styles.mountingDate}>
                <WithTitle title="Дата монтажа">
                    <DateRangeSelector
                        className={styles.rangeSlider}
                        startDate={dateFrom}
                        endDate={dateTo}
                        onChange={(startDate, endDate) => onChangeFilterPrams({...filterParams, dateFrom: startDate, dateTo: endDate})}
                    />
                </WithTitle>
            </NewSidebarItem>
            {employeesListItems.length > 0 && (
                <NewSidebarItem>
                    <WithTitle title="Менеджер">
                        <Select<number | undefined>
                            items={employeesListItems}
                            isMultiSelect={true}
                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                            selectedItems={
                                owners.length ? employeesListItems.filter(e => e.data && owners.includes(e.data)) : [employeesListItems[0]]
                            }
                            onMultiSelect={items => {
                                const employeesIds: number[] = []
                                items.map(({data}) => {
                                    if (data && data !== -1) {
                                        employeesIds.push(data)
                                    }
                                })
                                onChangeFilterPrams({...filterParams, owners: employeesIds})
                            }}
                        />
                    </WithTitle>
                </NewSidebarItem>
            )}
            <NewSidebarItem>
                <WithTitle title="Клиент">
                    <Select<number | undefined>
                        items={clientsListItems}
                        isMultiSelect={true}
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        shouldShowFilter={true}
                        selectedItems={
                            clients.length ? clientsListItems.filter(({data}) => data && clients.includes(data)) : [clientsListItems[0]]
                        }
                        onMultiSelect={items => {
                            const clientIds: number[] = []
                            items.map(item => {
                                if (item.data && item.data !== -1) {
                                    clientIds.push(item.data)
                                }
                            })
                            onChangeFilterPrams({...filterParams, clients: clientIds})
                        }}
                    />
                </WithTitle>
            </NewSidebarItem>
            {/* <NewSidebarItem className={styles.createdDate}>
                <WithTitle title="Дата отгрузки">
                    <DateRangeSelector
                        className={styles.rangeSlider}
                        startDate={shippingFrom}
                        endDate={shippingTo}
                        onChange={(startDate, endDate) => onChangeFilterPrams({...filterParams, shippingFrom: startDate, shippingTo: endDate})}
                    />
                </WithTitle>
            </NewSidebarItem> */}
            <NewSidebarItem className={styles.states}>
                <WithTitle title="Статус">
                    <Select<OrderStates[]>
                        items={STATES_ITEMS}
                        selectedItem={selectedStateItem}
                        onSelect={item => onChangeFilterPrams({...filterParams, states: item.data})}
                    />
                </WithTitle>
            </NewSidebarItem>
            {affiliatesListItems.length > 0 && (
                <NewSidebarItem className={styles.affiliate}>
                    <WithTitle title="Филиалы">
                        <Select<number | undefined>
                            isMultiSelect={true}
                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                            items={affiliatesListItems}
                            selectedItems={
                                filials?.length
                                    ? affiliatesListItems.filter(fl => fl.data && filials.includes(fl.data))
                                    : [affiliatesListItems[0]]
                            }
                            onMultiSelect={items => {
                                const filialIds: number[] = []
                                items.map(item => {
                                    if (item.data && item.data !== -1) {
                                        filialIds.push(item.data)
                                    }
                                })
                                onChangeFilterPrams({...filterParams, filials: filialIds})
                            }}
                        />
                    </WithTitle>
                </NewSidebarItem>
            )}
        </NewSidebar>
    )
}
