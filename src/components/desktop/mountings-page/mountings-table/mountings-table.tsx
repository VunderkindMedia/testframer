import * as React from 'react'
import {getDateDDMMYYYYFormat} from '../../../../helpers/date'
import {Mounting} from '../../../../model/framer/mounting'
import {PhoneButton} from '../../../common/button/phone-button/phone-button'
import {GeoButton} from '../../../common/button/geo-button/geo-button'
import {CommentButton} from '../../../common/button/comment-button/comment-button'
// import {BrigadeButton} from '../../../common/button/brigade-button/brigade-button'
import {useState} from 'react'
import {CloseButton} from '../../../common/button/close-button/close-button'
import {DateSelector} from '../../../common/date-selector/date-selector'
import {WithTitle} from '../../../common/with-title/with-title'
import {Input} from '../../../common/input/input'
import {ArrowTooltip} from '../../../common/arrow-tooltip/arrow-tooltip'
import {getOrderStateName, getOrderStateColor} from '../../../../model/framer/order-states'
import {useUpdateMountingDate, useUpdateMountingNote, useUpdateClientAddress} from '../../../../hooks/mountings'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import styles from './mountings-table.module.css'

interface IMountingsTableProps {
    mountings: Mounting[]
    currentMounting?: Mounting | null
    setCurrentMounting?: (mounting: Mounting) => void
}

enum InputTypes {
    Address,
    Brigade,
    BuildingType,
    Comment
}

export function MountingsTable(props: IMountingsTableProps): JSX.Element {
    const {mountings, currentMounting, setCurrentMounting} = props

    const [autofocusInput, setAutofocusInput] = useState<InputTypes>(InputTypes.Comment)
    const [editingMounting, setEditingMounting] = useState<Mounting | null>(null)

    const updateMountingDate = useUpdateMountingDate()
    const updateClientAddress = useUpdateClientAddress()
    const updateMountingNote = useUpdateMountingNote()

    return (
        <div className={`${styles.table} standard-table`}>
            <table>
                <thead>
                    <tr>
                        <th>№ / Дата создания</th>
                        <th>Статус заказа</th>
                        <th>Дата отгрузки</th>
                        <th>Менеджер</th>
                        <th>Клиент</th>
                        <th>Тел. / Адрес</th>
                        <th>Дата монтажа</th>
                        <th>Коммент.</th>
                        {/* <th>Бриг.</th> */}
                    </tr>
                </thead>
                <tbody>
                    {mountings.map(mounting => {
                        const isEditing = mounting.id === editingMounting?.id
                        const isSelected = mounting.id === currentMounting?.id
                        const {order, owner, client, datetime} = mounting

                        if (isEditing) {
                            return (
                                <tr
                                    key={mounting.id}
                                    className={`${isSelected && 'selected'}`}
                                    onClick={() => setCurrentMounting && setCurrentMounting(mounting)}>
                                    <td className={styles.editCard} colSpan={9}>
                                        <div className={styles.editCardHeader}>
                                            <p>
                                                Монтаж {order.orderNumber} / {getDateDDMMYYYYFormat(new Date(mounting.createdAt))}
                                            </p>
                                            <CloseButton onClick={() => setEditingMounting(null)} />
                                        </div>
                                        <div className={styles.editCardMountingDate}>
                                            <p>Дата монтажа</p>
                                            <DateSelector
                                                date={new Date(datetime)}
                                                onChange={value => {
                                                    updateMountingDate(mounting, value)
                                                }}
                                            />
                                        </div>
                                        <div className={styles.editCardFieldsGrid}>
                                            <WithTitle title="Адрес">
                                                <Input
                                                    isDisabled={!client}
                                                    defaultValue={client?.address ?? ''}
                                                    useAutofocus={autofocusInput === InputTypes.Address}
                                                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                                    onChange={value => updateClientAddress(mounting, value)}
                                                />
                                            </WithTitle>
                                            {/* <WithTitle title="Бригада">
                                                <Input
                                                    defaultValue={mounting.brigade ?? ''}
                                                    useAutofocus={autofocusInput === InputTypes.Brigade}
                                                />
                                            </WithTitle>
                                            <WithTitle title="Тип дома">{<Input defaultValue={mounting.buildingType ?? ''} />}</WithTitle> */}
                                            <WithTitle title="Комментарий">
                                                <Input
                                                    defaultValue={mounting.note ?? ''}
                                                    useAutofocus={autofocusInput === InputTypes.Comment}
                                                    debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                                    onChange={value => updateMountingNote(mounting, value)}
                                                />
                                            </WithTitle>
                                        </div>
                                    </td>
                                </tr>
                            )
                        }

                        return (
                            <tr
                                key={mounting.id}
                                className={`${isSelected && 'selected'}`}
                                onClick={() => setCurrentMounting && setCurrentMounting(mounting)}>
                                <td style={{whiteSpace: 'nowrap'}}>
                                    {order.orderNumber} / {getDateDDMMYYYYFormat(new Date(mounting.createdAt))}
                                </td>
                                <td style={{color: getOrderStateColor(order.state)}}>{getOrderStateName(order.state)}</td>
                                <td style={{whiteSpace: 'nowrap'}}>
                                    {order.shippingDate.length > 0 ? (
                                        getDateDDMMYYYYFormat(new Date(order.shippingDate))
                                    ) : order.estShippingDate.length === 0 ? (
                                        <>&mdash;</>
                                    ) : (
                                        <> ~ {getDateDDMMYYYYFormat(new Date(order.estShippingDate))}</>
                                    )}
                                </td>
                                <td>{owner?.name || owner?.email}</td>
                                <td>{client?.name}</td>
                                <td>
                                    <div className={styles.buttonsHolder}>
                                        {client?.phone ? (
                                            <ArrowTooltip title={client.phone} placement="bottom">
                                                <PhoneButton phone={client.phone} />
                                            </ArrowTooltip>
                                        ) : (
                                            <PhoneButton />
                                        )}
                                        {client?.address ? (
                                            <ArrowTooltip title={client.address} placement="bottom">
                                                <GeoButton
                                                    onClick={e => {
                                                        e.stopPropagation()
                                                        setAutofocusInput(InputTypes.Address)
                                                        setEditingMounting(mounting)
                                                    }}
                                                />
                                            </ArrowTooltip>
                                        ) : (
                                            <GeoButton
                                                isDisabled={!client}
                                                onClick={e => {
                                                    e.stopPropagation()
                                                    setAutofocusInput(InputTypes.Address)
                                                    setEditingMounting(mounting)
                                                }}
                                            />
                                        )}
                                    </div>
                                </td>
                                <td>{getDateDDMMYYYYFormat(new Date(datetime))}</td>
                                <td>
                                    <div className={styles.center}>
                                        <CommentButton
                                            onClick={e => {
                                                e.stopPropagation()
                                                setAutofocusInput(InputTypes.Comment)
                                                setEditingMounting(mounting)
                                            }}
                                        />
                                    </div>
                                </td>
                                {/* <td>
                                    <div className={styles.center}>
                                        <BrigadeButton
                                            onClick={e => {
                                                e.stopPropagation()
                                                setAutofocusInput(InputTypes.Brigade)
                                                setEditingMounting(mounting)
                                            }}
                                        />
                                    </div>
                                </td> */}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
