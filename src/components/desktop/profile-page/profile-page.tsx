import * as React from 'react'
import {useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {replace} from 'connected-react-router'
import {ITab, TabsPanel} from '../tabs-panel/tabs-panel'
import {Affiliates} from '../../common/profile-page/affiliates/affiliates'
import {Margins} from '../../common/profile-page/margins/margins'
import {AdditionalServices} from '../../common/profile-page/additional-services/additional-services'
import {PartnerGoods} from '../../common/profile-page/partner-goods/partner-goods'
import {PrintSettings} from '../../common/profile-page/print-settings/print-settings'
import {IAppState} from '../../../redux/root-reducer'
import {NAV_PROFILE} from '../../../constants/navigation'
import {profileTabs, ProfileTabIds} from '../../common/profile-page/tabs'
import {usePermissions, useIsExternalUser} from '../../../hooks/permissions'
import {Dealers} from '../../common/profile-page/dealers/dealers'
import {useSetActiveTab, useLoadProfileSettingsAction} from '../../../hooks/account'
import {getDiscountForPartner} from '../../../helpers/account'
import styles from './profile-page.module.css'

export function ProfilePage(): JSX.Element | null {
    const partner = useSelector((state: IAppState) => state.account.partner)
    const nextDiscount = useSelector((state: IAppState) => state.account.nextDiscount)
    const currentDealer = useSelector((state: IAppState) => state.account.currentDealer)
    const login = useSelector((state: IAppState) => state.account.login)

    const dispatch = useDispatch()
    useLoadProfileSettingsAction()
    const permission = usePermissions()
    const isExternalUser = useIsExternalUser()
    const activeTab = useSetActiveTab()

    const getTabs = useCallback((): ITab[] => {
        return profileTabs.get(permission.isNonQualifiedOrBossManager).map(tab => {
            return {
                ...tab,
                dataTest: tab.id,
                active: tab.id === activeTab
            }
        })
    }, [activeTab, permission])

    const onUpdateTabsHandler = useCallback(
        (tabs: ITab[]) => {
            const newTab = tabs.find(item => item.active)
            if (newTab) {
                const url = `${NAV_PROFILE}?activeTab=${newTab.id}`
                dispatch(replace(url))
            }
        },
        [dispatch]
    )

    if (!partner) {
        return null
    }

    return (
        <div className={styles.page}>
            <TabsPanel tabs={getTabs()} isCentered={true} onUpdateTabs={onUpdateTabsHandler} />
            {currentDealer && (
                <div data-test={`${activeTab}-content`} className={styles.container}>
                    <div className={styles.dealer}>
                        <div className={styles.userAvatar}>{currentDealer.name[0]}</div>
                        <p className={styles.info}>
                            <b>
                                {currentDealer.name} {login ? `(${login})` : ''}
                            </b>
                            {!isExternalUser && <span className={styles.discount}>{getDiscountForPartner(partner, nextDiscount)}</span>}
                        </p>
                    </div>
                    {activeTab === ProfileTabIds.Affiliate && <Affiliates />}
                    {activeTab === ProfileTabIds.Margins && <Margins />}
                    {activeTab === ProfileTabIds.AdditionalServices && <AdditionalServices />}
                    {activeTab === ProfileTabIds.PartnerGoods && <PartnerGoods />}
                    {activeTab === ProfileTabIds.PrintSettings && <PrintSettings />}
                    {activeTab === ProfileTabIds.Dealers && <Dealers />}
                </div>
            )}
        </div>
    )
}
