import * as React from 'react'
import {getOrderStateName, getOrderStateColor} from '../../../model/framer/order-states'
import {OrderConstructionsCard} from '../order-constructions-card/order-constructions-card'
import {GoodsCard} from '../../common/goods-card/goods-card'
import {ServicesCard} from '../../common/services-card/services-card'
import {Construction} from '../../../model/framer/construction'
import {OrderDiscountEditor} from '../../common/order-discount-editor/order-discount-editor'
import {OrderTabSidebar} from './order-tab-sidebar/order-tab-sidebar'
import {useCallback, useEffect, useRef, useState} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {useAddGood2, useAddNewConstruction, useSaveOrder, useSetCurrentConstruction, useUpdateOrderDiscount} from '../../../hooks/orders'
import {getOrderPageUrl} from '../../../helpers/orders'
import {getTabs} from '../../../helpers/tabs'
import {useReplaceTo} from '../../../hooks/router'
import {SelectClient} from '../../common/select-client/select-client'
import {useAppContext} from '../../../hooks/app'
import {useClients} from '../../../hooks/clients'
import styles from './order-tab-content.module.css'
import {Button} from '../../common/button/button'
import {useShowTemplateSelector} from '../../../hooks/template-selector'
import {ConstructionRemoveButtonBorder} from '../../common/construction-remove-button/construction-remove-button-border'
import {useSetConfigContext} from '../../../hooks/builder'
import {ConfigContexts} from '../../../model/config-contexts'
import {DropdownMenu} from '../../common/dropdown-menu/dropdown-menu'
import {Action} from '../../../model/action'
import {ButtonWithHint} from '../../common/button/button-with-hint/button-with-hint'
import {HINTS} from '../../../constants/hints'
import {GUID} from '../../../helpers/guid'
import {AnalyticsManager} from '../../../analytics-manager'

export function OrderTabContent(): JSX.Element | null {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const factoryGoodGroups = useSelector((state: IAppState) => state.goods.factoryGoodGroups)
    const allFactoryGoods = useSelector((state: IAppState) => state.goods.factoryGoods)
    const partnerGoodGroups = useSelector((state: IAppState) => state.goods.partnerGoodGroups)
    const allPartnerGoods = useSelector((state: IAppState) => state.goods.partnerGoods)
    const orderAvailableServices = useSelector((state: IAppState) => state.services.orderAvailableServices)

    const replaceTo = useReplaceTo()
    const saveOrder = useSaveOrder()
    const clients = useClients()
    const showTemplateSelector = useShowTemplateSelector()
    const setConfigContext = useSetConfigContext()
    const setCurrentConstruction = useSetCurrentConstruction()
    const addNewConstruction = useAddNewConstruction()

    const addConstructionTab = useCallback(
        (construction: Construction) => {
            if (!currentOrder) {
                return
            }

            replaceTo(getOrderPageUrl(currentOrder.id, getTabs(search), construction.id))
        },
        [currentOrder, search, replaceTo]
    )

    const copyingConstruction = useCallback(
        (construction: Construction) => {
            if (!currentOrder) {
                return
            }
            const productIdOld: string[] = []
            const productIdNew: string[] = []
            const beamOld: string[] = []
            const beamNew: string[] = []
            const connectorOld: string[] = []
            const connectorNew: string[] = []

            construction.id = GUID()
            construction.products.map((product, idx) => {
                productIdOld[idx] = product.productGuid
                product.productGuid = GUID()
                productIdNew[idx] = product.productGuid
                product.frame.frameFillingGuid = GUID()
                product.frame.frameBeams.forEach((beam, idx) => {
                    beamOld[idx] = beam.beamGuid
                    beam.beamGuid = GUID()
                    beamNew[idx] = beam.beamGuid
                    beam.parentNodeId = product.frame.frameFillingGuid
                })
                product.connectors.forEach((connector, idx) => {
                    connectorOld[idx] = connector.connectedBeamGuid
                    beamOld.forEach((item, idx) => {
                        if (connector.connectedBeamGuid === item) {
                            connector.connectedBeamGuid = beamNew[idx]
                        }
                    })
                    connector.parentNodeId = product.productGuid
                    connector.beamGuid = GUID()
                    connectorNew[idx] = connector.beamGuid
                    connectorOld.forEach((item, idx) => {
                        if (connector.connectedBeamGuid === item) {
                            connector.connectedBeamGuid = connectorNew[idx - 1]
                        }
                    })
                })
                product.frame.impostBeams.forEach(impostBeam => {
                    impostBeam.beamGuid = GUID()
                    impostBeam.parentNodeId = product.frame.frameFillingGuid
                })
                product.frame.innerFillings.forEach(innerFilling => {
                    if (innerFilling.solid) {
                        innerFilling.solid.solidFillingGuid = GUID()
                        innerFilling.solid.parentNodeId = product.frame.frameFillingGuid
                    }
                    if (innerFilling.leaf) {
                        innerFilling.leaf.frameFillingGuid = GUID()
                        innerFilling.leaf.innerFillings.forEach(innerFilling => {
                            if (innerFilling.solid) {
                                innerFilling.solid.solidFillingGuid = GUID()
                                if (innerFilling.leaf) innerFilling.solid.parentNodeId = innerFilling.leaf.frameFillingGuid
                            }
                        })
                    }
                })
            })
            construction.products.forEach(product => {
                product.connectors.forEach(connector => {
                    productIdOld.forEach((item, idx) => {
                        if (connector.connectedProductGuid === item) {
                            connector.connectedProductGuid = productIdNew[idx]
                        }
                    })
                })
            })
            if (currentOrder) {
                construction.position = currentOrder.constructions.length + 1
            }
            addNewConstruction(construction)
        },
        [addNewConstruction]
    )

    const addGood2 = useAddGood2()
    const updateOrderDiscount = useUpdateOrderDiscount()
    const {isTablet} = useAppContext()

    const [hasScrollbar, setHasScrollbar] = useState(false)
    const windowSize = useSelector((state: IAppState) => state.browser.windowSize)
    const tabRef = useRef<HTMLDivElement | null>(null)

    useEffect(() => {
        if (!tabRef.current) {
            return
        }

        setHasScrollbar(tabRef.current.scrollHeight !== tabRef.current.clientHeight)
    }, [windowSize, currentOrder])

    useEffect(() => {
        if (currentConstruction) return
        if (!currentOrder) return
        setCurrentConstruction(currentOrder.constructions[0])
    }, [])

    if (!currentOrder) {
        return null
    }

    const orderActions: Action[] = []
    if (currentConstruction && !currentOrder.readonly) {
        orderActions.push(
            new Action('Копировать конструкцию', () => {
                currentOrder.constructions.length
                const constructionClone = currentConstruction.clone
                AnalyticsManager.trackClickCopyingConstruction()
                copyingConstruction(constructionClone)
            })
        )
    }

    const orderGoods = currentOrder.goods.filter(g => !g.constructionId)
    const constructionGoods = currentOrder.goods.filter(g => g.constructionId)
    const orderServices = currentOrder.services.filter(s => !s.constructionId)
    const constructionServices = currentOrder.services.filter(s => s.constructionId)

    return (
        <div className={`${styles.orderTabContent} ${hasScrollbar && styles.hasScrollbar} ${isTablet ? styles.tablet : ''}`} ref={tabRef}>
            <div className={styles.body}>
                <div className={styles.bodyHeader}>
                    <p>
                        Создан:&nbsp;<span>{getDateDDMMYYYYFormat(currentOrder.createdAt ? new Date(currentOrder.createdAt) : null)}</span>
                    </p>
                    <div className={styles.clientSelector}>
                        <p className={styles.clientSelectLabel}>Клиент: </p>
                        <SelectClient order={currentOrder} clients={clients} />
                    </div>
                    <p>
                        Статус:&nbsp;
                        <span className={styles.orderState} style={{background: getOrderStateColor(currentOrder.state)}}>
                            {getOrderStateName(currentOrder.state)}
                        </span>
                    </p>
                </div>
                <div className={styles.bodyHeaderButtons}>
                    <div className={styles.bodyHeaderButton}>
                        <Button
                            buttonStyle="with-fill"
                            isDisabled={currentOrder.readonly}
                            className={styles.addConstruction}
                            onClick={() => {
                                showTemplateSelector(null)
                            }}>
                            Добавить конструкцию
                        </Button>
                        <ConstructionRemoveButtonBorder
                            order={currentOrder}
                            construction={currentConstruction ?? currentOrder.constructions[0]}
                            onChangeOrder={saveOrder}
                            className={styles.removeButton}
                        />
                        <ButtonWithHint hint={HINTS.orderReplaceConstructionParams} className={styles.replaceButton}>
                            <Button
                                buttonStyle="with-border"
                                isDisabled={currentOrder.readonly}
                                className={styles.className}
                                onClick={() => {
                                    setCurrentConstruction(currentOrder.constructions[0])
                                    setConfigContext(ConfigContexts.ConstructionParams)
                                }}>
                                Заменить во всех конструкциях
                            </Button>
                        </ButtonWithHint>
                    </div>
                    <DropdownMenu items={orderActions} />
                </div>
                <OrderConstructionsCard order={currentOrder} onChangeOrder={saveOrder} onEditConstructionClick={addConstructionTab} />
                <GoodsCard
                    className={styles.card}
                    title="Доп. материалы к заказу"
                    isDisabled={currentOrder.readonly}
                    goods={orderGoods}
                    allFactoryGoodGroups={factoryGoodGroups}
                    allFactoryGoods={allFactoryGoods}
                    allPartnerGoodGroups={partnerGoodGroups}
                    allPartnerGoods={allPartnerGoods}
                    onAddGood={good => addGood2(currentOrder, good)}
                    onChange={(goods, action) => {
                        const orderClone = currentOrder.clone
                        orderClone.goods = [...constructionGoods, ...goods]
                        orderClone.addAction(action)
                        saveOrder(orderClone)
                    }}
                />
                <ServicesCard
                    className={styles.card}
                    title="Услуги к заказу"
                    isDisabled={currentOrder.readonly}
                    services={orderServices}
                    availableServices={orderAvailableServices}
                    onChange={(services, action) => {
                        const orderClone = currentOrder.clone
                        orderClone.services = [...constructionServices, ...services]
                        orderClone.addAction(action)
                        saveOrder(orderClone)
                    }}
                />
                <OrderDiscountEditor order={currentOrder} onChangeDiscount={discount => updateOrderDiscount(currentOrder, discount)} />
            </div>
            <OrderTabSidebar />
        </div>
    )
}
