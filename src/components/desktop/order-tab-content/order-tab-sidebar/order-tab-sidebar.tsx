import * as React from 'react'
import {useCallback} from 'react'
import {NewSidebar} from '../../new-sidebar/new-sidebar'
import {NewSidebarItem} from '../../new-sidebar/new-sidebar-item/new-sidebar-item'
import {DateSelector} from '../../../common/date-selector/date-selector'
import {useSelector, useDispatch} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {useAddMounting} from '../../../../hooks/mountings'
import {updateMountingDate} from '../../../../redux/orders/actions'
import {Order} from '../../../../model/framer/order'
import {ShippingTypes} from '../../../../model/framer/shipping-types'
import {usePermissions} from '../../../../hooks/permissions'
import {ConfirmDealerLink} from '../../../common/confirm-dealer-link/confirm-dealer-link'
import styles from './order-tab-sidebar.module.css'
import {Button} from '../../../common/button/button'
import {FactoryTypes} from '../../../../model/framer/factory-types'
import {useLoadPassport} from '../../../../hooks/orders'
import {Link} from '../../../common/link/link'

export function OrderTabSidebar(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const partner = useSelector((state: IAppState) => state.account.partner)

    const loadPassport = useLoadPassport()

    const dispatch = useDispatch()
    const addMounting = useAddMounting()
    const permission = usePermissions()

    const addMountingAction = useCallback(
        (order: Order, mountingDate: Date) => {
            if (order.mountingDate) {
                dispatch(updateMountingDate(order, mountingDate))
            } else {
                addMounting(order, mountingDate)
            }
        },
        [addMounting, dispatch]
    )

    if (!currentOrder) {
        return null
    }

    const isCheckDateShipping = new Date().getTime() > new Date(currentOrder.shippingDate).getTime() + 86400000

    return (
        <NewSidebar headerTitle="Ближайшие запланированные события" className={styles.orderTabSidebar}>
            {permission.isNonQualified ? (
                <ConfirmDealerLink className={styles.permissionError} title="Для добавления событий необходимо" />
            ) : (
                <NewSidebarItem className={styles.item}>
                    <p className={styles.itemTitle}>Дата монтажа</p>
                    <DateSelector
                        date={currentOrder.mountingDate ? new Date(currentOrder.mountingDate) : null}
                        dataTest="order-mounting-date"
                        isDisabled={currentOrder.isCancelled}
                        onChange={date => addMountingAction(currentOrder, date)}
                    />
                </NewSidebarItem>
            )}
            {currentOrder.windrawOrderNumber ? (
                currentOrder.shippingType === ShippingTypes.Delivery ? (
                    <>
                        <NewSidebarItem className={`${styles.item} ${styles.shipping}`}>
                            <p className={styles.itemTitle}>Адрес доставки</p>
                            {currentOrder.shippingAddress ? currentOrder.shippingAddress.toString() : ''}
                        </NewSidebarItem>
                        <NewSidebarItem className={`${styles.item} ${styles.shipping}`}>
                            <p className={styles.itemTitle}>Получатель</p>
                            {currentOrder.contactPerson?.name}
                            <br />
                            {currentOrder.contactPerson?.phone}
                        </NewSidebarItem>
                    </>
                ) : (
                    <NewSidebarItem className={`${styles.item} ${styles.shipping}`}>
                        <p className={styles.itemTitle}>Склад самовывоза</p>
                        {currentOrder.warehouse?.name}
                    </NewSidebarItem>
                )
            ) : (
                ''
            )}
            {currentOrder.deliveryInfo[0] && (
                <NewSidebarItem className={`${styles.item} ${styles.shipping}`}>
                    <p className={styles.itemTitle}>Информация о доставке</p>
                    {currentOrder.deliveryInfo.map((elem, i) => {
                        return (
                            <>
                                <p className={styles.itemTitle}>Рейс {i + 1}</p>
                                <span>Телефон водителя: {elem.driverPhone}</span>
                                <span>Гос. номер машины: {elem.carNumber}</span>
                            </>
                        )
                    })}
                </NewSidebarItem>
            )}
            {currentOrder.windrawOrderNumber && partner?.factoryId === FactoryTypes.Perm && isCheckDateShipping && (
                <NewSidebarItem className={`${styles.item} ${styles.downloadLink}`}>
                    <Button
                        dataTest="order-tab-sidebar-download-passport-button"
                        dataId="order-tab-sidebar-download-passport-button"
                        buttonStyle="with-border"
                        isDisabled={permission.isNonQualified}
                        className={styles.passportButton}
                        onClick={e => {
                            e.stopPropagation()
                            loadPassport(currentOrder)
                        }}>
                        Скачать паспорта качества
                    </Button>
                    <Link
                        className={`${styles.link} ${styles.linkSertificates}`}
                        href="https://amegazavod.ru/sertificates_okna/?_ga=2.11026932.1769129912.1620883560-293434870.1617623849"
                        title="Сертификаты"
                    />
                </NewSidebarItem>
            )}
        </NewSidebar>
    )
}
