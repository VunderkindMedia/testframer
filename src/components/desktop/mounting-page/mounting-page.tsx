import * as React from 'react'
import {useCallback} from 'react'
import {Button} from '../../common/button/button'
import {PageTitle} from '../../common/page-title/page-title'
import {CalendarButton} from '../../common/button/calendar-button/calendar-button'
import {WithTitle} from '../../common/with-title/with-title'
import {Input} from '../../common/input/input'
import {Textarea} from '../../common/textarea/textarea'
// import {RemoveButton} from '../../common/button/remove-button/remove-button'
// import {AddButton} from '../../common/button/add-button/add-button'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
// import {Select} from '../../common/select/select'
// import {ListItem} from '../../../model/list-item'
import {Modal} from '../../common/modal/modal'
import {
    useUpdateMountingDate,
    useUpdateMountingNote,
    useUpdateClientAddress,
    useDeleteMountingModal,
    useDownloadMountingsReport,
    useMountingFromSearch
} from '../../../hooks/mountings'
import {useGoTo} from '../../../hooks/router'
import {NAV_MOUNTINGS} from '../../../constants/navigation'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../constants/settings'
import styles from './mounting-page.module.css'

export function MountingPage(): JSX.Element | null {
    const mounting = useMountingFromSearch()
    const updateMountingDate = useUpdateMountingDate()
    const updateClientAddress = useUpdateClientAddress()
    const updateMountingNote = useUpdateMountingNote()
    const downloadMountingsReport = useDownloadMountingsReport()
    const goTo = useGoTo()
    const [modalModel, showModal] = useDeleteMountingModal()

    const renderDate = useCallback((title: string, value?: string, onSelect?: (date: Date) => void): JSX.Element => {
        return (
            <div className={styles.date}>
                <span>{title}</span>
                <span className={styles.selectedDate}>{value ? getDateDDMMYYYYFormat(new Date(value)) : ''}</span>
                <CalendarButton date={value ? new Date(value) : undefined} onSelect={onSelect} />
            </div>
        )
    }, [])

    if (!mounting) {
        return null
    }

    const {order, createdAt, client, note, datetime} = mounting

    return (
        <div className={styles.page}>
            {modalModel && <Modal model={modalModel} />}
            <div className={styles.header}>
                <PageTitle title={`Монтажи / ${order.fullOrderNumber} / ${getDateDDMMYYYYFormat(new Date(createdAt))}`} />
                <Button
                    buttonStyle="with-border"
                    className={styles.button}
                    onClick={() =>
                        showModal(mounting, () => {
                            goTo(NAV_MOUNTINGS)
                        })
                    }>
                    Удалить
                </Button>
                <Button buttonStyle="with-border" className={styles.button} onClick={() => downloadMountingsReport(mounting)}>
                    Скачать
                </Button>
            </div>
            <div className={styles.form}>
                <div className={styles.dateList}>
                    {/* {renderDate('Дата замера', mounting.measuringDate)}
                    {renderDate('Дата отгрузки', mounting.shippingDate)}
                    {renderDate('Дата доставки', mounting.deliveryDate)} */}
                    {renderDate('Дата монтажа', datetime, value => {
                        updateMountingDate(mounting, value)
                    })}
                </div>
                <div className={styles.fieldList}>
                    {client && (
                        <WithTitle title="Адрес" className={`${styles.field} ${styles.address}`}>
                            <Input
                                className={styles.fieldInput}
                                debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                                value={client?.address}
                                onChange={value => updateClientAddress(mounting, value)}
                            />
                        </WithTitle>
                    )}
                    {/* <WithTitle title="Тип дома" className={styles.field}>
                        <Input className={styles.fieldInput} value={mounting.buildingType} />
                    </WithTitle>
                    <WithTitle title="Сотрудники" className={styles.field}>
                        <Select<number | undefined>
                            className={styles.select}
                            isMultiSelect={true}
                            items={employeesList}
                            selectedItems={
                                mounting.executorsIds?.length
                                    ? employeesList.filter(e => e.data !== undefined && mounting.executorsIds.includes(e.data))
                                    : [employeesList[0]]
                            }
                        />
                    </WithTitle> */}
                    <WithTitle title="Комментарий" className={styles.field}>
                        <Textarea
                            rows={3}
                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                            className={styles.fieldInput}
                            value={note}
                            onChange={value => updateMountingNote(mounting, value)}
                        />
                    </WithTitle>
                </div>
                {/* <p className={styles.eventTitle}>Другие события</p>
                <div className={styles.event}>
                    <Input className={styles.eventInput} />
                    <RemoveButton />
                </div>
                <AddButton title="Добавить" className={styles.addButton} /> */}
            </div>
        </div>
    )
}
