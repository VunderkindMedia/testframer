import * as React from 'react'
import {CalendarEventTypes, getCalendarEventName} from '../../../../model/framer/calendar/calendar-event-types'
import styles from './event-type.module.css'
import {getClassNames} from '../../../../helpers/css'

interface IEventTypeProps {
    type: CalendarEventTypes
    className?: string
    isCentered?: boolean
}

interface IEventTypeInfo {
    title: string
    className: string
}

const eventTypeInfo: {[key in CalendarEventTypes]: IEventTypeInfo} = {
    [CalendarEventTypes.Mounting]: {
        title: getCalendarEventName(CalendarEventTypes.Mounting),
        className: styles.mounting
    },
    [CalendarEventTypes.Shipping]: {
        title: getCalendarEventName(CalendarEventTypes.Shipping),
        className: styles.shipping
    },
    [CalendarEventTypes.Measuring]: {
        title: getCalendarEventName(CalendarEventTypes.Measuring),
        className: styles.measuring
    },
    [CalendarEventTypes.OtherEvent]: {
        title: getCalendarEventName(CalendarEventTypes.OtherEvent),
        className: styles.request
    }
}

export function EventType(props: IEventTypeProps): JSX.Element {
    const {className, isCentered = false} = props
    const type = eventTypeInfo[props.type]
    return (
        <div className={getClassNames(`${styles.type} ${type.className} ${className ?? ''}`, {[styles.centered]: isCentered})}>
            <span>{type.title}</span>
        </div>
    )
}
