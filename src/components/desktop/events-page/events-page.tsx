import * as React from 'react'
import {useState, useCallback, useEffect, useRef} from 'react'
import {useSelector} from 'react-redux'
import {PageTitle} from '../../common/page-title/page-title'
import {CalendarGrid} from './calendar-grid/calendar-grid'
import {CalendarList} from '../../common/calendar-list/calendar-list'
import {Sidebar} from './sidebar/sidebar'
import {getClassNames} from '../../../helpers/css'
import {ReactComponent as GridIcon} from './resources/grid_icon.inline.svg'
import {ReactComponent as ListIcon} from './resources/list_icon.inline.svg'
import {SHOW_CALENDAR_AS_LIST} from '../../../constants/local-storage'
import {IAppState} from '../../../redux/root-reducer'
import {getDateWithDaysOffset, getMonthEndDate, getMonthStartDate, getDayEndDate} from '../../../helpers/date'
import {MonthSelector} from '../../common/month-selector/month-selector'
import {useSetEventsFilters, useEventsFromSearchByDay} from '../../../hooks/events'
import {getEventsFilterParams} from '../../../helpers/events'
import styles from './events-page.module.css'

export function EventsPage(): JSX.Element {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const windowSize = useSelector((state: IAppState) => state.browser.windowSize)
    const [isListMode, setIsListMode] = useState(localStorage.getItem(SHOW_CALENDAR_AS_LIST) === 'true')
    const [hasScrollbar, setHasScrollbar] = useState(false)

    const pageRef = useRef<HTMLDivElement>(null)

    const setMountingsFilter = useSetEventsFilters()
    const events = useEventsFromSearchByDay()
    const filterParams = getEventsFilterParams(search)

    useEffect(() => {
        localStorage.setItem(SHOW_CALENDAR_AS_LIST, JSON.stringify(isListMode))
    }, [isListMode])

    useEffect(() => {
        if (!pageRef.current) {
            return
        }

        setHasScrollbar(pageRef.current.scrollHeight !== pageRef.current.clientHeight)
    }, [windowSize, events, isListMode])

    const changeViewMode = useCallback((isList: boolean) => {
        setIsListMode(isList)
    }, [])

    const startDate = getDateWithDaysOffset(filterParams.from, -1)
    const endDate = getDateWithDaysOffset(filterParams.to, 1)

    return (
        <div className={`${styles.events} ${hasScrollbar && styles.hasScrollbar}`} ref={pageRef}>
            <div className={styles.header}>
                <div className={styles.headerContent}>
                    <PageTitle title="Календарь" className={styles.title} />
                    <button
                        className={getClassNames(styles.viewModeButton, {
                            [styles.active]: isListMode
                        })}
                        onClick={() => changeViewMode(true)}>
                        <ListIcon />
                    </button>
                    <button
                        className={getClassNames(styles.viewModeButton, {
                            [styles.active]: !isListMode
                        })}
                        onClick={() => changeViewMode(false)}>
                        <GridIcon />
                    </button>
                    <MonthSelector
                        date={filterParams.from}
                        onMonthChange={date => {
                            setMountingsFilter({...filterParams, from: getMonthStartDate(date), to: getMonthEndDate(date)})
                        }}
                        className={styles.monthSelector}
                    />
                </div>
            </div>
            {isListMode ? (
                <CalendarList startDate={filterParams.from} endDate={getDayEndDate(filterParams.to)} events={events} />
            ) : (
                <CalendarGrid startDate={startDate} endDate={endDate} month={filterParams.from} events={events} />
            )}
            <Sidebar className={styles.sidebar} filterParams={filterParams} onChangeFilterParams={setMountingsFilter} />
        </div>
    )
}
