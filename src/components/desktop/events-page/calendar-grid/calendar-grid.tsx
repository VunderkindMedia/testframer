import * as React from 'react'
import {useCallback, useState} from 'react'
import {
    getWeekStartDate,
    getWeekEndDate,
    getAllDatesFromRange,
    getDateDDMMYYYYFormat,
    isMonthDate,
    getDayStartDate
} from '../../../../helpers/date'
import {getClassNames} from '../../../../helpers/css'
import {CalendarEvent} from '../../../../model/framer/calendar/calendar-event'
import {CalendarEventTypes, getCalendarEventName} from '../../../../model/framer/calendar/calendar-event-types'
import {EventsModal} from '../events-modal/events-modal'
import {useAppContext} from '../../../../hooks/app'
import styles from './calendar-grid.module.css'

interface ICalendarGridProps {
    startDate: Date
    endDate: Date
    month: Date
    events: Map<string, CalendarEvent[]>
}

export function CalendarGrid(props: ICalendarGridProps): JSX.Element {
    const {startDate, endDate, month, events} = props
    const [isModalOpened, setIsModalOpened] = useState(false)
    const [dayEvents, setDayEvents] = useState<CalendarEvent[]>([])
    const dates = getAllDatesFromRange(getWeekStartDate(startDate), getWeekEndDate(endDate))
    const {isTablet} = useAppContext()

    const toggleModal = useCallback((isOpened: boolean) => {
        setIsModalOpened(isOpened)
    }, [])

    const renderEvents = (type: CalendarEventTypes, dayEvents: CalendarEvent[], className: string): JSX.Element | undefined => {
        if (!dayEvents) {
            return
        }
        const eventList = dayEvents.filter(e => e.eventType === type)
        const count = eventList.length

        if (count === 0) {
            return
        }

        return (
            <p className={styles.event}>
                <span>{getCalendarEventName(type)}</span>
                <span className={`${styles.eventCount} ${className}`}>{count}</span>
            </p>
        )
    }

    return (
        <div>
            <p className={styles.header}>
                <span className={styles.label}>понедельник</span>
                <span className={styles.label}>вторник</span>
                <span className={styles.label}>среда</span>
                <span className={styles.label}>четверг</span>
                <span className={styles.label}>пятница</span>
                <span className={styles.label}>суббота</span>
                <span className={styles.label}>воскресенье</span>
            </p>
            <div className={styles.calendar}>
                {dates.map(date => {
                    const dateStr = getDateDDMMYYYYFormat(date)
                    const dayEvents = events.get(dateStr)
                    const isLastDay = date.getTime() < new Date().getTime()
                    const isPresentDay = getDayStartDate(date).getTime() === getDayStartDate(new Date()).getTime()
                    const isCurrentMonthDay = isMonthDate(date, month)

                    return (
                        <div
                            key={dateStr}
                            className={getClassNames(styles.day, {
                                [styles.tablet]: isTablet,
                                [styles.last]: isLastDay && isCurrentMonthDay,
                                [styles.present]: isPresentDay
                            })}>
                            <span className={getClassNames(styles.dayNumber, {[styles.disabled]: !isCurrentMonthDay})}>{date.getDate()}</span>
                            {dayEvents && (
                                <>
                                    {renderEvents(CalendarEventTypes.OtherEvent, dayEvents, styles.request)}
                                    {renderEvents(CalendarEventTypes.Measuring, dayEvents, styles.measuring)}
                                    {renderEvents(CalendarEventTypes.Shipping, dayEvents, styles.shipping)}
                                    {renderEvents(CalendarEventTypes.Mounting, dayEvents, styles.mounting)}
                                    {dayEvents.length > 0 && (
                                        <button
                                            className={styles.allEvents}
                                            onClick={() => {
                                                setDayEvents(dayEvents)
                                                toggleModal(true)
                                            }}>
                                            Все события
                                        </button>
                                    )}
                                </>
                            )}
                        </div>
                    )
                })}
            </div>
            {isModalOpened && <EventsModal onClose={() => toggleModal(false)} events={dayEvents} />}
        </div>
    )
}
