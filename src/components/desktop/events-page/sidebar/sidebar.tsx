import * as React from 'react'
import {NewSidebar} from '../../new-sidebar/new-sidebar'
import {NewSidebarItem} from '../../new-sidebar/new-sidebar-item/new-sidebar-item'
import {WithTitle} from '../../../common/with-title/with-title'
// import {EventType} from '../event-type/event-type'
// import {CalendarEventTypes} from '../../../../model/framer/calendar/calendar-event-types'
import {Select} from '../../../common/select/select'
import {ListItem} from '../../../../model/list-item'
import {useEmployees, useAffiliates} from '../../../../hooks/account'
import {useClients} from '../../../../hooks/clients'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import {IEventsFilterParams} from '../../../../helpers/events'
import styles from './sidebar.module.css'

interface ISidebarProps {
    filterParams: IEventsFilterParams
    className?: string
    onChangeFilterParams: (params: IEventsFilterParams) => void
}

export function Sidebar(props: ISidebarProps): JSX.Element {
    const {filterParams, className, onChangeFilterParams} = props
    const {filials, clients, owners} = filterParams

    const employeesList = useEmployees()
    const affiliatesList = useAffiliates()
    const clientsList = useClients()

    const affiliatesListItems = affiliatesList?.length
        ? [new ListItem('Все', 'Все', -1), ...affiliatesList.map(({id, name}) => new ListItem(String(id), name, id))]
        : []

    const clientsListItems = [new ListItem('Все', 'Все', -1), ...clientsList.map(({id, name}) => new ListItem(String(id), name, id))]
    const employeesListItems = employeesList?.length
        ? [new ListItem('Все', 'Все', -1), ...employeesList.map(({id, name}) => new ListItem(String(id), name, id))]
        : []

    // const eventTypeFilter = useCallback(
    //     (type: CalendarEventTypes) => {
    //         const currentStateIndex = states.indexOf(type)
    //         const newStates =
    //             currentStateIndex === -1 ? [...states, type] :
    // [...states.slice(0, currentStateIndex), ...states.slice(currentStateIndex + 1)]

    //         return (
    //             <button
    //                 className={getClassNames(styles.status, {
    //                     [styles.active]: states.includes(type)
    //                 })}
    //                 onClick={() => onChangeFilterParams({...filterParams, states: newStates})}>
    //                 <EventType type={type} isCentered={true} />
    //             </button>
    //         )
    //     },
    //     [filterParams, states, onChangeFilterParams]
    // )

    return (
        <NewSidebar headerTitle="Фильтр" className={className}>
            {affiliatesListItems.length > 0 && (
                <NewSidebarItem className={styles.item}>
                    <WithTitle title="Филиал">
                        <Select<number | undefined>
                            isMultiSelect={true}
                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                            items={affiliatesListItems}
                            selectedItems={
                                filials?.length
                                    ? affiliatesListItems.filter(fl => fl.data && filials.includes(fl.data))
                                    : [affiliatesListItems[0]]
                            }
                            onMultiSelect={items => {
                                const filialIds: number[] = []
                                items.map(item => {
                                    if (item.data && item.data !== -1) {
                                        filialIds.push(item.data)
                                    }
                                })
                                onChangeFilterParams({...filterParams, filials: filialIds})
                            }}
                        />
                    </WithTitle>
                </NewSidebarItem>
            )}
            {employeesListItems.length > 0 && (
                <NewSidebarItem className={styles.item}>
                    <WithTitle title="Менеджер">
                        <Select<number | undefined>
                            items={employeesListItems}
                            isMultiSelect={true}
                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                            selectedItems={
                                owners.length ? employeesListItems.filter(e => e.data && owners.includes(e.data)) : [employeesListItems[0]]
                            }
                            onMultiSelect={items => {
                                const employeesIds: number[] = []
                                items.map(({data}) => {
                                    if (data && data !== -1) {
                                        employeesIds.push(data)
                                    }
                                })
                                onChangeFilterParams({...filterParams, owners: employeesIds})
                            }}
                        />
                    </WithTitle>
                </NewSidebarItem>
            )}
            {clientsListItems.length > 0 && (
                <NewSidebarItem className={styles.item}>
                    <WithTitle title="Клиент">
                        <Select<number | undefined>
                            items={clientsListItems}
                            isMultiSelect={true}
                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                            shouldShowFilter={true}
                            selectedItems={
                                clients.length ? clientsListItems.filter(({data}) => data && clients.includes(data)) : [clientsListItems[0]]
                            }
                            onMultiSelect={items => {
                                const clientIds: number[] = []
                                items.map(item => {
                                    if (item.data && item.data !== -1) {
                                        clientIds.push(item.data)
                                    }
                                })
                                onChangeFilterParams({...filterParams, clients: clientIds})
                            }}
                        />
                    </WithTitle>
                </NewSidebarItem>
            )}
            {/* <NewSidebarItem className={styles.item}>
                <WithTitle title="Статус" className={styles.filter}>
                    <button
                        className={getClassNames(`${styles.status} ${styles.allStatus}`, {[styles.active]: !states.length})}
                        onClick={() => onChangeFilterParams({...filterParams, states: []})}>
                        Все
                    </button>
                    {eventTypeFilter(CalendarEventTypes.OtherEvent)}
                    {eventTypeFilter(CalendarEventTypes.Measuring)}
                    {eventTypeFilter(CalendarEventTypes.Shipping)}
                    {eventTypeFilter(CalendarEventTypes.Mounting)}
                </WithTitle>
            </NewSidebarItem> */}
        </NewSidebar>
    )
}
