import * as React from 'react'
import {ModalModel} from '../../../common/modal/modal-model'
import {useEffect, useState} from 'react'
import {CalendarEvent} from '../../../../model/framer/calendar/calendar-event'
import {CalendarEventTypes} from '../../../../model/framer/calendar/calendar-event-types'
import {getDateDDMMMMYYYYFormat, getWeekdayName} from '../../../../helpers/date'
import {Modal} from '../../../common/modal/modal'
import {EventList} from '../../../common/event-list/event-list'
import styles from './events-modal.module.css'

interface IEventsModalProps {
    events: CalendarEvent[]
    onClose: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

export function EventsModal(props: IEventsModalProps): JSX.Element | null {
    const {events, onClose} = props

    const [model, setModel] = useState<ModalModel | null>(null)

    useEffect(() => {
        const date = events[0].originalEvent ? new Date(events[0].originalEvent.datetime) : null

        const model = ModalModel.Builder.withClassName(styles.modal)
            .withBody(
                <div>
                    <p className={styles.title}>{date && `${getDateDDMMMMYYYYFormat(date)} (${getWeekdayName(date)})`}</p>
                    <div className={styles.content}>
                        <EventList
                            events={events.filter(e => e.eventType === CalendarEventTypes.Mounting)}
                            type={CalendarEventTypes.Mounting}
                        />
                    </div>
                </div>
            )
            .withOnCloseFunc(onClose)
            .build()

        setModel(model)
    }, [events, onClose])

    if (!model) {
        return null
    }

    return <Modal model={model} />
}
