import * as React from 'react'
import styles from './orders-page-table.module.css'
import {Order} from '../../../model/framer/order'
import {getOrderStateName, getOrderStateColor} from '../../../model/framer/order-states'
import {Client} from '../../../model/framer/client'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ReactComponent as CacheIcon} from '../../../resources/images/cache-icon.inline.svg'
import {HINTS} from '../../../constants/hints'
import {VisibilityButton} from '../../common/button/visibility-button/visibility-button'
import {ButtonWithHint} from '../../common/button/button-with-hint/button-with-hint'
import {Link} from 'react-router-dom'
import {getOrderLink} from '../../../helpers/orders'
import {SvgIconConstructionDrawer} from '../../common/svg-icon-construction-drawer/svg-icon-construction-drawer'
import {Colors} from '../../../constants/colors'
import {getCurrency} from '../../../helpers/currency'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {getAmegaCostTooltip} from '../../../helpers/account'
import {useIsSubdealerOrder} from '../../../hooks/orders'
import {useIsMainDealer} from '../../../hooks/permissions'
import {SelectClient} from '../../common/select-client/select-client'
import {WithHint} from '../../common/with-hint/with-hint'
import {ArrowTooltip} from '../../common/arrow-tooltip/arrow-tooltip'

interface IOrdersPageTableProps {
    orders: Order[]
    currentOrder: Order | null
    setCurrentOrder: (order: Order | null) => void
    shouldShowRealCost: boolean
    setShouldShowRealCost: (show: boolean) => void
    clients: Client[]
    isFramerPointsAvailable: boolean
    setClientToOrder?: (order: Order, client: Client) => void
}

export function OrdersPageTable(props: IOrdersPageTableProps): JSX.Element {
    const {orders, clients, currentOrder, setCurrentOrder, shouldShowRealCost, isFramerPointsAvailable, setShouldShowRealCost} = props
    const isMainDealer = useIsMainDealer()
    const getIsSubdealerOrder = useIsSubdealerOrder()

    return (
        <div className={`standard-table ${styles.ordersPageTable}`}>
            <table data-test="orders-page-table">
                <thead>
                    <tr>
                        <th>Дата создания</th>
                        <th>
                            <WithHint hint={HINTS.orderNumber}>Номер</WithHint>
                        </th>
                        <th>Конструкции</th>
                        <th>Клиент</th>
                        <th>
                            <WithHint className={styles.costTitle}>
                                <>
                                    Сумма&nbsp;
                                    <ButtonWithHint hint={HINTS.orderPriceToggle} className={styles.visibilityButtonWrapper}>
                                        <VisibilityButton
                                            isVisible={shouldShowRealCost}
                                            onClick={() => setShouldShowRealCost(!shouldShowRealCost)}
                                        />
                                    </ButtonWithHint>
                                </>
                            </WithHint>
                        </th>
                        {isMainDealer && <th>Сотрудник</th>}
                        <th>Статус заказа</th>
                        <th>Дата отгрузки</th>
                    </tr>
                </thead>
                <tbody>
                    {orders.map((order, idx) => {
                        const isSubdealerOrder = getIsSubdealerOrder(order)

                        return (
                            <tr
                                data-test={`order-table-row-${order.orderNumber}`}
                                className={`${currentOrder?.id === order.id && 'selected'} ${order.state} ${
                                    order.haveEnergyProducts && styles.energy
                                }`}
                                key={order.id}
                                onClick={() => setCurrentOrder(order)}
                                data-row={idx}>
                                <td>{getDateDDMMYYYYFormat(new Date(order.createdAt))}</td>
                                <td>
                                    <Link to={getOrderLink(order)} className={styles.orderNumbers}>
                                        <p>{order.orderNumber}</p>
                                        {order.windrawOrderNumber && <p>{order.windrawOrderNumber}</p>}
                                        {order.windrawGoodsOrderNumber && <p>{order.windrawGoodsOrderNumber}</p>}
                                    </Link>
                                </td>
                                <td>
                                    <div className={styles.orderConstructions}>
                                        {order.constructions.slice(0, 3).map(construction => (
                                            <SvgIconConstructionDrawer
                                                key={construction.id}
                                                construction={construction}
                                                maxWidth={280}
                                                maxHeight={28}
                                            />
                                        ))}
                                        {order.constructions.length > 3 && (
                                            <p className={styles.moreText} style={{color: Colors.Gray}}>
                                                ...
                                            </p>
                                        )}
                                    </div>
                                </td>
                                <td>
                                    <SelectClient order={order} clients={clients} />
                                </td>
                                <td className={`${styles.price} ${shouldShowRealCost && styles.showRealCost}`}>
                                    {order.totalCost === 0 ? (
                                        <>&mdash;</>
                                    ) : isSubdealerOrder && order.filial?.isExternal ? (
                                        <>
                                            {getCurrency(order.totalCost)} / {getCurrency(order.totalAmegaCostToDisplay)}{' '}
                                            {order.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                                            {isFramerPointsAvailable ? (
                                                <ArrowTooltip title={getAmegaCostTooltip(order, true)}>
                                                    <span className={styles.amegaCost}>({getCurrency(order.totalAmegaCost)})</span>
                                                </ArrowTooltip>
                                            ) : (
                                                <span>({getCurrency(order.totalAmegaCost)})</span>
                                            )}
                                        </>
                                    ) : (
                                        <>
                                            {getCurrency(order.totalCost)} {order.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                                            {isFramerPointsAvailable ? (
                                                <ArrowTooltip title={getAmegaCostTooltip(order, false)}>
                                                    <span className={styles.amegaCost}>({getCurrency(order.totalAmegaCostToDisplay)})</span>
                                                </ArrowTooltip>
                                            ) : (
                                                <span>({getCurrency(order.totalAmegaCostToDisplay)})</span>
                                            )}
                                        </>
                                    )}
                                </td>
                                {isMainDealer && (
                                    <td>
                                        {isSubdealerOrder && (
                                            <>
                                                {order.owner ? order.owner.name : ''}
                                                {order.filial ? <span className={styles.affiliate}>{order.filial.name}</span> : ''}
                                            </>
                                        )}
                                    </td>
                                )}
                                <td>
                                    <p style={{color: getOrderStateColor(order.state)}}>{getOrderStateName(order.state)}</p>
                                </td>
                                {order.shippingDate.length > 0 ? (
                                    <td>
                                        <span style={{fontWeight: 500}}>{getDateDDMMYYYYFormat(new Date(order.shippingDate))}</span>
                                    </td>
                                ) : (
                                    <td style={{whiteSpace: 'nowrap'}}>
                                        {order.estShippingDate.length === 0 ? (
                                            <>&mdash;</>
                                        ) : (
                                            <> ~ {getDateDDMMYYYYFormat(new Date(order.estShippingDate))}</>
                                        )}
                                    </td>
                                )}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
