import * as React from 'react'
import {useState, useCallback} from 'react'
import {Link} from 'react-router-dom'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {AddButton} from '../../common/button/add-button/add-button'
import {ArrowTooltip} from '../../common/arrow-tooltip/arrow-tooltip'
import {getDefaultOrdersLink} from '../../../helpers/orders'
import {usePaymentsPageLink} from '../../../hooks/router'
import logo from '../../../resources/images/logo.svg'
import {AnalyticsManager} from '../../../analytics-manager'
import {usePermissions, useIsDemoUser, usePartnerAccess} from '../../../hooks/permissions'
import {useGoToNewOrder} from '../../../hooks/orders'
import {Balance} from '../../common/balance/balance'
import {BonusScore} from '../bonus-score/bonus-score'
import {Notifications} from '../../common/notifications/notifications'
import {RegistrationModal} from '../../common/registration-modal/registration-modal'
import {BalanceModeSelector} from '../../common/balance-mode-selector/balance-mode-selector'
import styles from './header.module.css'

export function Header(): JSX.Element {
    const ordersOnPage = useSelector((state: IAppState) => state.orders.ordersOnPage)
    const accountBalance = useSelector((state: IAppState) => state.account.balance)
    const framerPoints = useSelector((state: IAppState) => state.account.framerPoints)
    const isFramerPointsAvailable = useSelector((state: IAppState) => state.account.partner?.isFramerPointsAvailable)
    const isCashMode = useSelector((state: IAppState) => state.account.isCashMode)
    const showBalance = useSelector((state: IAppState) => state.account.showBalance)

    const [isRegistrationModalOpened, setIsRegistrationModalOpened] = useState(false)
    const [isDemoLimitModalOpened, setIsDemoLimitModalOpened] = useState(false)

    const goToNewOrder = useGoToNewOrder()
    const {isExternal, isPermPartner} = usePartnerAccess()
    const isDemoUser = useIsDemoUser()
    const permissions = usePermissions()
    const paymentsPageLink = usePaymentsPageLink()

    const openRegistrationModalHandler = useCallback(() => {
        AnalyticsManager.trackRegisterFromDemoClick()
        setIsRegistrationModalOpened(true)
    }, [])

    const closeRegistrationModalHandler = useCallback(() => {
        setIsRegistrationModalOpened(false)
        setIsDemoLimitModalOpened(false)
    }, [])

    const newOrderHandler = useCallback(() => {
        const isNewOrderAvailable = goToNewOrder()
        !isNewOrderAvailable && setIsDemoLimitModalOpened(true)
    }, [goToNewOrder])

    const openConfirmModalHandler = useCallback(() => {
        AnalyticsManager.trackConfirmAccountFromHeaderClick()
        setIsRegistrationModalOpened(true)
    }, [])

    return (
        <div className={styles.header} data-id="header">
            {permissions.isNonQualified && (
                <div className={styles.registration}>
                    <button className={`${styles.actionButton} 'analytics-header-confirm-account-button'`} onClick={openConfirmModalHandler}>
                        Подтвердить учетную запись
                    </button>
                </div>
            )}
            {isDemoUser && (
                <div className={styles.registration}>
                    <button className={styles.actionButton} onClick={openRegistrationModalHandler}>
                        Зарегистрироваться
                    </button>
                </div>
            )}
            <div className={`${styles.topMenu} ${isCashMode && styles.cashMode}`}>
                <Link to={getDefaultOrdersLink(ordersOnPage)} className={styles.logoWrap}>
                    <img src={logo} alt="framer" draggable={false} width="48" height="24" />
                </Link>
                <AddButton
                    title="Новый заказ"
                    className={styles.addButton}
                    onClick={newOrderHandler}
                    dataTest="new-order-button"
                    dataId="new-order-button"
                />
                {!isExternal && (
                    <>
                        <ArrowTooltip title="+ Пополнить баланс">
                            <Link to={paymentsPageLink} className={styles.accountBalanceLink}>
                                <Balance
                                    value={isCashMode ? framerPoints : accountBalance}
                                    isHideSymbol={isFramerPointsAvailable}
                                    className={styles.accountBalance}
                                />
                            </Link>
                        </ArrowTooltip>
                        {isFramerPointsAvailable && <BalanceModeSelector className={styles.balanceMode} />}
                    </>
                )}
                {showBalance && isPermPartner && !isCashMode && <BonusScore className={styles.bonus} />}
                <Notifications className={styles.notificationButton} />
                <a className={styles.phoneLink} href="tel:88003010920" target="_blank" rel="noopener noreferrer">
                    8 (800) 301-09-20
                </a>
            </div>
            {(isRegistrationModalOpened || isDemoLimitModalOpened) && (
                <RegistrationModal isWarningMode={isDemoLimitModalOpened} onClose={closeRegistrationModalHandler} />
            )}
        </div>
    )
}
