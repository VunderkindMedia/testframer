import * as React from 'react'
import {useEffect} from 'react'
import {IOrdersFilterParams} from '../../../../helpers/orders'
import {NewSidebar} from '../../new-sidebar/new-sidebar'
import {NewSidebarItem} from '../../new-sidebar/new-sidebar-item/new-sidebar-item'
import {WithTitle} from '../../../common/with-title/with-title'
import {DateRangeSelector} from '../../../common/date-range-selector/date-range-selector'
import {SearchInput} from '../../../common/search-input/search-input'
import {ListItem} from '../../../../model/list-item'
import {OrderStates} from '../../../../model/framer/order-states'
import {Select} from '../../../common/select/select'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import {useEmployees, useAffiliates} from '../../../../hooks/account'
import {arraysHaveSameItems} from '../../../../helpers/array'
import {STATES_ITEMS} from '../../../../constants/states-items'
import styles from './new-orders-page-sidebar.module.css'
import {FilterBalanceSelector} from '../../../common/filter-balance-selector/filter-balance-selector'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'

interface INewOrdersPageSidebarProps {
    filterParams: IOrdersFilterParams
    onChangeFilterPrams: (params: IOrdersFilterParams) => void
}

export function NewOrdersPageSidebar(props: INewOrdersPageSidebarProps): JSX.Element {
    const {filterParams, onChangeFilterPrams} = props
    const {dateFrom, dateTo, states, filials, managers} = filterParams
    const isFramerPointsAvailable = useSelector((state: IAppState) => state.account.partner?.isFramerPointsAvailable)
    const employees = useEmployees()
    const affiliates = useAffiliates()
    const selectedItem = STATES_ITEMS.find(i => arraysHaveSameItems(i.data, states, (s1, s2) => s1 === s2))
    const affiliatesList = affiliates?.length
        ? [new ListItem('Все', 'Все', -1), ...affiliates.map(({id, name}) => new ListItem(String(id), name, id))]
        : []
    const employeesList = employees?.length
        ? [new ListItem('Все', 'Все', -1), ...employees.map(({id, name}) => new ListItem(String(id), name, id))]
        : []

    useEffect(() => {
        if (!selectedItem) {
            onChangeFilterPrams({...filterParams, states: STATES_ITEMS[0].data})
        }
    }, [selectedItem, filterParams, onChangeFilterPrams])

    return (
        <NewSidebar headerTitle="Фильтр" className={styles.newOrdersPageSidebar}>
            <NewSidebarItem className={styles.newOrdersPageSidebarItemCreatedDate}>
                <WithTitle title="Дата создания">
                    <DateRangeSelector
                        dataTest="orders-date-range"
                        startDate={dateFrom}
                        endDate={dateTo}
                        className={styles.newOrdersPageSidebarRangeSlider}
                        onChange={(startDate, endDate) => {
                            const newParams: IOrdersFilterParams = {...filterParams, dateFrom: startDate, dateTo: endDate}
                            onChangeFilterPrams(newParams)
                        }}
                    />
                </WithTitle>
            </NewSidebarItem>
            <NewSidebarItem className={styles.newOrdersPageSidebarItemOrderName}>
                <WithTitle title="Номер">
                    <SearchInput
                        dataTest="orders-number-filter-input"
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        value={filterParams.orderNumber}
                        placeholder="Поиск"
                        onChange={value => onChangeFilterPrams({...filterParams, orderNumber: value})}
                        onClear={() => onChangeFilterPrams({...filterParams, orderNumber: ''})}
                    />
                </WithTitle>
            </NewSidebarItem>
            <NewSidebarItem className={styles.newOrdersPageSidebarItemStates}>
                <WithTitle title="Статус">
                    <Select<OrderStates[]>
                        dataTest="order-status-select"
                        items={STATES_ITEMS}
                        selectedItem={selectedItem}
                        onSelect={item => onChangeFilterPrams({...filterParams, states: item.data})}
                    />
                </WithTitle>
            </NewSidebarItem>
            {affiliatesList.length > 0 && (
                <NewSidebarItem className={styles.sidebarItem}>
                    <WithTitle title="Филиалы">
                        <Select<number | undefined>
                            isMultiSelect={true}
                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                            items={affiliatesList}
                            selectedItems={
                                filials?.length
                                    ? affiliatesList.filter(fl => fl.data !== undefined && filials.includes(fl.data))
                                    : [affiliatesList[0]]
                            }
                            onMultiSelect={items => {
                                const filialIds: number[] = []
                                items.map(item => {
                                    if (item.data && item.data !== -1) {
                                        filialIds.push(item.data)
                                    }
                                })
                                onChangeFilterPrams({...filterParams, filials: filialIds})
                            }}
                        />
                    </WithTitle>
                </NewSidebarItem>
            )}
            {employeesList.length > 0 && (
                <NewSidebarItem className={styles.sidebarItem}>
                    <WithTitle title="Сотрудники">
                        <Select<number | undefined>
                            isMultiSelect={true}
                            items={employeesList}
                            debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                            selectedItems={
                                managers?.length
                                    ? employeesList.filter(e => e.data !== undefined && managers.includes(e.data))
                                    : [employeesList[0]]
                            }
                            onMultiSelect={items => {
                                const managerIds: number[] = []
                                items.map(item => {
                                    if (item.data && item.data !== -1) {
                                        managerIds.push(item.data)
                                    }
                                })
                                onChangeFilterPrams({...filterParams, managers: managerIds})
                            }}
                        />
                    </WithTitle>
                </NewSidebarItem>
            )}
            {isFramerPointsAvailable && (
                <NewSidebarItem className={styles.sidebarItem}>
                    <WithTitle title="Единицы измерения суммы">
                        <FilterBalanceSelector onChangeFilterPrams={onChangeFilterPrams} filterParams={filterParams} />
                    </WithTitle>
                </NewSidebarItem>
            )}
        </NewSidebar>
    )
}
