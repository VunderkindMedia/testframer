import * as React from 'react'
import {NAV_BOOKING, NAV_ORDERS} from '../../../constants/navigation'
import {PaginationMenu} from '../../common/pagination-menu/pagination-menu'
import {OrdersPageTable} from '../orders-page-table/orders-page-table'
import {getOrderLink, getOrdersFilter, getOrdersFilterParams, IOrdersFilterParams} from '../../../helpers/orders'
import {NewOrdersPageSidebar} from './new-orders-page-sidebar/new-orders-page-sidebar'
import {Button} from '../../common/button/button'
import {ButtonWithHint} from '../../common/button/button-with-hint/button-with-hint'
import {
    useLoadOffer,
    useOrdersFromSearch,
    useSetClientToOrder,
    useSetCurrentOrder,
    useShowRealCost,
    useRecalculateOrder,
    useSetOrderFilterParams,
    useDuplicateOrder,
    useRemoveOrderModal,
    useCancelOrderModal,
    useLoadOrderXml,
    useIsNewOrderAvailable
} from '../../../hooks/orders'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useGoTo} from '../../../hooks/router'
import {Action} from '../../../model/action'
import {useClients} from '../../../hooks/clients'
import {PageTitle} from '../../common/page-title/page-title'
import {useRef, useState, useEffect} from 'react'
import {Modal} from '../../common/modal/modal'
import {usePermissions, useIsExternalUser, useIsExportToXmlAvailable, useIsDemoUser} from '../../../hooks/permissions'
import {HINTS} from '../../../constants/hints'
import {TabletSidebar} from '../../tablet/tablet-sidebar/tablet-sidebar'
import {FilterButton} from '../../common/button/filter-button/filter-button'
import {OrderStates} from '../../../model/framer/order-states'
import {AnalyticsManager} from '../../../analytics-manager'
import {InsufficientFundsModal, useShouldShowInsufficientFundsModal} from '../../common/insufficient-funds-modal/insufficient-funds-modal'
import {DropdownMenu} from '../../common/dropdown-menu/dropdown-menu'
import {useUpdateAppVersion, useAppContext} from '../../../hooks/app'
import styles from './orders-page.module.css'

export function OrdersPage(): JSX.Element {
    const orders = useOrdersFromSearch()
    const hasRequestsInProcess = useSelector((state: IAppState) => state.request.count === 0)
    const ordersTotalCount = useSelector((state: IAppState) => state.orders.totalCount)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const clients = useClients()
    const permission = usePermissions()
    const isExternalUser = useIsExternalUser()
    const isExportToXmlAvailable = useIsExportToXmlAvailable()
    const isDemoUser = useIsDemoUser()
    const search = useSelector((state: IAppState) => state.router.location.search)
    const isFramerPointsAvailable = useSelector((state: IAppState) => state.account.partner?.isFramerPointsAvailable || false)

    const tabletSidebarRef = useRef<TabletSidebar | null>(null)

    const [shouldShowRealCost, setShouldShowRealCost] = useShowRealCost()
    const setCurrentOrder = useSetCurrentOrder()
    const setClientToOrder = useSetClientToOrder()
    const loadOffer = useLoadOffer()

    const goTo = useGoTo()
    const recalculateOrder = useRecalculateOrder()
    const setOrderFilterParams = useSetOrderFilterParams()
    const shouldShowInsufficientFundsModal = useShouldShowInsufficientFundsModal()
    const duplicateOrder = useDuplicateOrder()
    const loadXml = useLoadOrderXml()
    useUpdateAppVersion()
    const {isTablet} = useAppContext()
    const isNewOrderAvailable = useIsNewOrderAvailable()

    const [isVisibleInsufficientFundsModal, setIsVisibleInsufficientFundsModal] = useState(false)
    const [filterParams, setFilterParams] = useState<IOrdersFilterParams | null>(null)
    const [removeModalModel, showRemoveModal] = useRemoveOrderModal(currentOrder)
    const [cancelModalModel, showCancelModal] = useCancelOrderModal(currentOrder)

    useEffect(() => {
        setFilterParams(getOrdersFilterParams(search))
    }, [search])

    const orderActions: Action[] = []
    if (currentOrder) {
        if (isNewOrderAvailable) {
            orderActions.push(
                new Action('Повторить заказ', () => {
                    duplicateOrder(currentOrder.id)
                })
            )
        }
        if (currentOrder.isCancelAvailable) {
            orderActions.push(
                new Action('Отменить заказ', () => {
                    showCancelModal()
                })
            )
        }
    }

    return (
        <div className={styles.ordersPage}>
            {isVisibleInsufficientFundsModal && <InsufficientFundsModal onClose={() => setIsVisibleInsufficientFundsModal(false)} />}
            {removeModalModel && <Modal model={removeModalModel} />}
            {cancelModalModel && <Modal model={cancelModalModel} />}
            <div className={styles.content}>
                <div className={styles.header}>
                    <PageTitle title="Заказы" className={styles.title} />
                    {currentOrder && (
                        <>
                            <Button buttonStyle="with-border" onClick={() => goTo(getOrderLink(currentOrder))}>
                                Редактировать
                            </Button>
                            {[OrderStates.CalcErrors, OrderStates.NeedsRecalc, OrderStates.Draft].includes(currentOrder.state) && (
                                <Button
                                    buttonStyle="with-border"
                                    isDisabled={currentOrder.readonly}
                                    onClick={() => recalculateOrder(currentOrder)}>
                                    Пересчитать
                                </Button>
                            )}
                            <Button
                                dataTest="remove-order-button"
                                buttonStyle="with-border"
                                isDisabled={currentOrder.readonly}
                                onClick={showRemoveModal}>
                                Удалить
                            </Button>
                            <ButtonWithHint hint={permission.isNonQualified ? HINTS.nonQualifiedRestriction : ''}>
                                <Button
                                    dataTest="orders-page-download-kp-button"
                                    isDisabled={currentOrder.totalAmegaCostToDisplay === 0 || permission.isNonQualified}
                                    buttonStyle="with-border"
                                    onClick={() => {
                                        loadOffer(currentOrder)
                                        AnalyticsManager.trackClickDownloadKpButtonFromOrdersPage()
                                        currentOrder.addAction(`Скачали КП в заказе ${currentOrder.id}`)
                                    }}>
                                    Скачать КП
                                </Button>
                            </ButtonWithHint>
                            {currentOrder.producible && !isExternalUser && (
                                <ButtonWithHint
                                    hint={
                                        permission.isNonQualified
                                            ? HINTS.nonQualifiedRestriction
                                            : isDemoUser
                                            ? HINTS.demoAccessProducingRestriction
                                            : ''
                                    }>
                                    <Button
                                        buttonStyle="with-border"
                                        isDisabled={permission.isNonQualified || isDemoUser || permission.isCommonManager}
                                        onClick={() => {
                                            if (shouldShowInsufficientFundsModal) {
                                                setIsVisibleInsufficientFundsModal(true)
                                            } else {
                                                goTo(`${NAV_BOOKING}?orderId=${currentOrder.id}`)
                                            }
                                            AnalyticsManager.trackClickTransferToProductionButton()
                                        }}>
                                        Передать в производство
                                    </Button>
                                </ButtonWithHint>
                            )}
                            {isExportToXmlAvailable && (
                                <Button buttonStyle="with-border" onClick={() => loadXml(currentOrder)}>
                                    Сохранить XML
                                </Button>
                            )}
                            {orderActions.length > 0 && <DropdownMenu items={orderActions} />}
                            {isTablet && <FilterButton onClick={() => tabletSidebarRef.current?.show()} />}
                        </>
                    )}
                </div>
                <div className={styles.tableContainer}>
                    {orders.length > 0 ? (
                        <OrdersPageTable
                            orders={orders}
                            currentOrder={currentOrder}
                            setCurrentOrder={setCurrentOrder}
                            shouldShowRealCost={shouldShowRealCost}
                            setShouldShowRealCost={setShouldShowRealCost}
                            clients={clients}
                            setClientToOrder={setClientToOrder}
                            isFramerPointsAvailable={isFramerPointsAvailable}
                        />
                    ) : (
                        <>{hasRequestsInProcess && <p>{HINTS.ordersNotFound}</p>}</>
                    )}
                </div>
                {filterParams && (
                    <PaginationMenu
                        currentPage={Math.ceil(filterParams.offset / filterParams.limit)}
                        itemsOnPage={filterParams.limit}
                        totalCount={ordersTotalCount}
                        onSelect={page => {
                            const offset = filterParams.limit * page
                            const filter = getOrdersFilter({...filterParams, id: undefined, offset})
                            goTo(`${NAV_ORDERS}${filter}`)
                        }}
                    />
                )}
            </div>
            {filterParams &&
                (isTablet && orders.length > 0 ? (
                    <TabletSidebar ref={tabletSidebarRef}>
                        <NewOrdersPageSidebar filterParams={filterParams} onChangeFilterPrams={setOrderFilterParams} />
                    </TabletSidebar>
                ) : (
                    <NewOrdersPageSidebar filterParams={filterParams} onChangeFilterPrams={setOrderFilterParams} />
                ))}
        </div>
    )
}
