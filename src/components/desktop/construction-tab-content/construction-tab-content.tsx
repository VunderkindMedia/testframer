import * as React from 'react'
import {useState, useCallback, useEffect} from 'react'
import {SvgConstructionDrawerContainer} from '../../common/svg-construction-drawer-container/svg-construction-drawer-container'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {OrderErrorsExpandable} from '../order-errors-expandable/order-errors-expandable'
import {
    useSetConfigContext,
    useSetCurrentContext,
    useSetCurrentDimensionLine,
    useSetHighlightedContext,
    useSetHighlightedDimensionLine
} from '../../../hooks/builder'
import {useSetCurrentProduct} from '../../../hooks/orders'
import {useSetConnectingSide, useSetHostProduct, useSetTemplateFilter, useShowTemplateSelector} from '../../../hooks/template-selector'
import {UndoRedo} from '../undo-redo/undo-redo'
import {getClassNames} from '../../../helpers/css'
import {NewBuilderPageSidebar} from '../new-builder-page-sidebar/new-builder-page-sidebar'
import {useChangeDimensionLineValueHandler} from '../../../helpers/builder'
import {BuilderModes} from '../../../model/builder-modes'
import {setBuilderMode} from '../../../redux/builder/actions'
import {EnergyInfoModal} from '../../common/energy-info-modal/energy-info-modal'
import {useEffectSkipFirst} from '../../../hooks/skip-first'
import styles from './construction-tab-content.module.css'

interface IConstructionTabContentProps {
    className?: string
}

export function ConstructionTabContent(props: IConstructionTabContentProps): JSX.Element {
    console.log('render')
    const {className} = props
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const builderMode = useSelector((state: IAppState) => state.builder.mode)
    const highlightedContext = useSelector((state: IAppState) => state.builder.highlightedContext)
    const currentDimensionLine = useSelector((state: IAppState) => state.builder.currentDimensionLine)
    const highlightedDimensionLine = useSelector((state: IAppState) => state.builder.highlightedDimensionLine)
    const windowSize = useSelector((state: IAppState) => state.browser.windowSize)
    const lamination = useSelector((state: IAppState) => state.lamination.all)
    const isRestrictedMode = useSelector((state: IAppState) => state.settings.isRestrictedMode)

    const [showEnergyInfo, setShowEnergyInfo] = useState(false)

    const dispatch = useDispatch()
    const changeDimensionLineValueHandler = useChangeDimensionLineValueHandler()
    const setCurrentContext = useSetCurrentContext()
    const setHighlightedContext = useSetHighlightedContext()
    const setCurrentDimensionLine = useSetCurrentDimensionLine()
    const setHighlightedDimensionLine = useSetHighlightedDimensionLine()
    const setCurrentProduct = useSetCurrentProduct()
    const setConfigContext = useSetConfigContext()
    const setHostProduct = useSetHostProduct()
    const setConnectingSide = useSetConnectingSide()
    const setTemplateFilter = useSetTemplateFilter()
    const showTemplateSelector = useShowTemplateSelector()

    const onInfoModalClose = useCallback(() => {
        setShowEnergyInfo(false)
    }, [])

    useEffectSkipFirst(() => {
        if (!currentConstruction?.haveEnergyProducts) {
            setShowEnergyInfo(true)
        }
    }, [currentConstruction?.haveEnergyProducts])

    return (
        <div
            className={getClassNames(`${styles.content} ${className ?? ''}`, {[styles.connectorMode]: builderMode === BuilderModes.Connector})}>
            <div className={styles.builder}>
                <div className={styles.constructionDrawer}>
                    {currentOrder && currentConstruction && currentProduct && (
                        <SvgConstructionDrawerContainer
                            currentConstruction={currentConstruction}
                            currentContext={currentContext}
                            highlightedContext={highlightedContext}
                            currentDimensionLine={currentDimensionLine}
                            highlightedDimensionLine={highlightedDimensionLine}
                            onSelectContext={setCurrentContext}
                            onHoverContext={setHighlightedContext}
                            onSelectDimensionLine={setCurrentDimensionLine}
                            onHoverDimensionLine={setHighlightedDimensionLine}
                            setCurrentProductAction={setCurrentProduct}
                            setConfigContext={setConfigContext}
                            setHostProductAction={setHostProduct}
                            setConnectingSideAction={setConnectingSide}
                            setTemplateFilterAction={setTemplateFilter}
                            isReadonly={currentOrder.readonly}
                            isRestrictedMode={isRestrictedMode}
                            windowSize={windowSize}
                            lamination={lamination}
                            showTemplateSelector={showTemplateSelector}
                            onChangeDimensionLineValue={changeDimensionLineValueHandler}
                            onClickOutConstruction={() => {
                                setCurrentContext(null)
                                dispatch(setBuilderMode(null))
                            }}
                        />
                    )}
                </div>
                <UndoRedo />
                {currentConstruction?.haveEnergyProducts && (
                    <div
                        className={`${styles.energyIcon} ${
                            currentOrder && currentOrder.constructions.some(construction => construction.errors.length > 0) ? styles.right : ''
                        }`}>
                        {currentConstruction?.energyProductType}
                    </div>
                )}
            </div>
            <OrderErrorsExpandable />
            <div className={styles.sidebar}>
                <NewBuilderPageSidebar />
            </div>
            {showEnergyInfo && <EnergyInfoModal onAction={onInfoModalClose} onClose={onInfoModalClose} />}
        </div>
    )
}
