import * as React from 'react'
import styles from './lamination-sidebar-item.module.css'
import {getHexMoskitkaColor, getMoskitkaColorName, MoskitkaColors} from '../../../../model/framer/moskitka-colors'
import {Lamination} from '../../../../model/framer/lamination'
import {Construction} from '../../../../model/framer/construction'
import {getClassNames} from '../../../../helpers/css'
import {isWhiteColor} from '../../../../helpers/color'
import {ArrowTooltip} from '../../../common/arrow-tooltip/arrow-tooltip'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from '../new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {useSetConfigContext} from '../../../../hooks/builder'
import {ConfigContexts} from '../../../../model/config-contexts'
import {Button} from '../../../common/button/button'

interface ILaminationSidebarItemProps {
    construction: Construction
    outsideLamination: Lamination
    insideLamination: Lamination
    className?: string
}

export function LaminationSidebarItem(props: ILaminationSidebarItemProps): JSX.Element {
    const {construction, outsideLamination, insideLamination, className} = props

    const setConfigContext = useSetConfigContext()

    const isBrownMoskitka = (construction.isMoskitka && construction.moskitka?.color === MoskitkaColors.Brown) || false

    const outsideLaminationName = isBrownMoskitka ? getMoskitkaColorName(MoskitkaColors.Brown) : outsideLamination.name
    const insideLaminationName = isBrownMoskitka ? getMoskitkaColorName(MoskitkaColors.Brown) : insideLamination.name

    const outsideLaminationHex = isBrownMoskitka ? getHexMoskitkaColor(MoskitkaColors.Brown, outsideLamination.hex) : outsideLamination.hex
    const insideLaminationHex = isBrownMoskitka ? getHexMoskitkaColor(MoskitkaColors.Brown, insideLamination.hex) : insideLamination.hex

    return (
        <NewBuilderPageSidebarItem className={className} border="short">
            <NewBuilderPageSidebarItemText dataId="sidebar-lamination">
                Ламинация:&nbsp;
                <ArrowTooltip title={`Ламинация снаружи: ${outsideLaminationName}`} placement="top">
                    <Button onClick={() => setConfigContext(ConfigContexts.LaminationOutside)}>
                        <div className={styles.laminationButton}>
                            Снаружи
                            <div
                                className={getClassNames(styles.laminationIcon, {[styles.whiteIcon]: isWhiteColor(outsideLaminationHex)})}
                                style={{backgroundColor: outsideLaminationHex, marginRight: '8px'}}
                            />
                        </div>
                    </Button>
                </ArrowTooltip>
                <ArrowTooltip title={`Ламинация внутри: ${insideLaminationName}`} placement="top">
                    <Button onClick={() => setConfigContext(ConfigContexts.LaminationInside)}>
                        <div className={styles.laminationButton}>
                            Внутри
                            <div
                                className={getClassNames(styles.laminationIcon, {[styles.whiteIcon]: isWhiteColor(insideLaminationHex)})}
                                style={{backgroundColor: insideLaminationHex}}
                            />
                        </div>
                    </Button>
                </ArrowTooltip>
            </NewBuilderPageSidebarItemText>
        </NewBuilderPageSidebarItem>
    )
}
