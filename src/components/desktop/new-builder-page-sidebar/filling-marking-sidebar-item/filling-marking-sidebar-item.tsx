import * as React from 'react'
import {useCallback, useMemo} from 'react'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from '../new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {SolidFilling} from '../../../../model/framer/solid-filling'
import {useSetConfigContext} from '../../../../hooks/builder'
import {ConfigContexts} from '../../../../model/config-contexts'
import {Button} from '../../../common/button/button'

export function FillingMarkingSidebarItem(): JSX.Element | null {
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const context = useSelector((state: IAppState) =>
        state.orders.currentProduct?.isEmpty ? state.orders.currentProduct.frame.innerFillings[0].solid : state.builder.currentContext
    )

    const setConfigContext = useSetConfigContext()
    const fillings = useMemo(() => currentProduct?.availableFillings ?? [], [currentProduct])

    const setFillingContext = useCallback(() => {
        const selected = fillings.find(f => context instanceof SolidFilling && f.marking === context.fillingVendorCode) || null
        setConfigContext(selected?.group ? ConfigContexts.Filling : ConfigContexts.FillingEditor)
    }, [fillings, context, setConfigContext])

    if (!(context instanceof SolidFilling) || currentProduct?.isMoskitka) {
        return null
    }

    return (
        <NewBuilderPageSidebarItem>
            <NewBuilderPageSidebarItemText>
                Заполнение:&nbsp;<Button onClick={setFillingContext}>{context.fillingVendorCode}</Button>
            </NewBuilderPageSidebarItemText>
        </NewBuilderPageSidebarItem>
    )
}
