import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {SolidFilling} from '../../../../model/framer/solid-filling'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from '../new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {useSetConfigContext} from '../../../../hooks/builder'
import {ConfigContexts} from '../../../../model/config-contexts'
import {Button} from '../../../common/button/button'

export function FillingGlassColorSidebarItem(): JSX.Element | null {
    const context = useSelector((state: IAppState) =>
        state.orders.currentProduct?.isEmpty ? state.orders.currentProduct.frame.innerFillings[0].solid : state.builder.currentContext
    )

    const setConfigContext = useSetConfigContext()

    if (!(context instanceof SolidFilling) || !context.filling?.isMultifunctional) {
        return null
    }

    return (
        <NewBuilderPageSidebarItem>
            <NewBuilderPageSidebarItemText>
                Цвет стекла:&nbsp;<Button onClick={() => setConfigContext(ConfigContexts.GlassColorSelector)}>{context?.glassColor}</Button>
            </NewBuilderPageSidebarItemText>
        </NewBuilderPageSidebarItem>
    )
}
