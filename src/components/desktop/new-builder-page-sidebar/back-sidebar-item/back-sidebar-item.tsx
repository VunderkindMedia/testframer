import * as React from 'react'
import styles from './back-sidebar-item.module.css'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from '../new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {INewSidebarItemProps} from '../../new-sidebar/new-sidebar-item/new-sidebar-item'

export function BackSidebarItem(props: INewSidebarItemProps): JSX.Element {
    const {className, children, border = 'none', onClick, style} = props

    return (
        <NewBuilderPageSidebarItem className={`${styles.sidebarItem} ${className}`} border={border} onClick={onClick} style={style}>
            <div className={styles.backIcon} />
            <NewBuilderPageSidebarItemText>{children}</NewBuilderPageSidebarItemText>
        </NewBuilderPageSidebarItem>
    )
}
