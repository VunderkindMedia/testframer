import * as React from 'react'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from '../new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {RemoveButton} from '../../../common/button/remove-button/remove-button'
import styles from './remove-sidebar-item.module.css'

interface IRemoveSidebarItemProps {
    onClick?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void
}

export function RemoveSidebarItem(props: IRemoveSidebarItemProps): JSX.Element {
    const {onClick} = props

    return (
        <NewBuilderPageSidebarItem>
            <NewBuilderPageSidebarItemText>
                Удалить
                <RemoveButton className={styles.removeIcon} onClick={onClick} />
            </NewBuilderPageSidebarItemText>
        </NewBuilderPageSidebarItem>
    )
}
