import * as React from 'react'
import {INewSidebarItemProps, NewSidebarItem} from '../../new-sidebar/new-sidebar-item/new-sidebar-item'
import {getClassNames} from '../../../../helpers/css'
import {CSSProperties, ReactNode} from 'react'
import styles from './new-builder-page-sidebar-item.module.css'

export function NewBuilderPageSidebarItem(props: INewSidebarItemProps): JSX.Element {
    const {children, border = 'short', className, style, onClick} = props

    return (
        <NewSidebarItem
            border={border}
            className={getClassNames(`${styles.sidebarItem} ${className}`, {[styles.clickable]: Boolean(onClick)})}
            style={style}
            onClick={onClick}>
            {children}
        </NewSidebarItem>
    )
}

export interface INewBuilderPageSidebarItemTextProps {
    style?: CSSProperties
    children?: ReactNode
    dataId?: string
}

export function NewBuilderPageSidebarItemText(props: INewBuilderPageSidebarItemTextProps): JSX.Element {
    const {style, children, dataId} = props

    return (
        <div className={styles.text} style={style} data-id={dataId}>
            {children}
        </div>
    )
}
