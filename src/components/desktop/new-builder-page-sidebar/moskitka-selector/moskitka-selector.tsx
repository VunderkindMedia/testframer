import * as React from 'react'
import {useCallback} from 'react'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from '../new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {FrameFilling} from '../../../../model/framer/frame-filling'
import {SelectorWithIcon} from '../../../common/selector-with-icon/selector-with-icon'
import icon1 from './resources/icon_1.svg'
import icon2 from './resources/icon_2.svg'
import {Moskitka} from '../../../../model/framer/moskitka'
import {useSetConfigContext, useUpdateMoskitka} from '../../../../hooks/builder'
import {ConfigContexts} from '../../../../model/config-contexts'

export function MoskitkaSelector(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const setConfigContext = useSetConfigContext()
    const updateMoskitka = useUpdateMoskitka(currentOrder, currentContext)

    const add = useCallback(() => {
        if (!currentConstruction) {
            return
        }
        const frameFilling = currentConstruction.allFrameFillings.find(f => f.nodeId == currentContext?.nodeId)
        if (!frameFilling?.moskitka?.set) {
            const updatedMoskitka = frameFilling?.moskitka?.clone ?? new Moskitka()
            updatedMoskitka.set = true
            updateMoskitka(updatedMoskitka)
        }
        setConfigContext(ConfigContexts.MoskitkaConfigurator)
    }, [currentConstruction, currentContext, updateMoskitka, setConfigContext])

    const remove = useCallback(() => {
        if (!(currentContext instanceof FrameFilling) || !currentContext.moskitka) {
            return
        }
        const moskitka = currentContext.moskitka.clone
        moskitka.set = false
        updateMoskitka(moskitka)
    }, [currentContext, updateMoskitka])

    if (!(currentContext instanceof FrameFilling) || currentContext.isAluminium) {
        return null
    }

    return (
        <NewBuilderPageSidebarItem>
            <NewBuilderPageSidebarItemText style={{marginBottom: '8px'}}>
                Москитная сетка:&nbsp;<span>{currentContext.moskitka && currentContext.moskitka.set ? 'Есть' : 'Нет'}</span>
            </NewBuilderPageSidebarItemText>
            <SelectorWithIcon
                icons={[
                    {
                        src: icon1,
                        selected: Boolean(currentContext.moskitka?.set),
                        hint: 'Есть',
                        onClick: add
                    },
                    {
                        src: icon2,
                        selected: !currentContext.moskitka?.set,
                        hint: 'Нет',
                        onClick: remove
                    }
                ]}
            />
        </NewBuilderPageSidebarItem>
    )
}
