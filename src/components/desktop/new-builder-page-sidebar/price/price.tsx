import * as React from 'react'
import styles from './price.module.css'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from '../new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {ArrowTooltip} from '../../../common/arrow-tooltip/arrow-tooltip'
import {getCurrency} from '../../../../helpers/currency'
import {RUB_SYMBOL} from '../../../../constants/strings'
import {ReactComponent as CacheIcon} from '../../../../resources/images/cache-icon.inline.svg'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {Card} from '../../../common/card/card'
import {getConstructionPosition} from '../../../../helpers/orders'

export function Price(): JSX.Element | null {
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)

    if (!currentConstruction) {
        return null
    }

    const getPriceTitle = (): string => {
        if (!currentConstruction) {
            return ''
        }

        const position = getConstructionPosition(currentConstruction, currentOrder?.constructions ?? [])

        if (currentConstruction.isMoskitka) {
            return `Цена москитной сетки ${position}`
        }
        if (currentConstruction.isEmpty) {
            return `Цена заполнения ${position}`
        }

        return `Цена конструкции ${position}`
    }

    return (
        <Card className={styles.price}>
            <NewBuilderPageSidebarItem border="none">
                <NewBuilderPageSidebarItemText>
                    {getPriceTitle()}:&nbsp;
                    <ArrowTooltip title="Цена текущей конструкции, как она будет отражена в КП, с учетом количества." placement="top">
                        <span style={{outline: 'none'}}>
                            {getCurrency(currentConstruction.totalCost)} {currentOrder?.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                        </span>
                    </ArrowTooltip>
                </NewBuilderPageSidebarItemText>
            </NewBuilderPageSidebarItem>
        </Card>
    )
}
