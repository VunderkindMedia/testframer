import * as React from 'react'
import {useRef} from 'react'
import styles from './new-builder-page-sidebar.module.css'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {Price} from './price/price'
import {SolidFilling} from '../../../model/framer/solid-filling'
import {FrameFilling} from '../../../model/framer/frame-filling'
import {Handle} from '../../../model/handle'
import {Impost} from '../../../model/framer/impost'
import {useSetConfigContext, useSetCurrentContext} from '../../../hooks/builder'
import {IContext} from '../../../model/i-context'
import {Product} from '../../../model/framer/product'
import {Construction} from '../../../model/framer/construction'
import {Card} from '../../common/card/card'
import {CardHeader} from '../../common/card/card-header/card-header'
import {SolidFilingInfoFragment} from '../../common/builder-page-fragments/solid-filing-info-fragment/solid-filing-info-fragment'
import {FrameInfoFragment} from '../../common/builder-page-fragments/frame-info-fragment/frame-info-fragment'
import {FrameFillingInfoFragment} from '../../common/builder-page-fragments/frame-filling-info-fragment/frame-filling-info-fragment'
import {ImpostInfoFragment} from '../../common/builder-page-fragments/impost-info-fragment/impost-info-fragment'
import {HandleInfoFragment} from '../../common/builder-page-fragments/handle-info-fragment/handle-info-fragment'
import {ConstructionInfoFragment} from '../../common/builder-page-fragments/construction-info-fragment/construction-info-fragment'
import {getConfiguratorViewWithParamsByConfigContext} from '../../../helpers/builder'
import {BackSidebarItem} from './back-sidebar-item/back-sidebar-item'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from './new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {Connector} from '../../../model/framer/connector'
import {ConnectorInfoFragment} from '../../common/builder-page-fragments/connector-info-fragment/connector-info-fragment'
import {Beam} from '../../../model/framer/beam'
import {BeamInfoFragment} from '../../common/builder-page-fragments/beam-info-fragment/beam-info-fragment'
import {setBuilderMode} from '../../../redux/builder/actions'
import {ConnectorTypes} from '../../../model/framer/connector-types'
import {useTypeConstructionVisible} from '../../../model/framer/construction-types'

export enum MainWidgets {
    SolidFilingInfo = 'solidFilingInfo',
    FrameInfo = 'frameInfo',
    FrameFillingInfo = 'frameFillingInfo',
    ImpostInfo = 'impostInfo',
    BeamInfo = 'beamInfo',
    HandleInfo = 'handleInfo',
    ConstructionInfo = 'constructionInfo',
    ConnectorInfo = 'connectorInfo'
}

export const useMainWidget = (
    currentConstruction: Construction | null,
    currentProduct: Product | null,
    currentContext: IContext | null
): MainWidgets => {
    if (!currentConstruction || !currentProduct) {
        return MainWidgets.ConstructionInfo
    }
    if (currentContext instanceof SolidFilling && !currentProduct.isEmpty) {
        return MainWidgets.SolidFilingInfo
    } else if (currentContext instanceof FrameFilling) {
        if (currentContext.isRoot || currentContext.isMoskitka) {
            if (currentConstruction.products.length > 1 && currentProduct !== currentConstruction.products[0]) {
                return MainWidgets.FrameInfo
            }
        } else {
            // actionBarWidgets.push(WidgetTypes.MoskitkaConfigurator)
        }
        const hasFewParts = currentContext.frameBeams.every(b => currentProduct.profile.getAllGeometryParameters(b.modelPart).length > 1)
        if (hasFewParts || !currentContext.isRoot) {
            return MainWidgets.FrameFillingInfo
        }
    } else if (currentContext instanceof Impost) {
        return MainWidgets.ImpostInfo
    } else if (currentContext instanceof Handle) {
        return MainWidgets.HandleInfo
    } else if (currentContext instanceof Connector) {
        return MainWidgets.ConnectorInfo
    } else if (currentContext instanceof Beam) {
        return MainWidgets.BeamInfo
    }

    return MainWidgets.ConstructionInfo
}

export function NewBuilderPageSidebar(): JSX.Element | null {
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)
    const isRestrictedMode = useSelector((state: IAppState) => state.settings.isRestrictedMode)

    const mainWidget = useMainWidget(currentConstruction, currentProduct, currentContext)
    console.log(mainWidget)
    const setCurrentContext = useSetCurrentContext()
    const setConfigContext = useSetConfigContext()
    const dispatch = useDispatch()
    const typeConstructionFunction = useTypeConstructionVisible()

    const containerRef = useRef<HTMLDivElement>(null)

    if (!currentConstruction || !currentProduct) {
        return null
    }

    let closeTitle: string | undefined
    let backAction: (() => void) | undefined
    let mainView: JSX.Element | null = null
    let subView: JSX.Element | null = null

    const config = getConfiguratorViewWithParamsByConfigContext(currentProduct, currentContext, configContext, false)

    if (config) {
        closeTitle = config.closeTitle
        backAction = () => {
            config?.shouldResetContext && setCurrentContext(null)
            config?.shouldResetBuilderMode && dispatch(setBuilderMode(null))
            setConfigContext(null)
        }
        mainView = (
            <Card style={config.style}>
                {config.title && (
                    <NewBuilderPageSidebarItem border="none">
                        <NewBuilderPageSidebarItemText>
                            <span>{config.title}</span>
                        </NewBuilderPageSidebarItemText>
                    </NewBuilderPageSidebarItem>
                )}
                <NewBuilderPageSidebarItem style={{paddingTop: 0}}>{config.view}</NewBuilderPageSidebarItem>
            </Card>
        )
        if (config.subView && config.subView.length) {
            const currentSubView = config.subView.find(s => s.context === configContext)
            if (currentSubView) {
                subView = (
                    <Card style={currentSubView.style} className={styles.subview}>
                        {currentSubView.rootContext && (
                            <CardHeader
                                title={currentSubView.title ?? ''}
                                hasNavigation={true}
                                onClick={() => {
                                    currentSubView.rootContext && setConfigContext(currentSubView.rootContext)
                                }}
                            />
                        )}
                        {currentSubView.view}
                    </Card>
                )
            }
        }
    } else {
        let view: JSX.Element | null = null
        let title: string | undefined
        if (mainWidget === MainWidgets.SolidFilingInfo) {
            title = 'Параметры заполнения'
            view = <SolidFilingInfoFragment />
        } else if (mainWidget === MainWidgets.FrameInfo) {
            title = 'Параметры рамы'
            view = (
                <>
                    <NewBuilderPageSidebarItem key="type-constructions">
                        <NewBuilderPageSidebarItemText>Тип конструкции: {typeConstructionFunction}</NewBuilderPageSidebarItemText>
                    </NewBuilderPageSidebarItem>
                    <FrameInfoFragment />
                </>
            )
        } else if (mainWidget === MainWidgets.FrameFillingInfo) {
            title = 'Параметры створки'
            view = <FrameFillingInfoFragment />
        } else if (mainWidget === MainWidgets.ImpostInfo) {
            title = 'Параметры Импоста'
            view = <ImpostInfoFragment />
        } else if (mainWidget === MainWidgets.HandleInfo) {
            title = 'Параметры ручки'
            view = <HandleInfoFragment />
        } else if (mainWidget === MainWidgets.ConnectorInfo) {
            title = `Параметры ${
                currentContext instanceof Connector && currentContext.connectorType === ConnectorTypes.Extender ? 'расширителя' : 'соединителя'
            }`
            view = <ConnectorInfoFragment />
        } else if (mainWidget === MainWidgets.BeamInfo) {
            title = 'Параметры балки'
            view = <BeamInfoFragment />
        }
        if (view) {
            closeTitle = 'Назад к общим параметрам'
            backAction = () => setCurrentContext(null)
            mainView = (
                <Card>
                    {title && (
                        <NewBuilderPageSidebarItem border="none" style={{paddingBottom: 0}}>
                            <NewBuilderPageSidebarItemText>
                                <span>{title}</span>
                            </NewBuilderPageSidebarItemText>
                        </NewBuilderPageSidebarItem>
                    )}
                    {view}
                </Card>
            )
        }
    }

    if (!mainView) {
        mainView = (
            <Card className={styles.constructionInfo} dataId="builder-sidebar">
                <NewBuilderPageSidebarItem border="none" style={{paddingBottom: 0}}>
                    <NewBuilderPageSidebarItemText>
                        <span>Общие параметры</span>
                    </NewBuilderPageSidebarItemText>
                </NewBuilderPageSidebarItem>
                <ConstructionInfoFragment />
            </Card>
        )
    }

    return (
        <div
            ref={containerRef}
            className={`${styles.sidebar} ${closeTitle && styles.hasBackButton}`}
            onClick={e => {
                if (containerRef.current !== e.target && containerRef.current?.contains(e.target as HTMLElement)) {
                    e.stopPropagation()
                }
            }}>
            {closeTitle && backAction && (
                <Card>
                    <BackSidebarItem border="none" onClick={backAction}>
                        {closeTitle}
                    </BackSidebarItem>
                </Card>
            )}
            <div className={`${styles.mainView} ${isRestrictedMode && styles.expanded} ${closeTitle && styles.withNav}`}>
                {mainView}
                {subView ?? ''}
            </div>
            {!isRestrictedMode && <Price />}
        </div>
    )
}
