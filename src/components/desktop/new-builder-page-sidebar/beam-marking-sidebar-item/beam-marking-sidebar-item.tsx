import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {Beam} from '../../../../model/framer/beam'
import {FrameFilling} from '../../../../model/framer/frame-filling'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from '../new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {useSetConfigContext} from '../../../../hooks/builder'
import {ConfigContexts} from '../../../../model/config-contexts'
import {Button} from '../../../common/button/button'

export function BeamMarkingSidebarItem(): JSX.Element | null {
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const setConfigContext = useSetConfigContext()

    if (!currentProduct) {
        return null
    }

    if (!(currentContext instanceof Beam || currentContext instanceof FrameFilling) || currentProduct.isAluminium) {
        return null
    }

    const beam = currentContext instanceof Beam ? currentContext : currentContext.frameBeams[0]
    const parts = currentProduct.getAvailablePartsByBeam(beam)

    if (parts.length < 2) {
        return null
    }

    return (
        <NewBuilderPageSidebarItem>
            <NewBuilderPageSidebarItemText>
                Артикул:&nbsp;<Button onClick={() => setConfigContext(ConfigContexts.MarkingSelector)}>{beam.profileVendorCode}</Button>
            </NewBuilderPageSidebarItemText>
        </NewBuilderPageSidebarItem>
    )
}
