import * as React from 'react'
import {NewBuilderPageSidebarItem, NewBuilderPageSidebarItemText} from '../new-builder-page-sidebar-item/new-builder-page-sidebar-item'
import {SelectorWithIcon} from '../../../common/selector-with-icon/selector-with-icon'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../../redux/root-reducer'
import {useCurrentOpeningSystem, useOpeningSystems, useSetOpeningSystem} from '../../../../hooks/builder'
import {FrameFilling} from '../../../../model/framer/frame-filling'
import {SolidFilling} from '../../../../model/framer/solid-filling'
import {OpeningSystem} from '../../../../model/opening-system'
import {useCallback} from 'react'

export function OpeningSystemSelector(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const openingSystems = useOpeningSystems(currentProduct, currentContext)
    const currentOpeningSystem = useCurrentOpeningSystem(openingSystems, currentContext)

    const setOpeningSystem = useSetOpeningSystem(currentOrder, currentContext)

    const onSelect = useCallback(
        (openingSystem: OpeningSystem) => {
            if (!currentOpeningSystem || currentOpeningSystem === openingSystem) {
                return
            }

            setOpeningSystem(openingSystem, currentOpeningSystem)
        },
        [currentOpeningSystem, setOpeningSystem]
    )

    let shouldHideOpeningSystemSelector = currentProduct?.isMoskitka || currentProduct?.isEmpty
    if (!shouldHideOpeningSystemSelector && currentContext instanceof SolidFilling) {
        const parentNode = currentProduct?.frame.findNodeById(currentContext.parentNodeId) as FrameFilling
        if (parentNode && !parentNode.isRoot) {
            shouldHideOpeningSystemSelector = true
        }
    }

    if (!currentOrder || !currentProduct || shouldHideOpeningSystemSelector) {
        return null
    }

    if (currentContext instanceof FrameFilling && currentContext.isRoot) {
        return null
    }

    if (!(currentContext instanceof FrameFilling || currentContext instanceof SolidFilling)) {
        return null
    }

    if (!currentOpeningSystem) {
        return null
    }

    return (
        <NewBuilderPageSidebarItem>
            <NewBuilderPageSidebarItemText style={{display: 'inline-block', marginBottom: '8px'}}>
                Открывание:&nbsp;<span>{currentOpeningSystem?.name}</span>
            </NewBuilderPageSidebarItemText>
            <SelectorWithIcon
                icons={openingSystems.map(openingSystem => {
                    return {
                        src: openingSystem.img,
                        hint: openingSystem.name,
                        selected: currentOpeningSystem?.code === openingSystem.code,
                        onClick: () => onSelect(openingSystem)
                    }
                })}
            />
        </NewBuilderPageSidebarItem>
    )
}
