import * as React from 'react'
import styles from './tabs-panel.module.css'
import {CloseButton} from '../../common/button/close-button/close-button'
import {clamp} from '../../../helpers/number'
import {getClassNames} from '../../../helpers/css'
import {ReactNode} from 'react'

export interface ITab {
    id: string
    title: string
    active?: boolean
    icon?: JSX.Element
    activeIcon?: JSX.Element
    closable?: boolean
    dataTest?: string
}

interface ITabsPanelProps {
    tabs: ITab[]
    isCentered?: boolean
    children?: ReactNode
    onUpdateTabs: (tabs: ITab[]) => void
}

export function TabsPanel(props: ITabsPanelProps): JSX.Element {
    const {tabs, isCentered = false, children, onUpdateTabs} = props

    return (
        <div className={getClassNames(styles.panel, {[styles.centered]: isCentered})}>
            <div className={styles.tabsWrapper}>
                <div className={styles.tabs}>
                    {tabs.map(tab => (
                        <div
                            key={tab.id}
                            data-test={tab.dataTest}
                            style={{maxWidth: `${100 / tabs.length}%`}}
                            onClick={() => {
                                const newTabs = tabs.slice(0)
                                newTabs.forEach(t => (t.active = t.id === tab.id))
                                onUpdateTabs(newTabs)
                            }}
                            className={`${styles.tab} ${tab.active && styles.active}`}>
                            {tab.icon && <div className={styles.icon}>{tab.icon}</div>}
                            {tab.activeIcon && <div className={styles.activeIcon}>{tab.activeIcon}</div>}
                            <p className={styles.title}>{tab.title}</p>
                            <div className={styles.leftDecorator} />
                            <div className={styles.rightDecorator} />
                            <div className={styles.bottomDecorator} />
                            {tab.closable && (
                                <CloseButton
                                    className={styles.closeButton}
                                    onClick={e => {
                                        e.stopPropagation()

                                        const index = tabs.findIndex(t => t.id === tab.id)
                                        const newTabs = tabs.slice(0)
                                        newTabs.splice(index, 1)

                                        if (!newTabs.some(t => t.active)) {
                                            const newActiveTab = newTabs[clamp(index - 1, 0, newTabs.length - 1)]
                                            newActiveTab.active = true
                                        }

                                        onUpdateTabs(newTabs)
                                    }}
                                />
                            )}
                        </div>
                    ))}
                </div>
            </div>
            {children ?? children}
        </div>
    )
}
