import * as React from 'react'
import {useEffect} from 'react'
import styles from './order-page.module.css'
import {TabsPanel} from '../tabs-panel/tabs-panel'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {OrderTabContent} from '../order-tab-content/order-tab-content'
import {OrderPageFooter} from '../order-page-footer/order-page-footer'
import {ConstructionTabContent} from '../construction-tab-content/construction-tab-content'
import {TemplateSelectorContainer} from '../../common/template-selector/template-selector-container'
import {getOrderPageUrl} from '../../../helpers/orders'
import {useOrderTabs, useListenOrderPageNavigation, useCheckOrderUserParameters} from '../../../hooks/orders'
import {useGoTo} from '../../../hooks/router'
import {useSetConnectingSide, useSetHostProduct, useShowTemplateSelector} from '../../../hooks/template-selector'
import {AddButton} from '../../common/button/add-button/add-button'
import {ORDER_TAB_ID} from '../../../constants/navigation'

export function OrderPage(): JSX.Element {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const tabs = useOrderTabs(28, 28, true)

    const goTo = useGoTo()
    const showTemplateSelector = useShowTemplateSelector()
    const setHostProduct = useSetHostProduct()
    const setConnectingSide = useSetConnectingSide()
    useListenOrderPageNavigation()

    const checkUserParameter = useCheckOrderUserParameters()

    useEffect(() => {
        currentOrder?.id && checkUserParameter()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentOrder?.id])

    const activeTab = tabs.find(t => t.active)

    return (
        <div className={styles.orderPage}>
            {currentOrder && (
                <>
                    <TabsPanel
                        tabs={tabs}
                        onUpdateTabs={updatedTabs => {
                            const activeTabId = updatedTabs.find(t => t.active)?.id ?? ORDER_TAB_ID
                            goTo(
                                getOrderPageUrl(
                                    currentOrder.id,
                                    updatedTabs.map(t => t.id),
                                    activeTabId
                                )
                            )
                        }}>
                        {!currentOrder.readonly && (
                            <AddButton
                                className={styles.addConstruction}
                                style="secondary"
                                onClick={() => {
                                    setHostProduct(null)
                                    setConnectingSide(null)
                                    showTemplateSelector(null)
                                }}
                            />
                        )}
                    </TabsPanel>
                    <div className={styles.content}>
                        {activeTab?.id === ORDER_TAB_ID && <OrderTabContent />}
                        {currentConstruction && currentConstruction.id === activeTab?.id && <ConstructionTabContent />}
                    </div>
                    <OrderPageFooter />
                </>
            )}
            <TemplateSelectorContainer />
        </div>
    )
}
