import * as React from 'react'
import './kp-page.css'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useCallback, useEffect} from 'react'
import {loadOrder} from '../../../redux/orders/actions'
import {Good} from '../../../model/framer/good'
import {Service} from '../../../model/framer/service'
import {RUB_SYMBOL} from '../../../constants/strings'
import {A4_HEIGHT, A4_PADDING} from '../../../constants/print-page'
import {PrintPageBuilder} from '../../../helpers/print-page-builder'
import {getCurrency} from '../../../helpers/currency'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {FOOTER_HEIGHT, Footer} from '../report/footer/footer'
import {getConstruction} from '../report/construction/construction'

const STANDARD_VERTICAL_MARGIN = 20
const STANDARD_TABLE_ROW_HEIGHT = 20

const AVAILABLE_HEIGHT = A4_HEIGHT - A4_PADDING * 2 - FOOTER_HEIGHT

function getGoodsTable(goods: Good[]): JSX.Element {
    const maxAttributesLength = Math.max(...goods.map(g => g.attributes.length))
    let index = 0

    return (
        <table className="kp-page__goods-table" cellSpacing={0}>
            <tbody>
                {goods.map(good => {
                    const columns = [
                        <td key={index++} className="kp-info-text">
                            {good.quantity} x {good.name} ({getCurrency(good.cost / good.quantity)} {RUB_SYMBOL})
                        </td>,
                        ...good.attributes.map((attribute, aidx) => (
                            <td key={`a-${good.id}-${aidx}`} className="kp-info-text">
                                {attribute.caption}: <span>{attribute.visibleValue}</span>
                            </td>
                        ))
                    ]
                    columns.length = maxAttributesLength + 2

                    for (let i = good.attributes.length + 1; i < maxAttributesLength + 1; i++) {
                        columns[i] = <td key={`${good.id}-${i}`} />
                    }

                    columns[maxAttributesLength + 1] = (
                        <td key={index++} className="kp-info-text">
                            <span>
                                {getCurrency(good.cost)} {RUB_SYMBOL}
                            </span>
                        </td>
                    )

                    return <tr key={good.id}>{columns}</tr>
                })}
            </tbody>
        </table>
    )
}

function getServicesTable(title: string, services: Service[]): JSX.Element {
    return (
        <div style={{margin: `${STANDARD_VERTICAL_MARGIN}pt 0`}}>
            <p
                style={{
                    fontSize: '9pt',
                    lineHeight: '9pt',
                    marginBottom: '16pt',
                    fontWeight: 'bold'
                }}>
                {title}
            </p>
            {services.map(service => (
                <div key={service.id} className="kp-info-text-wrapper" style={{justifyContent: 'space-between'}}>
                    <p className="kp-info-text">{service.name}</p>
                    <p className="kp-info-text">
                        <span>
                            {getCurrency(service.price)} {RUB_SYMBOL}
                        </span>
                    </p>
                </div>
            ))}
            <div className="kp-info-text-wrapper" style={{justifyContent: 'space-between'}}>
                <p className="kp-info-text">
                    <span>Итого услуги</span>
                </p>
                <p className="kp-info-text">
                    <span>
                        {getCurrency(services.reduce((sum, srvc) => sum + srvc.price, 0))} {RUB_SYMBOL}
                    </span>
                </p>
            </div>
        </div>
    )
}

export function KpPage(): JSX.Element | null {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const location = useSelector((state: IAppState) => state.router.location)
    const browserSize = useSelector((state: IAppState) => state.browser.windowSize)
    const dispatch = useDispatch()
    const loadOrderAction = useCallback((orderId: number | string) => dispatch(loadOrder(orderId)), [dispatch])

    useEffect(() => {
        const pageParams = new URLSearchParams(location.search)
        const orderId = pageParams.get('orderId')
        orderId && loadOrderAction(orderId)
    }, [location, loadOrderAction])

    useEffect(() => {
        if (currentOrder) {
            document.body.classList.add('kp-page__body', 'printable')
        }

        return () => {
            document.body.classList.remove('kp-page__body', 'printable')
        }
    }, [currentOrder])

    if (!currentOrder) {
        return null
    }

    const {constructions, totalCost, totalDiscount, createdAt, orderNumber, goods} = currentOrder
    const services = currentOrder.services.filter(s => s.enabled)
    const orderGoods = goods.filter(({constructionId}) => !constructionId)
    const orderServices = services.filter(({constructionId}) => !constructionId)

    let constructionsCount = 0
    let productsCount = 0
    let constructionsTotalCost = 0
    constructions.forEach(construction => {
        const goods = currentOrder.goods.filter(g => g.constructionId === construction.id)
        const services = currentOrder.services.filter(s => s.constructionId === construction.id)
        const totalGoodsCost = goods.reduce((s, g) => s + g.cost, 0)
        const totalServicesCost = services.reduce((s, service) => s + service.price, 0)

        constructionsCount += construction.quantity
        productsCount += construction.products.length * construction.quantity
        constructionsTotalCost += construction.totalCost + totalGoodsCost + totalServicesCost
    })

    const createAtStr = getDateDDMMYYYYFormat(new Date(createdAt))

    const printPageBuilder = new PrintPageBuilder(AVAILABLE_HEIGHT, (pageIndex, pagesCount) => {
        return <Footer text={`Заказ №: ${orderNumber} от ${createAtStr}.`} pageIndex={pageIndex} pagesCount={pagesCount} />
    })

    printPageBuilder.addView(
        <div className="kp-header">
            <div className="kp-header__row">
                <p className="kp-header__title">
                    Заказ №: <span>{orderNumber}</span> от {createAtStr}
                </p>
                <p className="kp-header__agent-name">{currentOrder.offerAddonLine1 ?? ''}</p>
            </div>
            <div className="kp-header__row" style={{justifyContent: 'flex-end'}}>
                <p className="kp-header__agent-contact">{currentOrder.offerAddonLine2 ?? ''}</p>
            </div>
        </div>,
        35
    )

    currentOrder.client &&
        printPageBuilder.addView(
            <div className="kp-info-text-wrapper">
                <p className="kp-info-text">
                    Заказчик: <span>{currentOrder.client.name}</span>
                </p>
            </div>,
            STANDARD_TABLE_ROW_HEIGHT
        )

    printPageBuilder.addView(
        <div className="kp-order-info__sum">
            <div className="kp-info-text-wrapper">
                <p className="kp-info-text">
                    Сумма:{' '}
                    <span>
                        {getCurrency(totalCost)} {RUB_SYMBOL}
                    </span>
                </p>
                {totalDiscount !== 0 && (
                    <>
                        <p className="kp-info-text">
                            Сумма без скидки:{' '}
                            <span>
                                {getCurrency(totalCost + totalDiscount)} {RUB_SYMBOL}
                            </span>
                        </p>
                    </>
                )}
            </div>
            {totalDiscount !== 0 && (
                <div className="kp-info-text-wrapper">
                    <p className="kp-info-text">
                        Скидка: <span>{currentOrder.discount.toString()}</span>
                    </p>
                </div>
            )}
        </div>,
        STANDARD_TABLE_ROW_HEIGHT
    )

    printPageBuilder.addView(
        <div className="kp-info-text-wrapper">
            <p className="kp-info-text">
                Количество конструкций:{' '}
                <span>
                    {constructionsCount} шт. (Изделий: {productsCount} шт.)
                </span>
            </p>
            <p className="kp-info-text">
                Общая площадь:{' '}
                <span>
                    {currentOrder.totalSquare} м<sup>2</sup>
                </span>
            </p>
            <p className="kp-info-text">
                Общий вес: <span>{currentOrder.totalWeight} кг</span>
            </p>
        </div>,
        STANDARD_TABLE_ROW_HEIGHT
    )

    currentOrder.mountingDate &&
        printPageBuilder.addView(
            <div className="kp-info-text-wrapper">
                <p className="kp-info-text">
                    Дата монтажа: <span>{getDateDDMMYYYYFormat(new Date(currentOrder.mountingDate))}</span>
                </p>
            </div>,
            STANDARD_TABLE_ROW_HEIGHT
        )

    printPageBuilder.addView(
        <p className="kp-info-text" style={{marginTop: '7pt'}}>
            Цена действительна в течение <span>5 календарных дней</span>
        </p>,
        15
    )

    printPageBuilder.addView(
        <p
            style={{
                fontSize: '9pt',
                lineHeight: '9pt',
                fontWeight: 'bold',
                marginBottom: '14pt',
                marginTop: '20pt'
            }}>
            КОНСТРУКЦИИ
        </p>,
        43
    )

    currentOrder.sortedConstructions.forEach((construction, idx) => {
        const services = currentOrder.services.filter(({constructionId}) => constructionId === construction.id)
        const goods = currentOrder.goods.filter(({constructionId}) => constructionId === construction.id)

        const position = construction.position || idx + 1
        const constructionViews = getConstruction(construction, `Конструкция ${position}`, browserSize)
        constructionViews.map(({view, height}) => printPageBuilder.addView(view, height))

        if (services.length > 0) {
            printPageBuilder.addView(
                getServicesTable(`Услуги к конструкции ${position}`, services),
                STANDARD_VERTICAL_MARGIN + 25 + services.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT
            )
        }

        if (goods.length > 0) {
            printPageBuilder.addView(
                <div style={{margin: `${STANDARD_VERTICAL_MARGIN}pt 0`}}>
                    <p
                        style={{
                            fontSize: '9pt',
                            lineHeight: '9pt',
                            marginBottom: '16pt',
                            fontWeight: 'bold'
                        }}>
                        Дополнительные материалы к конструкции {position}
                    </p>
                    {getGoodsTable(goods)}
                    <div className="kp-info-text-wrapper" style={{justifyContent: 'space-between'}}>
                        <p className="kp-info-text">
                            <span>Итого дополнительные материалы</span>
                        </p>
                        <p className="kp-info-text">
                            <span>
                                {getCurrency(goods.reduce((sum, good) => sum + good.cost, 0))} {RUB_SYMBOL}
                            </span>
                        </p>
                    </div>
                </div>,
                STANDARD_VERTICAL_MARGIN + 25 + goods.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT
            )
        }
    })

    printPageBuilder.addView(
        <div className="kp-info-text-wrapper" style={{justifyContent: 'space-between', marginTop: `${STANDARD_VERTICAL_MARGIN}pt`}}>
            <p className="kp-info-text">
                <span>Итого конструкции</span>
            </p>
            <p className="kp-info-text">
                <span>
                    {getCurrency(constructionsTotalCost)} {RUB_SYMBOL}
                </span>
            </p>
        </div>,
        STANDARD_TABLE_ROW_HEIGHT
    )

    if (orderGoods.length > 0) {
        printPageBuilder.addView(
            <div style={{marginTop: `${STANDARD_VERTICAL_MARGIN}pt`}}>
                <p
                    style={{
                        fontSize: '9pt',
                        lineHeight: '9pt',
                        marginBottom: '16pt',
                        fontWeight: 'bold'
                    }}>
                    ДОПОЛНИТЕЛЬНЫЕ МАТЕРИАЛЫ
                </p>
                {getGoodsTable(orderGoods)}
                <div className="kp-info-text-wrapper" style={{justifyContent: 'space-between'}}>
                    <p className="kp-info-text">
                        <span>Итого дополнительные материалы</span>
                    </p>
                    <p className="kp-info-text">
                        <span>
                            {getCurrency(goods.reduce((sum, good) => sum + good.cost, 0))} {RUB_SYMBOL}
                        </span>
                    </p>
                </div>
            </div>,
            STANDARD_VERTICAL_MARGIN + 25 + goods.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT
        )
    }

    if (orderServices.length > 0) {
        printPageBuilder.addView(
            getServicesTable('УСЛУГИ', orderServices),
            STANDARD_VERTICAL_MARGIN + 25 + services.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT
        )
    }

    printPageBuilder.addView(
        <div
            style={{
                marginBottom: '34pt'
            }}>
            <p
                style={{
                    fontSize: '9pt',
                    lineHeight: '9pt',
                    marginBottom: '16pt',
                    fontWeight: 'bold',
                    marginTop: `${STANDARD_VERTICAL_MARGIN}pt`
                }}>
                ИТОГО
            </p>
            <div className="kp-info-text-wrapper" style={{justifyContent: totalDiscount !== 0 ? 'space-between' : 'flex-end'}}>
                {totalDiscount !== 0 && (
                    <>
                        <p className="kp-info-text">
                            Сумма без скидки:{' '}
                            <span>
                                {getCurrency(totalCost + totalDiscount)} {RUB_SYMBOL}
                            </span>
                        </p>
                        <p className="kp-info-text">
                            Скидка:{' '}
                            <span>
                                {getCurrency(totalDiscount)} {RUB_SYMBOL}
                            </span>
                        </p>
                    </>
                )}
                <p className="kp-info-text">
                    Сумма{totalDiscount !== 0 ? ' со скидкой' : ''}:{' '}
                    <span>
                        {getCurrency(totalCost)} {RUB_SYMBOL}
                    </span>
                </p>
            </div>
        </div>,
        25 + STANDARD_VERTICAL_MARGIN + STANDARD_TABLE_ROW_HEIGHT + 34
    )

    printPageBuilder.addView(
        <>
            <div className="kp-order-total-info__text">
                <p>
                    Заказчик уведомлен о том, что согласно ГОСТу 23166-99 «Блоки оконные. Общие технические условия» для обеспечения
                    безопасности, в целях предотвращения травматизма и возможности выпадения детей из окон в детских школьных, дошкольных
                    учреждениях, а так же в жилых домах, блоки оконные должны быть укомплектованы замками безопасности (пункт 5.18 ГОСТа
                    23166-99 «Блоки оконные. Общие технические условия». При отказе Заказчика от укомплектования оконного блока замком
                    безопасности Заказчик принимает на себя все последствия такого отказа.
                </p>
                <p>
                    <span>Внимание!</span> Подписывая заказ, Вы тем самым подтверждаете КОНФИГУРАЦИЮ, КОМПЛЕКТАЦИЮ и ВСЕ ВНУТРЕННИЕ РАЗМЕРЫ
                    заказываемых изделий. Убедитесь, что все ваши пожелания учтены. Все предварительные договоренности, не отраженные в заказе,
                    считаются недействительными.
                </p>
            </div>
            <div className="kp-signatures">
                <div className="kp-signature">
                    <div>
                        <p className="kp-signature__title">Заказчик согласен</p>
                        <p className="kp-signature__title">Расшифровка подписи</p>
                    </div>
                    <p className="kp-signature__hint">с предложенной комплектацией и стоимостью согласен (подпись)</p>
                </div>
            </div>
        </>,
        108
    )

    return <div className="kp-page">{printPageBuilder.build()}</div>
}
