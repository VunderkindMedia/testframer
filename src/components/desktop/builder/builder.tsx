import * as React from 'react'
import {useEffect, useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {login} from '../../../redux/account/actions'
import {setRestrictedMode} from '../../../redux/settings/actions'
import {ConstructionTabContent} from '../construction-tab-content/construction-tab-content'
import {LP_ACCOUNT} from '../../../constants/demo'
import {setCurrentConstructionAsync} from '../../../redux/orders/actions'
import {useCreateDemoOrder} from '../../../hooks/template-selector'
import {OrderPageFooter} from '../order-page-footer/order-page-footer'
import {IAppState} from '../../../redux/root-reducer'
import styles from './builder.module.css'

export const Builder = (): JSX.Element => {
    const token = useSelector((state: IAppState) => state.account.token)
    const account = useSelector((state: IAppState) => state.account.login)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const dispatch = useDispatch()

    const createDemoOrder = useCreateDemoOrder()

    const isLoggedIn = useCallback(() => {
        return token && account === LP_ACCOUNT.login
    }, [token, account])

    useEffect(() => {
        dispatch(setRestrictedMode(true))

        return () => {
            dispatch(setRestrictedMode(false))
        }
    }, [dispatch])

    useEffect(() => {
        const {login: lpLogin, password} = LP_ACCOUNT
        if (!isLoggedIn()) {
            dispatch(login(lpLogin, password, true))
        }
    }, [isLoggedIn, dispatch])

    useEffect(() => {
        if (isLoggedIn()) {
            createDemoOrder()
        }
    }, [isLoggedIn, createDemoOrder])

    useEffect(() => {
        currentOrder && dispatch(setCurrentConstructionAsync(currentOrder.constructions[0]))
    }, [currentOrder, dispatch])

    return (
        <div className={styles.builder}>
            <ConstructionTabContent className={styles.tabContent} />
            <OrderPageFooter />
        </div>
    )
}
