import * as React from 'react'
import {useEffect, useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {HINTS} from '../../../constants/hints'
import {OrderParamsCard} from '../../common/order-params-card/order-params-card'
import {Button} from '../../common/button/button'
import {loadConstructionParams} from '../../../redux/builder/actions'
import {useRecalculateOrder} from '../../../hooks/orders'
import {IAppState} from '../../../redux/root-reducer'
import {ConfigContexts} from '../../../model/config-contexts'
import {ProfileSelector} from '../../common/profile-selector/profile-selector'
import {FurnitureSelector} from '../../common/furniture-selector/furniture-selector'
import {FillingSelector} from '../../common/filling-selector/filling-selector'
import styles from './order-construction-params.module.css'
import {getClassNames} from '../../../helpers/css'
import {GlassColorSelector} from '../../common/glass-color-selector/glass-color-selector'

interface IOrderConstructionParamsProps {
    onClickHeader?: () => void
}

enum Tabs {
    Profile = 'profile',
    Furniture = 'furnitureSelector',
    Filling = 'fillingSelector',
    GlassColor = 'glassColorSelector'
}

export function OrderConstructionParams(props: IOrderConstructionParamsProps): JSX.Element | null {
    const {onClickHeader} = props
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const genericProduct = useSelector((state: IAppState) => state.orders.genericProduct)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)

    const recalculateOrder = useRecalculateOrder()
    const dispatch = useDispatch()
    const [selector, setSelector] = useState<JSX.Element | null>()
    const [activeTab, setActiveTab] = useState<Tabs>()

    useEffect(() => {
        configContext === ConfigContexts.ConstructionParams && currentOrder && dispatch(loadConstructionParams(currentOrder))
    }, [dispatch, configContext, currentOrder])

    if (!currentOrder || !genericProduct) {
        return null
    }
    const fillings = genericProduct?.availableFillings ?? []
    return (
        <OrderParamsCard title="Заменить во всех конструкциях" hint={HINTS.orderReplaceConstructionParams} onClickHeader={onClickHeader}>
            <div className={styles.orderConstructionParams}>
                <div className={styles.widgets}>
                    <button
                        className={getClassNames(styles.widget, {[styles.active]: activeTab === Tabs.Profile})}
                        onClick={() => {
                            setActiveTab(Tabs.Profile)
                            setSelector(<ProfileSelector />)
                        }}>
                        Профиль:&nbsp;<span>{genericProduct.profile?.name ?? '-'}</span>
                    </button>
                    <button
                        className={getClassNames(styles.widget, {[styles.active]: activeTab === Tabs.Furniture})}
                        onClick={() => {
                            setActiveTab(Tabs.Furniture)
                            setSelector(<FurnitureSelector />)
                        }}>
                        Фурнитура:&nbsp;<span>{genericProduct.furniture?.displayName ?? '-'}</span>
                    </button>
                    {fillings.length > 0 && (
                        <button
                            className={getClassNames(styles.widget, {[styles.active]: activeTab === Tabs.Filling})}
                            onClick={() => {
                                setActiveTab(Tabs.Filling)
                                setSelector(<FillingSelector />)
                            }}>
                            Заполнение:&nbsp;<span>{genericProduct.frame.allSolidFillings[0].filling?.marking ?? '-'}</span>
                        </button>
                    )}
                    {genericProduct.frame.allSolidFillings[0].filling?.isMultifunctional && (
                        <button
                            className={getClassNames(styles.widget, {[styles.active]: activeTab === Tabs.GlassColor})}
                            onClick={() => {
                                setActiveTab(Tabs.GlassColor)
                                setSelector(<GlassColorSelector />)
                            }}>
                            Цвет:&nbsp;<span>{genericProduct.frame.allSolidFillings[0].glassColor ?? '-'}</span>
                        </button>
                    )}
                </div>
                <div className={styles.selector}>{selector}</div>
                <Button className={styles.recalculateButton} buttonStyle="with-border" onClick={() => recalculateOrder(currentOrder)}>
                    Пересчитать
                </Button>
            </div>
        </OrderParamsCard>
    )
}
