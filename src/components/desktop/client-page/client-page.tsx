import * as React from 'react'
import {useCallback} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useClientFromSearch, useClientOrders, useUpdateClient, useCreateClient, useSetCurrentClient} from '../../../hooks/clients'
import {PageTitle} from '../../common/page-title/page-title'
import {HINTS} from '../../../constants/hints'
import {Client} from '../../../model/framer/client'
import {getTextOrMdash} from '../../../helpers/text'
import {OrdersTable} from './orders-table/orders-table'
import {ClientEditor} from '../../common/client-editor/client-editor'
import styles from './client-page.module.css'

export const ClientPage = (): JSX.Element | null => {
    const requestCount = useSelector((state: IAppState) => state.request.count)
    const client = useClientFromSearch()
    const orders = useClientOrders()
    const updateClient = useUpdateClient()
    const createClient = useCreateClient()
    const setCurrentClient = useSetCurrentClient()

    const handleClientChange = useCallback(
        (client: Client) => {
            if (client.isNew) {
                createClient(client)
            } else {
                updateClient(client)
                setCurrentClient(client)
            }
        },
        [updateClient, createClient, setCurrentClient]
    )

    if (!client) {
        return null
    }

    const {owner, filial} = client

    return (
        <div className={styles.page}>
            <div className={styles.content}>
                <PageTitle title={client.isNew ? 'Новый клиент' : 'Клиент'} className={styles.title} />
                <div className={styles.info}>
                    <ClientEditor client={client} onChange={handleClientChange} className={styles.form} />
                    {!client.isNew && (
                        <div>
                            <p>Менеджер: {getTextOrMdash(owner ? owner.name || owner.email : '')}</p>
                            <p>Филиал: {getTextOrMdash(filial ? filial.name : '')}</p>
                        </div>
                    )}
                </div>
                {!client.isNew && (
                    <>
                        <PageTitle title="Сделки" className={styles.title} />
                        {orders.length > 0 ? (
                            <OrdersTable orders={orders} className={styles.orders} />
                        ) : (
                            <>{requestCount === 0 && <p>{HINTS.clientOrdersNotFound}</p>}</>
                        )}
                    </>
                )}
            </div>
            <div className={styles.sidebar}></div>
        </div>
    )
}
