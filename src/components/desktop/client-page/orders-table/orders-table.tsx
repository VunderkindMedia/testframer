import * as React from 'react'
import {RUB_SYMBOL} from '../../../../constants/strings'
import {Order} from '../../../../model/framer/order'
import {getDateDDMMYYYYFormat} from '../../../../helpers/date'
import {SvgIconConstructionDrawer} from '../../../common/svg-icon-construction-drawer/svg-icon-construction-drawer'
import {Colors} from '../../../../constants/colors'
import {getOrderStateName, getOrderStateColor} from '../../../../model/framer/order-states'
import {getCurrency} from '../../../../helpers/currency'
import {ConstructionPreview} from '../../../common/construction-preview/construction-preview'
import {ErrorTypes} from '../../../../model/framer/error-types'
import styles from './orders-table.module.css'

interface IOrdersTableProps {
    orders: Order[]
    className?: string
}

export const OrdersTable = (props: IOrdersTableProps): JSX.Element => {
    const {orders, className} = props

    return (
        <div className={className}>
            <div className="standard-table">
                <table>
                    <thead>
                        <tr>
                            <th>Дата создания</th>
                            <th>Номер</th>
                            <th>Конструкции</th>
                            <th>Сумма,&nbsp;{RUB_SYMBOL}</th>
                            <th>Статус заказа</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orders.map(order => (
                            <tr key={order.id} className={`${styles.row} ${order.haveEnergyProducts && styles.energy}`}>
                                <td>{getDateDDMMYYYYFormat(new Date(order.createdAt))}</td>
                                <td>{order.fullOrderNumber}</td>
                                <td>
                                    <div className={styles.orderConstructions}>
                                        {order.constructions.slice(0, 3).map(construction => {
                                            const errors = construction.errors.filter(error => error.errorType === ErrorTypes.Error)
                                            return (
                                                <ConstructionPreview quantity={construction.quantity} isAutoWidth={true} key={construction.id}>
                                                    <SvgIconConstructionDrawer
                                                        construction={construction}
                                                        maxWidth={280}
                                                        maxHeight={28}
                                                        lineColor={errors.length > 0 ? Colors.Red : Colors.Gray}
                                                    />
                                                </ConstructionPreview>
                                            )
                                        })}
                                        {order.constructions.length > 3 && (
                                            <p className={styles.moreText} style={{color: Colors.Gray.toString()}}>
                                                ...
                                            </p>
                                        )}
                                    </div>
                                </td>
                                <td className={styles.price}>
                                    {order.totalCost === 0 ? (
                                        <span>&mdash;</span>
                                    ) : (
                                        <>
                                            {getCurrency(order.totalCost)} <span className={styles.currency}>{RUB_SYMBOL}</span>
                                        </>
                                    )}
                                </td>
                                <td>
                                    <p style={{color: getOrderStateColor(order.state)}}>{getOrderStateName(order.state)}</p>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
