import * as React from 'react'
import styles from './order-constructions-card.module.css'
import {Order} from '../../../model/framer/order'
import {SvgIconConstructionDrawer} from '../../common/svg-icon-construction-drawer/svg-icon-construction-drawer'
import {QuantityEditor} from '../../common/quantity-editor/quantity-editor'
import {Good} from '../../../model/framer/good'
import {Service} from '../../../model/framer/service'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ReactComponent as CacheIconSmall} from '../../../resources/images/cache-icon-small.inline.svg'
import {ReactComponent as CacheIconSmallGrey} from '../../../resources/images/cache-icon-small-grey.inline.svg'
import {EditButton} from '../../common/button/edit-button/edit-button'
import {Construction} from '../../../model/framer/construction'
import {ConfigContexts} from '../../../model/config-contexts'
import {Card} from '../../common/card/card'
import {useSetCurrentConstruction, useShowRealCost, useIsSubdealerOrder} from '../../../hooks/orders'
import {useSetConfigContext} from '../../../hooks/builder'
import {Hint} from '../../common/hint/hint'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {HINTS} from '../../../constants/hints'
import {ConstructionErrorsTooltip} from '../../common/construction-errors-tooltip/construction-errors-tooltip'
import {getCurrency} from '../../../helpers/currency'
import {VisibilityButton} from '../../common/button/visibility-button/visibility-button'
import {GoodSources} from '../../../model/framer/good-sources'
import {MAX_CONSTRUCTION_COUNT} from '../../../constants/settings'

interface IOrderCardProps {
    order: Order
    onChangeOrder: (order: Order) => void
    onEditConstructionClick: (construction: Construction) => void
}

export function OrderConstructionsCard(props: IOrderCardProps): JSX.Element {
    const {order, onChangeOrder, onEditConstructionClick} = props

    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)

    const setConfigContext = useSetConfigContext()
    const setCurrentConstruction = useSetCurrentConstruction()
    const getIsSubdealerOrder = useIsSubdealerOrder()

    const isSubdealerOrder = getIsSubdealerOrder(order)
    const allFactoryGoods = useSelector((state: IAppState) => state.goods.factoryGoods)
    const allPartnerGoods = useSelector((state: IAppState) => state.goods.partnerGoods)
    const hasAvailableGoods = allFactoryGoods.length > 0 || allPartnerGoods.length > 0

    const [shouldShowRealCost, setShouldShowRealCost] = useShowRealCost()

    const constructionGoodsMap = new Map<string, Good[]>()
    const constructionServicesMap = new Map<string, Service[]>()

    let totalConstructionsCost = 0
    let totalConstructionsAmegaCost = 0
    let totalConstructionsAmegaCostToDisplay = 0
    let totalGoodsCost = 0
    let totalGoodsAmegaCostToDisplay = 0
    let totalGoodsAmegaCost = 0
    let totalServicesCost = 0

    order.constructions.forEach(construction => {
        const goods = order.goods.filter(g => g.constructionId === construction.id)
        const services = order.services.filter(s => s.constructionId === construction.id)

        constructionGoodsMap.set(construction.id, goods)
        constructionServicesMap.set(construction.id, services)

        goods.map(g => {
            totalGoodsCost += g.cost
            totalGoodsAmegaCostToDisplay += g.amegaCostToDisplay
            totalGoodsAmegaCost += g.amegaCost
        })

        totalServicesCost += services.reduce((s, service) => s + service.price, 0)

        totalConstructionsCost += construction.totalCost
        totalConstructionsAmegaCostToDisplay += construction.amegaCostToDisplay
        totalConstructionsAmegaCost += construction.amegaCost
    })

    const showAmegaCost = isSubdealerOrder && order.filial?.isExternal

    return (
        <Card>
            <div className={styles.constructionsTable}>
                <div className={`${styles.constructionsTableRow} ${styles.constructionsTableRowHeader}`}>
                    <div className={styles.constructionsTableColumn}>Конструкции</div>
                    <div className={`${styles.constructionsTableColumn} ${styles.constructionsTableColumnTitle}`}>
                        <span>Доп. материалы</span>
                        <Hint text={HINTS.constructionGoodsCardTitle} />
                    </div>
                    <div className={`${styles.constructionsTableColumn} ${styles.constructionsTableColumnTitle}`}>
                        <span>Услуги</span>
                        <Hint text={HINTS.constructionServicesCardTitle} />
                    </div>
                    <div className={`${styles.constructionsTableColumn} ${styles.constructionsTableColumnTitle}`}>
                        <span>Итого</span>
                        <VisibilityButton isVisible={shouldShowRealCost} onClick={() => setShouldShowRealCost(!shouldShowRealCost)} />
                    </div>
                </div>
                {order.sortedConstructions.map((construction, idx) => {
                    const {size, weight, square, quantity, id} = construction

                    const position = construction.position || idx + 1
                    const goods = constructionGoodsMap.get(construction.id) ?? []
                    const services = constructionServicesMap.get(construction.id) ?? []

                    let goodsCost = 0
                    let goodsAmegaCost = 0
                    let goodsAmegaCostToDisplay = 0

                    goods.map(g => {
                        goodsCost += g.cost
                        goodsAmegaCost += g.amegaCost
                        goodsAmegaCostToDisplay += g.amegaCostToDisplay
                    })
                    const servicesCost = services.reduce((s, service) => s + service.price, 0)

                    const totalCost = construction.totalCost + goodsCost + servicesCost
                    const totalAmegaCostToDisplay = construction.amegaCostToDisplay + goodsAmegaCostToDisplay
                    const totalAmegaCost = construction.amegaCost + goodsAmegaCost
                    // TODO: доабвить отображение стоимости услуг ЗАВОДА, когда они появяться
                    return (
                        <div
                            key={id}
                            className={`${currentConstruction?.id === construction.id && 'active-construction-row-table'} ${
                                styles.constructionsTableRow
                            }`}
                            onClick={() => {
                                setCurrentConstruction(construction)
                            }}>
                            <div className={`${styles.constructionsTableColumn} ${styles.constructionsTableColumnConstructionInfo}`}>
                                <div className={styles.constructionsInfo}>
                                    <div className={styles.constructionTitle}>
                                        <SvgIconConstructionDrawer construction={construction} maxWidth={24} maxHeight={24} />
                                        <p>Конструкция {position}</p>
                                        <ConstructionErrorsTooltip construction={construction} />
                                    </div>
                                    <ul className={styles.constructionsDetails}>
                                        {construction.energyProductType && <li>Комплектация {construction.energyProductTypeName}</li>}
                                        <li>
                                            Профиль{' '}
                                            {construction.products[0].profile.displayName
                                                ? construction.products[0].profile.displayName
                                                : construction.products[0].profile.name}
                                        </li>
                                        <li>Фурнитура {construction.products[0].furniture.displayName}</li>
                                        <li>
                                            Заполнение{' '}
                                            {[...new Set(construction.products[0].frame.allSolidFillings.map(f => f.fillingVendorCode))].join(
                                                ', '
                                            )}
                                        </li>
                                        <li>
                                            {size.width} x {size.height} мм
                                        </li>
                                        <li>
                                            {weight} кг, {square} кв.м
                                        </li>
                                        <li>
                                            Цена: {getCurrency(construction.totalCost / quantity)}{' '}
                                            {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}{' '}
                                            {shouldShowRealCost && (
                                                <span className={styles.initPrice}>
                                                    {showAmegaCost ? (
                                                        <span>
                                                            / {getCurrency(construction.amegaCostToDisplay / quantity)}{' '}
                                                            {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL} (
                                                            {getCurrency(construction.amegaCost / quantity)}{' '}
                                                            {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                                        </span>
                                                    ) : (
                                                        <span>
                                                            ({getCurrency(construction.amegaCostToDisplay / quantity)}{' '}
                                                            {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                                        </span>
                                                    )}
                                                </span>
                                            )}
                                        </li>
                                    </ul>
                                </div>
                                <div className={styles.quantityEditorWrapper}>
                                    <QuantityEditor
                                        isDisabled={order.readonly}
                                        quantity={construction.quantity}
                                        min={1}
                                        max={MAX_CONSTRUCTION_COUNT}
                                        onChange={quantity => {
                                            const orderClone = order.clone
                                            const constructionClone = orderClone.findConstructionById(construction.id)
                                            if (constructionClone && quantity !== constructionClone.quantity) {
                                                constructionClone.quantity = quantity
                                                orderClone.addAction(`Задали количество конструкций ${quantity}`, constructionClone)
                                                onChangeOrder(orderClone)
                                            }
                                        }}
                                    />
                                </div>
                                <div className={styles.constructionsTableTotalConstructionCost}>
                                    <EditButton onClick={() => onEditConstructionClick(construction)} />
                                    <p className={styles.constructionsTableTotalCost}>
                                        {getCurrency(construction.totalCost)}{' '}
                                        {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}{' '}
                                        {shouldShowRealCost && (
                                            <span className={styles.initPrice}>
                                                {showAmegaCost ? (
                                                    <span>
                                                        / {getCurrency(construction.amegaCostToDisplay / quantity)}{' '}
                                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL} (
                                                        {getCurrency(construction.amegaCost / quantity)}{' '}
                                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                                    </span>
                                                ) : (
                                                    <span>
                                                        ({getCurrency(construction.amegaCostToDisplay / quantity)}{' '}
                                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                                    </span>
                                                )}
                                            </span>
                                        )}
                                    </p>
                                </div>
                            </div>
                            <div className={`${styles.constructionsTableColumn} ${styles.constructionsTableColumnConstruction}`}>
                                <ul className={styles.additions}>
                                    {goods.map((good, idx) => (
                                        <li key={good.id} className={styles.additionsRow}>
                                            <span>
                                                {idx + 1}. {good.name}
                                                {good.attributes.length === 1 && good.attributes[0].name === 'thick'
                                                    ? ` (${good.attributes[0].value}мм) `
                                                    : good.attributes.length === 2 &&
                                                      good.attributes[0].name === 'width' &&
                                                      good.attributes[1].name === 'height'
                                                    ? ` (${good.attributes[0].value}мм x ${good.attributes[1].value}мм) `
                                                    : ''}
                                                x {good.quantity}
                                                {shouldShowRealCost && good.goodSource === GoodSources.Factory && (
                                                    <span className={styles.initPrice}>
                                                        ({getCurrency(good.amegaCostToDisplay / good.quantity)}&nbsp;
                                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                                    </span>
                                                )}
                                            </span>
                                            <span className={styles.constructionsTableTotalCost}>
                                                {getCurrency(good.cost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}{' '}
                                                {shouldShowRealCost && good.goodSource === GoodSources.Factory && (
                                                    <span className={styles.initPrice}>
                                                        ({getCurrency(good.amegaCostToDisplay)}{' '}
                                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                                    </span>
                                                )}
                                            </span>
                                        </li>
                                    ))}
                                </ul>
                                <EditButton
                                    isDisabled={
                                        order.readonly || (constructionGoodsMap.get(construction.id)?.length === 0 && !hasAvailableGoods)
                                    }
                                    onClick={() => {
                                        setCurrentConstruction(construction)
                                        setConfigContext(ConfigContexts.Goods)
                                    }}
                                />
                                {goods.length > 0 && (
                                    <p className={styles.constructionsTableTotalCost}>
                                        {getCurrency(goodsCost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}{' '}
                                        {shouldShowRealCost && (
                                            <span className={styles.initPrice}>
                                                ({getCurrency(goodsAmegaCostToDisplay)}{' '}
                                                {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                            </span>
                                        )}
                                    </p>
                                )}
                            </div>
                            <div className={`${styles.constructionsTableColumn} ${styles.constructionsTableColumnConstruction}`}>
                                <ul className={styles.additions}>
                                    {services.map((service, idx) => (
                                        <li key={`${service.id}-${idx}`} className={styles.additionsRow}>
                                            <span>
                                                {idx + 1}. {service.name}
                                            </span>
                                            <span className={styles.constructionsTableTotalCost}>
                                                {getCurrency(service.price)}&nbsp;
                                                {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                                            </span>
                                        </li>
                                    ))}
                                </ul>
                                <EditButton
                                    isDisabled={order.readonly}
                                    onClick={() => {
                                        setCurrentConstruction(construction)
                                        setConfigContext(ConfigContexts.Services)
                                    }}
                                />
                                {services.length > 0 && (
                                    <p className={styles.constructionsTableTotalCost}>
                                        {getCurrency(servicesCost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                                    </p>
                                )}
                            </div>
                            <div className={`${styles.constructionsTableColumn} ${styles.constructionsTableColumnTotal}`}>
                                <p className={styles.constructionsTableConstructionCost}>
                                    {getCurrency(totalCost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                                    <br />
                                    {shouldShowRealCost && (
                                        <span>
                                            {showAmegaCost ? (
                                                <>
                                                    {getCurrency(totalAmegaCostToDisplay)}{' '}
                                                    {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL}
                                                    <br />({getCurrency(totalAmegaCost)}{' '}
                                                    {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                                </>
                                            ) : (
                                                <span>
                                                    ({getCurrency(construction.amegaCostToDisplay / quantity)}{' '}
                                                    {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                                </span>
                                            )}
                                        </span>
                                    )}
                                </p>
                            </div>
                        </div>
                    )
                })}
                <div className={`${styles.constructionsTableRow} ${styles.constructionsTableRowFinal}`}>
                    <p className={styles.finalCost}>
                        {getCurrency(totalConstructionsCost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                        <br />
                        {shouldShowRealCost && (
                            <span>
                                {showAmegaCost ? (
                                    <span>
                                        {getCurrency(totalConstructionsAmegaCostToDisplay)}{' '}
                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL} (
                                        {getCurrency(totalConstructionsAmegaCost)}{' '}
                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                    </span>
                                ) : (
                                    <span>
                                        ({getCurrency(totalConstructionsAmegaCostToDisplay)}{' '}
                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                    </span>
                                )}
                            </span>
                        )}
                    </p>
                    <p className={styles.finalCost}>
                        {getCurrency(totalGoodsCost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                        <br />
                        {shouldShowRealCost && (
                            <span>
                                ({getCurrency(totalGoodsAmegaCostToDisplay)}{' '}
                                {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                            </span>
                        )}
                    </p>
                    <p className={styles.finalCost}>
                        {getCurrency(totalServicesCost)} {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                    </p>
                    <p className={styles.constructionsTableConstructionCost}>
                        {getCurrency(totalConstructionsCost + totalGoodsCost + totalServicesCost)}{' '}
                        {order.calculatedWithFramerPoints ? <CacheIconSmall /> : RUB_SYMBOL}
                        <br />
                        {shouldShowRealCost && (
                            <span>
                                {showAmegaCost ? (
                                    <>
                                        {getCurrency(totalConstructionsAmegaCostToDisplay + totalGoodsAmegaCostToDisplay)}{' '}
                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL}
                                        <br />({getCurrency(totalConstructionsAmegaCost + totalGoodsAmegaCost)}{' '}
                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                    </>
                                ) : (
                                    <span>
                                        ({getCurrency(totalConstructionsAmegaCostToDisplay + totalGoodsAmegaCostToDisplay)}{' '}
                                        {order.calculatedWithFramerPoints ? <CacheIconSmallGrey /> : RUB_SYMBOL})
                                    </span>
                                )}
                            </span>
                        )}
                    </p>
                </div>
            </div>
        </Card>
    )
}
