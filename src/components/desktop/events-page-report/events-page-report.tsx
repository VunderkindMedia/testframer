import * as React from 'react'
import {useEffect, useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {PrintPageBuilder} from '../../../helpers/print-page-builder'
import styles from './events-page-report.module.css'
import {A4_HEIGHT, A4_PADDING} from '../../../constants/print-page'
import {Footer, FOOTER_HEIGHT} from '../report/footer/footer'
import {getDateMMYYYYFormat, getDateDDMMYYYYFormat, getAllDatesFromRange, getWeekdayName} from '../../../helpers/date'
import {EventType} from '../events-page/event-type/event-type'
import {CalendarEvent} from '../../../model/framer/calendar/calendar-event'
import {loadCalendarEvents} from '../../../redux/calendar/actions'

const AVAILABLE_HEIGHT = A4_HEIGHT - A4_PADDING * 2 - FOOTER_HEIGHT
const ROW_HEIGHT = 28

export function EventsPageReport(): JSX.Element | null {
    const location = useSelector((state: IAppState) => state.router.location)
    const events = useSelector((state: IAppState) => state.calendar.events)
    const partner = useSelector((state: IAppState) => state.account.partner)
    const [startDate, setStartDate] = useState<Date | null>(null)
    const [endDate, setEndDate] = useState<Date | null>(null)
    const [company, setCompany] = useState('')
    const dispatch = useDispatch()

    useEffect(() => {
        const pageParams = new URLSearchParams(location.search)
        const from = Number(pageParams.get('from'))
        const to = Number(pageParams.get('to'))

        if (from && !isNaN(from)) {
            setStartDate(new Date(from))
        }

        if (to && !isNaN(to)) {
            setEndDate(new Date(to))
        }
    }, [location])

    useEffect(() => {
        document.body.classList.add(styles.report, 'printable')

        return () => {
            document.body.classList.remove(styles.report, 'printable')
        }
    }, [])

    useEffect(() => {
        if (!partner) {
            return
        }

        setCompany(partner.offerAddonLine1)
    }, [partner])

    useEffect(() => {
        startDate && endDate && dispatch(loadCalendarEvents(''))
    }, [dispatch, endDate, startDate])

    if (!startDate || !endDate) {
        return null
    }

    const dates = getAllDatesFromRange(startDate, endDate)

    const printPageBuilder = new PrintPageBuilder(AVAILABLE_HEIGHT, (pageIndex, pagesCount) => {
        return <Footer pageIndex={pageIndex} pagesCount={pagesCount} />
    })

    printPageBuilder.addView(
        <div className={styles.header}>
            <p className={styles.title}>Календарь {getDateMMYYYYFormat(startDate)}</p>
            <p className={styles.details}>
                <span>{company}</span>
                <span>Создан: {getDateDDMMYYYYFormat(new Date())}</span>
            </p>
        </div>,
        31
    )

    console.log('events', events)

    const eventsByDay = new Map<string, CalendarEvent[]>()
    events.map(event => {
        const dayEvents = eventsByDay.get(event.date)

        if (dayEvents) {
            dayEvents.push(event)
        } else {
            eventsByDay.set(event.date, [event])
        }
    })

    dates.map(date => {
        const dateStr = getDateDDMMYYYYFormat(date)
        const dayEvents = eventsByDay.get(dateStr) ?? []

        printPageBuilder.addView(
            <div key={dateStr} className={styles.row}>
                <span className={styles.day}>{date.getDate()}</span>
                <span className={styles.weekday}>{getWeekdayName(date)}</span>
                <div>
                    {dayEvents.length > 0 ? (
                        dayEvents.map((e, index) => (
                            <div key={`${dateStr}-${index}`} className={styles.eventRow}>
                                <EventType type={e.eventType} className={styles.eventType} />
                                <p></p>
                            </div>
                        ))
                    ) : (
                        <div className={styles.eventRow}>
                            <p className={styles.eventType}>-</p>
                            <p>-</p>
                        </div>
                    )}
                </div>
            </div>,
            ROW_HEIGHT * Math.max(dayEvents.length, 1)
        )
    })

    return <div>{printPageBuilder.build()}</div>
}
