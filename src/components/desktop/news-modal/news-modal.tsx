import * as React from 'react'
import {useEffect, useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {Modal} from '../../common/modal/modal'
import {ModalModel} from '../../common/modal/modal-model'
import styles from './news-modal.module.css'
import {loadNews} from '../../../redux/cms/actions'
import {IAppState} from '../../../redux/root-reducer'
import {getDateDDMMMMYYYYFormat} from '../../../helpers/date'

interface INewsModal {
    onClose: () => void
}

export function NewsModal(props: INewsModal): JSX.Element {
    const {onClose} = props
    const news = useSelector((state: IAppState) => state.cms.news)
    const dispatch = useDispatch()
    const loadNewsAction = useCallback(() => dispatch(loadNews()), [dispatch])

    useEffect(() => {
        !news && loadNewsAction()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const model = ModalModel.Builder.withClassName(styles.modal)
        .withBody(
            <div className={styles.container}>
                <div className={styles.header}>Что нового?</div>
                <div className={styles.body}>
                    {news && news.length > 0
                        ? news.map(({id, title, publishedAt, content, images}) => {
                              return (
                                  <div key={id} className={styles.news}>
                                      <p className={styles.date}>{getDateDDMMMMYYYYFormat(new Date(publishedAt))} г.</p>
                                      <p className={styles.title}>{title}</p>
                                      {/* eslint-disable-next-line @typescript-eslint/naming-convention  */}
                                      <div className={styles.content} dangerouslySetInnerHTML={{__html: content}} />
                                      {images.length > 0 &&
                                          images.map(({id, url, alternativeText}) => (
                                              <img key={id} src={url} alt={alternativeText} className={styles.image} />
                                          ))}
                                  </div>
                              )
                          })
                        : 'Нет новостей'}
                </div>
            </div>
        )
        .withOnCloseFunc(onClose)
        .build()

    return <Modal model={model} isFullMode={true}></Modal>
}
