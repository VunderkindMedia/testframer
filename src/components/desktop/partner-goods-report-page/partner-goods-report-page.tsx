import * as React from 'react'
import './partner-goods-report-page.css'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useCallback, useState} from 'react'
import {loadPartnerGoods} from '../../../redux/orders/actions'
import {useEffect} from 'react'
import {getGoodsFilterParams, getOrdersFilter} from '../../../helpers/orders'
import {A4_HEIGHT, A4_PADDING} from '../../../constants/print-page'
import {PrintPageBuilder} from '../../../helpers/print-page-builder'
import {Good} from '../../../model/framer/good'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {Footer, FOOTER_HEIGHT} from '../report/footer/footer'

const AVAILABLE_HEIGHT = A4_HEIGHT - A4_PADDING * 2 - FOOTER_HEIGHT

export function PartnerGoodsReportPage(): JSX.Element | null {
    const partnerGoods = useSelector((state: IAppState) => state.orders.partnerGoods)
    const search = useSelector((state: IAppState) => state.router.location.search)

    const [goods, setGoods] = useState<Good[]>([])

    const dispatch = useDispatch()
    const loadOrdersWithPartnerGoodsAction = useCallback((filter: string) => dispatch(loadPartnerGoods(filter)), [dispatch])

    useEffect(() => {
        const params = getGoodsFilterParams(search)
        params.offset = 0
        params.limit = Number.MAX_SAFE_INTEGER

        let filter = getOrdersFilter(params)

        const goods = new URLSearchParams(search).get('goods')
        if (goods) {
            filter += `&goods=${goods}`
        }
        const t = new URLSearchParams(search).get('t')
        if (t) {
            filter += `&t=${t}`
        }

        loadOrdersWithPartnerGoodsAction(filter)
    }, [loadOrdersWithPartnerGoodsAction, search])

    useEffect(() => {
        if (goods.length > 0) {
            document.body.className = 'partner-goods-report-page__body printable'
        }

        return () => {
            document.body.className = ''
        }
    }, [goods])

    useEffect(() => {
        const params = new URLSearchParams(search)
        const goodsParam = params.get('goods')

        if (!goodsParam) {
            return
        }

        const goodsIds = goodsParam.split(',').map(id => +id)
        const goods = partnerGoods.filter(good => goodsIds.some(id => id === good.id))

        setGoods(goods)
    }, [search, partnerGoods])

    if (goods.length === 0) {
        return null
    }

    const createAtStr = new Intl.DateTimeFormat('ru', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric'
    }).format(new Date())

    const printPageBuilder = new PrintPageBuilder(AVAILABLE_HEIGHT, (pageIndex, pagesCount) => {
        return <Footer text={`Отчет по материалам от ${createAtStr}.`} pageIndex={pageIndex} pagesCount={pagesCount} />
    })

    printPageBuilder.addView(
        <div className="partner-goods-report-page__header">
            <p>Отчет по материалам от {createAtStr}</p>
            <p>{goods[0].order?.offerAddonLine1 ?? ''}</p>
        </div>,
        38
    )

    const tableHeader = (
        <div className="partner-goods-report-page__table-row partner-goods-report-page__table-row--header">
            <p>Дата создания</p>
            <p>Номер заказа</p>
            <p>Наименование материала</p>
            <p>Кол-во</p>
            <p>Параметры материала</p>
            <p>Дата отгрузки</p>
            <p>Дата монтажа</p>
        </div>
    )

    printPageBuilder.addView(tableHeader, 18)

    goods.forEach(good => {
        const {order} = good
        if (!order) {
            return
        }

        if (printPageBuilder.currentPageAvailableHeight < 28) {
            printPageBuilder.addView(tableHeader, 18)
        }

        printPageBuilder.addView(
            <div className="partner-goods-report-page__table-row">
                <p>{getDateDDMMYYYYFormat(new Date(order.createdAt))}</p>
                <div className="partner-goods-report-page__table-row__order-numbers">
                    <p>{order.orderNumber}</p>
                    <p>{order.windrawOrderNumber}</p>
                </div>
                <p>{good.name}</p>
                <p>{good.quantity}</p>
                <p>{good.attributes.map(a => a.visibleDescription).join(', ')}</p>
                {order.shippingDate.length > 0 ? (
                    <p>
                        <span style={{fontWeight: 500}}>{getDateDDMMYYYYFormat(new Date(order.shippingDate))}</span>
                    </p>
                ) : (
                    <p>
                        {order.estShippingDate.length === 0 ? <>&mdash;</> : <> ~ {getDateDDMMYYYYFormat(new Date(order.estShippingDate))}</>}
                    </p>
                )}
                <p>{order.mountingDate ? getDateDDMMYYYYFormat(new Date(order.mountingDate)) : <>&mdash;</>}</p>
            </div>,
            28
        )
    })

    return <div className="partner-goods-report-page">{printPageBuilder.build()}</div>
}
