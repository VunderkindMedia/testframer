import * as React from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {NAV_CLIENTS, NAV_ORDER, NAV_PARTNER_GOODS, NAV_PROFILE, NAV_MOUNTING, NAV_EVENTS} from '../../../constants/navigation'
import {usePermissions} from '../../../hooks/permissions'
import {Link} from 'react-router-dom'
import {profileTabs} from '../../common/profile-page/tabs'
import {SessionManager} from '../../../session-manager'
import {useOrdersPageLink, usePartnerGoodsPageLink, useMountingsPageLink, useEventsPageLink} from '../../../hooks/router'
import {ReactComponent as ProfileIcon} from '../../common/menu/resources/profile_icon.inline.svg'
import {ReactComponent as OrdersIcon} from '../../common/menu/resources/orders_icon.inline.svg'
import {ReactComponent as ClientsIcon} from '../../common/menu/resources/clients_icon.inline.svg'
import {ReactComponent as GoodsPageIcon} from '../../common/menu/resources/goods_page_icon.inline.svg'
import {ReactComponent as FaqIcon} from '../../common/menu/resources/faq_icon.inline.svg'
import {ReactComponent as MountingsIcon} from '../../common/menu/resources/mountings_icon.inline.svg'
import {ReactComponent as CalendarIcon} from '../../common/menu/resources/calendar_icon.inline.svg'
import {ReactComponent as MediaIcon} from '../../common/menu/resources/media_library_icon.inline.svg'
import {AnalyticsManager} from '../../../analytics-manager'
import {BuildTypes} from '../../../model/build-types'
import {FactoryTypes} from '../../../model/framer/factory-types'
import styles from './menu.module.css'

const PROFILE_DEFAULT_LINK = profileTabs.getFullPath(profileTabs.defaultTab)

export function Menu(): JSX.Element | null {
    const partner = useSelector((state: IAppState) => state.account.partner)
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const isVisibleChatButton = useSelector((state: IAppState) => state.chat.isVisibleButton)
    const isOpenedChat = useSelector((state: IAppState) => state.chat.isOpened)
    const url = useSelector((state: IAppState) => `${state.router.location.pathname}${state.router.location.search}`)
    const permission = usePermissions()
    const ordersPageLink = useOrdersPageLink()
    const partnerGoodsPageLink = usePartnerGoodsPageLink()
    const mountingsPageLink = useMountingsPageLink()
    const eventsPageLink = useEventsPageLink()

    return (
        <div className={styles.menu}>
            <div className={`${styles.menuItem} ${pathname.includes(NAV_PROFILE) && styles.active}`}>
                <Link className={styles.menuItemHeader} to={PROFILE_DEFAULT_LINK} data-test="profile-page">
                    <div className={styles.menuItemHeaderIcon}>
                        <ProfileIcon />
                    </div>
                    <p className={`${styles.menuItemHeaderTitle} ${url === PROFILE_DEFAULT_LINK && styles.active}`}>Профиль</p>
                </Link>
                <div className={styles.submenu}>
                    {profileTabs.get(permission.isNonQualifiedOrBossManager).map(({id, title}) => {
                        const tabUrl = profileTabs.getFullPath(id)
                        return (
                            <Link key={id} to={tabUrl} className={`${styles.submenuItem} ${url === tabUrl && styles.active}`}>
                                <p>{title}</p>
                            </Link>
                        )
                    })}
                    <div className={styles.submenuItem} onClick={() => SessionManager.resetSession()}>
                        <p>Выйти</p>
                    </div>
                </div>
            </div>
            <div className={`${styles.menuItem} ${pathname.includes(NAV_ORDER) && styles.active}`}>
                <Link className={styles.menuItemHeader} to={ordersPageLink} data-test="menu-nav-to-orders-page">
                    <div className={styles.menuItemHeaderIcon}>
                        <OrdersIcon />
                    </div>
                    <p className={styles.menuItemHeaderTitle}>Заказы</p>
                </Link>
            </div>
            <div className={`${styles.menuItem} ${pathname.includes(NAV_MOUNTING) && styles.active}`}>
                <Link className={styles.menuItemHeader} to={mountingsPageLink}>
                    <div className={styles.menuItemHeaderIcon}>
                        <MountingsIcon />
                    </div>
                    <p className={styles.menuItemHeaderTitle}>Монтажи</p>
                </Link>
            </div>
            <div className={`${styles.menuItem} ${pathname.includes(NAV_CLIENTS) && styles.active}`}>
                <Link className={styles.menuItemHeader} to={NAV_CLIENTS}>
                    <div className={styles.menuItemHeaderIcon}>
                        <ClientsIcon />
                    </div>
                    <p className={styles.menuItemHeaderTitle}>Клиенты</p>
                </Link>
            </div>
            <div className={`${styles.menuItem} ${pathname.includes(NAV_PARTNER_GOODS) && styles.active}`}>
                <Link className={styles.menuItemHeader} to={partnerGoodsPageLink}>
                    <div className={styles.menuItemHeaderIcon}>
                        <GoodsPageIcon />
                    </div>
                    <p className={styles.menuItemHeaderTitle}>Отчет по материалам</p>
                </Link>
            </div>
            <div className={styles.menuItem}>
                <a
                    className={styles.menuItemHeader}
                    onClick={() => {
                        AnalyticsManager.trackEvent({
                            name: 'Клик "Документация"',
                            target: 'documentation-link'
                        })
                    }}
                    href="https://docs.framer.ru/article/o-servise-framer"
                    target="_blank"
                    rel="noreferrer">
                    <div className={styles.menuItemHeaderIcon}>
                        <FaqIcon />
                    </div>
                    <p className={styles.menuItemHeaderTitle}>Документация</p>
                </a>
                <div className={styles.submenu}>
                    <a
                        href="https://docs.framer.ru/article/o-servise-framer"
                        target="_blank"
                        rel="noreferrer"
                        className={styles.submenuItem}
                        onClick={() => AnalyticsManager.trackClickInstructionLink()}>
                        <p>Инструкции</p>
                    </a>
                    <a
                        href="https://docs.framer.ru/faqs/faq-topic-1"
                        target="_blank"
                        rel="noreferrer"
                        className={styles.submenuItem}
                        onClick={() => AnalyticsManager.trackClickFaqLink()}>
                        <p>FAQ</p>
                    </a>
                    <a
                        href="mailto:info@framer.ru"
                        target="_blank"
                        rel="noopener noreferrer"
                        className={styles.submenuItem}
                        onClick={() => AnalyticsManager.trackEmailClick()}>
                        info@framer.ru
                    </a>
                </div>
            </div>
            {process.env.BUILD_TYPE !== BuildTypes.Prod && (
                <div className={`${styles.menuItem} ${styles.menuItemCalendar} ${pathname.includes(NAV_EVENTS) && styles.active}`}>
                    <Link className={styles.menuItemHeader} to={eventsPageLink}>
                        <div className={styles.menuItemHeaderIcon}>
                            <CalendarIcon />
                        </div>
                        <p className={styles.menuItemHeaderTitle}>Календарь</p>
                    </Link>
                </div>
            )}
            {partner?.factoryId && partner.factoryId !== FactoryTypes.Moscow && (
                <div className={styles.menuItem}>
                    <a
                        href="https://library.framer.ru"
                        className={styles.menuItemHeader}
                        target="_blank"
                        rel="noopener noreferrer"
                        onClick={() => AnalyticsManager.trackClickMedialibraryButton()}>
                        <div className={styles.menuItemHeaderIcon}>
                            <MediaIcon />
                        </div>
                        <p className={styles.menuItemHeaderTitle}>Медиабиблиотека</p>
                    </a>
                </div>
            )}
            {isVisibleChatButton && (
                <div
                    className={`${styles.menuItem} ${styles.menuItemChat} ${pathname.includes(NAV_CLIENTS) && styles.active} ${
                        isOpenedChat && styles.hidden
                    }`}
                    onClick={() => window.carrotquest?.open()}>
                    <div className={styles.menuItemHeader}>
                        <div className={styles.menuItemHeaderIcon} />
                        <div className={styles.menuItemHeaderTitle}>
                            <p>Задать вопрос</p>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}
