import * as React from 'react'
import {useState, useCallback, useRef, useEffect} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {BonusProgramModal} from '../bonus-program-modal/bonus-program-modal'
import {getBonusScore} from '../../../helpers/currency'
import {ModalOverlay} from '../../common/modal/modal-overlay/modal-overlay'
import {useDocumentClick} from '../../../hooks/ui'
import styles from './bonus-score.module.css'

interface IBonusScoreProps {
    className?: string
}

export const BonusScore = (props: IBonusScoreProps): JSX.Element => {
    const {className} = props
    const bonusScore = useSelector((state: IAppState) => state.account.bonusScore)
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const [isBonusModalOpened, setIsBonusModalOpened] = useState(false)
    const ref = useRef<HTMLDivElement | null>(null)

    const openBonusModal = useCallback((e: React.SyntheticEvent) => {
        e.stopPropagation()
        e.preventDefault()
        setIsBonusModalOpened(true)
    }, [])

    const closeModal = useCallback(
        (e: MouseEvent): void => {
            if (isBonusModalOpened && ref.current && !ref.current.contains(e.target as HTMLElement)) {
                setIsBonusModalOpened(false)
            }
        },
        [isBonusModalOpened, ref]
    )

    useDocumentClick(closeModal)

    useEffect(() => {
        setIsBonusModalOpened(false)
    }, [pathname])

    const {availableScore} = bonusScore
    return (
        <div className={`${styles.wrapper} ${className ?? ''}`}>
            <a href="" className={styles.bonus} onClick={openBonusModal}>
                {getBonusScore(availableScore)} Б
            </a>
            {isBonusModalOpened && <BonusProgramModal className={styles.bonusModal} ref={ref} />}
            {isBonusModalOpened && <ModalOverlay isTransparent={true} isFullMode={true} className={styles.overlay} />}
        </div>
    )
}
