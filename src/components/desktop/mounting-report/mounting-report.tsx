import * as React from 'react'
import {useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import styles from './mounting-report.module.css'
import {Footer, FOOTER_HEIGHT} from '../report/footer/footer'
import {IAppState} from '../../../redux/root-reducer'
import {loadOrder} from '../../../redux/orders/actions'
import {loadMounting} from '../../../redux/mountings/actions'
import {PrintPageBuilder} from '../../../helpers/print-page-builder'
import {A4_HEIGHT, A4_PADDING} from '../../../constants/print-page'
import {getDateDDMMYYYYFormat} from '../../../helpers/date'
import {getConstruction} from '../report/construction/construction'
import {Good} from '../../../model/framer/good'
import {Service} from '../../../model/framer/service'
import {Point} from '../../../model/geometry/point'
import {Connector} from '../../../model/framer/connector'

const AVAILABLE_HEIGHT = A4_HEIGHT - A4_PADDING * 2 - FOOTER_HEIGHT
const STANDARD_TABLE_ROW_HEIGHT = 20

export function MountingReport(): JSX.Element | null {
    const mountingId = useSelector((state: IAppState) => new URLSearchParams(state.router.location.search).get('id'))
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const mounting = useSelector((state: IAppState) => state.mountings.current)
    const browserSize = useSelector((state: IAppState) => state.browser.windowSize)

    const dispatch = useDispatch()

    useEffect(() => {
        mountingId && dispatch(loadMounting(+mountingId))
    }, [mountingId, dispatch])

    useEffect(() => {
        mounting && mounting.order.id && dispatch(loadOrder(mounting.order.id))
    }, [mounting, dispatch])

    useEffect(() => {
        if (currentOrder && mounting) {
            document.body.classList.add(styles.body, 'printable')
        }

        return () => {
            document.body.classList.remove(styles.body, 'printable')
        }
    }, [currentOrder, mounting])

    if (!currentOrder || !mounting) {
        return null
    }

    const {orderNumber, createdAt, totalSquare, totalWeight, goods, services} = currentOrder
    const {client, owner, note} = mounting
    const dateCreated = getDateDDMMYYYYFormat(new Date(createdAt))

    const printPageBuilder = new PrintPageBuilder(AVAILABLE_HEIGHT, (pageIndex, pagesCount) => {
        return <Footer text={`Заказ №: ${orderNumber} от ${dateCreated}.`} pageIndex={pageIndex} pagesCount={pagesCount} />
    })

    const constructionsCount = currentOrder.constructions.reduce((count, construction) => count + construction.quantity, 0)
    const productsCount = currentOrder.constructions.reduce(
        (count, construction) => count + construction.products.length * construction.quantity,
        0
    )

    const getGoodsList = (goods: Good[], title: string): JSX.Element => {
        return (
            <div className={styles.list}>
                <p className={styles.listTitle}>{title}</p>
                {goods.map(({id, quantity, name, attributes}) => (
                    <p key={id} className={styles.listItem}>
                        <span>
                            {quantity} x {name}
                        </span>
                        {attributes.map((attribute, index) => {
                            return (
                                <span key={`attr-${index}`} className={styles.itemDetails}>
                                    {attribute.caption}: <span>{attribute.visibleValue}</span>
                                </span>
                            )
                        })}
                    </p>
                ))}
            </div>
        )
    }

    const getServicesList = (services: Service[], title: string): JSX.Element => {
        return (
            <div className={styles.list}>
                <p className={styles.listTitle}>{title}</p>
                {services.map(({id, name}) => (
                    <p key={id} className={styles.listItem}>
                        {name}
                    </p>
                ))}
            </div>
        )
    }

    printPageBuilder.addView(
        <div className={styles.header}>
            <p className={styles.title}>
                Заказ №: <span>{orderNumber}</span> от {dateCreated}
            </p>
            <div className={styles.details}>
                <p>
                    Заказчик: <span>{client?.name}</span>
                </p>
                <p>
                    Тел.: <span>{client?.phone || '-'}</span>
                </p>
                <p>
                    Адрес: <span>{client?.address}</span>
                </p>
                <p>
                    Ответственный: <span>{owner?.name || owner?.email}</span>
                </p>
                <div className={styles.constructionDetails}>
                    <p>
                        Количество конструкций:{' '}
                        <span>
                            {constructionsCount} шт. (Изделий: {productsCount} шт.)
                        </span>
                    </p>
                    <p>
                        Общая площадь:{' '}
                        <span>
                            {totalSquare} м<sup>2</sup>
                        </span>
                    </p>
                    <p>
                        Общий вес: <span>{totalWeight} кг</span>
                    </p>
                </div>
            </div>
        </div>,
        83
    )

    if (note) {
        printPageBuilder.addView(
            <p className={styles.commentContainer}>
                <span className={styles.comment}>
                    Комментарий: <span>{note}</span>
                </span>
            </p>,
            38
        )
    }

    printPageBuilder.addView(<p className={styles.constructions}>Конструкции в заказе</p>, 40)

    currentOrder.sortedConstructions.forEach((construction, idx) => {
        const constructionViews = getConstruction(construction, `Конструкция ${construction.position}`, browserSize, true)
        constructionViews.map(({view, height}) => printPageBuilder.addView(view, height))

        const availableConnectors: {[key: string]: string} = {}
        construction.connectorsParameters.map(c => {
            availableConnectors[c.marking] = c.comment
        })

        const constructionGoods = goods.filter(({constructionId}) => constructionId === construction.id)
        const constructionServices = services.filter(({constructionId}) => constructionId === construction.id)
        const connectorsList: JSX.Element[] = []
        const groupedConnectors: {[key: string]: {[key: string]: Connector[]}} = {}
        construction.products.map(({connectors}) => {
            connectors.map(c => {
                const size: Point = new Point(c.end.x - c.start.x, c.end.y - c.start.y)
                const length = size.x !== 0 ? size.x : size.y

                if (!groupedConnectors[c.profileVendorCode]) {
                    groupedConnectors[c.profileVendorCode] = {}
                }
                if (!groupedConnectors[c.profileVendorCode][length]) {
                    groupedConnectors[c.profileVendorCode][length] = []
                }
                groupedConnectors[c.profileVendorCode][length].push(c)
            })
        })

        for (const code in groupedConnectors) {
            for (const length in groupedConnectors[code]) {
                const quantity = groupedConnectors[code][length].length

                connectorsList.push(
                    <p key={`${code}-${length}`} className={styles.listItem}>
                        <span>
                            {quantity} x {availableConnectors[code]}
                        </span>
                        <span className={styles.itemDetails}>Длина, мм: {length}</span>
                    </p>
                )
            }
        }

        const position = construction.position || idx + 1
        if (connectorsList.length > 0) {
            printPageBuilder.addView(
                <div className={styles.list}>
                    <p className={styles.listTitle}>Соединители к конструкции {position}</p>
                    {connectorsList}
                </div>,
                25 + connectorsList.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT
            )
        }

        if (constructionGoods.length > 0) {
            printPageBuilder.addView(
                getGoodsList(constructionGoods, `Дополнительные материалы к конструкции ${position}`),
                25 + constructionGoods.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT
            )
        }

        if (constructionServices.length > 0) {
            printPageBuilder.addView(
                getServicesList(constructionServices, `Услуги к конструкции ${position}`),
                25 + constructionServices.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT
            )
        }
    })

    const orderGoods = goods.filter(({constructionId}) => !constructionId)
    const orderServices = services.filter(({constructionId}) => !constructionId)

    if (orderGoods.length > 0) {
        printPageBuilder.addView(
            getGoodsList(orderGoods, 'ДОПОЛНИТЕЛЬНЫЕ МАТЕРИАЛЫ'),
            25 + orderGoods.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT
        )
    }

    if (orderServices.length > 0) {
        printPageBuilder.addView(
            getServicesList(orderServices, 'УСЛУГИ'),
            25 + orderServices.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT
        )
    }

    return <div className={styles.report}>{printPageBuilder.build()}</div>
}
