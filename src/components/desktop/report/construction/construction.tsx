import * as React from 'react'
import {Construction} from '../../../../model/framer/construction'
import {Size} from '../../../../model/size'
import {Lamination} from '../../../../model/framer/lamination'
import {FrameFilling} from '../../../../model/framer/frame-filling'
import {SvgConstructionDrawer} from '../../../common/svg-construction-drawer/svg-construction-drawer'
import {RUB_SYMBOL} from '../../../../constants/strings'
import {A4_HEIGHT, A4_PADDING, A4_WIDTH} from '../../../../constants/print-page'
import {
    INSIDE_FILLING_COLOR_ID,
    INSIDE_GALVANIZED_COLOR_ID,
    OUTSIDE_FILLING_COLOR_ID,
    OUTSIDE_GALVANIZED_COLOR_ID
} from '../../../../constants/default-user-parameters'
import {FOOTER_HEIGHT} from '../footer/footer'
import {getDimensionLinesOffsets} from '../../../../helpers/line'
import {PrintPageBuilder} from '../../../../helpers/print-page-builder'
import {getCurrency} from '../../../../helpers/currency'
import {getUniqItems} from '../../../../helpers/array'
import styles from './construction.module.css'
import {ShtulpOpenTypes} from '../../../../model/framer/shtulp-open-types'
import {ModelParts} from '../../../../model/framer/model-parts'
import {UserParameter} from '../../../../model/framer/user-parameter/user-parameter'

const STANDARD_VERTICAL_MARGIN = 20
const TITLE_HEIGHT_WITH_MARGIN2 = 21
const TITLE_HEIGHT_WITH_MARGIN = 29
const STANDARD_TABLE_ROW_HEIGHT = 20

const AVAILABLE_HEIGHT = A4_HEIGHT - A4_PADDING * 2 - FOOTER_HEIGHT
const AVAILABLE_WIDTH = A4_WIDTH - A4_PADDING * 2
const AVAILABLE_CONSTRUCTION_HEIGHT = AVAILABLE_HEIGHT / 2 - STANDARD_VERTICAL_MARGIN - TITLE_HEIGHT_WITH_MARGIN

const LINE_OFFSET = 18
const BRACKET_WIDTH = 12

const showFullWidth = (construction: Construction): boolean => {
    const {size} = construction.rect

    return size.width / size.height >= 2
}

const getFillingColors = (userParameters: UserParameter[]): string[] => {
    const colors: string[] = []
    userParameters.map(({id, value}) => {
        if (id === INSIDE_FILLING_COLOR_ID || id == INSIDE_GALVANIZED_COLOR_ID) {
            colors.push(value)
        }
        if (id === OUTSIDE_FILLING_COLOR_ID || id === OUTSIDE_GALVANIZED_COLOR_ID) {
            colors.unshift(value)
        }
    })
    return colors
}

const getConstructionDrawerSize = (construction: Construction): Size => {
    const width = PrintPageBuilder.ptToPx(showFullWidth(construction) ? AVAILABLE_WIDTH : AVAILABLE_WIDTH / 2 - 40)

    const dimensionLines = construction.getDimensionLines()
    const [verticalOffset, horizontalOffset] = getDimensionLinesOffsets(dimensionLines, LINE_OFFSET, BRACKET_WIDTH)

    const scale = (width - verticalOffset) / construction.width
    const height = construction.height * scale + horizontalOffset

    const availableHeight = PrintPageBuilder.ptToPx(AVAILABLE_CONSTRUCTION_HEIGHT)

    if (availableHeight < height) {
        const heightToFit = availableHeight - horizontalOffset
        const scaleToFit = heightToFit / construction.height
        const widthToFit = construction.width * scaleToFit + verticalOffset
        return new Size(PrintPageBuilder.pxToPt(widthToFit), AVAILABLE_CONSTRUCTION_HEIGHT)
    }

    return new Size(PrintPageBuilder.pxToPt(width), PrintPageBuilder.pxToPt(height))
}

interface IConstructionElement {
    construction: Construction
    title: string
    size: Size
    windowSize: Size
}

function ConstructionElement(props: IConstructionElement): JSX.Element {
    const {construction, title, size, windowSize} = props
    const {outsideLamination, insideLamination} = construction.products[0]

    return (
        <div className={styles.column}>
            <p className={styles.title}>{title}</p>
            <div style={{width: `${size.width}pt`, height: `${size.height}pt`}}>
                <SvgConstructionDrawer
                    currentConstruction={construction}
                    lineOffset={LINE_OFFSET}
                    fontSize={PrintPageBuilder.ptToPx(8)}
                    bracketWidth={BRACKET_WIDTH}
                    isReadonly={true}
                    lineWidth={2}
                    lineBoldWidth={4}
                    lamination={[outsideLamination, insideLamination]}
                    windowSize={windowSize}
                />
            </div>
        </div>
    )
}

const getInfoRows = (construction: Construction): JSX.Element[] => {
    const {outsideLamination, insideLamination} = construction.products[0]
    const leaf = construction.allFrameFillings.find(f => !f.isRoot)
    const framesWithMoskitka: FrameFilling[] = []
    construction.products.forEach(product => {
        framesWithMoskitka.push(...product.frame.allFrameFillings.filter(f => f.moskitka && f.moskitka.set))
    })

    const infoRows: JSX.Element[] = []

    if (construction.energyProductTypeName) {
        infoRows.push(
            <div key={`construction-${construction.id}-energy`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Комплектация: <span>{construction.energyProductTypeName}</span>
                </p>
            </div>
        )
    }

    infoRows.push(
        <div key={`construction-${construction.id}-size`} className={styles.textWrapper}>
            <p className={styles.text}>
                Размеры:{' '}
                <span>
                    {construction.rect.size.height} x {construction.rect.size.width} мм
                </span>
            </p>
        </div>
    )

    if (!construction.isEmpty && !construction.isMoskitka) {
        infoRows.push(
            <div key={`construction-${construction.id}-profile`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Профиль:{' '}
                    <span>
                        {construction.products[0].profile.displayName
                            ? construction.products[0].profile.displayName
                            : construction.products[0].profile.name}
                    </span>
                </p>
            </div>
        )
    }

    if (construction.isEmpty) {
        const filling = construction.products[0].frame.innerFillings[0].solid

        infoRows.push(
            <div key={`construction-${construction.id}-filling-group`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Тип: <span>{filling?.filling?.group}</span>
                </p>
            </div>,
            <div key={`construction-${construction.id}-filling-thickness`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Толщина: <span>{filling?.filling?.thickness} мм</span>
                </p>
            </div>,
            <div key={`construction-${construction.id}-filling-camernost`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Камерность: <span>{filling?.filling?.camernost}-камерные</span>
                </p>
            </div>
        )

        filling?.filling?.isMultifunctional &&
            infoRows.push(
                <div key={`construction-${construction.id}-filling-isMultifunctional`} className={styles.textWrapper}>
                    <p className={styles.text}>Мультифункционалльное</p>
                </div>,
                <div key={`construction-${construction.id}-filling-isMultifunctional`} className={styles.textWrapper}>
                    <p className={styles.text}>
                        Цвет: <span>{filling.glassColor}</span>
                    </p>
                </div>
            )

        filling?.filling?.isLowEmission &&
            infoRows.push(
                <div key={`construction-${construction.id}-filling-isLowEmission`} className={styles.textWrapper}>
                    <p className={styles.text}>Низкоэмиссионное</p>
                </div>
            )

        const colors = filling && getFillingColors(filling.userParameters)
        filling?.userParameters.length &&
            colors &&
            colors.length &&
            infoRows.push(
                <div key={`construction-${construction.id}-filling-color`} className={styles.textWrapper}>
                    <p className={styles.text}>
                        Цвет заполнения: <span>{getUniqItems(colors).join('/')}</span>
                    </p>
                </div>
            )
    }

    if (construction.isMoskitka) {
        infoRows.push(
            <div key={`construction-${construction.id}-moskitka-type`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Тип: <span>{construction.products[0].frame.innerFillings[0].leaf?.moskitka?.catproof ? 'Антикошка' : 'Обычная'}</span>
                </p>
            </div>
        )
    }

    if (!construction.isEmpty && !construction.isMoskitka) {
        infoRows.push(
            <div key={`construction-${construction.id}-furniture`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Фурнитура: <span>{construction.products[0].furniture.name}</span>
                </p>
            </div>
        )
    }

    if (!construction.isEmpty) {
        infoRows.push(
            <div key={`construction-${construction.id}-filling`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Заполнение:{' '}
                    <span>
                        {[
                            ...new Set(
                                construction.allSolidFillings.map(f => {
                                    const {fillingVendorCode, glassColor, filling, userParameters} = f
                                    const colors = getFillingColors(userParameters)

                                    return `${fillingVendorCode}${
                                        filling?.isMultifunctional
                                            ? `(${glassColor})`
                                            : colors.length
                                            ? ` [${getUniqItems(colors).join('/')}]`
                                            : ''
                                    }`
                                })
                            )
                        ].join(', ')}
                    </span>
                </p>
            </div>
        )
    }

    if (!construction.isEmpty && !construction.isMoskitka) {
        infoRows.push(
            <div key={`construction-${construction.id}-outsideLamination`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Ламинация снаружи:&nbsp;
                    <span>{outsideLamination.name === Lamination.getDefault().name ? '-' : outsideLamination.name}</span>
                </p>
            </div>,
            <div key={`construction-${construction.id}-insideLamination`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Ламинация внутри:&nbsp;
                    <span>{insideLamination.name === Lamination.getDefault().name ? '-' : insideLamination.name}</span>
                </p>
            </div>
        )
    }

    if (!construction.isEmpty && !construction.isMoskitka && leaf) {
        const entranceDoor = construction.products.find(p => p.isEntranceDoor)
        if (entranceDoor) {
            const porog = entranceDoor.frame.frameBeams.find(beam => beam.modelPart === ModelParts.Porog)
            const param = entranceDoor.profile.parts.find(part => part.marking === porog?.profileVendorCode)
            if (porog && param) {
                infoRows.push(
                    <div key={`product-${entranceDoor.productGuid}-porog-${porog.nodeId}`} className={styles.textWrapper}>
                        <p className={styles.text}>
                            Порог:&nbsp;
                            <span>
                                {param.comment} ({porog.profileVendorCode})
                            </span>
                        </p>
                    </div>
                )
            }

            const doorFrame = entranceDoor.frame.allFrameFillings.find(
                f => !f.isRoot && (f.shtulpOpenType === ShtulpOpenTypes.NoShtulp || f.shtulpOpenType === ShtulpOpenTypes.NoShtulpOnLeaf)
            )

            if (doorFrame?.userParameters.length !== 0) {
                doorFrame?.userParameters.forEach(param => {
                    let title = param.title
                    if (!title) {
                        const config = doorFrame.availableUserParameterConfigs?.find(config => config.id === param.id)
                        if (!config) {
                            return
                        }
                        title = config.name
                    }

                    infoRows.push(
                        <div key={`product-${entranceDoor.productGuid}-door-param-${param.id}`} className={styles.textWrapper}>
                            <p className={styles.text}>
                                {title}: <span>{param.value}</span>
                            </p>
                        </div>
                    )
                })
            }
        } else {
            infoRows.push(
                <div key={`construction-${construction.id}-handle`} className={styles.textWrapper}>
                    <p className={styles.text}>
                        Ручка: <span>{leaf.handleType}</span>
                    </p>
                    <p className={styles.text}>
                        Цвет: <span>{leaf.handleColor}</span>
                    </p>
                </div>
            )
        }
    }

    if (!construction.isEmpty && !construction.isMoskitka && framesWithMoskitka.length !== 0) {
        infoRows.push(
            <div key={`construction-${construction.id}-moskitkasCount`} className={styles.textWrapper}>
                <p className={styles.text}>
                    Москитные сетки: <span>{framesWithMoskitka.length} шт</span>
                </p>
            </div>
        )
    }

    infoRows.push(
        <div key={`construction-${construction.id}-weight`} className={styles.textWrapper}>
            <p className={styles.text}>
                Вес: <span>{construction.weight} кг</span>
            </p>
        </div>,
        <div key={`construction-${construction.id}-square`} className={styles.textWrapper}>
            <p className={styles.text}>
                Площадь:{' '}
                <span>
                    {construction.square} м<sup>2</sup>
                </span>
            </p>
        </div>
    )

    return infoRows
}

interface IInfoElement {
    construction: Construction
    rows: JSX.Element[]
    hidePrice?: boolean
}

const InfoElement = (props: IInfoElement): JSX.Element => {
    const {construction, rows, hidePrice = false} = props

    return (
        <div className={styles.column}>
            <p className={styles.title}>Характеристики конструкции</p>
            <div className={styles.info}>
                {rows}
                <div className={`${styles.textWrapper} ${styles.priceGroup}`}>
                    <p className={styles.text}>
                        Кол-во: <span>{construction.quantity} шт.</span>
                    </p>
                    {!hidePrice && (
                        <>
                            <p className={styles.text}>
                                Цена:{' '}
                                <span>
                                    {getCurrency(construction.totalCost / construction.quantity)} {RUB_SYMBOL}
                                </span>
                            </p>
                            <p className={styles.text}>
                                Сумма:{' '}
                                <span>
                                    {getCurrency(construction.totalCost)} {RUB_SYMBOL}
                                </span>
                            </p>
                        </>
                    )}
                </div>
            </div>
        </div>
    )
}

interface IConstruction {
    view: JSX.Element
    height: number
}

export const getConstruction = (construction: Construction, title: string, browserSize: Size, hidePrice?: boolean): IConstruction[] => {
    const size = getConstructionDrawerSize(construction)
    const infoRows = getInfoRows(construction)
    const constructionElement = <ConstructionElement construction={construction} title={title} size={size} windowSize={browserSize} />
    const infoElement = <InfoElement construction={construction} rows={infoRows} hidePrice={hidePrice} />

    if (showFullWidth(construction)) {
        return [
            {view: constructionElement, height: size.height + TITLE_HEIGHT_WITH_MARGIN2 + STANDARD_VERTICAL_MARGIN},
            {
                view: infoElement,
                height: Math.ceil(infoRows.length / 2) * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT + STANDARD_VERTICAL_MARGIN
            }
        ]
    } else {
        return [
            {
                view: (
                    <div className={styles.constructionInfo}>
                        {constructionElement}
                        {infoElement}
                    </div>
                ),
                height: Math.max(
                    size.height + TITLE_HEIGHT_WITH_MARGIN + STANDARD_VERTICAL_MARGIN,
                    infoRows.length * STANDARD_TABLE_ROW_HEIGHT + STANDARD_TABLE_ROW_HEIGHT + 38 + STANDARD_VERTICAL_MARGIN
                )
            }
        ]
    }
}
