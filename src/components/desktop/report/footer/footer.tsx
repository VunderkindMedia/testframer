import * as React from 'react'
import logo from '../../../../resources/images/logo_small.svg'
import {getDateDDMMYYYYFormat, getTimeHHMMFormat} from '../../../../helpers/date'
import styles from './footer.module.css'

interface IFooter {
    text?: string
    pageIndex: number
    pagesCount: number
}

export const FOOTER_HEIGHT = 50

export function Footer(props: IFooter): JSX.Element {
    const {text, pageIndex, pagesCount} = props

    const date = getDateDDMMYYYYFormat(new Date())
    const time = getTimeHHMMFormat(new Date())

    return (
        <div className={styles.footer}>
            <img src={logo} alt="" />
            <p>
                {text ? `${text} ` : ''}Отпечатано {date} в {time} Стр. {pageIndex + 1} из {pagesCount}
            </p>
        </div>
    )
}
