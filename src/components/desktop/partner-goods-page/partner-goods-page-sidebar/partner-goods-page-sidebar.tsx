import * as React from 'react'
import {useEffect} from 'react'
import styles from './partner-goods-page-sidebar.module.css'
import {OrderStates} from '../../../../model/framer/order-states'
import {IOrdersFilterParams} from '../../../../helpers/orders'
import {WithTitle} from '../../../common/with-title/with-title'
import {DateRangeSelector} from '../../../common/date-range-selector/date-range-selector'
import {SearchInput} from '../../../common/search-input/search-input'
import {NewSidebar} from '../../new-sidebar/new-sidebar'
import {NewSidebarItem} from '../../new-sidebar/new-sidebar-item/new-sidebar-item'
import {Select} from '../../../common/select/select'
import {DEFAULT_DEBOUNCE_TIME_MS} from '../../../../constants/settings'
import {arraysHaveSameItems} from '../../../../helpers/array'
import {STATES_ITEMS} from '../../../../constants/states-items'

interface IPartnerGoodsPageSidebarProps {
    filterParams: IOrdersFilterParams
    onChangeFilterPrams: (params: IOrdersFilterParams) => void
}

export function PartnerGoodsPageSidebar(props: IPartnerGoodsPageSidebarProps): JSX.Element {
    const {filterParams, onChangeFilterPrams} = props

    const {dateFrom, dateTo, states} = filterParams

    const selectedItem = STATES_ITEMS.find(i => arraysHaveSameItems(i.data, states, (s1, s2) => s1 === s2))

    useEffect(() => {
        if (!selectedItem) {
            onChangeFilterPrams({...filterParams, states: STATES_ITEMS[0].data})
        }
    }, [selectedItem, filterParams, onChangeFilterPrams])

    return (
        <NewSidebar headerTitle="Фильтр" className={styles.sidebar}>
            <NewSidebarItem className={styles.createdDate}>
                <WithTitle title="Дата монтажа">
                    <DateRangeSelector
                        className={styles.rangeSlider}
                        startDate={dateFrom}
                        endDate={dateTo}
                        onChange={(startDate, endDate) => onChangeFilterPrams({...filterParams, dateFrom: startDate, dateTo: endDate})}
                    />
                </WithTitle>
            </NewSidebarItem>
            <NewSidebarItem className={styles.name}>
                <WithTitle title="Номер">
                    <SearchInput
                        debounceTimeMs={DEFAULT_DEBOUNCE_TIME_MS}
                        value={filterParams.orderNumber}
                        placeholder="Поиск"
                        onChange={value => onChangeFilterPrams({...filterParams, orderNumber: value})}
                        onClear={() => onChangeFilterPrams({...filterParams, orderNumber: ''})}
                    />
                </WithTitle>
            </NewSidebarItem>
            <NewSidebarItem className={styles.states}>
                <WithTitle title="Статус">
                    <Select<OrderStates[]>
                        items={STATES_ITEMS}
                        selectedItem={selectedItem}
                        onSelect={item => onChangeFilterPrams({...filterParams, states: item.data})}
                    />
                </WithTitle>
            </NewSidebarItem>
            {/*<NewSidebarItem>*/}
            {/*    <WithTitle title="Дата отгрузки">*/}
            {/*        <DateRangeSelector*/}
            {/*            startDate={dateFrom}*/}
            {/*            endDate={dateTo}*/}
            {/*            onChange={(startDate, endDate) => onChangeFilterPrams({...filterParams, dateFrom: startDate, dateTo: endDate})}*/}
            {/*        />*/}
            {/*    </WithTitle>*/}
            {/*</NewSidebarItem>*/}
        </NewSidebar>
    )
}
