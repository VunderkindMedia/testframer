import * as React from 'react'
import {WithHint} from '../../../common/with-hint/with-hint'
import {getOrderStateName, getOrderStateColor} from '../../../../model/framer/order-states'
import {Good} from '../../../../model/framer/good'
import {HINTS} from '../../../../constants/hints'
import {Checkbox} from '../../../common/checkbox/checkbox'
import {Link} from 'react-router-dom'
import {getOrderLink} from '../../../../helpers/orders'
import {getDateDDMMYYYYFormat} from '../../../../helpers/date'
import styles from './partner-goods-table.module.css'

interface IPartnerGoodsTableProps {
    partnerGoods: Good[]
    selectedPartnerGoods: Good[]
    onSelectGoods: (good: Good[]) => void
    onDeselectGoods: (good: Good[]) => void
}

export function PartnerGoodsTable(props: IPartnerGoodsTableProps): JSX.Element | null {
    const {partnerGoods, selectedPartnerGoods, onSelectGoods, onDeselectGoods} = props

    if (partnerGoods.length === 0) {
        return null
    }

    return (
        <div className={`${styles.table} standard-table`}>
            <table>
                <thead>
                    <tr>
                        <th>
                            <Checkbox
                                isChecked={partnerGoods.every(g => selectedPartnerGoods.some(sg => sg.id === g.id))}
                                onChange={checked => (checked ? onSelectGoods(partnerGoods) : onDeselectGoods(partnerGoods))}
                            />
                        </th>
                        <th>Дата создания</th>
                        <th style={{whiteSpace: 'nowrap'}}>
                            <WithHint hint={HINTS.orderNumber}>{'Номер заказа'}</WithHint>
                        </th>
                        <th>Наименование материала</th>
                        <th style={{whiteSpace: 'nowrap'}}>Кол-во</th>
                        <th>Параметры материала</th>
                        <th>Статус заказа</th>
                        <th>Дата отгрузки</th>
                        <th>Дата монтажа</th>
                    </tr>
                </thead>
                <tbody>
                    {partnerGoods.map(good => {
                        const {order} = good

                        if (!order) {
                            return null
                        }

                        const isSelected = selectedPartnerGoods.some(g => g.id === good.id)

                        return (
                            <tr key={good.id} className={`${isSelected && 'selected'}`}>
                                <td>
                                    <Checkbox
                                        isChecked={isSelected}
                                        onChange={checked => (checked ? onSelectGoods([good]) : onDeselectGoods([good]))}
                                    />
                                </td>
                                <td>{getDateDDMMYYYYFormat(new Date(order.createdAt))}</td>
                                <td>
                                    <Link to={getOrderLink(order)} className={styles.orderNumbers}>
                                        <p>{order.orderNumber}</p>
                                        <p>{order.windrawOrderNumber}</p>
                                    </Link>
                                </td>
                                <td>
                                    <p>{good.name}</p>
                                </td>
                                <td>
                                    <p>{good.quantity}</p>
                                </td>
                                <td>
                                    <p>{good.attributes.map(a => a.visibleDescription).join(', ')}</p>
                                </td>
                                <td>
                                    <p className={order.state} style={{color: getOrderStateColor(order.state)}}>
                                        {getOrderStateName(order.state)}
                                    </p>
                                </td>
                                {order.shippingDate.length > 0 ? (
                                    <td style={{whiteSpace: 'nowrap'}}>
                                        <span style={{fontWeight: 500}}>{getDateDDMMYYYYFormat(new Date(order.shippingDate))}</span>
                                    </td>
                                ) : (
                                    <td style={{whiteSpace: 'nowrap'}}>
                                        {order.estShippingDate.length === 0 ? (
                                            <>&mdash;</>
                                        ) : (
                                            <> ~ {getDateDDMMYYYYFormat(new Date(order.estShippingDate))}</>
                                        )}
                                    </td>
                                )}
                                <td style={{whiteSpace: 'nowrap'}}>
                                    {order.mountingDate ? getDateDDMMYYYYFormat(new Date(order.mountingDate)) : <>&mdash;</>}
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
