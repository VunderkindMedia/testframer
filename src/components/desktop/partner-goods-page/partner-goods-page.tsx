import * as React from 'react'
import {useCallback, useRef} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {PartnerGoodsTable} from './partner-goods-table/partner-goods-table'
import {deselectGoods, loadPartnerGoodsReport, selectGoods} from '../../../redux/orders/actions'
import {PartnerGoodsPageSidebar} from './partner-goods-page-sidebar/partner-goods-page-sidebar'
import {NAV_PARTNER_GOODS} from '../../../constants/navigation'
import {Good} from '../../../model/framer/good'
import {getGoodsFilterParams, getOrdersFilter} from '../../../helpers/orders'
import {PageTitle} from '../../common/page-title/page-title'
import {HINTS} from '../../../constants/hints'
import {PaginationMenu} from '../../common/pagination-menu/pagination-menu'
import {usePartnerGoodsFromSearch, useSetPartnerGoodsFilterParams} from '../../../hooks/orders'
import {useGoTo} from '../../../hooks/router'
import {Button} from '../../common/button/button'
import {useAppContext} from '../../../hooks/app'
import {FilterButton} from '../../common/button/filter-button/filter-button'
import {TabletSidebar} from '../../tablet/tablet-sidebar/tablet-sidebar'
import {ButtonWithHint} from '../../common/button/button-with-hint/button-with-hint'
import {AnalyticsManager} from '../../../analytics-manager'
import styles from './partner-goods-page.module.css'

export function PartnerGoodsPage(): JSX.Element {
    const requestCount = useSelector((state: IAppState) => state.request.count)
    const partnerGoods = usePartnerGoodsFromSearch()
    const selectedPartnerGoods = useSelector((state: IAppState) => state.orders.selectedPartnerGoods)
    const search = useSelector((state: IAppState) => state.router.location.search)

    const tabletSidebarRef = useRef<TabletSidebar | null>(null)

    const dispatch = useDispatch()
    const selectGoodsAction = useCallback((goods: Good[]) => dispatch(selectGoods(goods)), [dispatch])
    const deselectGoodsAction = useCallback((goods: Good[]) => dispatch(deselectGoods(goods)), [dispatch])
    const loadPartnerGoodsReportAction = useCallback((filter: string) => dispatch(loadPartnerGoodsReport(filter)), [dispatch])
    const goTo = useGoTo()
    const setFilterParams = useSetPartnerGoodsFilterParams()
    const {isTablet} = useAppContext()

    const filterParams = getGoodsFilterParams(search)
    const {offset, limit} = filterParams

    return (
        <div className={styles.page}>
            <div className={styles.content}>
                <div className={styles.header}>
                    <PageTitle title="Отчет по материалам" className={styles.title} hint={HINTS.partnerGoodsReport} />
                    <ButtonWithHint
                        hint={selectedPartnerGoods.length === 0 ? 'Выделите галочками материалы которые необходимо распечатать' : ''}>
                        <Button
                            dataTest="partner-goods-page-download-report-button"
                            isDisabled={selectedPartnerGoods.length === 0}
                            buttonStyle="with-border"
                            onClick={() => {
                                loadPartnerGoodsReportAction(`${search}&goods=${selectedPartnerGoods.map(g => g.id).join(',')}`)
                                AnalyticsManager.trackEvent({
                                    name: 'Клик "Скачать отчет (по доп. материалам)"',
                                    target: 'download-partner-goods-report-button'
                                })
                            }}>
                            Скачать отчет
                        </Button>
                    </ButtonWithHint>
                    {isTablet && <FilterButton onClick={() => tabletSidebarRef.current?.show()} />}
                </div>
                <div className={styles.tableContainer}>
                    {partnerGoods.length > 0 ? (
                        <PartnerGoodsTable
                            partnerGoods={partnerGoods.slice(offset, offset + limit)}
                            selectedPartnerGoods={selectedPartnerGoods}
                            onSelectGoods={selectGoodsAction}
                            onDeselectGoods={deselectGoodsAction}
                        />
                    ) : (
                        <>{requestCount === 0 && <p>{HINTS.partnerGoodsNotFound}</p>}</>
                    )}
                </div>
                <PaginationMenu
                    currentPage={Math.ceil(offset / limit)}
                    itemsOnPage={limit}
                    totalCount={partnerGoods.length}
                    onSelect={page => goTo(`${NAV_PARTNER_GOODS}${getOrdersFilter({...filterParams, offset: limit * page})}`)}
                />
            </div>
            {isTablet ? (
                <TabletSidebar ref={tabletSidebarRef}>
                    <PartnerGoodsPageSidebar filterParams={filterParams} onChangeFilterPrams={setFilterParams} />
                </TabletSidebar>
            ) : (
                <PartnerGoodsPageSidebar filterParams={filterParams} onChangeFilterPrams={setFilterParams} />
            )}
        </div>
    )
}
