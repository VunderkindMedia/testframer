import * as React from 'react'
import styles from './clients-page.module.css'
import {ClientsPageSidebar} from './clients-page-sidebar/clients-page-sidebar'
import {PaginationMenu} from '../../common/pagination-menu/pagination-menu'
import {NAV_CLIENTS, NAV_CLIENT} from '../../../constants/navigation'
import {getClientsFilter, getClientsFilterParams} from '../../../helpers/clients'
import {PageTitle} from '../../common/page-title/page-title'
import {AddButton} from '../../common/button/add-button/add-button'
import {Button} from '../../common/button/button'
import {HINTS} from '../../../constants/hints'
import {getTextOrMdash} from '../../../helpers/text'
import {useSelector} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {useClientsFromSearch, useSetCurrentClient, useDeleteClientModal} from '../../../hooks/clients'
import {useGoTo} from '../../../hooks/router'
import {Modal} from '../../common/modal/modal'
import {useEffect} from 'react'

export function ClientsPage(): JSX.Element {
    const requestCount = useSelector((state: IAppState) => state.request.count)
    const clientsTotalCount = useSelector((state: IAppState) => {
        return state.clients.searchQuery ? state.clients.filtered.length : state.clients.totalCount
    })
    const currentClient = useSelector((state: IAppState) => state.clients.currentClient)
    const search = useSelector((state: IAppState) => state.router.location.search)
    const clients = useClientsFromSearch()
    const setCurrentClient = useSetCurrentClient()
    const [modalModel, showModal] = useDeleteClientModal()
    const goTo = useGoTo()

    const {offset, limit} = getClientsFilterParams(search)

    useEffect(() => {
        return () => {
            setCurrentClient(null)
        }
    }, [setCurrentClient])

    return (
        <div className={styles.page}>
            {modalModel && <Modal model={modalModel} />}
            <div className={styles.content}>
                <div className={styles.header}>
                    <PageTitle title="Клиенты" className={styles.title} />
                    <AddButton
                        className={styles.button}
                        title="Новый клиент"
                        onClick={() => {
                            setCurrentClient(null)
                            goTo(NAV_CLIENT)
                        }}
                    />
                    <Button
                        className={styles.button}
                        buttonStyle="with-border"
                        isDisabled={!currentClient}
                        onClick={() => currentClient && goTo(`${NAV_CLIENT}?clientId=${currentClient.id}`)}>
                        Редактировать
                    </Button>
                    <Button buttonStyle="with-border" isDisabled={!currentClient} onClick={() => currentClient && showModal(currentClient)}>
                        Удалить
                    </Button>
                </div>
                <div className={styles.table}>
                    {clients.length > 0 ? (
                        <div className="standard-table">
                            <table cellSpacing={0}>
                                <thead>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Телефон</th>
                                        <th>Эл. почта</th>
                                        <th>Адрес</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {clients.slice(offset, clients.length < offset + limit ? clients.length : offset + limit).map(client => (
                                        <tr
                                            key={client.id}
                                            className={`${currentClient?.id === client.id && 'selected'}`}
                                            onClick={() => setCurrentClient(currentClient?.id === client.id ? null : client)}>
                                            <td>{getTextOrMdash(client.name)}</td>
                                            <td>{getTextOrMdash(client.phone)}</td>
                                            <td>{getTextOrMdash(client.email)}</td>
                                            <td>{getTextOrMdash(client.address)}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    ) : (
                        <>{requestCount === 0 && <p>{HINTS.clientsNotFound}</p>}</>
                    )}
                </div>
                <PaginationMenu
                    currentPage={Math.ceil(offset / limit)}
                    itemsOnPage={limit}
                    totalCount={clientsTotalCount}
                    onSelect={page => goTo(`${NAV_CLIENTS}${getClientsFilter(limit * page, limit)}`)}
                />
            </div>
            <ClientsPageSidebar />
        </div>
    )
}
