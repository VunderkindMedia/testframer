import * as React from 'react'
import {useState, useEffect, useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {NewSidebar} from '../../new-sidebar/new-sidebar'
import {NewSidebarItem} from '../../new-sidebar/new-sidebar-item/new-sidebar-item'
import {WithTitle} from '../../../common/with-title/with-title'
import {SearchInput} from '../../../common/search-input/search-input'
import {NAV_CLIENTS} from '../../../../constants/navigation'
import {IAppState} from '../../../../redux/root-reducer'
import {useGoTo} from '../../../../hooks/router'
import {getClientsFilter} from '../../../../helpers/clients'
import {setClientSearchQuery} from '../../../../redux/clients/actions'
import styles from './clients-page-sidebar.module.css'

export function ClientsPageSidebar(): JSX.Element {
    const searchQuery = useSelector((state: IAppState) => state.clients.searchQuery)
    const dispatch = useDispatch()
    const clientsOnPage = useSelector((state: IAppState) => state.clients.clientsOnPage)
    const goTo = useGoTo()

    const [query, setQuery] = useState<string>('')

    useEffect(() => {
        setQuery(searchQuery)
    }, [searchQuery])

    const searchHandler = useCallback(
        (value: string) => {
            setQuery(value.trim())
            const filter = getClientsFilter(0, clientsOnPage)
            goTo(`${NAV_CLIENTS}${filter}`)
            dispatch(setClientSearchQuery(value))
        },
        [goTo, dispatch, clientsOnPage]
    )

    const searchInputHandler = useCallback(
        (value: string) => {
            searchHandler(value)
        },
        [searchHandler]
    )

    const clearHandler = useCallback(() => {
        searchHandler('')
    }, [searchHandler])

    return (
        <NewSidebar headerTitle="Фильтр" className={styles.sidebar}>
            <NewSidebarItem className={styles.name}>
                <WithTitle title="Имя">
                    <SearchInput value={query} placeholder="Поиск" onChange={searchInputHandler} onClear={clearHandler} />
                </WithTitle>
            </NewSidebarItem>
        </NewSidebar>
    )
}
