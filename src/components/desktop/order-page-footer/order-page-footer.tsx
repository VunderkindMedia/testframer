import * as React from 'react'
import styles from './order-page-footer.module.css'
import {Button} from '../../common/button/button'
import {ButtonWithHint} from '../../common/button/button-with-hint/button-with-hint'
import {useSelector, useDispatch} from 'react-redux'
import {IAppState} from '../../../redux/root-reducer'
import {RUB_SYMBOL} from '../../../constants/strings'
import {ReactComponent as CacheIcon} from '../../../resources/images/cache-icon.inline.svg'
import {ConfigContexts} from '../../../model/config-contexts'
import {useLoadOffer} from '../../../hooks/orders'
import {useSetConfigContext} from '../../../hooks/builder'
import {usePermissions} from '../../../hooks/permissions'
import {HINTS} from '../../../constants/hints'
import {OrderConstructionParams} from '../order-construction-params/order-construction-params'
import {setCurrentConstruction, setGenericProduct} from '../../../redux/orders/actions'
import {getCurrency} from '../../../helpers/currency'
import {getDateDDMonthFormat} from '../../../helpers/date'
import {AnalyticsManager} from '../../../analytics-manager'
import {SaveOrTransferToProductionButton} from '../../common/save-or-transfer-to-production-button/save-or-transfer-to-production-button'
import {BottomSheet} from '../../common/bottom-sheet/bottom-sheet'
import {ConstructionGoodsAndServicesSheets} from '../../common/construction-goods-and-services-sheets/construction-goods-and-services-sheets'

export function OrderPageFooter(): JSX.Element | null {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const isVisibleServiceCard = useSelector((state: IAppState) => state.builder.configContext === ConfigContexts.Services)
    const isVisibleGoodsCard = useSelector((state: IAppState) => state.builder.configContext === ConfigContexts.Goods)
    const isVisibleConstructionParams = useSelector((state: IAppState) => state.builder.configContext === ConfigContexts.ConstructionParams)
    const allFactoryGoods = useSelector((state: IAppState) => state.goods.factoryGoods)
    const allPartnerGoods = useSelector((state: IAppState) => state.goods.partnerGoods)
    const isRestrictedMode = useSelector((state: IAppState) => state.settings.isRestrictedMode)

    const loadOffer = useLoadOffer()
    const setConfigContext = useSetConfigContext()
    const permission = usePermissions()

    const dispatch = useDispatch()

    if (!currentOrder) {
        return null
    }
    const {estShippingDate, shippingDate} = currentOrder
    const date = shippingDate.length > 0 ? shippingDate : estShippingDate
    const isEstimated = shippingDate.length === 0

    const params = new URLSearchParams(search)
    const activeTab = params.get('activeTab')

    const constructionGoods = currentOrder.goods.filter(g => g.constructionId === currentConstruction?.id)

    const isVisibleServicesButton = true
    const isVisibleGoodsButton = constructionGoods.length > 0 || allFactoryGoods.length > 0 || allPartnerGoods.length > 0

    return (
        <>
            <BottomSheet
                isVisible={isVisibleConstructionParams}
                onHide={() => {
                    if (!isVisibleConstructionParams) {
                        return
                    }
                    dispatch(setCurrentConstruction(null))
                    dispatch(setGenericProduct(null))
                    setConfigContext(null)
                }}>
                <OrderConstructionParams onClickHeader={() => setConfigContext(null)} />
            </BottomSheet>
            <div className={styles.orderPageFooter}>
                <div className={styles.column} style={{flex: 1}}>
                    {!currentOrder.readonly &&
                        currentConstruction &&
                        activeTab === currentConstruction.id &&
                        (isVisibleServicesButton || isVisibleGoodsButton) && (
                            <div className={styles.constructionContextButtons} data-id="construction-addons-buttons">
                                <p>Добавить</p>
                                {isVisibleServicesButton && (
                                    <Button
                                        className={`${styles.additions} ${isVisibleServiceCard && styles.active}`}
                                        onClick={e => {
                                            e.stopPropagation()
                                            setConfigContext(isVisibleServiceCard ? null : ConfigContexts.Services)
                                        }}
                                        buttonStyle="with-border">
                                        Услуги
                                    </Button>
                                )}
                                {isVisibleGoodsButton && (
                                    <Button
                                        className={`${styles.additions} ${isVisibleGoodsCard && styles.active}`}
                                        onClick={e => {
                                            e.stopPropagation()
                                            setConfigContext(isVisibleGoodsCard ? null : ConfigContexts.Goods)
                                        }}
                                        buttonStyle="with-border">
                                        Доп. материалы
                                    </Button>
                                )}
                            </div>
                        )}
                </div>
                <div
                    className={styles.column}
                    style={{
                        flex: 1,
                        justifyContent: 'flex-end'
                    }}>
                    <p>
                        Готовность:&nbsp;
                        <span style={{fontWeight: 500}}>
                            {date.length === 0 ? (
                                <>&mdash;</>
                            ) : (
                                <>
                                    {isEstimated ? '~' : ''} {getDateDDMonthFormat(new Date(date))}
                                </>
                            )}
                        </span>
                    </p>
                    <p
                        style={{
                            margin: '0 20px'
                        }}>
                        Весь заказ:{' '}
                        <span style={{fontWeight: 'bold'}} data-test="order-page-order-total-cost">
                            {getCurrency(currentOrder.totalCost)} {currentOrder?.calculatedWithFramerPoints ? <CacheIcon /> : RUB_SYMBOL}
                        </span>
                    </p>
                </div>
                <div className={styles.column}>
                    {!isRestrictedMode && (
                        <ButtonWithHint hint={permission.isNonQualified ? HINTS.nonQualifiedRestriction : ''}>
                            <Button
                                dataTest="order-page-download-kp-button"
                                dataId="order-page-download-kp-button"
                                buttonStyle="with-border"
                                isDisabled={permission.isNonQualified}
                                className={styles.kpButton}
                                onClick={e => {
                                    e.stopPropagation()
                                    loadOffer(currentOrder)
                                    AnalyticsManager.trackClickDownloadKpButtonFromOrderOrBuilderPage()
                                    currentOrder.addAction(`Скачали КП в заказе ${currentOrder.id}`)
                                }}>
                                Скачать КП
                            </Button>
                        </ButtonWithHint>
                    )}
                    <SaveOrTransferToProductionButton />
                </div>
            </div>
            <ConstructionGoodsAndServicesSheets />
        </>
    )
}
