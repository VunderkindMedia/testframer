import * as React from 'react'

interface IOrderIconProps {
    fill: string
}

export function OrderIcon(props: IOrderIconProps): JSX.Element {
    const {fill} = props

    return (
        <svg fill="none" height="23" viewBox="0 0 28 23" width="28" xmlns="http://www.w3.org/2000/svg">
            <g fill={fill}>
                <path d="m0 0h2v23h-2z" />
                <path d="m11 0h2v23h-2z" />
                <path d="m13 0h2v13h-2z" transform="matrix(0 1 -1 0 13 -13)" />
                <path d="m28 0h2v13h-2z" transform="matrix(0 1 -1 0 28 -28)" />
                <path d="m28 9h2v13h-2z" transform="matrix(0 1 -1 0 37 -19)" />
                <path d="m17 11h2v11h-2z" transform="matrix(-1 0 0 -1 34 22)" />
                <path d="m28 11h2v11h-2z" transform="matrix(-1 0 0 -1 56 22)" />
                <path d="m28 17h2v13h-2z" transform="matrix(0 1 -1 0 45 -11)" />
                <path d="m28 13h2v13h-2z" transform="matrix(0 1 -1 0 41 -15)" />
                <path d="m28 21h2v13h-2z" transform="matrix(0 1 -1 0 49 -7)" />
                <path d="m13 21h2v13h-2z" transform="matrix(0 1 -1 0 34 8)" />
            </g>
        </svg>
    )
}
