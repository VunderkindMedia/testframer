import * as React from 'react'
import {useCallback, useState} from 'react'
import {CONSTRUCTION_CONFIGURATOR_HINT_SHOWED} from '../../../constants/local-storage'
import {Button} from '../../common/button/button'
import {NewBuilderPageSidebar} from '../new-builder-page-sidebar/new-builder-page-sidebar'
import styles from './construction-configurator-hint.module.css'

export function ConstructionConfiguratorHint(): JSX.Element | null {
    const [isShowed, setIsShowed] = useState(localStorage.getItem(CONSTRUCTION_CONFIGURATOR_HINT_SHOWED) === 'true')

    const hide = useCallback(() => {
        setIsShowed(true)
        localStorage.setItem(CONSTRUCTION_CONFIGURATOR_HINT_SHOWED, String(true))
    }, [])

    if (isShowed) {
        return null
    }

    return (
        <div className={styles.hint}>
            <p>
                Мы обновили построитель!
                <br />
                Теперь все параметры изделий указываются и редактируются в правой панели (а не вверху над изделием).
                <br />
                Нажимайте на параметры и изменяйте их.
            </p>
            <Button buttonStyle="with-border" onClick={hide}>
                Понятно
            </Button>
            <div className={styles.sidebar}>
                <NewBuilderPageSidebar />
            </div>
        </div>
    )
}
