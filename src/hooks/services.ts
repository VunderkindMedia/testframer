import {useDispatch, useSelector} from 'react-redux'
import {useCallback, useEffect} from 'react'
import {loadConstructionAvailableServices, loadInstallations, loadOrderAvailableServices, updateInstallation} from '../redux/services/actions'
import {Installation} from '../model/framer/installation'
import {IAppState} from '../redux/root-reducer'
import {ConstructionCost} from '../model/framer/construction-cost'

export const useLoadOrderAvailableServices = (): ((orderId: number) => void) => {
    const dispatch = useDispatch()
    return useCallback((orderId: number) => dispatch(loadOrderAvailableServices(orderId)), [dispatch])
}

export const useLoadConstructionAvailableServices = (): ((orderId: number, constructionId: string) => void) => {
    const dispatch = useDispatch()
    return useCallback(
        (orderId: number, constructionId: string) => dispatch(loadConstructionAvailableServices(orderId, constructionId)),
        [dispatch]
    )
}

export const useInstallation = (): Installation | null => {
    const installation = useSelector((state: IAppState) => state.services.installations[0])

    const dispatch = useDispatch()
    const loadInstallationsAction = useCallback(() => dispatch(loadInstallations()), [dispatch])

    useEffect(() => {
        loadInstallationsAction()
    }, [loadInstallationsAction])

    return installation ?? null
}

export const useUpdateInstallation = (): ((constructionCost: ConstructionCost) => void) => {
    const dispatch = useDispatch()

    return useCallback(
        (constructionCost: ConstructionCost) => {
            dispatch(updateInstallation(constructionCost))
        },
        [dispatch]
    )
}
