import {useCallback} from 'react'
import {goBack, push, replace} from 'connected-react-router'
import {useDispatch, useSelector} from 'react-redux'
import {
    getDefaultOrdersLink,
    getResetOrdersLink,
    getDefaultOrdersFilterParams,
    getDefaultPartnerGoodsLink,
    getResetPartnerGoodsLink,
    getDefaultGoodsFilterParams
} from '../helpers/orders'
import {getDefaultMountingsLink} from '../helpers/mountings'
import {getDefaultEventsLink} from '../helpers/events'
import {getDefaultPaymentsLink} from '../helpers/payments'
import {IAppState} from '../redux/root-reducer'
import {NAV_BOOKING, NAV_ORDER} from '../constants/navigation'

export const useGoTo = (): ((url: string) => void) => {
    const dispatch = useDispatch()
    return useCallback((url: string) => dispatch(push(url)), [dispatch])
}

export const useReplaceTo = (): ((url: string) => void) => {
    const dispatch = useDispatch()
    return useCallback((url: string) => dispatch(replace(url)), [dispatch])
}

export const useGoBack = (): (() => void) => {
    const dispatch = useDispatch()
    return useCallback(() => dispatch(goBack()), [dispatch])
}

export const useOrdersPageLink = (): string => {
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const ordersOnPage = useSelector((state: IAppState) => state.orders.ordersOnPage)

    let id: number | undefined

    if (currentOrder) {
        if (pathname === NAV_ORDER || pathname === NAV_BOOKING) {
            id = currentOrder.id
        }

        if (pathname === NAV_BOOKING) {
            const filterParams = getDefaultOrdersFilterParams(ordersOnPage)
            const isStatusChanged = !filterParams.states.some(state => state === currentOrder.state)

            if (isStatusChanged) {
                return `${getResetOrdersLink(ordersOnPage)}${id ? `&id=${id}` : ''}`
            }
        }
    }

    return `${getDefaultOrdersLink(ordersOnPage)}${id ? `&id=${id}` : ''}`
}

export const usePartnerGoodsPageLink = (): string => {
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const ordersOnPage = useSelector((state: IAppState) => state.orders.ordersOnPage)

    if (currentOrder && pathname === NAV_BOOKING) {
        const filterParams = getDefaultGoodsFilterParams(ordersOnPage)
        const isStatusChanged = !filterParams.states.some(state => state === currentOrder.state)

        if (isStatusChanged) {
            return getResetPartnerGoodsLink(ordersOnPage)
        }
    }

    return getDefaultPartnerGoodsLink(ordersOnPage)
}

export const useMountingsPageLink = (): string => {
    const mountingsOnPage = useSelector((state: IAppState) => state.mountings.mountingsOnPage)

    return getDefaultMountingsLink(mountingsOnPage)
}

export const useEventsPageLink = (): string => {
    return getDefaultEventsLink()
}

export const usePaymentsPageLink = (): string => {
    const paymentsOnPage = useSelector((state: IAppState) => state.payments.paymentsOnPage)

    return getDefaultPaymentsLink(paymentsOnPage)
}
