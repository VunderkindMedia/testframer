import {useEffect} from 'react'
import {useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {useShowOnboarding} from './app'

export const useDocumentClick = (onClick: {(e: MouseEvent): void}): void => {
    useEffect(() => {
        document.addEventListener('click', onClick)
        return () => {
            document.removeEventListener('click', onClick)
        }
    }, [onClick])
}

export const useModalOpen = (isOpen: boolean): void => {
    useEffect(() => {
        document.body.classList.toggle('with-modal', isOpen)
        return () => {
            document.body.classList.remove('with-modal')
        }
    }, [isOpen])
}

export const useResetScrollOnPathnameChange = (): void => {
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const isOnboardingShown = useShowOnboarding()

    useEffect(() => {
        !isOnboardingShown && window.scroll(0, 0)
    }, [pathname, isOnboardingShown])
}

export const useResetScrollOnSearchChange = (): void => {
    const search = useSelector((state: IAppState) => state.router.location.search)

    useEffect(() => {
        window.scroll(0, 0)
    }, [search])
}
