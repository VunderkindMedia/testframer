import {Client} from '../model/framer/client'
import {useDispatch, useSelector} from 'react-redux'
import * as React from 'react'
import {useCallback, useEffect, useRef, useState} from 'react'
import {
    addGood2,
    loadOrder,
    loadOrders,
    setClientToOrder,
    setCurrentConstructionAsync,
    setCurrentOrder,
    recalculateOrder,
    updateOrderDiscount,
    loadOffer,
    setCurrentConstruction,
    saveOrder,
    setCurrentProduct,
    deleteOrder,
    loadPartnerGoods,
    updateCurrentOrder,
    duplicateOrder,
    loadOrderXml,
    cancelOrder,
    loadPassport
} from '../redux/orders/actions'
import {Order} from '../model/framer/order'
import {LAST_EDITED_ORDER_ID, SHOW_REAL_COST} from '../constants/local-storage'
import {Construction} from '../model/framer/construction'
import {Good} from '../model/framer/good'
import {Discount} from '../model/framer/discount'
import {Product} from '../model/framer/product'
import {IAppState} from '../redux/root-reducer'
import {
    getGoodsFilterParams,
    getOrderPageUrl,
    getOrdersFilter,
    getOrdersFilterParams,
    IOrdersFilterParams,
    getConstructionPosition,
    isNewOrderAvailable,
    actualizeUserParameters
} from '../helpers/orders'
import {getGuidByContext} from '../helpers/builder'
import {NAV_ORDERS, NAV_ORDER, NAV_PARTNER_GOODS, ORDER_TAB_ID} from '../constants/navigation'
import {useGoTo, useReplaceTo} from './router'
import {CoordinateSystems} from '../model/coordinate-systems'
import {useSetCurrentContext} from './builder'
import {resetTemplateSelector} from '../redux/template-selector/actions'
import {HistoryManager} from '../history-manager'
import {getTabs} from '../helpers/tabs'
import {ORDER_FILTERS, GOODS_FILTERS} from '../constants/local-storage'
import {useIsMainDealer, useIsDemoUser} from './permissions'
import {useLoadOrderAvailableServices} from './services'
import {ITab} from '../components/desktop/tabs-panel/tabs-panel'
import {OrderIcon} from '../components/desktop/order-icon/order-icon'
import {Colors} from '../constants/colors'
import {SvgIconConstructionDrawer} from '../components/common/svg-icon-construction-drawer/svg-icon-construction-drawer'
import {ConstructionPreview} from '../components/common/construction-preview/construction-preview'
import {ErrorTypes} from '../model/framer/error-types'
import {Handle} from '../model/handle'
import {IContext} from '../model/i-context'
import {FrameFilling} from '../model/framer/frame-filling'
import {ModalModel} from '../components/common/modal/modal-model'
import {Action} from '../model/action'
import {UserParameterOption} from '../model/framer/user-parameter/user-parameter-option'
import {SolidFilling} from '../model/framer/solid-filling'
import {AnalyticsManager} from '../analytics-manager'
import {ConfigContexts} from '../model/config-contexts'

export const useLoadOrders = (): ((filter: string) => void) => {
    const dispatch = useDispatch()
    return useCallback((filter: string) => dispatch(loadOrders(filter)), [dispatch])
}

export const useLoadOrder = (): ((orderId: number) => void) => {
    const dispatch = useDispatch()
    return useCallback((orderId: number) => dispatch(loadOrder(orderId)), [dispatch])
}

export const useRecalculateOrder = (): ((order: Order, cb?: () => void) => void) => {
    const dispatch = useDispatch()
    return useCallback((order: Order, cb?: () => void) => dispatch(recalculateOrder(order, cb)), [dispatch])
}

export const useSaveOrder = (): ((order: Order) => void) => {
    const dispatch = useDispatch()
    return useCallback((order: Order) => dispatch(saveOrder(order)), [dispatch])
}

export const useDeleteOrder = (): ((orderId: number) => void) => {
    const dispatch = useDispatch()
    return useCallback((orderId: number) => dispatch(deleteOrder(orderId)), [dispatch])
}

export const useSetCurrentOrder = (): ((order: Order | null) => void) => {
    const dispatch = useDispatch()
    return useCallback((order: Order | null) => dispatch(setCurrentOrder(order)), [dispatch])
}

export const useSetCurrentConstruction = (): ((construction: Construction) => void) => {
    const dispatch = useDispatch()
    return useCallback((construction: Construction) => dispatch(setCurrentConstruction(construction)), [dispatch])
}

export const useSetCurrentConstructionAsync = (): ((construction: Construction) => void) => {
    const dispatch = useDispatch()
    return useCallback((construction: Construction) => dispatch(setCurrentConstructionAsync(construction)), [dispatch])
}

export const useSetCurrentProduct = (): ((product: Product) => void) => {
    const dispatch = useDispatch()
    return useCallback((product: Product) => dispatch(setCurrentProduct(product)), [dispatch])
}

export const useSetClientToOrder = (): ((order: Order, client: Client) => void) => {
    const dispatch = useDispatch()
    return useCallback(
        (order: Order, client: Client) => {
            dispatch(setClientToOrder(order, client))
        },
        [dispatch]
    )
}

export const useShowRealCost = (): [boolean, (s: boolean) => void] => {
    const [shouldShowRealCost, setShouldShowRealCost] = useState(localStorage.getItem(SHOW_REAL_COST) === 'true')

    useEffect(() => {
        localStorage.setItem(SHOW_REAL_COST, JSON.stringify(shouldShowRealCost))
    }, [shouldShowRealCost])

    return [shouldShowRealCost, setShouldShowRealCost]
}

export const useAddGood2 = (): ((order: Order, good: Good) => void) => {
    const dispatch = useDispatch()
    return useCallback((order: Order, good: Good) => dispatch(addGood2(order, good)), [dispatch])
}

export const useUpdateOrderDiscount = (): ((order: Order, discount: Discount) => void) => {
    const dispatch = useDispatch()
    return useCallback(
        (order, discount) => {
            dispatch(updateOrderDiscount(order, discount))
        },
        [dispatch]
    )
}

export const useLoadOffer = (): ((order: Order) => void) => {
    const dispatch = useDispatch()
    const checkUserParameters = useCheckOrderUserParameters()

    return useCallback(
        (order: Order) => {
            checkUserParameters(() => {
                dispatch(loadOffer(order))
            })
        },
        [dispatch, checkUserParameters]
    )
}

export const useLoadPassport = (): ((order: Order) => void) => {
    const dispatch = useDispatch()
    return useCallback((order: Order) => dispatch(loadPassport(order)), [dispatch])
}

export const useLoadOrderXml = (): ((order: Order) => void) => {
    const dispatch = useDispatch()
    return useCallback((order: Order) => dispatch(loadOrderXml(order)), [dispatch])
}

export const useOrder = (orderId: number): Order | null => {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const loadOrder = useLoadOrder()
    const setCurrentOrder = useSetCurrentOrder()

    useEffect(() => {
        loadOrder(orderId)

        return () => {
            setCurrentOrder(null)
        }
    }, [orderId, loadOrder, setCurrentOrder])

    return currentOrder
}

export const useOrdersFromSearch = (): Order[] => {
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const search = useSelector((state: IAppState) => state.router.location.search)
    const orders = useSelector((state: IAppState) => state.orders.all)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const ordersOnPage = useSelector((state: IAppState) => state.orders.ordersOnPage)
    const replaceTo = useReplaceTo()

    const basePathname = useRef(pathname)

    const loadOrders = useLoadOrders()

    useEffect(() => {
        loadOrders(search)
    }, [search, loadOrders])

    useEffect(() => {
        if (basePathname.current !== pathname || !currentOrder) {
            return
        }

        const filterParams = getOrdersFilterParams(search)
        const {offset} = filterParams
        const orderOffset = orders.indexOf(currentOrder)
        let currentOffset = offset
        if (orderOffset !== -1 && orderOffset >= offset + ordersOnPage) {
            currentOffset = offset + orderOffset
        }

        const params = {
            ...filterParams,
            offset: currentOffset,
            limit: ordersOnPage
        }
        const filter = getOrdersFilter(params)
        if (search !== filter) {
            replaceTo(`${NAV_ORDERS}${filter}`)
        }
    }, [currentOrder, orders, search, ordersOnPage, replaceTo, pathname])

    return orders
}

export const usePartnerGoodsFromSearch = (): Good[] => {
    const partnerGoods = useSelector((state: IAppState) => state.orders.partnerGoods)
    const ordersOnPage = useSelector((state: IAppState) => state.orders.ordersOnPage)
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)
    const search = useSelector((state: IAppState) => state.router.location.search)
    const replaceTo = useReplaceTo()

    const [filter, setFilter] = useState(search)

    const basePathname = useRef(pathname)

    const dispatch = useDispatch()
    const loadOrdersWithPartnerGoodsAction = useCallback((filter: string) => dispatch(loadPartnerGoods(filter)), [dispatch])

    useEffect(() => {
        const oldParams = getGoodsFilterParams(filter)
        const params = getGoodsFilterParams(search)

        oldParams.offset = params.offset
        oldParams.limit = params.limit

        if (getOrdersFilter(oldParams) === search) {
            return
        }

        setFilter(search)
    }, [search, filter])

    useEffect(() => {
        const params = getGoodsFilterParams(filter)
        params.offset = 0
        params.limit = Number.MAX_SAFE_INTEGER

        loadOrdersWithPartnerGoodsAction(getOrdersFilter(params))
    }, [loadOrdersWithPartnerGoodsAction, filter])

    useEffect(() => {
        if (basePathname.current !== pathname) {
            return
        }
        const oldParams = getGoodsFilterParams(search)

        const params = {
            ...oldParams,
            offset: oldParams.offset,
            limit: ordersOnPage
        }
        const filter = getOrdersFilter(params)
        replaceTo(`${NAV_PARTNER_GOODS}${filter}`)
    }, [search, ordersOnPage, replaceTo, pathname])

    return partnerGoods
}

export const useUpdateCurrentOrder = (): ((order: Order) => Order) => {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)

    const dispatch = useDispatch()
    const setCurrentContext = useSetCurrentContext()

    return useCallback(
        (order: Order) => {
            const pastOrder = currentOrder ? currentOrder.clone : null
            const pastContext =
                pastOrder && currentContext ? pastOrder.findConstructionAndProductAndNodeByNodeId(currentContext.nodeId).node : null

            currentConstruction?.id &&
                HistoryManager.addPast({
                    currentConstruction: pastOrder?.constructions?.find(c => c.id === currentConstruction?.id) ?? null,
                    currentContext: pastContext
                })

            const updatedOrder = order.clone
            updatedOrder.changed = true

            dispatch(updateCurrentOrder(updatedOrder))

            const construction = updatedOrder.constructions.find(c => c.id === currentConstruction?.id)

            if (construction) {
                const replacer = (key: string, value: JSON): JSON | undefined => {
                    if (
                        key.includes('_') ||
                        (construction.products.length === 1 && key.includes('productGuid')) ||
                        (construction.products.length === 1 && key.includes('beamGuid')) ||
                        key.includes('profile') ||
                        key.includes('furniture') ||
                        key.includes('outsideLamination') ||
                        key.includes('insideLamination') ||
                        key.includes('moskitka') ||
                        key.includes('handlePositionType') ||
                        key.includes('handleType') ||
                        key.includes('handleColor') ||
                        key.includes('profileSystemName') ||
                        key.includes('hardwareSystemName')
                    ) {
                        return undefined
                    } else {
                        return value
                    }
                }

                console.groupCollapsed('updateCurrentOrder')

                const clone = construction.clone
                clone.toCoordinateSystem(CoordinateSystems.Relative)
                console.log(JSON.stringify(clone.products, replacer))

                console.groupEnd()
            }

            const updatedConstruction = updatedOrder.constructions?.find(c => c.id === currentConstruction?.id) ?? null
            let updatedContext: IContext | null = null
            if (currentContext instanceof Handle) {
                const node =
                    updatedOrder && currentContext
                        ? updatedOrder.findConstructionAndProductAndNodeByNodeId<FrameFilling>(currentContext.parentNodeId ?? '').node
                        : null
                if (node) {
                    updatedContext = new Handle(node)
                }
            } else {
                updatedContext =
                    updatedOrder && currentContext ? updatedOrder.findConstructionAndProductAndNodeByNodeId(currentContext.nodeId).node : null
            }

            currentConstruction?.id &&
                HistoryManager.setPresent({
                    currentConstruction: updatedConstruction,
                    currentContext: updatedContext
                })

            updatedContext && setCurrentContext(updatedContext, updatedConstruction)

            return updatedOrder
        },
        [dispatch, currentOrder, currentConstruction?.id, currentContext, setCurrentContext]
    )
}

export const useAddNewConstruction = (): ((construction: Construction) => void) => {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const search = useSelector((state: IAppState) => state.router.location.search)

    const dispatch = useDispatch()
    const updateCurrentOrder = useUpdateCurrentOrder()
    const goTo = useGoTo()

    return useCallback(
        (construction: Construction) => {
            if (!currentOrder) {
                return
            }

            const order = currentOrder.clone
            order.constructions.push(construction)
            order.addAction('Добавили конструкцию', construction)
            updateCurrentOrder(order)
            goTo(getOrderPageUrl(order.id, getTabs(search), construction.id))
            dispatch(resetTemplateSelector())
        },
        [dispatch, updateCurrentOrder, goTo, currentOrder, search]
    )
}

export const useSetOrderFilterParams = (): ((params: IOrdersFilterParams) => void) => {
    const goTo = useGoTo()

    return useCallback(
        (params: IOrdersFilterParams) => {
            localStorage.setItem(ORDER_FILTERS, JSON.stringify(params))
            goTo(`${NAV_ORDERS}${getOrdersFilter({...params, id: undefined, offset: 0})}`)
        },
        [goTo]
    )
}

export const useSetPartnerGoodsFilterParams = (): ((params: IOrdersFilterParams) => void) => {
    const goTo = useGoTo()

    return useCallback(
        (params: IOrdersFilterParams) => {
            localStorage.setItem(GOODS_FILTERS, JSON.stringify(params))
            goTo(`${NAV_PARTNER_GOODS}${getOrdersFilter({...params, offset: 0})}`)
        },
        [goTo]
    )
}

export const useIsSubdealerOrder = (): ((order: Order) => boolean) => {
    const partner = useSelector((state: IAppState) => state.account.partner)
    const isMainDealer = useIsMainDealer()

    return useCallback(
        (order: Order) => {
            if (!partner) {
                return false
            }

            const {id} = partner

            return isMainDealer && order.owner?.id !== id
        },
        [partner, isMainDealer]
    )
}

export const useDuplicateOrder = (): ((orderId: number) => void) => {
    const dispatch = useDispatch()
    return useCallback((orderId: number) => dispatch(duplicateOrder(orderId)), [dispatch])
}

export const useListenOrderPageNavigation = (): void => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)

    const loadOrder = useLoadOrder()
    const setCurrentOrder = useSetCurrentOrder()
    const loadOrderAvailableServices = useLoadOrderAvailableServices()

    const pageParams = new URLSearchParams(search)
    const orderIdStr = pageParams.get('orderId')

    useEffect(() => {
        if (orderIdStr && orderIdStr !== '0') {
            const orderId = +orderIdStr
            if (orderId !== currentOrder?.id) {
                loadOrder(orderId)
            }
            localStorage.setItem(LAST_EDITED_ORDER_ID, orderId.toString())
            loadOrderAvailableServices(orderId)
        } else {
            setCurrentOrder(null)
        }
    }, [orderIdStr, loadOrder, loadOrderAvailableServices, currentOrder?.id, setCurrentOrder])
}

export const useOrderTabs = (maxIconWidth: number, maxIconHeight: number, isAutoWidth = false): ITab[] => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const setCurrentConstructionAsync = useSetCurrentConstructionAsync()
    const [tabs, setTabs] = useState<ITab[]>([])

    useEffect(() => {
        if (!currentOrder) {
            return
        }
        const pageParams = new URLSearchParams(search)
        const tabsStr = pageParams.get('tabs') ?? ''
        const tabs = tabsStr.split(',').filter(t => t.length !== 0)
        const activeTab = pageParams.get('activeTab') ?? 'order'

        const newTabs: ITab[] = [
            {
                id: ORDER_TAB_ID,
                dataTest: `${ORDER_TAB_ID} ${currentOrder.orderNumber}`,
                title: `Заказ № ${currentOrder.fullOrderNumber}`,
                active: activeTab === ORDER_TAB_ID,
                icon: <OrderIcon fill={Colors.Gray} />,
                activeIcon: <OrderIcon fill={Colors.Blue} />
            }
        ]

        tabs.forEach(tab => {
            const construction = currentOrder.constructions.find(c => c.id === tab)
            if (!construction) {
                return
            }

            const errors = construction.errors.filter(error => error.errorType === ErrorTypes.Error)

            newTabs.push({
                id: construction.id,
                dataTest: `construction ${construction.id}`,
                title: `Конструкция ${getConstructionPosition(construction, currentOrder.constructions ?? [])}`,
                active: construction.id === activeTab,
                icon: (
                    <ConstructionPreview quantity={construction.quantity} isAutoWidth={isAutoWidth}>
                        <SvgIconConstructionDrawer
                            construction={construction}
                            maxWidth={maxIconWidth}
                            maxHeight={maxIconHeight}
                            lineColor={errors.length ? Colors.Red : Colors.Gray}
                        />
                    </ConstructionPreview>
                ),
                activeIcon: (
                    <ConstructionPreview quantity={construction.quantity} isAutoWidth={isAutoWidth}>
                        <SvgIconConstructionDrawer
                            construction={construction}
                            maxWidth={maxIconWidth}
                            maxHeight={maxIconHeight}
                            lineColor={errors.length ? Colors.Red : Colors.Blue}
                        />
                    </ConstructionPreview>
                ),
                closable: true
            })
        })

        if (activeTab !== ORDER_TAB_ID) {
            const construction = currentOrder.constructions.find(c => c.id === activeTab)
            if (construction) {
                setCurrentConstructionAsync(construction)
            }
        }

        setTabs(newTabs)
    }, [
        currentOrder,
        currentConstruction,
        search,
        setCurrentConstructionAsync,
        currentOrder?.fullOrderNumber,
        isAutoWidth,
        maxIconHeight,
        maxIconWidth
    ])

    return tabs
}

export const useRemoveOrderModal = (order: Order | null): [ModalModel | null, () => void] => {
    const [modalModel, setModalModel] = useState<ModalModel | null>(null)

    const deleteOrder = useDeleteOrder()

    const showModal = useCallback(() => {
        if (!order) {
            return
        }
        setModalModel(
            ModalModel.Builder.withTitle(`Подтверждение удаления расчета ${order.orderNumber}`)
                .withBody(<p>Удалить расчет {order.orderNumber}?</p>)
                .withPositiveAction(
                    new Action(
                        'Удалить',
                        () => {
                            deleteOrder(order.id)
                            setModalModel(null)
                        },
                        'submit-order-remove'
                    )
                )
                .withNegativeAction(new Action('Отменить', () => setModalModel(null)))
                .build()
        )
    }, [order, deleteOrder])

    return [modalModel, showModal]
}

export const useUserParametersContext = (): IContext | null => {
    const currentContext = useSelector((state: IAppState) => {
        if (state.orders.currentConstruction?.products && state.orders.currentConstruction?.products.length > 1) {
            return state.builder.currentContext
        }
        if (state.orders.currentProduct?.isEmpty) {
            return state.orders.currentProduct.frame.innerFillings[0].solid
        }
        return state.builder.currentContext
    })
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const hasMultipleProducts = useSelector(
        (state: IAppState) => state.orders.currentConstruction?.products && state.orders.currentConstruction.products.length > 1
    )

    if (currentContext) {
        if (currentContext instanceof FrameFilling) {
            return currentContext.isRoot ? currentProduct : currentContext
        }

        return currentContext
    }

    if (currentProduct) {
        return hasMultipleProducts ? null : currentProduct
    }

    return null
}

export const useUpdateUserParameter = (): ((data: UserParameterOption, nameTitle: string | undefined) => void) => {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const colorParameters = useSelector((state: IAppState) => state.builder.userColorParameters)
    const currentUserParameterConfigs = useSelector((state: IAppState) => state.builder.userParameterConfigsMap)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)

    const updateCurrentOrder = useUpdateCurrentOrder()
    const userParametersContext = useUserParametersContext()

    return useCallback(
        (data: UserParameterOption, nameTitle: string | undefined) => {
            if (!userParametersContext) {
                return
            }

            const guid = getGuidByContext(userParametersContext)
            const currentContextUserParameterConfigs = guid ? currentUserParameterConfigs[guid] : null
            if (!currentContextUserParameterConfigs || currentContextUserParameterConfigs.length === 0) {
                return
            }

            const {userParameters} = userParametersContext

            if (!userParameters) {
                return
            }

            const side = configContext === ConfigContexts.ColorSelectorOutside ? 'снаружи' : 'изнутри'

            const selectedOption = data
            const selectedConfig = currentContextUserParameterConfigs.find(config => {
                if (colorParameters && userParametersContext instanceof SolidFilling) {
                    return config.id === colorParameters.id
                }
                return config.values?.some(o => o.id === selectedOption.id)
            })

            if (!selectedConfig) {
                return
            }

            const replacedUserParamIndex = userParameters.findIndex(up => up.id === selectedConfig.id)

            if (replacedUserParamIndex !== -1 && currentOrder) {
                const orderClone = currentOrder.clone
                const isFrame = userParametersContext instanceof Product
                const {node, product} = orderClone.findConstructionAndProductAndNodeByNodeId(
                    isFrame ? (userParametersContext as Product).frame.nodeId : userParametersContext.nodeId
                )
                const contextNode = isFrame ? product : node

                if (!contextNode) {
                    return
                }

                contextNode.userParameters = userParameters.slice(0)
                contextNode.userParameters[replacedUserParamIndex].value = selectedOption.value

                if (selectedConfig.isColor) {
                    contextNode.userParameters[replacedUserParamIndex].hex = selectedOption.hex
                } else {
                    contextNode.userParameters[replacedUserParamIndex].title = selectedConfig.name
                }

                orderClone.addAction(`Задали ${selectedOption.value} для ${nameTitle ? nameTitle : 'заполнения ' + side}`, currentConstruction)
                updateCurrentOrder(orderClone)
            }
        },
        [userParametersContext, currentOrder, colorParameters, currentUserParameterConfigs, updateCurrentOrder]
    )
}

export const useCheckOrderUserParameters = (): ((cb?: () => void) => void) => {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentOrderCityId = useSelector((state: IAppState) => state.orders.currentOrder?.city?.id)
    const recalculateOrder = useRecalculateOrder()

    return useCallback(
        async (cb?: () => void) => {
            if (!currentOrder || currentOrder.readonly) {
                cb && cb()
                return
            }

            const orderClone = currentOrder.clone
            if (currentOrderCityId) {
                const promises: Promise<boolean>[] = orderClone.constructions.map(construction => {
                    return actualizeUserParameters(construction, currentOrderCityId)
                })
                await Promise.all(promises)
                    .then(res => {
                        const needUpdate = res.some(r => r)
                        if (needUpdate) {
                            updateCurrentOrder(orderClone)
                            recalculateOrder(orderClone, cb)
                        } else {
                            cb && cb()
                        }
                    })
                    .catch(e => console.error(e))
            }
        },
        [currentOrder, currentOrderCityId, recalculateOrder]
    )
}

export const useGoToNewOrder = (): (() => boolean) => {
    const login = useSelector((state: IAppState) => state.account.login)
    const goTo = useGoTo()
    const isDemoUser = useIsDemoUser()

    return useCallback((): boolean => {
        const isAvailable = isNewOrderAvailable(login)
        if (isAvailable) {
            goTo(`${NAV_ORDER}?showTemplate&category=pvh`)
            isDemoUser ? AnalyticsManager.trackNewOrderFromDemoClick() : AnalyticsManager.trackClickNewOrderButton()
            return true
        }
        return false
    }, [login, goTo, isDemoUser])
}

export const useIsNewOrderAvailable = (): boolean => {
    const login = useSelector((state: IAppState) => state.account.login)

    return isNewOrderAvailable(login)
}

export const useCancelOrder = (): ((orderId: number) => void) => {
    const dispatch = useDispatch()
    return useCallback(
        (orderId: number) => {
            dispatch(cancelOrder(orderId))
        },
        [dispatch]
    )
}

export const useCancelOrderModal = (order: Order | null): [ModalModel | null, () => void] => {
    const [modalModel, setModalModel] = useState<ModalModel | null>(null)
    const cancelOrder = useCancelOrder()

    const showModal = useCallback(() => {
        if (!order) {
            return
        }
        const {orderNumber} = order
        setModalModel(
            ModalModel.Builder.withTitle(`Запрос отмены заказа ${orderNumber}`)
                .withBody(
                    <p>
                        Мы отменим этот заказ, если он еще не ушел в производство.
                        <br />
                        Обработка Вашего запроса займет не более 30 минут в рабочее время (с 9:00 до 18:00 по прм).
                        <br />
                        Если заказ будет находиться уже на этапе производства он не может быть отменен.
                    </p>
                )
                .withPositiveAction(
                    new Action('Запросить отмену заказа', () => {
                        cancelOrder(order.id)
                        setModalModel(null)
                    })
                )
                .withNegativeAction(new Action('Закрыть', () => setModalModel(null)))
                .build()
        )
    }, [order, cancelOrder])

    return [modalModel, showModal]
}
