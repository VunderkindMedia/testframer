import {useEffect, useRef} from 'react'

export const useEffectSkipFirst = (cb: React.EffectCallback, deps: React.DependencyList): void => {
    const skipFirst = useRef(true)

    useEffect(() => {
        if (skipFirst.current) {
            skipFirst.current = false
            return
        }
        // eslint-disable-next-line consistent-return
        return cb()
    }, deps) // eslint-disable-line react-hooks/exhaustive-deps
}
