import {useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {Roles} from '../model/framer/user-permissions'
import {isDemoAccount} from '../helpers/account'
import {FactoryTypes} from '../model/framer/factory-types'

interface IUserPermissions {
    isBossManager: boolean
    isNonQualified: boolean
    isChiefManager: boolean
    isNonQualifiedOrBossManager: boolean
    isCommonManager: boolean
}

const defaultPermissions: IUserPermissions = {
    isNonQualified: true,
    isCommonManager: true,
    isBossManager: false,
    isChiefManager: false,
    isNonQualifiedOrBossManager: true
}

export const usePermissions = (): IUserPermissions => {
    const permissions = useSelector((state: IAppState) => state.account.permissions)
    if (!permissions) {
        return defaultPermissions
    }
    return {
        isNonQualified: permissions.role === Roles.NonqualifiedBossManager,
        isCommonManager: permissions.role === Roles.CommonManager,
        isBossManager: permissions.role === Roles.BossManager,
        isChiefManager: permissions.role === Roles.ChiefManager,
        isNonQualifiedOrBossManager: permissions.role === Roles.BossManager || permissions.role === Roles.NonqualifiedBossManager
    }
}

export const useIsExternalUser = (): boolean => {
    const partner = useSelector((state: IAppState) => state.account.partner)
    return partner ? partner.isExternal : true
}

export const useIsMainDealer = (): boolean => {
    const partner = useSelector((state: IAppState) => state.account.partner)
    const permission = usePermissions()

    if (!partner) {
        return false
    }

    const {isSubdealershipAvailable} = partner

    return isSubdealershipAvailable && permission.isBossManager
}

export const useIsExportToXmlAvailable = (): boolean => {
    const partner = useSelector((state: IAppState) => state.account.partner)
    return partner?.isExportOrderToXmlAvailable || false
}

export const useIsDemoUser = (): boolean => {
    const login = useSelector((state: IAppState) => state.account.login)
    return isDemoAccount(login)
}

interface IPartnerAccess {
    isExternal: boolean
    isPermPartner: boolean
}

export const usePartnerAccess = (): IPartnerAccess => {
    const partner = useSelector((state: IAppState) => state.account.partner)

    if (!partner) {
        return {
            isExternal: true,
            isPermPartner: true
        }
    }

    return {
        isExternal: partner.isExternal,
        isPermPartner: partner.factoryId && partner.factoryId === FactoryTypes.Perm
    }
}
