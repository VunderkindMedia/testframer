import {useDispatch} from 'react-redux'
import {useCallback} from 'react'
import {showStatusbar} from '../redux/statusbar/actions'

export const useShowStatusbar = (): ((message: string, isError: boolean) => void) => {
    const dispatch = useDispatch()
    return useCallback((message: string, isError: boolean) => dispatch(showStatusbar(message, isError)), [dispatch])
}
