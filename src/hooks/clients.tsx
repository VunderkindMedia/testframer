import * as React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {useCallback, useEffect, useState} from 'react'
import {Client} from '../model/framer/client'
import {Order} from '../model/framer/order'
import {
    createClient,
    loadClients,
    loadClient,
    loadClientOrders,
    setCurrentClient,
    updateClientAsync,
    deleteClient
} from '../redux/clients/actions'
import {useReplaceTo} from './router'
import {NAV_CLIENTS, NAV_CLIENT} from '../constants/navigation'
import {getClientsFilter, getClientsFilterParams} from '../helpers/clients'
import {ModalModel} from '../components/common/modal/modal-model'
import {Action} from '../model/action'

export const useClients = (filter = getClientsFilter(0, Number.MAX_SAFE_INTEGER)): Client[] => {
    const clients = useSelector((state: IAppState) => state.clients.all)

    const dispatch = useDispatch()
    const loadClientsAction = useCallback((filter: string) => dispatch(loadClients(filter)), [dispatch])

    useEffect(() => {
        loadClientsAction(filter)
    }, [filter, loadClientsAction])

    return clients
}

export const useSetCurrentClient = (): ((client: Client | null) => void) => {
    const dispatch = useDispatch()
    return useCallback((client: Client | null) => dispatch(setCurrentClient(client)), [dispatch])
}

export const useClientsFromSearch = (): Client[] => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const clients = useSelector((state: IAppState) => {
        return state.clients.searchQuery ? state.clients.filtered : state.clients.all
    })
    const currentClient = useSelector((state: IAppState) => state.clients.currentClient)
    const clientsOnPage = useSelector((state: IAppState) => state.clients.clientsOnPage)

    const dispatch = useDispatch()
    const loadClientsAction = useCallback((filter: string) => dispatch(loadClients(filter)), [dispatch])
    const replaceTo = useReplaceTo()

    useEffect(() => {
        loadClientsAction(getClientsFilter(0, Number.MAX_SAFE_INTEGER))
    }, [loadClientsAction])

    const setCurrentClient = useSetCurrentClient()

    useEffect(() => {
        if (!currentClient) {
            return
        }

        const {offset} = getClientsFilterParams(search)
        const clientOffset = clients.findIndex(client => client.id === currentClient.id)

        if (clientOffset !== -1 && (clientOffset < offset || clientOffset >= offset + clientsOnPage)) {
            setCurrentClient(clients[offset])
        }
    }, [clients, search, clientsOnPage, setCurrentClient, currentClient])

    useEffect(() => {
        const {offset} = getClientsFilterParams(search)

        if (!search) {
            const filter = getClientsFilter(offset, clientsOnPage)
            replaceTo(`${NAV_CLIENTS}${filter}`)
        }
    }, [search, clientsOnPage, replaceTo])

    return clients
}

export const useClientFromSearch = (): Client | null => {
    const [draftClient, setDraftClient] = useState<Client | null>(null)

    const id = useSelector((state: IAppState) => new URLSearchParams(state.router.location.search).get('clientId'))
    const currentClient = useSelector((state: IAppState) => state.clients.currentClient)

    const dispatch = useDispatch()
    const replaceTo = useReplaceTo()

    useEffect(() => {
        id && dispatch(loadClient(id))
    }, [id, dispatch])

    useEffect(() => {
        setDraftClient(id ? currentClient : new Client())

        if (id) {
            setDraftClient(currentClient)
        } else {
            setDraftClient(new Client())
            currentClient && replaceTo(`${NAV_CLIENT}?clientId=${currentClient.id}`)
        }
    }, [id, currentClient, replaceTo])

    return draftClient
}

export const useClientOrders = (): Order[] => {
    const id = useSelector((state: IAppState) => new URLSearchParams(state.router.location.search).get('clientId'))
    const orders = useSelector((state: IAppState) => state.clients.currentClientOrders)
    const dispatch = useDispatch()

    useEffect(() => {
        id && dispatch(loadClientOrders(id))
    }, [id, dispatch])

    return orders || []
}

export const useUpdateClient = (): ((client: Client) => void) => {
    const dispatch = useDispatch()

    return useCallback(
        (client: Client) => {
            dispatch(updateClientAsync(client))
        },
        [dispatch]
    )
}

export const useCreateClient = (): ((client: Client) => void) => {
    const dispatch = useDispatch()

    return useCallback(
        (client: Client) => {
            dispatch(createClient(client))
        },
        [dispatch]
    )
}

export const useDeleteClientModal = (): [ModalModel | null, (client: Client) => void] => {
    const [modalModel, setModalModel] = useState<ModalModel | null>(null)

    const dispatch = useDispatch()

    const showModal = useCallback(
        (client: Client) => {
            setModalModel(
                ModalModel.Builder.withTitle(`Подтверждение удаления клиента ${client.name}`)
                    .withBody(<p>Удалить клиента {client.name}?</p>)
                    .withPositiveAction(
                        new Action(
                            'Удалить',
                            () => {
                                dispatch(deleteClient(client.id))
                                setModalModel(null)
                            },
                            'submit-client-remove'
                        )
                    )
                    .withNegativeAction(new Action('Отменить', () => setModalModel(null)))
                    .build()
            )
        },
        [dispatch]
    )

    return [modalModel, showModal]
}
