import {useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {
    NAV_BILL,
    NAV_FORGOT_PASSWORD,
    NAV_KP,
    NAV_PARTNER_GOODS_REPORT,
    NAV_REGISTRATION,
    NAV_LOGIN,
    NAV_RESET_PASSWORD,
    NAV_ROOT,
    NAV_EVENTS_REPORT,
    NAV_MOUNTING_REPORT,
    NAV_BUILDER
} from '../constants/navigation'

export const useHideNavigation = (): boolean => {
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)

    return (
        pathname === NAV_ROOT ||
        pathname === NAV_KP ||
        pathname === NAV_BILL ||
        pathname === NAV_FORGOT_PASSWORD ||
        pathname === NAV_RESET_PASSWORD ||
        pathname === NAV_REGISTRATION ||
        pathname === NAV_PARTNER_GOODS_REPORT ||
        pathname === NAV_EVENTS_REPORT ||
        pathname === NAV_MOUNTING_REPORT ||
        pathname === NAV_LOGIN ||
        pathname === NAV_BUILDER
    )
}
