import * as React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {useCallback, useEffect, useState, useRef} from 'react'
import {Mounting} from '../model/framer/mounting'
import {Order} from '../model/framer/order'
import {
    loadMountings,
    loadMounting,
    addMounting,
    updateMounting,
    deleteMounting,
    downloadMountingsReport,
    updateClientAddress
} from '../redux/mountings/actions'
import {ModalModel} from '../components/common/modal/modal-model'
import {Action} from '../model/action'
import {useGoTo, useReplaceTo} from '../hooks/router'
import {NAV_MOUNTINGS} from '../constants/navigation'
import {MOUNTINGS_FILTERS} from '../constants/local-storage'
import {getMountingsFilter, getMountingsFilterParams, IMountingsFilterParams} from '../helpers/mountings'

export const useMountingsFromSearch = (): Mounting[] => {
    const mountings = useSelector((state: IAppState) => state.mountings.all)
    const currentMounting = useSelector((state: IAppState) => state.mountings.current)
    const mountingsOnPage = useSelector((state: IAppState) => state.mountings.mountingsOnPage)
    const search = useSelector((state: IAppState) => state.router.location.search)
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)

    const dispatch = useDispatch()
    const replaceTo = useReplaceTo()
    const basePathname = useRef(pathname)

    const loadMountingsAction = useCallback(
        (filters: string) => {
            dispatch(loadMountings(filters))
        },
        [dispatch]
    )

    useEffect(() => {
        search && loadMountingsAction(search)
    }, [search, loadMountingsAction])

    useEffect(() => {
        if (basePathname.current !== pathname || !currentMounting) {
            return
        }

        const {offset} = getMountingsFilterParams(search)
        let currentOffset = offset

        if (currentMounting) {
            const orderOffset = mountings.indexOf(currentMounting)
            if (orderOffset !== -1 && orderOffset >= offset + mountingsOnPage) {
                currentOffset = offset + orderOffset
            }
        }

        const searchParams = getMountingsFilterParams(search)
        const params = {
            ...searchParams,
            offset: currentOffset,
            limit: mountingsOnPage
        }

        const filter = getMountingsFilter(params)
        if (filter !== search) {
            replaceTo(`${NAV_MOUNTINGS}${filter}`)
        }
    }, [search, mountingsOnPage, mountings, currentMounting, pathname, replaceTo])

    return mountings
}

export const useMountingFromSearch = (): Mounting | null => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const mounting = useSelector((state: IAppState) => state.mountings.current)

    const dispatch = useDispatch()
    const loadMountingAction = useCallback((id: number) => dispatch(loadMounting(id)), [dispatch])

    useEffect(() => {
        const params = new URLSearchParams(search)
        const id = Number(params.get('mountingId'))

        if (!isNaN(id) && id !== mounting?.id) {
            loadMountingAction(id)
        }
    }, [loadMountingAction, search, mounting])

    return mounting
}

export const useSetMountingsFilters = (): ((params: IMountingsFilterParams) => void) => {
    const goTo = useGoTo()

    return useCallback(
        (params: IMountingsFilterParams) => {
            localStorage.setItem(MOUNTINGS_FILTERS, JSON.stringify(params))
            goTo(`${NAV_MOUNTINGS}${getMountingsFilter({...params, offset: 0})}`)
        },
        [goTo]
    )
}

export const useAddMounting = (): ((order: Order, date: Date) => void) => {
    const dispatch = useDispatch()

    return useCallback(
        (order: Order, date: Date) => {
            const {owner} = order
            if (!owner || !owner.id) {
                return
            }
            const executorsIds = [owner.id]

            dispatch(addMounting(date.toISOString(), executorsIds, order))
        },
        [dispatch]
    )
}

export const useUpdateMountingDate = (): ((mounting: Mounting, date: Date) => void) => {
    const dispatch = useDispatch()
    const updateMountingAction = useCallback((mounting: Mounting) => dispatch(updateMounting(mounting.id, mounting)), [dispatch])

    return useCallback(
        (mounting: Mounting, date: Date) => {
            const mountingClone = mounting.clone
            mountingClone.datetime = date.toISOString()
            updateMountingAction(mountingClone)
        },
        [updateMountingAction]
    )
}

export const useUpdateMountingNote = (): ((mounting: Mounting, note: string) => void) => {
    const dispatch = useDispatch()
    const updateMountingAction = useCallback((mounting: Mounting) => dispatch(updateMounting(mounting.id, mounting)), [dispatch])

    return useCallback(
        (mounting: Mounting, note: string) => {
            const mountingClone = mounting.clone
            mountingClone.note = note.trim()
            updateMountingAction(mountingClone)
        },
        [updateMountingAction]
    )
}

export const useUpdateClientAddress = (): ((mounting: Mounting, address: string) => void) => {
    const dispatch = useDispatch()

    return useCallback(
        (mounting: Mounting, address: string) => {
            const client = mounting.client?.clone

            if (!client) {
                return
            }

            client.address = address

            dispatch(updateClientAddress(mounting, client))
        },
        [dispatch]
    )
}

export const useDeleteMounting = (): ((id: number) => void) => {
    const dispatch = useDispatch()

    return useCallback(
        (id: number) => {
            dispatch(deleteMounting(id))
        },
        [dispatch]
    )
}

export const useDeleteMountingModal = (): [ModalModel | null, (mounting: Mounting, onSuccess?: () => void) => void] => {
    const [modalModel, setModalModel] = useState<ModalModel | null>(null)
    const deleteMounting = useDeleteMounting()

    const showModal = useCallback(
        (mounting: Mounting, onSuccess?: () => void) => {
            const {order, id} = mounting

            setModalModel(
                ModalModel.Builder.withTitle(`Подтверждение удаления монтажа для заказа ${order.orderNumber}`)
                    .withBody(<p>Удалить заявку на монтаж для заказа {order.orderNumber}?</p>)
                    .withPositiveAction(
                        new Action(
                            'Удалить',
                            () => {
                                deleteMounting(id)
                                setModalModel(null)
                                onSuccess && onSuccess()
                            },
                            'submit-mounting-remove'
                        )
                    )
                    .withNegativeAction(new Action('Отменить', () => setModalModel(null)))
                    .build()
            )
        },
        [deleteMounting]
    )

    return [modalModel, showModal]
}

export const useDownloadMountingsReport = (): ((mounting: Mounting) => void) => {
    const dispatch = useDispatch()

    return useCallback((mounting: Mounting) => dispatch(downloadMountingsReport(mounting)), [dispatch])
}
