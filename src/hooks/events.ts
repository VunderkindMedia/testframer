import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {useCallback, useEffect, useState} from 'react'
import {useGoTo} from '../hooks/router'
import {IEventsFilterParams, getEventsFilter} from '../helpers/events'
import {NAV_EVENTS} from '../constants/navigation'
import {EVENTS_FILTERS} from '../constants/local-storage'
import {loadCalendarEvents} from '../redux/calendar/actions'
import {CalendarEvent} from '../model/framer/calendar/calendar-event'

export const useSetEventsFilters = (): ((params: IEventsFilterParams) => void) => {
    const goTo = useGoTo()

    return useCallback(
        (params: IEventsFilterParams) => {
            localStorage.setItem(EVENTS_FILTERS, JSON.stringify(params))
            goTo(`${NAV_EVENTS}${getEventsFilter(params)}`)
        },
        [goTo]
    )
}

export const useEventsFromSearch = (): CalendarEvent[] => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const events = useSelector((state: IAppState) => state.calendar.events)

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadCalendarEvents(search))
    }, [search, dispatch])

    return events
}

export const useEventsFromSearchByDay = (): Map<string, CalendarEvent[]> => {
    const [eventsByDay, setEventsByDay] = useState(new Map<string, CalendarEvent[]>())
    const events = useEventsFromSearch()

    useEffect(() => {
        const groupedEvents = new Map<string, CalendarEvent[]>()
        events.map(event => {
            const dayEvents = groupedEvents.get(event.date)

            if (dayEvents) {
                dayEvents.push(event)
            } else {
                groupedEvents.set(event.date, [event])
            }
        })
        setEventsByDay(groupedEvents)
    }, [events])

    return eventsByDay
}
