import {useCallback} from 'react'
import {ConfigContexts} from '../model/config-contexts'
import {
    setBuilderMode,
    setConfigContext,
    setCurrentContext,
    setCurrentDimensionLine,
    setFurnitureToAllConstructions,
    setHighlightedContext,
    setHighlightedDimensionLine,
    setProfileToAllConstructions
} from '../redux/builder/actions'
import {useDispatch, useSelector} from 'react-redux'
import {IContext} from '../model/i-context'
import {DimensionLine} from '../model/dimension-line'
import {FrameFilling} from '../model/framer/frame-filling'
import {IAppState} from '../redux/root-reducer'
import {Connector} from '../model/framer/connector'
import {Beam} from '../model/framer/beam'
import {Impost} from '../model/framer/impost'
import {setCurrentProduct} from '../redux/orders/actions'
import {runLongNetworkOperation} from '../redux/request/actions'
import {api} from '../api'
import {Profile} from '../model/framer/profile'
import {ConnectorParameters} from '../model/framer/connector-parameters'
import {useUpdateCurrentOrder, useUserParametersContext} from './orders'
import {FactoryTypes} from '../model/framer/factory-types'
import {SolidFilling} from '../model/framer/solid-filling'
import {Handle} from '../model/handle'
import {OpeningSystem} from '../model/opening-system'
import {Order} from '../model/framer/order'
import {Construction} from '../model/framer/construction'
import {Dispatch} from 'redux'
import {ShtulpOpenTypes} from '../model/framer/shtulp-open-types'
import {ModelParts} from '../model/framer/model-parts'
import {ImpostTypes} from '../model/impost-types'
import {Product} from '../model/framer/product'
import {
    EMPTY_OPENING_SYSTEM,
    LEFT_OPENING_SYSTEM,
    LEFT_SHTULP_OPENING_SYSTEM,
    OPENING_SYSTEMS,
    RIGHT_OPENING_SYSTEM,
    RIGHT_SHTULP_OPENING_SYSTEM
} from '../constants/opening-systems'
import {Furniture} from '../model/framer/furniture'
import {Lamination} from '../model/framer/lamination'
import {getMoskitkaColorName, MoskitkaColors} from '../model/framer/moskitka-colors'
import {Filling} from '../model/framer/filling'
import {DEFAULT_GLASS_COLOR, INSIDE_FILLING_COLOR_ID, OUTSIDE_FILLING_COLOR_ID} from '../constants/default-user-parameters'
import {FILLING_SELECTOR_APPLY_TO_ALL} from '../constants/local-storage'
import {HandlePositionTypes} from '../model/framer/handle-position-types'
import {HandleTypes} from '../model/framer/handle-types'
import {HandleColors} from '../model/framer/handle-colors'
import {Sides} from '../model/sides'
import {useSetConnectingSide, useSetHostProduct} from './template-selector'
import {GeometryParameters} from '../model/framer/geometry-parameters'
import {Dealer} from '../model/framer/dealer'
import {FURNITURE_ROTO} from '../constants/furniture-templates'
import {Moskitka} from '../model/framer/moskitka'
import {BuilderModes} from '../model/builder-modes'
import {UserParameterConfig} from '../model/framer/user-parameter/user-parameter-config'
import {getGuidByContext} from '../helpers/builder'
import {Partner} from '../model/framer/partner'
import {UserParameter} from '../model/framer/user-parameter/user-parameter'
import {getConstructionWithDefaultParameters} from '../helpers/orders'
import {ISolidFillingPart, SolidFillingPartType} from '../model/framer/filling-formula'

export const useSetConfigContext = (): ((configContext: ConfigContexts | null) => void) => {
    const dispatch = useDispatch()
    return useCallback(
        (configContext: ConfigContexts | null) => {
            dispatch(setConfigContext(configContext))

            if (configContext !== null) {
                configContext === ConfigContexts.Connector && dispatch(setBuilderMode(BuilderModes.Connector))
                dispatch(setCurrentDimensionLine(null))
                dispatch(setHighlightedContext(null))
            }
        },
        [dispatch]
    )
}

const getRealContextFromBeam = (product: Product, beam: Beam): Beam | FrameFilling => {
    const frame = product.frame.findNodeById(beam.parentNodeId) as FrameFilling
    const hasFewParts = product.getAvailablePartsByBeam(beam).length > 1
    const hasEqualsMarkingAllBeams = frame.frameBeams.every(b => b.profileVendorCode === beam.profileVendorCode)

    if (product.isEntranceDoor) {
        if (beam.nodeId === product.frame.bottommostBeam.nodeId) {
            return beam
        }
    }

    if (product.isAluminium) {
        return frame
    }

    if (hasFewParts && !hasEqualsMarkingAllBeams) {
        return beam
    } else {
        return frame
    }
}

export const useSetCurrentContext = (): ((context: IContext | null, construction?: Construction | null) => void) => {
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentContextNodeId = useSelector((state: IAppState) => state.builder.currentContext?.nodeId)
    const configContext = useSelector((state: IAppState) => state.builder.configContext)

    const dispatch = useDispatch()

    const resetContexts = useCallback(() => {
        dispatch(setCurrentContext(null))
        configContext !== ConfigContexts.Services && configContext !== ConfigContexts.Goods && dispatch(setConfigContext(null))
    }, [configContext, dispatch])

    return useCallback(
        (context: IContext | null, construction?: Construction | null) => {
            dispatch(setCurrentDimensionLine(null))
            dispatch(setHighlightedContext(null))

            if (configContext !== ConfigContexts.Connector && context?.nodeId !== currentContextNodeId) {
                resetContexts()
            }

            const construction1 = construction ?? currentConstruction

            if (!construction1) {
                resetContexts()
                return
            }

            if (!context) {
                resetContexts()
                return
            }

            if (context instanceof Handle) {
                dispatch(setCurrentContext(context))
                return
            }

            const {product, node} = construction1.findProductAndNodeByNodeId(context.nodeId)

            if (!product || !node) {
                resetContexts()
                return
            }

            dispatch(setCurrentProduct(product))

            let localContext = node
            if (node instanceof Connector) {
                dispatch(setBuilderMode(BuilderModes.Connector))
            } else if (node instanceof Impost) {
                const dimensionLines = construction1.getDimensionLines()

                let line: DimensionLine | undefined
                if (node.isVertical) {
                    line = dimensionLines.filter(dLine => dLine.isHorizontal).find(dLine => dLine.rightTopPoint.x === node.rightTopPoint.x)
                } else if (node.isHorizontal) {
                    line = dimensionLines.filter(dLine => dLine.isVertical).find(dLine => dLine.rightTopPoint.y === node.rightTopPoint.y)
                }

                line && dispatch(setCurrentDimensionLine(line))
            } else if (node instanceof Beam) {
                localContext = getRealContextFromBeam(product, node)

                if (localContext instanceof FrameFilling) {
                    if (!(localContext.isRoot && construction1.products.length < 2)) {
                        const frameStartX = localContext.rect.leftBottomPoint.x
                        const frameEndX = localContext.rect.rightTopPoint.x

                        const dimensionLines = construction1.getDimensionLines()

                        const lines = dimensionLines
                            .slice(0)
                            .filter(dLine => dLine.isHorizontal)
                            .filter(dLine => dLine.leftBottomPoint.x <= frameStartX && frameEndX <= dLine.rightTopPoint.x)
                            .sort((dLine1, dLine2) => {
                                const startDif1 = dLine1.leftBottomPoint.x - frameStartX
                                const endDif1 = dLine1.rightTopPoint.x - frameStartX

                                const startDif2 = dLine2.leftBottomPoint.x - frameStartX
                                const endDif2 = dLine2.rightTopPoint.x - frameStartX

                                const s1 = Math.abs(startDif1) + Math.abs(endDif1)
                                const s2 = Math.abs(startDif2) + Math.abs(endDif2)

                                return s1 - s2
                            })

                        const line = lines[0]
                        line && dispatch(setCurrentDimensionLine(line))
                    }

                    if (localContext.isRoot && !product?.isEntranceDoor && construction1.products.length === 1) {
                        return
                    }
                }
            }

            dispatch(setCurrentContext(localContext))
        },
        [currentConstruction, currentContextNodeId, configContext, resetContexts, dispatch]
    )
}

export const useSetHighlightedContext = (): ((context: IContext | null) => void) => {
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const highlightedContext = useSelector((state: IAppState) => state.builder.highlightedContext)

    const dispatch = useDispatch()
    return useCallback(
        (context: IContext | null) => {
            if (!currentConstruction) {
                return
            }
            if (Object.is(context, highlightedContext)) {
                return
            }

            const {product, node} = currentConstruction.findProductAndNodeByNodeId(context?.nodeId ?? '')
            let localContext = node

            if (product && node instanceof Beam && !(node instanceof Connector) && !(node instanceof Impost)) {
                localContext = getRealContextFromBeam(product, node)
            }

            if (
                localContext instanceof FrameFilling &&
                localContext.isRoot &&
                !product?.isEntranceDoor &&
                currentConstruction.products.length === 1
            ) {
                return
            }

            if (context instanceof Handle) {
                localContext = context
            }

            dispatch(setHighlightedDimensionLine(null))
            dispatch(setHighlightedContext(localContext))
        },
        [currentConstruction, highlightedContext, dispatch]
    )
}

export const useSetCurrentDimensionLine = (): ((dimensionLine: DimensionLine | null) => void) => {
    const dispatch = useDispatch()
    return useCallback(
        (dimensionLine: DimensionLine | null) => {
            dispatch(setCurrentDimensionLine(dimensionLine))
            dimensionLine && dispatch(setConfigContext(null))
        },
        [dispatch]
    )
}

export const useSetHighlightedDimensionLine = (): ((dimensionLine: DimensionLine | null) => void) => {
    const dispatch = useDispatch()
    return useCallback((dimensionLine: DimensionLine | null) => dispatch(setHighlightedDimensionLine(dimensionLine)), [dispatch])
}

const setProfile = async (profile: Profile, order: Order, construction: Construction, partner: Partner, dispatch: Dispatch): Promise<void> => {
    const oldConnectorsParameters = construction.connectorsParameters.slice(0)
    let newConnectorsParameters: ConnectorParameters[] | null = null

    await runLongNetworkOperation(async () => {
        newConnectorsParameters = await api.design.getConnectorsParameters(profile.id)
    })(dispatch)

    if (!newConnectorsParameters) {
        return
    }

    for (const product of construction.products) {
        product.setProfile(profile)
        product.availableFillings = await api.design.getFillings(profile.id)

        if (partner.factoryId === FactoryTypes.Perm && product.profile.name === 'Rehau Blitz New') {
            const defaultLamination = Lamination.getDefault()
            if (product.outsideLamination.hex !== defaultLamination.hex) {
                product.setOutsideLamination(defaultLamination)
            }
            if (product.insideLamination.hex !== defaultLamination.hex) {
                product.setInsideLamination(defaultLamination)
            }
        }
    }

    construction.connectorsParameters = newConnectorsParameters
    construction.allConnectors.forEach(connector => {
        const oldParam = oldConnectorsParameters.find(cp => cp.marking === connector.profileVendorCode)
        if (!oldParam) {
            return
        }
        const newParam = construction.connectorsParameters.find(
            newParam =>
                newParam.connectorType === oldParam.connectorType &&
                (newParam.comment === oldParam.comment || (newParam.isHType() && oldParam.isHType()))
        )
        if (!newParam) {
            return
        }
        connector.profileVendorCode = newParam.marking
        connector.width = newParam.a
    })

    order.addAction(`Задали профиль ${profile.displayName ? profile.displayName : profile.name}`, construction)
}

export const useSetProfileToCurrentConstruction = (): ((profile: Profile) => void) => {
    const partner = useSelector((state: IAppState) => state.account.partner)
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)

    const dispatch = useDispatch()
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        async (profile: Profile) => {
            if (!currentOrder || !partner) {
                return
            }

            const order = currentOrder.clone
            const construction = order.findConstructionById(currentConstruction?.id ?? '')

            if (!construction) {
                return
            }

            await setProfile(profile, order, construction, partner, dispatch)
            updateCurrentOrder(order)
        },
        [dispatch, updateCurrentOrder, currentOrder, currentConstruction?.id, partner]
    )
}

export const useOpeningSystems = (product: Product | null, context: IContext | null): OpeningSystem[] => {
    if (!product || !(context instanceof FrameFilling || context instanceof SolidFilling)) {
        return []
    }

    const shtulpGroups = product.findAvailableShtulpGroups(context)

    if ((context instanceof FrameFilling && context.isP400) || product.isP400) {
        return [RIGHT_OPENING_SYSTEM, LEFT_OPENING_SYSTEM, EMPTY_OPENING_SYSTEM]
    }

    return OPENING_SYSTEMS.filter(openingSystem => {
        if (shtulpGroups.length === 0) {
            return !openingSystem.equals(RIGHT_SHTULP_OPENING_SYSTEM) && !openingSystem.equals(LEFT_SHTULP_OPENING_SYSTEM)
        }

        if (openingSystem.equals(LEFT_SHTULP_OPENING_SYSTEM)) {
            return shtulpGroups.some(g => g.leftFilling.nodeId === context.nodeId)
        }
        if (openingSystem.equals(RIGHT_SHTULP_OPENING_SYSTEM)) {
            return shtulpGroups.some(g => g.rightFilling.nodeId === context.nodeId)
        }

        return true
    })
}

export const useCurrentOpeningSystem = (openingSystems: OpeningSystem[], context: IContext | null): OpeningSystem | undefined => {
    return context instanceof FrameFilling
        ? openingSystems.find(
              o =>
                  o.openType === context.openType &&
                  o.openSide === context.openSide &&
                  (context.shtulpOpenType === ShtulpOpenTypes.NoShtulpOnLeaf
                      ? o.shtulpOpenType === ShtulpOpenTypes.NoShtulp
                      : o.shtulpOpenType === context.shtulpOpenType)
          )
        : EMPTY_OPENING_SYSTEM
}

export const useSetOpeningSystem = (
    order: Order | null,
    context: IContext | null
): ((openingSystem: OpeningSystem, currentOpeningSystem: OpeningSystem) => void) => {
    const partner = useSelector((state: IAppState) => state.account.partner)

    const dispatch = useDispatch()
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        async (openingSystem: OpeningSystem, currentOpeningSystem: OpeningSystem) => {
            if (!order || !partner) {
                return
            }

            const orderClone = order.clone

            if (!orderClone || !context) {
                return
            }

            const {construction, product, node} = orderClone?.findConstructionAndProductAndNodeByNodeId<SolidFilling | FrameFilling>(
                context.nodeId
            )

            if (!construction || !product) {
                return
            }

            const setOpeningSystem = (order: Order): void => {
                if (product && node) {
                    order.addAction(
                        `Сменили тип открывания с ${currentOpeningSystem?.name} на ${openingSystem.name}`,
                        construction,
                        product,
                        node
                    )
                    product.setOpeningSystem(node, openingSystem)
                }
                const {handleColor, handleType} = product.frame
                product.frame.allFrameFillings.map(f => {
                    f.handleColor = handleColor
                    f.handleType = handleType
                })
                updateCurrentOrder(order)
            }

            if (
                openingSystem.shtulpOpenType === ShtulpOpenTypes.ShtulpOnLeaf &&
                !product.profile.parts.some(p => p.modelPart === ModelParts.Shtulp)
            ) {
                const profile = product.getAvailableProfilesFilteredByModelParts(product.usedModelParts.concat(ModelParts.Shtulp))[0]
                await setProfile(profile, orderClone, construction, partner, dispatch)
                setOpeningSystem(orderClone)
            } else {
                setOpeningSystem(orderClone)
            }
        },
        [dispatch, updateCurrentOrder, order, partner, context]
    )
}

export const useAddImpost = (order: Order | null, product: Product | null, context: IContext | null): ((type: ImpostTypes) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    const solidFilling = context instanceof SolidFilling ? context : product?.frame.innerFillings[0]?.leaf?.innerFillings[0]?.solid

    return useCallback(
        (type: ImpostTypes) => {
            if (!order || !solidFilling) {
                return
            }
            const orderClone = order.clone
            const {construction, product, node} = orderClone.findConstructionAndProductAndNodeByNodeId<SolidFilling>(solidFilling.nodeId)

            if (product && node) {
                product.addImpost(node, type)
                orderClone.addAction(`Добавили ${type.toLowerCase()} импост`, construction, product, node)
                updateCurrentOrder(orderClone)
            }
        },
        [order, solidFilling, updateCurrentOrder]
    )
}

export const useRemoveImpost = (order: Order | null, context: IContext | null): (() => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()
    const setCurrentContext = useSetCurrentContext()

    return useCallback(() => {
        if (!order || !(context instanceof Impost)) {
            return
        }
        const orderClone = order.clone
        const {construction, product, node} = orderClone.findConstructionAndProductAndNodeByNodeId<Impost>(context.nodeId)

        if (product && node) {
            product.removeImpost(node)
            orderClone.addAction(`Удалили ${node.isHorizontal ? 'горизонтальный' : 'вертикальный'} импост`, construction, product, node)
            const updatedProduct = updateCurrentOrder(orderClone).findProductByProductGuid(product.productGuid)
            const solidFilling = updatedProduct?.frame.allSolidFillings.find(sf => sf.containerPolygon.equals(node.containerPolygon))

            solidFilling && setCurrentContext(solidFilling)
        }
    }, [order, context, updateCurrentOrder, setCurrentContext])
}

export const useSetImpostOffset = (order: Order | null, context: IContext | null): ((offset: number) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        (offset: number) => {
            if (!order || !(context instanceof Impost)) {
                return
            }
            const orderClone = order.clone
            const {product, node} = orderClone.findConstructionAndProductAndNodeByNodeId<Impost>(context.nodeId)

            if (product && node) {
                product.setImpostOffset(node, offset)
                orderClone.addAction(`Задали отступ ${context.isVertical ? 'слева' : 'снизу'} ${offset} импосту ${context.nodeId}`)
                updateCurrentOrder(orderClone)
            }
        },
        [order, context, updateCurrentOrder]
    )
}

export const useSetFurniture = (
    dealer: Dealer | null,
    order: Order | null,
    construction: Construction | null,
    configContext: ConfigContexts | null
): ((furniture: Furniture) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    const dispatch = useDispatch()

    return useCallback(
        furniture => {
            if (!order) {
                return
            }
            const orderClone = order.clone
            const constructionClone = construction ? orderClone.findConstructionById(construction.id) : null

            if (configContext === ConfigContexts.ConstructionParams) {
                dispatch(setFurnitureToAllConstructions(furniture))
            } else if (constructionClone) {
                orderClone.addAction(
                    `Сменили фурнитуру с ${constructionClone.products[0].furniture.displayName} на ${furniture.displayName}`,
                    constructionClone
                )
                constructionClone.products.forEach(product => {
                    product.setFurniture(furniture)
                    if (dealer?.factoryId === FactoryTypes.Moscow && furniture.displayName === FURNITURE_ROTO.displayName) {
                        product.frame.allFrameFillings.forEach(frame => {
                            if (frame.handleType !== HandleTypes.Roto) {
                                frame.handleType = HandleTypes.MacbethMsk
                                frame.handleColor = HandleColors.White
                            }
                        })
                    }
                })
                updateCurrentOrder(orderClone)
            }
        },
        [dealer?.factoryId, order, construction, configContext, updateCurrentOrder, dispatch]
    )
}

export const useSetProfile = (configContext: ConfigContexts | null): ((profile: Profile) => void) => {
    const setProfileToCurrentConstruction = useSetProfileToCurrentConstruction()
    // const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    // const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    //TODO: Необходимо убрать если не используются эти переменные.
    const dispatch = useDispatch()

    return useCallback(
        profile => {
            if (configContext === ConfigContexts.ConstructionParams) {
                dispatch(setProfileToAllConstructions(profile))
            } else {
                setProfileToCurrentConstruction(profile)
            }
        },
        [configContext, setProfileToCurrentConstruction, dispatch]
    )
}

export const useSetLamination = (
    order: Order | null,
    construction: Construction | null,
    configContext: ConfigContexts | null,
    isApplyToBothSide: boolean
): ((lamination: Lamination) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    const setUserFillingParameters = useCallback(
        (params: UserParameter[], lamination: Lamination) => {
            const {name, hex} = lamination
            if (configContext === ConfigContexts.LaminationOutside || isApplyToBothSide) {
                const param = params.find(p => p.id === OUTSIDE_FILLING_COLOR_ID)
                if (param) {
                    param.value = name
                    param.hex = hex
                }
            }
            if (configContext === ConfigContexts.LaminationInside || isApplyToBothSide) {
                const param = params.find(p => p.id === INSIDE_FILLING_COLOR_ID)
                if (param) {
                    param.value = name
                    param.hex = hex
                }
            }
        },
        [isApplyToBothSide, configContext]
    )

    const setUserParameters = useCallback(
        (construction: Construction, lamination: Lamination) => {
            const solidFillings: SolidFilling[] = []
            const leafFillings: FrameFilling[] = []

            construction?.products.map(product => {
                product.frame.innerFillings.forEach(innerFilling => {
                    innerFilling.solid && solidFillings.push(innerFilling.solid)
                    innerFilling.leaf && leafFillings.push(innerFilling.leaf)
                })

                leafFillings.map(l => {
                    l.innerFillings.map(i => i.solid?.userParameters && setUserFillingParameters(i.solid?.userParameters, lamination))
                })

                solidFillings.map(s => setUserFillingParameters(s.userParameters, lamination))
            })
        },
        [setUserFillingParameters]
    )

    const setConstructionLamination = useCallback(
        (construction: Construction, lamination: Lamination) => {
            construction.products.forEach(product => {
                if (isApplyToBothSide) {
                    product.setOutsideLamination(lamination)
                    product.setInsideLamination(lamination)
                } else if (configContext === ConfigContexts.LaminationOutside) {
                    product.setOutsideLamination(lamination)
                } else if (configContext === ConfigContexts.LaminationInside) {
                    product.setInsideLamination(lamination)
                }
            })
        },
        [configContext, isApplyToBothSide]
    )

    return useCallback(
        lamination => {
            if (!order || !construction) {
                return
            }
            const orderClone = order.clone
            const constructionClone = orderClone.findConstructionById(construction.id)

            if (constructionClone) {
                setConstructionLamination(constructionClone, lamination)
                setUserParameters(constructionClone, lamination)

                if (constructionClone.isMoskitka) {
                    const moskitka = constructionClone.moskitka ? constructionClone.moskitka.clone : null
                    if (moskitka) {
                        moskitka.color =
                            lamination.id === Lamination.getDefault().id && isApplyToBothSide
                                ? MoskitkaColors.White
                                : MoskitkaColors.ConstructionColor
                        constructionClone.setMoskitka(constructionClone.products[0].frame.nodeId, moskitka)
                    }
                }

                const side = configContext === ConfigContexts.LaminationOutside ? 'снаружи' : 'изнутри'
                orderClone.addAction(`Задали ламинацию ${lamination.name} ${isApplyToBothSide ? 'с обеих сторон' : side}`, constructionClone)
                updateCurrentOrder(orderClone)
            }
        },
        [order, construction, configContext, isApplyToBothSide, setConstructionLamination, updateCurrentOrder, setUserParameters]
    )
}

export const useSetMoskitkaColor = (order: Order | null, construction: Construction | null): ((color: MoskitkaColors) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        color => {
            if (!order || !construction) {
                return
            }
            const orderClone = order.clone
            const constructionClone = orderClone.findConstructionById(construction.id)

            if (!constructionClone || !constructionClone.isMoskitka) {
                return
            }

            const moskitka = constructionClone.moskitka ? constructionClone.moskitka.clone : null

            if (!moskitka) {
                return
            }

            moskitka.color = color
            constructionClone.setMoskitka(constructionClone.products[0].frame.nodeId, moskitka)
            const defaultLamination = Lamination.getDefault()
            constructionClone.products[0].setOutsideLamination(defaultLamination)
            constructionClone.products[0].setInsideLamination(defaultLamination)

            const info = `${moskitka.catproof ? 'антикошка' : 'обычная'} ${getMoskitkaColorName(moskitka.color)} ${
                moskitka.set ? 'установлена' : 'не установлена'
            }`
            orderClone.addAction(`Обновили москитку на ${info.toLocaleLowerCase().replace('\n', '')}`, constructionClone)
            updateCurrentOrder(orderClone)
        },
        [order, construction, updateCurrentOrder]
    )
}

export const useSetFilling = (
    order: Order | null,
    construction: Construction | null,
    context: IContext | null,
    configContext: ConfigContexts | null,
    fillings: Filling[],
    isApplyToAll: boolean
): ((filling: Filling) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        filling => {
            if (!order) {
                return
            }

            const orderClone = order.clone

            const updateConstruction = (construction: Construction): void => {
                const updateSolidFilling = (solidFilling: SolidFilling, filling: Filling): void => {
                    solidFilling.fillingVendorCode = filling.marking
                    solidFilling.filling = filling

                    if (filling.isMultifunctional) {
                        const otherMultifunctionalFilling = construction.allSolidFillings.find(s => s.filling?.isMultifunctional)
                        solidFilling.glassColor = otherMultifunctionalFilling?.glassColor ?? DEFAULT_GLASS_COLOR.value
                    } else {
                        solidFilling.glassColor = DEFAULT_GLASS_COLOR.value
                    }
                }

                if (isApplyToAll) {
                    construction?.allSolidFillings.forEach(f => {
                        updateSolidFilling(f, filling)
                    })
                    orderClone.addAction(`Задали заполнение для всех заполнений ${filling.marking}`, construction)
                }

                if (!context) {
                    return
                }

                const {product, node: solidFilling} = orderClone.findConstructionAndProductAndNodeByNodeId<SolidFilling>(context.nodeId)
                if (!product || !solidFilling) {
                    return
                }

                updateSolidFilling(solidFilling, filling)
                orderClone.addAction(`Задали заполнение ${filling.marking}`, construction, product, solidFilling)
            }

            if (configContext === ConfigContexts.ConstructionParams) {
                for (const construction of orderClone.constructions) {
                    updateConstruction(construction)
                }
            } else {
                const constructionClone = orderClone.findConstructionById(construction?.id ?? '')
                if (constructionClone) {
                    updateConstruction(constructionClone)
                }
            }
            updateCurrentOrder(orderClone)
        },
        [order, construction?.id, context, configContext, isApplyToAll, fillings, updateCurrentOrder]
    )
}

export const useSetFillingVendorCode = (): ((fillingPart: ISolidFillingPart) => void) => {
    const order = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentContext = useSelector((state: IAppState) => state.builder.currentContext)
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        (fillingPart: ISolidFillingPart) => {
            if (!order || !currentContext) {
                return
            }
            const orderClone = order.clone
            const {node, construction} = orderClone.findConstructionAndProductAndNodeByNodeId<SolidFilling>(currentContext.nodeId)
            const isApplyToAll = localStorage.getItem(FILLING_SELECTOR_APPLY_TO_ALL) === 'true'

            const updateGlassColor = (solidFilling: SolidFilling | null, filling: Filling): void => {
                if (!solidFilling) {
                    return
                }
                if (filling.isMultifunctional) {
                    const otherMultifunctionalFilling = construction?.allSolidFillings.find(s => s.filling?.isMultifunctional)
                    solidFilling.glassColor = otherMultifunctionalFilling?.glassColor ?? DEFAULT_GLASS_COLOR.value
                } else {
                    solidFilling.glassColor = DEFAULT_GLASS_COLOR.value
                }
            }

            const updateSolidFilling = (solidFilling: SolidFilling | null): Filling | undefined => {
                if (!solidFilling || !solidFilling.filling) {
                    return
                }
                const fillingClone = solidFilling.clone()
                const formula = fillingClone.formula
                const frame =
                    fillingPart.type === SolidFillingPartType.Frame ? fillingPart : formula.find(f => f.type === SolidFillingPartType.Frame)
                const newVendorCode = formula
                    .map(f => {
                        if (f.type === SolidFillingPartType.Glass) {
                            const anotherFillingValue = f.tintFilm && f.tintFilm !== '-' ? `${f.value}[${f.tintFilm}]` : f.value
                            const checkedFillingValue =
                                fillingPart.tintFilm && fillingPart.tintFilm !== '-'
                                    ? `${fillingPart.value}[${fillingPart.tintFilm}]`
                                    : fillingPart.value
                            return f.id === fillingPart.id ? checkedFillingValue : anotherFillingValue
                        }
                        const frameOptions = []
                        frame?.hasArgon && frameOptions.push('Ar')
                        frame?.hasPvc && frameOptions.push('PVC')
                        return `${f.id === fillingPart.id ? fillingPart.value : f.value}${frameOptions.join('_')}`
                    })
                    .join('-')
                solidFilling.fillingVendorCode = newVendorCode
                const f = solidFilling.filling.clone()
                const thickness = solidFilling.formula.reduce((sum: number, current: ISolidFillingPart) => {
                    const value = /(\d+)/.exec(current.value)
                    return value ? sum + +value[0] : sum
                }, 0)
                f.id = new Date().valueOf()
                f.marking = newVendorCode
                f.thickness = thickness
                f.name = `Стеклопакет ${thickness} мм`
                f.isMultifunctional = /[тц]/i.test(newVendorCode)
                f.isLowEmission = /[i]/i.test(newVendorCode)
                solidFilling.filling = f
                updateGlassColor(solidFilling, f)
                return f
            }

            const updatedFilling = updateSolidFilling(node)
            if (isApplyToAll) {
                construction?.allSolidFillings.forEach(f => {
                    if (updatedFilling && node && f.filling && node.filling?.group === f.filling?.group) {
                        f.filling = updatedFilling.clone()
                        f.fillingVendorCode = f.filling.marking
                        updateGlassColor(f, updatedFilling)
                    }
                })
            }
            updateCurrentOrder(orderClone)
        },
        [order, currentContext, updateCurrentOrder]
    )
}

export const useSetHandlePositionType = (
    order: Order | null,
    context: IContext | null
): ((handlePositionType: HandlePositionTypes) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()
    const setCurrentContext = useSetCurrentContext()

    return useCallback(
        handlePositionType => {
            if (!order || !(context instanceof Handle) || !context.parentNodeId) {
                return
            }

            const orderClone = order.clone
            const {
                construction,
                product,
                node: frameFilling
            } = orderClone.findConstructionAndProductAndNodeByNodeId<FrameFilling>(context.parentNodeId)

            if (frameFilling) {
                orderClone.addAction(`Задалии положение ручки ${handlePositionType}`, construction, product, context)
                frameFilling.handlePositionType = handlePositionType

                const {node: updatedFrameFilling} = updateCurrentOrder(orderClone).findConstructionAndProductAndNodeByNodeId<FrameFilling>(
                    context.parentNodeId
                )

                updatedFrameFilling && setCurrentContext(new Handle(updatedFrameFilling))
            }
        },
        [order, context, updateCurrentOrder, setCurrentContext]
    )
}

export const useSetHandle = (
    order: Order | null,
    construction: Construction | null,
    context: IContext | null
): ((handleType: HandleTypes, handleColor: HandleColors) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        (handleType, handleColor) => {
            if (!order || !construction || !context) {
                return
            }

            const orderClone = order.clone
            const constructionClone = orderClone.findConstructionById(construction.id)

            const applyToConstruction = (construction: Construction): void => {
                construction.products.forEach(product => {
                    product.frame.allFrameFillings.forEach(frame => {
                        frame.handleType = handleType
                        frame.handleColor = handleColor
                    })
                })
            }

            if (constructionClone) {
                applyToConstruction(constructionClone)

                orderClone.addAction(`Задали ручки ${handleType} цвета ${handleColor}`, constructionClone)
                updateCurrentOrder(orderClone)
            }
        },
        [order, construction, context, updateCurrentOrder]
    )
}

export const useSetGlassColor = (
    order: Order | null,
    construction: Construction | null,
    configContext: ConfigContexts | null,
    context: IContext | null
): ((color: string) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        color => {
            if (!order || !construction || !context) {
                return
            }

            const orderClone = order.clone
            const updateConstruction = (construction: Construction): void => {
                construction.allSolidFillings.filter(s => s.filling?.isMultifunctional).forEach(s => (s.glassColor = color))
            }

            if (configContext === ConfigContexts.ConstructionParams) {
                for (const construction of orderClone.constructions) {
                    updateConstruction(construction)
                }
            } else {
                const {construction: constructionClone} = orderClone.findConstructionAndProductAndNodeByNodeId<SolidFilling>(context.nodeId)
                if (constructionClone) {
                    updateConstruction(constructionClone)
                    orderClone.addAction(`Задали цвета мультифункциональных окон ${color}`, constructionClone)
                }
            }
            updateCurrentOrder(orderClone)
        },
        [order, construction, context, updateCurrentOrder]
    )
}

export const useSetConnectorParams = (
    order: Order | null,
    construction: Construction | null,
    context: IContext | null,
    connectingSide: Sides | null,
    hostProduct: Product | null
): ((params: ConnectorParameters) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()
    const setConnectingSide = useSetConnectingSide()
    const setHostProduct = useSetHostProduct()
    const setConfigContext = useSetConfigContext()

    return useCallback(
        async params => {
            if (!order || !construction) {
                return
            }

            const orderClone = order.clone

            if (!orderClone) {
                return
            }

            if (context instanceof Connector) {
                const {construction, product, node} = orderClone.findConstructionAndProductAndNodeByNodeId<Connector>(context.nodeId)

                if (construction && node) {
                    orderClone.addAction(
                        `Обновили конектор/расширитель с ${node.profileVendorCode} на ${params.marking}(${params.comment})`,
                        construction,
                        product,
                        node
                    )
                    construction.updateConnector(node.nodeId, params, construction.connectorsParameters)
                    updateCurrentOrder(orderClone)
                }
            } else if (connectingSide && hostProduct) {
                const constructionClone = orderClone.findConstructionById(construction.id)
                const product = constructionClone?.findProductByProductGuid(hostProduct.productGuid)

                if (constructionClone && product) {
                    constructionClone.addConnector(product, connectingSide, params)
                    await getConstructionWithDefaultParameters(constructionClone, orderClone.city?.id)
                    orderClone.addAction(
                        `Добавили конектор/расширитель ${params.marking}(${params.comment}) to the ${connectingSide}`,
                        constructionClone,
                        product
                    )
                    updateCurrentOrder(orderClone)

                    setConnectingSide(null)
                    setHostProduct(null)
                    setConfigContext(null)
                }
            }
        },
        [order, construction, context, connectingSide, hostProduct, updateCurrentOrder, setConnectingSide, setHostProduct, setConfigContext]
    )
}

export const useRemoveProduct = (order: Order | null, construction: Construction | null, product: Product | null): (() => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(() => {
        if (!order || !construction || !product) {
            return
        }
        const orderClone = order.clone
        const constructionClone = orderClone.findConstructionById(construction.id)
        const productClone = constructionClone?.findProductByProductGuid(product.productGuid)

        if (!constructionClone || !productClone) {
            return
        }

        constructionClone.removeProduct(productClone)
        orderClone.addAction('Удалили изделие', constructionClone, productClone)
        updateCurrentOrder(orderClone)
    }, [order, construction, product, updateCurrentOrder])
}

export const useSetProductOffset = (
    order: Order | null,
    construction: Construction | null,
    product: Product | null
): ((offset: number) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        offset => {
            if (!order || !construction || !product) {
                return
            }
            const orderClone = order.clone
            const constructionClone = orderClone.findConstructionById(construction.id)
            const productClone = constructionClone?.findProductByProductGuid(product.productGuid)

            if (constructionClone && productClone) {
                constructionClone.setProductOffset(productClone, offset)

                orderClone.addAction(`Сместили изделие на ${offset}`, constructionClone, productClone)
                updateCurrentOrder(orderClone)
            }
        },
        [order, construction, product, updateCurrentOrder]
    )
}

export const useSetMarking = (order: Order | null, context: IContext | null): ((params: GeometryParameters) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        params => {
            if (!order || !(context instanceof Beam || context instanceof FrameFilling)) {
                return
            }

            const orderClone = order.clone
            if (context instanceof Beam) {
                const {construction, product, node} = orderClone.findConstructionAndProductAndNodeByNodeId<Beam>(context.nodeId)
                if (!construction || !product || !node) {
                    return
                }

                node.modelPart = params.modelPart
                node.profileVendorCode = params.marking
                orderClone.addAction(`Сменили артикул с ${context.profileVendorCode} на ${node.profileVendorCode}`, construction, product, node)
            } else {
                const {construction, product, node} = orderClone.findConstructionAndProductAndNodeByNodeId<FrameFilling>(context.nodeId)
                if (!construction || !product || !node) {
                    return
                }

                node.frameBeams.forEach(b => (b.profileVendorCode = params.marking))
                orderClone.addAction(
                    `Сменили артикул с ${context.frameBeams[0].profileVendorCode} на ${node.frameBeams[0].profileVendorCode}`,
                    construction,
                    product,
                    node
                )
            }

            updateCurrentOrder(orderClone)
        },
        [order, context, updateCurrentOrder]
    )
}

export const useUpdateMoskitka = (currentOrder: Order | null, currentContext: IContext | null): ((moskitka: Moskitka) => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()

    return useCallback(
        (moskitka: Moskitka): void => {
            if (!currentOrder || !currentContext) {
                return
            }
            const order = currentOrder.clone
            const {construction, product, node} = order.findConstructionAndProductAndNodeByNodeId<FrameFilling>(currentContext.nodeId)

            if (!construction || !product || !node) {
                return
            }

            construction.setMoskitka(node.nodeId, moskitka)
            const info = `${moskitka.catproof ? 'антикошка' : 'обычная'} ${getMoskitkaColorName(moskitka.color)} ${
                moskitka.set ? 'установлена' : 'не установлена'
            }`
            order.addAction(`Обновили москитку на ${info.toLocaleLowerCase().replace('\n', '')}`, construction, product, currentContext)
            updateCurrentOrder(order)
        },
        [currentOrder, currentContext, updateCurrentOrder]
    )
}

export const useRemoveConnector = (order: Order | null, context: IContext | null): (() => void) => {
    const updateCurrentOrder = useUpdateCurrentOrder()
    const setCurrentContext = useSetCurrentContext()
    const setConfigContext = useSetConfigContext()

    return useCallback(() => {
        if (!order || !(context instanceof Connector)) {
            return
        }
        const orderClone = order.clone
        const {construction, product, node} = orderClone.findConstructionAndProductAndNodeByNodeId<Connector>(context.nodeId)

        if (!construction || !product || !node) {
            return
        }

        construction.removeConnector(node)
        orderClone.addAction('Удалили конектор', construction, product, node)
        const newContext = updateCurrentOrder(orderClone).findConstructionById(construction.id)?.findNodeById(node.connectedBeamGuid)

        if (newContext instanceof Connector) {
            setConfigContext(ConfigContexts.Connector)
            setCurrentContext(newContext)
        } else {
            setConfigContext(null)
            setCurrentContext(null)
        }
    }, [order, context, updateCurrentOrder, setCurrentContext, setConfigContext])
}

export const useCurrentContextUserParameterConfigs = (): UserParameterConfig[] | undefined | null => {
    const userParameterConfigsMap = useSelector((state: IAppState) => state.builder.userParameterConfigsMap)
    const currentContext = useUserParametersContext()
    const guid = currentContext ? getGuidByContext(currentContext) : null

    if (!guid) {
        return null
    }

    return userParameterConfigsMap[guid]
}
