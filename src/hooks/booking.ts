import {BookingOptions} from '../model/framer/booking-options'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {useCallback, useEffect} from 'react'
import {
    book,
    calcShipping,
    loadBookingDates,
    loadBookingOptions,
    setBookingDates,
    setBookingOptions,
    setShippingResponse
} from '../redux/booking/actions'
import {Order} from '../model/framer/order'
import {ShippingTypes} from '../model/framer/shipping-types'
import {Address} from '../model/address'
import {ContactInfo} from '../model/framer/contact-info'
import {transferToProduction} from '../redux/orders/actions'
import {IShippingResponse} from '../model/framer/i-shipping-response'
import {Coordinates} from '../model/coordinates'
import {ISBookingDates} from '../model/framer/i-booking-dates'

export const useBookingOptions = (orderId: number): BookingOptions | null => {
    const bookingOptions: BookingOptions | null = useSelector((state: IAppState) => state.booking.bookingOptions)

    const dispatch = useDispatch()
    const loadBookingOptionsAction = useCallback((orderId: number) => dispatch(loadBookingOptions(orderId)), [dispatch])
    const setBookingOptionsAction = useCallback(
        (bookingOptions: BookingOptions | null) => {
            dispatch(setBookingOptions(bookingOptions))
        },
        [dispatch]
    )

    useEffect(() => {
        loadBookingOptionsAction(orderId)

        return () => {
            setBookingOptionsAction(null)
        }
    }, [orderId, loadBookingOptionsAction, setBookingOptionsAction])

    return bookingOptions
}

export const useBook = (): ((
    order: Order,
    date: string,
    warehouseId: number,
    shippingType: ShippingTypes,
    address: Address | null,
    contactPerson: ContactInfo | null
) => void) => {
    const dispatch = useDispatch()
    return useCallback(
        (
            order: Order,
            date: string,
            warehouseId: number,
            shippingType: ShippingTypes,
            address: Address | null,
            contactPerson: ContactInfo | null
        ) => dispatch(book(order, date, warehouseId, shippingType, address, contactPerson)),
        [dispatch]
    )
}

export const useTransferToProductionAction = (): ((
    order: Order,
    warehouseId: number,
    shippingType: ShippingTypes,
    address: Address | null,
    contactPerson: ContactInfo | null,
    shippingComment: string | null,
    availableBonusScore: number,
    isFramerPointsShouldBeUsed: boolean
) => void) => {
    const dispatch = useDispatch()
    return useCallback(
        (
            order: Order,
            warehouseId: number,
            shippingType: ShippingTypes,
            address: Address | null,
            contactPerson: ContactInfo | null,
            shippingComment: string | null,
            availableBonusScore: number,
            isFramerPointsShouldBeUsed: boolean
        ) =>
            dispatch(
                transferToProduction(
                    order,
                    warehouseId,
                    shippingType,
                    address,
                    contactPerson,
                    shippingComment,
                    availableBonusScore,
                    isFramerPointsShouldBeUsed
                )
            ),
        [dispatch]
    )
}

export const useShippingResponse = (
    order: Order,
    bookingOptions: BookingOptions,
    address: Address,
    shippingFloor: number,
    oversizedProducts: number
): IShippingResponse | null => {
    const shippingResponse = useSelector((state: IAppState) => state.booking.shippingResponse)

    const dispatch = useDispatch()
    const setShippingResponseAction = useCallback(
        (shippingResponse: IShippingResponse | null) => {
            dispatch(setShippingResponse(shippingResponse))
        },
        [dispatch]
    )
    const calcShippingAction = useCallback(
        (orderId: number, destination: Coordinates, address: Address, shippingFloor: number, oversizedProducts: number) => {
            dispatch(calcShipping(orderId, destination, address, shippingFloor, oversizedProducts))
        },
        [dispatch]
    )

    useEffect(() => {
        if (bookingOptions && address.coordinates && address.building.length > 0) {
            calcShippingAction(order.id, address.coordinates, address, shippingFloor, oversizedProducts)
            return
        }
        setShippingResponseAction(null)
    }, [order, address, bookingOptions, calcShippingAction, setShippingResponseAction, shippingFloor, oversizedProducts])

    return shippingResponse
}

export const useBookingDates = (orderId: number): ISBookingDates | null => {
    const bookingDates = useSelector((state: IAppState) => state.booking.bookingDates)

    const dispatch = useDispatch()
    const loadBookingDatesAction = useCallback((orderId: number) => dispatch(loadBookingDates(orderId)), [dispatch])
    const setBookingDatesAction = useCallback(
        (bookingDates: ISBookingDates | null) => {
            dispatch(setBookingDates(bookingDates))
        },
        [dispatch]
    )

    useEffect(() => {
        loadBookingDatesAction(orderId)

        return () => {
            setBookingDatesAction(null)
        }
    }, [orderId, loadBookingDatesAction, setBookingDatesAction])

    return bookingDates
}
