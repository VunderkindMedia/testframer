import {useState, useEffect, useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {loadPartner, loadEmployees, loadAffiliates, loadNotifications} from '../redux/account/actions'
import {profileTabs, ProfileTabIds} from '../components/common/profile-page/tabs'
import {Employee} from '../model/framer/employee'
import {Affiliate} from '../model/framer/affiliate'
import {usePermissions} from './permissions'
import {Notification} from '../model/framer/notifications/notification'
import {News} from '../model/framer/news'
import {loadNews} from '../redux/cms/actions'

export const useLoadProfileSettingsAction = (): void => {
    const dispatch = useDispatch()
    const loadProfileSettingsAction = useCallback(() => dispatch(loadPartner()), [dispatch])

    useEffect(() => {
        loadProfileSettingsAction()
    }, [loadProfileSettingsAction])
}

export const useSetActiveTab = (): string => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const [activeTab, setActiveTab] = useState<string>(profileTabs.defaultTab)

    useEffect(() => {
        const pageParams = new URLSearchParams(search)
        const activeTabParam = pageParams.get('activeTab')
        const availableParams: string[] = Object.values(ProfileTabIds)

        setActiveTab(activeTabParam !== null && availableParams.includes(activeTabParam) ? activeTabParam : profileTabs.defaultTab)
    }, [search])

    return activeTab
}

export const useEmployees = (): Employee[] => {
    const employees = useSelector((state: IAppState) => state.account.employees)

    const dispatch = useDispatch()
    const permission = usePermissions()

    useEffect(() => {
        if (permission.isBossManager || permission.isChiefManager) {
            dispatch(loadEmployees())
        }
    }, [permission.isBossManager, permission.isChiefManager, dispatch])

    return employees || []
}

export const useAffiliates = (): Affiliate[] => {
    const affiliates = useSelector((state: IAppState) => state.account.affiliates)

    const dispatch = useDispatch()
    const permission = usePermissions()

    useEffect(() => {
        permission.isBossManager && dispatch(loadAffiliates())
    }, [permission.isBossManager, dispatch])

    return affiliates || []
}

export const useNotifications = (): Notification[] => {
    const notifications = useSelector((state: IAppState) => state.account.notifications)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadNotifications())
    }, [dispatch])

    return notifications
}

export const useNews = (): News[] => {
    const news = useSelector((state: IAppState) => state.cms.news)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadNews())
    }, [dispatch])

    return news
}
