import {useEffect, useCallback, useRef} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {IAppState} from '../redux/root-reducer'
import {loadPaymentHistory, loadPaymentsCsv} from '../redux/payments/actions'
import {PaymentState} from '../model/framer/payment-state'
import {getPaymentsFilterParams, getPaymentsFilter} from '../helpers/payments'
import {NAV_PAYMENT} from '../constants/navigation'
import {useReplaceTo} from './router'

export const useLoadPayments = (): PaymentState[] => {
    const payments = useSelector((state: IAppState) => state.payments.payments)
    const search = useSelector((state: IAppState) => state.router.location.search)
    const paymentsOnPage = useSelector((state: IAppState) => state.payments.paymentsOnPage)
    const pathname = useSelector((state: IAppState) => state.router.location.pathname)

    const dispatch = useDispatch()
    const replaceTo = useReplaceTo()
    const loadPayments = useCallback((filter: string) => dispatch(loadPaymentHistory(filter)), [dispatch])

    const basePathname = useRef(pathname)

    useEffect(() => {
        search && loadPayments(search)
    }, [loadPayments, search])

    useEffect(() => {
        if (basePathname.current !== pathname) {
            return
        }

        const params = {
            ...getPaymentsFilterParams(search),
            limit: paymentsOnPage
        }
        const filter = getPaymentsFilter(params)

        if (search !== filter) {
            replaceTo(`${NAV_PAYMENT}${filter}`)
        }
    }, [search, paymentsOnPage, pathname, replaceTo])

    return payments
}

export const useLoadPaymentsCsv = (): (() => void) => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const paymentsTotalCount = useSelector((state: IAppState) => state.payments.paymentsTotalCount)
    const dispatch = useDispatch()

    const downloadPayments = useCallback((filter: string) => dispatch(loadPaymentsCsv(filter)), [dispatch])

    return useCallback(() => {
        const params = {
            ...getPaymentsFilterParams(search),
            limit: paymentsTotalCount,
            offset: 0
        }
        const filter = getPaymentsFilter(params)
        downloadPayments(filter)
    }, [search, paymentsTotalCount, downloadPayments])
}
