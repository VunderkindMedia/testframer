import {useDispatch, useSelector} from 'react-redux'
import {useCallback} from 'react'
import {
    setConnectingSide,
    setCurrentTemplate,
    setHostProduct,
    setTemplateFilter,
    resetTemplateSelector
} from '../redux/template-selector/actions'
import {Template} from '../model/template'
import {Product} from '../model/framer/product'
import {Sides} from '../model/sides'
import {Construction} from '../model/framer/construction'
import {IAppState} from '../redux/root-reducer'
import {runLongNetworkOperation} from '../redux/request/actions'
import {api} from '../api'
import {Order} from '../model/framer/order'
import {createOrder, setCurrentOrder, setCurrentConstruction} from '../redux/orders/actions'
import {useAddNewConstruction, useUpdateCurrentOrder} from './orders'
import {Profile} from '../model/framer/profile'
import {getClone} from '../helpers/json'
import {requiredFurnitureMaco} from '../helpers/furniture'
import {FURNITURE_MACO} from '../constants/furniture-templates'
import {DEFAULT_ENERGY_PROFILE_ID} from '../constants/default-energy-parameters'
import {LAST_ENERGY_CONSTRUCTION_ID} from '../constants/local-storage'
import {push} from 'connected-react-router'
import {useGoTo, useOrdersPageLink, useReplaceTo} from './router'
import {getOrderPageUrl, getConstructionWithDefaultParameters} from '../helpers/orders'
import {getTabs} from '../helpers/tabs'
import {ORDER_TAB_ID, NAV_ORDER} from '../constants/navigation'
import {CONSTRUCTION_TEMPLATE} from '../components/common/template-selector/templates'
import {getTemplateWithEnergyParameters, isEnergyConstruction} from '../helpers/templates'

export const useShowTemplateSelector = (): ((template?: Template | null) => void) => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const dispatch = useDispatch()

    return useCallback(
        (template?: Template | null) => {
            if (template && !template.templates.length) {
                return
            }
            const pageParams = new URLSearchParams(search)

            const orderId = pageParams.get('orderId')
            const tabs = pageParams.get('tabs')
            const activeTab = pageParams.get('activeTab')
            const category = pageParams.get('category') ?? 'pvh'
            const steps = pageParams.has('showTemplate') ? pageParams.get('steps') : null
            const params = []

            orderId && params.push(`orderId=${orderId}`)
            tabs && params.push(`tabs=${tabs}`)
            activeTab && params.push(`activeTab=${activeTab}`)
            params.push('showTemplate')
            category && params.push(`category=${category}`)

            if (template) {
                const currentStep = template.depth === 1 ? template.id : `${steps ? `${steps},` : ''}${template.id}`
                params.push(`steps=${currentStep}`)
            }
            dispatch(push(`/order?${params.join('&')}`))
        },
        [search, dispatch]
    )
}

export const useHideTemplateSelector = (): (() => void) => {
    const search = useSelector((state: IAppState) => state.router.location.search)
    const dispatch = useDispatch()

    return useCallback(() => {
        const pageParams = new URLSearchParams(search)

        const orderId = pageParams.get('orderId')
        const tabs = pageParams.get('tabs')
        const activeTab = pageParams.get('activeTab')
        const params = []

        orderId && params.push(`orderId=${orderId}`)
        tabs && params.push(`tabs=${tabs}`)
        activeTab && params.push(`activeTab=${activeTab}`)

        dispatch(resetTemplateSelector())
        dispatch(push(`/order?${params.join('&')}`))
    }, [search, dispatch])
}

export const useSelectTemplateCategory = (): ((category: string) => void) => {
    const dispatch = useDispatch()
    const search = useSelector((state: IAppState) => state.router.location.search)

    return useCallback(
        (category: string) => {
            const pageParams = new URLSearchParams(search)

            const orderId = pageParams.get('orderId')
            const tabs = pageParams.get('tabs')
            const activeTab = pageParams.get('activeTab')
            const params = []

            orderId && params.push(`orderId=${orderId}`)
            tabs && params.push(`tabs=${tabs}`)
            activeTab && params.push(`activeTab=${activeTab}`)
            params.push('showTemplate')
            params.push(`category=${category}`)

            dispatch(push(`/order?${params.join('&')}`))
        },
        [dispatch, search]
    )
}

export const useSetCurrentTemplate = (): ((template: Template | null) => void) => {
    const dispatch = useDispatch()
    return useCallback((template: Template | null) => dispatch(setCurrentTemplate(template)), [dispatch])
}

export const useSetHostProduct = (): ((product: Product | null) => void) => {
    const dispatch = useDispatch()
    return useCallback((product: Product | null) => dispatch(setHostProduct(product)), [dispatch])
}

export const useSetConnectingSide = (): ((side: Sides | null) => void) => {
    const dispatch = useDispatch()
    return useCallback((side: Sides | null) => dispatch(setConnectingSide(side)), [dispatch])
}

export const useSetTemplateFilter = (): ((filter: (construction: Construction) => boolean) => void) => {
    const dispatch = useDispatch()
    return useCallback((filter: (construction: Construction) => boolean) => dispatch(setTemplateFilter(filter)), [dispatch])
}

const loadConstructionDetails = async (template: Template, category: string): Promise<void> => {
    if (!template.data) {
        return
    }
    for (const productObj of template.data.products) {
        const {profiles, furniture} = await api.design.getProfilesAndFurniture(productObj.constructionTypeId, productObj.productionTypeId)

        const profileItem =
            profiles.find(p => (isEnergyConstruction(category) ? p.id === DEFAULT_ENERGY_PROFILE_ID : p.isDefault)) ?? profiles[0]
        const furnitureItem = furniture.find(f => f.isDefault) ?? furniture[0]

        const fillings = await api.design.getFillings(profileItem.id)

        productObj._availableProfiles = profiles
        productObj._availableFurniture = furniture
        productObj._availableFillings = fillings

        productObj.profile = profileItem
        productObj.furniture = requiredFurnitureMaco(productObj) ? FURNITURE_MACO : furnitureItem

        const {usedModelParts} = Product.parse(productObj)
        const checkProfileModelParts = (profile: Profile): boolean =>
            usedModelParts.every(uPart => profile.parts.some(part => part.modelPart === uPart))

        if (!checkProfileModelParts(profileItem)) {
            const altProfileItem = profiles.find(p => checkProfileModelParts(p))
            altProfileItem && (productObj.profile = altProfileItem)
        }
    }
}

export const useSelectTemplate = (): ((template: Template | null, category: string) => void) => {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const currentProduct = useSelector((state: IAppState) => state.orders.currentProduct)
    const connectorsParameters = useSelector((state: IAppState) => state.orders.currentConstruction?.connectorsParameters ?? [])
    const templateFilter = useSelector((state: IAppState) => state.templateSelector.templateFilter)
    const connectingSide = useSelector((state: IAppState) => state.templateSelector.connectingSide)
    const partner = useSelector((state: IAppState) => state.account.partner)

    const dispatch = useDispatch()
    const updateCurrentOrder = useUpdateCurrentOrder()
    const addNewConstruction = useAddNewConstruction()
    const hideTemplateSelector = useHideTemplateSelector()

    return useCallback(
        async (template: Template | null, category: string) => {
            await runLongNetworkOperation(async () => {
                if (!template) {
                    dispatch(setCurrentTemplate(null))
                    return
                }

                if (template.templates.length > 0) {
                    dispatch(setCurrentTemplate(template))
                    return
                }

                if (!template.data) {
                    dispatch(setCurrentTemplate(template))
                    return
                }

                template.data.connectorsParameters = getClone(connectorsParameters)
                await loadConstructionDetails(template, category)

                const isEnergyCategory = isEnergyConstruction(category)
                const construction = Construction.parse(isEnergyCategory ? getTemplateWithEnergyParameters(template.data) : template.data)
                construction.connectorsParameters = connectorsParameters
                construction.updateProductsGuids()

                if (currentOrder) {
                    if (templateFilter(construction) && connectingSide && currentConstruction && currentProduct) {
                        const order = currentOrder.clone
                        const hostConstruction = order.findConstructionById(currentConstruction.id)
                        const hostProduct = order.findProductByProductGuid(currentProduct.productGuid)

                        if (!hostConstruction || !hostProduct) {
                            return
                        }

                        const fillingTemplate = hostProduct.frame.allFrameFillings[0]
                        const {handleType, handleColor} = fillingTemplate
                        construction.allFrameFillings.map(f => {
                            f.handleColor = handleColor
                            f.handleType = handleType
                        })

                        hostConstruction.connectorsParameters = connectorsParameters
                        construction.products[0].setInsideLamination(hostConstruction.products[0].insideLamination)
                        construction.products[0].setOutsideLamination(hostConstruction.products[0].outsideLamination)
                        hostConstruction.connectProduct(hostProduct, construction.products[0], connectingSide)
                        order.addAction(
                            `Добавили изделие ${construction.products[0].productGuid} to the ${connectingSide}`,
                            hostConstruction,
                            hostProduct
                        )
                        await getConstructionWithDefaultParameters(hostConstruction, currentOrder.city?.id)
                        updateCurrentOrder(order)
                        hideTemplateSelector()
                    } else {
                        construction.setDefaultHandles(partner?.factoryId)
                        await getConstructionWithDefaultParameters(construction, currentOrder.city?.id)
                        addNewConstruction(construction)
                    }
                } else {
                    construction.setDefaultHandles(partner?.factoryId)
                    await getConstructionWithDefaultParameters(construction, partner?.filial?.cityId)
                    const draftOrder = Order.Builder.withConstructions([construction]).build()
                    if (isEnergyCategory) {
                        localStorage.setItem(LAST_ENERGY_CONSTRUCTION_ID, construction.id)
                    }
                    dispatch(createOrder(draftOrder, true))
                }
            })(dispatch)
        },
        [
            dispatch,
            updateCurrentOrder,
            addNewConstruction,
            hideTemplateSelector,
            currentOrder,
            currentConstruction,
            currentProduct,
            connectorsParameters,
            connectingSide,
            templateFilter,
            partner
        ]
    )
}

export const useCreateDemoOrder = (): (() => void) => {
    const dispatch = useDispatch()

    return useCallback(async () => {
        await runLongNetworkOperation(async () => {
            const template = CONSTRUCTION_TEMPLATE

            if (!template || !template.data) {
                return
            }

            await loadConstructionDetails(template, 'pvh')
            const construction = Construction.parse(template.data)
            construction.updateProductsGuids()
            const constructionWithParams = await getConstructionWithDefaultParameters(construction)
            constructionWithParams.setDefaultHandles()
            const draftOrder = Order.Builder.withConstructions([constructionWithParams]).build()
            dispatch(createOrder(draftOrder, true))
        })(dispatch)
    }, [dispatch])
}

export const useGoToPreviousStep = (): (() => void) => {
    const currentOrder = useSelector((state: IAppState) => state.orders.currentOrder)
    const currentConstruction = useSelector((state: IAppState) => state.orders.currentConstruction)
    const search = useSelector((state: IAppState) => state.router.location.search)
    const ordersPageLink = useOrdersPageLink()
    const replaceTo = useReplaceTo()
    const goTo = useGoTo()

    return useCallback(() => {
        const pageParams = new URLSearchParams(search)

        const activeTabId = currentConstruction?.id ?? ORDER_TAB_ID
        if (pageParams.has('steps')) {
            const steps = pageParams.get('steps')
            let stepsArr: string[] = []

            if (steps) {
                stepsArr = steps.split(',')
                stepsArr.splice(-1, 1)
            }
            const orderPageUrl = currentOrder ? `${getOrderPageUrl(currentOrder.id, getTabs(search), activeTabId)}&` : '/order?'
            replaceTo(`${orderPageUrl}showTemplate${stepsArr.length ? `&steps=${stepsArr.join(',')}` : ''}`)
        } else if (currentOrder) {
            replaceTo(getOrderPageUrl(currentOrder.id, getTabs(search), activeTabId))
        } else {
            goTo(ordersPageLink)
        }
    }, [search, currentOrder, currentConstruction, goTo, replaceTo, ordersPageLink])
}

export const useCreateTestOrder = (): (() => void) => {
    const dispatch = useDispatch()
    return useCallback(async () => {
        const template = CONSTRUCTION_TEMPLATE

        if (!template || !template.data) {
            return
        }

        await loadConstructionDetails(template, 'pvh')
        const construction = Construction.parse(template.data)
        construction.updateProductsGuids()
        const constructionWithParams = await getConstructionWithDefaultParameters(construction)
        constructionWithParams.setDefaultHandles()
        const draftOrder = Order.Builder.withConstructions([constructionWithParams]).build()
        draftOrder.id = 1
        draftOrder.orderNumber = '1'
        draftOrder.needRecalc = true

        dispatch(setCurrentOrder(draftOrder))
        dispatch(setCurrentConstruction(draftOrder.constructions[0]))
        dispatch(resetTemplateSelector())
        dispatch(
            push(`${NAV_ORDER}?orderId=${draftOrder.id}&tabs=${draftOrder.constructions[0].id}&activeTab=${draftOrder.constructions[0].id}`)
        )
    }, [dispatch])
}
