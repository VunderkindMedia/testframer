import {useCallback, useEffect, useRef, useContext} from 'react'
import {useDispatch, useSelector, useStore} from 'react-redux'
import {isMobileBrowser, isPhone} from '../helpers/mobile'
import {getTableItemsOnPage} from '../helpers/table'
import {setOrdersOnPage} from '../redux/orders/actions'
import {setClientsOnPage} from '../redux/clients/actions'
import {setMountingsOnPage} from '../redux/mountings/actions'
import {setPaymentsOnPage} from '../redux/payments/actions'
import {IAppState} from '../redux/root-reducer'
import {setBrowserWindowSize} from '../redux/browser/actions'
import Timer = NodeJS.Timer
import {Size} from '../model/size'
import {CarrotQuestManager} from '../carrot-quest-manager'
import {updateAppVersion} from '../redux/settings/actions'
import {setOnboardingStep} from '../redux/builder/actions'
import {AppContext, IAppContext} from '../app'
import {approximatelyEqual} from '../helpers/number'

export const useUpdateScreenSizeDependencies = (): void => {
    const resizeTimerRef = useRef<Timer | null>(null)
    const windowSize = useSelector((state: IAppState) => state.browser.windowSize)
    const isLegalDealer = useSelector((state: IAppState) => state.account.currentDealer?.type === 0)
    const dispatch = useDispatch()

    const updateScreenSizeDependencies = useCallback(() => {
        if (isMobileBrowser()) {
            const vh = window.innerHeight * 0.01
            document.documentElement.style.setProperty('--vh', `${vh}px`)
        }

        const TABLE_ROW_HEIGHT = 54
        const TABLE_HEADER_HEIGHT = 42

        const ORDERS_PAGE_TOP_SPACE_HEIGHT = 110
        const ORDERS_PAGE_BOTTOM_SPACE_HEIGHT = 54

        const CLIENTS_PAGE_TOP_SPACE_HEIGHT = 110
        const CLIENTS_PAGE_BOTTOM_SPACE_HEIGHT = 54

        const MOUNTINGS_PAGE_TOP_SPACE_HEIGHT = 110
        const MOUNTINGS_PAGE_BOTTOM_SPACE_HEIGHT = 54

        const PAYMENTS_PAGE_TOP_SPACE_HEIGHT_LEGAL = 436
        const PAYMENTS_PAGE_TOP_SPACE_HEIGHT = 548
        const PAYMENTS_PAGE_BOTTOM_SPACE_HEIGHT = 54

        const ordersOnPage = isPhone()
            ? 10
            : getTableItemsOnPage(
                  window.innerHeight - ORDERS_PAGE_TOP_SPACE_HEIGHT - ORDERS_PAGE_BOTTOM_SPACE_HEIGHT,
                  TABLE_HEADER_HEIGHT,
                  TABLE_ROW_HEIGHT
              )

        const clientsOnPage = isPhone()
            ? 10
            : getTableItemsOnPage(
                  window.innerHeight - CLIENTS_PAGE_TOP_SPACE_HEIGHT - CLIENTS_PAGE_BOTTOM_SPACE_HEIGHT,
                  TABLE_HEADER_HEIGHT,
                  TABLE_ROW_HEIGHT
              )

        const mountingsOnPage = isPhone()
            ? 10
            : getTableItemsOnPage(
                  window.innerHeight - MOUNTINGS_PAGE_TOP_SPACE_HEIGHT - MOUNTINGS_PAGE_BOTTOM_SPACE_HEIGHT,
                  TABLE_HEADER_HEIGHT,
                  TABLE_ROW_HEIGHT
              )

        const paymentsOnPage = isPhone()
            ? 10
            : getTableItemsOnPage(
                  window.innerHeight -
                      (isLegalDealer ? PAYMENTS_PAGE_TOP_SPACE_HEIGHT_LEGAL : PAYMENTS_PAGE_TOP_SPACE_HEIGHT) -
                      PAYMENTS_PAGE_BOTTOM_SPACE_HEIGHT,
                  TABLE_HEADER_HEIGHT,
                  TABLE_ROW_HEIGHT
              )

        dispatch(setOrdersOnPage(ordersOnPage))
        dispatch(setClientsOnPage(clientsOnPage))
        dispatch(setMountingsOnPage(mountingsOnPage))
        dispatch(setPaymentsOnPage(paymentsOnPage))
    }, [dispatch, isLegalDealer])

    const onResize = useCallback((): void => {
        const newWindowSize = new Size(window.innerWidth, window.innerHeight)

        const isVisibleKeyboard =
            isPhone() && approximatelyEqual(newWindowSize.width, windowSize.width, 1) && windowSize.height - newWindowSize.height > 200

        if (isVisibleKeyboard) {
            return
        }

        resizeTimerRef.current && clearTimeout(resizeTimerRef.current)
        resizeTimerRef.current = setTimeout(() => {
            if (!newWindowSize.equals(windowSize)) {
                dispatch(setBrowserWindowSize(newWindowSize))
            }

            updateScreenSizeDependencies()
        }, 100)
    }, [windowSize, dispatch, updateScreenSizeDependencies])

    useEffect(() => {
        window.addEventListener('resize', onResize)
        window.addEventListener('orientationchange', onResize)

        return () => {
            window.removeEventListener('resize', onResize)
            window.removeEventListener('orientationchange', onResize)
        }
    }, [onResize])

    useEffect(() => {
        updateScreenSizeDependencies()
        return () => {
            resizeTimerRef.current && clearTimeout(resizeTimerRef.current)
        }
    }, [updateScreenSizeDependencies])
}

export const useInitCarrotQuest = (): void => {
    const store = useStore()
    const token = useSelector((state: IAppState) => state.account.token)
    const partner = useSelector((state: IAppState) => state.account.partner)

    useEffect(() => {
        document.body.classList.toggle('authorized', !!token)
    }, [token])

    useEffect(() => {
        if (!partner) {
            return
        }

        CarrotQuestManager.auth(partner.userId, partner.userCQHash)
        CarrotQuestManager.defaultIdentify()
    }, [partner, store])
}

export const useUpdateAppVersion = (): void => {
    const latestAppVersion = useSelector((state: IAppState) => state.settings.latestAppVersion)
    const dispatch = useDispatch()

    const checkVersion = useCallback(() => {
        if (latestAppVersion) {
            dispatch(updateAppVersion(latestAppVersion))
        }
    }, [dispatch, latestAppVersion])

    useEffect(() => {
        checkVersion()
    }, [checkVersion])
}

export const useAppContext = (): IAppContext => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return useContext(AppContext)
}

export const useShowOnboarding = (): boolean => {
    const onboarding = useSelector((state: IAppState) => state.builder.onboarding)
    const isLoggedIn = useSelector((state: IAppState) => !!state.account.token)
    return isLoggedIn && onboarding !== undefined
}

export const useSetOnboardingStep = (): ((step: number | null) => void) => {
    const dispatch = useDispatch()

    return useCallback(
        (step: number | null) => {
            dispatch(setOnboardingStep(step))
        },
        [dispatch]
    )
}
