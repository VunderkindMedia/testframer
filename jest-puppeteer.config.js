module.exports = {
    server: {
        command: 'webpack serve --env prod --port 3000',
        port: 3000,
        launchTimeout: 30000
    }
}
