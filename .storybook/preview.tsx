import React from 'react'
import {useEffect, useState, useCallback} from 'react'
import {Story} from '@storybook/react/types-6-0'
import {AppContext} from '../src/app'
import '../src/desktop-app.css'

const MAX_PHONE_WIDTH = 768
const MAX_TABLET_WIDTH = 1024

export const decorators = [
    (Story: Story) => {
        const [width, setWidth] = useState(window.innerWidth)

        const onResize = useCallback(() => {
            setWidth(window.innerWidth)
        }, [])

        useEffect(() => {
            window.addEventListener('resize', onResize)
            return () => {
                window.removeEventListener('resize', onResize)
            }
        }, [onResize])

        const isPhone = useCallback(() => {
            return width < MAX_PHONE_WIDTH
        }, [width])

        const isTablet = useCallback(() => {
            return !isPhone() && width < MAX_TABLET_WIDTH
        }, [width])

        return (
            <AppContext.Provider value={{isMobile: isPhone(), isTablet: isTablet()}}>
                <div style={{marginBottom: 'auto'}}>
                    <Story />
                </div>
            </AppContext.Provider>
        )
    }
]

export const parameters = {
    controls: {expanded: true}
}
