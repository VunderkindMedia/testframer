const path = require('path')
const custom = require('../webpack.config.js')

module.exports = {
    stories: ['../src/components/**/*.stories.@(js|jsx|ts|tsx)'],
    addons: ['@storybook/addon-controls', '@storybook/addon-viewport', '@storybook/addon-actions'],
    webpackFinal: async config => {
        config.resolve.alias['core-js/modules'] = path.resolve(__dirname, '../node_modules/@storybook/core/node_modules/core-js/modules')

        return {...config, module: {...config.module, rules: custom().module.rules}}
    }
}
