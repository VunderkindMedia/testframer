module.exports = {
    preset: 'jest-puppeteer',
    roots: ['./src/e2e-tests'],
    transform: {
        '^.+\\.tsx?$': 'ts-jest'
    }
}
