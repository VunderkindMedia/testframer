REGISTRY = registry.gitlab.com/steklodom/steklodom-frontend
IMAGE = frontend-dev

build_prod: IMAGE = frontend-prod
build_prod: bundle
build_prod: prod.env
	docker build -t $(REGISTRY)/$(IMAGE) .
	docker push $(REGISTRY)/$(IMAGE)

build_prod_m: IMAGE = frontend-prod-mobile
build_prod_m: bundle_mobile
build_prod_m: prod.env
	docker build -t $(REGISTRY)/$(IMAGE) -f DockerfileMobile .
	docker push $(REGISTRY)/$(IMAGE)

build_dev: bundle
build_dev: dev.env
	docker build -t $(REGISTRY)/$(IMAGE) .
	docker push $(REGISTRY)/$(IMAGE)

build_dev_m: IMAGE = frontend-dev-mobile
build_dev_m: bundle_mobile
build_dev_m: dev.env
	docker build -t $(REGISTRY)/$(IMAGE) -f DockerfileMobile .
	docker push $(REGISTRY)/$(IMAGE)

bundle: node_modules
	npm run build

bundle_mobile: node_modules
	npm run build-mobile

node_modules: package.json
	npm install

%.env:
	cp -rf ./config/$@ ./.env
