module.exports = {
    roots: ['./src'],
    transform: {
        '^.+\\.tsx?$': 'ts-jest'
    },
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|svg)$': '<rootDir>/src/file-mock.js'
    },
    modulePathIgnorePatterns: ['./src/e2e-tests/', './src/screenshot-tests/']
}
