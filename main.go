package main

import (
    "bytes"
    "fmt"
    "log"
    "net/http"
    "strings"
)

func main() {
    http.HandleFunc("/s/images/", func(w http.ResponseWriter, r *http.Request) {
        http.ServeFile(w, r, "./images/"+r.URL.Path[9:])
    })

    http.HandleFunc("/s/docs/", func(w http.ResponseWriter, r *http.Request) {
        http.ServeFile(w, r, "./docs/"+r.URL.Path[7:])
    })

    http.HandleFunc("/post", postRequest)

    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        http.ServeFile(w, r, "./build/"+r.URL.Path[1:])
    })

    http.ListenAndServe(":8080", nil)
}

func postRequest(w http.ResponseWriter, r *http.Request) {
    if r.Method != "POST" {
        http.Error(w, "", http.StatusMethodNotAllowed)
        return
    }

    if err := r.ParseForm(); err != nil {
        log.Printf("ParseForm() err: %+v", err)
        return
    }

    name := r.FormValue("name")
    email := r.FormValue("email")
    phone := r.FormValue("phone")
    formName := r.FormValue("formName")

    log.Printf("lp.framer.ru new request, name: [%s], email: [%s], phone: [%s], formName: [%s]", name, email, phone, formName)

    go sendToRocketchat(fmt.Sprintf("lp.framer.ru: %s; %s; %s; %s;", name, email, phone, formName))
}

func sendToRocketchat(text string) {
    text = strings.Replace(text, "\"", "'", -1)

    url := "https://rchat.it-swarm.pro/hooks/v7fZCXxMxWFFdwQMP/7t9T5gfNFSeaqPuNQno46Zn5vAY5fZv6Hf5gCiDCJZoeY9pW"
    log.Print("URL:>", url)

    message := fmt.Sprintf(`{"username":"framer","icon_emoji":":sheep:","text":"%s" }`, text)
    var jsonStr = []byte(message)
    req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
    req.Header.Set("Content-Type", "application/json")

    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        panic(err)
    }
    defer resp.Body.Close()

    // log.Print("response Status:", resp.Status)
    // log.Print("response Headers:", resp.Header)
    // body, _ := ioutil.ReadAll(resp.Body)
    // log.Print("response Body:", string(body))

    if resp.StatusCode == 200 {
        log.Print("message sent successfully")
    } else {
        log.Print("  ==!! message was not sent !!==")
    }
}
