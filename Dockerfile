FROM golang:1.12 as builder
WORKDIR /go/src/steklodom-frontend
COPY main.go .
COPY go.mod .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o steklodom-frontend

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/steklodom-frontend/steklodom-frontend .
COPY build ./build
COPY docs ./docs
COPY .env ./build/.env
CMD ["./steklodom-frontend"]
